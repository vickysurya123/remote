﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remote.Helpers
{
    public class UrlEncoder
    {
        public static string Encode(string text)
        {
            if(text == null)
            {
                return null;
            }
            string encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(text));

            encoded = reverseChar(encoded);

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(encoded)).TrimEnd('=').Replace('+', '-').Replace('/', '_');

        }

        public static string Decode(string text)
        {
            if(text == null)
            {
                return null;
            }
            text = text.Replace('_', '/').Replace('-', '+');
            switch (text.Length % 4)
            {
                case 2:
                    text += "==";
                    break;
                case 3:
                    text += "=";
                    break;
            }

            string decoded = Encoding.UTF8.GetString(Convert.FromBase64String(text));

            decoded = reverseChar(decoded);

            return Encoding.UTF8.GetString(Convert.FromBase64String(decoded));

        }

        private static string reverseChar(string text)
        {
            string substrFirst = text.Substring(0, 2);
            string substrLast = text.Substring(text.Length - 2, 2);
            text = text.Remove(0, 2);
            text = text.Remove(text.Length - 2, 2);

            text = substrLast + text + substrFirst;

            return text;
        }
    }
}
