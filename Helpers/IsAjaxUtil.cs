﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote
{
    public class IsAjaxUtil
    {
        public static bool validate(HttpContext httpContext)
        {
            bool result = false;
            result = httpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest" ? true : false;
            return result;
        }
    }
}
