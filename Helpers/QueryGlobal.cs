﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Helpers
{
    public class QueryGlobal
    {
        public string Query_BE(string identifier = "")
        {
            identifier = string.IsNullOrWhiteSpace(identifier) ? "" : identifier + ".";
            string v_be = " AND " + identifier + "BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) ";

            return v_be;
        }


        public string Query_Tingkat(string identifier = "")
        {
            identifier = string.IsNullOrWhiteSpace(identifier) ? "" : identifier + ".";
            string v_be = " AND " + identifier + "BRANCH_ID IN (SELECT DISTINCT BRANCH_WHERE FROM V_BE WHERE BRANCH_ID = :d) ";

            return v_be;
        }
    }
}