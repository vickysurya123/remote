﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Helpers
{
    public class TestEmail
    {
        bool IS_TESTING = false;

        //private string EMAIL_APPROVE = "dewzzpro@gmail.com"; // "guntur.herdiawan@pelindo.co.id" // string.Empty; // 
        private string EMAIL_NOTIF = "kumpulanreget@gmail.com"; // "guntur.herdiawan@pelindo.co.id" // string.Empty; // 
        private string EMAIL_APPROVE = "kumpulanreget@gmail.com"; // "guntur.herdiawan@pelindo.co.id" // string.Empty; // 
        private string EMAIL_MAKER = "kumpulanreget@gmail.com"; // "guntur.herdiawan@pelindo.co.id" // string.Empty; // 
        //private string EMAIL_MAKER = "pamudi3311@gmail.com"; // "guntur.herdiawan@pelindo.co.id" // string.Empty; // 
        //private string EMAIL_MAKER = "hafidz.lazuardy@gmail.com";

        public string TEST_EMAIL_APPROVE()
        {
            string email = EMAIL_APPROVE;
            return IS_TESTING ? email : "";
        }


        public string TEST_EMAIL_NOTIF()
        {
            string email = EMAIL_NOTIF;
            return IS_TESTING ? email : "";
        }


        public string TEST_EMAIL_MAKER()
        {
            string email = EMAIL_MAKER;
            return IS_TESTING ? email : "";
        }
    }
}