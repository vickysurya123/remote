﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.Models;
using Remote.Models.MasterVariousBusiness;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class MasterVariousBusinessDAL
    {
        //----------------------- DEPRECATED ----------------------------
        public DataTablesVariousBusiness GetDataAnekaUsaha(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesVariousBusiness result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_VARIOUSBUSINESS_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0" && UserRole == "4")
            {
                if (search.Length == 0)
                {
                    string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH ORDER BY id DESC";
                    
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount);
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {

                        string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE ((be_id) || (UPPER(be_name)) || (service_code) || (UPPER(service_name)) || profit_center || (UPPER(terminal_name)) || (service_group) || (UPPER(ref_desc)) || (gl_account) || (unit) || (tax_code) || (val1) LIKE '%' ||:c|| '%' ) ORDER BY id DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                    }
                }
            }
            else if (KodeCabang == "0" && UserRole == "20")
            {
                if (search.Length == 0)
                {
                    string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE BRANCH_ID=:c ORDER BY id DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE ((be_id) || (UPPER(be_name)) || (service_code) || (UPPER(service_name)) || profit_center || (UPPER(terminal_name)) || (service_group) || (UPPER(ref_desc)) || (gl_account) || (unit) || (tax_code) || (val1) LIKE '%' ||:c|| '%' ) AND BRANCH_ID=:d ORDER BY id DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                    }
                }
            }
            else 
            {
                if (search.Length == 0)
                {
                    string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE BRANCH_ID=:c ORDER BY id DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE ((be_id) || (UPPER(be_name)) || (service_code) || (UPPER(service_name)) || profit_center || (UPPER(terminal_name)) || (service_group) || (UPPER(ref_desc)) || (gl_account) || (unit) || (tax_code) || (val1) LIKE '%' ||:c|| '%' ) AND BRANCH_ID=:d ORDER BY id DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                    }
                }
            }

            result = new DataTablesVariousBusiness();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();

            return result;
        }

        //----------------------- INSERT DATA ANEKA USAHA -----------------------
        public DataReturnVariousBusiness Add(string name, DataVariousBusiness data, string KodeCabang)
        {
            string qr = "";
            string service_code = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "select VAL3 from prop_parameter_ref_d a where a.REF_CODE = 'SERVICE_GROUP' and a.REF_DATA = :a";
            string tax_code = connection.ExecuteScalar<string>(sql, new {a = data.SERVICE_GROUP });

            sql = "INSERT INTO various_business " +
                "(be_id, profit_center, service_code, service_group, service_name, unit, active, tax_code, branch_id,PROFIT_CENTER_NEW) " +
                "VALUES (:a, :b, :c, :d, :e, :f, :h, :i, :j,:b)";

            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.PROFIT_CENTER,
                c = "",
                d = data.SERVICE_GROUP,
                e = data.SERVICE_NAME,
                f = data.UNIT,
                h = "1",
                i = tax_code,
                j = KodeCabang
            });
            //Query last id dan return value nya untuk step selanjutnya
            qr = "SELECT SERVICE_CODE FROM VARIOUS_BUSINESS WHERE BRANCH_ID=:a AND ID=(SELECT MAX(ID) FROM VARIOUS_BUSINESS)";
            service_code = connection.ExecuteScalar<string>(qr, new { a=KodeCabang });


            var resultfull = new DataReturnVariousBusiness();
            resultfull.SERVICE_CODE = service_code;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            // resultfull.datas = listDataOperator.ToList();


            //result  = (r > 0) ? true : false;
            //Mau return rest resultfull.RO_NUMBER & resultfull.RESULT_STAT untuk diambil di controller
            connection.Close();
            return resultfull;

            //result = (r > 0) ? true : false;

            //return result;
        }

        //--------------------------- INSERT DATA PRICING --------------------------
        public bool AddDataPricing(string name, DataVariousBusiness data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            string sqln = "update VARIOUS_BISINESS_PRICING SET ACTIVE=0 WHERE VB_ID=:a";
            int n = connection.Execute(sqln, new
            {
                a = data.VB_ID,
                b = data.UNIT,
                c = data.PRICE,
                //d = data.CONDITION_PRICING_UNIT,
                e = data.MULTIPLY_FUNCTION,
                f = data.VALID_FROM,
                g = data.VALID_TO,
                h = "1"
            });

            string sql = "INSERT INTO VARIOUS_BISINESS_PRICING " +
                "(VB_ID, UNIT, PRICE, MULTIPLY_FUNCTION, VALID_FROM, VALID_TO, ACTIVE, LEGAL_CONTRACT_NO, CONTRACT_DATE, CONTRACT_VALIDITY, BE_ID, SERVICE_CODE, PROFIT_CENTER) " +
                "VALUES (:a, :b, :c, :e, to_date(:f,'DD/MM/YYYY'), to_date(:g,'DD/MM/YYYY'), :h, :i, to_date(:j, 'DD/MM/YYYY'), to_date(:k, 'DD/MM/YYYY'), :l, :m, :n)";
            //connection.Execute($sql);

            int r = connection.Execute(sql, new
            {
                a = data.VB_ID,
                b = data.UNIT,
                c = data.PRICE,
                //d = data.CONDITION_PRICING_UNIT,
                e = data.MULTIPLY_FUNCTION,
                f = data.VALID_FROM,
                g = data.VALID_TO,
                h = "1",
                i = data.LEGAL_CONTRACT_NO,
                j = data.CONTRACT_DATE,
                k = data.CONTRACT_VALIDITY,
                l = data.BE_ID,
                m = data.SERVICE_CODE,
                n = data.PROFIT_CENTER
            });

            result = (r > 0) ? true : false;
            connection.Close();
            return result;
        }

        //----------------------- EDIT DATA ANEKA USAHA ----------------------
        public bool Edit(string name, DataVariousBusiness data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "UPDATE VARIOUS_BUSINESS SET " +
                         "SERVICE_NAME=:b, " +
                         "UNIT=:c " +
                         "WHERE ID=:a";

            int r = connection.Execute(sql, new
            {
                a = data.ID,
                b = data.SERVICE_NAME,
                c = data.UNIT
            });

            result = (r > 0) ? true : false;
            connection.Close();
            return result;
        }


        //----------------------- VIEW EDIT DATA ANEKA USAHA --------------------------------
        public AUP_VARIOUSBUSINESS_Remote GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string sql = "SELECT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, " +
                        "val1, unit " + 
                        "FROM V_VB " +
                         "WHERE id = :a";

            AUP_VARIOUSBUSINESS_Remote result = connection.Query<AUP_VARIOUSBUSINESS_Remote>(sql, new { a = id }).FirstOrDefault();
            connection.Close();
            return result;
        }

        //---------------------- UBAH STATUS ---------------------------------
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT ACTIVE " +
                "FROM VARIOUS_BUSINESS " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE VARIOUS_BUSINESS " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            return result;
        }

        //-------------------- UBAH STATUS PRICING ----------------------
        public dynamic UbahStatusPricing(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT ACTIVE " +
                "FROM VARIOUS_BISINESS_PRICING " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE VARIOUS_BISINESS_PRICING " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    sql = "UPDATE VARIOUS_BISINESS_PRICING " +
                       "SET ACTIVE = :a, VALID_TO=SYSDATE " +
                       "WHERE ID = :b";
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    sql = "UPDATE VARIOUS_BISINESS_PRICING " +
                       "SET ACTIVE = :a, VALID_TO=NULL " +
                       "WHERE ID = :b";

                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            return result;
        }

        //--------------------- VIEW DATA TABLE PRICING -----Coba---------------------
        public DataTablesVariousBusiness GetDataPricingDetail(int draw, int start, int length, string search, string idVB)
        {
            int count = 0;
            int end = start + length;
            DataTablesVariousBusiness result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_VARIOUSBUSINESS_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                ////string sql = "SELECT id_bp, vb_id, price, condition_pricing_unit, unit_1, multiply_function, to_char(valid_from,'DD/MM/YYYY') valid_from, to_char(valid_to, 'DD/MM/YYYY') valid_to, active_1, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD/MM/YYYY') CONTRACT_DATE, to_char(CONTRACT_VALIDITY, 'DD/MM/YYYY') CONTRACT_VALIDITY " +
                ////             "FROM V_VB " +
                ////             "WHERE vb_id=:c ORDER BY id_bp DESC";

                string sql = "SELECT ID AS ID_BP, VB_ID, PRICE, UNIT AS UNIT_1, MULTIPLY_FUNCTION, to_char(VALID_FROM,'DD/MM/YYYY') VALID_FROM, to_char(VALID_TO, 'DD/MM/YYYY') VALID_TO, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD/MM/YYYY') CONTRACT_DATE, to_char(CONTRACT_VALIDITY, 'DD/MM/YYYY') CONTRACT_VALIDITY, ACTIVE AS ACTIVE_1 FROM V_VB_PRICING WHERE VB_ID=:c";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new { a = start, b = end, c = idVB });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idVB });
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesVariousBusiness();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //--------------------- EDIT DATA PRICING ----------------------------
        public bool EditPricing(string name, DataVariousBusiness data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (data.UNIT_1 == null)
            {
                string sql = "UPDATE VARIOUS_BISINESS_PRICING SET " +
                         "PRICE=:a, MULTIPLY_FUNCTION=:c, VALID_FROM=to_date(:e,'DD/MM/YYYY'), VALID_TO=to_date(:f, 'DD/MM/YYYY'), LEGAL_CONTRACT_NO=:h, CONTRACT_DATE=to_date(:i, 'DD/MM/YYYY'), CONTRACT_VALIDITY=to_date(:j, 'DD/MM/YYYY') " +
                         "WHERE ID=:g";

                int r = connection.Execute(sql, new
                {
                    a = data.PRICE,
                    // b = data.CONDITION_PRICING_UNIT,
                    c = data.MULTIPLY_FUNCTION,
                    // d = data.UNIT_1,
                    e = data.VALID_FROM,
                    f = data.VALID_TO,
                    g = data.ID,
                    h = data.LEGAL_CONTRACT_NO,
                    i = data.CONTRACT_DATE,
                    j = data.CONTRACT_VALIDITY
                });

                result = (r > 0) ? true : false;
            }
            else
            {
                string sql = "UPDATE VARIOUS_BISINESS_PRICING SET " +
                         "PRICE=:a, MULTIPLY_FUNCTION=:c, UNIT=:d, VALID_FROM=to_date(:e,'DD/MM/YYYY'), VALID_TO=to_date(:f, 'DD/MM/YYYY'), LEGAL_CONTRACT_NO=:h, CONTRACT_DATE=to_date(:i, 'DD/MM/YYYY'), CONTRACT_VALIDITY=to_date(:j, 'DD/MM/YYYY') " +
                         "WHERE ID=:g";

                int r = connection.Execute(sql, new
                {
                    a = data.PRICE,
                    // b = data.CONDITION_PRICING_UNIT,
                    c = data.MULTIPLY_FUNCTION,
                    d = data.UNIT_1,
                    e = data.VALID_FROM,
                    f = data.VALID_TO,
                    g = data.ID,
                    h = data.LEGAL_CONTRACT_NO,
                    i = data.CONTRACT_DATE,
                    j = data.CONTRACT_VALIDITY
                });

                result = (r > 0) ? true : false;
            }
            connection.Close();
            return result;
        }

        //------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE2(string KodeCabang, string UserRole)
        {
            string sql = "";
            
            if (KodeCabang == "0" && UserRole == "4")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else if (KodeCabang == "0" && UserRole == "20")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }
            else 
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }
           
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a=KodeCabang });
            connection.Close();
            return listDetil;
        }

        //--------------------- LIST DROP DOWN PROFIT CENTER -----------------------
        public IEnumerable<DDProfitCenter> GetDataProfit_2(string KodeCabang, string UserRole, string UserId)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql="";

            if (KodeCabang == "0" && UserRole == "4")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else if (KodeCabang == "0" && UserRole == "20" )
            {
                sql = "SELECT A.PROFIT_CENTER_ID, A.PROFIT_CENTER_ID || ' - ' || A.TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER A JOIN APP_REPO.VW_APP_USER B ON B.BE_ID = A.BE_ID WHERE B.ID=:b AND A.STATUS = 1";
            }
            else
            {
                sql = "SELECT A.PROFIT_CENTER_ID, A.PROFIT_CENTER_ID || ' - ' || A.TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER A JOIN APP_REPO.VW_APP_USER B ON B.BE_ID = A.BE_ID WHERE B.ID=:b AND A.STATUS = 1";
            }

            
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a=KodeCabang, b=UserId });
            connection.Close();
            return listDetil;
        }

        //--------------------- LIST DROP DOWN SERVICE GROUP -----------------------
        public IEnumerable<DDServiceGroup> GetDataService()
        {
            string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC || ' - ' || VAL1 AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' and ACTIVE = 1 ORDER BY VAL1 ASC, ID ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDServiceGroup> listDetil = connection.Query<DDServiceGroup>(sql);
            connection.Close();
            return listDetil;
        }
        public IEnumerable<DDServiceGroup> GetDataServiceNew(string role_id, string user_id)
        {
            string sql = string.Empty;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (role_id == "4")
            {
                sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC || ' | ' || VAL1 AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' and ACTIVE = 1 ORDER BY VAL1 ASC, ID ASC";
                IEnumerable<DDServiceGroup> listDetil = connection.Query<DDServiceGroup>(sql);
                return listDetil;
            }
            else
            {
                sql = @"select c.REF_DATA,  c.REF_DATA || ' - ' || c.REF_DESC || ' | ' || c.VAL1 AS REF_DESC from SETTING_OTHER_SERVICE_DETAIL a
join SETTING_OTHER_SERVICE_HEADER b on a.HEADER_ID = b.ID 
join PROP_PARAMETER_REF_D c on a.VAL1 = c.VAL1
where b.USER_ID =:a and c.REF_CODE = 'SERVICE_GROUP' and c.ACTIVE = 1";
                IEnumerable<DDServiceGroup> listDetil = connection.Query<DDServiceGroup>(sql,new { a = user_id });
                connection.Close();
                return listDetil;
            }
        }

        //--------------------- LIST DROP DOWN UNIT -----------------------
        public IEnumerable<DDUnit> GetDataUnit()
        {
            string sql = "SELECT REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'UNIT' ORDER BY REF_DESC ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDUnit> listDetil = connection.Query<DDUnit>(sql);
            connection.Close();
            return listDetil;
        }

        //------------------------------ NEW GET DATA -------------------------------
        public DataTablesVariousBusiness GetDataAnekaUsahanew(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesVariousBusiness result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_VARIOUSBUSINESS_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND ((be_id) || (UPPER(be_name)) || (service_code) || (UPPER(service_name)) || profit_center || (UPPER(terminal_name)) || (service_group) || (UPPER(ref_desc)) || (gl_account) || (unit) || (tax_code) || (val1) LIKE '%' || '" + search.ToUpper() + "' || '%' ) " : "");

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                        string sql = "SELECT DISTINCT id, be_id || ' - ' || be_name AS be_id, service_code, service_name, profit_center || ' - ' || terminal_name as profit_center, service_group || ' - ' || ref_desc as service_group, active, gl_account, unit, tax_code, val1 " +
                                 "FROM V_VBH WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch + " ORDER BY id DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_VARIOUSBUSINESS_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            //c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });                
            

            result = new DataTablesVariousBusiness();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();

            return result;
        }


    }
}