﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;

namespace Remote.DAL
{
    public class MasgterDataLahanDal
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , TAHUN, LUAS_LAHAN, IS_ACTIVE FROM MASTER_DATA_LAHAN A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<MASTER_DATA_LAHAN>(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM (SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , TAHUN, LUAS_LAHAN, IS_ACTIVE FROM MASTER_DATA_LAHAN A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID) A
                                    WHERE ((UPPER(A.BE_NAME) LIKE '%' ||:c|| '%') or (UPPER(A.LUAS_LAHAN) LIKE '%' ||:c|| '%') or (UPPER(A.TAHUN) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<MASTER_DATA_LAHAN>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }
        public Results AddDataHeader(MASTER_DATA_LAHAN data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"select count(*) from MASTER_DATA_LAHAN where BUSINESS_ENTITY_ID =:a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.BUSINESS_ENTITY_ID
                });

                if (r > 0)
                {
                    result.Msg = "Master Data Lahan BUSINESS_ENTITY_ID Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into MASTER_DATA_LAHAN (BUSINESS_ENTITY_ID, TAHUN, LUAS_LAHAN, USER_ID_CREATE, IS_ACTIVE)
                            values(:a, :b, :c, :d, :e)";
                    r = connection.Execute(sql, new
                    {
                        a = data.BUSINESS_ENTITY_ID,
                        b = data.TAHUN,
                        c = data.LUAS_LAHAN,
                        d = UserID,
                        e = data.IS_ACTIVE,
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results EditDataHeader(MASTER_DATA_LAHAN data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update MASTER_DATA_LAHAN a set a.BUSINESS_ENTITY_ID =:a,  a.TAHUN =:b, 
                        a.LUAS_LAHAN =:c, a.USER_ID_UPDATE =:e, a.IS_ACTIVE = :f where a.ID =:d";
                r = connection.Execute(sql, new
                {
                    a = data.BUSINESS_ENTITY_ID,
                    b = data.TAHUN,
                    c = data.LUAS_LAHAN,
                    d = data.ID,
                    e = UserID,
                    f = data.IS_ACTIVE,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results DeleteDataHeader(MASTER_DATA_LAHAN data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from MASTER_DATA_LAHAN a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = data.ID
                });
                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang, string q)
        {
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY 
                                WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :a)  AND LOWER(BE_NAME) like :b ";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang, b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }
    }
}