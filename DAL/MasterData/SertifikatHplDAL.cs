﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;

namespace Remote.DAL
{
    public class SertifikatHplDAL
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, A.NO_SERTIFIKAT ,A.LUAS_LAHAN, A.MEMO, A.ATTACHMENT, A.PATCH, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    LEFT JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID ORDER BY A.ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM (SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(TANGGAL, 'DD/MM/YYYY') TANGGAL, NO_SERTIFIKAT ,LUAS_LAHAN, MEMO, ATTACHMENT, PATCH, SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID) A
                                    WHERE ((UPPER(A.BE_NAME) LIKE '%' ||:c|| '%') or (UPPER(A.NO_SERTIFIKAT) LIKE '%' ||:c|| '%') or (UPPER(A.LUAS_LAHAN) LIKE '%' ||:c|| '%') or (UPPER(A.TANGGAL) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IDataTable GetDataType(string SERTIFIKATTYPE, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            string sql = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    if (SERTIFIKATTYPE == "all")
                    {
                         sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, A.NO_SERTIFIKAT ,A.LUAS_LAHAN, A.MEMO, A.ATTACHMENT, A.PATCH, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID ORDER BY A.ID DESC";

                    } else
                    {
                         sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, A.NO_SERTIFIKAT ,A.LUAS_LAHAN, A.MEMO, A.ATTACHMENT, A.PATCH, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID WHERE A.SERTIFIKAT_TYPE = :type ORDER BY A.ID DESC";

                    }

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new { a = start, b = end, type = SERTIFIKATTYPE }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, type = SERTIFIKATTYPE });
                }
                else
                {
                    //search filter data
                     sql = @"SELECT * FROM (SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(TANGGAL, 'DD/MM/YYYY') TANGGAL, NO_SERTIFIKAT ,LUAS_LAHAN, MEMO, ATTACHMENT, PATCH, SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID) A
                                    WHERE ((UPPER(A.BE_NAME) LIKE '%' ||:c|| '%') or (UPPER(A.NO_SERTIFIKAT) LIKE '%' ||:c|| '%') or (UPPER(A.LUAS_LAHAN) LIKE '%' ||:c|| '%') or (UPPER(A.TANGGAL) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }


        public IDataTable GetSertifikatBeid(string BUSINESS_ENTITY_ID , int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            string sql = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    if (BUSINESS_ENTITY_ID == "all")
                    {
                        sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, A.NO_SERTIFIKAT ,A.LUAS_LAHAN, A.MEMO, A.ATTACHMENT, A.PATCH, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID ORDER BY A.ID DESC";

                    }
                    else
                    {
                        sql = @"SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, A.NO_SERTIFIKAT ,A.LUAS_LAHAN, A.MEMO, A.ATTACHMENT, A.PATCH, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID WHERE A.BUSINESS_ENTITY_ID = :type ORDER BY A.ID DESC";

                    }

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new { a = start, b = end, type = BUSINESS_ENTITY_ID }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, type = BUSINESS_ENTITY_ID });
                }
                else
                {
                    //search filter data
                    sql = @"SELECT * FROM (SELECT A.ID, BUSINESS_ENTITY_ID, BE_ID || ' - ' || BE_NAME BE_NAME , to_char(TANGGAL, 'DD/MM/YYYY') TANGGAL, NO_SERTIFIKAT ,LUAS_LAHAN, MEMO, ATTACHMENT, PATCH, SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BUSINESS_ENTITY_ID) A
                                    WHERE ((UPPER(A.BE_NAME) LIKE '%' ||:c|| '%') or (UPPER(A.NO_SERTIFIKAT) LIKE '%' ||:c|| '%') or (UPPER(A.LUAS_LAHAN) LIKE '%' ||:c|| '%') or (UPPER(A.TANGGAL) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<SERTIFIKAT_HPL>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }


        public IEnumerable<DDBranch> GetDataListBusinessEntity(string kodecabang)
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string sql = @"SELECT a.BE_ID, a.BE_NAME FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) 
                        ORDER BY BE_NAME ASC";
            //string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY " + where + " ORDER BY BE_NAME ASC";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql, new { d = kodecabang });
            connection.Close();
            return listDetil;

        }


        public Results AddDataHeader(SERTIFIKAT_HPL data, dynamic dataUser, dynamic file2, dynamic sFile)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"insert into SERTIFIKAT_HPL (BUSINESS_ENTITY_ID, TANGGAL, LUAS_LAHAN, USER_ID_CREATE, NO_SERTIFIKAT, ATTACHMENT, PATCH, MEMO, SERTIFIKAT_TYPE)
                            values(:a, to_date(:b,'DD/MM/YYYY'), :c, :d, :e, :f, :g, :h, :i)";
                r = connection.Execute(sql, new
                {
                    a = data.BE_NAME,
                    b = data.TANGGAL,
                    c = data.LUAS_LAHAN,
                    d = UserID,
                    e = data.NO_SERTIFIKAT,
                    f = sFile,
                    g = file2,
                    h = data.MEMO,
                    i = data.SERTIFIKAT_TYPE
                });
                if (r > 0)
                {
                    result.Msg = "Data Added Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            connection.Close();
            return result;
        }
        public Results EditDataHeader(SERTIFIKAT_HPL data, dynamic dataUser, dynamic sfile, dynamic file2)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = "";

                if (sfile != null && file2 != null)
                {
                     sql = @"update SERTIFIKAT_HPL a set a.BUSINESS_ENTITY_ID =:a,  a.TANGGAL = to_date(:b,'DD/MM/YYYY'), 
                        a.LUAS_LAHAN =:c, a.USER_ID_UPDATE =:e, a.NO_SERTIFIKAT =:f  
                        , a.ATTACHMENT =:g , a.PATCH =:h , a.MEMO =:i, a.SERTIFIKAT_TYPE =:j where a.ID =:d";
                }
                else
                {
                     sql = @"update SERTIFIKAT_HPL a set a.BUSINESS_ENTITY_ID =:a,  a.TANGGAL = to_date(:b,'DD/MM/YYYY'), 
                        a.LUAS_LAHAN =:c, a.USER_ID_UPDATE =:e, a.NO_SERTIFIKAT =:f  
                        , a.MEMO =:i, a.SERTIFIKAT_TYPE =:j where a.ID =:d";
                }
                r = connection.Execute(sql, new
                {
                    a = data.BUSINESS_ENTITY_ID,
                    b = data.TANGGAL,
                    c = data.LUAS_LAHAN,
                    d = data.ID,
                    e = UserID,
                    f = data.NO_SERTIFIKAT,
                    g = sfile,
                    h = file2,
                    i = data.MEMO,
                    j = data.SERTIFIKAT_TYPE,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            connection.Close();
            return result;
        }
        public Results DeleteDataHeader(SERTIFIKAT_HPL data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from SERTIFIKAT_HPL a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = data.ID
                });
                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            connection.Close();
            return result;
        }

        public bool AddFiles(string sFile, string directory, string KodeCabang, string BE_ID)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO BUSINESS_ENTITY_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, BE_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = BE_ID
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang, string q)
        {
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {

                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY 
                                WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :a)  AND LOWER(BE_NAME) like :b ";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang, b = $"%{q.ToLower()}%" }).ToList();
                }
            }
            catch (Exception) { }
            return listData;
        }
    }
}