﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.MasterWaterAndElectricity;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.Helpers;

namespace Remote.DAL
{
    public class MasterWaterAndElectricityDAL
    {
        public DataReturnWECode Add(string name, DataWaterAndElectricity data, string KodeCabang)
        {
            bool result = false;
            string qr = "";
            string id_we = "";
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();

            string tariff_code="";

            if (data.INSTALLATION_TYPE == "L-Sambungan Listrik")
            {
                // Ambil Generate TARIFF_CODE L
                tariff_code = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_CODE_L AS TARIFF_CODE_L FROM DUAL");
            }
            else
            {
                // Ambil Generate TARIFF_CODE A 
                tariff_code = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_CODE_A AS TARIFF_CODE_A FROM DUAL");
            }

            if (data.FALID_TO == " ")
            {
                string sql = "INSERT INTO SERVICES " +
                "(INSTALLATION_TYPE, DESCRIPTION, AMOUNT, CURRENCY, X_FACTOR, FALID_FROM, FALID_TO, ACTIVE, UNIT, TARIFF_CODE, PROFIT_CENTER, BRANCH_ID, PROFIT_CENTER_NEW) " +
                "VALUES (:a, :b, :c, :d, :e, to_date(:f,'DD/MM/YYYY'), to_date(:g,'DD/MM/YYYY'), :h, :j, :k, :l, :m, :l)";

                 r = connection.Execute(sql, new
                {
                    a = data.INSTALLATION_TYPE,
                    b = data.DESCRIPTION,
                    c = data.AMOUNT,
                    d = data.CURRENCY,
                    e = data.X_FACTOR,
                    f = data.FALID_FROM,
                    g = "01/01/2100",
                    h = data.ACTIVE,
                    //i = data.PER,
                    j = data.UNIT,
                    k = tariff_code,
                    l = data.PROFIT_CENTER,
                    m = KodeCabang
                });
                result = (r > 0) ? true : false;
            }
            else
            {
                string sql = "INSERT INTO SERVICES " +
                "(INSTALLATION_TYPE, DESCRIPTION, AMOUNT, CURRENCY, X_FACTOR, FALID_FROM, FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, BRANCH_ID, PROFIT_CENTER_NEW) " +
                "VALUES (:a, :b, :c, :d, :e, to_date(:f,'DD/MM/YYYY'), to_date(:g,'DD/MM/YYYY'), :h, :i, :j, :k, :l, :m, :l)";

                 r = connection.Execute(sql, new
                {
                    a = data.INSTALLATION_TYPE,
                    b = data.DESCRIPTION,
                    c = data.AMOUNT,
                    d = data.CURRENCY,
                    e = data.X_FACTOR,
                    f = data.FALID_FROM,
                    g = data.FALID_TO,
                    h = data.ACTIVE,
                    i = data.PER,
                    j = data.UNIT,
                    k = tariff_code,
                    l = data.PROFIT_CENTER,
                    m = KodeCabang
                });
                result = (r > 0) ? true : false;
            }

            //return result;
            //Query last id dan return value nya untuk step selanjutnya
            qr = "SELECT TARIFF_CODE FROM SERVICES WHERE BRANCH_ID=:a AND SERVICES_ID=(SELECT MAX(SERVICES_ID) FROM SERVICES)";
            id_we = connection.ExecuteScalar<string>(qr, new { a=KodeCabang });

            var resultfull = new DataReturnWECode();
            resultfull.TARIFF_CODE = id_we;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            return resultfull;
        }

        public bool EditData(string name, DataWaterAndElectricity data, string KodeCabang)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE SERVICES SET " +
                         "INSTALLATION_TYPE=:a, PROFIT_CENTER=:b, DESCRIPTION=:c, FALID_FROM=to_date(:d,'DD/MM/YYYY'), FALID_TO=to_date(:e, 'DD/MM/YYYY'), " +
                         "AMOUNT=:g, CURRENCY=:h, UNIT=:i, X_FACTOR=:j, PROFIT_CENTER_NEW=:b WHERE TARIFF_CODE=:f";

            int r = connection.Execute(sql, new
            {
                a = data.INSTALLATION_TYPE,
                b = data.PROFIT_CENTER,
                c = data.DESCRIPTION,
                d = data.FALID_FROM,
                e = data.FALID_TO,
                f = data.TARIFF_CODE,
                g = data.AMOUNT,
                h = data.CURRENCY,
                i = data.UNIT,
                j = data.X_FACTOR
            });

            result = (r > 0) ? true : false;
            connection.Close();
            return result;
        }

        public AUP_WE_PRICING GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD/MM/YYYY') as FALID_FROM, " +
                         "to_char(FALID_TO,'DD/MM/YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, decode(ACTIVE,'1','ACTIVE','0','INACTIVE') AS STAT, BRANCH_ID, NAMA_PROFIT_CENTER, TARIFF_CODE FROM V_SERVICES WHERE TARIFF_CODE=:a";

            AUP_WE_PRICING result = connection.Query<AUP_WE_PRICING>(sql, new { a = id }).FirstOrDefault();
            connection.Close();
            return result;
        }

        public IEnumerable<DataDDProfitCenter> GetDataDDProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DataDDProfitCenter> listData = null;

            try
            {
                string sql = "";

                if (KodeCabang == "0")
                {
                    sql = "SELECT * FROM PROFIT_CENTER where STATUS = 1";                    
                }
                else
                {
                    sql = "SELECT * FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";                    
                }

                listData = connection.Query<DataDDProfitCenter>(sql, new { a = KodeCabang }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            return listData;
        }

        //------------------------------------- DEPRECATED ---------------------------------------------
        public DataTablesMasterWaterAndElectricity GetDataWaterAndElectricity(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterWaterAndElectricity result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_WATERANDELECTRICITY_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {

                if (search.Length == 0)
                {
                    /*
                    string sql = "SELECT RO_NUMBER, BE_ID, RO_NAME, RO_CERTIFICATE_NUMBER, RO_ADDRESS, RO_POSTALCODE, " +
                                  "RO_CITY, RO_PROVINCE FROM V_RO" +
                                  "WHERE RO_NUMBER = :c";
                    */
                    //FALID_TO >= trunc(sysdate)
                    string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, CURRENCY || ' ' || AMOUNT AS AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD.MM.YYYY') as FALID_FROM, " +
                                  "to_char(FALID_TO,'DD.MM.YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, decode(ACTIVE,'1','ACTIVE','0','INACTIVE') AS STAT, BRANCH_ID, NAMA_PROFIT_CENTER FROM V_SERVICES ORDER BY SERVICES_ID DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_WATERANDELECTRICITY_Remote>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end});
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        //FALID_TO >= trunc(sysdate)
                        string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, CURRENCY || ' ' || AMOUNT AS AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD.MM.YYYY') as FALID_FROM, " +
                                 "to_char(FALID_TO,'DD.MM.YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, NAMA_PROFIT_CENTER FROM V_SERVICES WHERE " +
                                 "((UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(DESCRIPTION) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY) LIKE '%'||:c||'%') OR (AMOUNT LIKE '%'||:c||'%') OR (to_char(FALID_TO,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (to_char(FALID_FROM,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (PROFIT_CENTER LIKE '%'||:c||'%') OR (NAMA_PROFIT_CENTER LIKE '%'||:c||'%')) " +
                                 "ORDER BY SERVICES_ID DESC";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_WATERANDELECTRICITY_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            c = search.ToUpper(),
                            a = start,
                            b = end
                        });
                    }
                }
            }
            else
            {

                if (search.Length == 0)
                {
                    /*
                    string sql = "SELECT RO_NUMBER, BE_ID, RO_NAME, RO_CERTIFICATE_NUMBER, RO_ADDRESS, RO_POSTALCODE, " +
                                  "RO_CITY, RO_PROVINCE FROM V_RO" +
                                  "WHERE RO_NUMBER = :c";
                    */
                    //FALID_TO >= trunc(sysdate)
                    string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, CURRENCY || ' ' || AMOUNT AS AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD.MM.YYYY') as FALID_FROM, " +
                                  "to_char(FALID_TO,'DD.MM.YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, decode(ACTIVE,'1','ACTIVE','0','INACTIVE') AS STAT, BRANCH_ID, NAMA_PROFIT_CENTER FROM V_SERVICES WHERE BRANCH_ID=:c ORDER BY SERVICES_ID DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_WATERANDELECTRICITY_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        //FALID_TO >= trunc(sysdate)
                        string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, CURRENCY || ' ' || AMOUNT AS AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD.MM.YYYY') as FALID_FROM, " +
                                 "to_char(FALID_TO,'DD.MM.YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, NAMA_PROFIT_CENTER FROM V_SERVICES WHERE " +
                                 "((UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(DESCRIPTION) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY) LIKE '%'||:c||'%') OR (AMOUNT LIKE '%'||:c||'%') OR (to_char(FALID_TO,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (to_char(FALID_FROM,'DD.MM.YYYY') LIKE '%'||:c||'%')) " +
                                 "AND BRANCH_ID=:d ORDER BY SERVICES_ID DESC";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_WATERANDELECTRICITY_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search,
                            d = KodeCabang
                        });
                    }
                }
            }

            result = new DataTablesMasterWaterAndElectricity();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public dynamic DeleteData(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT SERVICES_ID " +
                "FROM SERVICES " +
                "WHERE SERVICES_ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                
                sql = "UPDATE SERVICES " +
                   "SET FALID_TO = trunc(sysdate-1), ACTIVE='0'" +
                   "WHERE SERVICES_ID = :a";
                

                /*
                sql = "DELETE FROM SERVICES " +
                    "WHERE SERVICES_ID = :a";
                */

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {
                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            return result;
        }

        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT ACTIVE " +
                "FROM SERVICES " +
                "WHERE SERVICES_ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE SERVICES " +
                    "SET ACTIVE = :a " +
                    "WHERE SERVICES_ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            return result;
        }

        //------------------------------------ NEW GET DATA -------------------------------------
        public DataTablesMasterWaterAndElectricity GetDataWaterAndElectricitynew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterWaterAndElectricity result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_WATERANDELECTRICITY_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND ((UPPER(TARIFF_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(DESCRIPTION) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (AMOUNT LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CURRENCY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (AMOUNT LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (to_char(FALID_TO,'DD.MM.YYYY') LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (to_char(FALID_FROM,'DD.MM.YYYY') LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
  
                        //FALID_TO >= trunc(sysdate)
                        string sql = "SELECT SERVICES_ID, INSTALLATION_TYPE, DESCRIPTION, CURRENCY || ' ' || AMOUNT AS AMOUNT, CURRENCY, X_FACTOR, TO_CHAR(FALID_FROM, 'DD.MM.YYYY') as FALID_FROM, " +
                                 "to_char(FALID_TO,'DD.MM.YYYY') as FALID_TO, ACTIVE, PER, UNIT, TARIFF_CODE, PROFIT_CENTER, NAMA_PROFIT_CENTER FROM V_SERVICES WHERE " +
                                 "BRANCH_ID IN (SELECT DISTINCT BRANCH_WHERE FROM V_BE WHERE BRANCH_ID = :d) " + wheresearch + " ORDER BY SERVICES_ID DESC";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_WATERANDELECTRICITY_Remote>(fullSql, new
                        {
                            a = start,
                            b = end,
                            //c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            //c = search,
                            d = KodeCabang
                        });
                    

            result = new DataTablesMasterWaterAndElectricity();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }



    }
}