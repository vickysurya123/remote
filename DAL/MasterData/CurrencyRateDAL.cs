﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;

namespace Remote.DAL
{
    public class MasgterCurrencyRate
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                
                if (search.Length == 0)
                {

                    string sql = @"SELECT A.ID, to_char(A.VALIDITY_FROM, 'DD/MM/YYYY') VALIDITY_FROM,to_char(A.VALIDTY_TO, 'DD/MM/YYYY') VALIDTY_TO,
                    A.MULTIPLY_RATE, A.DEVIDE_RATE, B.CODE AS CODE_X, C.CODE AS CODE_Y FROM  CURRENCY_RATE A 
                    JOIN CURRENCY B ON B.ID = A.CURRENCY_FROM JOIN CURRENCY C ON C.ID = A.CURRENCY_TO ORDER BY A.TIME_CREATE DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<CURRENCY_RATE>(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT A.ID, to_char(A.VALIDITY_FROM, 'DD/MM/YYYY') VALIDITY_FROM,to_char(A.VALIDTY_TO, 'DD/MM/YYYY') VALIDTY_TO,
                                    A.MULTIPLY_RATE, A.DEVIDE_RATE, B.CODE AS CODE_X, C.CODE AS CODE_Y FROM  CURRENCY_RATE A 
                                    JOIN CURRENCY B ON B.ID = A.CURRENCY_FROM JOIN CURRENCY C ON C.ID = A.CURRENCY_TO
                                    WHERE ((UPPER(B.CODE) LIKE '%' ||:c|| '%') or (UPPER(C.CODE) LIKE '%' ||:c|| '%') or (UPPER(A.MULTIPLY_RATE) LIKE '%' ||:c|| '%') or (UPPER(A.DEVIDE_RATE) LIKE '%' ||:c|| '%') 
                                     or (UPPER(A.VALIDTY_TO) LIKE '%' ||:c|| '%') or (UPPER(A.VALIDITY_FROM) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<CURRENCY_RATE>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }
        public Results AddDataHeader(CURRENCY_RATE data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                    var sql = @"insert into CURRENCY_RATE (CURRENCY_FROM, CURRENCY_TO, MULTIPLY_RATE, DEVIDE_RATE, USER_ID_CREATE, VALIDITY_FROM, VALIDTY_TO)
                            values(:a, :b, :c, :d, :e,to_date(:f,'DD/MM/YYYY'), to_date(:g,'DD/MM/YYYY'))";
                    r = connection.Execute(sql, new
                    {
                        a = data.CURRENCY_FROM,
                        b = data.CURRENCY_TO,
                        c = data.MULTIPLY_RATE,
                        d = data.DEVIDE_RATE,
                        e = UserID,
                        f = data.VALIDITY_FROM,
                        g = data.VALIDTY_TO,
                        
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results EditDataHeader(CURRENCY_RATE data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update CURRENCY_RATE a set a.CURRENCY_FROM =:a,  a.CURRENCY_TO =:b, a.MULTIPLY_RATE =:c, a.DEVIDE_RATE =:d, 
                        a.VALIDITY_FROM = to_date(:e,'DD/MM/YYYY'), a.VALIDTY_TO = to_date(:f,'DD/MM/YYYY'),
                        a.USER_ID_UPDATE =:g where a.ID =:h";
                r = connection.Execute(sql, new
                {
                    a = data.CURRENCY_FROM,
                    b = data.CURRENCY_TO,
                    c = data.MULTIPLY_RATE,
                    d = data.DEVIDE_RATE,
                    e = data.VALIDITY_FROM,
                    f = data.VALIDTY_TO,
                    g = UserID,
                    h = data.ID,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results DeleteDataHeader(CURRENCY_RATE data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from CURRENCY_RATE a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = data.ID
                    
                });
                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            finally
            {
                connection.Close();
            }

            return result;
        }

        public IEnumerable<DDCurrency> GetDataCurrency(String q)
        {
            IEnumerable<DDCurrency> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT* FROM CURRENCY WHERE LOWER(CODE) like :b";
                    listData = connection.Query<DDCurrency>(sql, new {b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }
    }
}