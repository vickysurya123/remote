﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.Models;
using Remote.Models.MasterInstallation;
using Remote.Models.InstallationB;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class InstallationBDAL
    {

        //------------------------- DEPRECATED ------------------------

        public DataTablesInstallation GetDataInstallation(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesInstallation result = null;

            IEnumerable<AUP_INSTALLATION_Remote> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (KodeCabang == "0")
                    {

                        if (search.Length == 0)
                        {
                            /*
                            string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED " +
                                          "FROM V_INST WHERE BRANCH_ID=:c AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                            */

                            string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, SERIAL_NUMBER," +
                                          "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BUSINESS_ENTITY " +
                                          "FROM V_INST WHERE INSTALLATION_TYPE='L-Sambungan Listrik'";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount);
                        }
                        else
                        {
                            // Search data installation
                            if (search.Length > 2)
                            {
                                /*
                                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED " +
                                          "FROM V_INST WHERE " +
                                          "((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND BRANCH_ID=:d AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                                */

                                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BUSINESS_ENTITY " +
                                          "FROM V_INST WHERE ((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper()
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper() });
                            }
                        }
                    }
                    else
                    {

                        if (search.Length == 0)
                        {
                            /*
                            string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED " +
                                          "FROM V_INST WHERE BRANCH_ID=:c AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                            */

                            string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BUSINESS_ENTITY " +
                                          "FROM V_INST WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:c";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                        }
                        else
                        {
                            // Search data installation
                            if (search.Length > 2)
                            {
                                /*
                                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED " +
                                          "FROM V_INST WHERE " +
                                          "((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND BRANCH_ID=:d AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                                */

                                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                          "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BUSINESS_ENTITY " +
                                          "FROM V_INST WHERE ((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:d";
                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper(),
                                    d = KodeCabang
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string aaa;
                    aaa = ex.ToString();
                }

                result = new DataTablesInstallation();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }

            return result;
        }

        //-------------------------------- INSERT DATA INSTALLATION -------------------------

        public DataReturnInstallation Add(string name, DataInstallation data, string KodeCabang)
        {
            string qr = "";
            string id_installation = "";
            string installation_number = "";
            string installation_code = "";
            string cek_installation_code = "";
            var resultfull = new DataReturnInstallation();
            using (var connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    if (data.INSTALLATION_TYPE == "L-Sambungan Listrik")
                    {
                        // Ambil Generate INSTALLATION L
                        installation_number = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_INSTALLATION_L AS INSTALLATION_L FROM DUAL");

                        /*
                        cek apakah installation code untuk customer_sap_ar yg akan disimpan sudah ada sebelumnya atau belum
                        kalau sudah ada panggil function
                        kalau belum set nilai default increment id = 1
                        * */

                        cek_installation_code = connection.ExecuteScalar<string>("SELECT MAX(SUBSTR (INSTALLATION_CODE,-1)) FROM PROP_SERVICES_INSTALLATION WHERE CUSTOMER_ID= '" + data.CUSTOMER_ID + "' AND SUBSTR(INSTALLATION_TYPE,0,1)='L'");


                        if (cek_installation_code != null)
                        {
                            installation_code = connection.ExecuteScalar<string>("SELECT L_GENCODE2('" + data.CUSTOMER_ID + "') FROM DUAL");
                        }
                        else
                        {
                            installation_code = "L/" + data.CUSTOMER_ID + "/01";
                        }
                    }
                    else
                    {
                        // Ambil Generate INSTALLATION A 
                        installation_number = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_INSTALLATION_A AS INSTALLATION_A FROM DUAL");

                        /*
                        cek apakah installation code untuk customer_sap_ar yg akan disimpan sudah ada sebelumnya atau belum
                        kalau sudah ada panggil function
                        kalau belum set nilai default increment id = 1
                        * */

                        cek_installation_code = connection.ExecuteScalar<string>("SELECT MAX(SUBSTR (INSTALLATION_CODE,-1)) FROM PROP_SERVICES_INSTALLATION WHERE CUSTOMER_ID= '" + data.CUSTOMER_ID + "' AND SUBSTR(INSTALLATION_TYPE,0,1)='A'");


                        if (cek_installation_code != null)
                        {
                            installation_code = connection.ExecuteScalar<string>("SELECT A_GENCODE2('" + data.CUSTOMER_ID + "') FROM DUAL");
                        }
                        else
                        {
                            installation_code = "A/" + data.CUSTOMER_ID + "/01";
                        }
                    }

                    /*
                    string sql = "INSERT INTO PROP_SERVICES_INSTALLATION " +
                       "(INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_ID, INSTALLATION_DATE, POWER_CAPACITY, INSTALLATION_ADDRESS, TARIFF_CODE, TAX_CODE, MINIMUM_AMOUNT, CURRENCY, INSTALLATION_NUMBER, RO_CODE, STATUS, CUSTOMER_NAME, CUSTOMER_SAP_AR, INSTALLATION_CODE, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BRANCH_ID, BIAYA_ADMIN, CURRENCY) " +
                       "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w)";
                    */
                    string sql = "INSERT INTO PROP_SERVICES_INSTALLATION " +
                       "(INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_ID, INSTALLATION_DATE, POWER_CAPACITY, INSTALLATION_ADDRESS, TARIFF_CODE, TAX_CODE, MINIMUM_AMOUNT, CURRENCY, INSTALLATION_NUMBER, RO_CODE, STATUS, CUSTOMER_NAME, CUSTOMER_SAP_AR, INSTALLATION_CODE, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BRANCH_ID, BIAYA_ADMIN, PROFIT_CENTER_NEW,SERIAL_NUMBER) " +
                       "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v,:b, :w)";


                    int r = connection.Execute(sql, new
                    {
                        a = data.INSTALLATION_TYPE,
                        b = data.PROFIT_CENTER,
                        c = data.CUSTOMER_ID,
                        d = data.INSTALLATION_DATE,
                        e = data.POWER_CAPACITY,
                        f = data.INSTALLATION_ADDRESS,
                        g = data.TARIFF_CODE,
                        h = data.TAX_CODE,
                        i = data.MINIMUM_AMOUNT,
                        j = "IDR",
                        k = installation_number,
                        l = data.RO_NUMBER,
                        m = "1",
                        n = data.CUSTOMER_NAME,
                        o = data.CUSTOMER_SAP_AR,
                        p = installation_code,
                        q = data.MINIMUM_PAYMENT,
                        r = data.MULTIPLY_FACT,
                        s = data.BIAYA_BEBAN,
                        t = data.MINIMUM_USED,
                        u = KodeCabang,
                        v = data.BIAYA_ADMIN,
                        W = data.SERIAL_NUMBER
                    });

                    //Query last id dan return value nya untuk step selanjutnya
                    qr = "SELECT INSTALLATION_CODE FROM PROP_SERVICES_INSTALLATION WHERE BRANCH_ID=:a AND ID=(SELECT MAX(ID) FROM PROP_SERVICES_INSTALLATION)";
                    id_installation = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });

                    resultfull.INSTALLATION_NUMBER = id_installation;
                    resultfull.RESULT_STAT = (r > 0) ? true : false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return resultfull;
        }

        //----------------------------- VIEW DATA EDIT --------------------
        public AUP_INSTALLATION_Remote GetDataForEdit(string id)
        {
            var result = new AUP_INSTALLATION_Remote();
            using (var connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    /*
                    string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_ID, " +
                                      "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_NUMBER || ' - ' || RO_NAME AS RO_NUMBER, STATUS, POWER_CAPACITY " +
                                      "FROM V_INST WHERE INSTALLATION_NUMBER = :a";
                    */

                    string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, SERIAL_NUMBER, " +
                                      "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE, " +
                                      "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BIAYA_ADMIN, PROFIT_CENTER_ID, BE_ID " +
                                      "FROM V_INST WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND ID=:a";
                    result = connection.Query<AUP_INSTALLATION_Remote>(sql, new { a = id }).FirstOrDefault();
                }
                catch (Exception)
                {
                }
            }
            return result;
        }

        //------------------------------- UBAH STATUS INSTALASI -------------------------
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            using (var connection = DatabaseFactory.GetConnection())
            //using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "SELECT STATUS " +
                "FROM PROP_SERVICES_INSTALLATION " +
                "WHERE INSTALLATION_NUMBER = :a";

                string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

                if (!string.IsNullOrEmpty(kdAktif))
                {
                    sql = "UPDATE PROP_SERVICES_INSTALLATION " +
                        "SET STATUS = :a " +
                        "WHERE INSTALLATION_NUMBER = :b";

                    if (kdAktif == "1")
                    {
                        connection.Execute(sql, new
                        {
                            a = "0",
                            b = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Status is Inactive."
                        };
                    }
                    else
                    {
                        connection.Execute(sql, new
                        {
                            a = "1",
                            b = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Status is Active."
                        };
                    }
                }
                else
                {
                    result = new
                    {
                        status = "E",
                        message = "Something went wrong."
                    };
                }


                return result;
            }
            
        }

        //----------------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------

        public IEnumerable<DDBusinessEntity> GetDataBE2()
        {
            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                //using (var connection = DatabaseFactory.GetConnection())
                //if (connection.State.Equals(ConnectionState.Closed))
                //    connection.Open();
                IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql);
                return listDetil;
            }
                
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";

            using (var connection = DatabaseFactory.GetConnection())
            {
                IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
                return listDetil;
            }
        }

        //----------------------------- LIST DROP DOWN TARIFF CODE ------------------------

        public IEnumerable<DDTariffCode> GetDataTariff(string KodeCabang)
        {
            string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS AMOUNT FROM SERVICES WHERE BRANCH_ID=:a ORDER BY SERVICES_ID DESC";
            using (var connection = DatabaseFactory.GetConnection())
            {
                IEnumerable<DDTariffCode> listDetil = connection.Query<DDTariffCode>(sql, new { a = KodeCabang });
                return listDetil;
            }
            //    using (var connection = DatabaseFactory.GetConnection())
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
        }

        //----------------------------- LIST DROP DOWN RENTAL OBJECT ------------------------

        public IEnumerable<DDRentalObject> GetDataRental(string KodeCabang)
        {
            string sql = "SELECT RO_CODE, RO_CODE || ' - ' || RO_NAME AS RO_NAME FROM RENTAL_OBJECT WHERE BRANCH_ID=:a ORDER BY RO_CODE DESC";
            using (var connection = DatabaseFactory.GetConnection())
            {
                //using (var connection = DatabaseFactory.GetConnection())
                //if (connection.State.Equals(ConnectionState.Closed))
                //    connection.Open();
                IEnumerable<DDRentalObject> listDetil = connection.Query<DDRentalObject>(sql, new { a = KodeCabang });
                return listDetil;
            }
        }

        // Dropdown tarif sesuai combo L atau A
        public IEnumerable<DDTariffCode> GetDataDropDownTarifL(string KodeCabang, string PROFIT_CENTER)
        {

            IEnumerable<DDTariffCode> listData = null;
            using (var connection = DatabaseFactory.GetConnection())
            { 
            try
            {
                if (PROFIT_CENTER != null && PROFIT_CENTER != "")
                {
                    string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT || ' - ' || DESCRIPTION AS AMOUNT FROM SERVICES WHERE BRANCH_ID=:a AND SUBSTR(TARIFF_CODE,0,1)='L' AND PROFIT_CENTER_NEW=:b ORDER BY SERVICES_ID DESC";

                    listData = connection.Query<DDTariffCode>(sql, new { a = KodeCabang, b = PROFIT_CENTER }).ToList();
                }
                else
                {
                    string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT || ' - ' || DESCRIPTION AS AMOUNT FROM SERVICES WHERE BRANCH_ID=:a AND SUBSTR(TARIFF_CODE,0,1)='L' ORDER BY SERVICES_ID DESC";

                    listData = connection.Query<DDTariffCode>(sql, new { a = KodeCabang }).ToList();
                }
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                
            }
            catch (Exception) { }
            }

            return listData;
        }

        public IEnumerable<DDTariffCode> GetDataDropDownTarifA(string KodeCabang)
        {

            IEnumerable<DDTariffCode> listData = null;
            using (var connection = DatabaseFactory.GetConnection())
            { 
            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS AMOUNT FROM SERVICES WHERE BRANCH_ID=:a AND SUBSTR(TARIFF_CODE,0,1)='A' ORDER BY SERVICES_ID DESC";

                listData = connection.Query<DDTariffCode>(sql, new { a=KodeCabang }).ToList();
            }
            catch (Exception) { }
            }

            return listData;
        }

        //Add Pricing
        public bool AddPricing(string name, InstallationBPricing data, string KodeCabang)
        {
            bool result = false;
            using (var connection = DatabaseFactory.GetConnection())
            { 
            string sql = "INSERT INTO PROP_SERVICES_INSTALLATION_P " +
                "(INSTALLATION_ID, PRICE_TYPE, PRICE_CODE, MAX_RANGE_USED, INSTALLATION_CODE, BRANCH_ID) " +
                "VALUES (:a, :b, :c, :d, :e, :f)";

                int r = connection.Execute(sql, new
                {
                    a = data.INSTALLATION_ID,
                    b = data.PRICE_TYPE,
                    c = data.PRICE_CODE,
                    d = data.MAX_RANGE_USED,
                    e = data.INSTALLATION_CODE,
                    f = KodeCabang
                });

                result = (r > 0) ? true : false;
            }

            return result;
        }

        //------------------------------- DATA TABLE PRICING --------------------------
        public DataTablePricing GetDataPricing(int draw, int start, int length, string search, string id, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablePricing result = null;
            IEnumerable<InstallationBPricing> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    /*
                    string sql = "SELECT id, be_id, measurement_type, description, amount, unit, valid_from, valid_to " +
                            "FROM business_entity_detail ORDER BY ID DESC ";
                     * */
                    string sql = "SELECT ID, INSTALLATION_ID, PRICE_TYPE, p.PRICE_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS PRICE_CODE , MAX_RANGE_USED " +
                             "FROM PROP_SERVICES_INSTALLATION_P p, SERVICES s WHERE s.TARIFF_CODE=p.PRICE_CODE AND INSTALLATION_ID=:c AND p.BRANCH_ID=:d ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<InstallationBPricing>(fullSql, new { a = start, b = end, c = id, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id, d = KodeCabang });
                }
                else
                {
                    // Do nothing. Not implemented yet.
                }
            }

            result = new DataTablePricing();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();


            return result;
        }

        //------------Edit Data Pricing
        public bool editPricing(string name, InstallationBPricing data)
        {
            bool result = false;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "UPDATE PROP_SERVICES_INSTALLATION_P SET " +
                             "PRICE_TYPE=:b, PRICE_CODE=:c, MAX_RANGE_USED=:d " +
                             "WHERE ID=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.ID,
                    b = data.PRICE_TYPE,
                    c = data.PRICE_CODE,
                    d = data.MAX_RANGE_USED
                });

                result = (r > 0) ? true : false;
            }

            return result;
        }

        public dynamic deletePricing(string id)
        {
            dynamic result = null;

            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "SELECT ID " +
                    "FROM PROP_SERVICES_INSTALLATION_P " +
                    "WHERE ID = :a";

                string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

                if (!string.IsNullOrEmpty(recStat))
                {
                    sql = "DELETE FROM PROP_SERVICES_INSTALLATION_P " +
                        "WHERE ID = :a";

                    if (recStat == "Y")
                    {
                        connection.Execute(sql, new
                        {

                            a = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Data Deleted Successfully."
                        };
                    }
                    else
                    {
                        connection.Execute(sql, new
                        {

                            a = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Data Deleted Successfully."
                        };
                    }
                }
                else
                {
                    result = new
                    {
                        status = "E",
                        message = "Error Deleting Data."
                    };
                }
            }

            return result;
        }

        public bool AddCosting(string name, InstallationBPricing data, string KodeCabang)
        {
            bool result = false;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "INSERT INTO PROP_SERVICES_INSTALLATION_C " +
                    "(SERVICES_INSTALLATION_ID, DESCRIPTION, PERCENTAGE, INSTALLATION_CODE, BRANCH_ID) " +
                    "VALUES (:a, :b, :c, :d, :e)";

                int r = connection.Execute(sql, new
                {
                    a = data.SERVICES_INSTALLATION_ID,
                    b = data.DESCRIPTION,
                    c = data.PERCENTAGE,
                    d = data.INSTALLATION_CODE,
                    e = KodeCabang
                });

                result = (r > 0) ? true : false;
            }
            return result;
        }

        public DataTablePricing GetDataCosting(int draw, int start, int length, string search, string id, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablePricing result = null;

            IEnumerable<InstallationBPricing> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    /*
                    string sql = "SELECT id, be_id, measurement_type, description, amount, unit, valid_from, valid_to " +
                            "FROM business_entity_detail ORDER BY ID DESC ";
                     * */
                    string sql = "SELECT ID, SERVICES_INSTALLATION_ID, DESCRIPTION, PERCENTAGE FROM PROP_SERVICES_INSTALLATION_C " +
                                 "WHERE SERVICES_INSTALLATION_ID=:c AND BRANCH_ID=:d ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<InstallationBPricing>(fullSql, new { a = start, b = end, c = id, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id, d = KodeCabang });
                }
                else
                {
                    // Do nothing. Not implemented yet.
                }
            }

            result = new DataTablePricing();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();


            return result;
        }

        //------------Edit Data Costing
        public bool editCosting(string name, InstallationBPricing data)
        {
            bool result = false;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "UPDATE PROP_SERVICES_INSTALLATION_C SET " +
                             "DESCRIPTION=:b, PERCENTAGE=:c " +
                             "WHERE ID=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.ID,
                    b = data.DESCRIPTION,
                    c = data.PERCENTAGE
                });

                result = (r > 0) ? true : false;
            }

            return result;
        }

        public dynamic deleteCosting(string id)
        {
            dynamic result = null;

            using (var connection = DatabaseFactory.GetConnection())
            {
                string sql = "SELECT ID " +
                    "FROM PROP_SERVICES_INSTALLATION_C " +
                    "WHERE ID = :a";

                string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

                if (!string.IsNullOrEmpty(recStat))
                {
                    sql = "DELETE FROM PROP_SERVICES_INSTALLATION_C " +
                        "WHERE ID = :a";

                    if (recStat == "Y")
                    {
                        connection.Execute(sql, new
                        {

                            a = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Data Deleted Successfully."
                        };
                    }
                    else
                    {
                        connection.Execute(sql, new
                        {

                            a = id
                        });

                        result = new
                        {
                            status = "S",
                            message = "Data Deleted Successfully."
                        };
                    }
                }
                else
                {
                    result = new
                    {
                        status = "E",
                        message = "Error Deleting Data."
                    };
                }
            }


            return result;
        }

        public bool UpdateDataHeader(string name, DataInstallation data)
        {
            bool result = false;
            using (var connection = DatabaseFactory.GetConnection())
            {
                /*
                string sql = "UPDATE PROP_SERVICES_INSTALLATION SET " +
                             "PROFIT_CENTER=:b, CUSTOMER_ID=:c, CUSTOMER_NAME=:d, CUSTOMER_SAP_AR=:e, INSTALLATION_DATE=to_date(:f,'DD/MM/YYYY'), POWER_CAPACITY=:g, " +
                             "INSTALLATION_ADDRESS=:h, TARIFF_CODE=:i, MINIMUM_AMOUNT=:j, RO_CODE=:k, MINIMUM_USED=:l, MINIMUM_PAYMENT=:m, MULTIPLY_FACT=:n, " +
                             "BIAYA_ADMIN=:p " +
                             "WHERE ID=:a";
                */

                string sql = "UPDATE PROP_SERVICES_INSTALLATION SET " +
                             "PROFIT_CENTER=:b, CUSTOMER_ID=:c, CUSTOMER_NAME=:d, CUSTOMER_SAP_AR=:e, INSTALLATION_DATE=to_date(:f,'DD/MM/YYYY'), POWER_CAPACITY=:g, " +
                             "INSTALLATION_ADDRESS=:h, TARIFF_CODE=:i, MINIMUM_AMOUNT=:j, RO_CODE=:k, MINIMUM_USED=:l, MINIMUM_PAYMENT=:m, MULTIPLY_FACT=:n, BIAYA_ADMIN=:p,PROFIT_CENTER_NEW=:b,SERIAL_NUMBER=:o " +
                             "WHERE ID=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.ID,
                    b = data.PROFIT_CENTER,
                    c = data.CUSTOMER_ID,
                    d = data.CUSTOMER_NAME,
                    e = data.CUSTOMER_SAP_AR,
                    f = data.INSTALLATION_DATE,
                    g = data.POWER_CAPACITY,
                    h = data.INSTALLATION_ADDRESS,
                    i = data.TARIFF_CODE,
                    j = data.MINIMUM_AMOUNT,
                    k = data.RO_CODE,
                    l = data.MINIMUM_USED,
                    m = data.MINIMUM_PAYMENT,
                    n = data.MULTIPLY_FACT,
                    p = data.BIAYA_ADMIN,
                    O = data.SERIAL_NUMBER
                });

                result = (r > 0) ? true : false;
            }

            return result;
        }

        //------------------------------------- NEW GET DATA --------------------------------------
        public DataTablesInstallation GetDataInstallationnew(int draw, int start, int length, string search, string KodeCabang)
      {
            int count = 0;
            int end = start + length;
            DataTablesInstallation result = null;
            IEnumerable<AUP_INSTALLATION_Remote> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR(SERIAL_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%' ) OR (INSTALLATION_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(TARIFF_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CURRENCY_1) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (AMOUNT LIKE '%' || '" + search.ToUpper() + "' || '%') OR (RO_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(RO_NAME) LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
                QueryGlobal query = new QueryGlobal();
                string v_be = query.Query_BE();

                /*
                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                          "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED " +
                          "FROM V_INST WHERE " +
                          "((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND BRANCH_ID=:d AND INSTALLATION_TYPE='L-Sambungan Listrik'";
                */

                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE,SERIAL_NUMBER, " +
                          "to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, " +
                          "MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BUSINESS_ENTITY " +
                          "FROM V_INST WHERE INSTALLATION_TYPE='L-Sambungan Listrik' " + v_be + wheresearch;
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new
                {
                    a = start,
                    b = end,
                    //c = search.ToUpper(),
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });
           
            result = new DataTablesInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            }

            return result;
        }


    }
}