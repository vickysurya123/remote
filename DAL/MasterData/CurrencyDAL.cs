﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;

namespace Remote.DAL
{
    public class MasgterCurrency
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT CODE, SYMBOL, USER_ID_CREATE, USER_ID_UPDATE, TIME_CREATE, TIME_UPDATE, ID, to_char(VALIDITY_FROM, 'DD/MM/YYYY') VALIDITY_FROM, to_char(VALIDTY_TO, 'DD/MM/YYYY') VALIDTY_TO   FROM CURRENCY ORDER BY TIME_CREATE DESC "; 

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<CURRENCY>(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM CURRENCY A
                                    WHERE ((UPPER(A.CODE) LIKE '%' ||:c|| '%') or (UPPER(A.SYMBOL) LIKE '%' ||:c|| '%') or (UPPER(A.VALIDITY_FROM) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<CURRENCY>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }
        public Results AddDataHeader(CURRENCY data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"select count(*) from CURRENCY where CODE =:a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.CODE
                });

                if (r > 0)
                {
                    result.Msg = "Master Data Currency CODE Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into CURRENCY (CODE, SYMBOL, USER_ID_CREATE, VALIDITY_FROM, VALIDTY_TO)
                            values(:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'))";
                    r = connection.Execute(sql, new
                    {
                        a = data.CODE,
                        b = data.SYMBOL,
                        c = UserID,
                        d = data.VALIDITY_FROM,
                        e = data.VALIDTY_TO,
                        
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results EditDataHeader(CURRENCY data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update CURRENCY a set a.CODE =:a,  a.SYMBOL =:b, 
                        a.VALIDITY_FROM = to_date(:c,'DD/MM/YYYY'), a.VALIDTY_TO = to_date(:d,'DD/MM/YYYY'),
                        a.USER_ID_UPDATE =:e where a.ID =:f";
                r = connection.Execute(sql, new
                {
                    a = data.CODE,
                    b = data.SYMBOL,
                    c = data.VALIDITY_FROM,
                    d = data.VALIDTY_TO,
                    e = UserID,
                    f = data.ID,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            finally
            {
                connection.Close();
            }

            return result;
        }
        public Results DeleteDataHeader(CURRENCY data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from CURRENCY a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = data.ID
                    
                });
                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            finally
            {
                connection.Close();
            }

            return result;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang, string q)
        {
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY 
                                WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :a)  AND LOWER(BE_NAME) like :b ";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang, b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }
    }
}