﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.Models;
using Remote.Models.MasterInstallation;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class MasterInstallationDAL
    {

        //------------------------- DEPRECATED ------------------------

        public DataTablesInstallation GetDataInstallation(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_INSTALLATION_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0") 
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                  "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT AS MIN_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, BUSINESS_ENTITY, MINIMUM_USED " +
                                  "FROM V_INST WHERE INSTALLATION_TYPE='A-Sambungan Air'";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    // Search data installation
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                 "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT AS MIN_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, BUSINESS_ENTITY, MINIMUM_USED " +
                                 "FROM V_INST WHERE " +
                                 "((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') " +
                                 "OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND INSTALLATION_TYPE='A-Sambungan Air'";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                  "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, BUSINESS_ENTITY, MINIMUM_USED " +
                                  "FROM V_INST WHERE BRANCH_ID=:c AND INSTALLATION_TYPE='A-Sambungan Air'";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    // Search data installation
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                  "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, BUSINESS_ENTITY, MINIMUM_USED " +
                                  "FROM V_INST WHERE " +
                                  "((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%' ) OR (INSTALLATION_CODE LIKE '%' ||:c|| '%') OR (UPPER(TARIFF_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CURRENCY_1) LIKE '%' ||:c|| '%') OR (AMOUNT LIKE '%' ||:c|| '%') OR (RO_CODE LIKE '%' ||:c|| '%') OR (UPPER(RO_NAME) LIKE '%'||:c||'%')) AND BRANCH_ID=:d AND INSTALLATION_TYPE='A-Sambungan Air'";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {
                            
                        }
                    }
                }
            }

            result = new DataTablesInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        //-------------------------------- INSERT DATA INSTALLATION -------------------------
        public DataReturnInstallation Add(string name, DataInstallation data, string KodeCabang)
        {
            string qr = "";
            string id_installation = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string installation_number = "";
            string installation_code = "";
            string cek_installation_code = "";
            string mata_uang = "";

            if (data.INSTALLATION_TYPE == "L-Sambungan Listrik")
            {
                // Ambil Generate INSTALLATION L
                installation_number = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_INSTALLATION_L AS INSTALLATION_L FROM DUAL");

                /*
                cek apakah installation code untuk customer_sap_ar yg akan disimpan sudah ada sebelumnya atau belum
                kalau sudah ada panggil function
                kalau belum set nilai default increment id = 1
                * */

                cek_installation_code = connection.ExecuteScalar<string>("SELECT MAX(SUBSTR (INSTALLATION_CODE,-1)) FROM PROP_SERVICES_INSTALLATION WHERE CUSTOMER_ID= '" + data.CUSTOMER_ID + "' AND SUBSTR(INSTALLATION_TYPE,0,1)='L'");


                if (cek_installation_code != null)
                {
                    installation_code = connection.ExecuteScalar<string>("SELECT L_GENCODE2('" + data.CUSTOMER_ID + "') FROM DUAL");
                }
                else
                {
                    installation_code = "L/" + data.CUSTOMER_ID + "/01";
                }
            }
            else
            {
                // Ambil Generate INSTALLATION A 
                installation_number = connection.ExecuteScalar<string>("SELECT GEN_TARIFF_INSTALLATION_A AS INSTALLATION_A FROM DUAL");

                /*
                cek apakah installation code untuk customer_sap_ar yg akan disimpan sudah ada sebelumnya atau belum
                kalau sudah ada panggil function
                kalau belum set nilai default increment id = 1
                * */

                cek_installation_code = connection.ExecuteScalar<string>("SELECT MAX(SUBSTR (INSTALLATION_CODE,-1)) FROM PROP_SERVICES_INSTALLATION WHERE CUSTOMER_ID= '" + data.CUSTOMER_ID + "' AND SUBSTR(INSTALLATION_TYPE,0,1)='A'");


                if (cek_installation_code != null)
                {
                    installation_code = connection.ExecuteScalar<string>("SELECT A_GENCODE2('" + data.CUSTOMER_ID + "') FROM DUAL");
                }
                else
                {
                    installation_code = "A/" + data.CUSTOMER_ID + "/01";
                }
            }

            //GET MATA UANG
            mata_uang = connection.ExecuteScalar<string>("SELECT CURRENCY FROM SERVICES WHERE TARIFF_CODE='"+data.TARIFF_CODE+"'");

            string sql = "INSERT INTO PROP_SERVICES_INSTALLATION " +
               "(INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_ID, INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE, TAX_CODE, MINIMUM_AMOUNT, CURRENCY, INSTALLATION_NUMBER, RO_CODE, STATUS, CUSTOMER_NAME, CUSTOMER_SAP_AR, INSTALLATION_CODE, BRANCH_ID, PROFIT_CENTER_NEW) " +
               "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :b)";

            int r = connection.Execute(sql, new
            {
                a = data.INSTALLATION_TYPE,
                b = data.PROFIT_CENTER,
                c = data.CUSTOMER_ID,
                d = data.INSTALLATION_DATE,
                //e = data.POWER_CAPACITY,
                f = data.INSTALLATION_ADDRESS,
                g = data.TARIFF_CODE,
                h = data.TAX_CODE,
                i = data.MINIMUM_AMOUNT,
                j = mata_uang,
                k = installation_number,
                l = data.RO_NUMBER,
                m = "1",
                n = data.CUSTOMER_NAME,
                o = data.CUSTOMER_SAP_AR,
                p = installation_code,
                q = KodeCabang
            });

            //Query last id dan return value nya untuk step selanjutnya
            qr = "SELECT INSTALLATION_CODE FROM PROP_SERVICES_INSTALLATION WHERE BRANCH_ID=:a AND ID=(SELECT MAX(ID) FROM PROP_SERVICES_INSTALLATION)";
            id_installation = connection.ExecuteScalar<string>(qr, new { a=KodeCabang });

            var resultfull = new DataReturnInstallation();
            resultfull.INSTALLATION_NUMBER = id_installation;
            resultfull.RESULT_STAT = (r > 0) ? true : false;

            connection.Close();
            return resultfull;
        }

        //----------------------------- VIEW DATA EDIT --------------------
        public AUP_INSTALLATION_Remote GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            /*
            string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_ID, " +
                              "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_NUMBER || ' - ' || RO_NAME AS RO_NUMBER, STATUS, POWER_CAPACITY " +
                              "FROM V_INST WHERE INSTALLATION_NUMBER = :a";
            */
            string sql = "SELECT INSTALLATION_NUMBER, to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, PROFIT_CENTER_ID, CUSTOMER_NAME, CUSTOMER_ID, CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, " + 
                         "TARIFF_CODE, MINIMUM_AMOUNT " +
                         "FROM V_INST WHERE INSTALLATION_NUMBER=:a";

            AUP_INSTALLATION_Remote result = connection.Query<AUP_INSTALLATION_Remote>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            return result;
        }

        //------------------------------- UBAH STATUS INSTALASI -------------------------
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT STATUS " +
                "FROM PROP_SERVICES_INSTALLATION " +
                "WHERE INSTALLATION_NUMBER = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_SERVICES_INSTALLATION " +
                    "SET STATUS = :a " +
                    "WHERE INSTALLATION_NUMBER = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            return result;
        }

        //----------------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------

        public IEnumerable<DDBusinessEntity> GetDataBE2()
        {
            string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql);
            connection.Close();
            return listDetil;
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE STATUS = 1";
            }
            else 
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";                
            }
            
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a=KodeCabang });
            connection.Close();
            return listDetil;
        }

        //----------------------------- LIST DROP DOWN TARIFF CODE ------------------------

        public IEnumerable<DDTariffCode> GetDataTariff()
        {
            string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS AMOUNT FROM SERVICES ORDER BY SERVICES_ID DESC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDTariffCode> listDetil = connection.Query<DDTariffCode>(sql);
            connection.Close();
            return listDetil;
        }

        //----------------------------- LIST DROP DOWN RENTAL OBJECT ------------------------

        public IEnumerable<DDRentalObject> GetDataRental(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0") 
            {
                sql = "SELECT RO_CODE, RO_CODE || ' - ' || RO_NAME AS RO_NAME FROM RENTAL_OBJECT ORDER BY RO_CODE DESC";
            }
            else
            {
                sql = "SELECT RO_CODE, RO_CODE || ' - ' || RO_NAME AS RO_NAME FROM RENTAL_OBJECT WHERE BRANCH_ID=:a ORDER BY RO_CODE DESC";
            }
            
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRentalObject> listDetil = connection.Query<DDRentalObject>(sql, new { a=KodeCabang });
            connection.Close();
            return listDetil;
        }

        // Dropdown tarif sesuai combo L atau A
        public IEnumerable<DDTariffCode> GetDataDropDownTarifL(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDTariffCode> listData = null;

            try
            {
               // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS AMOUNT FROM SERVICES WHERE SUBSTR(TARIFF_CODE,0,1)='L' AND BRANCH_ID=:a ORDER BY SERVICES_ID DESC";

                listData = connection.Query<DDTariffCode>(sql, new { a=KodeCabang }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            return listData;
        }

        public IEnumerable<DDTariffCode> GetDataDropDownTarifA(string KodeCabang, string PROFIT_CENTER)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDTariffCode> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT TARIFF_CODE, TARIFF_CODE || ' - ' || CURRENCY || ' - ' || AMOUNT AS AMOUNT FROM SERVICES WHERE SUBSTR(TARIFF_CODE,0,1)='A' AND BRANCH_ID=:a AND PROFIT_CENTER_NEW=:b ORDER BY SERVICES_ID DESC";

                listData = connection.Query<DDTariffCode>(sql, new { a=KodeCabang, b = PROFIT_CENTER }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataDropDownProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            string sql = "";
            try
            {
                if (KodeCabang == "0")
                {
                    sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
                }
                else
                {
                    sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";
                }

                listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public bool Edit(string name, DataInstallation data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE PROP_SERVICES_INSTALLATION SET " +
                         "INSTALLATION_TYPE=:a, PROFIT_CENTER=:b, CUSTOMER_ID=:c, " +
                         "CUSTOMER_NAME=:d, CUSTOMER_SAP_AR=:e, INSTALLATION_DATE=to_date(:f,'DD/MM/YYYY'), INSTALLATION_ADDRESS=:g, " +
                         "TARIFF_CODE=:h, MINIMUM_AMOUNT=:i, PROFIT_CENTER_NEW=:b " +
                         "WHERE INSTALLATION_NUMBER=:j";

            int r = connection.Execute(sql, new
            {
                a = data.INSTALLATION_TYPE,
                b = data.PROFIT_CENTER,
                c = data.CUSTOMER_ID,
                d = data.CUSTOMER_NAME,
                e = data.CUSTOMER_SAP_AR,
                f = data.INSTALLATION_DATE,
                g = data.INSTALLATION_ADDRESS,
                h = data.TARIFF_CODE,
                i = data.MINIMUM_AMOUNT,
                j = data.INSTALLATION_NUMBER
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        //-------------------------------- NEW GET DATA --------------------------------
        public DataTablesInstallation GetDataInstallationnew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_INSTALLATION_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_TYPE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%' ) OR (INSTALLATION_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(TARIFF_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CURRENCY_1) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (AMOUNT LIKE '%' || '" + search.ToUpper() + "' || '%') OR (RO_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(RO_NAME) LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                    {
                        string sql = "SELECT INSTALLATION_NUMBER, INSTALLATION_TYPE, CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_SAP_AR, PROFIT_CENTER, STATUS, POWER_CAPACITY, INSTALLATION_CODE, " +
                                  "to_char(INSTALLATION_DATE,'DD.MM.YYYY') INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, BUSINESS_ENTITY, MINIMUM_USED " +
                                  "FROM V_INST WHERE INSTALLATION_TYPE='A-Sambungan Air' " + v_be + wheresearch;


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_INSTALLATION_Remote>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }                               

            result = new DataTablesInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }


    }
}