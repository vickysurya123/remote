﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.TransWEList;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransWEListDAL
    {
        //--------------------------------------- DEPRECATED ----------------------
        public DataTablesTransWEList GetDataTransWEList(int draw, int start, int length, string search, string status_dinas, string KodeCabang)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0") 
            {
                if (search.Length == 0)
                {
                    try 
                    {
                    /*
                    string sql = "SELECT * FROM V_TRANS_WE_LIST ORDER BY ID DESC";
                    */

                    string sql = @"SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                                    AMOUNT, STATUS, STATUS_DINAS, to_char(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, 
                                    CANCEL_STATUS FROM V_TRANS_WE_LIST_HEADER ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, x_status_dinas = x_status_dinas });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, x_status_dinas = x_status_dinas });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try 
                        {
                            /*
                            string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS " +
                                         "FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(CUSTOMER)) || (PERIOD) || (AMOUNT) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) LIKE '%' ||:c|| '%' ) ORDER BY ID DESC";
                            */
                            /*    string sql = "SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS " +
                                         "FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' ||:c|| '%' ) ORDER BY ID DESC"; */
                            string sql = @"SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, STATUS_DINAS
                                         FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' ||:c|| '%' ) ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try {
                        //string sql = "SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, to_char(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, BRANCH_ID FROM V_TRANS_WE_LIST_HEADER WHERE BRANCH_ID=:c AND BRANCH_ID_1=:c ORDER BY ID DESC";

                        string sql = @"SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, STATUS_DINAS,
                                        to_char(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, BRANCH_ID FROM V_TRANS_WE_LIST_HEADER 
                                        WHERE BRANCH_ID=:c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try {
                            //string sql = "SELECT * FROM V_TRANS_WE_LIST WHERE (ID LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            /*string sql = "SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, TO_CHAR(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS " +
                                     "FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' ||:c|| '%' ) AND BRANCH_ID=:d AND BRANCH_ID_1=:d ORDER BY ID DESC"; */
                            string sql = @"SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, TO_CHAR(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, STATUS_DINAS
                                         FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' ||:c|| '%' ) AND BRANCH_ID=:d ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE TRANS WE --------------------------
        public DataTablesTransWEList GetDataTransactionWater(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    /*
                    string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER ||' - '||TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, PERIOD, METER_FROM, METER_TO, QUANTITY, UNIT, " +
                                 "MULTIPLY_FACTOR, PRICE, AMOUNT, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, SAP_DOCUMENT_NUMBER, SURCHARGE, SUB_TOTAL, CUSTOMER_NAME " +
                                 "FROM V_TRANS_WE_LIST_DETAIL WHERE ID=:c AND INSTALLATION_CODE=:d";
                     */

                    //string sql = "SELECT * FROM V_ANJUNGAN_HEADER_LISTRIK WHERE KD_TRANSAKSI=:c AND INSTALLATION_NUMBER=:d";
                    string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER ||' - '||TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, PERIOD, METER_FROM, METER_TO, QUANTITY, UNIT,
                             MULTIPLY_FACTOR, PRICE, AMOUNT, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, SAP_DOCUMENT_NUMBER, SURCHARGE, SUB_TOTAL, CUSTOMER_NAME, COA_PROD
                             , RATE, AMOUNT/NVL(RATE, 1) GRAND_TOTAL_USD FROM V_TRANS_WE_LIST_DETAIL WHERE ID=:c AND INSTALLATION_CODE=:d";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d = installation_code });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }


        public DataTablesTransWEList GetDataTransactionWE(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try {
                /*
                string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER ||' - '||TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, PERIOD, METER_FROM, METER_TO, QUANTITY, UNIT, " +
                             "MULTIPLY_FACTOR, PRICE, AMOUNT, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, SAP_DOCUMENT_NUMBER, SURCHARGE, SUB_TOTAL, CUSTOMER_NAME " +
                             "FROM V_TRANS_WE_LIST_DETAIL WHERE ID=:c AND INSTALLATION_CODE=:d";
                 */

                    string sql = "SELECT * FROM V_ANJUNGAN_HEADER_LISTRIK WHERE KD_TRANSAKSI=:c AND INSTALLATION_NUMBER=:d";
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d = installation_code });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }


        public DataTablesTransWEList GetDataTransactionWEDetail(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    /*
                    string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER ||' - '||TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, PERIOD, METER_FROM, METER_TO, QUANTITY, UNIT, " +
                                 "MULTIPLY_FACTOR, PRICE, AMOUNT, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, SAP_DOCUMENT_NUMBER, SURCHARGE, SUB_TOTAL, CUSTOMER_NAME " +
                                 "FROM V_TRANS_WE_LIST_DETAIL WHERE ID=:c AND INSTALLATION_CODE=:d";
                     */

                    string sql = "SELECT * FROM V_ANJUNGAN_DETAIL_LISTRIK WHERE KD_TRANSAKSI=:c";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d = installation_code });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesTransWEList GetDataTransactionWEDetailsUsed(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT * FROM USED_DETAIL WHERE ID_TRANSACTION=:c";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d = installation_code });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
 
        //------------------------GET DATA FOR EDIT TRANSAKSI AIR LISTRIK--------------------
        public AUP_TRANS_WE_LIST_WEBAPPS GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT * FROM V_TRANS_WE_LIST WHERE ID=:a";

            AUP_TRANS_WE_LIST_WEBAPPS result = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //-----------------------UPDATE DATA TRANSAKSI WE------------------------------------
        public bool Edit(string name, DataTransWEList data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"UPDATE PROP_SERVICES_TRANSACTION SET METER_FROM=:a, METER_TO=:b, QUANTITY=:d, AMOUNT=:e, SURCHARGE=:f, SUB_TOTAL=:g, 
            COA_PROD=:h, STATUS_DINAS=:j, GRAND_TOTAL=:total, updated_by=:name, updated_date=sysdate, RATE =:rate WHERE ID=:c";

            int r = connection.Execute(sql, new
            {
                a = data.METER_FROM,
                b = data.METER_TO,
                c = data.ID,
                d = data.QUANTITY,
                e = data.AMOUNT,
                f = data.SURCHARGE,
                g = data.SUB_TOTAL,
                h = data.COA_PROD,
                j = data.STATUS_DINAS,
                total = data.GRAND_TOTAL,
                name = name,
                rate = data.RATE
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public AUP_TRANS_ELECTRICITY GetDataForEditElectricity(string idTrans)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            // Ambil dari V_EDIT_TRANS_ELECTRICITY
            /*AUP_TRANS_ELECTRICITY
            string sql = "SELECT i.ID, i.INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_ID, CUSTOMER_NAME, AMOUNT, X_FACTOR, UNIT, INSTALLATION_NUMBER, TERMINAL_NAME, " +
                          "i.PROFIT_CENTER AS PROFIT_CENTER, i.MINIMUM_AMOUNT, (SELECT MAX(METER_TO) FROM PROP_SERVICES_TRANSACTION WHERE ID_INSTALASI = :a) METER_FROM " +
                          "FROM " +
                          "PROP_SERVICES_INSTALLATION i, SERVICES s, PROFIT_CENTER p, PROP_SERVICES_TRANSACTION t  WHERE s.TARIFF_CODE = i.TARIFF_CODE AND  i.PROFIT_CENTER= p.PROFIT_CENTER_ID AND t.INSTALLATION_CODE = i.INSALLATION_CODE AND t.ID = :a";
            */
            string sql = "SELECT TRANSACTION_ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER, TERMINAL_NAME, CUSTOMER_ID, INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_NUMBER, CUSTOMER_NAME, CUSTOMER_SAP_AR, INSTALLATION_CODE, MINIMUM_PAYMENT, MULTIPLY_FACT, MINIMUM_USED, BRANCH_ID, BIAYA_ADMIN, BIAYA_BEBAN, STATUS_DINAS WHERE TRANSACTION_ID='43000533'";
            AUP_TRANS_ELECTRICITY result = connection.Query<AUP_TRANS_ELECTRICITY>(sql, new { a = idTrans }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        // Get data header instalasi 
        public IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> getHeaderElectricityEdit(string TRANSACTION_ID, string INSTALLATION_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, to_char(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, POWER_CAPACITY, MINIMUM_USED, MINIMUM_PAYMENT, INSTALLATION_ADDRESS, MULTIPLY_FACT, BIAYA_ADMIN, DESCRIPTION, TARIFF_CODE, BIAYA_BEBAN, BRANCH_ID, STATUS_DINAS, RATE FROM V_TRANS_WE_LIST_HEADER WHERE ID=:a AND INSTALLATION_ID=:b";

                listData = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(sql, new { a = TRANSACTION_ID, b = INSTALLATION_ID }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastLWBPFrom(string id_transaksi, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_FROM, METER_TO, USED, COA_PROD,KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='LWBP' AND SERVICES_TRANSACTION_ID=:a " + v_be +" )";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = id_transaksi, d = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastLWBPTo(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='LWBP' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_WBP> getLastWBPFrom(string id_transaksi, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_WBP> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_FROM, METER_TO, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='WBP' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_WBP>(sql, new { a = id_transaksi, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        public IEnumerable<DD_ELECTRICITY_TRANS_WBP> getUsedLWBP(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_WBP> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='LWBP' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_WBP>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_WBP> getLastWBPTo(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_WBP> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='WBP' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_WBP>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_WBP> getUsedWBP(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_WBP> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='WBP' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_WBP>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastKVARHFrom(string id_transaksi, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_FROM, METER_TO, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='KVARH' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = id_transaksi, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastKVARHTo(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='KVARH' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getUsedKVARH(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='KVARH' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastBLOKSATUFrom(string id_transaksi, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_FROM, METER_TO, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='BLOK1' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = id_transaksi, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastBLOKDUAFrom(string id_transaksi, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_FROM, METER_TO, USED, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='BLOK2' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = id_transaksi, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastBLOKSATUTo(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='BLOK1' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        public IEnumerable<DD_ELECTRICITY_TRANS_D> getUsedBLOKSATU(string installationCode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, USED, KETARANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='BLOK1' AND SERVICES_TRANSACTION_ID=:a AND BRANCH_ID=:b)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode, b = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public DataTablesTransWEList GetDataTransWEListnew(int draw, int start, int length, string search, string status_dinas, string KodeCabang)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";         
            string wheresearch = (search.Length >= 2 ? " AND ((ID) || (UPPER(INSTALLATION_ID)) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' || '" + search.ToUpper() + "' || '%' ) " : "");

            try
            {

                //string sql = "SELECT * FROM V_TRANS_WE_LIST WHERE (ID LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                /*string sql = "SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, TO_CHAR(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS " +
                         "FROM V_TRANS_WE_LIST_HEADER WHERE ((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (TERMINAL_NAME) || (AMOUNT) || (UPPER(CUSTOMER_NAME)) || (PERIOD) || (AMOUNT) || (POWER_CAPACITY) || (TERMINAL_NAME) || (PROFIT_CENTER) || (POSTING_DATE) || (SAP_DOCUMENT_NUMBER) LIKE '%' ||:c|| '%' ) AND BRANCH_ID=:d AND BRANCH_ID_1=:d ORDER BY ID DESC"; */

                string sql = @"SELECT DISTINCT ID, INSTALLATION_ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER, 
                                CUSTOMER_NAME, PERIOD, AMOUNT, STATUS, TO_CHAR(POSTING_DATE,'DD/MM/YYYY') POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE,
                                INSTALLATION_NUMBER, POWER_CAPACITY, CANCEL_STATUS, STATUS_DINAS, BRANCH_ID, BILLING_TYPE, GRAND_TOTAL, SURCHARGE, RATE, GRAND_TOTAL / NVL(RATE,1) GRAND_TOTAL_USD
                                FROM V_TRANS_WE_LIST_HEADER WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch + " ORDER BY ID DESC";

                fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_WE_LIST_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                //c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                //c = search.ToUpper(),
                                d = KodeCabang
                            });
           
             }
             catch (Exception)
             {

             }
                    

            result = new DataTablesTransWEList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}