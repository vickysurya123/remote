﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.TransWE;
using Remote.Models.TransElectricity;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Newtonsoft.Json;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransElectricityDAL
    {
        public AUP_TRANS_ELECTRICITY GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, CUSTOMER_ID, INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_NUMBER, CUSTOMER_NAME, CUSTOMER_SAP_AR, INSTALLATION_CODE, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, BIAYA_ADMIN, INSTALLATION_NUMBER, BIAYA_BEBAN BRANCH_ID FROM V_H_ADD_TRANS_ELECTRICITY WHERE ID = :a";

            AUP_TRANS_ELECTRICITY result = connection.Query<AUP_TRANS_ELECTRICITY>(sql, new { a = id }).FirstOrDefault();
            connection.Close();

            connection.Dispose();
            return result;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastLWBP(string installationCode)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='LWBP' AND INSTALLATION_CODE=:a)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode }).ToList();
            }
            catch (Exception) { }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_WBP> getLastWBP(string installationCode)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_WBP> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='WBP' AND INSTALLATION_CODE=:a)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_WBP>(sql, new { a = installationCode }).ToList();
            }
            catch (Exception) { }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastKVARH(string installationCode)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='KVARH' AND INSTALLATION_CODE=:a)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode }).ToList();
            }
            catch (Exception) { }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        public IEnumerable<DD_ELECTRICITY_TRANS_D> getLastBLOKSATU(string installationCode)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DD_ELECTRICITY_TRANS_D> listData = null;

            try
            {
                string sql = "SELECT ID, PRICE_TYPE, METER_TO, KETERANGAN FROM PROP_SERVICES_TRANSACTION_D WHERE ID = (SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION_D WHERE PRICE_TYPE='BLOK1' AND INSTALLATION_CODE=:a)";

                listData = connection.Query<DD_ELECTRICITY_TRANS_D>(sql, new { a = installationCode }).ToList();
            }
            catch (Exception) { }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        // Tidak jadi terpakai
        public DataReturnAddElectricityTrans getDataEelectricityInstallation(string name, DataTransElectricity data, string id)
        {
            string qr = "";
            string id_ro = "";
            string ro_code = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();


            var resultfull = new DataReturnAddElectricityTrans();
            resultfull.RO_NUMBER = ro_code;
            resultfull.RO_ID = id_ro;
            //resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        // Tidak Terpakai
        public DataReturnMany GetDataForEditReturn(string name, DataTransWE data, string KodeCabang)
        {
            string billing_type = "";
            string qr = "";
            string trans_code = "";
            //string kode_transaksi = "";

            IDbConnection connection = DatabaseFactory.GetConnection();

            qr = "SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:a";
            trans_code = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });

            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D (SERVICES_TRANSACTION_ID, PRICE_TYPE, PRICE_CODE, TARIFF, METER_FROM, " +
                         "METER_TO, USED, MULTIPLY, BRANCH_ID, INSTALLATION_CODE)" +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)";
            int r = connection.Execute(sql, new
            {
                a = trans_code,
                b = data.PRICE_TYPE,
                c = data.PRICE_CODE,
                d = data.TARIFF,
                e = data.METER_FROM,
                f = data.METER_TO,
                g = data.USED,
                h = data.MULTIPLY,
                i = KodeCabang,
                j = data.INSTALLATION_CODE
            });

            var resultfull = new DataReturnMany();
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        //----GET DATA PRICING
        public IEnumerable<DT_PRICING> GetDataPricing(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DT_PRICING> listData = null;

            try
            {
                string sql= "SELECT P_ID, PRICE_TYPE, PRICE_CODE, AMOUNT, MULTIPLY_FACT, MAX_RANGE_USED, CURRENCY FROM V_TRANS_PRICING WHERE ID=:a AND BRANCH_ID=:b ORDER BY PRICE_TYPE ASC";

                listData = connection.Query<DT_PRICING>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        //----GET DATA COSTING
        public IEnumerable<DT_COSTING> GetDataCosting(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DT_COSTING> listData = null;

            try
            {
                string sql = "SELECT DESCRIPTION, PERCENTAGE FROM V_TRANS_COSTING WHERE SERVICES_INSTALLATION_ID=:a AND BRANCH_ID=:b";

                listData = connection.Query<DT_COSTING>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }
            connection.Close();

            connection.Dispose();
            return listData;
        }

        public DataReturnWENumber Add(string name, DataTransWE data, string KodeCabang)
        {
            string id_we = "";
            string billing_type = "";
            //string status_dinas = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string trans_code = "";
            var jsonString = JsonConvert.SerializeObject(data);

            if (data.INSTALLATION_TYPE == "A-Sambungan Air")
            {
                //trans_code = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CODE_A AS TRANS_CODE_A FROM DUAL");
                string _BE_ID = "1";
                string _param1 = "42";
                trans_code = DatabaseHelper.GetPenomoranData(_BE_ID, "TRANS_AIR", _param1, 6);

                billing_type = "ZM01";
            }
            else
            {
                //trans_code = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CODE_L AS TRANS_CODE_L FROM DUAL");
                string _BE_ID = "1";
                string _param1 = "43";
                trans_code = DatabaseHelper.GetPenomoranData(_BE_ID, "TRANS_LISTRIK", _param1, 6);

                billing_type = "ZM02";
            }

            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION " +
                "(ID, ID_INSTALASI, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER, PERIOD, MULTIPLY_FACTOR, AMOUNT, UNIT, INSTALLATION_NUMBER, BILLING_TYPE, BRANCH_ID, INSTALLATION_CODE, QUANTITY, CUSTOMER_NAME, CREATED_DATE, CREATED_BY, STATUS_DINAS, RATE) " +
                "VALUES (:p, :a, :b, :c, :d, :e, :i, :j, :l, :n, :o, :q, :s, :t, :u, sysdate, :v, :z, :yy)";


            int r = connection.Execute(sql, new
            {
                a = data.ID_INSTALASI,
                b = data.INSTALLATION_TYPE,
                c = data.PROFIT_CENTER,
                d = data.CUSTOMER,
                e = data.PERIOD,
                i = data.MULTIPLY_FACTOR,
                j = data.AMOUNT,
                l = data.UNIT,
                n = data.INSTALLATION_NUMBER,
                o = billing_type,
                p = trans_code,
                q = KodeCabang,
                s = data.INSTALLATION_CODE,
                t = data.QUANTITY,
                u = data.CUSTOMER_NAME,
                v = name,
                z = data.STATUS_DINAS,
                yy = data.RATE,
            });

            // insert data pricing
            if (data.detailpricings != null)
            {
                string sqlPricing = "INSERT INTO PROP_SERVICES_TRANSACTION_D (SERVICES_TRANSACTION_ID, PRICE_TYPE, PRICE_CODE, TARIFF, METER_FROM, " +
                         "METER_TO, USED, MULTIPLY, BRANCH_ID, INSTALLATION_CODE, COA_PROD,KETERANGAN, CURRENCY_RATE )" +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m)";
                foreach (var i in data.detailpricings)
                {
                    try
                    {
                        r = connection.Execute(sqlPricing, new
                        {
                            a = trans_code,
                            b = i.PRICE_TYPE,
                            c = i.PRICE_CODE,
                            d = i.TARIFF,
                            e = i.METER_FROM,
                            f = i.METER_TO,
                            g = i.USED,
                            h = i.MULTIPLY,
                            i = KodeCabang,
                            j = i.INSTALLATION_CODE,
                            k = i.COA_PROD,
                            l = i.KETERANGAN,
                            m = data.RATE,
                        });
                        r = 1;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }
                }
            }

            // insert data costing
            if (data.detailcostings != null)
            {
                string sqlCosting = "INSERT INTO PROP_SERVICES_TRANSACTION_C (DESCRIPTION, PERCENTAGE, SERVICES_TRANSACTION_ID, BRANCH_ID) " +
                         "VALUES (:a, :b, :c, :d)";
                foreach (var i in data.detailcostings)
                {
                    try
                    {
                        r = connection.Execute(sqlCosting, new
                        {
                            a = i.DESCRIPTION,
                            b = i.PERCENTAGE,
                            c = trans_code,
                            d = KodeCabang
                        });
                        r = 1;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }
                }
            }

            var resultfull = new DataReturnWENumber();
            resultfull.WE_NUMBER = trans_code;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        public DataReturnWENumber AddDetailUsed(DataTransWE data)
        {
            
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            int r = 0;
            string _BE_ID = "1";
            string _param1 = "10";
            string trans_used = DatabaseHelper.GetPenomoranData(_BE_ID, "TRANS_USED", _param1, 6);

            var list = JsonConvert.DeserializeObject<dynamic>(data.LIST);

            foreach (var i in list)
            {
                string sql = "INSERT INTO USED_DETAIL (SUMBER, PRICE_TYPE, FAKTOR_PENGALI, TGL_STAN_AWAL, TGL_STAN_AKHIR, " +
                         "STAN_AWAL, STAN_AKHIR, SELISIH, KWH_TENAN, KWH_DITAGIHKAN, TOTAL, ID_TRANSACTION) " +
                         "VALUES ('"+ i.SUMBER + "', '"+ i.PRICE_TYPE + "', '"+ i.PENGALI + "', to_date('" + i.TGL_STAN_AWAL + "' , 'YYYY-MM-DD'), to_date('" + i.TGL_STAN_AKHIR + "' , 'YYYY-MM-DD'), '"+ i.STAN_AWAL + "', '"+ i.STAN_AKHIR + "', '"+ i.SELISIH + "', '"+ i.KWH_TENAN + "', '"+ i.KWH_DITAGIHKAN + "', '"+ data.TOTAL + "', '"+ data.ID_TRANSACTION + "')";
                r = connection.Execute(sql);
            }

            var resultfull = new DataReturnWENumber();
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        public DataReturnWENumber AddDetailPerhitungan(string name, DataTransWE data, string KodeCabang)
        {
            string billing_type = "";
            string qr = "";
            string trans_code = "";
            //string kode_transaksi = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            qr = "SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:a";
            trans_code = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });
            //kode_transaksi = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });
            
            /*
            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D " +
                "(ID, ID_INSTALASI, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER, PERIOD, MULTIPLY_FACTOR, AMOUNT, UNIT, INSTALLATION_NUMBER, BILLING_TYPE, BRANCH_ID, INSTALLATION_CODE) " +
                "VALUES (:p, :a, :b, :c, :d, :e, :i, :j, :l, :n, :o, :q, :s)";
            */

            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D (SERVICES_TRANSACTION_ID, PRICE_TYPE, PRICE_CODE, TARIFF, METER_FROM, " +
                         "METER_TO, USED, MULTIPLY, BRANCH_ID, INSTALLATION_CODE)" +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)";
            int r = connection.Execute(sql, new
            {
                a = trans_code,
                b = data.PRICE_TYPE,
                c = data.PRICE_CODE,
                d = data.TARIFF,
                e = data.METER_FROM,
                f = data.METER_TO,
                g = data.USED,
                h = data.MULTIPLY,
                i = KodeCabang,
                j = data.INSTALLATION_CODE
            });

            var resultfull = new DataReturnWENumber();
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }


        public DataReturnWENumber AddDetailCosting(string name, DataTransWE data, string KodeCabang)
        {
            string billing_type = "";
            string qr = "";
            string trans_code = "";
            //string kode_transaksi = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            qr = "SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:a";
            trans_code = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });
            //kode_transaksi = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });

            /*
            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D " +
                "(ID, ID_INSTALASI, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER, PERIOD, MULTIPLY_FACTOR, AMOUNT, UNIT, INSTALLATION_NUMBER, BILLING_TYPE, BRANCH_ID, INSTALLATION_CODE) " +
                "VALUES (:p, :a, :b, :c, :d, :e, :i, :j, :l, :n, :o, :q, :s)";
            */

            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_C (DESCRIPTION, PERCENTAGE, SERVICES_TRANSACTION_ID, BRANCH_ID) " +
                         "VALUES (:a, :b, :c, :d)";
            int r = connection.Execute(sql, new
            {
                a = data.DESCRIPTION,
                b = data.PERCENTAGE,
                c = trans_code,
                d = KodeCabang
            });

            var resultfull = new DataReturnWENumber();
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        public DataReturnWENumber AddDetailPricing(string name, DataTransWE data, string KodeCabang)
        {
            string billing_type = "";
            string qr = "";
            string trans_code = "";
            //string kode_transaksi = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            qr = "SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION WHERE INSTALLATION_TYPE='L-Sambungan Listrik' AND BRANCH_ID=:a";
            trans_code = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });
            //kode_transaksi = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });


            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D (SERVICES_TRANSACTION_ID, PRICE_TYPE, PRICE_CODE, TARIFF, METER_FROM, METER_TO, USED, MULTIPLY, BRANCH_ID, INSTALLATION_CODE) " +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)";
            int r = connection.Execute(sql, new
            {
                a = trans_code,
                b = data.PRICE_TYPE,
                c = data.PRICE_CODE,
                d = data.TARIFF,
                e = data.METER_FROM,
                f = data.METER_TO,
                g = data.USED,
                h = data.MULTIPLY,
                i = KodeCabang,
                j = data.INSTALLATION_CODE
            });

            var resultfull = new DataReturnWENumber();
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();

            connection.Dispose();
            return resultfull;
        }

        public bool UpdateDataHeader(string name, DataTransWE data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"UPDATE PROP_SERVICES_TRANSACTION SET
                            AMOUNT=:a, QUANTITY=:b, MULTIPLY_FACTOR=:d, PERIOD=:e, STATUS_DINAS=:f, RATE =:g
                            WHERE ID=:c";
            int r = connection.Execute(sql, new
            {
                a = data.AMOUNT,
                b = data.QUANTITY,
                c = data.ID_TRANSAKSI,
                d = data.MULTIPLY_FACTOR,
                e = data.PERIOD,
                f = data.STATUS_DINAS,
                g = data.RATE,
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        // Update detail pricing
        public bool UpdateDetailPricing(string name, DataTransWE data, string KodeCabang)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            int r = 0;

            if (data.ID_EDIT == null)
            {
                string sql = "INSERT INTO PROP_SERVICES_TRANSACTION_D (SERVICES_TRANSACTION_ID, PRICE_TYPE, PRICE_CODE, TARIFF, METER_FROM, METER_TO, USED, MULTIPLY, BRANCH_ID, INSTALLATION_CODE,COA_PROD,KETERANGAN, RATE ) " +
                          "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m)";
                r = connection.Execute(sql, new
                {
                    a = data.ID_TRANSAKSI,
                    b = data.PRICE_TYPE,
                    c = data.PRICE_CODE,
                    d = data.TARIFF,
                    e = data.METER_FROM,
                    f = data.METER_TO,
                    g = data.USED,
                    h = data.MULTIPLY,
                    i = KodeCabang,
                    j = data.INSTALLATION_CODE,
                    k = data.COA_PROD,
                    l = data.KETERANGAN,
                    m = data.RATE,
                });
            }
            else
            {
                string sql = "UPDATE PROP_SERVICES_TRANSACTION_D SET " +
                         "PRICE_TYPE=:a, PRICE_CODE=:b, TARIFF=:c, METER_FROM=:d, METER_TO=:e, USED=:f, MULTIPLY=:g, INSTALLATION_CODE=:h, COA_PROD=:k, KETERANGAN=:j, RATE=:l WHERE ID=:i";

                r = connection.Execute(sql, new
                {
                    a = data.PRICE_TYPE,
                    b = data.PRICE_CODE,
                    c = data.TARIFF,
                    d = data.METER_FROM,
                    e = data.METER_TO,
                    f = data.USED,
                    g = data.MULTIPLY,
                    h = data.INSTALLATION_CODE,
                    i = data.ID_EDIT,
                    j = data.KETERANGAN,
                    k = data.COA_PROD,
                    l = data.RATE,
                });
            }           

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_TRANS_WE_WEBAPPS> getLastPeriod(string id, string id_periode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT PERIOD FROM PROP_SERVICES_TRANSACTION WHERE ID_INSTALASI=:a AND PERIOD=:b AND BRANCH_ID=:c AND CANCEL_STATUS IS NULL";

                listData = connection.Query<AUP_TRANS_WE_WEBAPPS>(sql, new
                {
                    a = id,
                    b = id_periode,
                    c = KodeCabang
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }
    }
   
}