﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.TransVB;
using Remote.Models.TransVBList;
using Remote.Models.Select2;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper; 
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransVBListDAL
    {
        public DataTablesTransVB GetDataVBTransList(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {

                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (KodeCabang == "0" && UserRole == "4")
                {
                    if (search.Length == 0)
                    {
                        /*
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB ORDER BY ID DESC";
                        */
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB WHERE ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) || (GL_ACCOUNT) || (UPPER(VAL2)) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            */
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                    "FROM V_TRANSVB_NEW WHERE ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                    }
                }
                else if (KodeCabang == "0" && UserRole == "20")
                {
                    if (search.Length == 0)
                    {
                        /*
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:c ORDER BY ID DESC";
                        */
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:c ORDER BY ID DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data 
                        if (search.Length > 0) 
                        {
                            /*
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) || (GL_ACCOUNT) || (UPPER(VAL2)) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                           */

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                    }
                } 
                else
                {
                    if (search.Length == 0)
                    {
                        /*
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:c ORDER BY ID DESC";
                        */
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:c ORDER BY ID DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) || (GL_ACCOUNT) || (UPPER(VAL2)) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            */
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                    }
                }

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DDServiceGroup> GetDataDropDownServiceGroup(string user_id, string role_id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDServiceGroup> listData = null;

            try
            {
                string sql = string.Empty;

                if (role_id == "4")
                {
                    sql ="SELECT DISTINCT  VAL1, VAL3, VAL4, VAL1 || ' - ' || VAL2 PENDAPATAN FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' AND ACTIVE = 1 AND VAL1 NOT IN('4070300000','4070100000') ORDER BY VAL1 ASC";
                    listData = connection.Query<DDServiceGroup>(sql, new { a = user_id }).ToList();
                }
                else
                {
                    sql = @"select a.VAL1,a.VAL3,a.VAL4, a.PENDAPATAN from SETTING_OTHER_SERVICE_DETAIL a
                                join SETTING_OTHER_SERVICE_HEADER b on a.HEADER_ID = b.ID where b.USER_ID =:a";
                    listData = connection.Query<DDServiceGroup>(sql, new { a = user_id }).ToList();
                }
                //string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC || ' - ' || VAL1 AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' AND ACTIVE = '1' ORDER BY VAL1 ASC, ID ASC";
                
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        public AUP_TRANSVB_WEBAPPS GetDataForEdit(string id)
        {
            AUP_TRANSVB_WEBAPPS result = new AUP_TRANSVB_WEBAPPS();
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "SELECT ID, PROFIT_CENTER, GL_ACCOUNT, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TAX_CODE, PPH_CODE, RATE FROM V_TRANSVB WHERE ID=:a";

                    result = connection.Query<AUP_TRANSVB_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

                }
                catch (Exception)
                {

                    throw;
                }
            }

            return result;
        }

        public IEnumerable<DDProfitCenter> GetDataDropDownProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            try
            {
                string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a  and STATUS = 1";
                listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        public Select2ServiceCode GetServiceCodeSelect2(string id, string Search, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            Select2ServiceCode data = null;
            try
            {
                data = new Select2ServiceCode();
                Search = "%" + Search + "%";

                
                string query = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%'  AND a.ACTIVE = '1' AND b.ACTIVE ='1' ORDER BY a.SERVICE_CODE ASC";
                
                
                connection.Open();
                try
                {
                    data.items = connection.Query<OptionServiceCode>(query, new { SERVICE_NAME = Search, c = KodeCabang });
                    data.total_count = data.items.Count();
                    data.incomplete_results = false;
                }
                catch (Exception) { }
                connection.Close();
                connection.Dispose();
            }
            catch (Exception)
            {
                data = null;
            }
            return data;
        }

        //------------------------------ LIST AUTO COMPLETE SERVICE CODE ----------------
        public List<AUP_SERVICE_CODE_WEBAPPS> GetDataServiceCode(string SERVICE_NAME, string SERVICE_GROUP, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            //string str = "SELECT a.ID, a.TAX_CODE, a.SERVICE_CODE code, a.SERVICE_CODE || ' - ' || a.SERVICE_NAME label, a. SERVICE_NAME, a. SERVICE_NAME, a.UNIT || ' | ' ||(SELECT DISTINCT ref_desc FROM prop_parameter_ref_d WHERE a.UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION multiply, b.VB_ID, b.ACTIVE, VAL1 GL_ACCOUNT " +
            //                "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b, PROP_PARAMETER_REF_D d " +
            //                "WHERE a.ID = b.VB_ID(+) AND ((UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%') OR (a.SERVICE_CODE LIKE '%'||:SERVICE_NAME||'%')) AND a.ACTIVE = '1' AND b.ACTIVE ='1' AND SERVICE_GROUP=:SERVICE_GROUP AND a.BRANCH_ID=:a " +
            //                "AND REF_DATA=:SERVICE_GROUP";

            string str = "SELECT a.ID, (select VAL3 from prop_parameter_ref_d a where a.REF_CODE = 'SERVICE_GROUP' and a.REF_DATA = a.SERVICE_GROUP) as TAX_CODE, a.SERVICE_CODE code, a.SERVICE_CODE || ' - '  || a.SERVICE_NAME label, a.SERVICE_NAME, a.UNIT || ' | ' ||(SELECT DISTINCT ref_desc FROM prop_parameter_ref_d WHERE a.UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION multiply, b.VB_ID, b.ACTIVE, a.SERVICE_GROUP, d.VAL1 GL_ACCOUNT " +
                               "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b, PROP_PARAMETER_REF_D d " +
                               "WHERE a.ID = b.VB_ID(+) AND ((UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%') OR (a.SERVICE_CODE LIKE '%'||:SERVICE_NAME||'%')) AND a.ACTIVE = '1' AND b.ACTIVE ='1' " +
                               "AND a.SERVICE_GROUP=d.REF_DATA AND d.REF_CODE='SERVICE_GROUP' AND d.VAL1=:SERVICE_GROUP AND a.BRANCH_ID=:a " +
                               "ORDER BY a.SERVICE_CODE ASC";

            
            List<AUP_SERVICE_CODE_WEBAPPS> exec = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                exec = connection.Query<AUP_SERVICE_CODE_WEBAPPS>(str, new
                {
                    SERVICE_NAME = SERVICE_NAME,
                    SERVICE_GROUP = SERVICE_GROUP,
                    a = KodeCabang
                }).ToList();
            }
            catch (Exception) { }
            connection.Close();

            connection.Dispose();
            return exec;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string MPLG_NAMA, string KodeCabang, string UserRole)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            if (KodeCabang == "0")
            {
                str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                       "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A' AND KD_CABANG = 1";
            }
            else
            {
                str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                        "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A' AND KD_CABANG = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();
            connection.Close();
            connection.Dispose();
            return exec;
        }
        private string InsertOtherServiceHeader(DataTransVB data, string KodeCabang, string CreatedBy)
        {
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            string ret = string.Empty;
            
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                OracleCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT_OTHERSERVICE_HDR";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;
                cmd.Parameters.Add("returnVal", OracleDbType.Varchar2, RETURN_VALUE_BUFFER_SIZE);
                cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                /*
                 * 
                P_BE_ID IN VARCHAR2, P_PROFIT_CENTER IN VARCHAR2, 
P_CUSTOMER_MDM IN VARCHAR2,
P_CUSTOMER_ID IN VARCHAR2, P_INSTALLATION_ADDRESS  IN VARCHAR2,P_CUSTOMER_SAP_AR IN VARCHAR2,P_GL_ACCOUNT IN VARCHAR2,
P_DOCUMENT_TYPE IN VARCHAR2, P_BILLING_TYPE IN VARCHAR2, P_TAX_CODE IN VARCHAR2,P_TOTAL IN NUMBER,P_BRANCH_ID IN VARCHAR2,P_UPDATE_BY IN VARCHAR2,P_POSTING_DATE
                 */
                
                OracleParameter pBE_ID = new OracleParameter("P_BE_ID", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.BE_ID,
                    Size = 100
                };
                cmd.Parameters.Add(pBE_ID);

                OracleParameter P_PROFIT_CENTER = new OracleParameter("P_PROFIT_CENTER", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.PROFIT_CENTER,
                    Size = 100
                };
                cmd.Parameters.Add(P_PROFIT_CENTER);

                OracleParameter P_CUSTOMER_MDM = new OracleParameter("P_CUSTOMER_MDM", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.COSTUMER_MDM,
                    Size = 100
                };
                cmd.Parameters.Add(P_CUSTOMER_MDM);

                OracleParameter P_CUSTOMER_ID = new OracleParameter("P_CUSTOMER_ID", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.COSTUMER_ID,
                    Size = 100
                };
                cmd.Parameters.Add(P_CUSTOMER_ID);

                OracleParameter P_INSTALLATION_ADDRESS = new OracleParameter("P_INSTALLATION_ADDRESS", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.INSTALLATION_ADDRESS,
                    Size = 100
                };
                cmd.Parameters.Add(P_INSTALLATION_ADDRESS);

                OracleParameter P_CUSTOMER_SAP_AR = new OracleParameter("P_CUSTOMER_SAP_AR", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.CUSTOMER_SAP_AR,
                    Size = 100
                };
                cmd.Parameters.Add(P_CUSTOMER_SAP_AR);

                OracleParameter P_GL_ACCOUNT = new OracleParameter("P_GL_ACCOUNT", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.SERVICES_GROUP,
                    Size = 100
                };
                cmd.Parameters.Add(P_GL_ACCOUNT);

                OracleParameter P_DOCUMENT_TYPE = new OracleParameter("P_DOCUMENT_TYPE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = "1M",
                    Size = 100
                };
                cmd.Parameters.Add(P_DOCUMENT_TYPE);

                OracleParameter P_BILLING_TYPE = new OracleParameter("P_BILLING_TYPE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = "ZM03",
                    Size = 100
                };
                cmd.Parameters.Add(P_BILLING_TYPE);

                OracleParameter P_TAX_CODE = new OracleParameter("P_TAX_CODE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.TAX_CODE,
                    Size = 100
                };
                cmd.Parameters.Add(P_TAX_CODE);

                OracleParameter P_PPH_CODE = new OracleParameter("P_PPH_CODE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.PPH_CODE,
                    Size = 100
                };
                cmd.Parameters.Add(P_PPH_CODE);

                OracleParameter P_TOTAL = new OracleParameter("P_TOTAL", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.TOTAL,
                    Size = 100
                };
                cmd.Parameters.Add(P_TOTAL);

                OracleParameter P_BRANCH_ID = new OracleParameter("P_BRANCH_ID", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = KodeCabang,
                    Size = 100
                };
                cmd.Parameters.Add(P_BRANCH_ID);

                OracleParameter P_UPDATE_BY = new OracleParameter("P_UPDATE_BY", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = CreatedBy,
                    Size = 100
                };
                cmd.Parameters.Add(P_UPDATE_BY);

                OracleParameter P_POSTING_DATE = new OracleParameter("P_POSTING_DATE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data.POSTING_DATE,
                    Size = 100
                };
                cmd.Parameters.Add(P_POSTING_DATE);

                try
                {
                    cmd.ExecuteNonQuery();
                    ret = cmd.Parameters["returnVal"].Value.ToString();
                }
                catch (Exception ex)
                {
                    ret = string.Empty;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                    cmd.Dispose();
                }
            }
            return ret;
        }
        public DataReturnTransNumber AddHeader(string name, DataTransVB data, string KodeCabang, string CreatedBy)
        {
            string qr = "";
            string id_trans = "";
            string id_trans_vb = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string penomoran = "";
            string _BE_ID = "1";
            string _param1 = "53"; 

            //try
            //{
            //    penomoran = DatabaseHelper.GetPenomoranData(_BE_ID, "TRANS_VARIOUS_BUSINESS", _param1, 6);
            //    //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_VB_TRANSACTION AS TRANS_CODE_VB FROM DUAL");
            //}
            //catch (Exception) { }

            var resultfull = new DataReturnTransNumber();
            int r = 0;
            int x = 0;
            int z = 0;
            try
            {
                penomoran = InsertOtherServiceHeader(data, KodeCabang, CreatedBy);
                //string sql = "INSERT INTO PROP_VB_TRANSACTION " +
                //    "(ID, POSTING_DATE, COSTUMER_MDM, BE_ID, PROFIT_CENTER, GL_ACCOUNT, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, DOCUMENT_TYPE, BILLING_TYPE, BRANCH_ID, CREATED_DATE, CREATED_BY, TAX_CODE) " +
                //    "VALUES (:z, to_date(:a,'DD/MM/YYYY'), :b, :c, :d, :e, :f, :g, :h, :j, :k, :l, :n, sysdate, :o, :p)";

                //r = connection.Execute(sql, new
                //{
                //    z = penomoran,
                //    a = data.POSTING_DATE,
                //    b = data.COSTUMER_MDM,
                //    c = data.BE_ID,
                //    d = data.PROFIT_CENTER,
                //    e = data.SERVICES_GROUP,
                //    f = data.COSTUMER_ID,
                //    g = data.INSTALLATION_ADDRESS,
                //    h = data.CUSTOMER_SAP_AR,
                //    // i = data.TAX_CODE,
                //    j = data.TOTAL,
                //    k = "1M",
                //    l = "ZM03",
                //    //m = id_trans_vb,
                //    n = KodeCabang,
                //    o = CreatedBy,
                //    p = data.TAX_CODE
                //});
            }
            catch (Exception) { }

                //Save Detail
                long TOTAL_AMOUNT = 0;
                if (data.details != null)
                {

                try
                {
                    string insertLog = "INSERT INTO JSON_LOG (MODULE_NAME, RAW_JSON_DATA, REF_DOC_NO, CREATED_DATE) VALUES (:MODULE_NAME, :RAW_JSON_DATA, :REF_DOC_NO, sysdate)";
                    z = connection.Execute(insertLog, new
                    {
                        MODULE_NAME = "OTHER_SERVICES",
                        RAW_JSON_DATA = data.RAW_JSON_DATA,
                        REF_DOC_NO = penomoran
                    });
                    z = 1;
                }
                catch (Exception)
                {
                    z = 0;
                    throw;
                }

                string sqlx = "INSERT INTO PROP_VB_TRANSACTION_DETAIL " +
                    "(START_DATE, END_DATE, SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT, SURCHARGE, AMOUNT, " +
                    "VB_ID, TAX_CODE, REMARK, BRANCH_ID, CREATED_BY, PPH_CODE, COA_PROD) " +
                    "VALUES (to_date(:a,'DD/MM/YYYY'), to_date(:b,'DD/MM/YYYY'), :c, :d, :e, :f, :g, :h, :i, :j, :k, :n, :o, :p, :q, :t, :u, :v)";

                    foreach (var i in data.details)
                    {
                        TOTAL_AMOUNT += Convert.ToInt64(i.AMOUNT);
                        //TOTAL_AMOUNT += Int32.Parse(i.AMOUNT);
                        try
                        {
                            x = connection.Execute(sqlx, new
                            {
                                a = i.START_DATE,
                                b = i.END_DATE,
                                c = i.SERVICE_CODE,
                                d = i.SERVICE_NAME,
                                e = i.CURRENCY,
                                f = i.PRICE,
                                g = i.MULTIPLY_FACTOR,
                                h = i.QUANTITY,
                                i = i.UNIT,
                                j = i.SURCHARGE,
                                k = i.AMOUNT,
                                n = penomoran,
                                o = i.TAX_CODE,
                                p = i.REMARK,
                                q = data.KD_CABANG,
                                t = data.CREATED_BY,
                                u = i.PPH_CODE,
                                v = i.COA_PROD
                            });
                            x = 1;
                            //ret.RESULT_STAT = true;
                        }
                        catch (Exception e)
                        {
                            x = 0;
                            throw;
                        }
                    }
                }

                string update_amount_header = "UPDATE PROP_VB_TRANSACTION SET TOTAL=:total, RATE=:rate WHERE ID=:idt AND BRANCH_ID=:c";
                try
                {
                    r = connection.Execute(update_amount_header, new
                    {
                        total = TOTAL_AMOUNT,
                        idt = penomoran,
                        c = KodeCabang,
                        rate = data.RATE,
                    });
                    r = 1;
                }
                catch (Exception)
                {
                    r = 0;
                    throw;
                }

            connection.Close();
            connection.Dispose();
            resultfull.ID_TRANS = penomoran;
                resultfull.RESULT_STAT = ((r > 0 && x > 0)) ? true : false;

            return resultfull;
        }

        public DataReturnTransNumber SaveHeaderEdit(string name, DataTransVB data, string KodeCabang, string CreatedBy)
        {
            string id_trans = "";
            string qr = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            var resultfull = new DataReturnTransNumber();
            int r = 0;
            try
            {
                string sql = "UPDATE PROP_VB_TRANSACTION SET POSTING_DATE=to_date(:a,'DD/MM/YYYY'), COSTUMER_MDM=:b, PROFIT_CENTER=:d, GL_ACCOUNT=:e, " +
                             "COSTUMER_ID=:f, INSTALLATION_ADDRESS=:g, CUSTOMER_SAP_AR=:h, TOTAL=:j, DOCUMENT_TYPE=:k, BILLING_TYPE=:l, " +
                             "BRANCH_ID=:n, UPDATED_DATE=sysdate, UPDATED_BY=:o, TAX_CODE=:p, PPH_CODE=:r, RATE=:rate WHERE ID=:q";

                r = connection.Execute(sql, new
                {
                    a = data.POSTING_DATE,
                    b = data.COSTUMER_MDM,
                    //c = data.BE_ID,
                    d = data.PROFIT_CENTER,
                    e = data.SERVICES_GROUP,
                    f = data.COSTUMER_ID,
                    g = data.INSTALLATION_ADDRESS,
                    h = data.CUSTOMER_SAP_AR,
                    j = data.TOTAL,
                    k = "1M",
                    l = "ZM03",
                    n = KodeCabang,
                    o = CreatedBy,
                    p = data.TAX_CODE,
                    q = data.ID_TRANS,
                    r = data.PPH_CODE,
                    rate = data.RATE
                });
            }
            catch (Exception) { }

            try
            {
                qr = "DELETE FROM PROP_VB_TRANSACTION_DETAIL WHERE VB_ID=:a ";
                id_trans = connection.ExecuteScalar<string>(qr, new { a = data.ID_TRANS });
            }
            catch (Exception) { }

            //Save Detail
            long TOTAL_AMOUNT = 0;
            if (data.details != null)
            {
                string sqlx = "INSERT INTO PROP_VB_TRANSACTION_DETAIL " +
                "(START_DATE, END_DATE, SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT, SURCHARGE, AMOUNT, " +
                "VB_ID, TAX_CODE, REMARK, UPDATED_BY, BRANCH_ID_EDIT, PPH_CODE, COA_PROD) " +
                "VALUES (to_date(:a,'DD/MM/YYYY'), to_date(:b,'DD/MM/YYYY'), :c, :d, :e, :f, :g, :h, :i, :j, :k, :n, :o, :p, :q, :s, :t, :u)";

                int x = 0;
                foreach (var i in data.details)
                {
                    TOTAL_AMOUNT += Convert.ToInt64(i.AMOUNT);
                    //TOTAL_AMOUNT += Int32.Parse(i.AMOUNT);
                    try
                    {
                        x = connection.Execute(sqlx, new
                        {
                            a = i.START_DATE,
                            b = i.END_DATE,
                            c = i.SERVICE_CODE,
                            d = i.SERVICE_NAME,
                            e = i.CURRENCY,
                            f = i.PRICE,
                            g = i.MULTIPLY_FACTOR,
                            h = i.QUANTITY,
                            i = i.UNIT,
                            j = i.SURCHARGE,
                            k = i.AMOUNT,
                            n = data.ID_TRANS,
                            o = i.TAX_CODE,
                            p = i.REMARK,
                            q = data.UPDATED_BY,
                            s = data.KD_CABANG_EDIT,
                            t = i.PPH_CODE,
                            u = i.COA_PROD
                        });
                        x = 1;
                    }
                    catch (Exception)
                    {
                        x = 0;
                        throw;
                    }
                }
            }

            string update_amount_header = "UPDATE PROP_VB_TRANSACTION SET TOTAL=:total WHERE ID=:idt";
            int z = 0;
            try
            {
                z = connection.Execute(update_amount_header, new
                {
                    total = TOTAL_AMOUNT,
                    idt = data.ID_TRANS
                });
                z = 1;
            }
            catch (Exception)
            {
                z = 0;
                throw;
            }

            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        public IEnumerable<AUP_TRANSVB_WEBAPPS> GetDataDetailRentalEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listData = null;

            try
            {
                string sql = @"SELECT d.ID, d.TAX_CODE, d.PPH_CODE, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, to_char(START_DATE, 'DD/MM/YYYY') AS S_DATE, to_char(END_DATE,'DD/MM/YYYY') AS E_DATE, 
                             SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT || ' | ' || (SELECT DISTINCT ref_desc 
                             FROM prop_parameter_ref_d WHERE UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, SURCHARGE, AMOUNT, REMARK, COA_PROD, VB_ID, CURRENCY ||'  '|| PRICE AS CM_PRICE, AMOUNT/NVL(v.RATE,1) TOTAL_USD 
                             FROM PROP_VB_TRANSACTION_DETAIL d, PROP_VB_TRANSACTION v WHERE d.VB_ID=v.ID AND VB_ID = :c";

                listData = connection.Query<AUP_TRANSVB_WEBAPPS>(sql, new
                {
                    c = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public DataTablesTransVB GetDataVBTransListnew(int draw, int start, int length, string search, string KodeCabang, string UserRole, string select_cabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string selectedCabang = (select_cabang != null && select_cabang != "") ? select_cabang : KodeCabang;
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                string wheresearch = (search.Length >= 2 ? " AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||'" + search.ToUpper() + "'|| '%') " : "");

                QueryGlobal query = new QueryGlobal();
                string v_be = query.Query_BE();

                string sql = @"SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, RATE, TOTAL/NVL(RATE,1) TOTAL_USD , 
                            INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN, BRANCH_ID
                            FROM V_TRANSVB_NEW WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch + " ORDER BY ID DESC";
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                {
                    a = start,
                    b = end,
                    //c = search.ToUpper(),
                    d = selectedCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = selectedCabang });

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception e) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        }
        public IEnumerable<DDRefD> GetDataDropDownCoaProduksi(string param)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                //param = param + "%";
                param = param + '%';
                string sql = "SELECT * FROM VW_GET_COA_PROD where VAL1 like :a ";

                listData = connection.Query<DDRefD>(sql,new { a = param }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DDRefD> GetDataDropDownunit(string param)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                //param = param + "%";
                param = param + '%';
                string sql = "select DISTINCT SATUAN_MDM from masterdata.silapor_satuan where coa like " + "'" + param + "'";

                listData = connection.Query<DDRefD>(sql, new { a = param }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
    }
}