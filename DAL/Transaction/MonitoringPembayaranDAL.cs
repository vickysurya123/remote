﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportOtherService;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models.MasterBusinessEntity;
using Remote.DAL;
using Remote.Models.DynamicDatatable;
using Remote.Helpers;

namespace Remote.Controllers
{
    internal class MonitoringPembayaranDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                listData = connection.Query<DDBusinessEntity>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                string sql = "SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";

                listData = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang }).ToList();
            }
            catch (Exception) { }

            connection.Close();

            connection.Dispose();
            return listData;
        }

        public IEnumerable<DDHarbourClass> GetDataHarbourClass()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDHarbourClass> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT HARBOUR_CLASS HB_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS FROM BUSINESS_ENTITY";

                listData = connection.Query<DDHarbourClass>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        //public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        //{
        //    string sql = "";

        //    if (KodeCabang == "0")
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
        //    }
        //    else
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
        //    }

        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
        //    connection.Close();
        //    return listDetil;
        //}
        public IEnumerable<VW_BUSINESS_ENTITY> getBEID(string xPARAM)
        {
            VW_BUSINESS_ENTITY result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_BUSINESS_ENTITY> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, BE_ADDRESS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_BUSINESS_ENTITY>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();

            return listData;
        }
        public IEnumerable<AUP_BE_WEBAPPS> getHARBOURClASS(string xPARAM)
        {
            AUP_BE_WEBAPPS result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_BE_WEBAPPS> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<AUP_BE_WEBAPPS>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();

            return listData;
        }
        public IDataTable ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_BE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT B.BE_ID || ' - ' || B.BE_NAME AS BE, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, b.BE_ADDRESS, " +
                                     "(SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID, b.POSTAL_CODE, " +
                                     "d.amount, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, phone_1 || '  ' || PHONE_2 as phone, fax_1 || '   ' || FAX_2 as fax, c.EMAIL " +
                                     "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c, BUSINESS_ENTITY_DETAIL d WHERE c.BE_ID = b.BE_ID(+) and c.id = d.id ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                { }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_REPORT_BE> ShowAllTwo(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT B.BE_ID || ' - ' || B.BE_NAME AS BE, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, b.BE_ADDRESS, " +
                                     "(SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID, b.POSTAL_CODE, " +
                                     "d.amount, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, phone_1 || '  ' || PHONE_2 as phone, fax_1 || '   ' || FAX_2 as fax, c.EMAIL " +
                                     "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c, BUSINESS_ENTITY_DETAIL d WHERE c.BE_ID = b.BE_ID(+) and c.id = d.id ";

                listData = connection.Query<AUP_REPORT_BE>(sql, new
                {
                    c = KodeCabang
                });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();
            return listData;

        }

        //--------------------------------- DEPRECATED ----------------------------------------
        public IDataTable GetDataFilter(int draw, int start, int length, string be, string tipe, string jenis_tagihan, string search, string KodeCabang)
        {
            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_jenis_tagihan = string.IsNullOrEmpty(jenis_tagihan) ? "x" : jenis_tagihan;
            string x_tipe = string.IsNullOrEmpty(tipe) ? "x" : tipe;

            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DashboardDetailPiutang> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string sql = "";
            string where = "";
            string where1 = "";
            where += x_be == "x" ? " " : " and BRANCH_ID = " + x_be;
            //where += x_tipe == "x" ? " " : " and STATUS_LUNAS = '" + x_tipe + "'";
            //where += x_jenis_tagihan == "x" ? " " : " and jenis_tagihan = '" + x_jenis_tagihan + "'";


            if (x_be.Length >= 0 && x_tipe == "x")
            {
                where += " ";
            }
            else
            {
                if (x_be.Length >= 0 && x_tipe == "BELUM LUNAS")
                {
                    where += " and a.tgl_lunas is null and a.tgl_batal is null ";
                }
                else if (x_be.Length >= 0 && x_tipe == "LUNAS")
                {
                    where += " and a.tgl_lunas is not null ";
                }
                else
                {
                    where += " and a.tgl_batal is not null ";
                }
                //if (x_be.Length >= 0 && x_tipe == "BELUM LUNAS")
                //{
                //    where1 += " when a.tgl_lunas is null and a.tgl_batal is null then 'Belum Lunas' ";
                //}
                //else if (x_be.Length >= 0 && x_tipe == "LUNAS")
                //{
                //    where1 += " when a.TGL_LUNAS is not null then 'Lunas' ";
                //}
                //else
                //{
                //    where1 += " when a.tgl_batal is not null then 'Batal' ";
                //}
            }
            if (x_be.Length >=0 && x_jenis_tagihan == "PROPERTI")
            {
                //sql = @"select b.BRANCH_ID, 'Property' jenis_tagihan, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE , 
                //                            c.MPLG_NAMA CUSTOMER_NAME,a.PSTNG_DATE,a.TGL_BATAL,a.TGL_LUNAS,a.DOC_NUMBER TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                            @"end as STATUS_LUNAS from ITGR_HEADER a 
                //                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                //                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE  and c.KD_CABANG = b.BRANCH_ID
                //                            where  doc_number is not null and doc_number != '/0000' and a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + where;

                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_PROPERTI WHERE " + where;
                sql = @"select b.BRANCH_ID, 'Property' jenis_tagihan, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE , 
                                            c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS,
                                            TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL, a.DOC_NUMBER, TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                            ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                            'Nota Belum Lunas'
                                            when a.tgl_lunas is not null then
                                            'Nota Lunas'
                                            else 
                                            'Nota Batal'
                                            end as STATUS_LUNAS
                                            from ITGR_HEADER a 
                                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE  and c.KD_CABANG = b.BRANCH_ID
                                            where  doc_number is not null and doc_number != '/0000' and a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + where;

                if (search.Length > 2)
                {
                    sql += @" and ((b.BILLING_NO LIKE '%'||" + search + "||'%') or (b.BILLING_CONTRACT_NO LIKE '%'||" + search + "||'%') or (b.CUSTOMER_CODE LIKE '%'||" + search + "||'%') or (c.MPLG_NAMA LIKE '%'||" + search + "||'%'))";
                }
            }
            else if (x_be.Length >= 0 && x_jenis_tagihan == "AIR")
            {
                //sql = @"select b.BRANCH_ID, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as jenis_tagihan, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME,a.PSTNG_DATE, TO_CHAR (b.BIAYA_BEBAN, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                        @" end as STATUS_LUNAS from ITGR_HEADER a 
                //                        join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                //                        where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + where;
                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_AIRLISTRIK WHERE " + where;
                sql = @"select b.BRANCH_ID, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as jenis_tagihan, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME,TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE,
                        TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER, TO_CHAR (b.BIAYA_BEBAN, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                                        where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + where;

                if (search.Length > 2)
                {
                    sql += @" and ((b.ID LIKE '%'||" + search + "||'%') or (b.INSTALLATION_CODE LIKE '%'||" + search + "||'%') or (b.CUSTOMER LIKE '%'||" + search + "||'%') or (c.CUSTOMER_NAME LIKE '%'||" + search + "||'%'))";
                }
            }
            else
            {
                //sql = @"select b.BRANCH_ID, 'Other Service' jenis_tagihan, b.ID BILLING_NO,
                //                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
                //                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, a.CREATED_DATE,  TO_CHAR (b.TOTAL, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                        @" end as STATUS_LUNAS from ITGR_HEADER a 
                //                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                //                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                //                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                //                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + where;
                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_OTHERSERVICE WHERE " + where;
                sql = @"select b.BRANCH_ID, 'Other Service' jenis_tagihan, b.ID BILLING_NO,
                                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
                                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER,  TO_CHAR (b.TOTAL, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + where;
                if (search.Length > 2)
                {
                    sql += @" and ((b.ID LIKE '%'||" + search + "||'%') or (b.COSTUMER_ID LIKE '%'||" + search + "||'%') or (c.MPLG_NAMA LIKE '%'||" + search + "||'%'))";
                }
            }

            try
            {
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<DashboardDetailPiutang>(fullSql, new
                {
                    a = start,
                    b = end,
                    x_be = x_be,
                    x_jenis_tagihan = x_jenis_tagihan,
                    x_tipe = x_tipe
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    x_be = x_be,
                    x_jenis_tagihan = x_jenis_tagihan,
                    x_tipe = x_tipe
                });
            }
            catch (Exception e)
            { }
            //else
            //{
            //    if (x_be.Length >= 0 && x_jenis_tagihan.Length >= 0)
            //    {
            //        if (search.Length == 0)
            //        {
            //            try
            //            {
            //                string sql = "SELECT BE, BE_CITY, BE_ID, BE_PROVINCE, BE_ADDRESS, " +
            //                            "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
            //                            "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
            //                            "FROM V_REPORT_BE " +
            //                            "WHERE BRANCH_ID = :d AND BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) and branch_id=:d ";

            //                fullSql = fullSql.Replace("sql", sql);

            //                listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new
            //                {
            //                    a = start,
            //                    b = end,
            //                    x_be = x_be,
            //                    x_jenis_tagihan = x_jenis_tagihan,
            //                    d = KodeCabang
            //                });

            //                fullSqlCount = fullSqlCount.Replace("sql", sql);

            //                count = connection.ExecuteScalar<int>(fullSqlCount, new
            //                {
            //                    a = start,
            //                    b = end,
            //                    x_be = x_be,
            //                    x_jenis_tagihan = x_jenis_tagihan,
            //                    d = KodeCabang
            //                });
            //            }
            //            catch (Exception e)
            //            { }
            //        }
            //    }
            //    else
            //    {

            //    }

            //}

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_REPORT_BE> GetDataFilterTwo(string be, string harbour_class, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_harbour_class = string.IsNullOrEmpty(harbour_class) ? "x" : harbour_class;
            if (KodeCabang == "0")
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE, BE_CITY, BE_PROVINCE, BE_ADDRESS, " +
                                     "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                     "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                     "FROM V_REPORT_BE WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) ";


                        listData = connection.Query<AUP_REPORT_BE>(sql, new
                        {
                            x_be = x_be,
                            x_harbour_class = x_harbour_class
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {

                }
            }
            else
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE, BE_CITY, BE_PROVINCE, BE_ADDRESS, " +
                                     "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                     "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                     "FROM V_REPORT_BE WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) AND branch_id=:d ";


                        listData = connection.Query<AUP_REPORT_BE>(sql, new
                        {
                            x_be = x_be,
                            x_harbour_class = x_harbour_class,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {

                }
            }

            connection.Close();
            connection.Dispose();
            return listData;

        }
        public List<DashboardDetailPiutang> GetDetailExcel(string BUSINESS_ENTITY, string JENIS_TAGIHAN, string NOTA_TYPE, string KodeCabang)
        {
            List<DashboardDetailPiutang> listData = null;
            string where = "";
            string sql = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE("b");

            try
            {
                where += BUSINESS_ENTITY == "" || BUSINESS_ENTITY == null ? " " : " and b.BRANCH_ID = " + BUSINESS_ENTITY;
                if (NOTA_TYPE == "")
                {
                    where += " ";
                }
                else
                {
                    if (NOTA_TYPE == "BELUM LUNAS")
                    {
                        where += " and a.tgl_lunas is null and a.tgl_batal is null ";
                    }
                    else if (NOTA_TYPE == "LUNAS")
                    {
                        where += " and a.tgl_lunas is not null ";
                    }
                    else
                    {
                        where += " and a.tgl_batal is not null ";
                    }
                }
                if (JENIS_TAGIHAN == "PROPERTI")
                {
                    sql = @"select b.BRANCH_ID, 'Property' jenis_tagihan, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE CUSTOMER_CODE , 
                                            c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS,
                                            TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,COALESCE(a.DOC_NUMBER, b.SAP_DOC_NO) DOC_NUMBER, TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                            ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                            'Nota Belum Lunas'
                                            when a.tgl_lunas is not null then
                                            'Nota Lunas'
                                            else 
                                            'Nota Batal'
                                            end as STATUS_LUNAS
                                            from ITGR_HEADER a 
                                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE  and c.KD_CABANG = b.BRANCH_ID
                                            where  a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + where + v_be;
                    
                }
                else if (JENIS_TAGIHAN == "AIR")
                {
                    sql = @"select b.BRANCH_ID, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as jenis_tagihan, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME CUSTOMER_NAME,TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE,
                        TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER, TO_CHAR (b.BIAYA_BEBAN, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                                        where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + where + v_be;
                    
                }
                else
                {
                    sql = @"select b.BRANCH_ID, 'Other Service' jenis_tagihan, b.ID BILLING_NO,
                                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
                                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER,  TO_CHAR (b.TOTAL, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + where + v_be;
                    
                }
                listData = connection.Query<DashboardDetailPiutang>(sql, new
                {
                    d = KodeCabang,
                }).ToList();
            }
            catch (Exception ex)
            {
                listData = null;
            }


            connection.Close();
            connection.Dispose();
            return listData;
        }

        //--------------------------------- NEW GET DATA FILTER -------------------------------
        public IDataTable GetDataFilternew(int draw, int start, int length, string be, string tipe, string jenis_tagihan, string search, string KodeCabang)
        {
            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_jenis_tagihan = string.IsNullOrEmpty(jenis_tagihan) ? "x" : jenis_tagihan;
            string x_tipe = string.IsNullOrEmpty(tipe) ? "x" : tipe;

            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DashboardDetailPiutang> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string sql = "";
            string where = "";
            string where1 = "";
            where += x_be == "x" ? " " : " and BRANCH_ID = " + x_be;
            //where += x_tipe == "x" ? " " : " and STATUS_LUNAS = '" + x_tipe + "'";
            //where += x_jenis_tagihan == "x" ? " " : " and jenis_tagihan = '" + x_jenis_tagihan + "'";
            //string wheresearch = (search.Length >= 2 ? " and ((b.BILLING_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (CODE LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (CUSTOMER_CODE LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (CUSTOMER_NAME LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
            //string wheresearch = (search.Length >= 2 ? " and ((b.BILLING_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (CUSTOMER_NAME LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");



            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            if (x_be.Length >= 0 && x_tipe == "x")
            {
                where += " ";
            }
            else
            {
                if (x_be.Length >= 0 && x_tipe == "BELUM LUNAS")
                {
                    where += " and a.tgl_lunas is null and a.tgl_batal is null ";
                }
                else if (x_be.Length >= 0 && x_tipe == "LUNAS")
                {
                    where += " and a.tgl_lunas is not null ";
                }
                else
                {
                    where += " and a.tgl_batal is not null and tgl_lunas is null ";
                }
                //if (x_be.Length >= 0 && x_tipe == "BELUM LUNAS")
                //{
                //    where1 += " when a.tgl_lunas is null and a.tgl_batal is null then 'Belum Lunas' ";
                //}
                //else if (x_be.Length >= 0 && x_tipe == "LUNAS")
                //{
                //    where1 += " when a.TGL_LUNAS is not null then 'Lunas' ";
                //}
                //else
                //{
                //    where1 += " when a.tgl_batal is not null then 'Batal' ";
                //}
            }
            if (x_be.Length >= 0 && x_jenis_tagihan == "PROPERTI")
            {
                //sql = @"select b.BRANCH_ID, 'Property' jenis_tagihan, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE , 
                //                            c.MPLG_NAMA CUSTOMER_NAME,a.PSTNG_DATE,a.TGL_BATAL,a.TGL_LUNAS,a.DOC_NUMBER TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                            @"end as STATUS_LUNAS from ITGR_HEADER a 
                //                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                //                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE  and c.KD_CABANG = b.BRANCH_ID
                //                            where  doc_number is not null and doc_number != '/0000' and a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + where;

                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_PROPERTI WHERE " + where;
                string wheresearchpro = (search.Length >= 2 ? " and ((b.BILLING_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (b.BILLING_CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (b.CUSTOMER_CODE LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (c.MPLG_NAMA LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");

                sql = @"select b.BRANCH_ID, 'Property' jenis_tagihan, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE CUSTOMER_CODE , 
                                            c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS,
                                            TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,COALESCE(a.DOC_NUMBER, b.SAP_DOC_NO) DOC_NUMBER, TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                            ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                            'Nota Belum Lunas'
                                            when a.tgl_lunas is not null then
                                            'Nota Lunas'
                                            else 
                                            'Nota Batal'
                                            end as STATUS_LUNAS
                                            from ITGR_HEADER a 
                                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE  and c.KD_CABANG = b.BRANCH_ID
                                            where  a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + where + v_be + wheresearchpro; // doc_number is not null and doc_number != '/0000' and

                //if (search.Length > 2)
                //{
                //    sql += @" and ((b.BILLING_NO LIKE '%'||" + search + "||'%') or (b.BILLING_CONTRACT_NO LIKE '%'||" + search + "||'%') or (b.CUSTOMER_CODE LIKE '%'||" + search + "||'%') or (c.MPLG_NAMA LIKE '%'||" + search + "||'%'))";
                //}
            }
            else if (x_be.Length >= 0 && x_jenis_tagihan == "AIR")
            {
                //sql = @"select b.BRANCH_ID, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as jenis_tagihan, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME,a.PSTNG_DATE, TO_CHAR (b.BIAYA_BEBAN, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                        @" end as STATUS_LUNAS from ITGR_HEADER a 
                //                        join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                //                        where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + where;
                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_AIRLISTRIK WHERE " + where;
                string wheresearchair = (search.Length >= 2 ? " and ((b.ID LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (b.CUSTOMER LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (b.CUSTOMER_NAME LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
                
                sql = @"select b.BRANCH_ID, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as jenis_tagihan, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME CUSTOMER_NAME,TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE,
                        TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER, TO_CHAR (b.BIAYA_BEBAN, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                                        where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + where + v_be + wheresearchair;

                //if (search.Length > 2)
                //{
                //    sql += @" and ((b.ID LIKE '%'||" + search + "||'%') or (b.INSTALLATION_CODE LIKE '%'||" + search + "||'%') or (b.CUSTOMER LIKE '%'||" + search + "||'%') or (c.CUSTOMER_NAME LIKE '%'||" + search + "||'%'))";
                //}
            }
            else
            {
                //sql = @"select b.BRANCH_ID, 'Other Service' jenis_tagihan, b.ID BILLING_NO,
                //                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
                //                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, a.CREATED_DATE,  TO_CHAR (b.TOTAL, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT, case "+where1+
                //                        @" end as STATUS_LUNAS from ITGR_HEADER a 
                //                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                //                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                //                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                //                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + where;
                //sql = @"select BRANCH_ID, JENIS_TAGIHAN, BILLING_NO, CODE, CUSTOMER_CODE, CUSTOMER_NAME,
                //        PSTNG_DATE, TGL_BATAL, TGL_LUNAS, DOC_NUMBER, AMOUNT, STATUS_LUNAS from V_MON_OTHERSERVICE WHERE " + where;
                string wheresearch = (search.Length >= 2 ? " and ((b.ID LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (b.COSTUMER_ID LIKE '%'|| '" + search.ToUpper() + "' ||'%') or (c.MPLG_NAMA LIKE '%'|| '" + search.ToUpper() + "' ||'%')) " : "");
                
                sql = @"select b.BRANCH_ID, 'Other Service' jenis_tagihan, b.ID BILLING_NO,
                                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
                                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, TO_CHAR(TO_DATE(a.PSTNG_DATE,'yyyy-mm-dd'),'DD-MM-YYYY') as PSTNG_DATE, TO_CHAR(a.TGL_LUNAS,'DD-MM-YYYY') as TGL_LUNAS, TO_CHAR(a.TGL_BATAL,'DD-MM-YYYY') as TGL_BATAL,a.DOC_NUMBER,  TO_CHAR (b.TOTAL, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                                        ,case when a.tgl_lunas is null and a.tgl_batal is null then
                                        'Nota Belum Lunas'
                                        when a.tgl_lunas is not null then
                                        'Nota Lunas'
                                        else 
                                        'Nota Batal'
                                        end as STATUS_LUNAS
                                        from ITGR_HEADER a 
                                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + where + v_be + wheresearch;
                //if (search.Length > 2)
                //{
                //    sql += @" and ((b.ID LIKE '%'||" + search + "||'%') or (b.COSTUMER_ID LIKE '%'||" + search + "||'%') or (c.MPLG_NAMA LIKE '%'||" + search + "||'%'))";
                //}
            }

            try
            {
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<DashboardDetailPiutang>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang,
                    x_be = x_be,
                    x_jenis_tagihan = x_jenis_tagihan,
                    x_tipe = x_tipe
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang,
                    x_be = x_be,
                    x_jenis_tagihan = x_jenis_tagihan,
                    x_tipe = x_tipe
                });
            }
            catch (Exception e)
            { }
            
            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}