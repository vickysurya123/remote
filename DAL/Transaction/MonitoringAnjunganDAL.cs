﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;

namespace Remote.DAL
{
    public class MonitoringAnjunganDAL
    {

        //------------------------------- DEPRECATED ---------------------
        public IDataTable GetData(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<CONTRACT_ANJUNGAN_HEADER> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                                        CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                                        TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID
                                        FROM V_CONTRACT_ANJUNGAN_HEADER ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<CONTRACT_ANJUNGAN_HEADER>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                                        CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                                        TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID
                                        FROM V_CONTRACT_ANJUNGAN_HEADER WHERE " +
                                "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(PROFIT_CENTER) LIKE '%' ||:c|| '%') OR (UPPER(BE_ID) LIKE '%' ||:c|| '%') ";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<CONTRACT_ANJUNGAN_HEADER>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                                        CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                                        TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID
                                        FROM V_CONTRACT_ANJUNGAN_HEADER WHERE 
                                        BRANCH_ID=:c ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<CONTRACT_ANJUNGAN_HEADER>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                                        CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                                        TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID
                                        FROM V_CONTRACT_ANJUNGAN_HEADER WHERE BRANCH_ID =:d AND " +
                                      "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(PROFIT_CENTER) LIKE '%' ||:c|| '%') ";
                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<CONTRACT_ANJUNGAN_HEADER>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        
        //------------------------------- DATA TABLE HEADER DATA ---------------------
        public IDataTable GetDataDetail(int draw, int start, int length, string search, int id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<CONTRACT_ANJUNGAN_DETAIL> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            
            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT ID, CONTRACT_ANJUNGAN_ID, 
                                    OBJECT_ID, OBJECT_TYPE, OBJECT_TYPE_ID, RO_NAME, RO_NUMBER, 
                                    LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY
                                    FROM V_CONTRACT_ANJUNGAN_DETAIL WHERE 
                                    CONTRACT_ANJUNGAN_ID=:c ";

                    fullSql = fullSql.Replace("sql", sql);

                    listData = connection.Query<CONTRACT_ANJUNGAN_DETAIL>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                //search filter data
                // DO NOTHING
            }
            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public CONTRACT_ANJUNGAN_HEADER GetHeader(int id)
        {
            CONTRACT_ANJUNGAN_HEADER result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR') CONTRACT_END_DATE, 
                            TO_CHAR(CONTRACT_START_DATE, 'DD/MM/RRRR') CONTRACT_START_DATE, 
                            CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                            CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                            TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID
                            FROM V_CONTRACT_ANJUNGAN_HEADER WHERE ID=:c ";

                result = connection.Query<CONTRACT_ANJUNGAN_HEADER>(sql, new { c = id }).DefaultIfEmpty(null).FirstOrDefault();
            } catch (Exception e) {
                string aaa = e.ToString();
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public int SetStatusByContractOffer(string status, string coNumber, string note = "")
        {
            int result = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                if (status != "REJECTED") { 
                    string sql = @"UPDATE PROP_CONTRACT_ANJUNGAN SET STATUS = :b WHERE CONTRACT_OFFER_NO = :a ";

                    result = connection.Execute(sql, new
                    {
                        a = coNumber,
                        b = status
                    });
                } else
                {

                    string sqlReject = @"UPDATE PROP_CONTRACT_ANJUNGAN SET STATUS = :b, REJECT_NOTE = :c WHERE CONTRACT_OFFER_NO = :a ";

                    result = connection.Execute(sqlReject, new
                    {
                        a = coNumber,
                        b = status,
                        c = note
                    });
                }
            }
            catch (Exception e) {
                string aaa = e.ToString();
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public int SetStatusContractOffer(string status, string anjunganId, string coNumber)
        {
            int result = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                string sql = @"UPDATE PROP_CONTRACT_ANJUNGAN SET STATUS = :b, CONTRACT_OFFER_NO = :c WHERE ID = :a ";

                result = connection.Execute(sql, new
                {
                    a = anjunganId,
                    b = status,
                    c = coNumber
                });

                sql = @"UPDATE PROP_CONTRACT_OFFER SET ANJUNGAN_ID = :a WHERE CONTRACT_OFFER_NO = :c ";

                result = connection.Execute(sql, new
                {
                    a = anjunganId,
                    c = coNumber
                });
            }
            catch (Exception)
            {
                result = 0;
                throw;
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public int SetStatusContract(string status, string contractNumber)
        {
            int result = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                // TODO HERE
                string sql = @"UPDATE PROP_CONTRACT_ANJUNGAN up
                                SET up.STATUS = :a, up.CONTRACT_NO = :b 
                                where exists (
                                select 1
                                from PROP_CONTRACT_OFFER b 
                                inner join PROP_CONTRACT a on b.CONTRACT_OFFER_NO = a.CONTRACT_OFFER_NO  
                                where b.CONTRACT_OFFER_NO = up.CONTRACT_OFFER_NO)";

                result = connection.Execute(sql, new
                {
                    a = status,
                    b = contractNumber
                });
            }
            catch (Exception e) {
                string aaa = e.ToString();
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- NEW DATA TABLE HEADER DATA ---------------------
        public IDataTable GetDatanew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<CONTRACT_ANJUNGAN_HEADER> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND (CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CONTRACT_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(PROFIT_CENTER) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(BE_ID) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_NO, 
                                        CONTRACT_TYPE, CONTRACT_USAGE, CONTRACT_NAME, BE_ID, PROFIT_CENTER,
                                        TERM_IN_MONTHS, OLD_CONTRACT, STATUS, ID, KODE_BAYAR, REJECT_NOTE
                                        FROM V_CONTRACT_ANJUNGAN_HEADER WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<CONTRACT_ANJUNGAN_HEADER>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                            
            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public int SetReject(int id, string note)
        {
            var res = 0;

            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                // TODO HERE
                string sql = @"UPDATE PROP_CONTRACT_ANJUNGAN SET STATUS = 'REJECTED', REJECT_NOTE = :c WHERE ID = :a ";

                res = connection.Execute(sql, new {
                    a = id,
                    c = note
                });
            }
            catch (Exception)
            {

            }

            connection.Close();
            connection.Dispose();
            return res;
        }
    }
}