﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransContract;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransContractDAL
    {

        //-------------------- DEPRECATED --------------------
        public DataTablesContract GetDataTransContract(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS, " +
                                 "CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS, BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAMES, BE_NAME, TERMINAL_NAME, RO_CERTIFICATE_NUMBER, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS " +
                                 "FROM V_TRANS_SPECIAL_CONT_HEADER WHERE DIRECT_CONTRACT = '0' ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS, " +
                                 "CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS, BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAMES, BE_NAME, TERMINAL_NAME, RO_CERTIFICATE_NUMBER, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS " +
                                 "FROM V_TRANS_SPECIAL_CONT_HEADER WHERE DIRECT_CONTRACT = '0' AND ((CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BUSINESS_PARTNER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_START_DATE) LIKE '%'||:c||'%') OR (UPPER(CONTRACT_END_DATE) LIKE '%'||:c||'%'))";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS, " +
                                 "CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS, BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAMES, BE_NAME, TERMINAL_NAME, RO_CERTIFICATE_NUMBER, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS " +
                                 "FROM V_TRANS_SPECIAL_CONT_HEADER WHERE BRANCH_ID=:c AND DIRECT_CONTRACT = '0' ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS, " +
                                 "CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS, BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAMES, BE_NAME, TERMINAL_NAME, RO_CERTIFICATE_NUMBER, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS " +
                                 "FROM V_TRANS_SPECIAL_CONT_HEADER WHERE DIRECT_CONTRACT = '0' AND ((CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BUSINESS_PARTNER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_START_DATE) LIKE '%'||:c||'%') OR (UPPER(CONTRACT_END_DATE) LIKE '%'||:c||'%')) " +
                                 "AND BRANCH_ID = :d";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------ DEPRECATED
        public DataTablesContract GetDataTransContractOffer(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TERM_IN_MONTHS, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "CUSTOMER_AR, CUSTOMER_ID, CUSTOMER_NAME, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, CONTRACT_NUMBER, ID_INC, RENTAL_REQUEST_NAME, CONTRACT_OFFER_TYPE_F, OLD_CONTRACT, " +
                                 "BUSINESS_ENTITY_F, PROFIT_CENTER_F, CONTRACT_USAGE_F FROM V_HEADER_LIST_CONTRACT WHERE CONTRACT_NUMBER IS NULL AND COMPLETED_STATUS = '1' ORDER BY ID_INC DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TERM_IN_MONTHS, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "CUSTOMER_AR, CUSTOMER_ID, CUSTOMER_NAME, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, CONTRACT_NUMBER, ID_INC, RENTAL_REQUEST_NAME, CONTRACT_OFFER_TYPE_F, " +
                                 "BUSINESS_ENTITY_F, PROFIT_CENTER_F, CONTRACT_USAGE_F FROM V_HEADER_LIST_CONTRACT WHERE CONTRACT_NUMBER IS NULL AND COMPLETED_STATUS = '1' " +
                                 "AND (CONTRACT_OFFER_NO || UPPER(CONTRACT_OFFER_NAME) || CONTRACT_START_DATE || CONTRACT_END_DATE || UPPER(CONTRACT_USAGE_F) LIKE '%'||:c||'%' ) " +
                                 "ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TERM_IN_MONTHS, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "CUSTOMER_AR, CUSTOMER_ID, CUSTOMER_NAME, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, CONTRACT_NUMBER, ID_INC, RENTAL_REQUEST_NAME, CONTRACT_OFFER_TYPE_F, " +
                                 "BUSINESS_ENTITY_F, PROFIT_CENTER_F, CONTRACT_USAGE_F FROM V_HEADER_LIST_CONTRACT WHERE CONTRACT_NUMBER IS NULL AND COMPLETED_STATUS = '1' AND BRANCH_ID = :d ORDER BY ID_INC DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TERM_IN_MONTHS, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "CUSTOMER_AR, CUSTOMER_ID, CUSTOMER_NAME, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, CONTRACT_NUMBER, ID_INC, RENTAL_REQUEST_NAME, CONTRACT_OFFER_TYPE_F, " +
                                 "BUSINESS_ENTITY_F, PROFIT_CENTER_F, CONTRACT_USAGE_F FROM V_HEADER_LIST_CONTRACT WHERE CONTRACT_NUMBER IS NULL AND BRANCH_ID = :d  AND COMPLETED_STATUS = '1' " +
                                 "AND (CONTRACT_OFFER_NO || UPPER(CONTRACT_OFFER_NAME) || CONTRACT_START_DATE || CONTRACT_END_DATE || UPPER(CONTRACT_USAGE_F) LIKE '%'||:c||'%' ) " +
                                 "ORDER BY ID_INC DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------- DATA DETAIL OBJECT --------------------------
        public DataTablesContract GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE,'DD.MM.YYYY') START_DATE, to_char(END_DATE,'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_TRANS_SPECIAL_CONT_OBJECT " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> GetCondition(string CONTRACT_NO)
        {
            string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO,'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, to_char(START_DUE_DATE,'DD.MM.YYYY') START_DUE_DATE, MANUAL_NO, " +
                             "FORMULA, MEASUREMENT_TYPE, LUAS_1, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, COA_PROD " +
                             "FROM V_TRANS_SPECIAL_CONT_CONDITION " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDetil = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new { c = CONTRACT_NO });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- DATA DETAIL CONDITION --------------------------
        public DataTablesContract GetDataDetailCondition(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO,'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, to_char(START_DUE_DATE,'DD.MM.YYYY') START_DUE_DATE, MANUAL_NO, " +
                             "FORMULA, MEASUREMENT_TYPE, LUAS_1, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, COA_PROD " +
                             "FROM V_TRANS_SPECIAL_CONT_CONDITION " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesContract GetDataDetailManualy(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                             "FROM V_TRANS_SPECIAL_CONT_MANUAL " +
                             "WHERE CONTRACT_NO=:c";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql,
                        new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MEMO ----------------------
        public DataTablesContract GetDataDetailMemo(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_CALC, CONDITION_TYPE, MEMO " +
                             "FROM V_TRANS_SPECIAL_CONT_MEMO " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------- UBAH STATUS ----------------------
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT STATUS " +
                "FROM PROP_CONTRACT " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_CONTRACT " +
                    "SET STATUS = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE2()
        {
            string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql);

            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter()
        {
            string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1 ";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN USAGE TYPE ------------------------
        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT_USAGE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listDetil = connection.Query<DDContractUsage>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN CONTRACT TYPE ------------------------
        public IEnumerable<DDContractType> GetDataContractType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractType> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT' AND ACTIVE = '1' AND ID !=188 ORDER BY ID";

                listData = connection.Query<DDContractType>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA UNTUK CONTRACT OFFER
        public IEnumerable<CO_RO_OBJECT> GetDataObject(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_RO_OBJECT> listData = null;

            try
            {
                string sql = "SELECT OBJECT_TYPE, OBJECT_NAME, CONTRACT_OFFER_NO, to_char(START_DATE, 'DD/MM/YYYY') START_DATE, to_char(END_DATE, 'DD/MM/YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, OBJECT_ID, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c";

                listData = connection.Query<CO_RO_OBJECT>(sql, new
                {
                    c = id
                });
            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA UNTUK CONDITION
        public IEnumerable<CO_CONDITION> GetDataCondition(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_CONDITION> listData = null;

            try
            {
                string sql = "SELECT CONTRACT_OFFER_NO, CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD/MM/YYYY') VALID_FROM, to_char(VALID_TO, 'DD/MM/YYYY') VALID_TO, MONTHS, " +
                             "STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD/MM/YYYY')START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, COA_PROD " +
                             "FROM V_CONTRACT_OFFER_CONDITION WHERE CONTRACT_OFFER_NO=:c ";

                listData = connection.Query<CO_CONDITION>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA UNTUK MANUAL FREQUENCY
        public IEnumerable<CO_MANUAL_FREQUENCY> GetDataManualFrequency(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_MANUAL_FREQUENCY> listData = null;

            try
            {
                string sql = "SELECT ID, CONTRACT_OFFER_NO, MANUAL_NO, CONDITION AS CONDITION_FREQ, to_char(DUE_DATE, 'DD/MM/YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT FROM V_CONTRACT_OFFER_MANUAL WHERE CONTRACT_OFFER_NO=:c";

                listData = connection.Query<CO_MANUAL_FREQUENCY>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA MEMO
        public IEnumerable<CO_MEMO> GetDataMemo(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_MEMO> listData = null;

            try
            {
                string sql = "SELECT ID, CALC_OBJECT, CONDITION_TYPE AS CONDITON_TYPE_MEMO, MEMO, CONTRACT_OFFER_NO FROM V_CONTRACT_OFFER_MEMO WHERE CONTRACT_OFFER_NO=:c";

                listData = connection.Query<CO_MEMO>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA SALES BASED
        public IEnumerable<CO_SALES_BASED> GetDataSalesBased(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_SALES_BASED> listData = null;

            try
            {
                string sql = "SELECT SALES_TYPE, NAME_OF_TERM, to_char(CALC_FROM,'DD/MM/YYYY') CALC_FROM, to_char(CALC_TO, 'DD/MM/YYYY') CALC_TO, UNIT, PERCENTAGE, TARIF, RR, MINSALES, CONTRACT_OFFER_NO, SR, PAYMENT_TYPE, MINPRODUCTION " +
                             "FROM PROP_CONTRACT_OFFER_SALES_RULE WHERE CONTRACT_OFFER_NO=:c";

                listData = connection.Query<CO_SALES_BASED>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----GET DATA REPORTING RULE
        public IEnumerable<CO_REPORTING_RULE> GetDataReportingRule(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_REPORTING_RULE> listData = null;

            try
            {
                //string sql = "SELECT SALES_TYPE, NAME_OF_TERM, to_char(CALC_FROM,'DD/MM/YYYY') CALC_FROM, to_char(CALC_TO, 'DD/MM/YYYY') CALC_TO, UNIT, PERCENTAGE, TARIF, RR, MINSALES, CONTRACT_OFFER_NO, SR FROM PROP_CONTRACT_OFFER_SALES_RULE WHERE CONTRACT_OFFER_NO=:c";
                string sql = "SELECT * FROM PROP_CONTRACT_OFFER_RR WHERE CONTRACT_OFFER_NO=:c";

                listData = connection.Query<CO_REPORTING_RULE>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //------------------------- INSERT DATA HEADER ---------------------
        //public DataReturnTransNumber AddHeader(string KodeCabang, string name, DataTransContract data)
        //{
        //    bool result = false;
        //    try
        //    {
        //        //OracleCommand cmd = (OracleCommand)DatabaseFactory.GetConnection().CreateCommand();
        //        using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
        //        {
        //            //connection.Open();
        //            if (connection.State.Equals(ConnectionState.Closed))
        //                connection.Open();
        //            OracleCommand cmd = connection.CreateCommand();
        //            cmd.CommandText = "PROP_BILLING.CREATE_BILLING";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.BindByName = true;
        //            OracleParameter pContractNumber = new OracleParameter("p_contract_number", OracleDbType.Varchar2,
        //                ParameterDirection.Input)
        //            {
        //                Value = contractNo,
        //                Size = 10
        //            };
        //            OracleParameter pUserName = new OracleParameter("p_update_by", OracleDbType.Varchar2,
        //                ParameterDirection.Input)
        //            {
        //                Value = updateBy,
        //                Size = 50
        //            };
        //            /* jika mengembalikan nilai balik dari oracle 
        //            OracleParameter pOut = new OracleParameter("ret", OracleDbType.Varchar2,
        //                ParameterDirection.ReturnValue)
        //            { Size = 50 };
        //            */
        //            cmd.Parameters.Add(pContractNumber);
        //            cmd.Parameters.Add(pUserName);
        //            //cmd.Parameters.AddArray<string>(
        //            //    name: "list_kd_konfirmasi",
        //            //    dbType: OracleDbType.Varchar2,
        //            //    array: list,
        //            //    direction: ParameterDirection.Input,
        //            //    emptyArrayValue: null);
        //            //cmd.Parameters.Add(pOut);

        //            cmd.ExecuteNonQuery();

        //            result = true;
        //            //Convert.ToString(cmd.Parameters["ret"].Value);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //    }

        //    return result;

        //}

        public DataReturnTransNumber AddHeaderProcedure(string kodeCabang, DataTransContract data, string updateBy)
        {
            DataReturnTransNumber ret = new DataReturnTransNumber();
            string _contractNo = String.Empty;
            //bool result = false;
            try
            {
                //OracleCommand cmd = (OracleCommand)DatabaseFactory.GetConnection().CreateCommand();
                using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
                {
                    //connection.Open();
                    if (connection == null || connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    OracleCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "PROC_GENERATE_CONTRACT_DATA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.BindByName = true;
                    OracleParameter pBRANCH_ID = new OracleParameter("pBRANCH_ID", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = kodeCabang,
                        Size = 10
                    };
                    OracleParameter pContract_Offer = new OracleParameter("pContract_Offer", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = data.CONTRACT_OFFER_NO,
                        Size = 50
                    };
                    OracleParameter pContract_name = new OracleParameter("pContract_name", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = data.CONTRACT_NAME,
                        Size = 100
                    };
                    OracleParameter pIjin_prinsip_no = new OracleParameter("pIjin_prinsip_no", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = (String.IsNullOrEmpty(data.IJIN_PRINSIP_NO) ? "" : data.IJIN_PRINSIP_NO),
                        Size = 100
                    };
                    OracleParameter pIjin_prinsip_date = new OracleParameter("pIjin_prinsip_date", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = (data.IJIN_PRINSIP_DATE == null ? "" : data.IJIN_PRINSIP_DATE),
                        Size = 100
                    };
                    OracleParameter plegal_no = new OracleParameter("plegal_no", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = (String.IsNullOrEmpty(data.LEGAL_CONTRACT_NO) ? "" : data.LEGAL_CONTRACT_NO),
                        Size = 100
                    };
                    OracleParameter plegal_date = new OracleParameter("plegal_date", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = (data.LEGAL_CONTRACT_DATE == null ? "" : data.LEGAL_CONTRACT_DATE),
                        Size = 100
                    };
                    OracleParameter pCreation_by = new OracleParameter("pCreation_by", OracleDbType.Varchar2,
                       ParameterDirection.Input)
                    {
                        Value = updateBy,
                        Size = 100
                    };
                    OracleParameter pOut = new OracleParameter("pContractNo", OracleDbType.Varchar2,
                        ParameterDirection.Output)
                    {
                        Size = 50
                    };

                    cmd.Parameters.Add(pBRANCH_ID);
                    cmd.Parameters.Add(pContract_Offer);
                    cmd.Parameters.Add(pContract_name);
                    cmd.Parameters.Add(pIjin_prinsip_no);
                    cmd.Parameters.Add(pIjin_prinsip_date);
                    cmd.Parameters.Add(plegal_no);
                    cmd.Parameters.Add(plegal_date);
                    cmd.Parameters.Add(pCreation_by);
                    //cmd.Parameters.AddArray<string>(
                    //    name: "list_kd_konfirmasi",
                    //    dbType: OracleDbType.Varchar2,
                    //    array: list,
                    //    direction: ParameterDirection.Input,
                    //    emptyArrayValue: null);
                    cmd.Parameters.Add(pOut);

                    cmd.ExecuteNonQuery();
                    _contractNo = pOut.Value.ToString();

                    //var parameter = new DynamicParameters();

                    //parameter.Add(":pBRANCH_ID", kodeCabang);
                    //parameter.Add(":pContract_Offer", data.CONTRACT_OFFER_NO);
                    //parameter.Add(":pContract_name", data.CONTRACT_NAME);
                    //parameter.Add(":pIjin_prinsip_no", string.IsNullOrEmpty(data.IJIN_PRINSIP_NO) ? "" : data.IJIN_PRINSIP_NO);
                    //parameter.Add(":pIjin_prinsip_date", data.IJIN_PRINSIP_DATE == null ? "" : data.IJIN_PRINSIP_DATE);
                    //parameter.Add(":plegal_no", string.IsNullOrEmpty(data.LEGAL_CONTRACT_NO) ? "" : data.LEGAL_CONTRACT_NO);
                    //parameter.Add(":plegal_date", data.LEGAL_CONTRACT_DATE == null ? "" : data.LEGAL_CONTRACT_DATE);
                    //parameter.Add(":pCreation_by", updateBy);
                    //parameter.Add(":pContractNo", _contractNo, dbType: DbType.String, direction: ParameterDirection.Output);

                    //connection.Execute("PROC_GENERATE_CONTRACT_DATA", parameter, commandType: CommandType.StoredProcedure);

                    ret = new DataReturnTransNumber();
                    ret.ID_TRANS = _contractNo;
                    ret.RESULT_STAT = (ret.ID_TRANS.Length > 0) ? true : false;
                    MonitoringAnjunganDAL dalM = new MonitoringAnjunganDAL();
                    if (ret.ID_TRANS.Length > 0)
                    {
                        var r = dalM.SetStatusContract("CONTRACT CREATED", _contractNo);
                    }
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
                ret = null;
            }



            return ret;
        }

        public DataReturnTransNumber AddHeader(string name, DataTransContract data, string KodeCabang)
        {
            string qr = "";
            string id_trans = "";
            string id_trans_vb = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (data.CODE_CONTRACT_TYPE == "ZC01")
            {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CONTRACT_ZC01 FROM DUAL");
            }
            else if (data.CODE_CONTRACT_TYPE == "ZC02")
            {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CONTRACT_ZC02 FROM DUAL");
            }
            else if (data.CODE_CONTRACT_TYPE == "ZC03")
            {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CONTRACT_ZC03 FROM DUAL");
            }
            else if (data.CODE_CONTRACT_TYPE == "ZC05")
            {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CONTRACT_ZC05 FROM DUAL");
            }
            else
            {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CONTRACT_ZC06 FROM DUAL");
            }

            //string sql = "INSERT INTO PROP_CONTRACT " +
            //             "(CONTRACT_NO, CONTRACT_NAME, COMPANY_CODE, CURRENCY, RENTAL_REQUEST_NO, " +
            //             "CONTRACT_OFFER_NO, CONTRACT_USAGE, CONTRACT_TYPE, BUSINESS_PARTNER, BUSINESS_PARTNER_NAME, CUSTOMER_AR, BE_ID, PROFIT_CENTER, " +
            //             "IJIN_PRINSIP_NO, IJIN_PRINSIP_DATE, LEGAL_CONTRACT_NO, LEGAL_CONTRACT_DATE, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
            //             "STATUS, DIRECT_CONTRACT, TERM_IN_MONTHS) " +
            //            "VALUES (:a, :b, :c, :d, :e, " +
            //            ":f, :g, :h, :i, :j, :k, :l, :m " +
            //            ":n to_date(:o,'DD/MM/YYYY'), :p, to_date(:q,'DD/MM/YYYY'), to_date(:r,'DD/MM/YYYY'), to_date(:s,'DD/MM/YYYY'), :t, :u, :v)";

            //int r = connection.Execute(sql, new
            //{
            //    a = id_trans_vb,
            //    b = data.CONTRACT_NAME,
            //    c = "1000",
            //    d = data.CURRENCY,
            //    e = data.RENTAL_REQUEST_NO,
            //    f = data.CONTRACT_OFFER_NO,
            //    g = data.CONTRACT_USAGE,
            //    h = data.CONTRACT_TYPE,
            //    i = data.BUSINESS_PARTNER,
            //    j = data.BUSINESS_PARTNER_NAME,
            //    k = data.CUSTOMER_AR,
            //    l = data.BE_ID,
            //    m = data.PROFIT_CENTER,
            //    n = data.IJIN_PRINSIP_NO,
            //    o = data.IJIN_PRINSIP_DATE,
            //    p = data.LEGAL_CONTRACT_NO,
            //    q = data.LEGAL_CONTRACT_DATE,
            //    r = data.CONTRACT_START_DATE,
            //    s = data.CONTRACT_END_DATE,
            //    t = 1,
            //    u = 0,
            //    v = data.TERM_IN_MONTHS
            //});

            string sql = "INSERT INTO PROP_CONTRACT " +
                        "(CONTRACT_NO, CONTRACT_NAME, COMPANY_CODE, CURRENCY, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, CONTRACT_USAGE, CONTRACT_TYPE, BUSINESS_PARTNER, BUSINESS_PARTNER_NAME, CUSTOMER_AR, BE_ID, PROFIT_CENTER, IJIN_PRINSIP_NO, IJIN_PRINSIP_DATE, LEGAL_CONTRACT_NO, LEGAL_CONTRACT_DATE, CONTRACT_START_DATE, CONTRACT_END_DATE, STATUS, DIRECT_CONTRACT, TERM_IN_MONTHS, CONTRACT_STATUS, BRANCH_ID) " +
                        "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, to_date(:o,'DD/MM/YYYY'), :p, to_date(:q,'DD/MM/YYYY'), to_date(:r,'DD/MM/YYYY'), to_date(:s,'DD/MM/YYYY'), :t, :u, :v, :w, :x)";
            int r = connection.Execute(sql, new
            {
                a = id_trans_vb,
                b = data.CONTRACT_NAME,
                c = "1000",
                d = data.CURRENCY,
                e = data.RENTAL_REQUEST_NO,
                f = data.CONTRACT_OFFER_NO,
                g = data.CONTRACT_USAGE,
                h = data.CONTRACT_TYPE,
                i = data.BUSINESS_PARTNER,
                j = data.BUSINESS_PARTNER_NAME,
                k = data.CUSTOMER_AR,
                l = data.BE_ID,
                m = data.PROFIT_CENTER,
                n = data.IJIN_PRINSIP_NO,
                o = data.IJIN_PRINSIP_DATE,
                p = data.LEGAL_CONTRACT_NO,
                q = data.LEGAL_CONTRACT_DATE,
                r = data.CONTRACT_START_DATE,
                s = data.CONTRACT_END_DATE,
                t = 1,
                u = 0,
                v = data.TERM_IN_MONTHS,
                w = "CREATED",
                x = KodeCabang
            });


            try
            {
                MonitoringAnjunganDAL dalM = new MonitoringAnjunganDAL();
                r = dalM.SetStatusContract("CONTRACT CREATED", id_trans_vb);
            }
            catch (Exception)
            {
                r = 0;
                throw;
            }


            qr = "SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE BRANCH_ID=:a AND ID=(SELECT MAX(ID) FROM PROP_CONTRACT)";

            string sql2 = "UPDATE PROP_CONTRACT_OFFER SET CONTRACT_NUMBER=:a WHERE CONTRACT_OFFER_NO=:b";

            int s = connection.Execute(sql2, new
            {
                a = id_trans_vb,
                b = data.CONTRACT_OFFER_NO
            });

            id_trans = connection.ExecuteScalar<string>(qr, new { a = KodeCabang });

            var resultfull = new DataReturnTransNumber();
            resultfull.ID_TRANS = id_trans;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        public bool prosedurBillingCashflow(string contractNo, string updateBy)
        {
            bool result = false;
            try
            {
                //OracleCommand cmd = (OracleCommand)DatabaseFactory.GetConnection().CreateCommand();
                using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
                {
                    //connection.Open();
                    if (connection.State.Equals(ConnectionState.Closed))
                        connection.Open();
                    OracleCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "PROP_BILLING.CREATE_BILLING";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.BindByName = true;
                    OracleParameter pContractNumber = new OracleParameter("p_contract_number", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = contractNo,
                        Size = 10
                    };
                    OracleParameter pUserName = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = updateBy,
                        Size = 50
                    };
                    /* jika mengembalikan nilai balik dari oracle 
                    OracleParameter pOut = new OracleParameter("ret", OracleDbType.Varchar2,
                        ParameterDirection.ReturnValue)
                    { Size = 50 };
                    */
                    cmd.Parameters.Add(pContractNumber);
                    cmd.Parameters.Add(pUserName);
                    //cmd.Parameters.AddArray<string>(
                    //    name: "list_kd_konfirmasi",
                    //    dbType: OracleDbType.Varchar2,
                    //    array: list,
                    //    direction: ParameterDirection.Input,
                    //    emptyArrayValue: null);
                    //cmd.Parameters.Add(pOut);

                    cmd.ExecuteNonQuery();
                    result = true;
                    //Convert.ToString(cmd.Parameters["ret"].Value);
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        //------------------------- INSERT DATA DETAIL OBJECT----------------------------
        public bool AddDetailObject(DataTransContract data)
        {
            string vb_id = "";
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            string sql = "INSERT INTO PROP_CONTRACT_OBJECT " +
                "(CONTRACT_NO, OBJECT_TYPE, OBJECT_NAME, START_DATE, END_DATE, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY, OBJECT_ID) " +
                "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";

            int r = connection.Execute(sql, new
            {
                a = vb_id,
                b = data.OBJECT_TYPE,
                c = data.RO_NAME,
                d = data.START_DATE,
                e = data.END_DATE,
                f = data.LUAS_TANAH,
                g = data.LUAS_BANGUNAN,
                h = data.INDUSTRY,
                i = data.OBJECT_ID
            });
            result = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- INSERT DATA DETAIL ---------------------
        public bool AddDetailCondition(DataTransContract data)
        {
            string vb_id = "";
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");
            //alter_date = connection.ExecuteScalar<string>("ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY'");

            //string sql = "INSERT INTO PROP_CONTRACT_CONDITION " +
            //    "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
            //    "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_NO) " +
            //    "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY HH24:MI:SS'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t)";

            if (data.START_DUE_DATE == null)
            {
                string sql = "INSERT INTO PROP_CONTRACT_CONDITION " +
                "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
                "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_NO) " +
                "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :z, :s, :t)";

                int r = connection.Execute(sql, new
                {
                    a = data.CALC_OBJECT,
                    b = data.CONDITION_TYPE,
                    c = data.VALID_FROM,
                    d = data.VALID_TO,
                    e = data.MONTHS,
                    f = data.STATISTIC,
                    g = data.UNIT_PRICE,
                    h = data.AMT_REF,
                    i = data.FREQUENCY,
                    j = data.START_DUE_DATE,
                    k = data.MANUAL_NO,
                    l = data.FORMULA,
                    m = data.MEASUREMENT_TYPE,
                    n = data.LUAS,
                    o = data.TOTAL,
                    p = data.NJOP_PERCENT,
                    q = data.KONDISI_TEKNIS_PERCENT,
                    z = data.SALES_RULE,
                    s = data.TOTAL_NET_VALUE,
                    t = vb_id
                });
                result = (r > 0) ? true : false;
            }
            else
            {
                string sql = "INSERT INTO PROP_CONTRACT_CONDITION " +
                    "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
                    "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_NO) " +
                    "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t)";
                int r = connection.Execute(sql, new
                {
                    a = data.CALC_OBJECT,
                    b = data.CONDITION_TYPE,
                    c = data.VALID_FROM,
                    d = data.VALID_TO,
                    e = data.MONTHS,
                    f = data.STATISTIC,
                    g = data.UNIT_PRICE,
                    h = data.AMT_REF,
                    i = data.FREQUENCY,
                    j = data.START_DUE_DATE,
                    k = data.MANUAL_NO,
                    l = data.FORMULA,
                    m = data.MEASUREMENT_TYPE,
                    n = data.LUAS,
                    o = data.TOTAL,
                    p = data.NJOP_PERCENT,
                    q = data.KONDISI_TEKNIS_PERCENT,
                    z = data.SALES_RULE,
                    s = data.TOTAL_NET_VALUE,
                    t = vb_id
                });
                result = (r > 0) ? true : false;
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- INSERT DATA DETAIL MANUAL FREQUENCY--------------------
        public bool AddDetailManualFrequency(DataTransContract data)
        {
            string vb_id = "";
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            string sql = "INSERT INTO PROP_CONTRACT_FREQUENCY " +
                "(CONTRACT_NO, MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT) " +
                "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g)";

            int r = connection.Execute(sql, new
            {
                a = vb_id,
                b = data.MANUAL_NO,
                c = data.CONDITION,
                d = data.DUE_DATE,
                e = data.NET_VALUE,
                f = data.QUANTITY,
                g = data.UNIT,
            });
            result = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- INSERT DATA DETAIL MEMO----------------------------
        //public bool AddDetailMemo(DataTransContract data)
        public DataReturnTransNumber AddDetailMemo(string name, DataTransContract data)
        {
            string id_trans = "";
            string vb_id = "";
            //bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            string sql = "INSERT INTO PROP_CONTRACT_MEMO " +
                "(CONTRACT_NO, OBJECT_CALC, CONDITION_TYPE, MEMO) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = vb_id,
                b = data.CALC_OBJECT,
                c = data.CONDITION_TYPE,
                d = data.MEMO
            });

            id_trans = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            var resultfull = new DataReturnTransNumber();
            resultfull.ID_TRANS = id_trans;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        //------------------------- INSERT DATA DETAIL SALES BASED----------------------------
        public DataReturnTransNumber AddDetailSalesBased(string name, DataTransContract data)
        {
            string id_trans = "";
            string vb_id = "";
            //bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            string sql = "INSERT INTO PROP_CONTRACT_SALES_RULE" +
                "(CONTRACT_NO, SALES_TYPE, NAME_OF_TERM, CALC_FROM, CALC_TO, UNIT, PERCENTAGE, TARIF, RR, MINSALES, SR, MINPRODUCTION, PAYMENT_TYPE) " +
                "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e, 'DD/MM/YYYY'), :f, :g, :h, :i, :j, :k, :l, :m)";


            int r = connection.Execute(sql, new
            {
                a = vb_id,
                b = data.SALES_TYPE,
                c = data.NAME_OF_TERM,
                d = data.CALC_FROM,
                e = data.CALC_TO,
                f = data.UNIT,
                g = data.PERCENTAGE,
                h = data.TARIF,
                i = data.RR,
                j = data.MINSALES,
                k = data.SR,
                l = data.MINPRODUCTION,
                m = data.PAYMENT_TYPE
            });

            id_trans = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            var resultfull = new DataReturnTransNumber();
            resultfull.ID_TRANS = id_trans;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        //------------------------- INSERT DATA DETAIL REPORTING RULE ---------------------------
        public DataReturnTransNumber AddDetailReportingRule(string name, DataTransContract data)
        {
            string id_trans = "";
            string vb_id = "";
            //bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();

            vb_id = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            string sql = "INSERT INTO PROP_CONTRACT_SALES_REPORT" +
                "(CONTRACT_NO, REPORTING_RULE_NO, SALES_TYPE, TERM_OF_REPORTING_RULE) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = vb_id,
                b = data.REPORTING_RULE_NO,
                c = data.SALES_TYPE,
                d = data.TERM_OF_REPORTING_RULE
            });

            id_trans = connection.ExecuteScalar<string>("SELECT CONTRACT_NO FROM PROP_CONTRACT WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT)");

            var resultfull = new DataReturnTransNumber();
            resultfull.ID_TRANS = id_trans;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        public bool AddFiles(string sFile, string directory, string KodeCabang, string subPath)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_CONTRACT_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, CONTRACT_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = subPath
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesAttachmentContract GetDataAttachment(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesAttachmentContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONTRACT_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT ID_ATTACHMENT, CASE WHEN ID_ATTACHMENT <= 2603 THEN 'https://s3.pelindo.co.id/remote/REMOTE/Contract/' || CONTRACT_ID || '/' || FILE_NAME
ELSE DIRECTORY END AS DIRECTORY, FILE_NAME, CONTRACT_ID FROM PROP_CONTRACT_ATTACHMENT WHERE CONTRACT_ID=:c ORDER BY ID_ATTACHMENT DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_CONTRACT_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesAttachmentContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM PROP_CONTRACT_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_CONTRACT_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- TERMINATED DATA CONTRACT ---------------------------        
        public bool getTerminatedContract(string updatedBy, DataTerminateContract data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (data.SELECT_CONTRACT_STATUS == "TERMINATED")
            {

                //--- UPDATE TERMINATE
                string sql1 = "UPDATE PROP_CONTRACT SET " +
                              "CONTRACT_STATUS = :a, STATUS = 2, INACTIVE_DATE = TO_DATE(:b,'DD/MM/YYYY'), MEMO_INACTIVE = :c, INACTIVE_CONTRACT_BY = :d " +
                              "WHERE CONTRACT_NO=:e";

                int r1 = connection.Execute(sql1, new
                {
                    a = data.SELECT_CONTRACT_STATUS,
                    b = data.STOP_CONTRACT_DATE,
                    c = data.MEMO_CONTRACT,
                    d = updatedBy,
                    e = data.CONTRACT_NO
                });
                result = (r1 > 0) ? true : false;

                //---UPDATE TGL OCUPIED VALID TO
                //string sql2 = "UPDATE PROP_RO_OCCUPANCY SET " +
                //              "VALID_TO = TO_DATE(:a,'DD/MM/YYYY'), LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate " +
                //              "WHERE CONTRACT_NO=:c";
                string sql2 = @"UPDATE PROP_RO_OCCUPANCY SET 
                            VALID_TO = TO_DATE(:a,'DD/MM/YYYY'), LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate 
                            WHERE CONTRACT_NO=:c and STATUS='OCCUPIED'";

                int r2 = connection.Execute(sql2, new
                {
                    a = data.STOP_CONTRACT_DATE,
                    b = updatedBy,
                    c = data.CONTRACT_NO
                });
                result = (r2 > 0) ? true : false;

                //--- UPDATE TGL VACANT VALID FROM
                //string sql3 = "UPDATE PROP_RO_OCCUPANCY SET " +
                //              "VALID_FROM = (select VALID_TO + 1 from PROP_RO_OCCUPANCY where CONTRACT_NO=:a), LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate " +
                //              "WHERE RO_NUMBER = (select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:c and STATUS='OCCUPIED') AND STATUS='VACANT'";
                string sql3 = @"UPDATE PROP_RO_OCCUPANCY SET 
                            VALID_FROM = TO_DATE(:a,'DD/MM/YYYY') + 1, LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate 
                            WHERE RO_NUMBER= (select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:c and STATUS='OCCUPIED') and STATUS='VACANT'";

                int r3 = connection.Execute(sql3, new
                {
                    a = data.STOP_CONTRACT_DATE,
                    b = updatedBy,
                    c = data.CONTRACT_NO
                });
                result = (r3 > 0) ? true : false;

                //post to silapor
                TransContractBillingDAL dal = new TransContractBillingDAL();
                dal.PostTerminatedSilapor(data.CONTRACT_NO, data.STOP_CONTRACT_DATE);
            }
            else if (data.SELECT_CONTRACT_STATUS == "INACTIVE")
            {

                //--- UPDATE INACTIVE
                string sql1 = "UPDATE PROP_CONTRACT SET " +
                              "CONTRACT_STATUS = :a, STATUS = 0, INACTIVE_DATE = TO_DATE(:b,'DD/MM/YYYY'), MEMO_INACTIVE = :c, INACTIVE_CONTRACT_BY = :d " +
                              "WHERE CONTRACT_NO=:e";

                int r1 = connection.Execute(sql1, new
                {
                    a = data.SELECT_CONTRACT_STATUS,
                    b = data.STOP_CONTRACT_DATE,
                    c = data.MEMO_CONTRACT,
                    d = updatedBy,
                    e = data.CONTRACT_NO
                });
                result = (r1 > 0) ? true : false;

                //--- UPDATE TGL VACANT VALID FROM
                //string sql3 = "UPDATE PROP_RO_OCCUPANCY SET " +
                //              "VALID_FROM = (select VALID_FROM from PROP_RO_OCCUPANCY where ACTIVE_STATUS=0 " +
                //              "and RO_NUMBER=(select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:a and STATUS='OCCUPIED') and STATUS='ARCHIVED' " +
                //              "and ID = (select MAX(ID) from PROP_RO_OCCUPANCY where ACTIVE_STATUS=0 " +
                //              "and RO_NUMBER=(select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:b and STATUS='OCCUPIED') and STATUS='ARCHIVED')), " +
                //              "LAST_UPDATE_BY = :c, LAST_UPDATE_DATE = sysdate " +
                //              "WHERE RO_NUMBER=(select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:d and STATUS='OCCUPIED') AND STATUS='VACANT'";

                //int r3 = connection.Execute(sql3, new
                //{
                //    a = data.CONTRACT_NO,
                //    b = data.CONTRACT_NO,
                //    c = updatedBy,
                //    d = data.CONTRACT_NO
                //});
                //result = (r3 > 0) ? true : false;
                string sql2 = @"UPDATE PROP_RO_OCCUPANCY SET 
                            VALID_TO = TO_DATE(:a,'DD/MM/YYYY'), LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate 
                            WHERE CONTRACT_NO=:c and STATUS='OCCUPIED'";

                int r2 = connection.Execute(sql2, new
                {
                    a = data.STOP_CONTRACT_DATE,
                    b = updatedBy,
                    c = data.CONTRACT_NO
                });
                result = (r2 > 0) ? true : false;

                //--- UPDATE STATUS OCUPIED TO ARCHIVED
                //string sql2 = "UPDATE PROP_RO_OCCUPANCY SET " +
                //              "STATUS = 'ARCHIVED', ACTIVE_STATUS = 0, LAST_UPDATE_BY = :a, LAST_UPDATE_DATE = sysdate " +  
                //              "WHERE CONTRACT_NO=:b and ACTIVE_STATUS=1";

                //int r2 = connection.Execute(sql2, new
                //{
                //    a = updatedBy,
                //    b = data.CONTRACT_NO
                //});
                //result = (r2 > 0) ? true : false;
                string sql3 = @"UPDATE PROP_RO_OCCUPANCY SET 
                            VALID_FROM = TO_DATE(:a,'DD/MM/YYYY') + 1, LAST_UPDATE_BY = :b, LAST_UPDATE_DATE = sysdate 
                            WHERE RO_NUMBER= (select RO_NUMBER from PROP_RO_OCCUPANCY where CONTRACT_NO=:c and STATUS='OCCUPIED') and STATUS='VACANT'";

                int r3 = connection.Execute(sql3, new
                {
                    a = data.STOP_CONTRACT_DATE,
                    b = updatedBy,
                    c = data.CONTRACT_NO
                });
                result = (r3 > 0) ? true : false;

            }

            //--- UPDATE TGL INACTIVE BILLING
            string sql4 = "UPDATE PROP_CONTRACT_BILLING SET " +
                          "INACTIVE_DATE = TO_DATE(:a,'DD/MM/YYYY') " +
                          "WHERE ID NOT IN (select BILLING_ID from PROP_CONTRACT_BILLING_POST where BILLING_CONTRACT_NO=:b) AND BILLING_CONTRACT_NO=:c";

            int r4 = connection.Execute(sql4, new
            {
                a = data.STOP_CONTRACT_DATE,
                b = data.CONTRACT_NO,
                c = data.CONTRACT_NO
            });
            result = (r4 > 0) ? true : false;

            //--- UPDATE STATUS REMINDER NOTIFICATION
            string sql5 = "UPDATE PROP_CONTRACT_NOTIFICATION SET " +
                          "STATUS_REMINDER = 1 " +
                          "WHERE CONTRACT_NO=:a";

            int r5 = connection.Execute(sql5, new
            {
                a = data.CONTRACT_NO
            });
            result = (r5 > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }


        //-------------------- DATA TABLE HEADER DATA --------------------
        public DataTablesContract GetDataTransContractnew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string whereSql = search.Length > 2 ? "AND ((CONTRACT_NO || UPPER(CONTRACT_NAME) || UPPER(BUSINESS_PARTNER_NAME) || UPPER(CONTRACT_START_DATE) || UPPER(CONTRACT_END_DATE)) LIKE '%'|| '" + search.ToUpper() + "' ||'%') " : "";
            string whereRegional = " AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) ";
            if (KodeCabang == "2")
            {
                whereRegional = " AND BRANCH_ID IN (SELECT DISTINCT A.BRANCH_ID FROM V_BE A JOIN PROFIT_CENTER B ON B.BRANCH_ID = A.BRANCH_ID AND B.STATUS = 1 WHERE a.REG_GROUP_ONLY = 1 and B.REGIONAL_ID = 100) ";
            } else if (KodeCabang == "9")
            {
                whereRegional = " AND BRANCH_ID IN (SELECT DISTINCT A.BRANCH_ID FROM V_BE A JOIN PROFIT_CENTER B ON B.BRANCH_ID = A.BRANCH_ID AND B.STATUS = 1 WHERE a.REG_GROUP_ONLY = 1 and B.REGIONAL_ID = 110) ";

            } else if (KodeCabang == "12")
            {
                whereRegional = " AND BRANCH_ID IN (SELECT DISTINCT A.BRANCH_ID FROM V_BE A JOIN PROFIT_CENTER B ON B.BRANCH_ID = A.BRANCH_ID AND B.STATUS = 1 WHERE a.REG_GROUP_ONLY = 1 and B.REGIONAL_ID = 130) ";
            } else if (KodeCabang == "24")
            {
                whereRegional = " AND BRANCH_ID IN (SELECT DISTINCT A.BRANCH_ID FROM V_BE A JOIN PROFIT_CENTER B ON B.BRANCH_ID = A.BRANCH_ID AND B.STATUS = 1 WHERE a.REG_GROUP_ONLY = 1 and B.REGIONAL_ID = 120) ";
            } 

            try
            {
                string sql = @"SELECT DISTINCT ID, REGIONAL_NAME, RENTAL_REQUEST_NO, CONTRACT_OFFER_NO, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS,
                        CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS, BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAMES,
                        BE_NAME, TERMINAL_NAME, RO_CERTIFICATE_NUMBER, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS 
                        FROM V_TRANS_SPECIAL_CONT_HEADER WHERE DIRECT_CONTRACT = '0' " + whereSql
                        + whereRegional + " ORDER BY ID DESC ";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------ GET DATA FILTER CONTRACT OFFER
        public DataTablesContract GetDataTransContractOffernew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string whereSql = search.Length > 2 ? " AND (CONTRACT_OFFER_NO || UPPER(CONTRACT_OFFER_NAME) || CONTRACT_START_DATE || CONTRACT_END_DATE || UPPER(CONTRACT_USAGE_F) LIKE '%'|| '" + search.ToUpper() + "' ||'%' ) " : "";

            try
            {
                string sql = "SELECT CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TERM_IN_MONTHS, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                        "CUSTOMER_AR, CUSTOMER_ID, CUSTOMER_NAME, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, CONTRACT_NUMBER, ID_INC, RENTAL_REQUEST_NAME, CONTRACT_OFFER_TYPE_F, " +
                        "BUSINESS_ENTITY_F, PROFIT_CENTER_F, CONTRACT_USAGE_F, OLD_CONTRACT FROM V_HEADER_LIST_CONTRACT WHERE CONTRACT_NUMBER IS NULL AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)  AND COMPLETED_STATUS = '1' " + whereSql
                        + "ORDER BY ID_INC DESC";
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        public OLD_CONTRACT GetOldData(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"select c.CONTRACT_NO CONTRACT_OLD, c.CONTRACT_OFFER_NO CONTRACT_OFFER_OLD, c.RENTAL_REQUEST_NO RENTAL_REQUEST_OLD from PROP_CONTRACT_OFFER a 
                            join PROP_RENTAL_REQUEST b on a.CONTRACT_OFFER_NO = b.CONTRACT_OFFER_NUMBER
                            left join PROP_CONTRACT c on b.OLD_CONTRACT = c.CONTRACT_NO
                            where a.CONTRACT_OFFER_NO = :a";

            var result = connection.Query<OLD_CONTRACT>(sql, new { a = id }).FirstOrDefault();
            connection.Close();
            connection.Dispose();
            return result;
        }
        public dynamic CheckBilling(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = @"select count(1) from PROP_CONTRACT_BILLING_POST a 
                            where a.BILLING_CONTRACT_NO = :a and a.SAP_DOC_NO is not null and a.POSTING_DATE is not null and a.CANCEL_STATUS is null";

            int kdAktif = connection.ExecuteScalar<int>(sql, new { a = id });

            if (kdAktif == 0)
            {
                result = new
                {
                    status = "S",
                    message = "Sukses"
                };
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "gagal"
                };
            }
            connection.Close();
            connection.Dispose();

            return result;
        }
    }
}