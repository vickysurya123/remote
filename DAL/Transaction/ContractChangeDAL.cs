﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransContractOffer;
using Remote.Models.TransContract;
using Remote.Models.TransRR;
using Remote.Models.ContractChange;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Microsoft.SqlServer.Server;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ContractChangeDAL
    {
        public AUP_TRANS_CONTRACT_WEBAPPS GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            /*
            string sql = "SELECT INSTALLATION_NUMBER, to_char(INSTALLATION_DATE,'DD/MM/YYYY') INSTALLATION_DATE, PROFIT_CENTER_ID, CUSTOMER_NAME, CUSTOMER_ID, CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, " +
                         "TARIFF_CODE, MINIMUM_AMOUNT " +
                         "FROM V_INST WHERE INSTALLATION_NUMBER=:a";
            */

            string sql = "SELECT CONTRACT_NO, CONTRACT_NAME, to_char(CONTRACT_START_DATE,'DD/MM/RRRR')CONTRACT_START_DATE, TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, TERM_IN_MONTHS, PROFIT_CENTER, IJIN_PRINSIP_NO, TO_CHAR(IJIN_PRINSIP_DATE, 'MM/DD/YYYY')IJIN_PRINSIP_DATE, LEGAL_CONTRACT_NO, TO_CHAR(LEGAL_CONTRACT_DATE,'MM/DD/YYYY') LEGAL_CONTRACT_DATE FROM V_EDIT_CONTRACT WHERE CONTRACT_NO=:a";

            AUP_TRANS_CONTRACT_WEBAPPS result = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            return result;
        }

        public DataTablesContractChange GetDataFilterContract(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesContractChange result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_NAME, to_char(CONTRACT_START_DATE,'DD/MM/RRRR')CONTRACT_START_DATE, TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, TERM_IN_MONTHS, PROFIT_CENTER FROM V_EDIT_CONTRACT";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);


                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT CONTRACT_NO, CONTRACT_NAME, to_char(CONTRACT_START_DATE,'DD/MM/RRRR')CONTRACT_START_DATE, TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, TERM_IN_MONTHS, PROFIT_CENTER FROM V_EDIT_CONTRACT WHERE " +
                                    "(CONTRACT_NO || UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%')";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_NAME, to_char(CONTRACT_START_DATE,'DD/MM/RRRR')CONTRACT_START_DATE, TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, TERM_IN_MONTHS, PROFIT_CENTER FROM V_EDIT_CONTRACT WHERE BRANCH_ID=:c";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT CONTRACT_NO, CONTRACT_NAME, to_char(CONTRACT_START_DATE,'DD/MM/RRRR')CONTRACT_START_DATE, TO_CHAR(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, TERM_IN_MONTHS, PROFIT_CENTER FROM V_EDIT_CONTRACT WHERE " +
                                    "(CONTRACT_NO || UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') AND BRANCH_ID=:d";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            

            result = new DataTablesContractChange();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<CO_RO_OBJECT> GetDataObject(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_RO_OBJECT> listData = null;

            try
            {
                string sql = "SELECT ID, CONTRACT_NO, OBJECT_TYPE, OBJECT_NAME RO_NAME, to_char(START_DATE,'DD/MM/RRRR') CONTRACT_START_DATE, " +
                             "to_char(END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, LUAS, INDUSTRY, LUAS_TANAH LAND_DIMENSION, LUAS_BANGUNAN BUILDING_DIMENSION, OBJECT_ID " +
                             "FROM PROP_CONTRACT_OBJECT WHERE CONTRACT_NO=:c";

                listData = connection.Query<CO_RO_OBJECT>(sql, new
                {
                    c = id
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            return listData;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailConditionEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {
                //string sql = "SELECT ID, RENTAL_REQUEST_NO, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, OBJECT_ID, RO_NAME, REF_DATA || ' - ' || REF_DESC AS OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER FROM V_EDIT_RENTAL_REQUEST WHERE RENTAL_REQUEST_NO=:a";
                string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD/MM/YYYY') VALID_FROM, to_char(VALID_TO, 'DD/MM/YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD/MM/YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, TAX_CODE, COA_PROD " +
                             "FROM V_CONTRACT_CONDITION " +
                             "WHERE CONTRACT_NO=:a ORDER BY ID DESC";

                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailManualFrequencyEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {


                string sql = "SELECT DISTINCT MANUAL_NO, CONDITION,  ' | ' || CONDITION || ' ' || REF_DESC AS CONDITION_TYPE, to_char(DUE_DATE,'DD/MM/YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                            "FROM V_CONTRACT_MANUAL_EDIT " +
                            "WHERE CONTRACT_NO=:a ORDER BY MANUAL_NO ASC";                

                /*
                string sql = "SELECT MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE " +
                             "FROM V_TRANS_SPECIAL_CONT_MANUAL " +
                             "WHERE CONTRACT_NO=:c";
                */
                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id, 
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            connection.Close();
            return listData;
        }


        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailMemoEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, MEMO " +
                             "FROM V_CONTRACT_MEMO " +
                             "WHERE CONTRACT_NO=:a";

                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<CO_RO_OBJECT> GetDataRO(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_RO_OBJECT> listData = null;

            try
            {
                string sql = "SELECT ID, CONTRACT_NO, OBJECT_NAME RO_NAME, to_char(START_DATE,'DD/MM/RRRR') CONTRACT_START_DATE, " +
                             "to_char(END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, LUAS, INDUSTRY, LUAS_TANAH LAND_DIMENSION, LUAS_BANGUNAN BUILDING_DIMENSION, OBJECT_ID " +
                             "FROM PROP_CONTRACT_OBJECT WHERE CONTRACT_NO=:c";

                listData = connection.Query<CO_RO_OBJECT>(sql, new
                {
                    c = id
                });
            }
            catch (Exception)
            {
            }
            connection.Close();
            return listData;
        }

        public IEnumerable<DDMeasType> GetDataMeasType(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDMeasType> listData = null;

            try
            {
                string sql = "SELECT * FROM V_MEAS_TYPE_CO WHERE RO_CODE=:c";

                listData = connection.Query<DDMeasType>(sql, new
                {
                    c = id
                });
            }
            catch (Exception) { }
            connection.Close();
            return listData;
        }

        //----------------------------------- Save Edit Contract --------------------------
        public bool UpdateHeader(string name, DataTransContract data, string KodeCabang, string UpdatedBy)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "";
            int r = 0;

            // Update Ijin Prinsip  
            sql = "UPDATE PROP_CONTRACT SET IJIN_PRINSIP_NO=:b, IJIN_PRINSIP_DATE=to_date(:c,'MM/DD/YYYY'), LEGAL_CONTRACT_NO=:d, LEGAL_CONTRACT_DATE=to_date(:e,'MM/DD/YYYY') WHERE CONTRACT_NO=:a";
            //sql = "UPDATE PROP_CONTRACT SET IJIN_PRINSIP_NO=:b WHERE CONTRACT_NO=:a";            
            try
            {
                r = connection.Execute(sql, new
                {
                    a = data.CONTRACT_NO,
                    b = data.IJIN_PRINSIP_NO,
                    c = data.IJIN_PRINSIP_DATE,
                    d = data.LEGAL_CONTRACT_NO,
                    e = data.LEGAL_CONTRACT_DATE                   
                });
            }
            catch
            {

            }
            

            // Insert History Perubahan
            sql = "INSERT INTO PROP_CONTRACT_HTR (CONTRACT_NO, CHANGE_DATE, CHANGE_BY) VALUES (:a, sysdate, :b)";
            try
            {
                r = connection.Execute(sql, new
                {
                    a = data.CONTRACT_NO,
                    b = UpdatedBy
                });
            }
            catch
            {

            }

            // Get last ID untuk nomor kontrak yg baru saja di insert ke PROP_CONTRACT_HTR
            string ID_PROP_CONTRACT_HTR = connection.ExecuteScalar<string>("SELECT MAX(ID) FROM PROP_CONTRACT_HTR WHERE CONTRACT_NO='"+data.CONTRACT_NO+"'");

            if (data.Conditions != null)
            {
                // Select data condition lama dan pindah ke HTR
                sql = "INSERT INTO PROP_CONTRACT_CONDITION_HTR (CALC_OBJECT, CONDITION_TYPE, CONDITION_TYPE_ID, VALID_FROM, VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, MEASUREMENT_TYPE_ID, LUAS, TOTAL, NJOP_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, CONTRACT_NO, KONDISI_TEKNIS_PERCENT, INSTALLMENT_AMOUNT, TAX_CODE, INSTALLMENT_QTY, OBJECT_ID, PROP_CONTRACT_HTR, COA_PROD) " +
                      "SELECT CALC_OBJECT, CONDITION_TYPE, CONDITION_TYPE_ID, VALID_FROM, VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, MEASUREMENT_TYPE_ID, LUAS, TOTAL, NJOP_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, CONTRACT_NO, KONDISI_TEKNIS_PERCENT, INSTALLMENT_AMOUNT, TAX_CODE, INSTALLMENT_QTY, OBJECT_ID, :a, coa_prod " +
                      "FROM PROP_CONTRACT_CONDITION WHERE CONTRACT_NO=:b";
                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = ID_PROP_CONTRACT_HTR,
                        b = data.CONTRACT_NO
                    });
                }
                catch
                {

                }

                // Hapus data condition lama
                sql = "DELETE FROM PROP_CONTRACT_CONDITION WHERE CONTRACT_NO=:a";
                try
                {
                    r = connection.Execute(sql, new
                    {
                       a = data.CONTRACT_NO
                    });
                }
                catch
                {

                }

                // Insert Data Condition Ke Temporary
                string ID_CONDITION_TYPE = "";
                sql = "INSERT INTO PROP_CONTRACT_CONDITION_TMP " +
                      "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
                      "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_NO, TAX_CODE, INSTALLMENT_AMOUNT, CONDITION_TYPE_ID, OBJECT_ID, COA_PROD) " +
                      "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t, :u, :v, :w, :x, :y)";
                
                foreach (var i in data.Conditions)
                {
                    string subConditionType = i.CONDITION_TYPE.Substring(0, 4);
                    ID_CONDITION_TYPE = connection.ExecuteScalar<string>("SELECT ID FROM PROP_PARAMETER_REF_D WHERE REF_DATA='"+subConditionType+"' AND REF_CODE='CONDITION_TYPE'");

                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = i.CALC_OBJECT,
                            b = i.CONDITION_TYPE,
                            c = i.VALID_FROM,
                            d = i.VALID_TO,
                            e = i.MONTHS,
                            f = i.STATISTIC,
                            g = i.UNIT_PRICE,
                            h = i.AMT_REF,
                            i = i.FREQUENCY,
                            j = i.START_DUE_DATE,
                            k = i.MANUAL_NO,
                            l = i.FORMULA,
                            m = i.MEASUREMENT_TYPE,
                            n = i.LUAS,
                            o = i.TOTAL,
                            p = i.NJOP_PERCENT,
                            q = i.KONDISI_TEKNIS_PERCENT,
                            z = string.Empty,
                            s = i.TOTAL_NET_VALUE,
                            t = data.CONTRACT_NO,
                            u = i.TAX_CODE,
                            v = i.INSTALLMENT_AMOUNT,
                            w = ID_CONDITION_TYPE,
                            x = i.OBJECT_ID,
                            y = i.COA_PROD
                        });
                        r = 1;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }
                }

                    // Insert data dari table temporary ke table utama
                    sql = "INSERT INTO PROP_CONTRACT_CONDITION (CALC_OBJECT, CONDITION_TYPE, CONDITION_TYPE_ID, VALID_FROM, VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, MEASUREMENT_TYPE_ID, LUAS, TOTAL, NJOP_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, CONTRACT_NO, KONDISI_TEKNIS_PERCENT, INSTALLMENT_AMOUNT, TAX_CODE, INSTALLMENT_QTY, OBJECT_ID, COA_PROD) " +
                          "SELECT CALC_OBJECT, CONDITION_TYPE, CONDITION_TYPE_ID, VALID_FROM, VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, MEASUREMENT_TYPE_ID, LUAS, TOTAL, NJOP_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, CONTRACT_NO, KONDISI_TEKNIS_PERCENT, INSTALLMENT_AMOUNT, TAX_CODE, INSTALLMENT_QTY, OBJECT_ID,  COA_PROD " +
                          "FROM PROP_CONTRACT_CONDITION_TMP WHERE CONTRACT_NO=:a";
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO
                        });
                    }
                    catch
                    {

                    }

                    // Hapus data dari table temporary
                    sql = "DELETE FROM PROP_CONTRACT_CONDITION_TMP WHERE CONTRACT_NO=:a";
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO
                        });
                    }
                    catch
                    {

                    }            
            }
            
           
            if (data.Frequencies != null)
            {
                // Select data frequency lama dan pindah ke HTR
                sql = "INSERT INTO PROP_CONTRACT_FREQUENCY_HTR (CONTRACT_NO, MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, BILLING_NO, CALC_OBJECT, CONDITION_ID, FREQUENCY_TYPE, VAT_VALUE, GROSS_VALUE, SEQUENCE_PAYMENT, QUANTITY, UNIT, PROP_CONTRACT_HTR) " +
                         "SELECT CONTRACT_NO, MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, BILLING_NO, CALC_OBJECT, CONDITION_ID, FREQUENCY_TYPE, VAT_VALUE, GROSS_VALUE, SEQUENCE_PAYMENT, QUANTITY, UNIT, :b " +
                         "FROM PROP_CONTRACT_FREQUENCY WHERE CONTRACT_NO=:a";
                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO,
                        b = ID_PROP_CONTRACT_HTR
                    });
                }
                catch
                {

                }

                // Hapus data frequency lama
                sql = "DELETE FROM PROP_CONTRACT_FREQUENCY WHERE CONTRACT_NO=:a";
                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO
                    });
                }
                catch
                {

                }

                // Insert data frequency baru
                sql = "INSERT INTO PROP_CONTRACT_FREQUENCY " +
                  "(CONTRACT_NO, MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT) " +
                  "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g)";
                foreach (var i in data.Frequencies)
                {
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = i.MANUAL_NO,
                            c = i.CONDITION_TYPE,
                            d = i.DUE_DATE,
                            e = i.NET_VALUE,
                            f = i.QUANTITY,
                            g = i.UNIT
                        });
                        r = 1;
                    }
                    catch (Exception e)
                    {
                        string aaaa;
                        aaaa = e.Message;
                        r = 0;
                        throw;
                    }
                }
            }


            if (data.Memos != null)
            {
                // Select data memo lama dan pindah ke HTR
                sql = "INSERT INTO PROP_CONTRACT_MEMO_HTR (OBJECT_CALC, CONDITION_TYPE, MEMO, CONTRACT_NO, PROP_CONTRACT_HTR) " +
                      "SELECT OBJECT_CALC, CONDITION_TYPE, MEMO, CONTRACT_NO, :b " +
                      "FROM PROP_CONTRACT_MEMO WHERE CONTRACT_NO=:a";
                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO,
                        b = ID_PROP_CONTRACT_HTR
                    });
                }
                catch
                {

                }

                // Hapus data memo lama
                sql = "DELETE FROM PROP_CONTRACT_MEMO WHERE CONTRACT_NO=:a";
                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO
                    });
                }
                catch
                {

                }

                sql = "INSERT INTO PROP_CONTRACT_MEMO " +
                      "(CONTRACT_NO, OBJECT_CALC, CONDITION_TYPE, MEMO) " +
                      "VALUES (:a, :b, :c, :d)";
                foreach (var i in data.Memos)
                {
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = i.OBJECT_CALC,
                            c = i.CONDITION_TYPE,
                            d = i.MEMO
                        });
                        r = 1;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }
                }
            }

            // RE-GENERATE TEMPLATE BILLING
            /*
            try
            {
                // Panggil Procedure Generate Template Billing
                using (var connectionx = new OracleConnection(DatabaseHelper.GetConnectionString()))
                {
                    //connection.Open();
                    if (connectionx.State.Equals(ConnectionState.Closed))
                        connectionx.Open();
                    OracleCommand cmd = connectionx.CreateCommand();
                    cmd.CommandText = "PROC_GEN_CONTRACT_EDIT";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.BindByName = true;
                    OracleParameter XCONTRACT_NO = new OracleParameter("XCONTRACT_NO", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = data.CONTRACT_NO,
                        Size = 10
                    };
                    OracleParameter p_update_by = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = UpdatedBy,
                        Size = 50
                    };
                    cmd.Parameters.Add(XCONTRACT_NO);
                    cmd.Parameters.Add(p_update_by);

                    cmd.ExecuteNonQuery();

                    connection.Close();
                    result = true;
                }
            }
            catch
            {

            }
            */

            /*
            sql = "UPDATE PROP_CONTRACT_BILLING SET INACTIVE_DATE=sysdate WHERE BILLING_CONTRACT_NO=:a";
            try
            {
                r = connection.Execute(sql, new
                {
                    a = data.CONTRACT_NO
                });
                r = 1;
            }
            catch (Exception)
            {
                r = 0;
                throw;
            }
            */

            try
            {
                //OracleCommand cmd = (OracleCommand)DatabaseFactory.GetConnection().CreateCommand();
                using (var connectionx = new OracleConnection(DatabaseHelper.GetConnectionString()))
                {
                    //connection.Open();
                    if (connectionx.State.Equals(ConnectionState.Closed))
                        connectionx.Open();
                    OracleCommand cmd = connectionx.CreateCommand();
                    cmd.CommandText = "PROP_BILLING.CREATE_BILLING";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.BindByName = true;
                    OracleParameter pContractNumber = new OracleParameter("p_contract_number", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = data.CONTRACT_NO,
                        Size = 10
                    };
                    OracleParameter pUserName = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = UpdatedBy,
                        Size = 50
                    };

                    cmd.Parameters.Add(pContractNumber);
                    cmd.Parameters.Add(pUserName);

                    cmd.ExecuteNonQuery();

                    connectionx.Close();
                    result = true;
                }
            }
            catch (Exception)
            {
            }

            result = (r > 0) ? true : false;
            connection.Close();
            return result;
        }

        public DataTablesHistoryContractChanges GetDataHistoryContract(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesHistoryContractChanges result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            try
            {
                string sql = "SELECT ID, CONTRACT_NO, to_char(CHANGE_DATE,'DD/MM/RRRR') CHANGE_DATE, CHANGE_BY FROM PROP_CONTRACT_HTR WHERE CONTRACT_NO=:c ORDER BY CHANGE_DATE DESC";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id });
            }
            catch (Exception)
            {

            }

            result = new DataTablesHistoryContractChanges();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;

        }

        public DataTablesContract GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE,'DD.MM.YYYY') START_DATE, to_char(END_DATE,'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_TRANS_SPECIAL_CONT_OBJECT " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //-------------------- DATA DETAIL CONDITION --------------------------
        public DataTablesContract GetDataDetailCondition(int draw, int start, int length, string search, string contract_no, string id_perubahan)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO,'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, to_char(START_DUE_DATE,'DD.MM.YYYY') START_DUE_DATE, MANUAL_NO, " +
                             "FORMULA, MEASUREMENT_TYPE, LUAS_1, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE " +
                             "FROM V_HISTORY_CONTRACT_CONDITION " +
                             "WHERE CONTRACT_NO=:c AND PROP_CONTRACT_HTR=:d ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = contract_no, d = id_perubahan });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = contract_no, d = id_perubahan });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesContract GetDataDetailManualy(int draw, int start, int length, string search, string contract_no, string id_perubahan)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                             "FROM V_HISTORY_CONTRACT_FREQUENCY " +
                             "WHERE CONTRACT_NO=:c AND PROP_CONTRACT_HTR=:d";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql,
                        new { a = start, b = end, c = contract_no, d = id_perubahan });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = contract_no, d = id_perubahan });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public DataTablesContract GetDataDetailMemo(int draw, int start, int length, string search, string contract_no, string id_perubahan)
        {
            int count = 0;
            int end = start + length;
            DataTablesContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_CALC, CONDITION_TYPE, MEMO " +
                             "FROM V_HISTORY_CONTRACT_MEMO " +
                             "WHERE CONTRACT_NO=:c AND PROP_CONTRACT_HTR=:d ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = contract_no, d = id_perubahan });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = contract_no, d = id_perubahan });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;

        }

        public DataTablesPostedBilling GetDataDetailPostedBilling(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPostedBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_HISTORY_POSTED_BILLING> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT BILLING_ID, BILLING_CONTRACT_NO, BILLING_NO, SAP_DOC_NO, to_char(BILLING_DUE_DATE, 'DD/MM/RRRR') BILLING_DUE_DATE, BILLING_PERIOD FROM PROP_CONTRACT_BILLING_POST WHERE BILLING_CONTRACT_NO=:c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_HISTORY_POSTED_BILLING>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPostedBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public DataTablesPostedBilling GetDataDetailPostedBillingD(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPostedBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_HISTORY_POSTED_BILLING> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT BILLING_ID, CONDITION_TYPE, to_char(BILLING_DUE_DATE,'DD/MM/RRRR') BILLING_DUE_DATE, BILLING_PERIOD, OBJECT_ID, INSTALLMENT_AMOUNT FROM PROP_CONTRACT_BILLING_POST_D WHERE BILLING_ID=:c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_HISTORY_POSTED_BILLING>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception e)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPostedBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }
    }
}