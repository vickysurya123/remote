﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransVB;
using Remote.Models.Select2;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransVBDAL
    {
        //------------------------- DATA TABLE TRANSVB ------------------------------//
        public DataTablesTransVB GetDataTransVB(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try {

                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (KodeCabang == "0" && UserRole == "4")
                {
                    if (search.Length == 0)
                    {
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME, SAP_DOCUMENT_NUMBER, BILLING_TYPE " +
                                         "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                         "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%') AND (SAP_DOCUMENT_NUMBER LIKE '%' ||:c|| '%'))";
                            */

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB WHERE ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                    }
                }
                else if (KodeCabang == "0" && UserRole == "20")
                {
                    if (search.Length == 0)
                    {
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME, SAP_DOCUMENT_NUMBER, BILLING_TYPE " +
                                         "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                         "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%') AND (SAP_DOCUMENT_NUMBER LIKE '%' ||:c|| '%'))";
                            */

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                    }
                }
                else
                {
                    if (search.Length == 0)
                    {
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME, SAP_DOCUMENT_NUMBER, BILLING_TYPE " +
                                         "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                         "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%') AND (SAP_DOCUMENT_NUMBER LIKE '%' ||:c|| '%'))";
                            */

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS " +
                                     "FROM V_TRANSVB WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) LIKE '%' ||:c|| '%') ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                    }
                }

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE TRANS VB --------------------------
        public DataTablesTransVB GetDataTransactionVB(int draw, int start, int length, string search, string idVB)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try { 
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT d.ID, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, START_DATE AS S_DATE, END_DATE AS E_DATE, 
                                 SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT || ' | ' || (SELECT DISTINCT ref_desc FROM prop_parameter_ref_d 
                                  WHERE UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, SURCHARGE, AMOUNT, REMARK, VB_ID, CURRENCY ||'  '|| PRICE AS CM_PRICE, COA_PROD, RATE, AMOUNT/NVL(RATE,1) TOTAL_USD
                                 FROM PROP_VB_TRANSACTION_DETAIL d, PROP_VB_TRANSACTION v WHERE d.VB_ID=v.ID AND VB_ID = :c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = idVB });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    // Do nothing. Not implemented yet.
                }

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        } 

        //------------------------- INSERT DATA HEADER ---------------------
        public DataReturnTransNumber AddHeader(string name, DataTransVB data, string KodeCabang)
        {
            string qr = "";
            string id_trans = "";
            string id_trans_vb = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try {
                id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_TRANS_VB_TRANSACTION AS TRANS_CODE_VB FROM DUAL");            
            }
            catch (Exception) { }

            var resultfull = new DataReturnTransNumber();

            try {
                string sql = "INSERT INTO PROP_VB_TRANSACTION " +
                    "(ID, POSTING_DATE, COSTUMER_MDM, BE_ID, PROFIT_CENTER, SERVICES_GROUP, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, DOCUMENT_TYPE, BILLING_TYPE, BRANCH_ID) " +
                    "VALUES (:m, to_date(:a,'DD/MM/YYYY'), :b, :c, :d, :e, :f, :g, :h, :j, :k, :l, :n)";

                int r = connection.Execute(sql, new
                {
                    a = data.POSTING_DATE,
                    b = data.COSTUMER_MDM,
                    c = data.BE_ID,
                    d = data.PROFIT_CENTER,
                    e = data.SERVICES_GROUP,
                    f = data.COSTUMER_ID,
                    g = data.INSTALLATION_ADDRESS,
                    h = data.CUSTOMER_SAP_AR,
                   // i = data.TAX_CODE,
                    j = data.TOTAL,
                    k = "1M",
                    l = "ZM03",
                    m = id_trans_vb,
                    n = KodeCabang
                });

                try
                {
                    qr = "SELECT ID FROM PROP_VB_TRANSACTION WHERE BRANCH_ID=:a AND ID=(SELECT MAX(ID) FROM PROP_VB_TRANSACTION)";
                    id_trans = connection.ExecuteScalar<string>(qr, new {a=KodeCabang});
                }
                catch (Exception) { }

                resultfull.ID_TRANS = id_trans;
                resultfull.RESULT_STAT = (r > 0) ? true : false;
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            //resultfull.RESULT_STAT = (r1 > 0) ? true : false;

            return resultfull;
            //result = (r > 0) ? true : false;

            //return result;
        }

        //------------------------- INSERT DATA DETAIL ---------------------
        public bool AddDetail(string name, DataTransVB data)
        {
            string vb_id = "";
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                vb_id = connection.ExecuteScalar<string>("SELECT ID FROM PROP_VB_TRANSACTION WHERE ID=(SELECT MAX(ID) FROM PROP_VB_TRANSACTION)");
            }
            catch (Exception) { }
            try { 
                string sql = "INSERT INTO PROP_VB_TRANSACTION_DETAIL " +
                    "(START_DATE, END_DATE, SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT, SURCHARGE, AMOUNT, " +
                    "VB_ID, TAX_CODE, REMARK) " +
                    "VALUES (to_date(:a,'DD/MM/YYYY'), to_date(:b,'DD/MM/YYYY'), :c, :d, :e, :f, :g, :h, :i, :j, :k, :n, :o, :p)";
             
                int r = connection.Execute(sql, new
                {
                    a = data.START_DATE,
                    b = data.END_DATE,
                    c = data.SERVICE_CODE,
                    d = data.SERVICE_NAME,
                    e = data.CURRENCY,
                    f = data.PRICE,
                    g = data.MULTIPLY_FACTOR,
                    h = data.QUANTITY,
                    i = data.UNIT,
                    j = data.SURCHARGE,
                    k = data.AMOUNT,
                    n = vb_id,
                    o = data.TAX_CODE,
                    p = data.REMARK
                });

                result = (r > 0) ? true : false;
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- DROP DOWN PROFIT CENTER ---------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string user_id)
        {
            string sql = "SELECT A.PROFIT_CENTER_ID, A.BE_ID, A.PROFIT_CENTER_ID || ' - ' || A.TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER A JOIN APP_REPO.VW_APP_USER B ON B.BE_ID = A.BE_ID WHERE B.ID=:a and A.STATUS = 1";
            // string sql = "SELECT PROFIT_CENTER_ID, BE_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a  and STATUS = 1";
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDProfitCenter> listDetil = null;
            if (connection.State.Equals(ConnectionState.Closed))
            connection.Open();            
            try {
                listDetil = connection.Query<DDProfitCenter>(sql, new { a=user_id });
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //--------------------- LIST DROP DOWN SERVICE GROUP -----------------------
        public IEnumerable<DDServiceGroup> GetDataService()
        {
            string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC || ' - ' || VAL1 AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' and VAL1 != '4070100000' and VAL1 != '4070300000'  AND ACTIVE = '1' ORDER BY VAL1 ASC, REF_DESC ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDServiceGroup> listDetil=null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try {
                listDetil = connection.Query<DDServiceGroup>(sql);
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //--------------------- LIST DROP DOWN SERVICE CODE -----------------------
        public List<DDServiceCode> DDServiceCode(string SERVICE_NAME)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND UPPER(SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%' AND a.ACTIVE = '1' AND b.ACTIVE ='1' ";
            List<DDServiceCode> exec = null;
            connection.Open();
            try {
                exec = connection.Query<DDServiceCode>(str, new
                {
                    SERVICE_NAME = SERVICE_NAME
                }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return exec;
        }

        public IEnumerable<DDServiceCode> GetDataDDServiceCode()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDServiceCode> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string str = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND a.ACTIVE = '1' AND b.ACTIVE ='1' ";

                listData = connection.Query<DDServiceCode>(str).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //--------------SELECT 2 SERVICE CODE
        public Select2ServiceCode GetServiceCodeSelect2(string id, string Search)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            Select2ServiceCode data = null;
            try
            {
                data = new Select2ServiceCode();
                Search = "%" + Search + "%";
                string query = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%' AND a.ACTIVE = '1' AND b.ACTIVE ='1' ORDER BY a.SERVICE_CODE ASC";

                
                connection.Open();
                try { 
                    data.items = connection.Query<OptionServiceCode>(query, new { SERVICE_NAME = Search });
                    data.total_count = data.items.Count();
                    data.incomplete_results = false;
                }
                catch (Exception) { }
                connection.Close();
                connection.Dispose();
            }
            catch (Exception)
            {
                data = null;
            }
            return data;
        }

        //----------AUTOCOMPLETE SERVICE CODE RUNNING
        public List<DDServiceCode> DDServiceCode2(string SERVICE_NAME)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string str = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%' AND a.ACTIVE = '1' AND b.ACTIVE ='1' ";
            List<DDServiceCode> exec = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try { 
                exec = connection.Query<DDServiceCode>(str, new
                {
                    SERVICE_NAME = SERVICE_NAME
                }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();

            return exec;
        }

        //------------------------------ LIST AUTO COMPLETE SERVICE CODE ----------------
        public List<AUP_SERVICE_CODE_WEBAPPS> GetDataServiceCode(string SERVICE_NAME, string SERVICE_GROUP, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            // sebelum setting tax air = 2 by gl account
            //string str = "SELECT a.ID, a.TAX_CODE, a.SERVICE_CODE code, a.SERVICE_CODE || ' - ' || a.SERVICE_NAME label, a. SERVICE_NAME, a. SERVICE_NAME, a.UNIT || ' | ' ||(SELECT DISTINCT ref_desc FROM prop_parameter_ref_d WHERE a.UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION multiply, b.VB_ID, b.ACTIVE " +
            //                "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
            //                "WHERE a.ID = b.VB_ID(+) AND ((UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%') OR (a.SERVICE_CODE LIKE '%'||:SERVICE_NAME||'%')) AND a.ACTIVE = '1' AND b.ACTIVE ='1' AND SERVICE_GROUP=:SERVICE_GROUP AND a.BRANCH_ID=:a";

            string str = "SELECT a.ID, a.TAX_CODE, a.SERVICE_CODE code, a.SERVICE_CODE || ' - ' || a.SERVICE_NAME label, a. SERVICE_NAME, a. SERVICE_NAME, a.UNIT || ' | ' ||(SELECT DISTINCT ref_desc FROM prop_parameter_ref_d WHERE a.UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION multiply, b.VB_ID, b.ACTIVE, VAL1 GL_ACCOUNT " + 
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b, PROP_PARAMETER_REF_D d "+
                            "WHERE a.ID = b.VB_ID(+) AND ((UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%') OR (a.SERVICE_CODE LIKE '%'||:SERVICE_NAME||'%')) AND a.ACTIVE = '1' AND b.ACTIVE ='1' AND SERVICE_GROUP=:SERVICE_GROUP AND a.BRANCH_ID=:a " +
                            "AND REF_DATA=:SERVICE_GROUP";
            
            /*
            string str = "SELECT ID, TAX_CODE, label, SERVICE_CODE as code, SERVICE_NAME, UNIT || ' | ' || UNIT_D AS UNIT, ACTIVE, CURRENCY, PRICE, MULTIPLY, VB_ID, ACTIVE_1, SERVICE_GROUP FROM V_DATA_SERVICE_CODE " +
                         "WHERE ((UPPER(SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%') OR (SERVICE_CODE LIKE '%'||:SERVICE_NAME||'%')) AND ACTIVE = '1' AND SERVICE_GROUP=:SERVICE_GROUP";
            */
            List<AUP_SERVICE_CODE_WEBAPPS> exec = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try { 
                exec = connection.Query<AUP_SERVICE_CODE_WEBAPPS>(str, new
                {
                    SERVICE_NAME = SERVICE_NAME,
                    SERVICE_GROUP = SERVICE_GROUP,
                    a = KodeCabang
                }).ToList();
            }
            catch (Exception) { }
            connection.Close();
            return exec;
        }

        //
        
        /*
        public string PostSAP(string BILL_ID, string BILL_TYPE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string XDOC_NUMBER = "";
            var p = new DynamicParameters();
            p.Add("XBILL_ID", BILL_ID); 
            p.Add("XBILL_TYPE", BILL_TYPE);
            p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

            connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
            string retVal = p.Get<string>("XDOC_NUMBER");
            return retVal;
        }
         * */
        
        
        //-- POSTING TO SAP RUNNING
        public DataReturnSAP_COCUMENT_NUMBER PostSAP(string BILL_ID, string BILL_TYPE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string XDOC_NUMBER = string.Empty;
            string retVal = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try { 
                var p = new DynamicParameters();
                p.Add("XBILL_ID", BILL_ID); 
                p.Add("XBILL_TYPE", BILL_TYPE);
                p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

                connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
                retVal = p.Get<string>("XDOC_NUMBER");
            }
            catch (Exception) {
                retVal = null;
            }

            if (string.IsNullOrEmpty(retVal) == false)
            {
                try
                {
                    string sql = "UPDATE PROP_VB_TRANSACTION " +
                      "SET SAP_DOCUMENT_NUMBER =:a " +
                      "WHERE ID = :b";
                    connection.Execute(sql, new
                    {
                        a = retVal,
                        b = BILL_ID
                    });
                }
                catch (Exception) { }
            }
            else
            {
                retVal = null;
            }
            connection.Close();
            connection.Dispose();

            var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
            resultfull.DOCUMENT_NUMBER = retVal;
            //resultfull.RESULT_STAT = (retVal != null) ? true : false;
            return resultfull;

        }

        // Dropdown Service Group
        public IEnumerable<DDServiceGroup> GetDataDropDownServiceGroup()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDServiceGroup> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC || ' - ' || VAL1 AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' AND ACTIVE = '1' ORDER BY VAL1 ASC, ID ASC";

                listData = connection.Query<DDServiceGroup>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        public Array GetDataMsgSAP(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string xml = "";
            try
            {
                /*string a = "SELECT MAX(HEADER_ID) HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO=:REF_DOC_NO";
                string getID = connection.ExecuteScalar<string>(a, new
                {
                    REF_DOC_NO = BILL_ID
                });*/

                string sql = "SELECT XML_DATA FROM VW_ITGR_LOG " +
                  "WHERE REF_DOC_NO = :REF_DOC_NO";

                xml = connection.ExecuteScalar<string>(sql, new
                {
                    REF_DOC_NO = BILL_ID
                });
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            string[] msg = xml.Split('|');

            return msg;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string MPLG_NAMA, string KodeCabang, string UserRole)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            if (KodeCabang == "0" && UserRole == "20")
            {
                 str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                        "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A' AND KD_CABANG = 1";
            }
            else
            {
                str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                        "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A' AND KD_CABANG = '" + KodeCabang + "'";
            }
           
            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();
            connection.Close();
            connection.Dispose();
            return exec;
        }
    }
}