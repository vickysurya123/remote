﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;

namespace Remote.DAL
{

    public class MultipleBillingDAL
    {

        //------------------------------- DEPRECATED ---------------------
        public IDataTable GetData(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<MULTIPLE_BILLING_HEADER> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"select
                                            BE_ID
                                            , PROFIT_CENTER
                                            , CONTRACT_NO
                                            , CONTRACT_NAME
                                            , TOTAL_AMOUNT
                                            , POSTED_AMOUNT
                                            , UNPOSTED_AMOUNT
                                            , CUSTOMER_NAME
                                            , CONTRACT_START_DATE_S CONTRACT_START_DATE
                                            , CONTRACT_END_DATE_S CONTRACT_END_DATE
                                        from V_MULTIPLE_BILLING_HEADER ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<MULTIPLE_BILLING_HEADER>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length >= 2)
                    {
                        try
                        {
                            string sql = @"select 
                                            BE_ID
                                            , PROFIT_CENTER
                                            , CONTRACT_NO
                                            , CONTRACT_NAME
                                            , RO_ADDRESS
                                            , TOTAL_AMOUNT
                                            , POSTED_AMOUNT
                                            , UNPOSTED_AMOUNT
                                            , CUSTOMER_NAME
                                            , CONTRACT_START_DATE_S CONTRACT_START_DATE
                                            , CONTRACT_END_DATE_S CONTRACT_END_DATE
                                        from V_MULTIPLE_BILLING_HEADER
                                        where " +
                                "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(RO_ADDRESS) LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') " +
                                "ORDER BY CONTRACT_START_DATE DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<MULTIPLE_BILLING_HEADER>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"select 
                                            BE_ID
                                            , PROFIT_CENTER
                                            , CONTRACT_NO
                                            , CONTRACT_NAME
                                            , RO_ADDRESS
                                            , TOTAL_AMOUNT
                                            , POSTED_AMOUNT
                                            , UNPOSTED_AMOUNT
                                            , CUSTOMER_NAME
                                            , CONTRACT_START_DATE_S CONTRACT_START_DATE
                                            , CONTRACT_END_DATE_S CONTRACT_END_DATE
                                        from V_MULTIPLE_BILLING_HEADER
                                        where BRANCH_ID = :c ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<MULTIPLE_BILLING_HEADER>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"select 
                                            BE_ID
                                            , PROFIT_CENTER
                                            , CONTRACT_NO
                                            , CONTRACT_NAME
                                            , TOTAL_AMOUNT
                                            , POSTED_AMOUNT
                                            , UNPOSTED_AMOUNT
                                            , CUSTOMER_NAME
                                            , CONTRACT_START_DATE_S CONTRACT_START_DATE
                                            , CONTRACT_END_DATE_S CONTRACT_END_DATE
                                        from V_MULTIPLE_BILLING_HEADER
                                        where BRANCH_ID = :d and
                                (CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%')
                                ORDER BY CONTRACT_START_DATE DESC";
                            //where BRANCH_ID = :d and 
                                //(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%')
                                //ORDER BY CONTRACT_START_DATE DESC

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<MULTIPLE_BILLING_HEADER>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- NEW GET DATA ---------------------
        public IDataTable GetDatanew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<MULTIPLE_BILLING_HEADER> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND (CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CONTRACT_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            
                    try
                    {
                        string sql = @"select
                                            BE_ID
                                            , PROFIT_CENTER
                                            , CONTRACT_NO
                                            , CONTRACT_NAME
                                            , RO_ADDRESS
                                            , TOTAL_AMOUNT
                                            , POSTED_AMOUNT
                                            , UNPOSTED_AMOUNT
                                            , CUSTOMER_NAME
                                            , CONTRACT_START_DATE_S CONTRACT_START_DATE
                                            , CONTRACT_END_DATE_S CONTRACT_END_DATE
                                            , CONTRACT_OFFER_NO
                                            , TOTAL_AMOUNT_CO
                                        from V_MULTIPLE_BILLING_HEADER where BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<MULTIPLE_BILLING_HEADER>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                    }
                    catch (Exception ex)
                    {

                    }
                
            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE HEADER DATA ---------------------
        public IDataTable GetDataDetail(int contract, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<MULTIPLE_BILLING_DETAIL> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            
            if (search.Length == 0)
            {
                try
                {
                    string sql = @"select
                                        CONTRACT_NO
                                        , CONDITION_TYPE
                                        , TOTAL_AMOUNT NET_VALUE
                                        , DUE_DATE_S DUE_DATE
                                        , POSTING_DATE_S POSTING_DATE
                                        , PAID_DATE_S PAID_DATE
                                        , SAP_DOC_NO
                                        , MANUAL_NO
                                        , BILLING_NO
                                        , STATUS
                                    from V_MULTIPLE_BILLING_DETAIL where CONTRACT_NO = :c";

                    fullSql = fullSql.Replace("sql", sql);

                    listData = connection.Query<MULTIPLE_BILLING_DETAIL>(fullSql, new { a = start, b = end, c = contract });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = contract });
                } catch (Exception) { }
            } else { }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();

            connection.Dispose();
            return result;
        }
    }
}