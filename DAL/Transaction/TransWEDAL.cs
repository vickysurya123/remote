﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransWE;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;
using Remote.Entities;

namespace Remote.DAL
{
    public class TransWEDAL
    {
        //------------------------- DEPRECATED ------------------------------//
        public DataTablesTransWE GetDataTransWE(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_INSTALLATION_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, CUSTOMER_ID, CUSTOMER_NAME, " +
                                     "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_CODE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER " +
                                     "FROM V_TRANS_WE WHERE STATUS ='1' ORDER BY INSTALLATION_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data

                    if (search.Length > 2)
                    {
                        try 
                        { 
                        /*
                        string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME " +
                                     "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                     "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%'))";
                        */
                        string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, CUSTOMER_ID, CUSTOMER_NAME, " +
                                 "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_CODE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER " +
                                 "FROM V_TRANS_WE WHERE STATUS ='1' AND ((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR ((POWER_CAPACITY) LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (CUSTOMER_ID LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%')) ORDER BY INSTALLATION_NUMBER DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                    string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, CUSTOMER_ID, CUSTOMER_NAME, " +
                                 "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_CODE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER " +
                                 "FROM V_TRANS_WE WHERE STATUS=1 AND BRANCH_ID=:c ORDER BY INSTALLATION_NUMBER DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data

                    if (search.Length > 2)
                    {
                        try 
                        {
                        /*
                        string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME " +
                                     "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                     "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%'))";
                        */
                        string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, CUSTOMER_ID, CUSTOMER_NAME, " +
                                 "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_CODE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER " +
                                 "FROM V_TRANS_WE WHERE STATUS=1 AND BRANCH_ID=:d AND ((INSTALLATION_NUMBER LIKE '%' ||:c|| '%') OR ((POWER_CAPACITY) LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (CUSTOMER_ID LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%')) ORDER BY INSTALLATION_NUMBER DESC";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }

            result = new DataTablesTransWE();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public AUP_TRANS_WE_WEBAPPS GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //string sql = "SELECT " + 
            //                    "i.ID, i.INSTALLATION_TYPE, " +
            //                    "CUSTOMER_ID, " +
            //                    "CUSTOMER_NAME, " + 
            //                    "AMOUNT, " +
            //                    "X_FACTOR, " +
            //                    "UNIT, " +
            //                    "INSTALLATION_NUMBER, " +
            //                    "TERMINAL_NAME, " +
            //                    "r.PROFIT_CENTER_ID AS PROFIT_CENTER, " +
            //                    "(SELECT MAX(METER_TO) FROM PROP_SERVICES_TRANSACTION WHERE ID_INSTALASI = :a) METER_FROM " +
            //                "FROM " +
            //                    "PROP_SERVICES_INSTALLATION i, " +
            //                    "SERVICES s, " +
            //                    "RENTAL_OBJECT r, " +
            //                    "PROFIT_CENTER p " +
            //                "WHERE " +
            //                    "s.TARIFF_CODE = i.TARIFF_CODE AND " +
            //                    "i.RO_CODE = r.RO_CODE AND " +
            //                    "r.PROFIT_CENTER_ID = p.PROFIT_CENTER_ID AND i.ID = :a";

            string sql = @"SELECT i.ID, i.INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_ID, CUSTOMER_NAME, AMOUNT, X_FACTOR, UNIT, INSTALLATION_NUMBER, TERMINAL_NAME, 
                          i.PROFIT_CENTER_NEW AS PROFIT_CENTER, i.MINIMUM_AMOUNT, (SELECT MAX(METER_TO) FROM PROP_SERVICES_TRANSACTION WHERE ID_INSTALASI = :a) METER_FROM, s.CURRENCY 
                          FROM 
                          PROP_SERVICES_INSTALLATION i, SERVICES s, PROFIT_CENTER p  WHERE s.TARIFF_CODE = i.TARIFF_CODE AND  i.PROFIT_CENTER_NEW= p.PROFIT_CENTER_ID AND i.ID = :a AND P.STATUS = '1' ";

            AUP_TRANS_WE_WEBAPPS result = connection.Query<AUP_TRANS_WE_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //---------INSERT DATA TRANSAKSI OTHER SERVICES
        public DataReturnWENumber Add(string name, DataTransWE data, string KodeCabang)
        {
            string id_we = "";
            string billing_type = "";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string trans_code = "";


            if (data.INSTALLATION_TYPE == "A-Sambungan Air"){         
                trans_code = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CODE_A AS TRANS_CODE_A FROM DUAL");
                billing_type = "ZM01";
            }
            else
            {
                trans_code = connection.ExecuteScalar<string>("SELECT GEN_TRANS_CODE_L AS TRANS_CODE_L FROM DUAL");
                billing_type = "ZM02";
            }

            string sql = "INSERT INTO PROP_SERVICES_TRANSACTION " +
                "(ID, ID_INSTALASI, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, PERIOD, METER_FROM, METER_TO, PRICE, AMOUNT, QUANTITY, UNIT, INSTALLATION_NUMBER, BILLING_TYPE, BRANCH_ID, INSTALLATION_CODE, SURCHARGE, SUB_TOTAL, CUSTOMER, CREATED_DATE, CREATED_BY, COA_PROD, STATUS_DINAS, GRAND_TOTAL, RATE) " +
                "VALUES (:p, :a, :b, :c, :d, :e, :f, :g, :h, :j, :k, :l, :n, :o, :q, :s, :t, :u, :v, sysdate, :w, :x, :z, :total, :yy)";


            int r = connection.Execute(sql, new
            {
                a = Convert.ToInt64(data.ID_INSTALASI),
                b = data.INSTALLATION_TYPE,
                c = data.PROFIT_CENTER,
                d = data.CUSTOMER_NAME,
                e = data.PERIOD,
                f = Convert.ToInt64(data.METER_FROM),
                g = Convert.ToInt64(data.METER_TO),
                h = Convert.ToInt64(data.PRICE),
                //i = data.MULTIPLY_FACTOR,
                j = Convert.ToInt64(data.AMOUNT),
                k = Convert.ToInt64(data.QUANTITY),
                l = data.UNIT,
                n = data.INSTALLATION_NUMBER,
                o = billing_type,
                p = trans_code,
                q = KodeCabang,
                s = data.INSTALLATION_CODE,
                t = Convert.ToInt64(data.SURCHARGE),
                u = Convert.ToInt64(data.SUB_TOTAL),
                v = data.CUSTOMER,
                w = name,
                x = data.COA_PROD,
                z = data.STATUS_DINAS,
                total = Convert.ToInt64(data.GRAND_TOTAL),
                yy = data.RATE,

            });

            //Query last id dan return value nya untuk step selanjutnya
            //id_we = connection.ExecuteScalar<string>("SELECT MAX(ID) FROM PROP_SERVICES_TRANSACTION");

            var resultfull = new DataReturnWENumber();
            resultfull.WE_NUMBER = trans_code;
            resultfull.RESULT_STAT = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        public IEnumerable<AUP_TRANS_WE_WEBAPPS> getLastPeriod(string id, string id_periode, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT PERIOD FROM PROP_SERVICES_TRANSACTION WHERE ID_INSTALASI=:a AND PERIOD=:b AND BRANCH_ID=:c AND CANCEL_STATUS IS NULL";

                listData = connection.Query<AUP_TRANS_WE_WEBAPPS>(sql, new
                {
                    a = id,
                    b = id_periode,
                    c = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }


        //------------------------- DATA TABLE TRANSWE ------------------------------//
        public DataTablesTransWE GetDataTransWEnew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_INSTALLATION_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                /*
                string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME " +
                             "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                             "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%'))";
                */
                string sql = "SELECT ID, INSTALLATION_NUMBER, INSTALLATION_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, CUSTOMER_ID, CUSTOMER_NAME, " +
                         "INSTALLATION_DATE, INSTALLATION_ADDRESS, TARIFF_CODE || ' - ' || CURRENCY_1 || '  ' ||AMOUNT AS TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, RO_CODE || ' - ' || RO_NAME AS RO_CODE, STATUS, POWER_CAPACITY, INSTALLATION_CODE, PROFIT_CENTER_NEW || ' - ' || TERMINAL_NAME AS PROFIT_CENTER " +
                         "FROM V_TRANS_WE WHERE STATUS=1 "+ v_be + wheresearch + " ORDER BY INSTALLATION_NUMBER DESC";
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    c = search.ToUpper(),
                    d = KodeCabang
                });
            }
            catch (Exception)
            {

            }

            result = new DataTablesTransWE();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}