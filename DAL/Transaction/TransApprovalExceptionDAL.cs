﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
using Remote.Models.TransContractOffer;
using Remote.Models.TransRR;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Microsoft.SqlServer.Server;
using Remote.ViewModels;
using Remote.Models;
using Remote.Models.ApprovalSetting;
using Remote.Models.GeneralResult;
using Remote.Models.UserModels;
using Remote.Models.EmailConfiguration;
using Remote.Models.PendingList;
using RestSharp;
using Newtonsoft.Json;
using Remote.Helpers;
using Remote.Controllers;
using Remote.Models.ListNotifikasi;
using RestSharp.Authenticators;

namespace Remote.DAL
{
    public class TransApprovalExceptionDAL
    {
        
        //------------------------------ DATA TABLE CONTRACT OFFER OBJECT ----------------------
        public DataTablesTransContractOffer GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }


        //------------------------------ DATA TABLE DETAIL SURAT ----------------------
        public DataTablesTransContractOffer GetDataDetailSurat(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CASE WHEN STATUS_APV = 1 THEN NO_SURAT ELSE ' - ' END NO_SURAT, SUBJECT, ID, DIRECTORY, TIME_CREATE FROM PROP_SURAT WHERE CONTRACT_NO = :c ORDER  BY TIME_CREATE ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }


        //------------------------------ DATA TABLE CONTRACT OFFER CONDITION ----------------------
        public DataTablesTransContractOffer GetDataDetailCondition(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, COA_PROD " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesTransContractOffer GetDataDetailManualy(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION || ' - ' || REF_DESC AS CONDITION, to_char(DUE_DATE,'DD.MM.YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT, KODE_BAYAR " +
                            "FROM V_CONTRACT_OFFER_MANUAL " +
                            "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MEMO ----------------------
        public DataTablesTransContractOffer GetDataDetailMemo(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, MEMO " +
                             "FROM V_CONTRACT_OFFER_MEMO " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        
        //------------------------------ DATA TABLE CONTRACT OFFER REPORTING RULE ----------------------
        public DataTablesTransContractOffer GetDataDetailReportingRule(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT * FROM PROP_CONTRACT_OFFER_RR WHERE CONTRACT_OFFER_NO=:c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //--------------------------------------- DEPRECATED ----------------------
        public DataTablesTransContractOffer GetDataHistoryWorkflow(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c AND BRANCH_ID = :d ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close(); 
            connection.Dispose();

            return result;
        }

        //----------------------------------------------AUTOCOMPLETE REQUEST NAME RUNNING ------------------------------------
        public List<DDRentalRequest> DDRequestName(string REQUEST_NAME)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            /*
            string str = "SELECT a.ID, a.SERVICE_CODE, a.SERVICE_NAME, a.UNIT, a.ACTIVE, b.UNIT AS CURRENCY, b.PRICE, b.MULTIPLY_FUNCTION, b.VB_ID, b.ACTIVE " +
                            "FROM VARIOUS_BUSINESS a, VARIOUS_BISINESS_PRICING b " +
                            "WHERE a.ID = b.VB_ID(+) AND UPPER(a.SERVICE_NAME) LIKE '%'|| UPPER(:SERVICE_NAME)||'%' AND a.ACTIVE = '1' AND b.ACTIVE ='1' ";
            */
            string str = "SELECT RENTAL_REQUEST_NO as code, RENTAL_REQUEST_NAME as label FROM PROP_RENTAL_REQUEST WHERE UPPER(RENTAL_REQUEST_NAME) LIKE '%'||UPPER(:REQUEST_NAME)||'%'";
            List<DDRentalRequest> exec = connection.Query<DDRentalRequest>(str, new
            {
                REQUEST_NAME = REQUEST_NAME
            }).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }

        //--------------------------------------DROPDOWN CONTRACT OFFER------------------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            string sql = "";
            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY ORDER BY BE_NAME";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a AND STATUS = 1 ORDER BY BE_NAME";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;

        }

        //--------------------------------------DROPDOWN PROFIT CENTER ------------------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //--------------------------------------DROPDOWN CONTRACT USAGE ------------------------------------
        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            string sql = "SELECT ID, ID AS USAGE_TYPE_ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONTRACT_USAGE'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listDetil = connection.Query<DDContractUsage>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------ ---------------------GET DATA FILTER RENTAL DETAIL ----------------------
        public DataTablesTransRR GetDataTransRentalRequest(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransRR result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_RENTAL_REQUEST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "RENTAL_REQUEST_ID, BE_ID AS ID_BE, BE_NAME, " +
                                 "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, CONTRACT_USAGE_NAME, CONTRACT_USAGE_CODE, PROFIT_CENTER_ID, TERMINAL_NAME, F_PROFIT_CENTER " +
                                 "FROM V_TRANSRENTAL_REQ WHERE STATUS='APPROVED' AND ACTIVE='1' AND CONTRACT_OFFER_NUMBER IS NULL";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT DISTINCT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                 "RENTAL_REQUEST_ID, BE_ID AS ID_BE, BE_NAME, " +
                                 "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, CONTRACT_USAGE_NAME, CONTRACT_USAGE_CODE, PROFIT_CENTER_ID, TERMINAL_NAME, F_PROFIT_CENTER " +
                                 "FROM V_TRANSRENTAL_REQ WHERE STATUS='APPROVED' AND ACTIVE='1' AND (RENTAL_REQUEST_NO || UPPER(RENTAL_REQUEST_TYPE) || UPPER(BE_ID) || UPPER(BE_NAME) || UPPER(RENTAL_REQUEST_NAME) || UPPER(CUSTOMER_NAME) LIKE '%'||:c||'%' ) " +
                                 "AND CONTRACT_OFFER_NUMBER IS NULL";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                                 "RENTAL_REQUEST_ID, BE_ID AS ID_BE, BE_NAME, " +
                                 "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, CONTRACT_USAGE_NAME, CONTRACT_USAGE_CODE, PROFIT_CENTER_ID, TERMINAL_NAME, F_PROFIT_CENTER " +
                                 "FROM V_TRANSRENTAL_REQ WHERE STATUS='APPROVED' AND BRANCH_ID = :c AND ACTIVE='1' AND CONTRACT_OFFER_NUMBER IS NULL";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT DISTINCT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                 "RENTAL_REQUEST_ID, BE_ID AS ID_BE, BE_NAME, " +
                                 "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, CONTRACT_USAGE_NAME, CONTRACT_USAGE_CODE, PROFIT_CENTER_ID, TERMINAL_NAME, F_PROFIT_CENTER " +
                                 "FROM V_TRANSRENTAL_REQ WHERE STATUS='APPROVED' AND ACTIVE='1' AND (RENTAL_REQUEST_NO || UPPER(RENTAL_REQUEST_TYPE) || UPPER(BE_ID) || UPPER(BE_NAME) || UPPER(RENTAL_REQUEST_NAME) || UPPER(CUSTOMER_NAME) LIKE '%'||:c||'%' ) " +
                                 "AND BRANCH_ID = :d AND CONTRACT_OFFER_NUMBER IS NULL";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransRR();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //----------------------------------GET DATA RO---------------------- 
        public DataTablesRO GetDataRO(int draw, int start, int length, string search, string idRO)
        {
            int count = 0;
            int end = start + length;
            DataTablesRO result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CO_RO_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT RENTAL_REQUEST_NO, RO_NUMBER, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, OBJECT_ID, RO_CODE, RO_NAME, " +
                                 "OBJECT_TYPE_ID, to_char(CONTRACT_START_DATE, 'DD/MM/YYYY') AS CONTRACT_START_DATE, to_char(CONTRACT_END_DATE, 'DD/MM/YYYY') AS CONTRACT_END_DATE, ACTIVE, OBJECT_TYPE FROM V_RO_CONTRACT_OFFER " +
                                 "WHERE RENTAL_REQUEST_NO=:c AND ACTIVE=1";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CO_RO_Remote>(fullSql, new { a = start, b = end, c = idRO });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idRO });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesRO();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //---------------------------------- FILTER DATA DETAIL ---------------------
        public DataTablesGetRO GetDataFilterRentalObject(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            DataTablesGetRO result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<V_SEARCH_DETAIL_RENTAL> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT RO_NUMBER, OBJECT_ID, OBJECT_TYPE, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                    {
                        a = start,
                        b = end
                    });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount);
                }
                catch (Exception)
                {

                }
            }
            else
            {
                //search filter data
                if (search.Length > 2)
                {
                    try
                    {
                        string sql = "SELECT RO_NUMBER, OBJECT_ID, OBJECT_TYPE, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL WHERE " +
                                "(UPPER(OBJECT_ID) || UPPER(RO_NAME) || UPPER(OBJECT_TYPE) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%')";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            result = new DataTablesGetRO();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-----------------------------GET DATA UNTUK DROPDOWN RO------------------
        public IEnumerable<DDRo> GetDataRO(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRo> listData = null;

            try
            {
                string sql = "SELECT * FROM V_RO_CONTRACT_OFFER WHERE RENTAL_REQUEST_NO=:c";

                listData = connection.Query<DDRo>(sql, new
                {
                    c = id
                });

                // listData = connection.Query<DDRo>(sql).ToList();
            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //-----------------------------GET DATA UNTUK DROPDOWN RO------------------
        public IEnumerable<CO_RO_OBJECT> GetDataObject(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CO_RO_OBJECT> listData = null;

            try
            {
                string sql = "SELECT RENTAL_REQUEST_NO, RO_NUMBER, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, OBJECT_ID, RO_CODE, RO_NAME, " +
                             "OBJECT_TYPE_ID, to_char(CONTRACT_START_DATE, 'DD/MM/YYYY') AS CONTRACT_START_DATE, to_char(CONTRACT_END_DATE, 'DD/MM/YYYY') AS CONTRACT_END_DATE, ACTIVE, OBJECT_TYPE FROM V_RO_CONTRACT_OFFER " +
                             "WHERE RENTAL_REQUEST_NO=:c AND ACTIVE=1";

                listData = connection.Query<CO_RO_OBJECT>(sql, new
                {
                    c = id
                });

            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //----------------------------------GET DATA UNTUK CHECKBOX CONDITION TYPE-------------------
        public IEnumerable<CBConditionType> GetDataCheckboxConditonType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<CBConditionType> listData = null;

            try
            {
                string sql = "SELECT * FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONDITION_TYPE' AND REF_DESC != 'AIR' AND REF_DESC != 'LISTRIK' AND ACTIVE=1 ORDER BY REF_DATA ASC";

                listData = connection.Query<CBConditionType>(sql);

                // listData = connection.Query<DDRo>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //-----------------------------GET DATA UNTUK DROPDOWN MEAS_TYPE-----------------
        public IEnumerable<DDMeasType> GetDataMeasType(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDMeasType> listData = null;

            try
            {
                string sql = "SELECT * FROM V_MEAS_TYPE_CO WHERE RO_NUMBER=:c";

                listData = connection.Query<DDMeasType>(sql, new
                {
                    c = id
                });

                // listData = connection.Query<DDRo>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //-------------------------------------GET DATA LUAS MEAS_TYPE-------------------
        public IEnumerable<DDMeasType> GetDataLuasMeasType(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDMeasType> listData = null;

            try
            {
                string sql = "SELECT * FROM V_MEAS_TYPE_CO WHERE ID_DETAIL=:c";

                listData = connection.Query<DDMeasType>(sql, new
                {
                    c = id
                });

                // listData = connection.Query<DDRo>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //------------------------- INSERT DATA HEADER ---------------------
        public DataReturnTransNumber AddHeader(string name, DataTransContractOffer data, string KodeCabang, string Currency)
        {
            //int r = 0;
            //string id_trans = "";
            string id_trans_vb = "";
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            var resultfull = new DataReturnTransNumber();
            // IDbConnection connection = DatabaseFactory.GetConnection();
            string _param1 = string.Empty;
            if (data.CONTRACT_OFFER_TYPE == "ZC01")
            {
                //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_CO_ZC01 AS TRANS_CODE FROM DUAL");
                _param1 = "21";
            }
            else if (data.CONTRACT_OFFER_TYPE == "ZC02")
            {
                //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_CO_ZC02 AS TRANS_CODE FROM DUAL");
                _param1 = "22";
            }
            else if (data.CONTRACT_OFFER_TYPE == "ZC03")
            {
                //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_CO_ZC03 AS TRANS_CODE FROM DUAL");
                _param1 = "23";
            }
            else
            {
                //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_CO_ZC04 AS TRANS_CODE FROM DUAL");
                _param1 = "24";
            }
            string _BE_ID = "1";
            id_trans_vb = DatabaseHelper.GetPenomoranData(_BE_ID, "CONTRACT_OFFER", _param1, 6);

            //id_trans_vb = GenerateContractOfferNumber(data, KodeCabang, RETURN_VALUE_BUFFER_SIZE, _param1);
            if (!String.IsNullOrEmpty(id_trans_vb))
            {
                resultfull = SetDataContractOffer(data, KodeCabang, id_trans_vb, name, Currency);
            }
            else
            {
                resultfull.ID_TRANS = string.Empty;
                resultfull.RESULT_STAT = false;
                resultfull.MessageResult = "Contract Offer Number not found.";
            }
            return resultfull;
        }


        private DataReturnTransNumber SetDataContractOffer(DataTransContractOffer data, string kodeCabang, string contractOfferNumber, string updatedBy, string Currency)
        {
            int r = 0;
            DataReturnTransNumber ret = new DataReturnTransNumber();
            RENTAL_OBJECT ro = new RENTAL_OBJECT();
            ret.ID_TRANS = contractOfferNumber;
            ret.MessageResult = string.Empty;
            string sql = string.Empty, sql1 = string.Empty, sql2 = string.Empty, sql3 = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                try
                {
                    dynamic rate = "1";
                    if (Currency == "USD")
                    {
                        var rates = GetDataCurrencyRate();
                        if (rates.Status == "S")
                        {
                            rate = rates.Data.ToString();
                        }
                    }

                    var CurrencyRate = @"SELECT AB.MULTIPLY_RATE FROM CURRENCY_RATE AB
                                        JOIN (SELECT AB.CURRENCY_FROM, MAX(AB.VALIDITY_FROM) AS VALIDITY_FROM
                                        FROM CURRENCY_RATE AB GROUP BY AB.CURRENCY_FROM) X ON X.CURRENCY_FROM = AB.CURRENCY_FROM AND X.VALIDITY_FROM = AB.VALIDITY_FROM
                                        JOIN CURRENCY A ON A.ID = AB.CURRENCY_FROM WHERE A.CODE LIKE '%' ||:c|| '%'";
                    var Currencys = connection.Query<string>(CurrencyRate, new { c = Currency }).FirstOrDefault();

                    sql = "INSERT INTO PROP_CONTRACT_OFFER " +
                          "(CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, CONTRACT_START_DATE, " +
                          "CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, ACTIVE, BRANCH_ID, CREATION_BY, PARAF_LEVEL, CURRENCY_RATE) " +
                          "VALUES (:a, :b, :c, :d, to_date(:e,'DD/MM/YYYY'), " +
                          "to_date(:f,'DD/MM/YYYY'), :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r)";
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = contractOfferNumber,
                            b = data.BE_ID,
                            c = data.RENTAL_REQUEST_NO,
                            d = data.CONTRACT_OFFER_NAME,
                            e = data.CONTRACT_START_DATE,
                            f = data.CONTRACT_END_DATE,
                            g = data.TERM_IN_MONTHS,
                            h = data.CUSTOMER_AR,
                            i = data.PROFIT_CENTER,
                            j = data.CONTRACT_USAGE,
                            k = data.CURRENCY,
                            l = data.CONTRACT_OFFER_TYPE,
                            m = "10",
                            n = "1",
                            o = kodeCabang,
                            p = updatedBy,
                            q = -1,
                            // r = Currencys
                            r = rate
                        });
                        r = 1;
                        ret.RESULT_STAT = true;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }

                    if (!string.IsNullOrEmpty(data.ANJUNGAN_ID))
                    {
                        try
                        {
                            MonitoringAnjunganDAL dalM = new MonitoringAnjunganDAL();
                            r = dalM.SetStatusContractOffer("WAITING FOR APPROVAL", data.ANJUNGAN_ID, contractOfferNumber);
                        }
                        catch (Exception)
                        {
                            r = 0;
                            throw;
                        }
                    }

                    //Update rental request
                    sql2 = "UPDATE PROP_RENTAL_REQUEST SET CONTRACT_OFFER_NUMBER=:a WHERE RENTAL_REQUEST_NO=:b";
                    try
                    {
                        int s = connection.Execute(sql2, new
                        {
                            a = contractOfferNumber,
                            b = data.RENTAL_REQUEST_NO
                        });
                        r = 1;
                        ret.RESULT_STAT = true;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }

                    // insert contract object
                    if (data.Objects != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_OFFER_OBJECT " +
                              "(CONTRACT_OFFER_NO, OBJECT_TYPE, OBJECT_NAME, START_DATE, END_DATE, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY, OBJECT_ID) " +
                              "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";


                        foreach (var i in data.Objects)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractOfferNumber,
                                    b = i.OBJECT_TYPE,
                                    c = i.RO_NAME,
                                    d = i.START_DATE,
                                    e = i.END_DATE,
                                    f = i.LUAS_TANAH,
                                    g = i.LUAS_BANGUNAN,
                                    h = i.INDUSTRY,
                                    i = i.OBJECT_ID
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                            try
                            {
                                //data RO
                                sql = @"select RO_NUMBER,VALID_FROM, VALID_TO from RENTAL_OBJECT WHERE RO_CODE =:a";
                                ro = connection.Query<RENTAL_OBJECT>(sql, new { a = i.OBJECT_ID }).FirstOrDefault();
                                //UPDATE STATUS BOOKED DI PROP_RO_OCCUPANCY
                                sql1 = "UPDATE PROP_RO_OCCUPANCY SET CONTRACT_OFFER_NO = :b, VALID_TO= to_date(:c,'DD/MM/YYYY') WHERE RO_CODE = :a AND STATUS = 'BOOKED' ";

                                r = connection.Execute(sql1, new
                                {
                                    a = i.OBJECT_ID,
                                    b = contractOfferNumber,
                                    c = data.CONTRACT_END_DATE
                                });

                                sql1 = @"insert into PROP_RO_OCCUPANCY(RO_NUMBER,STATUS,REASON,VALID_FROM,VALID_TO,ACTIVE_STATUS,CREATION_DATE,RO_CODE)
                                        VALUES (:a,:b,:c,to_date(:d,'DD/MM/YYYY') + 1,:e,:f,:g,:h)";

                                r = connection.Execute(sql1, new
                                {
                                    a = ro.RO_NUMBER,
                                    b = "VACANT",
                                    c = "No Reason",
                                    d = data.CONTRACT_END_DATE,
                                    e = ro.VALID_TO,
                                    f = 1,
                                    g = DateTime.Now,
                                    h = i.OBJECT_ID,
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                            //try
                            //{
                            //    //UPDATE STATUS BOOKED DI PROP_RO_OCCUPANCY
                            //    sql3 = "INSERT INTO PROP_RO_BOOKED " +
                            //           "(CONTRACT_OFFER_NO, RO_NUMBER, CREATION_BY) " +
                            //           "VALUES (:a, :b, :c) ";
                            //    r = connection.Execute(sql1, new
                            //    {
                            //        a = i.OBJECT_ID,
                            //        b = contractOfferNumber
                            //    });
                            //    r = 1;
                            //    ret.RESULT_STAT = true;
                            //}
                            //catch (Exception)
                            //{
                            //    r = 0;
                            //    throw;
                            //}
                        }
                    }
                    //AddDetailCondition(name, data.Conditions);
                    if (data.Conditions != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_OFFER_CONDITION " +
                              "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
                              "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_OFFER_NO, TAX_CODE, INSTALLMENT_AMOUNT, COA_PROD) " +
                              "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t, :u, :v, :w)";
                        // var condition = data.Conditions;
                        foreach (var i in data.Conditions)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = i.CALC_OBJECT,
                                    b = i.CONDITION_TYPE,
                                    c = i.VALID_FROM,
                                    d = i.VALID_TO,
                                    e = i.MONTHS,
                                    f = i.STATISTIC,
                                    g = i.UNIT_PRICE,
                                    h = i.AMT_REF,
                                    i = i.FREQUENCY,
                                    j = i.START_DUE_DATE,
                                    k = i.MANUAL_NO,
                                    l = i.FORMULA,
                                    m = i.MEASUREMENT_TYPE,
                                    n = i.LUAS,
                                    o = i.TOTAL,
                                    p = i.NJOP_PERCENT,
                                    q = i.KONDISI_TEKNIS_PERCENT,
                                    z = string.Empty,
                                    s = i.TOTAL_NET_VALUE,
                                    t = contractOfferNumber,
                                    u = i.TAX_CODE,
                                    v = i.INSTALLMENT_AMOUNT,
                                    w = i.COA_PROD
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                        }
                    }
                    sql = "INSERT INTO PROP_CONTRACT_OFFER_FREQUENCY " +
                          "(CONTRACT_OFFER_NO, MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE, OBJECT_ID, QUANTITY, UNIT) " +
                          "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h)";
                    if (data.Frequencies != null)
                    {
                        foreach (var i in data.Frequencies)
                        {
                            decimal qtyDecimal = decimal.Parse(i.QUANTITY);
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractOfferNumber,
                                    b = i.MANUAL_NO,
                                    c = i.CONDITION,
                                    d = i.DUE_DATE,
                                    e = i.NET_VALUE,
                                    f = i.OBJECT_ID,
                                    g = qtyDecimal,
                                    h = i.UNIT
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }
                    if (data.Memos != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_OFFER_MEMO " +
                              "(CONTRACT_OFFER_NO, CALC_OBJECT, CONDITION_TYPE, MEMO) " +
                              "VALUES (:a, :b, :c, :d)";
                        foreach (var i in data.Memos)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractOfferNumber,
                                    b = i.CALC_OBJECT,
                                    c = i.CONDITION_TYPE,
                                    d = i.MEMO
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    r = 0;
                    ret.RESULT_STAT = false;
                    ret.MessageResult = ex.Message;
                }
            }

            return ret;
        }

        //----------------------------------- RELEASE TO WORKFLOW -------------------------
        public ResultInformation ReleaseToWorkflow(string updateBy, string userId, string data, string kodeCabang)
        {
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            ResultInformation ret = new ResultInformation();
            // string ret = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                OracleCommand cmd = connection.CreateCommand();
                cmd.CommandText = "PROC_PROP_RUN_WORKFLOW";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;
                cmd.Parameters.Add("returnVal", OracleDbType.Varchar2, RETURN_VALUE_BUFFER_SIZE);
                cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                OracleParameter pContractOfferNo = new OracleParameter("p_contract_offer_no", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = data,
                    Size = 100
                };
                cmd.Parameters.Add(pContractOfferNo);

                OracleParameter pUserId = new OracleParameter("P_USER_LOGIN", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = userId,
                    Size = 100
                };
                cmd.Parameters.Add(pUserId);
                OracleParameter plevelApprove = new OracleParameter("p_level_approve", OracleDbType.Varchar2,
                   ParameterDirection.Input)
                {
                    Value = "20",
                    Size = 100
                };
                cmd.Parameters.Add(plevelApprove);
                OracleParameter pApproveType = new OracleParameter("p_approve_type", OracleDbType.Varchar2,
                   ParameterDirection.Input)
                {
                    Value = "2",
                    Size = 100
                };
                cmd.Parameters.Add(pApproveType);
                OracleParameter pNote = new OracleParameter("p_notes", OracleDbType.Clob,
                   ParameterDirection.Input)
                {
                    Value = ""
                };
                cmd.Parameters.Add(pNote);
                OracleParameter pUpdateBy = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = updateBy,
                    Size = 100
                };
                //cmd.Parameters.Add(pUpdateBy);
                //OracleParameter pOut = new OracleParameter("pContractNo", OracleDbType.Varchar2,
                //        ParameterDirection.Output)
                //{ Size = 50 };
                //cmd.Parameters.Add(pOut);
                try
                {
                    cmd.ExecuteNonQuery();
                    ret.TransactionCode = cmd.Parameters["returnVal"].Value.ToString();
                    ret.ResultCode = "S";
                    ret.ResultDescription = "Success";
                }
                catch (Exception ex)
                {
                    ret.TransactionCode = String.Empty;
                    ret.ResultCode = "E";
                    ret.ResultDescription = "Error: " + ex.Message;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                    cmd.Dispose();
                }
            }
            return ret;
        }

        //----------------------------- Get Data For Edit Contract Offer --------------------------
        public AUP_TRANS_CONTRACT_OFFER_WEBAPPS EditOffer(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            /*
            string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                             "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE_NAME, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                             "FROM V_TRANSRENTAL_REQ WHERE id = :a";
             */

            string sql = @"SELECT ID_INC, RENTAL_REQUEST_ID, CONTRACT_OFFER_NO, BE_NAME, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, FUNC_GENERAL_REF_DESC('CONTRACT_OFFER_STATUS', OFFER_STATUS) OFFER_STATUS, CUSTOMER_ID, CUSTOMER_NAME, 
                            to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER, PROFIT_CENTER_NAME, CONTRACT_USAGE, CONTRACT_USAGE_ID, CONTRACT_USAGE_ID_REF, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_OFFER_TYPE_ID, CONTRACT_OFFER_TYPE_ID_REF, CONTRACT_NUMBER, ACTIVE, OFFER_STATUS, RENTAL_REQUEST_NAME, OLD_CONTRACT, 
                            OLD_CONTRACT_OFFER, OLD_RENTAL_REQUEST, PARAF_LEVEL
                            FROM V_EDIT_CONTRACT_OFFER_HEADER WHERE CONTRACT_OFFER_NO=:a";

            var result = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new { a = id }).FirstOrDefault();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailConditionEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {
                //string sql = "SELECT ID, RENTAL_REQUEST_NO, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, OBJECT_ID, RO_NAME, REF_DATA || ' - ' || REF_DESC AS OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER FROM V_EDIT_RENTAL_REQUEST WHERE RENTAL_REQUEST_NO=:a";
                string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD/MM/YYYY') VALID_FROM, to_char(VALID_TO, 'DD/MM/YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD/MM/YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, TAX_CODE, COA_PROD " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:a ORDER BY ID DESC";

                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            return listData;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailManualFrequencyEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT DISTINCT MANUAL_NO, CONDITION, OBJECT_ID || ' | ' || CONDITION_TYPE AS CONDITION_TYPE, to_char(DUE_DATE,'DD/MM/YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                            "FROM V_CONTRACT_OFFER_MANUAL_EDIT " +
                            "WHERE CONTRACT_OFFER_NO=:a ORDER BY MANUAL_NO ASC";

                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            return listData;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailMemoEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, MEMO " +
                             "FROM V_CONTRACT_OFFER_MEMO " +
                             "WHERE CONTRACT_OFFER_NO=:a";

                listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }

            return listData;
        }

        //----------------------------------- Save Edit Contract Offer --------------------------
        public bool UpdateHeader(string name, DataTransContractOffer data, string KodeCabang, string UpdatedBy, string Currency)
        {
            bool result = false;
            string sql = string.Empty, sql1 = string.Empty, sql2 = string.Empty;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    if (data.TYPE_EDIT == "revise")
                    {
                        sql = @"UPDATE PROP_CONTRACT_OFFER SET
                      BE_ID=:a, RENTAL_REQUEST_NO=:b, CONTRACT_OFFER_NAME=:c, CONTRACT_START_DATE=to_date(:d, 'DD/MM/YYYY'), CONTRACT_END_DATE=to_date(:e, 'DD/MM/YYYY'), TERM_IN_MONTHS=:f,
                      CUSTOMER_AR=:g, PROFIT_CENTER=:h, CONTRACT_USAGE=:i, CURRENCY=:j, CONTRACT_OFFER_TYPE=:k, CURRENCY_RATE=:n, UPDATED_BY =:o
                      WHERE CONTRACT_OFFER_NO=:m";
                    }
                    else
                    {
                        sql = @"UPDATE PROP_CONTRACT_OFFER SET
                         BE_ID=:a, RENTAL_REQUEST_NO=:b, CONTRACT_OFFER_NAME=:c, CONTRACT_START_DATE=to_date(:d, 'DD/MM/YYYY'), CONTRACT_END_DATE=to_date(:e, 'DD/MM/YYYY'), TERM_IN_MONTHS=:f, 
                         CUSTOMER_AR=:g, PROFIT_CENTER=:h, CONTRACT_USAGE=:i, CURRENCY=:j, CONTRACT_OFFER_TYPE=:k, CURRENCY_RATE=:n, UPDATED_BY =:o
                         WHERE CONTRACT_OFFER_NO=:m";

                    }
                    var CurrencyRate = @"SELECT AB.MULTIPLY_RATE FROM CURRENCY_RATE AB
                                        JOIN (SELECT AB.CURRENCY_FROM, MAX(AB.VALIDITY_FROM) AS VALIDITY_FROM
                                        FROM CURRENCY_RATE AB GROUP BY AB.CURRENCY_FROM) X ON X.CURRENCY_FROM = AB.CURRENCY_FROM AND X.VALIDITY_FROM = AB.VALIDITY_FROM
                                        JOIN CURRENCY A ON A.ID = AB.CURRENCY_FROM WHERE A.CODE LIKE '%' ||:c|| '%'";
                    var Currencys = connection.Query<string>(CurrencyRate, new { c = Currency }).FirstOrDefault();

                    int r = connection.Execute(sql, new
                    {
                        a = data.BE_ID,
                        b = data.RENTAL_REQUEST_NO,
                        c = data.CONTRACT_OFFER_NAME,
                        d = data.CONTRACT_START_DATE,
                        e = data.CONTRACT_END_DATE,
                        f = data.TERM_IN_MONTHS,
                        g = data.CUSTOMER_AR,
                        h = data.PROFIT_CENTER,
                        i = data.CONTRACT_USAGE,
                        j = data.CURRENCY,
                        k = data.CONTRACT_OFFER_TYPE,
                        m = data.CONTRACT_OFFER_NO,
                        n = Currencys,
                        o = name,
                    }, transaction: transaction);

                    if (data.Objects != null)
                    {
                        string sqlDel = "DELETE FROM PROP_CONTRACT_OFFER_OBJECT WHERE CONTRACT_OFFER_NO=:a";

                        int x = connection.Execute(sqlDel, new
                        {
                            a = data.CONTRACT_OFFER_NO
                        }, transaction: transaction);

                        sql = "INSERT INTO PROP_CONTRACT_OFFER_OBJECT " +
                                      "(CONTRACT_OFFER_NO, OBJECT_TYPE, OBJECT_NAME, START_DATE, END_DATE, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY, OBJECT_ID) " +
                                      "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";
                        foreach (var i in data.Objects)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = data.CONTRACT_OFFER_NO,
                                    b = i.OBJECT_TYPE,
                                    c = i.RO_NAME,
                                    d = i.START_DATE,
                                    e = i.END_DATE,
                                    f = i.LUAS_TANAH,
                                    g = i.LUAS_BANGUNAN,
                                    h = i.INDUSTRY,
                                    i = i.OBJECT_ID
                                }, transaction: transaction);
                                r = 1;
                                //ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                    //AddDetailCondition(name, data.Conditions);
                    if (data.Conditions != null)
                    {

                        string sqlDel = "DELETE FROM PROP_CONTRACT_OFFER_CONDITION WHERE CONTRACT_OFFER_NO=:a";

                        int x = connection.Execute(sqlDel, new
                        {
                            a = data.CONTRACT_OFFER_NO
                        }, transaction: transaction);

                        sql = "INSERT INTO PROP_CONTRACT_OFFER_CONDITION " +
                              "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
                              "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_OFFER_NO, TAX_CODE, INSTALLMENT_AMOUNT, COA_PROD) " +
                              "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t, :u, :v, :w )";
                        // var condition = data.Conditions;
                        foreach (var i in data.Conditions)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = i.CALC_OBJECT,
                                    b = i.CONDITION_TYPE,
                                    c = i.VALID_FROM,
                                    d = i.VALID_TO,
                                    e = i.MONTHS,
                                    f = i.STATISTIC,
                                    g = i.UNIT_PRICE,
                                    h = i.AMT_REF,
                                    i = i.FREQUENCY,
                                    j = i.START_DUE_DATE,
                                    k = i.MANUAL_NO,
                                    l = i.FORMULA,
                                    m = i.MEASUREMENT_TYPE,
                                    n = i.LUAS,
                                    o = i.TOTAL,
                                    p = i.NJOP_PERCENT,
                                    q = i.KONDISI_TEKNIS_PERCENT,
                                    z = string.Empty,
                                    s = i.TOTAL_NET_VALUE,
                                    t = data.CONTRACT_OFFER_NO,
                                    u = i.TAX_CODE,
                                    v = i.INSTALLMENT_AMOUNT,
                                    W = i.COA_PROD
                                }, transaction: transaction);
                                r = 1;
                                //ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                        }
                    }

                    string sqlDelFrequency = "DELETE FROM PROP_CONTRACT_OFFER_FREQUENCY WHERE CONTRACT_OFFER_NO=:a";

                    int f = connection.Execute(sqlDelFrequency, new
                    {
                        a = data.CONTRACT_OFFER_NO
                    }, transaction: transaction);

                    sql = "INSERT INTO PROP_CONTRACT_OFFER_FREQUENCY " +
                          "(CONTRACT_OFFER_NO, MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE, OBJECT_ID, QUANTITY, UNIT) " +
                          "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h)";
                    if (data.Frequencies != null)
                    {
                        foreach (var i in data.Frequencies)
                        {
                            decimal qtyDecimal = decimal.Parse(i.QUANTITY);
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = data.CONTRACT_OFFER_NO,
                                    b = i.MANUAL_NO,
                                    c = i.CONDITION,
                                    d = i.DUE_DATE,
                                    e = i.NET_VALUE,
                                    f = i.OBJECT_ID,
                                    g = qtyDecimal,
                                    h = i.UNIT
                                }, transaction: transaction);
                                r = 1;
                                //ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                    string sqlDelMemo = "DELETE FROM PROP_CONTRACT_OFFER_MEMO WHERE CONTRACT_OFFER_NO=:a";

                    int m = connection.Execute(sqlDelMemo, new
                    {
                        a = data.CONTRACT_OFFER_NO
                    }, transaction: transaction);
                    if (data.Memos != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_OFFER_MEMO " +
                              "(CONTRACT_OFFER_NO, CALC_OBJECT, CONDITION_TYPE, MEMO) " +
                              "VALUES (:a, :b, :c, :d)";
                        foreach (var i in data.Memos)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = data.CONTRACT_OFFER_NO,
                                    b = i.CALC_OBJECT,
                                    c = i.CONDITION_TYPE,
                                    d = i.MEMO
                                }, transaction: transaction);
                                r = 1;
                                //ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }


                    if (data.Objects != null)
                    {

                        string sqlDelObjects = "DELETE FROM PROP_CONTRACT_OFFER_OBJECT WHERE CONTRACT_OFFER_NO=:a";

                        int o = connection.Execute(sqlDelObjects, new
                        {
                            a = data.CONTRACT_OFFER_NO
                        }, transaction: transaction);


                        try
                        {
                            //UPDATE STATUS BOOKED PADA PROP_RO_OCCUPANCY PADA DATA SEBELUMNYA
                            sql2 = "UPDATE PROP_RO_OCCUPANCY SET STATUS = 'VACANT' , CONTRACT_OFFER_NO = NULL WHERE CONTRACT_OFFER_NO = :b AND STATUS = 'BOOKED' ";

                            r = connection.Execute(sql2, new
                            {
                                b = data.CONTRACT_OFFER_NO
                            }, transaction: transaction);
                            r = 1;
                        }
                        catch (Exception)
                        {
                            r = 0;
                            throw;
                        }

                        sql = "INSERT INTO PROP_CONTRACT_OFFER_OBJECT " +
                              "(CONTRACT_OFFER_NO, OBJECT_TYPE, OBJECT_NAME, START_DATE, END_DATE, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY, OBJECT_ID) " +
                              "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";
                        foreach (var i in data.Objects)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = data.CONTRACT_OFFER_NO,
                                    b = i.OBJECT_TYPE,
                                    c = i.RO_NAME,
                                    d = i.START_DATE,
                                    e = i.END_DATE,
                                    f = i.LUAS_TANAH,
                                    g = i.LUAS_BANGUNAN,
                                    h = i.INDUSTRY,
                                    i = i.OBJECT_ID
                                }, transaction: transaction);
                                r = 1;
                                //ret.RESULT_STAT = true;

                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                            try
                            {
                                //UPDATE STATUS BOOKED DI PROP_RO_OCCUPANCY
                                sql1 = "UPDATE PROP_RO_OCCUPANCY SET STATUS = 'BOOKED', CONTRACT_OFFER_NO = :b WHERE RO_NUMBER = :a AND STATUS = 'VACANT' ";

                                r = connection.Execute(sql1, new
                                {
                                    a = i.OBJECT_ID,
                                    b = data.CONTRACT_OFFER_NO
                                }, transaction: transaction);
                                r = 1;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                    // Update Contract Offer Object START_DATE & END_DATE
                    string sqlUpdOfferObject = "UPDATE PROP_CONTRACT_OFFER_OBJECT SET START_DATE=to_date(:a,'DD/MM/RRRR'), END_DATE=to_date(:b,'DD/MM/RRRR') WHERE CONTRACT_OFFER_NO=:c";
                    try
                    {
                        r = connection.Execute(sqlUpdOfferObject, new
                        {
                            a = data.CONTRACT_START_DATE,
                            b = data.CONTRACT_END_DATE,
                            c = data.CONTRACT_OFFER_NO
                        }, transaction: transaction);
                        r = 1;
                    }
                    catch
                    {

                    }

                    // Update Rental Request START_DATE & END_DATE
                    string sqlUpdRequest = "UPDATE PROP_RENTAL_REQUEST SET CONTRACT_START_DATE=to_date(:a,'DD/MM/RRRR'), " +
                                               "CONTRACT_END_DATE=to_date(:b,'DD/MM/RRRR') WHERE CONTRACT_OFFER_NUMBER=:c";
                    try
                    {
                        r = connection.Execute(sqlUpdRequest, new
                        {
                            a = data.CONTRACT_START_DATE,
                            b = data.CONTRACT_END_DATE,
                            c = data.CONTRACT_OFFER_NO
                        }, transaction: transaction);
                        r = 1;
                    }
                    catch
                    {

                    }
                    transaction.Commit();
                    result = (r > 0) ? true : false;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                    result = false;
                }
                
            }
            /*
             sql = "INSERT INTO PROP_CONTRACT_OFFER " +
                          "(CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, CONTRACT_START_DATE, " +
                          "CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, OFFER_STATUS, ACTIVE, BRANCH_ID) " +
                          "VALUES (:a, :b, :c, :d, to_date(:e,'DD/MM/YYYY'), " +
                          "to_date(:f,'DD/MM/YYYY'), :g, :h, :i, :j, :k, :l, :m, :n, :o)";
             */
            
            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool AddFiles(string sFile, string directory, string KodeCabang, string subPath)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_CONTRACT_OFFER_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, CONTRACT_OFFER_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = subPath
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesAttachmentOffer GetDataAttachment(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesAttachmentOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_OFFER_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT ID_ATTACHMENT, CASE WHEN ID_ATTACHMENT <= 8847 THEN 'https://s3.pelindo.co.id/remote/REMOTE/ContractOffer/' || CONTRACT_OFFER_ID || '/' || FILE_NAME
                    ELSE DIRECTORY END AS DIRECTORY, FILE_NAME, CONTRACT_OFFER_ID FROM PROP_CONTRACT_OFFER_ATTACHMENT WHERE CONTRACT_OFFER_ID=:c ORDER BY ID_ATTACHMENT DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_OFFER_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesAttachmentOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool UpdateStatusContractDAL(DataTransContrOff data)
        {
            bool result = false;
            string XSTS = string.Empty;
            string XMSG = string.Empty;
            string sts = null;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "UPDATE PROP_CONTRACT_OFFER " +
                            "SET ACTIVE=:a " +
                            "WHERE CONTRACT_OFFER_NO=:b";

                r = connection.Execute(sql, new
                {
                    b = data.xID,
                    a = data.xSTATUS
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            try
            {
                var p = new DynamicParameters();
                p.Add("P_CONTRACT_OFFER_NO", data.xID);
                p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);

                connection.Execute("SET_STATUS_BOOKED_OFFER", p, null, null, commandType: CommandType.StoredProcedure);

                sts = p.Get<string>("XSTS");
                var xyz = Convert.ToInt32(sts);

                result = (xyz > 0) ? true : false;
            }
            catch (Exception)
            {
                sts = "0";
            }

            try
            {
                string sql = "UPDATE PROP_RENTAL_REQUEST " +
                            "SET ACTIVE=:a " +
                            "WHERE CONTRACT_OFFER_NUMBER=:b";

                r = connection.Execute(sql, new
                {
                    b = data.xID,
                    a = "0"
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool UpdateStatusApprovalDAL(DataTransContrOff data)
        {
            bool result = false;
            string XSTS = string.Empty;
            string XMSG = string.Empty;
            string sts = null;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "UPDATE PROP_CONTRACT_OFFER " +
                             "SET ACTIVE=:a " +
                             "WHERE CONTRACT_OFFER_NO=:b";

                r = connection.Execute(sql, new
                {
                    b = data.xNO,
                    a = "0"
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            try
            {
                string sql = "UPDATE PROP_RENTAL_REQUEST " +
                             "SET ACTIVE=:a " +
                             "WHERE CONTRACT_OFFER_NUMBER=:b";

                r = connection.Execute(sql, new
                {
                    b = data.xNO,
                    a = "0"
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            try
            {
                var p = new DynamicParameters();
                p.Add("P_CONTRACT_OFFER_NO", data.xNO);
                p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);

                connection.Execute("SET_STATUS_BOOKED_OFFER", p, null, null, commandType: CommandType.StoredProcedure);

                sts = p.Get<string>("XSTS");
                var xyz = Convert.ToInt32(sts);

                result = (xyz > 0) ? true : false;
            }
            catch (Exception)
            {
                sts = "0";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> GetDataDetailObjectP(string CONTRACT_OFFER_NO)
        {
            string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDetil = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new { c = CONTRACT_OFFER_NO });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDRefD> GetDataDropDownCoaProduksi()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                string sql = "SELECT * FROM VW_GET_COA_PROD";

                listData = connection.Query<DDRefD>(sql).ToList();
            }
            catch (Exception) { }

            return listData;
        }

        public IEnumerable<DDRefD> GetDataDropDownCoaProduksiLahan(string search)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                string sql = "SELECT * FROM VW_GET_COA_PROD where val1 like '4070%' and lower(ref_desc) like '%" + search + "%'";

                listData = connection.Query<DDRefD>(sql).ToList();
            }
            catch (Exception) { }

            return listData;
        }

        public IEnumerable<DDRefD> GetDataDropDownCoaProduksiWater()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                string sql = "SELECT * FROM VW_GET_COA_PROD where val1 like '408010%'";

                listData = connection.Query<DDRefD>(sql).ToList();
            }
            catch (Exception) { }

            return listData;
        }
        public IEnumerable<DDRefD> GetDataDropDownCoaProduksiElectricity()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRefD> listData = null;

            try
            {
                string sql = "SELECT * FROM VW_GET_COA_PROD where val1 like '408020%'";

                listData = connection.Query<DDRefD>(sql).ToList();
            }
            catch (Exception) { }

            return listData;
        }

        public ResultInformation ReleaseToWorkflowNew(string updateBy, string userId, string data, string kodeCabang, string note = null)
        {
            string namaEmpl = updateBy;
            ResultInformation ret = new ResultInformation();
            AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    //query mencari approval
                    var sql = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                    {
                        a = data
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    //update table
                    sql = @"update PROP_CONTRACT_OFFER a set COMPLETED_STATUS =:b , APV_DISPOSISI =:d ,OFFER_STATUS =:c where a.CONTRACT_OFFER_NO = :a";
                    var r = connection.Execute(sql, new
                    {
                        a = data,
                        b = 2,
                        c = 10,
                        d = 1,
                    }, transaction: transaction);
                    co_new.COMPLETED_STATUS = "2";
                    co_new.OFFER_STATUS = "10";
                    ApprovalSettingDAL dalrole = new ApprovalSettingDAL();
                    dynamic role = dalrole.SetApprovalRole(data, "2");

                    if (role.Status == "S")
                    {
                        //update table
                        sql = @"update PROP_CONTRACT_OFFER a set COMPLETED_STATUS =:b , DISPOSISI_PARAF =:h ,OFFER_STATUS =:c, APPROVAL_NEXT_LEVEL=:d, TOP_APPROVED_LEVEL=:e, APV_POSITION=:f, RELEASE_BY_NIK=:g where a.CONTRACT_OFFER_NO = :a";
                        r = connection.Execute(sql, new
                        {
                            a = data,
                            b = 2,
                            c = role.Data.now_apv,
                            d = role.Data.next_apv,//sesuai data approval
                            e = role.Data.top_apv,//sesuai data approval
                            f = role.Data.apv_position,
                            g = userId,
                            h = "paraf"
                        }, transaction: transaction);

                        co_new.COMPLETED_STATUS = "2";
                        co_new.OFFER_STATUS = role.Data.now_apv.ToString();
                        co_new.TOP_APPROVED_LEVEL = role.Data.top_apv.ToString();
                        co_new.APV_POSITION = role.Data.apv_position.ToString();

                        //insert PROP_WORKFLOW_LOG 
                        sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                        string offer_status = connection.ExecuteScalar<string>(sql, new
                        {
                            a = co_new.OFFER_STATUS
                        });
                        sql = @"insert into PROP_WORKFLOW_LOG (CONTRACT_OFFER_NO,STATUS_CONTRACT,USER_LOGIN,BRANCH_ID,CREATION_DATE,CREATION_BY,LAST_UPDATE_DATE,LAST_UPDATE_BY,NOTES,LOG_STATUS,STATUS_CONTRACT_DESC)
                            values(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k)";
                        var log = connection.Execute(sql, new
                        {
                            a = data,
                            b = co_new.OFFER_STATUS,
                            c = userId.ToString(),//userlogin
                            d = kodeCabang,
                            e = DateTime.Now,
                            f = userId.ToString(),
                            g = DateTime.Now,
                            h = userId.ToString(),
                            i = "Release to Workflow",
                            j = "Done",
                            k = "Release to Workflow",
                        }, transaction: transaction);

                        MonitoringAnjunganDAL dalM = new MonitoringAnjunganDAL();
                        EmailConfigurationDAL dal = new EmailConfigurationDAL();

                        //proses ubah status
                        r = dalM.SetStatusByContractOffer("PROCESS FOR APPROVAL", data);

                        //proses kirim email
                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        param.USER_LOGIN = "";
                        param.APPROVAL_NOTE = note;

                        DataSurat dataSurat = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.CONTRACT_NO =:a AND A.STATUS_APV = 0";
                        dataSurat = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = data
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        transaction.Commit();
                        //send email
                        var user = dal.SearchUserRole(role.Data.next_apv, kodeCabang, co_new, "setujui", param, role.Data.det_id, dataSurat, namaEmpl, role.Data.role_name, 2, userId);
                        ret.ResultCode = "S";
                        ret.ResultDescription = "Success";
                        ret.TransactionCode = data;

                        //push notifikasi mobile
                        //dal.pushNotif(role.Data.next_apv, co_new.BRANCH_ID);

                    }
                    else
                    {
                        transaction.Rollback();
                        ret.ResultCode = "E";
                        ret.ResultDescription = "Approval Setting Tidak ditemukan";
                        ret.TransactionCode = "";
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ret.ResultCode = "E";
                    ret.ResultDescription = "Error" + ex.Message;
                    ret.TransactionCode = "";
                }
            }

            return ret;
        }

        //------------------------------ DATA TABLE APPROVAL EXCEPTION ------------
        public DataTablesTransContractOffer GetDataTransApprovalException(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string whereSql = search.Length > 2 ? " AND ((CONTRACT_OFFER_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(BE_ID) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CONTRACT_OFFER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' || '" + search.ToUpper() + "' || '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "";

            try
            {

                string sql = @"SELECT DISTINCT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                        to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                        TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                        CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') 
                        ELSE (SELECT REF_DESC AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') 
                        END AS OFFER_STATUS, BE_CITY, OLD_CONTRACT_OFFER, OLD_RENTAL_REQUEST, OLD_CONTRACT, REGIONAL_NAME, PARAF_LEVEL, DISPOSISI_LEVEL, DISPOSISI_PARAF
                        FROM V_CONTRACT_OFFER_HEADER 
                        WHERE BRANCH_ID in ( SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d ) AND (OFFER_STATUS = '10' OR (COMPLETED_STATUS != '1' OR COMPLETED_STATUS IS NULL)) " + whereSql
                        + " ORDER BY ID_INC DESC";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, d = KodeCabang });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
            }
            catch (Exception e)
            {
                throw;
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        public bool UpdateApprove(ContractOfferApprovalParameter data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "";
            int r = 0;

            // Update Approve Exception 
            sql = "UPDATE PROP_CONTRACT_OFFER SET COMPLETED_STATUS = 1, OFFER_STATUS = :b, TOP_APPROVED_LEVEL = :b, APPROVAL_NEXT_LEVEL = :b, APV_POSITION = 1, PARAF_LEVEL = 1, APV_DISPOSISI = 0 WHERE CONTRACT_OFFER_NO=:a";
            //sql = "UPDATE PROP_CONTRACT SET IJIN_PRINSIP_NO=:b WHERE CONTRACT_NO=:a";            
            try
            {
                r = connection.Execute(sql, new
                {
                    a = data.CONTRACT_OFFER_NO,         
                    b = data.TOP_APPROVAL
                });
                r = 1;
            }
            catch (Exception)
            {

            }
            result = (r > 0) ? true : false;
            connection.Close();
            return result;
        }
        
        //--------------------------------------- DATA TABLE HISTORY ----------------------
        public DataTablesTransContractOffer GetDataHistoryWorkflownew(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT * FROM (SELECT CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_DATE AS DATEH, NOTES 
                                    FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c
                                    UNION 
                                    SELECT A.CONTRACT_NO AS CONTRACT_OFFER_NO, A.TYPE AS STATUS_CONTRACT_DESC, B.USER_LOGIN || ' | ' || B.USER_NAME AS USER_LOGIN,TO_CHAR(A.TIME_CREATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, A.TIME_CREATE AS DATEH,
                                    A.MEMO AS NOTES 
                                    FROM PROP_HISTORY A JOIN APP_REPO.APP_USER B on A.USER_ID_CREATE = B.ID 
                                    WHERE A.CONTRACT_NO = :c and a.type != 'Approved') A ORDER BY A.DATEH DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                //search filter data
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        public Results AddDataSurat(DataSurat data, dynamic dataUser, dynamic directory, dynamic file_name, dynamic listUrl)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string namaEmpl = dataUser.UserRoleName;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    // api create surat dinas milea
                    string url = "https://peo.pelindo.co.id/api/web/surat-dinas/nomor";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.Json
                    };
                    var body = new
                    {
                        NIP = userLogin,
                        KD_KLASIFIKASI = data.KODE_KLASIFIKASI,
                        PERIHAL = data.SUBJECT,
                        KEPADA = data.NAMA_KEPADA,
                        TANGGAL = DateTime.Now
                    };
                    request.AddJsonBody(body);
                    request.AddHeader("token", "07f4a02dff1c535f543d23574c872821cd66e1b0");
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    if (list.status == 401)
                    {
                        result.Msg = list.message;
                        result.Status = "E";
                    }
                    else
                    {
                        string nomor_surat = list.data.NOMOR_SURAT;

                        var sql = @"declare surat clob; begin INSERT INTO PROP_SURAT (KODE_KLASIFIKASI,CONTRACT_NO, NO_SURAT, SUBJECT, ISI_SURAT, TTD, 
                            PARAF, LINK, ATAS_NAMA, STATUS_KIRIM, MEMO, USER_ID_CREATE, COUNT_SURAT,
                            TANGGAL, DIRECTORY, FILE_NAME, KEPADA, TEMBUSAN, NAMA_KEPADA, NAMA_TEMBUSAN, NAMA_TTD, STATUS_APV, BRANCH_ID, KEPADA_EKSTERNAL, TEMBUSAN_EKSTERNAL )
                            values(:z,:a, :b, :c, empty_clob(),:e, :f, :g, :h, :i, :j, :k, :l, to_date(:m,'DD/MM/YYYY'), :n, :o, :p, :q, :r, :s, :t, :u, :v, :kepadaEks, :tembusanEks) returning isi_surat into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = nomor_surat,
                            c = data.SUBJECT,
                            d = "",
                            e = data.TTD,
                            f = data.PARAF,
                            g = data.LINK,
                            h = data.ATAS_NAMA,
                            i = data.STATUS_KIRIM,
                            j = data.MEMO,
                            k = UserID,
                            l = 1,
                            m = data.TANGGAL,
                            n = directory,
                            o = file_name,
                            p = data.KEPADA,
                            q = data.TEMBUSAN,
                            r = data.NAMA_KEPADA,
                            s = data.NAMA_TEMBUSAN,
                            t = data.NAMA_TTD,
                            u = 0,
                            v = KodeCabang,
                            z = data.KODE_KLASIFIKASI,
                            kepadaEks = data.KEPADA_EKSTERNAL,
                            tembusanEks = data.TEMBUSAN_EKSTERNAL,
                        });
                        var cari = @" SELECT MAX(ID) ID FROM PROP_SURAT WHERE USER_ID_CREATE = :a and CONTRACT_NO = :b and NO_SURAT = :c ";
                        var ID = connection.Query<int>(cari, new { 
                            a = UserID, 
                            b = data.CONTRACT_NO, 
                            c = nomor_surat, 
                        });

                        foreach (var item in listUrl)
                        {
                            var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                            int kt = connection.Execute(insertAttachment, new
                            {
                                a = ID,
                                b = Convert.ToString(item.file_name),
                                c = Convert.ToString(item.directory),
                            });
                        }

                        DataSurat datasuratbaru = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                        datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = ID
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        var LIST_PARAF = JsonConvert.DeserializeObject<dynamic>(data.LIST_PARAF);
                        int total_paraf = LIST_PARAF.Count;
                        int w = 0;

                        dynamic dataEmail = null;
                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        if (data.STATUS_KIRIM == "Send")
                        {
                            param.USER_LOGIN = "";
                            param.APPROVAL_NOTE = "";

                            //jika tidak ada pemaraf
                            if (total_paraf == 0)
                            {
                                //TransContractOfferDAL dalWf = new TransContractOfferDAL();
                                //var hasil = dalWf.ReleaseToWorkflowNew(
                                //    Convert.ToString(name),
                                //    Convert.ToString(userLogin),
                                //    Convert.ToString(data.CONTRACT_NO),
                                //    Convert.ToString(KodeCabang)
                                //    );
                            }
                            else
                            {
                                foreach (var item in LIST_PARAF)
                                {
                                    var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                                    connection.Execute(sql3, new
                                    {
                                        a = Convert.ToInt32(item.ID),
                                        b = Convert.ToInt32(item.HIRARKI),
                                        c = "surat",
                                        d = data.CONTRACT_NO,
                                        e = ID,
                                        f = 0,
                                        g = 1,
                                        h = Convert.ToString(item.NAMA),
                                        i = total_paraf,
                                    });
                                    if (Convert.ToInt32(item.HIRARKI) == 1)
                                    {
                                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                                        {
                                            var cariEmail = @"SELECT USER_EMAIL, USER_NAME, USER_LOGIN FROM APP_USER WHERE ID =:b";
                                            dataEmail = conn.Query(cariEmail, new { b = Convert.ToInt32(item.ID) }).DefaultIfEmpty(null).FirstOrDefault();
                                        }
                                    }
                                }

                                var query_update_contract = @"update PROP_CONTRACT_OFFER a set PARAF_LEVEL =:b ,  
                                DISPOSISI_PARAF =:c where a.CONTRACT_OFFER_NO = :a";
                                var x = connection.Execute(query_update_contract, new
                                {
                                    a = data.CONTRACT_NO,
                                    b = 1,
                                    c = "paraf",
                                });
                            }

                            var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                            w = connection.Execute(sql2, new
                            {
                                a = ID,
                                b = data.CONTRACT_NO,
                                c = UserID,
                                d = data.MEMO,
                                e = data.SUBJECT,
                                f = "Surat",
                                g = 0,
                                h = 0,
                                i = namaEmpl,
                            });
                        }
                        else
                        {
                            var query_update_contract = @"update PROP_CONTRACT_OFFER a set PARAF_LEVEL =:b ,  
                           DISPOSISI_PARAF =:c where a.CONTRACT_OFFER_NO = :a";
                            var x = connection.Execute(query_update_contract, new
                            {
                                a = data.CONTRACT_NO,
                                b = 0, //sudah create surat status draft
                                c = "paraf",
                            });

                            foreach (var item in LIST_PARAF)
                            {
                                var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                                connection.Execute(sql3, new
                                {
                                    a = Convert.ToInt32(item.ID),
                                    b = Convert.ToInt32(item.HIRARKI),
                                    c = "surat",
                                    d = data.CONTRACT_NO,
                                    e = ID,
                                    f = 0,
                                    g = 1,
                                    h = Convert.ToString(item.NAMA),
                                    i = total_paraf,
                                });
                            }

                            var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                            w = connection.Execute(sql2, new
                            {
                                a = ID,
                                b = data.CONTRACT_NO,
                                c = UserID,
                                d = data.MEMO,
                                e = data.SUBJECT,
                                f = "Draft",
                                g = 0,
                                h = 0,
                                i = namaEmpl,
                            });
                        }
                        if (r != 0 && w > 0)
                        {
                            transaction.Commit();
                            if (data.STATUS_KIRIM == "Send")
                            {
                                if (total_paraf == 0)
                                {
                                    TransContractOfferDAL dalWf = new TransContractOfferDAL();
                                    var hasil = dalWf.ReleaseToWorkflowNew(
                                        Convert.ToString(name),
                                        Convert.ToString(userLogin),
                                        Convert.ToString(data.CONTRACT_NO),
                                        Convert.ToString(KodeCabang),
                                        Convert.ToString(data.MEMO)
                                        );
                                }
                                else
                                {
                                    EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                    var sendingemail = dal.formateSendEmail(co_new, datasuratbaru, KodeCabang, "setujui", param, dataEmail, namaEmpl);

                                    ListNotifikasiController listNotif = new ListNotifikasiController();
                                    ParamNotification paramNotif = new ParamNotification();
                                    paramNotif.username = dataEmail.USER_LOGIN;
                                    paramNotif.type = "3"; //TYPE PARAF
                                    paramNotif.notification = data.MEMO;
                                    paramNotif.no_contract = data.CONTRACT_NO;
                                    paramNotif.pengirim = userLogin;
                                    var sendNotif = listNotif.addNewNotif(paramNotif);
                                }
                            }
                            result.Data = ID;
                            result.Msg = "Data Added Successfully";
                            result.Status = "S";
                        }
                        else
                        {
                            result.Msg = "Failed to Add Data";
                            result.Status = "E";

                        }
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            return result;
        }

        public Results EditDataSurat(DataSurat data, dynamic dataUser, dynamic directory, dynamic file_name, dynamic listUrl)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string namaEmpl = dataUser.UserRoleName;

            Results result = new Results();

            int r = 0;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();


                    var sql = "";
                    if (directory == null)
                    {
                        directory = "";
                        file_name = "";
                        sql = @"declare surat clob; begin UPDATE PROP_SURAT SET SUBJECT =:a, ISI_SURAT = empty_clob(), TTD =:c, 
                            PARAF =:d, LINK =:e , ATAS_NAMA =:f, STATUS_KIRIM =:g, MEMO =:h, TIME_UPDATE = sysdate ,COUNT_SURAT =:j,
                            TANGGAL = to_date(:k,'DD/MM/YYYY'), KEPADA =:n, TEMBUSAN =:o, NAMA_KEPADA =:p, NAMA_TEMBUSAN =:q, NAMA_TTD =:r, KEPADA_EKSTERNAL =:kepadaEks, TEMBUSAN_EKSTERNAL =:tembusanEks 
                            WHERE ID =:s RETURNING ISI_SURAT into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                    }
                    else
                    {
                        sql = @"declare surat clob; begin UPDATE PROP_SURAT SET SUBJECT =:a, ISI_SURAT = empty_clob(), TTD =:c, 
                            PARAF =:d, LINK =:e , ATAS_NAMA =:f, STATUS_KIRIM =:g, MEMO =:h, TIME_UPDATE = sysdate, COUNT_SURAT =:j,
                            TANGGAL = to_date(:k,'DD/MM/YYYY'), DIRECTORY =:l, FILE_NAME =:m, KEPADA =:n, TEMBUSAN =:o, NAMA_KEPADA =:p, NAMA_TEMBUSAN =:q, NAMA_TTD =:r, KEPADA_EKSTERNAL =:kepadaEks, TEMBUSAN_EKSTERNAL =:tembusanEks  
                            WHERE ID =:s RETURNING ISI_SURAT into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                    }

                    r = connection.Execute(sql, new
                    {
                        a = data.SUBJECT,
                        b = "",
                        c = data.TTD,
                        d = data.PARAF,
                        e = data.LINK,
                        f = data.ATAS_NAMA,
                        g = data.STATUS_KIRIM,
                        h = data.MEMO,
                        j = 1,
                        k = data.TANGGAL,
                        l = directory,
                        m = file_name,
                        n = data.KEPADA,
                        o = data.TEMBUSAN,
                        p = data.NAMA_KEPADA,
                        q = data.NAMA_TEMBUSAN,
                        r = data.NAMA_TTD,
                        s = data.ID,
                        kepadaEks = data.KEPADA_EKSTERNAL,
                        tembusanEks = data.TEMBUSAN_EKSTERNAL,
                    });

                    foreach (var item in listUrl)
                    {
                        var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                        int kt = connection.Execute(insertAttachment, new
                        {
                            a = data.ID,
                            b = Convert.ToString(item.file_name),
                            c = Convert.ToString(item.directory),
                        });
                    }

                    var DeleteParaf = @"DELETE PROP_DISPOSISI_PARAF A WHERE A.SURAT_ID =:a AND CONTRACT_NO =:b AND COUNT_SURAT = 1 ";
                    connection.Execute(DeleteParaf, new { a = data.ID, b = data.CONTRACT_NO });

                    if (data.STATUS_KIRIM == "Send")
                    {
                        dynamic sendingemail = null;
                        DataSurat datasuratbaru = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                        datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = data.ID
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        param.USER_LOGIN = "";
                        param.APPROVAL_NOTE = "";

                        EmailConfigurationDAL dalMail = new EmailConfigurationDAL();
                        // sendingemail = dalMail.formateSendEmail(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, namaEmpl);

                        List<dynamic> LIST_PARAF = JsonConvert.DeserializeObject<List<dynamic>>(data.LIST_PARAF);
                        int total_paraf = LIST_PARAF.Count;
                        if (total_paraf == 0)
                        {
                            TransContractOfferDAL dalWf = new TransContractOfferDAL();
                            var hasil = dalWf.ReleaseToWorkflowNew(
                                Convert.ToString(name),
                                Convert.ToString(userLogin),
                                Convert.ToString(data.CONTRACT_NO),
                                Convert.ToString(KodeCabang)
                                );
                        }
                        else
                        {
                            foreach (var item in LIST_PARAF)
                            {
                                var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                                connection.Execute(sql3, new
                                {
                                    a = Convert.ToInt32(item.ID),
                                    b = Convert.ToInt32(item.HIRARKI),
                                    c = "surat",
                                    d = data.CONTRACT_NO,
                                    e = data.ID,
                                    f = 0,
                                    g = 1,
                                    h = Convert.ToString(item.NAMA),
                                    I = total_paraf,
                                });
                            }
                            //cari data pemaraf
                            var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a and a.count_surat = 1 AND a.urutan =:b ";
                            var nextParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = 1 }).FirstOrDefault();

                            dynamic dataEmail = null;
                            using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                            {
                                var cariEmail = @"SELECT USER_EMAIL, USER_NAME FROM APP_USER WHERE ID =:b";
                                dataEmail = conn.Query(cariEmail, new { b = nextParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                            }
                            sendingemail = dalMail.emailParaf(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, namaEmpl, dataEmail, data.MEMO);

                            var query_update_contract = @"update PROP_CONTRACT_OFFER a set PARAF_LEVEL =:b ,  
                            DISPOSISI_PARAF =:c where a.CONTRACT_OFFER_NO = :a";
                            var x = connection.Execute(query_update_contract, new
                            {
                                a = data.CONTRACT_NO,
                                b = 1,
                                c = "paraf",
                            });
                        }

                        var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                        int w = connection.Execute(sql2, new
                        {
                            a = data.ID,
                            b = data.CONTRACT_NO,
                            c = UserID,
                            d = data.MEMO,
                            e = data.SUBJECT,
                            f = "Surat",
                            g = 0,
                            h = 0,
                            i = namaEmpl,
                        });
                    }
                    else
                    {
                        List<dynamic> LIST_PARAF = JsonConvert.DeserializeObject<List<dynamic>>(data.LIST_PARAF);
                        int total_paraf = LIST_PARAF.Count;
                        foreach (var item in LIST_PARAF)
                        {
                            var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                            connection.Execute(sql3, new
                            {
                                a = Convert.ToInt32(item.ID),
                                b = Convert.ToInt32(item.HIRARKI),
                                c = "surat",
                                d = data.CONTRACT_NO,
                                e = data.ID,
                                f = 0,
                                g = 1,
                                h = Convert.ToString(item.NAMA),
                                i = total_paraf,
                            });
                        }

                        var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                        int w = connection.Execute(sql2, new
                        {
                            a = data.ID,
                            b = data.CONTRACT_NO,
                            c = UserID,
                            d = data.MEMO,
                            e = data.SUBJECT,
                            f = "Edit",
                            g = 0,
                            h = 0,
                            i = namaEmpl,
                        });
                    }

                    if (r != 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Edit Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Edit Data";
                        result.Status = "E";

                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            return result;
        }

        public Results AddDataAttachment(dynamic data, dynamic listUrl)
        {
            Results result = new Results();
            var ID = data[0];
            var file_name = listUrl.fileName;
            var directory = listUrl.directory;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                    int w = connection.Execute(sql2, new
                    {
                        a = ID,
                        b = file_name,
                        c = directory,
                    });
                    if (w > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)

                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            return result;
        }

        public Results AddDataAttachmentEdit(dynamic data, dynamic directory, dynamic file_name)
        {
            Results result = new Results();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data,
                        b = file_name,
                        c = directory,
                    });
                    if (w > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)

                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            return result;
        }
        public DataSurat GetDataDisposisi(dynamic id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT * FROM PROP_SURAT A WHERE A.COUNT_SURAT = 1 AND A.CONTRACT_NO = :a";
            var result = connection.Query<DataSurat>(sql, new { a = id }).FirstOrDefault();

            return result;
        }

        public IEnumerable<DisposisiHistory> GetDataHistoryDisposisi(string id)
        {
            IEnumerable<DisposisiHistory> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT A.CONTRACT_NO, A.MEMO, A.SUBJECT, A.TYPE, A.TIME_CREATE, NAMA, INSTRUKSI
                                FROM PROP_HISTORY A
                                WHERE A.CONTRACT_NO = :b ORDER BY A.TIME_CREATE";

                    listData = connection.Query<DisposisiHistory>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public IEnumerable<LIST_PARAF> GetDataParaf(string id)
        {
            IEnumerable<LIST_PARAF> listData = null;

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT USER_ID, URUTAN HIRARKI, NAMA
                                FROM PROP_DISPOSISI_PARAF A
                                WHERE A.SURAT_ID = :b ";

                    listData = connection.Query<LIST_PARAF>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public DataTablesTransContractOffer GetDataSertifikat(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string fullSql =
                "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {

                    string sql = @"SELECT A.ATTACHMENT, A.ID AS ATTACHMENT_ID, A.NO_SERTIFIKAT, A.LUAS_LAHAN, A.SERTIFIKAT_TYPE FROM SERTIFIKAT_HPL A ORDER BY TIME_CREATE DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(fullSql, new { a = start, b = end, c = id });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }

            }
            else
            {
            }

            result = new DataTablesTransContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public Results GetDataCurrencyRate()
        {
            Results result = new Results();
            try
            {
                var date = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                // api get currency
                string url = "https://fuse19.pelindo.co.id/master/mdm/safrvalutakurs/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                var body = new
                {
                    identity = new
                    {
                        applicationid = "REMOTE",
                        reqtxnid = "e1",
                        reqdate = DateTime.Now
                    },
                    paging = new
                    {
                        page = "1",
                        limit = "1"
                    },
                    parameter = new
                    {
                        TGL_MULAI_RATE = date,
                        column = "KODE_VALUTA, TGL_MULAI_RATE, NILAI_RATE,KD_AKTIF"
                    }
                };

                request.AddJsonBody(body);

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                dynamic rates = "1";
                if (returnData.status.responsecode != "404")
                {
                    rates = returnData.result[0].NILAI_RATE.ToString();
                }

                result.Status = "S";
                result.Data = rates;
            }
            catch (Exception e)
            {
                result.Msg = "Terjadi error. " + e.Message;
                result.Status = "E";
            }
            return result;
        }

    }
}