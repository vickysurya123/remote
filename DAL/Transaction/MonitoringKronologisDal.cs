﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;
using Remote.Models.MonitoringKronologisMaster;
using Oracle.ManagedDataAccess.Client;

namespace Remote.DAL
{
    public class MonitoringKronologisDal
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT A.ID, A.JUDUL, TO_CHAR(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, B.BE_NAME, C.BE_NAME AS REGIONAL, A.IS_APPROVE
                                    FROM PROP_KRONOLOGIS A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                    JOIN BUSINESS_ENTITY C ON C.BE_ID = A.REGIONAL
                                    ORDER BY A.TIME_CREATE DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT A.ID, A.JUDUL, TO_CHAR(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, B.BE_NAME, A.IS_APPROVE
                                FROM PROP_KRONOLOGIS A
                                JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                WHERE ((UPPER(A.JUDUL) LIKE '%' ||:c|| '%') or (UPPER(A.TANGGAL) LIKE '%' ||:c|| '%') or (UPPER(B.BE_NAME) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<KRONOLOGIS_LIST>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IDataTable GetDataDetail(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string fullSql =
                "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a order by id";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {

                    string sql = @"SELECT A.ID, A.JUDUL, A.TANGGAL, B.BE_NAME, D.BE_NAME AS REGIONAL, TO_CHAR(C.TANGGAL_SURAT, 'DD/MM/YYYY') TANGGAL_SURAT, 
                                        C.KENDALA, C.PROGRESS, C.TINDAK_LANJUT, C.DIRECTORY, C.FILE_NAME, C.KETERANGAN, C.NO_SURAT
                                        FROM PROP_KRONOLOGIS A
                                        JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                        JOIN BUSINESS_ENTITY D ON D.BE_ID = A.REGIONAL 
                                        JOIN PROP_KRONOLOGIS_DETAIL C ON C.KRONOLOGIS_ID = A.ID
                                        WHERE A.ID =:c ORDER BY C.ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<KRONOLOGIS_LIST>(fullSql, new { a = start, b = end, c = id });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }

            }
            else
            {
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_MONITORING_KRONOLOGIS> GetDataDetailEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_MONITORING_KRONOLOGIS> listData = null;

            try
            {

                string sql = @"SELECT A.NO_SURAT, TO_CHAR(A.TANGGAL_SURAT, 'DD/MM/YYYY') TANGGAL_SURAT, A.KETERANGAN, A.KENDALA, A.TINDAK_LANJUT, A.PROGRESS, A.DIRECTORY  FROM PROP_KRONOLOGIS_DETAIL A
                                        WHERE A.KRONOLOGIS_ID =:c";

                listData = connection.Query<AUP_MONITORING_KRONOLOGIS>(sql, new
                {
                    c = id
                });
            }
            catch (Exception)
            {
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IDataTable GetDataDownload(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string fullSql =
                "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a order by id";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {

                    string sql = @"SELECT A.ID, A.DIRECTORY, A.FILE_NAME FROM PROP_KRONOLOGIS_DETAIL A
                                        WHERE A.KRONOLOGIS_ID =:c";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<KRONOLOGIS_LIST>(fullSql, new { a = start, b = end, c = id });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }

            }
            else
            {
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            connection.Close();
            connection.Dispose();
            return result;
        }



        public IEnumerable<DDCabang> GetDataCabang()
        {
            string sql = "SELECT BE_ID, BE_NAME FROM BUSINESS_ENTITY WHERE BE_NAME LIKE '%Cabang%'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDCabang> listDetil = connection.Query<DDCabang>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDRegional> GetDataRegional()
        {
            string sql = "SELECT BE_ID, BE_NAME FROM BUSINESS_ENTITY WHERE BE_NAME LIKE '%Regional%'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRegional> listDetil = connection.Query<DDRegional>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public AUP_MONITORING_KRONOLOGIS GetDataForEditKronologi(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = @"SELECT A.ID, A.JUDUL, TO_CHAR(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, B.BE_NAME AS CABANG, D.BE_NAME AS REGIONAL
                                        FROM PROP_KRONOLOGIS A
                                        JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                        JOIN BUSINESS_ENTITY D ON D.BE_ID = A.REGIONAL 
                                        WHERE A.ID =:a";

            AUP_MONITORING_KRONOLOGIS result = connection.Query<AUP_MONITORING_KRONOLOGIS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results DeleteDataHeader(DataMonitoringKronologis data, String id)
        {
            Results result = new Results();
            int r = 0, r2 = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from PROP_KRONOLOGIS a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = id

                });

                var sql2 = @"delete from PROP_KRONOLOGIS_DETAIL a where a.KRONOLOGIS_ID =:c";
                r2 = connection.Execute(sql2, new
                {
                    c = id

                });

                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }


        public IDataTable GetDataMonitoring(int be_id, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT A.ID, A.JUDUL, TO_CHAR(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, B.BE_NAME 
                                    FROM PROP_KRONOLOGIS A
                                    JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID 
                                    where a.BE_ID = :be_id
                                    order by a.BE_ID desc ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<KRONOLOGIS_LIST>(fullSql, new { a = start, b = end, be_id = be_id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, be_id = be_id });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT A.ID, A.JUDUL, TO_CHAR(A.TANGGAL, 'DD/MM/YYYY') TANGGAL, B.BE_NAME 
                                FROM PROP_KRONOLOGIS A
                                JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                WHERE ((UPPER(A.JUDUL) LIKE '%' ||:c|| '%') or (UPPER(A.TANGGAL) LIKE '%' ||:c|| '%') or (UPPER(B.BE_NAME) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<KRONOLOGIS_LIST>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        be_id = be_id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, be_id = be_id });
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

            connection.Close();
            connection.Dispose();

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public Results AddDetailMonitoring(DataMonitoringKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            int q = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"select * from PROP_KRONOLOGIS_DETAIL";
                sql = @"INSERT INTO PROP_KRONOLOGIS (JUDUL, BE_ID, USER_ID_CREATE, REGIONAL, TANGGAL)
                            values(:a, :b, :c, :d,  to_date(:e,'DD/MM/YYYY'))";
                r = connection.Execute(sql, new
                {
                    a = data.JUDUL,
                    b = data.FDDCabang,
                    c = UserID,
                    d = data.FDDRegional,
                    e = data.TANGGAL,

                });
                r = 1;
            }
            catch (Exception ex)
            {
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool AddDataMonitoring(DataMonitoringKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql = @"INSERT INTO PROP_KRONOLOGIS (JUDUL,TANGGAL,BE_ID,REGIONAL,USER_ID_CREATE)
                            values(:a, to_date(:b,'DD/MM/YYYY'), :c, :d, :e)";

                    var r = connection.Execute(sql, new
                    {
                        a = data.JUDUL,
                        b = data.TANGGAL,
                        c = data.FDDCabang,
                        d = data.FDDRegional,
                        e = UserID
                    });

                    if (r == 0)
                    {
                        return false;
                    }

                    var kronologi = @" SELECT MAX(ID) ID FROM PROP_KRONOLOGIS WHERE USER_ID_CREATE = :a ";
                    var ID = connection.ExecuteScalar<int>(kronologi, new { a = UserID });

                    bool isFail = false;
                    foreach (var detail in data.detailKronologis)
                    {
                        var sql2 = @"INSERT INTO PROP_KRONOLOGIS_DETAIL (KRONOLOGIS_ID,NO_SURAT,TANGGAL_SURAT,KETERANGAN,TINDAK_LANJUT,KENDALA,PROGRESS,FILE_NAME,DIRECTORY)
                            values(:a, :b, to_date(:c,'DD/MM/YYYY'), :d, :e, :f, :g, :h, :i)";

                        var r2 = connection.Execute(sql2, new
                        {
                            a = ID,
                            b = detail.NO_SURAT,
                            c = detail.TANGGAL_SURAT,
                            d = detail.KETERANGAN,
                            e = detail.TINDAK_LANJUT,
                            f = detail.KENDALA,
                            g = detail.PROGRESS,
                            h = detail.FILE_NAME,
                            i = detail.DIRECTORY
                        });

                        if (r2 == 0)
                        {
                            isFail = true;
                            break;
                        }
                    }

                    if (isFail)
                    {
                        transaction.Rollback();
                    }

                    transaction.Commit();
                    connection.Close();
                    connection.Dispose();
                    return !isFail;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    connection.Dispose();
                    return false;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
        }

        public bool AddDataMonitoringEdit(DataMonitoringKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql = @"UPDATE PROP_KRONOLOGIS a SET 
                            JUDUL = :a,
                            TANGGAL = to_date(:b,'DD/MM/YYYY'),
                            BE_ID = :c,
                            REGIONAL = :d,
                            USER_ID_UPDATE = :e,
                            TIME_UPDATE = sysdate
                            where ID = :id";

                    var r = connection.Execute(sql, new
                    {
                        id = data.ID,
                        a = data.JUDUL,
                        b = data.TANGGAL,
                        c = data.FDDCabang,
                        d = data.FDDRegional,
                        e = UserID
                    });

                    if (r == 0)
                    {
                        return false;
                    }

                    //var kronologi = @" SELECT MAX(ID) ID FROM PROP_KRONOLOGIS WHERE USER_ID_CREATE = :a ";
                    //var ID = connection.ExecuteScalar<int>(kronologi, new { a = UserID });

                    if (data.delete_line != null)
                    {
                        var sql3 = @"delete from PROP_KRONOLOGIS_DETAIL a where a.ID in :a";
                        var r3 = connection.Execute(sql3, new
                        {
                            a = data.delete_line

                        });
                    }

                    bool isFail = false;
                    foreach (var detail in data.detailKronologis)
                    {
                        dynamic r2;
                        if (detail.ID == null || detail.ID == "")
                        {
                            var sql2 = @"INSERT INTO PROP_KRONOLOGIS_DETAIL (KRONOLOGIS_ID,NO_SURAT,TANGGAL_SURAT,KETERANGAN,TINDAK_LANJUT,KENDALA,PROGRESS,FILE_NAME,DIRECTORY)
                            values(:a, :b, to_date(:c,'DD/MM/YYYY'), :d, :e, :f, :g, :h, :i)";

                            r2 = connection.Execute(sql2, new
                            {
                                a = data.ID,
                                b = detail.NO_SURAT,
                                c = detail.TANGGAL_SURAT,
                                d = detail.KETERANGAN,
                                e = detail.TINDAK_LANJUT,
                                f = detail.KENDALA,
                                g = detail.PROGRESS,
                                h = detail.FILE_NAME,
                                i = detail.DIRECTORY
                            });
                        }
                        else
                        {
                            string sql2 = "";

                            sql2 = @"UPDATE PROP_KRONOLOGIS_DETAIL SET 
                                    KRONOLOGIS_ID = :a,
                                    NO_SURAT = :b,
                                    TANGGAL_SURAT = to_date(:c,'DD/MM/YYYY'),
                                    KETERANGAN = :d,
                                    TINDAK_LANJUT = :e,
                                    KENDALA = :f,
                                    PROGRESS = :g ";

                            if (detail.FILE_DELETE == "deleted" || detail.FILE != null)
                            {
                                sql2 += @", FILE_NAME = :h,
                                    DIRECTORY = :i ";
                            }

                            sql2 += " where ID = :id ";

                            r2 = connection.Execute(sql2, new
                            {
                                id = detail.ID,
                                a = data.ID,
                                b = detail.NO_SURAT,
                                c = detail.TANGGAL_SURAT,
                                d = detail.KETERANGAN,
                                e = detail.TINDAK_LANJUT,
                                f = detail.KENDALA,
                                g = detail.PROGRESS,
                                h = detail.FILE_NAME,
                                i = detail.DIRECTORY
                            });
                        }

                        if (r2 == 0)
                        {
                            isFail = true;
                            break;
                        }
                    }

                    if (isFail)
                    {
                        transaction.Rollback();
                        connection.Close();
                        return !isFail;
                    }

                    transaction.Commit();
                    connection.Close();
                    connection.Dispose();
                    return !isFail;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    connection.Dispose();
                    return false;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
        }

        public Results EditDataHeader(DataMonitoringKronologis data, dynamic dataUser, dynamic sFile, dynamic file2)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int q = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string DelData = "DELETE FROM PROP_KRONOLOGIS_DETAIL WHERE KRONOLOGIS_ID=:a";

                int x = connection.Execute(DelData, new
                {
                    a = data.ID
                });


                var sqlz = @"INSERT INTO PROP_KRONOLOGIS_DETAIL (KRONOLOGIS_ID, NO_SURAT, TANGGAL_SURAT, KETERANGAN, TINDAK_LANJUT, KENDALA, PROGRESS, FILE_NAME, DIRECTORY)
                            values(:a, :b, to_date(:c,'DD/MM/YYYY'), :d, :e, :f, :g, :h, :i)";
                q = connection.Execute(sqlz, new
                {
                    a = data.ID,
                    b = data.NO_SURAT,
                    c = data.TANGGAL_SURAT,
                    d = data.KETERANGAN,
                    e = data.TINDAK_LANJUT,
                    f = data.KENDALA,
                    g = data.PROGRESS,
                    h = file2,
                    i = sFile,
                });
                q = 1;
                if (q > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }
        public Results EditDataMonitoring(DataMonitoringKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_KRONOLOGIS_DETAIL a set a.JUDUL =:a,  a.BE_ID =:b, 
                        a.TANGGAL = to_date(:c,'DD/MM/YYYY'),
                        a.USER_ID_UPDATE =:d where a.ID =:f";
                r = connection.Execute(sql, new
                {
                    a = data.JUDUL,
                    b = data.BE_ID,
                    c = data.TANGGAL,
                    d = UserID,
                    f = data.ID,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results EditDataDetailMonitoring(detailKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_KRONOLOGIS_DETAIL a set a.JUDUL =:a,  a.BE_ID =:b, 
                        a.TANGGAL = to_date(:c,'DD/MM/YYYY'),
                        a.USER_ID_UPDATE =:d where a.ID =:f";
                r = connection.Execute(sql, new
                {
                    a = data.NO_SURAT,
                });
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results AddHeader(DataMonitoringKronologis data, dynamic dataUser, dynamic sFile, dynamic file2)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            int q = 0;
            int x = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();


            try
            {
                var kronologi = @"SELECT MAX(ID) ID FROM PROP_KRONOLOGIS WHERE USER_ID_CREATE = :a ";
                var ID = connection.Query<int>(kronologi, new { a = UserID });
                var sqlz = @"INSERT INTO PROP_KRONOLOGIS_DETAIL (NO_SURAT)
                            values (:b)";
                /* var sqlz = @"INSERT ALL
                               INTO PROP_KRONOLOGIS_DETAIL (KRONOLOGIS_ID, NO_SURAT, KETERANGAN, TINDAK_LANJUT, KENDALA, PROGRESS, FILE_NAME, DIRECTORY) VALUES (:a, :b, :d, :e, :f, :g, :h, :i)
                             SELECT * FROM dual";*/
                /*for (var x = 0; x < data.NO_SURAT; x++)
                {

                }*/
                //var hitung_surat = new List<string> {data.NO_SURAT};
                if (data.NO_SURAT != null)
                {
                    foreach (var i in data.NO_SURAT)
                    {
                        try
                        {
                            q = connection.Execute(sqlz, new
                            {
                                /*a = ID,*/
                                b = data.NO_SURAT,
                                /*c = data.TANGGAL_SURAT,
                                d = data.KETERANGAN,
                                e = data.TINDAK_LANJUT,
                                f = data.KENDALA,
                                g = data.PROGRESS,
                                h = file2,
                                i = sFile*/
                            });
                            q = 1;
                        }
                        catch (Exception)
                        {
                            r = 0;
                            throw;
                        }
                    }

                    /*b = i;*/
                }



                /*var sqlfile = @"UPDATE PROP_KRONOLOGIS_DETAIL SET FILE_NAME=:a, DIRECTORY=:b  WHERE KRONOLOGIS_ID = :c";
                *//* var sqlz = @"INSERT ALL
                               INTO PROP_KRONOLOGIS_DETAIL (KRONOLOGIS_ID, NO_SURAT, KETERANGAN, TINDAK_LANJUT, KENDALA, PROGRESS, FILE_NAME, DIRECTORY) VALUES (:a, :b, :d, :e, :f, :g, :h, :i)
                             SELECT * FROM dual";*//*
                q = connection.Execute(sqlfile, new
                {
                    *//*a = ID,
                    b = data.NO_SURAT,*/
                /*c = data.TANGGAL_SURAT,
                d = data.KETERANGAN,
                e = data.TINDAK_LANJUT,
                f = data.KENDALA,
                g = data.PROGRESS,*//*
                a = file2,
                b = sFile,
                c = ID
            });*/



                if (q > 0)
                {
                    result.Msg = "Data Added Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results UpdateDataMonitoring(DataMonitoringKronologis data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            Results result = new Results();
            int r = 0;
            int q = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                //var sql = @"select * from PROP_KRONOLOGIS";
                // var kronologi = @" SELECT MAX(ID) ID FROM PROP_KRONOLOGIS WHERE USER_ID_CREATE = :a ";
                // var ID = connection.Query<int>(kronologi, new { a = UserID });
                var sql = @" UPDATE PROP_KRONOLOGIS a SET a.JUDUL = :a,  a.TANGGAL = to_date(:b,'DD/MM/YYYY'), a.BE_ID =:c, a.REGIONAL =:d WHERE a.ID =:e";
                /*foreach (var item in data.detailKronologis)
                {
                    try
                    {*/
                r = connection.Execute(sql, new
                {
                    a = data.JUDUL,
                    b = data.TANGGAL,
                    c = data.FDDCabang,
                    d = data.FDDRegional,
                    e = data.ID
                });
                r = 1;
                if (r > 0)
                {
                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }
                /* }
                 catch(Exception)
                 {
                     r = 0;
                     throw;
                 }
             }*/
            }
            catch (Exception ex)
            {
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataMonitoringKronologis GetFullData(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql = @"SELECT * FROM PROP_KRONOLOGIS WHERE ID = :a";

                    DataMonitoringKronologis dataMonitoringKronologis = connection.Query<DataMonitoringKronologis>(sql, new { a = id }).FirstOrDefault();

                    if (dataMonitoringKronologis == null)
                    {
                        return null;
                    }

                    var sql2 = @"SELECT * FROM PROP_KRONOLOGIS_DETAIL WHERE KRONOLOGIS_ID = :a order by id DESC";

                    List<detailKronologis> detailKronologisList = connection.Query<detailKronologis>(sql2, new { a = id }).ToList();

                    dataMonitoringKronologis.detailKronologis = detailKronologisList;

                    return dataMonitoringKronologis;
                }
                catch (Exception ex)
                {
                    return null;
                }
        }

        public bool Approve(string id, dynamic dataUser)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql = @"UPDATE PROP_KRONOLOGIS a SET 
                            IS_APPROVE = 1,
                            USER_ID_UPDATE = :e,
                            TIME_UPDATE = sysdate
                            where ID = :id";

                    var r = connection.Execute(sql, new
                    {
                        id = id,
                        e = dataUser.UserID
                    });

                    if (r == 0)
                    {
                        transaction.Rollback();
                        connection.Close();
                        connection.Dispose();
                        return false;
                    }

                    transaction.Commit();
                    connection.Close();
                    connection.Dispose();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    connection.Dispose();
                    return false;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
        }

        public Results GetExcel(DashExcel data)
        {
            Results result = new Results();
            List<detailKronologisExcel> list = new List<detailKronologisExcel>();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql2 = @"SELECT A.ID, A.JUDUL, A.TANGGAL, B.BE_NAME AS CABANG, D.BE_NAME AS REGIONAL, TO_CHAR(C.TANGGAL_SURAT, 'DD/MM/YYYY') TANGGAL_SURAT, 
                                        C.KENDALA, C.PROGRESS, C.TINDAK_LANJUT, C.DIRECTORY, C.FILE_NAME, C.KETERANGAN, C.NO_SURAT
                                        FROM PROP_KRONOLOGIS A
                                        JOIN BUSINESS_ENTITY B ON B.BE_ID = A.BE_ID
                                        JOIN BUSINESS_ENTITY D ON D.BE_ID = A.REGIONAL 
                                        JOIN PROP_KRONOLOGIS_DETAIL C ON C.KRONOLOGIS_ID = A.ID
                                        WHERE A.ID =" + data.ID + "";

                /*var sql2 = @"SELECT TO_CHAR(C.TANGGAL_SURAT, 'DD/MM/YYYY') TANGGAL_SURAT, 
                                        C.KENDALA, C.PROGRESS, C.TINDAK_LANJUT, C.DIRECTORY, C.FILE_NAME, C.KETERANGAN, C.NO_SURAT
                                        FROM PROP_KRONOLOGIS_DETAIL C 
                                        WHERE C.KRONOLOGIS_ID =" + data.ID + "";*/

                list = connection.Query<detailKronologisExcel>(sql2).ToList();
                result.Status = "S";
                result.Data = list;

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

    }

}