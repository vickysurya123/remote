﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.TransRentalRequest;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Models.MasterRentalObject;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransRentalRequestDAL
    {

        //-------------------- DATA TABLE HEADER DATA --------------------
        public DataTablesTransRentalRequest GetDataTransRentalRequest(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransRentalRequest result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_RENTAL_REQUEST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql =
                            "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                            "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                            "FROM V_TRANS_RENTAL_REQ_HEADER WHERE STATUS = 'CREATED'";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql,
                            new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql =
                                "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                                "FROM V_TRANS_RENTAL_REQ_HEADER WHERE STATUS = 'CREATED' AND " +
                                "((RENTAL_REQUEST_NO) || (UPPER(RENTAL_REQUEST_TYPE)) || (UPPER(RENTAL_REQUEST_NAME)) LIKE '%' ||:c|| '%')";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql =
                            "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                            "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                            "FROM V_TRANS_RENTAL_REQ_HEADER WHERE STATUS = 'CREATED' AND BRANCH_ID=:c";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql,
                            new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql =
                                "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                                "FROM V_TRANS_RENTAL_REQ_HEADER WHERE STATUS = 'CREATED' AND " +
                                "((RENTAL_REQUEST_NO) || (UPPER(RENTAL_REQUEST_TYPE)) || (UPPER(RENTAL_REQUEST_NAME)) LIKE '%' ||:c|| '%') AND BRANCH_ID=:d";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransRentalRequest();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesTransRentalRequest GetDataTransRentalRequestApproved(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransRentalRequest result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_RENTAL_REQUEST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql =
                            "SELECT ID, RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                            "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, CONTRACT_OFFER_NUMBER " +
                            "FROM V_TRANS_RENTAL_REQ_HEADER WHERE STATUS = 'APPROVED' OR STATUS = 'REJECTED'";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql,
                            new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql =
                                "SELECT ID, RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, CONTRACT_OFFER_NUMBER " +
                                "FROM V_TRANS_RENTAL_REQ_HEADER WHERE (STATUS = 'APPROVED' OR STATUS = 'REJECTED') AND " +
                                "((RENTAL_REQUEST_NO) || (UPPER(RENTAL_REQUEST_TYPE)) || (UPPER(RENTAL_REQUEST_NAME)) LIKE '%' ||:c|| '%')";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql =
                            "SELECT ID, RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                            "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, CONTRACT_OFFER_NUMBER " +
                            "FROM V_TRANS_RENTAL_REQ_HEADER WHERE BRANCH_ID=:c AND (STATUS = 'APPROVED' OR STATUS = 'REJECTED')";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql,
                            new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql =
                                "SELECT ID, RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, " +
                                "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, CONTRACT_OFFER_NUMBER " +
                                "FROM V_TRANS_RENTAL_REQ_HEADER WHERE BRANCH_ID=:d AND (STATUS = 'APPROVED' OR STATUS = 'REJECTED') AND " +
                                "((RENTAL_REQUEST_NO) || (UPPER(RENTAL_REQUEST_TYPE)) || (UPPER(RENTAL_REQUEST_NAME)) LIKE '%' ||:c|| '%')";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransRentalRequest();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------- DATA DETAIL RENTAL REQUEST --------------------------
        public DataTablesTransRentalRequest GetDataDetailRental(int draw, int start, int length, string search, string id_rental)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransRentalRequest result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_RENTAL_REQUEST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql =
                        "SELECT OBJECT_ID, OBJECT_TYPE, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                        "FROM V_TRANS_RENTAL_REQ_DETIL " +
                        "WHERE RENTAL_REQUEST_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(fullSql,
                        new { a = start, b = end, c = id_rental });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_rental });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransRentalRequest();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- FILTER DATA DETAIL ---------------------
        public DataTablesRentalDetail GetDataFilterRentalDetail(int draw, int start, int length, string to, string from,
            string luas_tanah_from, string luas_tanah_to, string luas_bangunan_from, string luas_bangunan_to,
            string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesRentalDetail result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<V_SEARCH_DETAIL_RENTAL> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                // search parameter jika semua nilai 0 atau field kosong tidak diisi
                if (to.Length == 0 && from.Length == 0 && luas_tanah_to.Length == 0 && luas_tanah_from.Length == 0 &&
                    luas_bangunan_from.Length == 0 && luas_bangunan_to.Length == 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql =
                                "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL ORDER BY RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                            {
                                a = start,
                                b = end
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL " +
                                    "WHERE (UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') ORDER BY RO_NUMBER DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper()
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper()
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else
                {
                    // search parameter range tgl, luas tanah dan luas bangunan
                    if (to.Length > 0 && from.Length > 0 && luas_tanah_to.Length > 0 && luas_tanah_from.Length > 0 &&
                        luas_bangunan_from.Length > 0 && luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "ORDER BY RO_NUMBER DESC";

                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    VAL_FROM = from,
                                    VAL_TO = to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    VAL_FROM = from,
                                    VAL_TO = to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            try
                            {
                                if (search.Length > 2)
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";

                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper()
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper()
                                    });
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }

                    }
                    // search parameter tanah dan bangunan
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0 && luas_bangunan_from.Length > 0 &&
                             luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";

                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        c = search.ToUpper()
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        c = search.ToUpper()

                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    // search parameter range tgl dan bangunan
                    else if (from.Length > 0 && to.Length > 0 && luas_bangunan_from.Length > 0 &&
                             luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";

                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper(),
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper(),
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter range tgl dan luas tanah
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0 && from.Length > 0 &&
                             to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";

                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            c = search.ToUpper(),
                                            VAL_FROM = from,
                                            VAL_TO = to,
                                            TANAH_FROM = luas_tanah_from,
                                            TANAH_TO = luas_tanah_to
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper(),
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter range tgl
                    else if (from.Length > 0 && to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            VAL_FROM = from,
                                            VAL_TO = to,
                                            c = search.ToUpper()
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper()
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    // search parameter luas tanah
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";

                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            TANAH_FROM = luas_tanah_from,
                                            TANAH_TO = luas_tanah_to,
                                            c = search.ToUpper()
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        c = search.ToUpper()
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter luas bangunan
                    else if (luas_bangunan_from.Length > 0 && luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            BANGUNAN_FROM = luas_bangunan_from,
                                            BANGUNAN_TO = luas_bangunan_to,
                                            c = search.ToUpper()
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        c = search.ToUpper()
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //search untuk kode cabang
                // search parameter jika semua nilai 0 atau field kosong tidak diisi
                if (to.Length == 0 && from.Length == 0 && luas_tanah_to.Length == 0 && luas_tanah_from.Length == 0 &&
                    luas_bangunan_from.Length == 0 && luas_bangunan_to.Length == 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql =
                                "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL WHERE BRANCH_ID=:c ORDER BY RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = KodeCabang
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO FROM V_SEARCH_DETAIL_RENTAL " +
                                    "WHERE (UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') AND BRANCH_ID=:d ORDER BY RO_NUMBER DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper(),
                                    d = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search.ToUpper(),
                                    d = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else
                {
                    // search parameter range tgl, luas tanah dan luas bangunan
                    if (to.Length > 0 && from.Length > 0 && luas_tanah_to.Length > 0 && luas_tanah_from.Length > 0 &&
                        luas_bangunan_from.Length > 0 && luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND BRANCH_ID=:c ORDER BY RO_NUMBER DESC";

                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND BRANCH_ID=:d AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter tanah dan bangunan
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0 && luas_bangunan_from.Length > 0 &&
                             luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND BRANCH_ID=:c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') AND " +
                                        "BRANCH_ID=:d ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        d = KodeCabang
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    // search parameter range tgl dan bangunan
                    else if (from.Length > 0 && to.Length > 0 && luas_bangunan_from.Length > 0 &&
                             luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND BRANCH_ID = :c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') AND " +
                                        "BRANCH_ID = :d ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper(),
                                        d = KodeCabang,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to
                                    });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter range tgl dan luas tanah
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0 && from.Length > 0 &&
                             to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND BRANCH_ID = :c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "AND BRANCH_ID = :d ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            c = search.ToUpper(),
                                            d = KodeCabang,
                                            VAL_FROM = from,
                                            VAL_TO = to,
                                            TANAH_FROM = luas_tanah_from,
                                            TANAH_TO = luas_tanah_to,
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter range tgl
                    else if (from.Length > 0 && to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                    "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                    "AND BRANCH_ID = :c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    VAL_FROM = from,
                                    VAL_TO = to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE VALID_FROM <= TO_DATE(:VAL_FROM,'DD/MM/RRRR') " +
                                        "AND VALID_FROM <= TO_DATE(:VAL_TO,'DD/MM/RRRR') " +
                                        "AND BRANCH_ID = :d AND (UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            VAL_FROM = from,
                                            VAL_TO = to,
                                            c = search.ToUpper(),
                                            d = KodeCabang
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        VAL_FROM = from,
                                        VAL_TO = to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    // search parameter luas tanah
                    else if (luas_tanah_from.Length > 0 && luas_tanah_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND BRANCH_ID = :c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    TANAH_FROM = luas_tanah_from,
                                    TANAH_TO = luas_tanah_to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_TANAH_RO BETWEEN :TANAH_FROM AND :TANAH_TO) AND BRANCH_ID = :d AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            TANAH_FROM = luas_tanah_from,
                                            TANAH_TO = luas_tanah_to,
                                            c = search.ToUpper(),
                                            d = KodeCabang
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        TANAH_FROM = luas_tanah_from,
                                        TANAH_TO = luas_tanah_to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                    }
                    // search parameter luas bangunan
                    else if (luas_bangunan_from.Length > 0 && luas_bangunan_to.Length > 0)
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                    "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND BRANCH_ID = :c ORDER BY RO_NUMBER DESC";
                                fullSql = fullSql.Replace("sql", sql);
                                listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    c = KodeCabang
                                });
                                fullSqlCount = fullSqlCount.Replace("sql", sql);
                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    BANGUNAN_FROM = luas_bangunan_from,
                                    BANGUNAN_TO = luas_bangunan_to,
                                    c = KodeCabang
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            if (search.Length > 0)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER, OBJECT_ID, RO_NAME, LUAS_BANGUNAN_RO, LUAS_TANAH_RO, VALID_FROM, VALID_TO " +
                                        "FROM V_SEARCH_DETAIL_RENTAL WHERE (LUAS_BANGUNAN_RO BETWEEN :BANGUNAN_FROM AND :BANGUNAN_TO) AND BRANCH_ID = :d AND " +
                                        "(UPPER(RO_NAME) || UPPER(OBJECT_TYPE_ID) || UPPER(RO_NUMBER) || UPPER(OBJECT_ID) || LUAS_BANGUNAN_RO || LUAS_TANAH_RO || VALID_FROM || VALID_TO LIKE '%' ||:c|| '%') " +
                                        "ORDER BY RO_NUMBER DESC";
                                    fullSql = fullSql.Replace("sql", sql);
                                    listDataFilterDetail = connection.Query<V_SEARCH_DETAIL_RENTAL>(
                                        fullSql, new
                                        {
                                            a = start,
                                            b = end,
                                            BANGUNAN_FROM = luas_bangunan_from,
                                            BANGUNAN_TO = luas_bangunan_to,
                                            c = search.ToUpper(),
                                            d = KodeCabang
                                        });
                                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        BANGUNAN_FROM = luas_bangunan_from,
                                        BANGUNAN_TO = luas_bangunan_to,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }

            result = new DataTablesRentalDetail();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        private string varParam1 = string.Empty;
        //------------------------- INSERT DATA HEADER ---------------------
        public DataReturnTransRentalNumber AddHeader(string name, DataTransRentalRequest data, string kodeCabang)
        {

            //string id_rental_request = "";
            string rental_request_no = "";

            //int r = 0;
            // IDbConnection connectionOne = DatabaseFactory.GetConnection();
            //if (connectionOne.State.Equals(ConnectionState.Closed))
            //    connectionOne.Open();

            if (data.RENTAL_REQUEST_TYPE == "81")
            {
                // Ambil Generate ZC01 - PERMOHONAN BARU
                //rental_request_no = connectionOne.ExecuteScalar<string>("SELECT GEN_TRANS_RENTAL_ZC01 AS TRANS_RENTAL_ZC01 FROM DUAL");
                varParam1 = "11";
            }
            else if (data.RENTAL_REQUEST_TYPE == "82")
            {
                // Ambil Generate ZC02 - PERMOHONAN PERPANJANGAN 
                //rental_request_no = connectionOne.ExecuteScalar<string>("SELECT GEN_TRANS_RENTAL_ZC02 AS TRANS_RENTAL_ZC02 FROM DUAL");
                varParam1 = "12";
            }
            else if (data.RENTAL_REQUEST_TYPE == "83")
            {
                // Ambil Generate ZC03 - PERMOHONAN PERALIHAN 
                //rental_request_no = connectionOne.ExecuteScalar<string>("SELECT GEN_TRANS_RENTAL_ZC03 AS TRANS_RENTAL_ZC03 FROM DUAL");
                varParam1 = "13";
            }
            else
            {
                // Ambil Generate ZC04 - PERMOHONAN KERINGANAN
                //rental_request_no = connectionOne.ExecuteScalar<string>("SELECT GEN_TRANS_RENTAL_ZC04 AS TRANS_RENTAL_ZC04 FROM DUAL");
                varParam1 = "14";
            }
            string _BE_ID = "1";
            //patch 100: by redyarman. remark : change how to get ro_code
            rental_request_no = DatabaseHelper.GetPenomoranData(_BE_ID, "RENTAL_REQUEST", varParam1, 6);

            var resultfull = new DataReturnTransRentalNumber();
            if (!String.IsNullOrEmpty(rental_request_no))
            {
                resultfull = SetRentalRequestData(kodeCabang, rental_request_no, data, name);
            }
            else
            {
                resultfull.ID_TRANS = "";
                resultfull.RESULT_STAT = false;
                resultfull.MessageInfo = "Rental Request Number not found.";
            }
            return resultfull;
        }

        private DataReturnTransRentalNumber SetRentalRequestData(string kodeCabang, string rentalRequestNo, DataTransRentalRequest data, string updatedBy)
        {
            string sql = string.Empty;
            string sql1 = string.Empty;
            int r = 0;
            DataReturnTransRentalNumber ret = new DataReturnTransRentalNumber();
            ret.ID_TRANS = rentalRequestNo;
            ret.RESULT_STAT = false;
            ret.MessageInfo = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    if (!String.IsNullOrEmpty(rentalRequestNo))
                    {
                        sql = "INSERT INTO PROP_RENTAL_REQUEST " +
                              "(RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID, RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE, ACTIVE, BRANCH_ID,CREATION_BY) " +
                              "VALUES (:a, :b, :c, :d, to_date(:e,'DD/MM/YYYY'), to_date(:f,'DD/MM/YYYY'), :g, :h, :i, :j, :k, :l, :m, :n, :o) ";
                        try
                        {
                            r = connection.Execute(sql, new
                            {
                                a = rentalRequestNo,
                                b = data.RENTAL_REQUEST_TYPE,
                                c = data.BE_ID,
                                d = data.RENTAL_REQUEST_NAME,
                                e = data.CONTRACT_START_DATE,
                                f = data.CONTRACT_END_DATE,
                                g = data.CUSTOMER_ID,
                                h = data.CUSTOMER_AR,
                                i = data.CUSTOMER_NAME,
                                j = "CREATED",
                                k = data.OLD_CONTRACT,
                                l = data.CONTRACT_USAGE,
                                m = '1',
                                n = kodeCabang,
                                o = updatedBy
                            });
                            r = 1;
                            ret.RESULT_STAT = true;
                        }
                        catch (Exception)
                        {
                            r = 0;
                            throw;
                        }

                        if (data.Objects != null)
                        {

                            sql = "INSERT INTO PROP_RENTAL_REQUEST_DETIL " +
                                  "(RENTAL_REQUEST_NO, RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, BUILDING_DIMENSION, " +
                                  "LAND_DIMENSION, INDUSTRY, BRANCH_ID,CREATION_BY) " +
                                  "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i)";

                            foreach (var i in data.Objects)
                            {
                                try
                                {
                                    r = connection.Execute(sql, new
                                    {
                                        a = rentalRequestNo,
                                        b = i.RO_NUMBER,
                                        c = i.OBJECT_ID,
                                        d = i.OBJECT_TYPE_ID,
                                        e = (String.IsNullOrEmpty(i.BUILDING_DIMENSION) ? "0" : i.BUILDING_DIMENSION.Replace(',', '.')),
                                        f = (String.IsNullOrEmpty(i.LAND_DIMENSION) ? "0" : i.LAND_DIMENSION.Replace(',', '.')),
                                        g = i.INDUSTRY,
                                        h = kodeCabang,
                                        i = updatedBy
                                    });
                                    r = 1;
                                    ret.RESULT_STAT = true;
                                }
                                catch (Exception)
                                {
                                    r = 0;
                                    throw;
                                }
                            }

                            //UPDATE STATUS BOOKED DI PROP_RO_OCCUPANCY
                            sql1 = "UPDATE PROP_RO_OCCUPANCY SET STATUS = 'BOOKED' WHERE RO_NUMBER = :a AND STATUS = 'VACANT' ";

                            foreach (var z in data.Objects)
                            {
                                try
                                {
                                    r = connection.Execute(sql1, new
                                    {
                                        a = z.RO_NUMBER
                                    });
                                    r = 1;
                                    ret.RESULT_STAT = true;
                                }
                                catch (Exception)
                                {
                                    r = 0;
                                    throw;
                                }
                            }
                        }
                    }
                    //id_rental_request = connection.ExecuteScalar<string>("SELECT RENTAL_REQUEST_NO FROM PROP_RENTAL_REQUEST WHERE ID=(SELECT MAX(ID) FROM PROP_RENTAL_REQUEST)");
                }
                catch (Exception ex)
                {
                    r = 0;
                    ret.RESULT_STAT = false;
                    ret.MessageInfo = ex.Message;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return ret;
        }

        //------------------------- INSERT DATA DETAIL ---------------------
        //public bool AddDetail(string name, DataTransRentalRequest data, string KodeCabang)
        //{
        //    string id_rental = "";
        //    bool result = false;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    id_rental = data.RENTAL_REQUEST_NO;// connection.ExecuteScalar<string>("SELECT RENTAL_REQUEST_NO FROM PROP_RENTAL_REQUEST WHERE ID=(SELECT MAX(ID) FROM PROP_RENTAL_REQUEST)");

        //    string sql = "INSERT INTO PROP_RENTAL_REQUEST_DETIL " +
        //        "(RENTAL_REQUEST_NO, RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, BUILDING_DIMENSION, LAND_DIMENSION, INDUSTRY, BRANCH_ID,CREATION_BY) " +
        //        "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i)";

        //    int r = connection.Execute(sql, new
        //    {
        //        a = id_rental,
        //        b = data.RO_NUMBER,
        //        c = data.OBJECT_ID,
        //        d = data.OBJECT_TYPE_ID,
        //        e = data.BUILDING_DIMENSION,
        //        f = data.LAND_DIMENSION,
        //        g = data.INDUSTRY,
        //        h = KodeCabang,
        //        i = name
        //    });

        //    result = (r > 0) ? true : false;
        //    connection.Close();
        //    return result;
        //}

        //----------------------- VIEW EDIT DATA --------------------------------
        public AUP_TRANS_RENTAL_REQUEST_Remote GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE,'DD/MM/YYYY') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/YYYY') CONTRACT_END_DATE, " +
                             "CUSTOMER_ID, CUSTOMER_AR, CUSTOMER_NAME, STATUS, OLD_CONTRACT, CONTRACT_USAGE_NAME, ACTIVE, ID, CONTRACT_OFFER_NUMBER " +
                             "FROM V_TRANSRENTAL_REQ WHERE id = :a";

            AUP_TRANS_RENTAL_REQUEST_Remote result = connection.Query<AUP_TRANS_RENTAL_REQUEST_Remote>(sql, new { a = id }).FirstOrDefault();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------------------------------- EDIT DATA -----------------------------
        public bool Edit(string name, DataTransRentalRequest data, string KodeCabang, string UpdatedBy)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "UPDATE PROP_RENTAL_REQUEST SET " +
                         "RENTAL_REQUEST_TYPE=:b, BE_ID=:c, RENTAL_REQUEST_NAME=:d, CONTRACT_USAGE=:e, CUSTOMER_NAME=:f, CONTRACT_START_DATE=to_date(:g,'DD/MM/YYYY'), " +
                         "CONTRACT_END_DATE=to_date(:h,'DD/MM/YYYY'), CUSTOMER_ID=:i, CUSTOMER_AR=:j, OLD_CONTRACT=:k " +
                         "WHERE ID=:a";

            int r = connection.Execute(sql, new
            {
                a = data.ID,
                b = data.RENTAL_REQUEST_TYPE,
                c = data.BE_ID,
                d = data.RENTAL_REQUEST_NAME,
                e = data.CONTRACT_USAGE,
                f = data.CUSTOMER_NAME,
                g = data.CONTRACT_START_DATE,
                h = data.CONTRACT_END_DATE,
                i = data.CUSTOMER_ID,
                j = data.CUSTOMER_AR,
                k = data.OLD_CONTRACT
            });

            //Data Object
            if (data.Objects != null)
            {
                string sqlDel = "DELETE FROM PROP_RENTAL_REQUEST_DETIL WHERE RENTAL_REQUEST_NO=:a";

                int x = connection.Execute(sqlDel, new
                {
                    a = data.RENTAL_REQUEST_NO
                });

                sql = "INSERT INTO PROP_RENTAL_REQUEST_DETIL " +
                      "(RENTAL_REQUEST_NO, RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, BUILDING_DIMENSION, " +
                      "LAND_DIMENSION, INDUSTRY, BRANCH_ID,CREATION_BY) " +
                      "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i)";

                foreach (var i in data.Objects)
                {
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = data.RENTAL_REQUEST_NO,
                            b = i.RO_NUMBER,
                            c = i.OBJECT_ID,
                            d = i.OBJECT_TYPE_ID,
                            e = (String.IsNullOrEmpty(i.BUILDING_DIMENSION) ? "0" : i.BUILDING_DIMENSION.Replace(',', '.')),
                            f = (String.IsNullOrEmpty(i.LAND_DIMENSION) ? "0" : i.LAND_DIMENSION.Replace(',', '.')),
                            g = i.INDUSTRY,
                            h = KodeCabang,
                            i = UpdatedBy
                        });
                        //r = 1;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }
                }
            }

            result = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool StatusApproval(string updatedBy, DataTransRentalRequest data)
        {
            bool result = false;
            string XSTS = string.Empty;
            string XMSG = string.Empty;
            string sts = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            if (data.STATUS == "APPROVED")
            {
                string sql = "UPDATE PROP_RENTAL_REQUEST SET " +
                         "STATUS = :b, ACTIVE = '1', APPROVED_BY = :c, APPROVED_DATE = :d " +
                         "WHERE RENTAL_REQUEST_NO=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.RENTAL_REQUEST_NO,
                    b = data.STATUS,
                    c = updatedBy,
                    d = DateTime.Now
                });
                result = (r > 0) ? true : false;

            }
            else if (data.STATUS == "INACTIVE")
            {
                string sql = "UPDATE PROP_RENTAL_REQUEST SET " +
                         "ACTIVE = '0' " +
                         "WHERE RENTAL_REQUEST_NO=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.RENTAL_REQUEST_NO
                });
                result = (r > 0) ? true : false;
            }
            else
            {
                string sql = "UPDATE PROP_RENTAL_REQUEST SET " +
                         "STATUS = :b, ACTIVE = '0' " +
                         "WHERE RENTAL_REQUEST_NO=:a";

                int r = connection.Execute(sql, new
                {
                    a = data.RENTAL_REQUEST_NO,
                    b = data.STATUS
                });
                result = (r > 0) ? true : false;

                try
                {
                    var p = new DynamicParameters();
                    p.Add("P_RENTAL_REQUEST_NO", data.RENTAL_REQUEST_NO);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);

                    connection.Execute("SET_STATUS_BOOKED", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    var xyz = Convert.ToInt32(sts);

                    result = (xyz > 0) ? true : false;
                }
                catch (Exception)
                {
                    sts = "0";
                }

            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool StatusUpdate(string name, DataTransRentalRequest data)
        {
            bool result = false;
            string XSTS = string.Empty;
            string XMSG = string.Empty;
            string sts = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE PROP_RENTAL_REQUEST SET " +
                         "ACTIVE = '0' " +
                         "WHERE RENTAL_REQUEST_NO=:a";

            int r = connection.Execute(sql, new
            {
                a = data.RENTAL_REQUEST_NO
            });
            result = (r > 0) ? true : false;

            try
            {
                var p = new DynamicParameters();
                p.Add("P_RENTAL_REQUEST_NO", data.RENTAL_REQUEST_NO);
                p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);

                connection.Execute("SET_STATUS_BOOKED", p, null, null, commandType: CommandType.StoredProcedure);

                sts = p.Get<string>("XSTS");
                var xyz = Convert.ToInt32(sts);

                result = (xyz > 0) ? true : false;
            }
            catch (Exception)
            {
                sts = "0";
            }
            connection.Close();
            connection.Dispose();

            return result;
        }

        //------------------------------------ UBAH STATUS ----------------------
        public dynamic UbahStatus(string id, string no)
        {
            dynamic result = null;
            string XSTS = string.Empty;
            string XMSG = string.Empty;
            string sts = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "SELECT ACTIVE " +
                "FROM PROP_RENTAL_REQUEST " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_RENTAL_REQUEST " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    //result = new
                    //{
                    //    status = "S",
                    //    message = "Status is Inactive."
                    //};
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    //result = new
                    //{
                    //    status = "S",
                    //    message = "Status is Active."
                    //};
                }

                try
                {
                    var p = new DynamicParameters();
                    p.Add("P_RENTAL_REQUEST_NO", no);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);

                    connection.Execute("SET_STATUS_BOOKED", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    var xyz = Convert.ToInt32(sts);

                    result = (xyz > 0) ? true : false;
                }
                catch (Exception)
                {
                    sts = "0";
                }

            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {

                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else
            {

                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1 ";

            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE2(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            } 
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN RENTAL REQUEST TYPE ------------------------
        public IEnumerable<DDRentalType> GetDataRentalType()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'RENTAL_REQUEST_TYPE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRentalType> listDetil = connection.Query<DDRentalType>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN USAGE TYPE ------------------------
        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT_USAGE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listDetil = connection.Query<DDContractUsage>(sql);

            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDCurrency> GetDataCurrency()
        {
            string sql = "SELECT ID, CODE FROM CURRENCY";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDCurrency> listDetil = connection.Query<DDCurrency>(sql);

            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN USAGE TYPE ------------------------
        public IEnumerable<DDIndustry> GetDataIndustry()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDIndustry> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS REF_DESC1 FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'INDUSTRY' AND ACTIVE = '1' ORDER BY ID";

                listData = connection.Query<DDIndustry>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DT_DETAIL_RENTAL_REQUEST> GetDataDetailRentalEdit(string id, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DT_DETAIL_RENTAL_REQUEST> listData = null;

            try
            {
                string sql = "SELECT ID, RENTAL_REQUEST_NO, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, OBJECT_ID, RO_NAME, REF_DATA || ' - ' || REF_DESC AS OBJECT_TYPE, OBJECT_TYPE_ID, RO_NUMBER FROM V_EDIT_RENTAL_REQUEST WHERE RENTAL_REQUEST_NO=:a";

                listData = connection.Query<DT_DETAIL_RENTAL_REQUEST>(sql, new
                {
                    a = id,
                    b = KodeCabang
                });

            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return listData;
        }
        public DDProfitCenter GetDataPC(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            DDProfitCenter listData = null;

            try
            {
                string sql = @"select b.PROFIT_CENTER_ID, b.PROFIT_CENTER_ID || ' - ' || b.TERMINAL_NAME as TERMINAL_NAME, c.BE_ID as BRANCH_ID,c.BE_ID || ' - ' || c.BE_NAME as NAMA_PROFIT_CENTER from RENTAL_OBJECT a
                                join profit_center b on a.PROFIT_CENTER_ID_NEW = b.PROFIT_CENTER_ID
                                join BUSINESS_ENTITY c on a.BE_ID = c.BE_ID where a.RO_CODE = :a";

                listData = connection.Query<DDProfitCenter>(sql, new { a = id }).FirstOrDefault();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
    }
}