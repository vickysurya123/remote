﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransSpecialContract;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransSpecialContractDAL
    {

        //-------------------- DATA TABLE HEADER DATA --------------------
        public DataTablesSpecialContract GetDataTransSpecialContract(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string whereSql = search.Length > 2 ? " AND (CONTRACT_NO || UPPER(CONTRACT_NAME) || UPPER(BUSINESS_PARTNER_NAME) || UPPER(CONTRACT_START_DATE) || UPPER(CONTRACT_END_DATE)) LIKE '%' || '"+ search.ToUpper()+"' || '%')" : "";

            try { 
                string sql = "SELECT ID, BE_ID, BUSINESS_PARTNER, BUSINESS_PARTNER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, CONTRACT_NAME, CONTRACT_NO, CONTRACT_STATUS, TERM_IN_MONTHS, " +
                    "CONTRACT_TYPE, CONTRACT_USAGE, CURRENCY, CUSTOMER_AR, IJIN_PRINSIP_DATE, IJIN_PRINSIP_NO, LEGAL_CONTRACT_DATE, LEGAL_CONTRACT_NO, PROFIT_CENTER, STATUS " +
                    "FROM V_TRANS_SPECIAL_CONT_HEADER WHERE DIRECT_CONTRACT = '1' AND BRANCH_ID in ( SELECT DISTINCT BRANCH_ID FROM v_BE WHERE BRANCH_WHERE = :d) " + whereSql;

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
            } catch (Exception e) {
            }

            result = new DataTablesSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------- DATA DETAIL OBJECT --------------------------
        public DataTablesSpecialContract GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE,'DD.MM.YYYY') START_DATE, to_char(END_DATE,'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_TRANS_SPECIAL_CONT_OBJECT " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------- DATA DETAIL CONDITION --------------------------
        public DataTablesSpecialContract GetDataDetailCondition(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO,'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, AMT_REF, FREQUENCY, to_char(START_DUE_DATE,'DD.MM.YYYY') START_DUE_DATE, MANUAL_NO, " +
                             "FORMULA, MEASUREMENT_TYPE, LUAS_1, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, UNIT_PRICE, COA_PROD " +
                             "FROM V_TRANS_SPECIAL_CONT_CONDITION " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesSpecialContract GetDataDetailManualy(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                             "FROM V_TRANS_SPECIAL_CONT_MANUAL " +
                             "WHERE CONTRACT_NO=:c";
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql,
                        new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MEMO ----------------------
        public DataTablesSpecialContract GetDataDetailMemo(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_CALC, CONDITION_TYPE, MEMO " +
                             "FROM V_TRANS_SPECIAL_CONT_MEMO " +
                             "WHERE CONTRACT_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------------------- INSERT DATA CONTRACT -------------------------
        public DataReturnTransNumber AddHeader(DataTransContract data, string name, string KodeCabang)
        {
            int r = 0;
            string id_trans = "";
            string id_trans_vb = "";
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            var resultfull = new DataReturnTransNumber();
            // IDbConnection connection = DatabaseFactory.GetConnection();
            string _param1 = string.Empty;

            if (data.CONTRACT_TYPE == "ZC06")
            {
                //id_trans_vb = connection.ExecuteScalar<string>("SELECT GEN_CO_ZC01 AS TRANS_CODE FROM DUAL");
                _param1 = "36";
            }

            string _BE_ID = "1";
            id_trans_vb = DatabaseHelper.GetPenomoranData(_BE_ID, "CONTRACT_TRANS", _param1, 6);

            //id_trans_vb = GenerateContractOfferNumber(data, KodeCabang, RETURN_VALUE_BUFFER_SIZE, _param1);
            if (!String.IsNullOrEmpty(id_trans_vb))
            {
                resultfull = SetDataContractOffer(data, KodeCabang, id_trans_vb, name);
            }
            else
            {
                resultfull.ID_TRANS = string.Empty;
                resultfull.RESULT_STAT = false;
                resultfull.MessageResult = "Contract Number not found.";
            }
            return resultfull;
        }

        private DataReturnTransNumber SetDataContractOffer(DataTransContract data, string kodeCabang, string contractNo, string updatedBy)
        {
            int r = 0;
            DataReturnTransNumber ret = new DataReturnTransNumber();
            ret.ID_TRANS = contractNo;
            ret.MessageResult = string.Empty;
            string sql = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                try
                {
                    sql = "INSERT INTO PROP_CONTRACT " +
                    "(CONTRACT_NO, CONTRACT_TYPE, BE_ID, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, TERM_IN_MONTHS, BUSINESS_PARTNER_NAME,  " +
                    "BUSINESS_PARTNER, CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, IJIN_PRINSIP_NO, IJIN_PRINSIP_DATE, LEGAL_CONTRACT_NO, LEGAL_CONTRACT_DATE, " +
                    "CURRENCY, COMPANY_CODE, CONTRACT_STATUS, STATUS, DIRECT_CONTRACT, CREATION_BY, CREATION_DATE, BRANCH_ID) " +
                    "VALUES (:a, :b, :c, :d, TO_DATE(:e,'DD/MM/YYYY'), TO_DATE(:f,'DD/MM/YYYY'), :g, :h, :i, :j, :k, :l, :m, " +
                    "TO_DATE(:n,'DD/MM/YYYY'), :o, TO_DATE(:p,'DD/MM/YYYY'), :q, :r, :s, :t, :u, :v, SYSDATE, :x)";
                    try
                    {
                        r = connection.Execute(sql, new
                        {
                            a = contractNo,
                            b = data.CONTRACT_TYPE,
                            c = data.BE_ID,
                            d = data.CONTRACT_NAME,
                            e = data.CONTRACT_START_DATE,
                            f = data.CONTRACT_END_DATE,
                            g = data.TERM_IN_MONTHS,
                            h = data.BUSINESS_PARTNER_NAME,
                            i = data.BUSINESS_PARTNER,
                            j = data.CUSTOMER_AR,
                            k = data.PROFIT_CENTER,
                            l = data.CONTRACT_USAGE,
                            m = data.IJIN_PRINSIP_NO,
                            n = data.IJIN_PRINSIP_DATE,
                            o = data.LEGAL_CONTRACT_NO,
                            p = data.LEGAL_CONTRACT_DATE,
                            q = data.CURRENCY,
                            r = "1000",
                            s = "APPROVED",
                            t = "1",
                            u = "1",
                            v = updatedBy,
                            x = kodeCabang
                        });
                        r = 1;
                        ret.RESULT_STAT = true;
                    }
                    catch (Exception)
                    {
                        r = 0;
                        throw;
                    }

                    // insert contract object
                    if (data.Objects != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_OBJECT " +
                              "(CONTRACT_NO, OBJECT_TYPE, OBJECT_NAME, START_DATE, END_DATE, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY, OBJECT_ID) " +
                              "VALUES (:a, :b, :c, TO_DATE(:d,'DD/MM/YYYY'), TO_DATE(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";
                        foreach (var i in data.Objects)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractNo,
                                    b = i.OBJECT_TYPE,
                                    c = i.RO_NAME,
                                    d = data.CONTRACT_START_DATE,
                                    e = data.CONTRACT_END_DATE,
                                    f = i.LUAS_TANAH,
                                    g = i.LUAS_BANGUNAN,
                                    h = i.INDUSTRY,
                                    i = i.OBJECT_ID
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                    //AddDetailCondition(name, data.Conditions);
                    if (data.Conditions != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_CONDITION " +
                        "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, "+
                        "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, TAX_CODE, INSTALLMENT_AMOUNT, CONTRACT_NO, COA_PROD) " +
                        "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v)";
                        // var condition = data.Conditions;
                        foreach (var i in data.Conditions)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = i.CALC_OBJECT,
                                    b = i.CONDITION_TYPE,
                                    c = i.VALID_FROM,
                                    d = i.VALID_TO,
                                    e = i.MONTHS,
                                    f = i.STATISTIC,
                                    g = i.UNIT_PRICE,
                                    h = i.AMT_REF,
                                    i = i.FREQUENCY,
                                    j = i.START_DUE_DATE,
                                    k = i.MANUAL_NO,
                                    l = i.FORMULA,
                                    m = i.MEASUREMENT_TYPE,
                                    n = i.LUAS,
                                    o = i.TOTAL,
                                    p = i.NJOP_PERCENT,
                                    q = i.KONDISI_TEKNIS_PERCENT,
                                    r = i.TOTAL_NET_VALUE,
                                    s = i.TAX_CODE,
                                    t = i.INSTALLMENT_AMOUNT,
                                    u = contractNo,
                                    v = i.COA_PROD,
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }

                        }
                    }
                    sql = "INSERT INTO PROP_CONTRACT_FREQUENCY " +
                          "(CONTRACT_NO, MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT) " +
                          "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), :e, :g, :h)";
                    if (data.Frequencies != null)
                    {
                        foreach (var i in data.Frequencies)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractNo,
                                    b = i.MANUAL_NO,
                                    c = i.CONDITION,
                                    d = i.DUE_DATE,
                                    e = i.NET_VALUE,
                                    g = i.QUANTITY,
                                    h = i.UNIT
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }
                    if (data.Memos != null)
                    {
                        sql = "INSERT INTO PROP_CONTRACT_MEMO " +
                              "(CONTRACT_NO, OBJECT_CALC, CONDITION_TYPE, MEMO) " +
                              "VALUES (:a, :b, :c, :d)";
                        foreach (var i in data.Memos)
                        {
                            try
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = contractNo,
                                    b = i.OBJECT_CALC,
                                    c = i.CONDITION_TYPE,
                                    d = i.MEMO
                                });
                                r = 1;
                                ret.RESULT_STAT = true;
                            }
                            catch (Exception)
                            {
                                r = 0;
                                throw;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    r = 0;
                    ret.RESULT_STAT = false;
                    ret.MessageResult = ex.Message;
                }
            }

            return ret;
        }

        //----------------------------- GENERATE TEMPLATE BILLING -----------------------
        public bool prosedurBillingCashflow(string contractNo, string updateBy)
        {
            bool result = false;
            try
            {
                //OracleCommand cmd = (OracleCommand)DatabaseFactory.GetConnection().CreateCommand();
                using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
                {
                    //connection.Open();
                    if (connection.State.Equals(ConnectionState.Closed))
                        connection.Open();
                    OracleCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "PROP_BILLING.CREATE_BILLING";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.BindByName = true;
                    OracleParameter pContractNumber = new OracleParameter("p_contract_number", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = contractNo,
                        Size = 10
                    };
                    OracleParameter pUserName = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                        ParameterDirection.Input)
                    {
                        Value = updateBy,
                        Size = 50
                    };
                    /* jika mengembalikan nilai balik dari oracle 
                    OracleParameter pOut = new OracleParameter("ret", OracleDbType.Varchar2,
                        ParameterDirection.ReturnValue)
                    { Size = 50 };
                    */
                    cmd.Parameters.Add(pContractNumber);
                    cmd.Parameters.Add(pUserName);
                    //cmd.Parameters.AddArray<string>(
                    //    name: "list_kd_konfirmasi",
                    //    dbType: OracleDbType.Varchar2,
                    //    array: list,
                    //    direction: ParameterDirection.Input,
                    //    emptyArrayValue: null);
                    //cmd.Parameters.Add(pOut);

                    cmd.ExecuteNonQuery();

                    connection.Close();
                    connection.Dispose();
                    result = true;
                    //Convert.ToString(cmd.Parameters["ret"].Value);
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        //------------------- UBAH STATUS ----------------------
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT STATUS " +
                "FROM PROP_CONTRACT " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_CONTRACT " +
                    "SET STATUS = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------- LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE2(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0") {

                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
                
            }
            else {

                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID = :a ";

            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0") {

                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";

            }
            else {

                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID = :a and STATUS = 1";

            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN USAGE TYPE ------------------------
        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT_USAGE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listDetil = connection.Query<DDContractUsage>(sql);
            return listDetil;
        }

        //------------------- LIST DROP DOWN CONTRACT TYPE ------------------------
        public IEnumerable<DDContractType> GetDataContractType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractType> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT' AND ACTIVE = '1' ORDER BY ID";

                listData = connection.Query<DDContractType>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            return listData;
        }

        //-------------------------------- GET DATA ATTACHMENT --------------------------
        public DataTablesAttachmentContract GetDataAttachment(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesAttachmentContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONTRACT_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT ID_ATTACHMENT, DIRECTORY, FILE_NAME, CONTRACT_ID FROM PROP_CONTRACT_ATTACHMENT WHERE CONTRACT_ID=:c ORDER BY ID_ATTACHMENT DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_CONTRACT_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesAttachmentContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        
        //-------------------------------- ADD FILES ATTACHMENT ---------------------------------
        public bool AddFiles(string sFile, string directory, string KodeCabang, string subPath)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_CONTRACT_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, CONTRACT_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = subPath
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        //----------------------------------- DELETE ATTACHMENT ----------------------
        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM PROP_CONTRACT_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_CONTRACT_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}