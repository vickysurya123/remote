﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransWEBilling;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{ 
    public class TransWEBillingDAL
    {
        // --------------------- DEPRECATED ------------------------
        public DataTablesTransWEBilling  GetDataTransWEBilling(int draw, int start, int length, string search, string status_dinas, string KodeCabang)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;
            string where = "";
            where += x_status_dinas == "x" ? " " : " AND t.STATUS_DINAS = " + x_status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
                        
            if (KodeCabang == "0" && x_status_dinas == "NON KEDINASAN") 
            {
                try
                {
                    //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                    string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                                    REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
                                    CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'NON KEDINASAN' ORDER BY ID DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, x_status_dinas = x_status_dinas });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, x_status_dinas = x_status_dinas });
                }
                catch (Exception)
                {

                }

                if (search.Length == 0)
                {
                    try
                    {
                    //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                    string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, 
                                    METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, 
                                    SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'NON KEDINASAN' ORDER BY ID DESC";

                    
                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, x_status_dinas = x_status_dinas });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, x_status_dinas = x_status_dinas });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try {
                        /*
                        string sql = "SELECT * FROM V_TRANS_WE_LIST_HEADER WHERE CANCEL_STATUS IS NULL AND " +
                                     "((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (UPPER(TERMINAL_NAME)) || (UPPER(CUSTOMER)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                        */
                        string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND " +
                                     "((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL AND STATUS_DINAS = 'NON KEDINASAN' ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            x_status_dinas = x_status_dinas
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            x_status_dinas = x_status_dinas
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try {
                    string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, 
                                    INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND CANCEL_STATUS IS NULL AND STATUS_DINAS = 'NON KEDINASAN' ORDER BY ID DESC";
                    //string sql = "SELECT ID, INSTALLATION_TYPE, CUSTOMER, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, BILLING_TYPE FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang, x_status_dinas = x_status_dinas });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang, x_status_dinas = x_status_dinas });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try {
                        //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE ((ID LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (UPPER(CUSTOMER) LIKE '%'||:c||'%') OR (PERIOD LIKE '%'||:c||'%') OR (F_REPORT_ON LIKE '%'||:c||'%')) ORDER BY ID DESC";
                        string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:d AND CANCEL_STATUS IS NULL AND
                                     ((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL AND STATUS_DINAS = 'NON KEDINASAN' ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang,
                            x_status_dinas = x_status_dinas
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang,
                            x_status_dinas = x_status_dinas
                        });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }

            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        // --------------------- DEPRECATED ------------------------
        public DataTablesTransWEBilling GetDataTransWEBillingDinas(int draw, int start, int length, string search, string status_dinas, string KodeCabang)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;
            string where = "";
            where += x_status_dinas == "x" ? " " : " AND t.STATUS_DINAS = " + x_status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0" && x_status_dinas == "KEDINASAN")
            {
                try
                {
                    //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                    string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                                    REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, AMOUNT / NVL(RATE,1) TOTAL_USD,
                                    RATE,CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'KEDINASAN' ORDER BY ID DESC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, x_status_dinas = x_status_dinas });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, x_status_dinas = x_status_dinas });
                }
                catch (Exception)
                {

                }

                if (search.Length == 0)
                {
                    try
                    {
                        //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                        string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, 
                                    METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, RATE, AMOUNT / NVL(RATE,1) TOTAL_USD,
                                    SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'KEDINASAN' ORDER BY ID DESC";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, x_status_dinas = x_status_dinas });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, x_status_dinas = x_status_dinas });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            /*
                            string sql = "SELECT * FROM V_TRANS_WE_LIST_HEADER WHERE CANCEL_STATUS IS NULL AND " +
                                         "((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (UPPER(TERMINAL_NAME)) || (UPPER(CUSTOMER)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                            */
                            string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND " +
                                         "((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL AND STATUS_DINAS = 'KEDINASAN' ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                x_status_dinas = x_status_dinas
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                x_status_dinas = x_status_dinas
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, 
                                    INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND CANCEL_STATUS IS NULL AND STATUS_DINAS = 'KEDINASAN' ORDER BY ID DESC";
                        //string sql = "SELECT ID, INSTALLATION_TYPE, CUSTOMER, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, BILLING_TYPE FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang, x_status_dinas = x_status_dinas });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang, x_status_dinas = x_status_dinas });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE ((ID LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (UPPER(CUSTOMER) LIKE '%'||:c||'%') OR (PERIOD LIKE '%'||:c||'%') OR (F_REPORT_ON LIKE '%'||:c||'%')) ORDER BY ID DESC";
                            string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:d AND CANCEL_STATUS IS NULL AND
                                     ((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL AND STATUS_DINAS = 'KEDINASAN' ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang,
                                x_status_dinas = x_status_dinas
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang,
                                x_status_dinas = x_status_dinas
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }

                }
            }

            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //public DataTablesTransWEBilling  GetDataTransWEBilling(int draw, int start, int length, string search, string status_dinas, string KodeCabang)
        //{
        //    string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;
        //    string where = "";
        //    where += x_status_dinas == "x" ? " " : " AND t.STATUS_DINAS = " + x_status_dinas;

        //    int count = 0;
        //    int end = start + length;
        //    DataTablesTransWEBilling result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

        //    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
        //    string fullSqlCount = "SELECT count(*) FROM (sql)";

        //    if (KodeCabang == "0" && x_status_dinas == "NON KEDINASAN") 
        //    {
        //        if (search.Length == 0 && x_status_dinas == "NON KEDINASAN")
        //        {
        //            try
        //            {
        //            //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
        //            string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL " + where + " ORDER BY ID DESC" ;


        //            fullSql = fullSql.Replace("sql", sql);

        //            listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end });

        //            fullSqlCount = fullSqlCount.Replace("sql", sql);

        //            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
        //            }
        //            catch (Exception)
        //            {

        //            }
        //        }
        //        else
        //        {
        //            //search filter data
        //            if (search.Length > 2 && x_status_dinas == "NON KEDINASAN")
        //            {
        //                try {
        //                /*
        //                string sql = "SELECT * FROM V_TRANS_WE_LIST_HEADER WHERE CANCEL_STATUS IS NULL AND " +
        //                             "((ID) || (UPPER(INSTALLATION_TYPE)) || (PROFIT_CENTER) || (UPPER(TERMINAL_NAME)) || (UPPER(CUSTOMER)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
        //                */
        //                string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL AND " +
        //                             "((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL " + where + "ORDER BY ID DESC";

        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
        //                {
        //                    a = start,
        //                    b = end,
        //                    c = search.ToUpper()
        //                });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new
        //                {
        //                    a = start,
        //                    b = end,
        //                    c = search.ToUpper()
        //                });
        //                }
        //                catch (Exception)
        //                {

        //                }
        //            }

        //        }
        //    }
        //    else
        //    {
        //        if (search.Length == 0 && x_status_dinas == "NON KEDINASAN")
        //        {
        //            try {
        //            string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND CANCEL_STATUS IS NULL " + where + " ORDER BY ID DESC";
        //            //string sql = "SELECT ID, INSTALLATION_TYPE, CUSTOMER, PERIOD, AMOUNT, STATUS, POSTING_DATE, SAP_DOCUMENT_NUMBER, INSTALLATION_CODE, BILLING_TYPE FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:c AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";

        //            fullSql = fullSql.Replace("sql", sql);

        //            listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

        //            fullSqlCount = fullSqlCount.Replace("sql", sql);

        //            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
        //            }
        //            catch (Exception)
        //            {

        //            }
        //        }
        //        else
        //        {
        //            //search filter data
        //            if (search.Length > 2 && x_status_dinas == "NON KEDINASAN")
        //            {
        //                try {
        //                //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE ((ID LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (UPPER(CUSTOMER) LIKE '%'||:c||'%') OR (PERIOD LIKE '%'||:c||'%') OR (F_REPORT_ON LIKE '%'||:c||'%')) ORDER BY ID DESC";
        //                string sql = "SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING WHERE BRANCH_ID=:d AND CANCEL_STATUS IS NULL AND " +
        //                             "((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' ||:c|| '%' ) AND SAP_DOCUMENT_NUMBER IS NULL " + where + " ORDER BY ID DESC";

        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new
        //                {
        //                    a = start,
        //                    b = end,
        //                    c = search.ToUpper(),
        //                    d = KodeCabang
        //                });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new
        //                {
        //                    a = start,
        //                    b = end,
        //                    c = search.ToUpper(),
        //                    d = KodeCabang
        //                });
        //                }
        //                catch (Exception)
        //                {

        //                }
        //            }

        //        }
        //    }

        //    result = new DataTablesTransWEBilling();
        //    result.draw = draw;
        //    result.recordsFiltered = count;
        //    result.recordsTotal = count;
        //    result.data = listDataOperator.ToList();
        //    connection.Close();            
        //    return result;
        //}

        //------------------------------- DATA TABLE TRANS WE --------------------------

        public DataTablesTransWEBilling GetDataTransactionWE(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try {
                string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE ID=:c AND INSTALLATION_CODE=:d";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d=installation_code });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();

            return result;
        }

        public DataTablesTransWEBilling GetDataTransactionWEDinas(int draw, int start, int length, string search, string id_trans, string installation_code)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE ID=:c AND INSTALLATION_CODE=:d";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = id_trans, d = installation_code });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id_trans, d = installation_code });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();

            return result;
        }

        //-------------------POSTING BILLING-----------------
        public dynamic PostingBilling(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            /*
            string sql = "SELECT ACTIVE " +
                "FROM SERVICES " +
                "WHERE SERVICES_ID = :a";
            */
            string sql = "SELECT SAP_DOCUMENT_NUMBER FROM V_TRANS_WE_BILLING WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_SERVICES_TRANSACTION " +
                    "SET SAP_DOCUMENT_NUMBER = '1' " +
                    "WHERE ID = :a";

                if (kdAktif == null)
                {
                    connection.Execute(sql, new
                    {
                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Billing Has Been Posted."
                    };
                }
                else
                {
                    result = new
                    {
                        status = "E",
                        message = "Something went wrong, billing cannot be posted."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }


        //SEND DATA TO SAP
        public DataReturnSAP_COCUMENT_NUMBER PostSAP(string BILL_ID, string BILL_TYPE, string POSTING_DATE)
        {
            /*
            IDbConnection connection = DatabaseFactory.GetConnection();
            string XDOC_NUMBER = string.Empty;
            var p = new DynamicParameters();
            p.Add("XBILL_ID", BILL_ID);
            p.Add("XBILL_TYPE", BILL_TYPE);
            p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

            connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
            string retVal = p.Get<string>("XDOC_NUMBER");
            */

            IDbConnection connection = DatabaseFactory.GetConnection();

            try
            {
                string sql = "UPDATE PROP_SERVICES_TRANSACTION " +
                  "SET POSTING_DATE=TO_DATE(:a,'DD/MM/YYYY') " +
                  "WHERE ID = :b";
                connection.Execute(sql, new
                {
                    a = POSTING_DATE,
                    b = BILL_ID
                });
            }
            catch (Exception) { }

            string XDOC_NUMBER = string.Empty;
            string retVal = null;
            string tahun = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("XBILL_ID", BILL_ID);
                p.Add("XBILL_TYPE", BILL_TYPE);
                p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

                connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
                retVal = p.Get<string>("XDOC_NUMBER");
            }
            catch (Exception ex) {
                var aaa = ex.ToString();
                retVal = null;            
            }
           
            if (string.IsNullOrEmpty(retVal) == false && retVal!="/0000")
            {
                try
                {
                  string sql = "UPDATE PROP_SERVICES_TRANSACTION " +
                  "SET SAP_DOCUMENT_NUMBER =:a, POSTING_DATE=to_date(:c,'DD/MM/YYYY') " +
                  "WHERE ID = :b";

                    connection.Execute(sql, new
                    {
                        a = retVal,
                        b = BILL_ID,
                        c = POSTING_DATE
                    });
                }
                catch (Exception) {
                    retVal = null;
                }
            }

            // Tambahan pengecekan apakah retVal == /0000
            if (tahun == "/0000")
            {
                // Update Value E_DOCNUMBER di ITGR
                string cekEDOCNUMBER = "UPDATE ITGR_LOG WHERE E_DOCNUMBER='/0000' AND REF_DOC_NO='" + BILL_ID + "'";

                string cekITGRHEADER = connection.ExecuteScalar<string>("SELECT HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'");

                if (string.IsNullOrEmpty(cekITGRHEADER) == false)
                {
                    string hapusITGRHEADER = "DELETE FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'";
                    string hapusITGRITEM = "DELETE FROM ITGR_ITEM WHERE HEADER_ID='" + cekITGRHEADER + "'";
                }
            }
            connection.Close();
            connection.Dispose();

            var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
            resultfull.DOCUMENT_NUMBER = retVal;
            resultfull.BILL_ID = BILL_ID;
            //resultfull.RESULT_STAT = (retVal != null) ? true : false;
            return resultfull;
        }

        public Array GetDataMsgSAP(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string xml = "";
            try
            {
                string a = "SELECT MAX(HEADER_ID) HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO=:REF_DOC_NO";
                string getID = connection.ExecuteScalar<string>(a, new
                {
                    REF_DOC_NO = BILL_ID
                });

                string sql = "SELECT XML_DATA FROM VW_ITGR_LOG " +
                  "WHERE HEADER_ID = :HEADER_ID";

                xml = connection.ExecuteScalar<string>(sql, new
                {
                    HEADER_ID = getID
                });
            }
            catch (Exception ex) { }
            string[] msg;
            if (string.IsNullOrEmpty(xml))
            {
                msg = Array.Empty<string>();
            }
            else
            {
                msg = xml.Split('|');
            }

            connection.Close();
            connection.Dispose();
            return msg;
        }

        //-------------------GET DATA TRANS WE BILLING-----------------
        public DataTablesTransWEBilling GetDataTransWEBillingnew(int draw, int start, int length, string search, string status_dinas, string KodeCabang, string UserID)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;
            string where = "";
            where += x_status_dinas == "x" ? " " : " AND t.STATUS_DINAS = " + x_status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' || '" + search.ToUpper() + "' || '%' ) " : "");
            
            //if (x_status_dinas == "NON KEDINASAN")
            //{
                try
                {
                //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                //string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                //                REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
                //                CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS 
                //                ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID  = :user_id AND a.JENIS_TRANSAKSI = '2-Air') ROLE_WATER
                //                ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID  = :user_id AND a.JENIS_TRANSAKSI = '3-Listrik') ROLE_ELECTRICITY
                //                FROM V_TRANS_WE_BILLING 
                //                WHERE CANCEL_STATUS IS NULL AND (STATUS_DINAS != 'KEDINASAN' or STATUS_DINAS is null) AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :c) " + wheresearch + " ORDER BY ID DESC";

                string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                                    REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
                                    CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS, RATE, AMOUNT / NVL(RATE,1) TOTAL_USD 
                                    FROM V_TRANS_WE_BILLING 
                                    WHERE CANCEL_STATUS IS NULL AND (STATUS_DINAS != 'KEDINASAN' or STATUS_DINAS is null) AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :c) " + wheresearch + " ORDER BY ID DESC";

                fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang, user_id = UserID });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                }
                
                catch (Exception)
                {

                }
            
            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesTransWEBilling GetDataTransWEBillingDinasnew(int draw, int start, int length, string search, string status_dinas, string KodeCabang, string UserID)
        {
            string x_status_dinas = string.IsNullOrEmpty(status_dinas) ? "x" : status_dinas;
            string where = "";
            where += x_status_dinas == "x" ? " " : " AND t.STATUS_DINAS = " + x_status_dinas;

            int count = 0;
            int end = start + length;
            DataTablesTransWEBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_WE_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((ID) || (UPPER(INSTALLATION_TYPE)) || (UPPER(PROFIT_CENTER)) || (POWER_CAPACITY) || (UPPER(CUSTOMER_NAME)) || (PERIOD) LIKE '%' || '" + search.ToUpper() + "' || '%' ) " : "");

            //if (x_status_dinas == "NON KEDINASAN")
            //{
            try
            {
                //string sql = "SELECT * FROM V_TRANS_WE_BILLING WHERE CANCEL_STATUS IS NULL ORDER BY ID DESC";
                //string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                //                    REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
                //                    CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS 
                //                    ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID  = :user_id AND a.JENIS_TRANSAKSI = '2-Air') ROLE_WATER
                //                    ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID  = :user_id AND a.JENIS_TRANSAKSI = '3-Listrik') ROLE_ELECTRICITY
                //                    FROM V_TRANS_WE_BILLING 
                //                    WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'KEDINASAN' AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :c) " + wheresearch + " ORDER BY ID DESC";

                string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
                                    REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
                                    CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS, RATE, AMOUNT / NVL(RATE,1) TOTAL_USD 
                                    FROM V_TRANS_WE_BILLING 
                                    WHERE CANCEL_STATUS IS NULL AND STATUS_DINAS = 'KEDINASAN' AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :c) " + wheresearch + " ORDER BY ID DESC";


                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_WE_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang, user_id = UserID });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
            }

            catch (Exception)
            {

            }

            result = new DataTablesTransWEBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        public string PostSilapor(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            var resultfull = "E";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("BILL_NO", BILL_ID);

                connection.Execute("SILAPOR_WE", p, null, null, commandType: CommandType.StoredProcedure);
                resultfull = "S";
            }
            catch (Exception)
            {
            }
            return resultfull;
        }
    }
}