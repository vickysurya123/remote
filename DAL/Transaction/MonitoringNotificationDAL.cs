﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using Remote.Models.EmailConfiguration;
using Remote.Models.GeneralResult;
using Remote.Models.NotificationContract;
using Remote.Models.UserModels;

namespace Remote.DAL
{
    public class MonitoringNotificationDAL
    {

        //------------------------------- DEPRECATED ---------------------
        public IDataTable GetData(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND " +
                                "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BADAN_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(ADDRESS) LIKE '%' ||:c|| '%') ";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND
                                        BRANCH_ID=:c ";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND BRANCH_ID =:d AND " +
                                        "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BADAN_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(ADDRESS) LIKE '%' ||:c|| '%') ";
                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DEPRECATED ---------------------
        public IDataTable GetFilter(int draw, int start, int length, string search, string KodeCabang, string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string where = string.Empty;

            // PRIORITY 1: CONTRACT_NO
            if (!contract_no.Equals("0"))
            {
                where += " AND CONTRACT_NO = '" + contract_no + "' ";
            } else {
                // PRIORITY 2: PROFIT_CENTER > BE_ID
                if (!profit_center.Equals("0"))
                {
                    where += " AND PROFIT_CENTER = '" + profit_center + "' ";
                } else
                if (!be_id.Equals("0"))
                {
                    where += " AND BE_ID = '" + be_id + "' ";
                }

                // PRIORITY 2: CUSTOMER_ID > BADAN_USAHA
                if (!customer_id.Equals("0"))
                {
                    where += " AND CUSTOMER_ID = '" + customer_id + "' ";
                } else
                if (badan_usaha.Equals("PERORANGAN"))
                {
                    where += " AND BADAN_USAHA = '" + badan_usaha + "' ";
                } else
                if (badan_usaha.Equals("PERUSAHAAN"))
                {
                    where += " AND BADAN_USAHA != 'PERORANGAN' ";
                } 

                // PRIORITY 2: START DATE 
                if (!date_start.Equals("x"))
                {
                    where += " AND CONTRACT_START_DATE > TO_DATE('" + date_start + "', 'DD/MM/YYYY') ";
                }

                // PRIORITY 2: END DATE
                if (!date_end.Equals("x"))
                {
                    where += " AND CONTRACT_END_DATE < TO_DATE('" + date_end + "', 'DD/MM/YYYY') ";
                }
            }

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 " + where;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND " +
                                        "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BADAN_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(ADDRESS) LIKE '%' ||:c|| '%') "
                                        + where;

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND
                                        BRANCH_ID=:c " + where;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE FIRST = 1 AND BRANCH_ID =:d AND " +
                                        "(CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(BADAN_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(ADDRESS) LIKE '%' ||:c|| '%') "
                                        + where;
                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DEPRECATED ---------------------
        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        
        public AUP_BE_WEBAPPS GetHeader(string kd_cabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_BE_WEBAPPS result = new AUP_BE_WEBAPPS();
            
            string sql = @"select a.BRANCH_ID, a.BE_NAME, a.BE_ID, a.BE_ADDRESS, a.BE_CITY from BUSINESS_ENTITY a
                            where a.BRANCH_ID = :a ";
            result = connection.Query<AUP_BE_WEBAPPS>(sql, new
            {
                a = kd_cabang
            }).DefaultIfEmpty(null).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public EmailConf GetPrint(string contract_no, string peringatan_no, string kd_cabang, string type)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IDbConnection connectionRepo = DatabaseFactory.GetConnection("app_repo");
            if (connectionRepo.State.Equals(ConnectionState.Closed))
                connectionRepo.Open();
            string email_to = "";
            string name = "";
            EmailConf result = new EmailConf();
            DataUser res = null;
            var sql = @"select CREATION_BY, BUSINESS_PARTNER, CONTRACT_NAME from PROP_CONTRACT where CONTRACT_NO =:a";
            dynamic temp = connection.Query<dynamic>(sql, new
            {
                a = contract_no
            }).DefaultIfEmpty(null).FirstOrDefault();
            if (temp != null)
            {
                if (type == "user")
                {
                    var createby = temp.CREATION_BY.Split(new string[] { " | " }, StringSplitOptions.None);
                    sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                                where a.USER_LOGIN = :a and a.APP_ID = 1";
                    res = connectionRepo.Query<DataUser>(sql, new
                    {
                        a = createby[0],
                        b = kd_cabang
                    }).DefaultIfEmpty(null).FirstOrDefault();
                    if (res != null)
                    {
                        email_to = res.USER_EMAIL;
                        name = createby[1];
                    }
                }
                else // pj
                {
                    sql = @"select MPLG_NAMA, MPLG_EMAIL_ADDRESS from VW_CUSTOMERS where MPLG_KODE =:a and KD_CABANG =:b ";
                    dynamic customer = connection.Query<dynamic>(sql, new
                    {
                        a = temp.BUSINESS_PARTNER,
                        b = kd_cabang
                    }).DefaultIfEmpty(null).FirstOrDefault();
                    if (customer != null)
                    {
                        email_to = customer.MPLG_EMAIL_ADDRESS;
                        name = customer.MPLG_NAMA;
                    }
                }

                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                var body = dal.bodyContract(contract_no, peringatan_no, type, name);
                body += dal.DataCabang(kd_cabang);
                // body += dal.emailfooter();

                result.EMAIL_TO = email_to;
                result.CASE = "1";//1.email to user || 2.email to pengguna jasa
                result.BODY = body;
                result.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                result.SUBJECT = "Pemberitahuan " + peringatan_no + " " + temp.CONTRACT_NAME;
                
            }
            else
            {

            }

            connectionRepo.Close();
            connectionRepo.Dispose();
            connection.Close();
            connection.Dispose();

            return result;
        }

        //------------------------------- NEW DATA TABLE HEADER DATA ---------------------
        public IDataTable GetDatanew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND (CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(BADAN_USAHA) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(ADDRESS) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
                           
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE STOP_WARNING_DATE IS NULL " + v_be + wheresearch;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }                                                  

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- NEW DATA TABLE HEADER DATA FILTER ---------------------
        public IDataTable GetFilternew(int draw, int start, int length, string search, string KodeCabang, string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_badan_usaha = string.IsNullOrEmpty(badan_usaha) ? "x" : badan_usaha;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;


            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            //string where = string.Empty;
            string where = "";

            //string wheresearch = (search.Length >= 2 ? " AND (CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(BADAN_USAHA) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(ADDRESS) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            // PRIORITY 1: CONTRACT_NO
            if (!x_contract_no.Equals("0"))
            {
                where += " AND CONTRACT_NO = '" + x_contract_no + "' ";
            }
            
                // PRIORITY 2: PROFIT_CENTER > BE_ID
                if (!x_profit_center.Equals("0"))
                {
                    //where += " AND PROFIT_CENTER = '" + x_profit_center + "' ";
                }
                
                if (!x_be_id.Equals("0"))
                {
                    where += " AND BRANCH_ID = '" + x_be_id + "' ";
                }

                // PRIORITY 2: CUSTOMER_ID > BADAN_USAHA
                if (!x_customer_id.Equals("0"))
                {
                    where += " AND CUSTOMER_ID = '" + x_customer_id + "' ";
                }
                else
                if (x_badan_usaha.Equals("PERORANGAN"))
                {
                    where += " AND BADAN_USAHA = '" + x_badan_usaha + "' ";
                }
                else
                if (x_badan_usaha.Equals("PERUSAHAAN"))
                {
                    where += " AND BADAN_USAHA != 'PERORANGAN' ";
                }

                // PRIORITY 2: START DATE 
                if (!x_date_start.Equals("x"))
                {
                    where += " AND CONTRACT_START_DATE > TO_DATE('" + x_date_start + "', 'DD/MM/YYYY') ";
                }

                // PRIORITY 2: END DATE
                if (!x_date_end.Equals("x"))
                {
                    where += " AND CONTRACT_END_DATE < TO_DATE('" + x_date_end + "', 'DD/MM/YYYY') ";
                }
            
                        
                    try
                    {
                        string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE STOP_WARNING_DATE IS NULL " + where + v_be;

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new {
                            a = start,
                            b = end,
                            d = KodeCabang,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_date_end = x_date_end, 
                            x_date_start = x_date_start,
                            x_badan_usaha = x_badan_usaha,
                            x_contract_no = x_contract_no,
                            x_customer_id = x_customer_id
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new {
                            a = start,
                            b = end,
                            d = KodeCabang,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_date_end = x_date_end,
                            x_date_start = x_date_start,
                            x_badan_usaha = x_badan_usaha,
                            x_contract_no = x_contract_no,
                            x_customer_id = x_customer_id
                        });
                    }
                    catch (Exception)
                    {

                    }               

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();

            connection.Dispose();
            return result;
        }

        //------------------------------- NEW DATA TABLE HEADER DATA FILTER HISTORY---------------------
        public IDataTable GetFilterHistory(int draw, int start, int length, string search, string KodeCabang, string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_badan_usaha = string.IsNullOrEmpty(badan_usaha) ? "x" : badan_usaha;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;


            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            //string where = string.Empty;
            string where = "";

            //string wheresearch = (search.Length >= 2 ? " AND (CONTRACT_NO LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(BADAN_USAHA) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(ADDRESS) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            // PRIORITY 1: CONTRACT_NO
            if (!x_contract_no.Equals("0"))
            {
                where += " AND CONTRACT_NO = '" + x_contract_no + "' ";
            }

            // PRIORITY 2: PROFIT_CENTER > BE_ID
            if (!x_profit_center.Equals("0"))
            {
                //where += " AND PROFIT_CENTER = '" + x_profit_center + "' ";
            }

            if (!x_be_id.Equals("0"))
            {
                where += " AND BRANCH_ID = '" + x_be_id + "' ";
            }

            // PRIORITY 2: CUSTOMER_ID > BADAN_USAHA
            if (!x_customer_id.Equals("0"))
            {
                where += " AND CUSTOMER_ID = '" + x_customer_id + "' ";
            }
            else
            if (x_badan_usaha.Equals("PERORANGAN"))
            {
                where += " AND BADAN_USAHA = '" + x_badan_usaha + "' ";
            }
            else
            if (x_badan_usaha.Equals("PERUSAHAAN"))
            {
                where += " AND BADAN_USAHA != 'PERORANGAN' ";
            }

            // PRIORITY 2: START DATE 
            if (!x_date_start.Equals("x"))
            {
                where += " AND CONTRACT_START_DATE > TO_DATE('" + x_date_start + "', 'DD/MM/YYYY') ";
            }

            // PRIORITY 2: END DATE
            if (!x_date_end.Equals("x"))
            {
                where += " AND CONTRACT_END_DATE < TO_DATE('" + x_date_end + "', 'DD/MM/YYYY') ";
            }


            try
            {
                string sql = @"SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, 
                                        TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, 
                                        NVL(TO_CHAR(STOP_NOTIFICATION_DATE, 'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE,
                                        MEMO_NOTIFICATION, DAYS_REMAINING || ' DAYS' AS DAYS,
                                        CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, 
                                        EMAIL, CONTRACT_TYPE_DESC CONTRACT_TYPE, CONTRACT_NAME, BE_ID, BRANCH_ID, BE_NAME, PROFIT_CENTER, PROFIT_CENTER_NAME, 
                                        BADAN_USAHA, FIRST, SECOND, THIRD, NVL(TO_CHAR(STOP_WARNING_DATE, 'DD.MM.RRRR'), '-') STOP_WARNING_DATE, NVL(MEMO_WARNING, '-') MEMO_WARNING
                                        FROM V_MONITOR_NOTIF WHERE STOP_WARNING_DATE IS NOT NULL " + where + v_be;

                fullSql = fullSql.Replace("sql", sql);

                listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang,
                    x_be_id = x_be_id,
                    x_profit_center = x_profit_center,
                    x_date_end = x_date_end,
                    x_date_start = x_date_start,
                    x_badan_usaha = x_badan_usaha,
                    x_contract_no = x_contract_no,
                    x_customer_id = x_customer_id
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang,
                    x_be_id = x_be_id,
                    x_profit_center = x_profit_center,
                    x_date_end = x_date_end,
                    x_date_start = x_date_start,
                    x_badan_usaha = x_badan_usaha,
                    x_contract_no = x_contract_no,
                    x_customer_id = x_customer_id
                });
            }
            catch (Exception)
            {

            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();

            connection.Dispose();
            return result;
        }

        //------------------------------- NEW DATA BUSINESS ENTITY ---------------------
        public IEnumerable<DDBusinessEntity> GetDataBusinessEntitynew(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
                       
                try
                {
                    string sql = "SELECT BE_ID, BRANCH_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";

                    listData = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang }).ToList();
                }
                catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        
        public bool ConfirmationDate(string updatedBy, DataNotificationContract data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE PROP_CONTRACT SET " +
                         "STOP_WARNING_DATE = TO_DATE(:a,'DD/MM/YYYY'), MEMO_WARNING = :b, STOP_WARNING_BY = :c " +
                         "WHERE CONTRACT_NO=:d";

            int r = connection.Execute(sql, new
            {
                a = data.STOP_WARNING_DATE,
                b = data.MEMO_WARNING,
                c = updatedBy,
                d = data.CONTRACT_NO
            });

            result = (r > 0) ? true : false;

            string sql1 = "UPDATE PROP_CONTRACT_NOTIFICATION SET " +
                         "STATUS_REMINDER = '1' " +
                         "WHERE CONTRACT_NO=:d";

            int r1 = connection.Execute(sql1, new
            {
                d = data.CONTRACT_NO
            });

            result = (r1 > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}
