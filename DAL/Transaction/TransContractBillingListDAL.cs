﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransContractBilling;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransContractBillingListDAL
    {
        //------------------------------- DEPRECATED ---------------------
        public DataTablesTransContractBillingList GetDataTransContractBillingList(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBillingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, BE_ID, BE_ID || ' - ' || BE_NAME AS BE_NAME, CONTRACT_NAME, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, CURRENCY_CODE, " +
                                "BILLING_NO, SAP_DOC_NO, NO_REF1, NO_REF2, NO_REF3, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, " +
                                "to_char(CREATION_DATE,'DD.MM.YYYY') CREATION_DATE, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER_NAME, CANCEL_STATUS, BRANCH_ID " +
                                "FROM V_CONTRACT_LIST_BILLING ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, BE_ID || ' - ' || BE_NAME AS BE_NAME, CONTRACT_NAME, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, CURRENCY_CODE, " +
                                "BILLING_NO, SAP_DOC_NO, NO_REF1, NO_REF2, NO_REF3, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, " +
                                "to_char(CREATION_DATE,'DD.MM.YYYY') CREATION_DATE, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER_NAME, CANCEL_STATUS,BRANCH_ID " +
                                "FROM V_CONTRACT_LIST_BILLING WHERE " +
                                "(BILLING_NO LIKE '%'||:c||'%') OR (BILLING_CONTRACT_NO LIKE '%'||:c||'%') OR  (UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%'||:c||'%') OR " +
                                "(SAP_DOC_NO LIKE '%'||:c||'%') OR (BE_ID LIKE '%'||:c||'%') OR (CUSTOMER_CODE LIKE '%'||:c||'%') " +
                                "ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, BE_ID, BE_ID || ' - ' || BE_NAME AS BE_NAME, CONTRACT_NAME, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, CURRENCY_CODE, " +
                                "BILLING_NO, SAP_DOC_NO, NO_REF1, NO_REF2, NO_REF3, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, " +
                                "to_char(CREATION_DATE,'DD.MM.YYYY') CREATION_DATE, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER_NAME, CANCEL_STATUS, BRANCH_ID " +
                                "FROM V_CONTRACT_LIST_BILLING WHERE BRANCH_ID=:c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, BE_ID || ' - ' || BE_NAME AS BE_NAME, CONTRACT_NAME, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, CURRENCY_CODE, " +
                                "BILLING_NO, SAP_DOC_NO, NO_REF1, NO_REF2, NO_REF3, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, " +
                                "to_char(CREATION_DATE,'DD.MM.YYYY') CREATION_DATE, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER_NAME, CANCEL_STATUS,BRANCH_ID " +
                                "FROM V_CONTRACT_LIST_BILLING WHERE BRANCH_ID=:d AND "+
                                "((BILLING_NO LIKE '%'||:c||'%') OR (BILLING_CONTRACT_NO LIKE '%'||:c||'%') OR (UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%'||:c||'%') OR " +
                                "(SAP_DOC_NO LIKE '%'||:c||'%') OR (BE_ID LIKE '%'||:c||'%') OR (CUSTOMER_CODE LIKE '%'||:c||'%')) " +
                                "ORDER BY ID DESC";
                                
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                        catch (Exception) 
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransContractBillingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA DETAIL LIST BILLING --------------------------
        public DataTablesTransContractBillingList GetDataDetailContractBillingList(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBillingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT REF_DESC, OBJECT_ID, INSTALLMENT_AMOUNT, CURRENCY_CODE, BILLING_QTY, BILLING_UOM, GL_ACOUNT_NO, TO_CHAR(BILLING_DUE_DATE,'DD.MM.RRRR') BILLING_DUE_DATE, BILLING_FLAG_TAX1 " +
                             "FROM V_CONTRACT_LIST_BILLING_DETAIL " +
                             "WHERE BILLING_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractBillingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA DETAIL LIST DETAIL --------------------------
        public DataTablesTransContractBillingList GetDataDetailContractBillingListDetail(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBillingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT REF_DESC, OBJECT_ID, INSTALLMENT_AMOUNT, CURRENCY_CODE, BILLING_QTY, BILLING_UOM, GL_ACOUNT_NO, BILLING_FLAG_TAX1 " +
                             "FROM V_CONTRACT_LIST_BILLING_DETAIL " +
                             "WHERE BILLING_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractBillingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Array GetDataMsgSAP(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string xml = "";
            try
            {
                /*string a = "SELECT MAX(HEADER_ID) HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO=:REF_DOC_NO";
                string getID = connection.ExecuteScalar<string>(a, new
                {
                    REF_DOC_NO = BILL_ID
                });*/

                string sql = "SELECT XML_DATA FROM VW_ITGR_LOG " +
                  "WHERE REF_DOC_NO = :REF_DOC_NO";

                xml = connection.ExecuteScalar<string>(sql, new
                {
                    REF_DOC_NO = BILL_ID
                });
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            string[] msg = xml.Split('|');

            return msg;
        }

        //------------------------------- DATA TABLE HEADER DATA ---------------------
        public DataTablesTransContractBillingList GetDataTransContractBillingListNew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBillingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            var v_be = new QueryGlobal().Query_BE();
            var where = @"AND ((BILLING_NO LIKE '%'||:c||'%') OR (BILLING_CONTRACT_NO LIKE '%'||:c||'%') OR  (UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR 
                        (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (UPPER(CONTRACT_NAME) LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%'||:c||'%') OR 
                        (SAP_DOC_NO LIKE '%'||:c||'%') OR (BE_ID LIKE '%'||:c||'%') OR (CUSTOMER_CODE LIKE '%'||:c||'%')) ";
            where = search.Length > 2 ? where : "";

            //search filter data
            string sql = "SELECT ID, BE_ID, BE_ID || ' - ' || BE_NAME AS BE_NAME, CONTRACT_NAME, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, CURRENCY_CODE, " +
                        "BILLING_NO, SAP_DOC_NO, NO_REF1, NO_REF2, NO_REF3, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, " +
                        "to_char(CREATION_DATE,'DD.MM.YYYY') CREATION_DATE, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER_NAME, CANCEL_STATUS,BRANCH_ID " +
                        "FROM V_CONTRACT_LIST_BILLING WHERE 1 = 1 " + where + v_be + 
                        "ORDER BY ID DESC";
            try {

                fullSql = fullSql.Replace("sql", sql);
                fullSqlCount = fullSqlCount.Replace("sql", sql);

                if (! string.IsNullOrEmpty(where)) {
                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        d = KodeCabang
                    });
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search.ToUpper(), d = KodeCabang });
                } else {
                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(fullSql, new
                    {
                        a = start,
                        b = end,
                        d = KodeCabang
                    });
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
                }
            }
            catch (Exception)
            {

            }

            result = new DataTablesTransContractBillingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}