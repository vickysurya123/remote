﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.PendingList;
using Remote.Models.TransContractOffer;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Remote.Controllers;
using Remote.Models.SettingDisposisi;
using Remote.Models.ListNotifikasi;

namespace Remote.DAL
{
    public class PendingListDAL
    {
        //-------------------------------------- DEPRECATED ------------------------------
        public DataTablesPendingList GetDataHeaderContractOffer(int draw, int start, int length, string search, string KodeCabang, string UserRole, string roleProperty)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (UserRole == "16") //SM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL IN ('60', '80', '90') AND COMPLETED_STATUS = '2' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                         "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                         "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                         "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL IN ('60', '80',  '90') AND COMPLETED_STATUS = '2' AND " +
                                         "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                         "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "17") //DIREKTUR OPERASIONAL
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '70' AND COMPLETED_STATUS = '2' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                         "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                         "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                         "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '70' AND COMPLETED_STATUS = '2' AND " +
                                         "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                         "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "4" || UserRole == "7") //STAFF PUSAT & ADMIN
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE COMPLETED_STATUS = '2' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                          "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                          "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                          "FROM V_CONTRACT_OFFER_HEADER WHERE COMPLETED_STATUS = '2' AND " +
                                          "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                          "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                }
            }
            else
            {
                if (UserRole == "12") //MANAGER
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '30' AND COMPLETED_STATUS = '2' AND BRANCH_ID = :c  ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                         "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                         "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                         "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :d AND APPROVAL_NEXT_LEVEL = '30' AND COMPLETED_STATUS = '2' AND " +
                                         "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                         "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search, d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "4") //STAFF PUSAT & ADMIN
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :c AND COMPLETED_STATUS = '2' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                          "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                          "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                          "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :d AND COMPLETED_STATUS = '2' AND " +
                                          "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                          "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                }
                else if (UserRole == "13") //DEPUTI GM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '40' AND COMPLETED_STATUS = '2' AND BRANCH_ID = :c ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                         "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                         "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                         "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '40' AND COMPLETED_STATUS = '2' AND BRANCH_ID = :d AND " +
                                         "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                         "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search, d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "15") //GM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :c AND COMPLETED_STATUS = '2' AND APPROVAL_NEXT_LEVEL = '50' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                         "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                         "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI " +
                                         "FROM V_CONTRACT_OFFER_HEADER WHERE APPROVAL_NEXT_LEVEL = '50' AND COMPLETED_STATUS = '2' AND BRANCH_ID = :d AND " +
                                         "((CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                         "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search, d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        //--------------------------------------- DEPRECATED ----------------------
        public DataTablesPendingList GetDataHistoryWorkflow(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC AS STATUS_CONTRACT, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC AS STATUS_CONTRACT, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c AND BRANCH_ID = :d ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        //------------------------------------- VIEW APPROVE PAGE --------------------------------
        public AUP_PENDING_LIST_WEBAPPS GetDataForApprovePage(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_NAME, " +
                       "(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS, APPROVAL_NEXT_LEVEL, " +
                       "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, DISPOSISI_PARAF " +
                       "FROM V_CONTRACT_OFFER_HEADER WHERE CONTRACT_OFFER_NO = :a ORDER BY ID_INC DESC";

            AUP_PENDING_LIST_WEBAPPS result = connection.Query<AUP_PENDING_LIST_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE OBJECT --------------------------
        public DataTablesPendingList GetDataObject(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {

                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE CONDITION --------------------------
        public DataTablesPendingList GetDataCondition(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {

                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE MANUAL --------------------------
        public DataTablesPendingList GetDataManual(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, to_char(DUE_DATE,'DD.MM.YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                             "FROM V_CONTRACT_OFFER_MANUAL " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE MEMO --------------------------
        public DataTablesPendingList GetDataMemo(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------------------- DEPRECATED ----------------------
        public ResultInformation setContractOfferApproval(ContractOfferApprovalParameter param)
        {
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            ResultInformation ret = new ResultInformation();
            // string ret = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                OracleCommand cmd = connection.CreateCommand();
                cmd.CommandText = "PROC_PROP_RUN_WORKFLOW";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;
                cmd.Parameters.Add("returnVal", OracleDbType.Varchar2, RETURN_VALUE_BUFFER_SIZE);
                cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                OracleParameter pContractOfferNo = new OracleParameter("p_contract_offer_no", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = param.CONTRACT_OFFER_NO,
                    Size = 100
                };
                cmd.Parameters.Add(pContractOfferNo);

                OracleParameter pUserId = new OracleParameter("P_USER_LOGIN", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = param.USER_LOGIN,
                    Size = 100
                };
                cmd.Parameters.Add(pUserId);
                OracleParameter plevelApprove = new OracleParameter("p_level_approve", OracleDbType.Varchar2,
                   ParameterDirection.Input)
                {
                    Value = param.LEVEL_NO,
                    Size = 100
                };
                cmd.Parameters.Add(plevelApprove);
                OracleParameter pApproveType = new OracleParameter("p_approve_type", OracleDbType.Varchar2,
                   ParameterDirection.Input)
                {
                    Value = param.APPROVAL_TYPE,
                    Size = 100
                };
                cmd.Parameters.Add(pApproveType);
                OracleParameter pNote = new OracleParameter("p_notes", OracleDbType.Clob,
                  ParameterDirection.Input)
                {
                    Value = String.IsNullOrEmpty(param.APPROVAL_NOTE) ? "" : param.APPROVAL_NOTE
                };
                cmd.Parameters.Add(pNote);
                OracleParameter pUpdateBy = new OracleParameter("p_update_by", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = param.LAST_UPDATE_BY,
                    Size = 100
                };
                cmd.Parameters.Add(pUpdateBy);
                //OracleParameter pOut = new OracleParameter("pContractNo", OracleDbType.Varchar2,
                //        ParameterDirection.Output)
                //{ Size = 50 };
                //cmd.Parameters.Add(pOut);
                try
                {
                    cmd.ExecuteNonQuery();
                    ret.TransactionCode = cmd.Parameters["returnVal"].Value.ToString();
                    ret.ResultCode = "S";
                    ret.ResultDescription = "Success";
                }
                catch (Exception ex)
                {
                    ret.TransactionCode = String.Empty;
                    ret.ResultCode = "E";
                    ret.ResultDescription = "Error: " + ex.Message;
                }
                finally
                {
                    connection.Close();

                    connection.Dispose();
                    cmd.Dispose();
                }
            }
            return ret;
        }

        public DataTablesAttachmentOffer GetDataAttachment(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesAttachmentOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_OFFER_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT ID_ATTACHMENT, DIRECTORY, FILE_NAME, CONTRACT_OFFER_ID FROM PROP_CONTRACT_OFFER_ATTACHMENT WHERE CONTRACT_OFFER_ID=:c ORDER BY ID_ATTACHMENT DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_OFFER_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesAttachmentOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();

            connection.Dispose();
            return result;
        }

        public bool AddFiles(string sFile, string directory, string KodeCabang, string subPath)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_CONTRACT_OFFER_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, CONTRACT_OFFER_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = subPath
            });

            result = (r > 0) ? true : false;

            connection.Close();

            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();

            connection.Dispose();
            return result;
        }

        //-------------------------------- APPROVED WORKFLOW ----------------------
        public ResultInformation setContractOfferApprovalnew(ContractOfferApprovalParameter param, dynamic dataUser)
        {
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            string nameUser = dataUser.Name;
            string idUser = dataUser.UserID;
            string namaEmpl = dataUser.UserRoleName;
            string propertyRole = dataUser.UserRoleID;
            string userLogin = dataUser.UserLoginID;

            int status;
            ResultInformation ret = new ResultInformation();
            AUP_TRANS_CONTRACT_OFFER_WEBAPPS co = null;
            EmailConfigurationDAL dal = new EmailConfigurationDAL();
            MonitoringAnjunganDAL dalM = new MonitoringAnjunganDAL();
            var stat = "";
            AUP_TRANS_CONTRACT_OFFER_WEBAPPS res = null;

            // string ret = string.Empty;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        //query mencari maker
                        var query = @"select CREATION_BY from PROP_CONTRACT_OFFER where CONTRACT_OFFER_NO = :a";
                        string maker = connection.ExecuteScalar<string>(query, new
                        {
                            a = param.CONTRACT_OFFER_NO
                        });
                        string[] makers = maker.Split(new[] { " | " }, StringSplitOptions.RemoveEmptyEntries);

                        //var makers = maker.Split(new[] { " | " }, StringSplitOptions.None);

                        //query mencari maker
                        query = @"select BRANCH_ID from PROP_CONTRACT_OFFER where CONTRACT_OFFER_NO = :a";
                        string branch = connection.ExecuteScalar<string>(query, new
                        {
                            a = param.CONTRACT_OFFER_NO
                        });

                        //query mencari approval
                        ApprovalSettingDAL dalrole = new ApprovalSettingDAL();
                        dynamic role = dalrole.SetApprovalRole(param.CONTRACT_OFFER_NO, param.APPROVAL_TYPE);

                        dynamic IDSurat = null;
                        dynamic istype = null;
                        int SURAT_ID = 0;
                        //cari data surat yg belum di approve
                        var dataSurat = @" SELECT * FROM PROP_SURAT WHERE CONTRACT_NO = :a AND STATUS_APV = 0";
                        IDSurat = connection.Query(dataSurat, new { a = param.CONTRACT_OFFER_NO }).DefaultIfEmpty(null).FirstOrDefault();

                        if (IDSurat != null)
                        {
                            SURAT_ID = Convert.ToInt32(IDSurat.ID);
                        }
                        //update contract offer
                        if (role.Status == "S")
                        {
                            var sql = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                    to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                    CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                    TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                    CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                    OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY  
                                    from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                            res = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(sql, new
                            {
                                a = param.CONTRACT_OFFER_NO
                            }).DefaultIfEmpty(null).FirstOrDefault();

                            if (param.APPROVAL_TYPE == "2")
                            {
                                int apv_disposisi = 0;
                                dynamic userlogin = "";
                                
                                apv_disposisi = 0;

                                if(IDSurat != null)
                                {
                                    //jika tgl surat null
                                    if (IDSurat.TANGGAL == null)
                                    {
                                        //update data surat
                                        var updateSurat = @"UPDATE PROP_SURAT A SET STATUS_APV =:a, A.TANGGAL =:c WHERE A.ID =:b  ";

                                        int ry = connection.Execute(updateSurat, new
                                        {
                                            a = 1,
                                            b = IDSurat.ID,
                                            c = DateTime.Now
                                        }, transaction: transaction);
                                    }
                                    else
                                    {
                                        //update data surat
                                        var updateSurat = @"UPDATE PROP_SURAT A SET STATUS_APV =:a WHERE A.ID =:b  ";

                                        //NO_SURAT = :nomor_surat

                                        int ry = connection.Execute(updateSurat, new
                                        {
                                            a = 1,
                                            b = IDSurat.ID
                                        }, transaction: transaction);
                                    }
                                }

                                var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                                    values(:a, :b, :c, :d, :e, :f, :g)";
                                int w = connection.Execute(sql2, new
                                {
                                    a = SURAT_ID,
                                    b = param.CONTRACT_OFFER_NO,
                                    c = idUser,
                                    d = param.APPROVAL_NOTE,
                                    e = "Approved",
                                    f = 0,
                                    g = namaEmpl,
                                }, transaction: transaction);

                                sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                                string offer_status = connection.ExecuteScalar<string>(sql, new
                                {
                                    a = res.OFFER_STATUS
                                });

                                sql = @"insert into PROP_WORKFLOW_LOG (CONTRACT_OFFER_NO,STATUS_CONTRACT,USER_LOGIN,BRANCH_ID,CREATION_DATE,CREATION_BY,LAST_UPDATE_DATE,LAST_UPDATE_BY,NOTES,LOG_STATUS,STATUS_CONTRACT_DESC)
                                values(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k)";
                                var log = connection.Execute(sql, new
                                {
                                    a = param.CONTRACT_OFFER_NO,
                                    b = res.OFFER_STATUS,
                                    c = param.USER_LOGIN, //userlogin
                                    d = param.BRANCH_ID,
                                    e = DateTime.Now,
                                    f = param.USER_LOGIN,
                                    g = DateTime.Now,
                                    h = param.USER_LOGIN,
                                    i = param.APPROVAL_NOTE,
                                    j = "Done",
                                    k = offer_status,
                                }, transaction: transaction);

                                if (role.Data.now_apv != role.Data.top_apv)
                                {
                                    //create orang yg akan disposisi
                                    dynamic roleID = "";
                                    if (role.Data.role_name != null)
                                    {
                                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                                        {
                                            string cariRole = @"select role_id ID from app_role where property_role =:b and role_name =:a ";
                                            roleID = conn.Query(cariRole, new { a = role.Data.role_name, b = role.Data.next_apv }).DefaultIfEmpty(null).FirstOrDefault();

                                        }
                                    }
                                    //cari data setting disposisi
                                    IEnumerable<SettingDisposisi> dataSettingDisposisi = null;
                                    var settingDisposisi = @"SELECT * FROM V_SETTING_DISPOSISI A WHERE A.ROLE_ID =:a ";
                                    dataSettingDisposisi = connection.Query<SettingDisposisi>(settingDisposisi, new
                                    {
                                        a = roleID.ID,
                                    });
                                    if(dataSettingDisposisi.Count() > 0)
                                    {
                                        var name_before = "";
                                        var userid_before = "";
                                        var user_login_before = "";
                                        foreach (SettingDisposisi temp in dataSettingDisposisi)
                                        {
                                            if(temp.URUTAN == 0)
                                            {
                                                name_before = temp.NAME;
                                                userid_before = temp.USER_ID;
                                                user_login_before = temp.USER_LOGIN;

                                                var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF ( URUTAN, TYPE, CONTRACT_NO, SURAT_ID, IS_CREATE_SURAT, ROLE_ID,IS_ACTION_DISPOSISI) 
                                                VALUES(:a, :b, :c, :d, :e, :f, :g)";
                                                connection.Execute(sql3, new
                                                {
                                                    a = 0,
                                                    b = "disposisi",
                                                    c = param.CONTRACT_OFFER_NO,
                                                    d = SURAT_ID,
                                                    e = 0,
                                                    f = roleID.ID,
                                                    g = 1,
                                                }, transaction: transaction);

                                            }
                                            if (temp.URUTAN > 0)
                                            {
                                                int is_action_disposisi = 1;
                                                if (temp.URUTAN + 1 == dataSettingDisposisi.Count())
                                                {
                                                    is_action_disposisi = 0;
                                                }
                                                var sqlPropDisposisi = @"insert into PROP_DISPOSISI (CONTRACT_NO, NO_SURAT, SUBJECT, MEMO, DISPOSISI_KE, SURAT_ID, USER_ID_CREATE, IS_DELETE, INSTRUKSI, IS_CREATE_SURAT)
                                                values(:b, :c, :g, :a, :f, :e, :d, :h, :i, :j)";
                                                int z = connection.Execute(sqlPropDisposisi, new
                                                {
                                                    a = temp.MEMO,
                                                    b = param.CONTRACT_OFFER_NO,
                                                    c = IDSurat.NO_SURAT,
                                                    d = temp.USER_ID,
                                                    e = IDSurat.ID,
                                                    f = userid_before,
                                                    g = IDSurat.SUBJECT,
                                                    h = 0,
                                                    i = temp.INSTRUKSI,
                                                    j = 0,
                                                }, transaction: transaction);
                                                var cari = @" SELECT MAX(ID) ID FROM PROP_DISPOSISI WHERE USER_ID_CREATE = :a ";
                                                var ID = connection.Query<int>(cari, new { a = userid_before });

                                                var sqlDisposisiParaf = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, SURAT_ID, IS_CREATE_SURAT, 
                                                DISPOSISI_ID, MAX_URUTAN, NAMA, IS_ACTION_DISPOSISI, COUNT_SURAT) 
                                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)";
                                                connection.Execute(sqlDisposisiParaf, new
                                                {
                                                    a = temp.USER_ID,
                                                    b = temp.URUTAN,
                                                    c = "disposisi",
                                                    d = param.CONTRACT_OFFER_NO,
                                                    e = IDSurat.ID,
                                                    f = 0,
                                                    g = ID,
                                                    h = temp.URUTAN,
                                                    i = temp.NAME,
                                                    j = is_action_disposisi,
                                                    k = IDSurat.COUNT_SURAT
                                                }, transaction: transaction);

                                                var sqlPropHistory = @"insert into PROP_HISTORY (SURAT_ID, DISPOSISI_ID, CONTRACT_NO, USER_ID_CREATE, 
                                                MEMO, SUBJECT, TYPE, IS_DELETE, INSTRUKSI, IS_CREATE_SURAT, NAMA_KEPADA, NAMA)
                                                values(:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l)";
                                                int x = connection.Execute(sqlPropHistory, new
                                                {
                                                    a = IDSurat.ID,
                                                    b = ID,
                                                    c = param.CONTRACT_OFFER_NO,
                                                    d = userid_before,
                                                    e = temp.MEMO,
                                                    f = IDSurat.SUBJECT,
                                                    g = "Disposisi",
                                                    h = 0,
                                                    i = temp.INSTRUKSI,
                                                    j = 0,
                                                    k = temp.NAME,
                                                    l = name_before,
                                                }, transaction: transaction);

                                                dynamic dataEmail = null;
                                                using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                                                {
                                                    var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' - '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                                                    dataEmail = conn.Query(cariEmail, new { b = temp.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                                                }

                                                var sendingemail = dal.emailDisposisi(res, IDSurat, temp.KD_CABANG, "setujui", param, userLogin, name_before, dataEmail, temp.MEMO);

                                                ListNotifikasiController listNotif = new ListNotifikasiController();
                                                ParamNotification paramNotif = new ParamNotification();
                                                paramNotif.username = temp.USER_LOGIN;
                                                paramNotif.type = "2"; //TYPE DISPOSISI
                                                paramNotif.notification = temp.MEMO;
                                                paramNotif.no_contract = param.CONTRACT_OFFER_NO;
                                                paramNotif.pengirim = user_login_before;
                                                var sendNotif = listNotif.addNewNotif(paramNotif);

                                                name_before = temp.NAME;
                                                userid_before = temp.USER_ID;
                                                user_login_before = temp.USER_LOGIN;
                                            }
                                        }
                                    } else
                                    {
                                        var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF ( URUTAN, TYPE, CONTRACT_NO, SURAT_ID, IS_CREATE_SURAT, ROLE_ID) 
                                        VALUES(:a, :b, :c, :d, :e, :f)";
                                        connection.Execute(sql3, new
                                        {
                                            a = 0,
                                            b = "disposisi",
                                            c = param.CONTRACT_OFFER_NO,
                                            d = SURAT_ID,
                                            e = 0,
                                            f = roleID.ID,
                                        }, transaction: transaction);
                                    }
                                    
                                }
                                else
                                {
                                    //proses approval terakhir
                                    param.LAST_UPDATE_BY = nameUser;

                                }

                                stat = "setujui";
                                status = (role.Data.now_apv == role.Data.top_apv ? 1 : 2);
                                sql = @"update PROP_CONTRACT_OFFER a set COMPLETED_STATUS =:b , OFFER_STATUS =:c, APPROVAL_NEXT_LEVEL=:d, TOP_APPROVED_LEVEL=:e,
                                        APV_POSITION=:f, LAST_APPROVAL_NIK=:g, APPROVE_MEMO =:h , DISPOSISI_PARAF =:i, APV_DISPOSISI =:j where a.CONTRACT_OFFER_NO = :a";
                                var r = connection.Execute(sql, new
                                {
                                    a = param.CONTRACT_OFFER_NO,
                                    b = status,
                                    c = role.Data.now_apv,
                                    d = role.Data.next_apv,//sesuai data approval
                                    e = role.Data.top_apv,//sesuai data approval
                                    f = role.Data.apv_position,
                                    g = param.USER_LOGIN, //login
                                    h = param.APPROVAL_NOTE, //login
                                    i = "disposisi",
                                    j = apv_disposisi,
                                }, transaction: transaction);

                                res.COMPLETED_STATUS = status.ToString();
                                res.OFFER_STATUS = role.Data.now_apv.ToString();
                                res.TOP_APPROVED_LEVEL = role.Data.top_apv.ToString();
                                res.APV_POSITION = role.Data.apv_position.ToString();

                                if (status == 2)
                                {
                                    //proses kirim email
                                    param.LAST_UPDATE_BY = nameUser;
                                    if (role.Data.now_apv != role.Data.top_apv)
                                    {
                                        if (IDSurat != null)
                                        {
                                            var user = dal.SearchUserRole(role.Data.next_apv, branch, res, stat, param, role.Data.det_id, IDSurat, namaEmpl, role.Data.role_name, 5, userLogin);
                                        }
                                    } 
                                    
                                    ret.ResultCode = "S";
                                    ret.ResultDescription = "Success";
                                    ret.TransactionCode = param.CONTRACT_OFFER_NO;

                                }
                                else // proses approve terakhir
                                {

                                    r = dalM.SetStatusByContractOffer("APPROVED", param.CONTRACT_OFFER_NO);

                                    //get kode bayar Manual F
                                    SetKodeBayarDetail(param.CONTRACT_OFFER_NO, res.CUSTOMER_ID, res.BRANCH_ID.ToString(), param.USER_LOGIN);
                                    if(IDSurat != null)
                                    {
                                        istype = 8;
                                        var user = dal.spamEmail(res, IDSurat, branch, stat, param, namaEmpl, istype);
                                    }

                                    //var user = dal.SearchUserRole(role.Data.next_apv, branch, res, stat, param, role.Data.det_id, IDSurat, namaEmpl);
                                }
                            }
                            else if (param.APPROVAL_TYPE == "3" || param.APPROVAL_TYPE == "4")// revisi atau reject
                            {

                                stat = (param.APPROVAL_TYPE == "3" ? "revisi" : "tolak");
                                //APV_POSITION =:f,
                                sql = @"update PROP_CONTRACT_OFFER a set COMPLETED_STATUS =:b , OFFER_STATUS =:c,
                                        LAST_APPROVAL_NIK=:g where a.CONTRACT_OFFER_NO = :a";
                                var r = connection.Execute(sql, new
                                {
                                    a = param.CONTRACT_OFFER_NO,
                                    b = param.APPROVAL_TYPE,
                                    c = role.Data.now_apv,
                                    //f = role.Data.apv_position,
                                    g = param.USER_LOGIN, //login
                                }, transaction: transaction);

                                res.COMPLETED_STATUS = param.APPROVAL_TYPE;
                                res.OFFER_STATUS = role.Data.now_apv.ToString();
                                res.APV_POSITION = role.Data.apv_position;

                                if (param.APPROVAL_TYPE == "3")
                                {
                                    //cari data surat
                                    if (param.DISPOSISI_PARAF == "disposisi")
                                    {
                                        sql = @"update PROP_CONTRACT_OFFER a set LAST_REVISED_NIK =:b, a.APV_DISPOSISI = 0 ,REVISE_MEMO =:c, ACTIVE = 1 where a.CONTRACT_OFFER_NO = :a";
                                        r = dalM.SetStatusByContractOffer("REVISED", param.CONTRACT_OFFER_NO);

                                        var updateSurat = @"UPDATE PROP_SURAT A SET A.STATUS_KIRIM =:a, A.APV_LEVEL =:d ,A.DISPOSISI_MAX =:c WHERE A.ID =:b ";
                                        int ry = connection.Execute(updateSurat, new
                                        {
                                            a = "Draft",
                                            b = SURAT_ID,
                                            c = 0,
                                            d = 0,
                                        }, transaction: transaction);

                                        var BeforeDataSurat = @"SELECT A.COUNT_SURAT FROM PROP_SURAT A
                                            WHERE A.CONTRACT_NO =:a AND A.ID =:c ";
                                        dynamic hasilBefore = connection.Query(BeforeDataSurat, new { a = param.CONTRACT_OFFER_NO, c = SURAT_ID }).FirstOrDefault();
                                        int countSurat = 1;
                                        if(Convert.ToInt32(hasilBefore.COUNT_SURAT) != 1)
                                        {
                                            countSurat = Convert.ToInt32(hasilBefore.COUNT_SURAT) - 1;
                                        }
                                        var dataSuratIni = @"SELECT A.ID FROM PROP_SURAT A
                                            WHERE A.CONTRACT_NO =:a AND A.COUNT_SURAT =:c ";
                                        var hasilIni = connection.Query(dataSuratIni, new { a = param.CONTRACT_OFFER_NO, c = countSurat }).FirstOrDefault();

                                        var updateParaf = @"UPDATE PROP_DISPOSISI_PARAF A SET A.IS_ACTION =:a WHERE A.CONTRACT_NO =:b AND IS_CREATE_SURAT = 0 AND A.TYPE =:c AND A.SURAT_ID =:d ";
                                        int rz = connection.Execute(updateParaf, new
                                        {
                                            a = 0,
                                            b = param.CONTRACT_OFFER_NO,
                                            c = "disposisi",
                                            d = hasilIni.ID,
                                        }, transaction: transaction);
                                    }
                                    else
                                    {
                                        sql = @"update PROP_CONTRACT_OFFER a set LAST_REVISED_NIK =:b, REVISE_MEMO =:c, a.APV_DISPOSISI = 0 , ACTIVE = 1 where a.CONTRACT_OFFER_NO = :a";
                                        r = dalM.SetStatusByContractOffer("REVISED", param.CONTRACT_OFFER_NO);

                                    }

                                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                                    values(:a, :b, :c, :d, :e, :f, :g)";
                                    int w = connection.Execute(sql2, new
                                    {
                                        a = SURAT_ID,
                                        b = param.CONTRACT_OFFER_NO,
                                        c = idUser,
                                        d = param.APPROVAL_NOTE,
                                        e = "Revised",
                                        f = 0,
                                        g = namaEmpl,
                                    }, transaction: transaction);

                                    dynamic dataEmail = null;
                                    using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                                    {
                                        var cariEmail = @"SELECT USER_EMAIL, USER_NAME, USER_LOGIN FROM APP_USER WHERE ID =:b";
                                        dataEmail = conn.Query(cariEmail, new { b = IDSurat.USER_ID_CREATE }).DefaultIfEmpty(null).FirstOrDefault();
                                    }
                                    if(IDSurat != null)
                                    {
                                        var mailToMaker = dal.mailToMaker(makers[0], branch, res, stat, param, IDSurat, namaEmpl, dataEmail, param.APPROVAL_TYPE, userLogin);
                                    }

                                }
                                else
                                {
                                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                                    values(:a, :b, :c, :d, :e, :f, :g)";
                                    int w = connection.Execute(sql2, new
                                    {
                                        a = SURAT_ID,
                                        b = param.CONTRACT_OFFER_NO,
                                        c = idUser,
                                        d = param.APPROVAL_NOTE,
                                        e = "Rejected",
                                        f = 0,
                                        g = namaEmpl,
                                    }, transaction: transaction);
                                    sql = @"update PROP_CONTRACT_OFFER a set LAST_CANCELLED_NIK =:b, CANCEL_MEMO =:c, ACTIVE = 0 where a.CONTRACT_OFFER_NO = :a";
                                    r = dalM.SetStatusByContractOffer("REJECTED", param.CONTRACT_OFFER_NO, param.APPROVAL_NOTE);
                                    if(IDSurat != null)
                                    {
                                        var user = dal.spamEmail(res, IDSurat, branch, stat, param, namaEmpl, 7);
                                    }
                                }
                                r = connection.Execute(sql, new
                                {
                                    a = param.CONTRACT_OFFER_NO,
                                    b = param.USER_LOGIN,
                                    c = param.APPROVAL_NOTE
                                }, transaction: transaction);
                                //add creation by
                                res.CREATION_BY = makers[0] + " | " + makers[1];
                                if (param.APPROVAL_TYPE == "3")
                                {
                                    //sql = @"SELECT ROLE_NAME AS OFFER_STATUS FROM APP_REPO.APP_ROLE WHERE PROPERTY_ROLE = :a ";
                                    sql = @"SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                                }
                                else
                                {
                                    sql = @"SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                                }

                                string offer_status = connection.ExecuteScalar<string>(sql, new
                                {
                                    a = res.OFFER_STATUS
                                });
                                sql = @"insert into PROP_WORKFLOW_LOG (CONTRACT_OFFER_NO,STATUS_CONTRACT,USER_LOGIN,BRANCH_ID,CREATION_DATE,CREATION_BY,LAST_UPDATE_DATE,LAST_UPDATE_BY,NOTES,LOG_STATUS,STATUS_CONTRACT_DESC)
                        values(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k)";
                                var log = connection.Execute(sql, new
                                {
                                    a = param.CONTRACT_OFFER_NO,
                                    b = res.OFFER_STATUS,
                                    c = param.USER_LOGIN,//userlogin
                                    d = param.BRANCH_ID,
                                    e = DateTime.Now,
                                    f = param.USER_LOGIN,
                                    g = DateTime.Now,
                                    h = param.USER_LOGIN,
                                    i = param.APPROVAL_NOTE,
                                    j = "Done",
                                    k = offer_status,
                                }, transaction: transaction);
                                
                                if (param.APPROVAL_TYPE == "4") // membuat vacant Rental object
                                {
                                    sql = @"select OBJECT_ID from PROP_CONTRACT_OFFER_OBJECT where CONTRACT_OFFER_NO =:a";
                                    var obj = connection.Query(sql, new
                                    {
                                        a = res.CONTRACT_OFFER_NO
                                    }).ToList();
                                    foreach (var temp in obj)
                                    {
                                        //find object
                                        sql = @"select RO_NUMBER,VALID_FROM, VALID_TO from RENTAL_OBJECT WHERE RO_CODE =:a";
                                        var ro = connection.Query<RENTAL_OBJECT>(sql, new { a = temp.OBJECT_ID }).FirstOrDefault();

                                        sql = @"select count(1) from PROP_RO_OCCUPANCY where RO_CODE =:a and status ='BOOKED'";
                                        int b = connection.ExecuteScalar<int>(sql, new { a = temp.OBJECT_ID });
                                        if (b == 1)
                                        {
                                            //var a = "asdasdads";
                                            //hapus vacant
                                            sql = @"delete from PROP_RO_OCCUPANCY where RO_CODE =:a and status ='VACANT'";
                                            r = connection.Execute(sql, new { a = temp.OBJECT_ID }, transaction: transaction);
                                            //update booking menjadi vacant
                                            sql = @"update PROP_RO_OCCUPANCY set STATUS = 'VACANT', VALID_TO =:b, CONTRACT_OFFER_NO = null, CONTRACT_NO = null where RO_CODE =:a and status ='BOOKED'";
                                            r = connection.Execute(sql, new { a = temp.OBJECT_ID, b = ro.VALID_TO }, transaction: transaction);
                                        }

                                    }
                                    //transaction.Rollback();
                                }
                            }
                            //send mail to maker

                            transaction.Commit();

                            ret.TransactionCode = "";
                            ret.ResultCode = "S";
                            ret.ResultDescription = "Success";
                        }
                        else
                        {
                            transaction.Rollback();
                            ret.ResultDescription = role.result.Msg;
                            ret.ResultCode = "E";
                        }

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ret.TransactionCode = String.Empty;
                        ret.ResultCode = "E";
                        ret.ResultDescription = "Error: " + ex.Message;
                    }
                    finally
                    {
                        connection.Close();

                        connection.Dispose();
                    }
                }
            }
            return ret;
        }

        //-------------------------------------- DATA TABLE CONTRACT OFFER ------------------------------
        public DataTablesPendingList GetDataHeaderContractOffernew(int draw, int start, int length, string search, string KodeCabang, string UserRole, string roleProperty, string KodeTerminal = null)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeTerminal == "10")
            {
                KodeCabang = "9";
            }

            // BY PASS ADMINISTRATOR
            string whereProperty = (UserRole == "4") ? "" : " and APPROVAL_NEXT_LEVEL = '" + roleProperty + "' ";
            string whereSearch = search.Length >= 2 ? " AND ( CONTRACT_OFFER_NO||UPPER(CONTRACT_OFFER_TYPE)||UPPER(CUSTOMER_NAME)||TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR')||TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%'||'" + search.ToUpper() + "'||'%' ) " : "";
            string whereBranch = "";
            if (roleProperty == "50")
            {
                whereBranch = @" AND BRANCH_ID IN (SELECT  B.BRANCH_ID
                FROM BUSINESS_ENTITY A
                JOIN(SELECT DISTINCT BRANCH_ID, BE_WHERE  FROM V_BE) B ON B.BE_WHERE = A.REGIONAL_ID
                WHERE A.BRANCH_ID = :branch) ";
            } else
            {
                whereBranch = " AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :branch) ";
            }
            try
            {

                string sql = @"SELECT ID_INC
                        , CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID
                        , RENTAL_REQUEST_NO
                        , CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE
                        , CUSTOMER_ID
                        , CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME
                        , (SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS
                        , (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL
                        , to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE
                        , TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI 
                        FROM V_CONTRACT_OFFER_HEADER 
                        WHERE COMPLETED_STATUS = '2'
                         "+ whereBranch + whereProperty + whereSearch + " ORDER BY OFFER_STATUS DESC ";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { branch = KodeCabang, a = start, b = end });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { branch = KodeCabang });

            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();

            connection.Dispose();

            return result;
        }

        //--------------------------------------- DATA TABLE HISTORY ----------------------
        public DataTablesPendingList GetDataHistoryWorkflownew(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";


            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT * FROM (SELECT CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_DATE AS DATEH, NOTES 
                                    FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c
                                    UNION 
                                    SELECT A.CONTRACT_NO AS CONTRACT_OFFER_NO, A.TYPE AS STATUS_CONTRACT_DESC, B.USER_LOGIN || ' | ' || B.USER_NAME AS USER_LOGIN,TO_CHAR(A.TIME_CREATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, A.TIME_CREATE AS DATEH,
                                    A.MEMO AS NOTES 
                                    FROM PROP_HISTORY A JOIN APP_REPO.APP_USER B on A.USER_ID_CREATE = B.ID 
                                    WHERE A.CONTRACT_NO = :c and a.type != 'Approved') A ORDER BY A.DATEH DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                //search filter data
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        //--------------------------------------- DATA TABLE OBJECT -----------------------------
        public DataTablesPendingList GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------- DATA DETAIL CONDITION --------------------------
        public DataTablesPendingList GetDataDetailCondition(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, COA_PROD " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }


        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesPendingList GetDataDetailManualy(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION || ' - ' || REF_DESC AS CONDITION, to_char(DUE_DATE,'DD.MM.YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                            "FROM V_CONTRACT_OFFER_MANUAL " +
                            "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MEMO ----------------------
        public DataTablesPendingList GetDataDetailMemo(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, MEMO " +
                             "FROM V_CONTRACT_OFFER_MEMO " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------------------ DATA TABLE CONTRACT OFFER SALES BASED ----------------------
        public DataTablesPendingList GetDataDetailSalesBased(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT SALES_TYPE, NAME_OF_TERM, to_char(CALC_FROM,'DD/MM/YYYY') CALC_FROM, to_char(CALC_TO, 'DD/MM/YYYY') CALC_TO, UNIT, PERCENTAGE, TARIF, RR, MINSALES, CONTRACT_OFFER_NO, SR, MINPRODUCTION, PAYMENT_TYPE FROM PROP_CONTRACT_OFFER_SALES_RULE WHERE CONTRACT_OFFER_NO=:c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }
        public string SetKodeBayarDetail(string contract_offer, string cust, string kd_cabang, string user)
        {
            string result = "";
            var sql = "";
            //int r;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {

                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        sql = @"select ID,CONTRACT_OFFER_NO,MANUAL_NO,CONDITION,DUE_DATE,NET_VALUE,OBJECT_ID,QUANTITY,UNIT,KODE_BAYAR from PROP_CONTRACT_OFFER_FREQUENCY 
                                where CONTRACT_OFFER_NO =:a";
                        var manual = connection.Query<dynamic>(sql, new { a = contract_offer }).ToList();
                        foreach (var item in manual)
                        {
                            //H2HKodeBayar.H2HInboundKodeBayarSoapClient client = new H2HKodeBayar.H2HInboundKodeBayarSoapClient();
                            //var resKdBayar = client.inboundKodeBayar(DateTime.Now.ToString("dd-MMM-yyyy"), kd_cabang , "EPB REMOTE", "IDR", item.NET_VALUE.ToString(), cust, cust, contract_offer, contract_offer, contract_offer, contract_offer, "1B", "REMOTE");
                            //var kodeBayar = resKdBayar.R_LOG;

                            //sql = @"update PROP_CONTRACT_OFFER_FREQUENCY set KODE_BAYAR = :b where ID=:a";
                            //r = connection.Execute(sql, new { a = item.ID, b = kodeBayar }, transaction: transaction);
                            ////itgr_h2h_kdbayar
                            //sql = @"insert into ITGR_H2H_KDBAYAR (KD_CABANG,NO_PMH,KODE_BAYAR,REC_STAT,CREATED_DATE,CREATED_BY,TOTAL,KODE_REVERSE,TOTAL_REVERSE,TGL_LUNAS,TGL_BATAL,PPN,MATERAI,JUMLAH_IN_TAX,NAMA_BANK,BANK_CD)
                            //    values(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k,:l,:m,:n,:o,:p)";
                            //var log = connection.Execute(sql, new
                            //{
                            //    a = kd_cabang,
                            //    b = contract_offer,
                            //    c = kodeBayar,//userlogin
                            //    d = 0,
                            //    e = DateTime.Now,
                            //    f = user,
                            //    g = item.NET_VALUE.ToString(),
                            //    h = "",
                            //    i = "",
                            //    j = "",
                            //    k = "",
                            //    l = 0,
                            //    m = 0,
                            //    n = item.NET_VALUE.ToString(),
                            //    o = "",
                            //    p = "",
                            //}, transaction: transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return result;
        }
        public DataSurat GetDataDisposisi(dynamic id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT * FROM PROP_SURAT A WHERE A.STATUS_APV = 0 AND A.CONTRACT_NO = :a";
            var result = connection.Query<DataSurat>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DisposisiHistory> GetDataHistoryDisposisi(string id)
        {
            IEnumerable<DisposisiHistory> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT A.CONTRACT_NO, NVL(A.MEMO, '-') MEMO, A.SUBJECT, A.TYPE, A.TIME_CREATE, NAMA, NAMA_KEPADA,INSTRUKSI
                                FROM PROP_HISTORY A
                                WHERE A.CONTRACT_NO = :b ORDER BY A.TIME_CREATE, TYPE";

                    listData = connection.Query<DisposisiHistory>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public IEnumerable<NextParaf> GetDataNextParaf(string id)
        {
            IEnumerable<NextParaf> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT * FROM V_NEXT_PARAF A
                                WHERE A.CONTRACT_NO = :b";

                    listData = connection.Query<NextParaf>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public DISPOSISI_LIST GetDataSurat(dynamic id, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string RoleID = dataUser.UserRoleID;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, A.USER_ID_CREATE ,B.USER_ID, B.ROLE_ID
                            FROM PROP_SURAT A 
                            LEFT JOIN (SELECT A.SURAT_ID, A.USER_ID, A.ROLE_ID 
                            FROM PROP_DISPOSISI_PARAF A
                            WHERE A.CONTRACT_NO = :a
                            ) B ON B.SURAT_ID = A.ID
                            WHERE A.CONTRACT_NO = :a AND (A.USER_ID_CREATE =:b OR B.USER_ID = :b OR B.ROLE_ID =:c ) ";
            var result = connection.Query<DISPOSISI_LIST>(sql, new { a = id, b = UserID, c = RoleID }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }
    }
}