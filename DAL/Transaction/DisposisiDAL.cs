﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;
using Remote.Models.ListNotifikasi;
using Oracle.ManagedDataAccess.Client;
using Remote.Models.EmailConfiguration;
using Remote.Models.PendingList;
using RestSharp;
using Remote.Controllers;
using Newtonsoft.Json;

namespace Remote.DAL
{
    public class DisposisiDal
    {
        public object CurrentUser { get; private set; }

        public IDataTable GetDataDisposisi(int draw, int start, int length, string search, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string RoleID = dataUser.UserRoleID;
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT * FROM (
                                    SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    C.IS_CREATE_SURAT, IS_ACTION_DISPOSISI, A.STATUS_KIRIM, C.ROLE_ID, 1 EDIT,c.IS_ACTION, C.USER_ID,
                                    (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV
                                    FROM PROP_SURAT A 
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO                                    
                                    LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION 
                                    FROM PROP_DISPOSISI_PARAF A  WHERE A.TYPE = 'disposisi'
                                    GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION) 
                                    C ON C.CONTRACT_NO = A.CONTRACT_NO AND c.SURAT_ID = A.ID
                                    UNION ALL
                                    SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    1 IS_CREATE_SURAT, 0 IS_ACTION_DISPOSISI, A.STATUS_KIRIM,  NULL ROLE_ID, 0 EDIT,
                                    1 IS_ACTION, A.USER_ID_CREATE USER_ID,
                                    (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV
                                    FROM PROP_SURAT A 
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                                    ) Z WHERE Z.USER_ID = :c OR Z.ROLE_ID =:d  ORDER BY Z.ID DESC ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DISPOSISI_LIST>(fullSql, new { a = start, b = end, c = UserID, d = RoleID }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = UserID, d = RoleID });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM (
                                    SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    C.IS_CREATE_SURAT, IS_ACTION_DISPOSISI, A.STATUS_KIRIM, C.ROLE_ID, 1 EDIT,c.IS_ACTION, C.USER_ID,
                                    (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV
                                    FROM PROP_SURAT A 
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                                    LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION 
                                    FROM PROP_DISPOSISI_PARAF A  WHERE A.TYPE = 'disposisi'
                                    GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION) 
                                    C ON C.CONTRACT_NO = A.CONTRACT_NO AND c.SURAT_ID = A.ID
                                    UNION ALL
                                    SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    1 IS_CREATE_SURAT, 0 IS_ACTION_DISPOSISI, A.STATUS_KIRIM,  NULL ROLE_ID, 0 EDIT,
                                    1 IS_ACTION, A.USER_ID_CREATE USER_ID,
                                    (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV
                                    FROM PROP_SURAT A 
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                                    ) Z WHERE (Z.USER_ID =:d OR Z.ROLE_ID =:e) AND ((UPPER(Z.NO_SURAT) LIKE '%' ||:c|| '%') or (UPPER(Z.OFFER_TYPE) LIKE '%' ||:c|| '%') or 
                                    (UPPER(Z.CONTRACT_NO) LIKE '%' ||:c|| '%') or (UPPER(Z.CUSTOMER_NAME) LIKE '%' ||:c|| '%')) ORDER BY Z.ID DESC ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DISPOSISI_LIST>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        d = UserID,
                        e = RoleID,
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IDataTable GetDataParaf(int draw, int start, int length, string search, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @" SELECT * FROM (SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL,
                                    (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV, A.COUNT_SURAT
                                    FROM PROP_SURAT A 
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO                                    
                                    LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO, A.URUTAN, A.IS_ACTION, A.IS_ACTION_DISPOSISI FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.TYPE = 'surat' GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID,  A.URUTAN, A.IS_ACTION, A.IS_ACTION_DISPOSISI) C ON C.SURAT_ID = A.ID AND C.CONTRACT_NO = A.CONTRACT_NO
                                    WHERE A.COUNT_SURAT = 1 AND C.USER_ID = :c 
                                    UNION ALL
                                    SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                    TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME, 
                                    B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL, A.STATUS_APV, A.COUNT_SURAT
                                    FROM PROP_SURAT A
                                    JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                    JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO                
                                    LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO, A.URUTAN, A.IS_ACTION, IS_ACTION_DISPOSISI FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.TYPE = 'disposisi' AND A.ROLE_ID IS NULL AND A.IS_CREATE_SURAT = 0 GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID,  A.URUTAN, A.IS_ACTION, IS_ACTION_DISPOSISI) C ON C.CONTRACT_NO = A.CONTRACT_NO
                                    WHERE A.COUNT_SURAT > 1 AND A.DISPOSISI_MAX IS NOT NULL AND C.USER_ID = :c) Z 
                                    WHERE (Z.IS_ACTION = 1 AND STATUS_APV = 0) OR (Z.URUTAN = APV_LEVEL AND STATUS_APV = 0) OR (Z.IS_ACTION = 0 AND COUNT_SURAT = 1) ORDER BY Z.ID DESC
                                    ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DISPOSISI_LIST>(fullSql, new { a = start, b = end, c = UserID }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = UserID });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM (SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME ,
                                B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL,
                                (CASE WHEN A.STATUS_APV = 1 THEN 1 ELSE 0 END) AS STATUS_APV, A.COUNT_SURAT
                                FROM PROP_SURAT A 
                                JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO                                
                                LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO , A.URUTAN, A.IS_ACTION FROM PROP_DISPOSISI_PARAF A
                                WHERE A.TYPE = 'surat' GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.URUTAN, A.IS_ACTION) C ON C.SURAT_ID = A.ID AND C.CONTRACT_NO = A.CONTRACT_NO
                                WHERE A.COUNT_SURAT = 1 AND  C.USER_ID = :d AND ((UPPER(A.NO_SURAT) LIKE '%' ||:c|| '%') or (UPPER(A.SUBJECT) LIKE '%' ||:c|| '%') OR
                                (UPPER(B.CONTRACT_OFFER_NO) LIKE '%' ||:c|| '%') or (UPPER(B.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') or (UPPER(d.CUSTOMER_NAME) LIKE '%' ||:c|| '%')) 
                                UNION ALL
                                SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                                TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, d.CUSTOMER_NAME ,
                                B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL, A.STATUS_APV, A.COUNT_SURAT
                                FROM PROP_SURAT A 
                                JOIN PROP_CONTRACT_OFFER B ON B.CONTRACT_OFFER_NO = A.CONTRACT_NO
                                JOIN PROP_RENTAL_REQUEST d ON d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO                                
                                LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO , A.URUTAN, A.IS_ACTION FROM PROP_DISPOSISI_PARAF A
                                WHERE A.TYPE = 'disposisi' AND A.ROLE_ID IS NULL AND A.IS_CREATE_SURAT = 0 GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID , A.URUTAN, A.IS_ACTION) C ON C.CONTRACT_NO = A.CONTRACT_NO
                                WHERE A.COUNT_SURAT > 1 AND A.DISPOSISI_MAX IS NOT NULL AND C.USER_ID = :d AND ((UPPER(A.NO_SURAT) LIKE '%' ||:c|| '%') or (UPPER(A.SUBJECT) LIKE '%' ||:c|| '%') OR 
                                (UPPER(B.CONTRACT_OFFER_NO) LIKE '%' ||:c|| '%') or (UPPER(B.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') or (UPPER(d.CUSTOMER_NAME) LIKE '%' ||:c|| '%')) ) Z WHERE (Z.IS_ACTION = 1 AND STATUS_APV = 0) OR (Z.URUTAN = APV_LEVEL AND STATUS_APV = 0) OR (Z.IS_ACTION = 0 AND COUNT_SURAT = 1) ORDER BY Z.ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DISPOSISI_LIST>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        d = UserID
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public DISPOSISI_LIST GetDataDisposisi(dynamic id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE,
                                TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, B.CUSTOMER_NAME, B.CONTRACT_OFFER_NO 
                                FROM PROP_SURAT A 
                                LEFT JOIN V_CONTRACT_OFFER_HEADER B ON (B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO) 
                                WHERE ID = :a";
            var result = connection.Query<DISPOSISI_LIST>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DISPOSISI_LIST GetDataSurat(dynamic id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, A.ISI_SURAT, B.CONTRACT_OFFER_TYPE OFFER_TYPE, 
                                TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE,
                                TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, B.CUSTOMER_NAME, B.CONTRACT_OFFER_NO 
                                FROM PROP_SURAT A 
                                LEFT JOIN V_CONTRACT_OFFER_HEADER B ON (B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO) 
                                WHERE ID = :a";
            var result = connection.Query<DISPOSISI_LIST>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataSurat GetDataSurat2(dynamic id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT A.PREV_SURAT, A.MEMO, A.SUBJECT ,A.CONTRACT_NO, A.NO_SURAT, A.ISI_SURAT, A.TTD, A.NAMA_TTD, A.ATAS_NAMA, A.ID, A.DISPOSISI_MAX, 
            TO_CHAR(A.TANGGAL, 'dd/mm/yyyy') TANGGAL, TEMBUSAN, KEPADA, NAMA_TEMBUSAN, NAMA_KEPADA, TEMBUSAN_EKSTERNAL, KEPADA_EKSTERNAL 
            FROM PROP_SURAT A WHERE A.ID =:a";
            var result = connection.Query<DataSurat>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DDDisposisiEmployee> GetDataEmployee(string KodeCabang, string q)
        {
            IEnumerable<DDDisposisiEmployee> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection("app_repo"))
                {
                    string sql = @"SELECT ID , KD_CABANG BE_ID, CONCAT(CONCAT(USER_NAME,' - '),C.ROLE_NAME) EMPLOYEE_NAME 
                                FROM APP_USER A
                                JOIN APP_USER_ROLE B ON B.USER_ID = A.ID
                                JOIN APP_ROLE c on C.ROLE_ID = B.ROLE_ID and B.APP_ID = A.APP_ID 
                                WHERE LOWER(USER_NAME) like :b ";

                    listData = connection.Query<DDDisposisiEmployee>(sql, new { b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public IEnumerable<DDEmailEmployee> GetDataEmailEmployee(string KodeCabang, string q)
        {
            IEnumerable<DDEmailEmployee> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection("app_repo"))
                {
                    string sql = @"SELECT USER_EMAIL EMAIL , KD_CABANG BE_ID, CONCAT(CONCAT(USER_NAME,' - '),C.ROLE_NAME) EMPLOYEE_NAME 
                                FROM APP_USER A
                                JOIN APP_USER_ROLE B ON B.USER_ID = A.ID
                                JOIN APP_ROLE c on C.ROLE_ID = B.ROLE_ID and B.APP_ID = A.APP_ID 
                                WHERE LOWER(USER_NAME) like :b ";

                    listData = connection.Query<DDEmailEmployee>(sql, new { b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }
            
            return listData;
        }

        public IEnumerable<DDEmailEmployee> GetDataKlasifikasi(string q)
        {
            dynamic listData = null;

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT KODE, CONCAT(CONCAT(KODE,' - '),LABEL) EMPLOYEE_NAME FROM KODE_KLASIFIKASI WHERE LOWER(KODE) like :a ";

                    listData = connection.Query<DDEmailEmployee>(sql, new { a = $"%{q.ToLower()}%" }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listData;
        }
        //DISPOSISI
        public Results AddData(Disposisi data, dynamic dataUser, dynamic listDataDisposisiParaf)
        {
            string RoleID = dataUser.UserRoleID;
            string RoleName = dataUser.UserRoleName;
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;

            Results result = new Results();
            int r = 0;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    var sql = @"insert into PROP_DISPOSISI (CONTRACT_NO, NO_SURAT, SUBJECT, MEMO, DISPOSISI_KE, SURAT_ID, USER_ID_CREATE, IS_DELETE, INSTRUKSI, IS_CREATE_SURAT)
                            values(:b, :c, :g, :a, :f, :e, :d, :h, :i, :j)";
                    r = connection.Execute(sql, new
                    {
                        a = data.MEMO,
                        b = data.CONTRACT_NO,
                        c = data.NO_SURAT,
                        d = UserID,
                        e = data.SURAT_ID,
                        f = data.DISPOSISI_KE,
                        g = data.SUBJECT,
                        h = 0,
                        i = data.INSTRUKSI,
                        j = data.IS_CREATE_SURAT,
                    });
                    var cari = @" SELECT MAX(ID) ID FROM PROP_SURAT WHERE ID = :a and CONTRACT_NO = :b ";
                    var ID = connection.Query<int>(cari, new
                    {
                        a = data.SURAT_ID,
                        b = data.CONTRACT_NO,
                    });
                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, DISPOSISI_ID, CONTRACT_NO, USER_ID_CREATE, 
                            MEMO, SUBJECT, TYPE, IS_DELETE, INSTRUKSI, IS_CREATE_SURAT, NAMA_KEPADA, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data.SURAT_ID,
                        b = ID,
                        c = data.CONTRACT_NO,
                        d = UserID,
                        e = data.MEMO,
                        f = data.SUBJECT,
                        g = "Disposisi",
                        h = 0,
                        i = data.INSTRUKSI,
                        j = data.IS_CREATE_SURAT,
                        k = data.DISPOSISI_NAMA,
                        l = RoleName,
                    });

                    //update disposisi
                    var updateDisposisi = @"UPDATE PROP_DISPOSISI_PARAF A SET A.IS_ACTION_DISPOSISI = 1
                                        WHERE A.CONTRACT_NO =:a  AND A.TYPE = 'disposisi' AND  (A.USER_ID =:b or A.ROLE_ID =:c )";
                    int z = connection.Execute(updateDisposisi, new
                    {
                        a = data.CONTRACT_NO,
                        b = UserID,
                        c = RoleID,
                    });

                    var cariData = @"SELECT URUTAN, ROLE_ID FROM PROP_DISPOSISI_PARAF WHERE (ROLE_ID = :b OR USER_ID =:a) AND CONTRACT_NO =:c AND TYPE =:d  ";
                    dynamic hasil = connection.Query(cariData, new { a = UserID, b = RoleID, c = data.CONTRACT_NO, d = "disposisi" }).FirstOrDefault();
                    int nextDisposisi = 0;
                    //if (data.IS_CREATE_SURAT == 1)
                    //{
                    //    nextDisposisi = Convert.ToInt32(hasil.URUTAN);
                    //}
                    //else
                    //{
                    nextDisposisi = Convert.ToInt32(hasil.URUTAN) + 1;
                    //}
                    foreach (var item in listDataDisposisiParaf)
                    {
                        var sql3 = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, SURAT_ID, IS_CREATE_SURAT, DISPOSISI_ID, MAX_URUTAN, NAMA) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                        connection.Execute(sql3, new
                        {
                            a = item.disposisi_id,
                            b = nextDisposisi,
                            c = "disposisi",
                            d = data.CONTRACT_NO,
                            e = data.SURAT_ID,
                            f = data.IS_CREATE_SURAT,
                            g = ID,
                            h = nextDisposisi,
                            i = item.disposisi_nama
                        });

                        dynamic sendingemail = null;
                        DataSurat datasuratbaru = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                        datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = data.SURAT_ID
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        dynamic dataEmail = null;
                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                        {
                            var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                            dataEmail = conn.Query(cariEmail, new { b = item.disposisi_id }).DefaultIfEmpty(null).FirstOrDefault();
                        }
                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        param.USER_LOGIN = "";
                        param.APPROVAL_NOTE = "";
                        EmailConfigurationDAL dalMail = new EmailConfigurationDAL();
                        sendingemail = dalMail.emailDisposisi(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, RoleName, dataEmail, data.MEMO);

                        ListNotifikasiController listNotif = new ListNotifikasiController();
                        ParamNotification paramNotif = new ParamNotification();
                        paramNotif.username = dataEmail.USER_LOGIN;
                        paramNotif.type = "2"; //TYPE DISPOSISI
                        paramNotif.notification = data.MEMO;
                        paramNotif.no_contract = data.CONTRACT_NO;
                        paramNotif.pengirim = userLogin;
                        var sendNotif = listNotif.addNewNotif(paramNotif);
                    };

                    if (r > 0 && w > 0 && z > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results AddDataSurat(DataSurat data, dynamic dataUser, dynamic directory, dynamic file_name, dynamic listUrl)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;
            Results result = new Results();
            int r = 0;
            string decodeId = UrlEncoder.Decode(data.SURAT_ID);
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"SELECT COUNT(*) FROM PROP_SURAT A WHERE A.CONTRACT_NO =:a ";
                    dynamic v = connection.ExecuteScalar(sql2, new
                    {
                        a = data.CONTRACT_NO,
                    });

                    //cari data surat sebelumnya
                    var beforeSurat = @"SELECT MAX(ID) ID FROM PROP_SURAT A WHERE A.CONTRACT_NO =:a AND STATUS_APV = 1";
                    dynamic vv = connection.ExecuteScalar(beforeSurat, new
                    {
                        a = data.CONTRACT_NO,
                    });

                    var DeleteParaf = @"DELETE PROP_DISPOSISI_PARAF A WHERE A.SURAT_ID =:a AND A.ROLE_ID IS NULL AND CONTRACT_NO =:b AND IS_CREATE_SURAT = 0 AND USER_ID IS NOT NULL AND TYPE = 'disposisi' ";
                    connection.Execute(DeleteParaf, new { a = decodeId, b = data.CONTRACT_NO });

                    var LIST_PARAF = JsonConvert.DeserializeObject<dynamic>(data.LIST_PARAF);
                    int total_paraf = LIST_PARAF.Count;
                    int hirarki = 0;
                    foreach (var item in LIST_PARAF)
                    {
                        if (Convert.ToInt32(item.HIRARKI) == 1)
                        {
                            hirarki = total_paraf;
                        }
                        else if (Convert.ToInt32(item.HIRARKI) == 2)
                        {
                            if (total_paraf == 3)
                            {
                                hirarki = 2;
                            }
                            else if (total_paraf == 2)
                            {
                                hirarki = 1;
                            }
                            else if (total_paraf == 4)
                            {
                                hirarki = 3;
                            }
                            else if (total_paraf == 5)
                            {
                                hirarki = 4;
                            }

                        }
                        else if (Convert.ToInt32(item.HIRARKI) == 3)
                        {
                            if (total_paraf == 3)
                            {
                                hirarki = 1;
                            }
                            else if (total_paraf == 4)
                            {
                                hirarki = 2;
                            }
                            else if (total_paraf == 5)
                            {
                                hirarki = 3;
                            }
                        }
                        else if (Convert.ToInt32(item.HIRARKI) == 4)
                        {
                            if (total_paraf == 4)
                            {
                                hirarki = 1;
                            }
                            else if (total_paraf == 5)
                            {
                                hirarki = 2;
                            }
                        }
                        else if (Convert.ToInt32(item.HIRARKI) == 5)
                        {
                            if (total_paraf == 5)
                            {
                                hirarki = 1;
                            }
                        }
                        else
                        {
                            hirarki = 0;
                        }
                        var sqlInsertDisposisiParaf = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN, IS_ACTION, IS_ACTION_DISPOSISI) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)";
                        connection.Execute(sqlInsertDisposisiParaf, new
                        {
                            a = Convert.ToInt32(item.ID),
                            b = hirarki,
                            c = "disposisi",
                            d = data.CONTRACT_NO,
                            e = decodeId,
                            f = 0,
                            g = 1,
                            h = Convert.ToString(item.NAMA),
                            i = total_paraf,
                            j = 0,
                            k = 1,
                        });
                    }

                    var sql3 = @"SELECT MAX(A.MAX_URUTAN) MAX_URUTAN FROM PROP_DISPOSISI_PARAF A WHERE A.CONTRACT_NO =:a  AND A.SURAT_ID =:c AND IS_CREATE_SURAT = 0";
                    var w = connection.ExecuteScalar(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        b = UserID,
                        c = decodeId,
                    });
                    int dataMaxUrutan = Convert.ToInt32(w);

                    var updateDisposisi = @"UPDATE PROP_DISPOSISI_PARAF A SET A.IS_ACTION = 1 WHERE A.SURAT_ID =:a AND A.IS_CREATE_SURAT = 1";
                    int y = connection.Execute(updateDisposisi, new
                    {
                        a = decodeId,
                    });

                    //api create surat dinas milea
                    string url = "https://peo.pelindo.co.id/api/web/surat-dinas/nomor";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.Json
                    };
                    var body = new
                    {
                        NIP = userLogin,
                        KD_KLASIFIKASI = data.KODE_KLASIFIKASI,
                        PERIHAL = data.SUBJECT,
                        KEPADA = data.NAMA_KEPADA,
                        TANGGAL = DateTime.Now
                    };
                    request.AddJsonBody(body);
                    request.AddHeader("token", "07f4a02dff1c535f543d23574c872821cd66e1b0");
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);

                    if (list.status == 401)
                    {
                        result.Msg = list.message;
                        result.Status = "E";
                    }
                    else
                    {
                        string nomor_surat = list.data.NOMOR_SURAT;

                        if (data.STATUS_KIRIM == "Send")
                        {
                            var sql = @"declare surat clob; begin INSERT INTO PROP_SURAT (KODE_KLASIFIKASI,CONTRACT_NO, NO_SURAT, SUBJECT, ISI_SURAT, TTD, 
                            PARAF, LINK, ATAS_NAMA, STATUS_KIRIM, MEMO, USER_ID_CREATE, COUNT_SURAT, TANGGAL, 
                            DISPOSISI_MAX, APV_LEVEL, STATUS_APV, KEPADA, TEMBUSAN, NAMA_KEPADA, NAMA_TEMBUSAN, 
                            PREV_SURAT, DIRECTORY, FILE_NAME, NAMA_TTD, BRANCH_ID, KEPADA_EKSTERNAL, TEMBUSAN_EKSTERNAL)
                            values(:z,:a, :b, :c, empty_clob() ,:e, :f, :g, :h, :i, :j, :k, :l,to_date(:m,'DD/MM/YYYY'), :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :x, :y, :kepadaEks, :tembusanEks ) returning isi_surat into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                            r = connection.Execute(sql, new
                            {
                                a = data.CONTRACT_NO,
                                b = nomor_surat,
                                c = data.SUBJECT,
                                d = "",
                                e = data.TTD,
                                f = data.PARAF,
                                g = data.LINK,
                                h = data.ATAS_NAMA,
                                i = data.STATUS_KIRIM,
                                j = data.MEMO,
                                k = UserID,
                                l = v + 1,
                                m = data.TANGGAL,
                                n = 1,
                                o = dataMaxUrutan,
                                p = 0,
                                q = data.KEPADA,
                                r = data.TEMBUSAN,
                                s = data.NAMA_KEPADA,
                                t = data.NAMA_TEMBUSAN,
                                u = vv,
                                v = directory,
                                w = file_name,
                                x = data.NAMA_TTD,
                                y = KodeCabang,
                                z = data.KODE_KLASIFIKASI,
                                kepadaEks = data.KEPADA_EKSTERNAL,
                                tembusanEks = data.TEMBUSAN_EKSTERNAL,
                            });
                            var cari = @" SELECT MAX(ID) ID FROM PROP_SURAT WHERE USER_ID_CREATE = :a and CONTRACT_NO = :b and NO_SURAT = :c ";
                            var ID = connection.Query<int>(cari, new
                            {
                                a = UserID,
                                b = data.CONTRACT_NO,
                                c = nomor_surat,
                            });

                            foreach (var item in listUrl)
                            {
                                var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                                int kt = connection.Execute(insertAttachment, new
                                {
                                    a = ID,
                                    b = Convert.ToString(item.file_name),
                                    c = Convert.ToString(item.directory),
                                });
                            }

                            AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                            //query mencari approval
                            var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                            co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                            {
                                a = data.CONTRACT_NO
                            }).DefaultIfEmpty(null).FirstOrDefault();

                            DataSurat datasuratbaru = null;
                            var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                            datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                            {
                                a = ID
                            }).DefaultIfEmpty(null).FirstOrDefault();

                            ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                            param.USER_LOGIN = "";
                            param.APPROVAL_NOTE = "";

                            var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                            JOIN(SELECT MAX(A.URUTAN) URUTAN FROM PROP_DISPOSISI_PARAF A
                            WHERE A.CONTRACT_NO =:a  AND A.IS_CREATE_SURAT = 0 AND a.SURAT_ID =:b) B ON B.URUTAN = A.URUTAN
                            WHERE A.CONTRACT_NO =:a AND A.IS_CREATE_SURAT = 0 AND a.SURAT_ID =:b";
                            dynamic nextParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = decodeId }).FirstOrDefault();

                            dynamic dataEmail = null;
                            using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                            {
                                var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                                dataEmail = conn.Query(cariEmail, new { b = nextParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                            }

                            EmailConfigurationDAL dal = new EmailConfigurationDAL();
                            var sendingemail = dal.formateSendEmail(co_new, datasuratbaru, KodeCabang, "setujui", param, dataEmail, RoleName);

                            ListNotifikasiController listNotif = new ListNotifikasiController();
                            ParamNotification paramNotif = new ParamNotification();
                            paramNotif.username = dataEmail.USER_LOGIN;
                            paramNotif.type = "3"; //TYPE PARAF
                            paramNotif.notification = data.MEMO;
                            paramNotif.no_contract = data.CONTRACT_NO;
                            paramNotif.pengirim = userLogin;
                            var sendNotif = listNotif.addNewNotif(paramNotif);

                            var sql5 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                            w = connection.Execute(sql5, new
                            {
                                a = ID,
                                b = data.CONTRACT_NO,
                                c = UserID,
                                d = data.MEMO,
                                e = data.SUBJECT,
                                f = "Surat",
                                g = 0,
                                h = 0,
                                i = RoleName,
                            });

                            if (dataMaxUrutan == 0)
                            {
                                var sql6 = @"UPDATE PROP_CONTRACT_OFFER a set a.APV_DISPOSISI =:b , a.COMPLETED_STATUS =:c  WHERE CONTRACT_OFFER_NO =:a ";
                                int zz = connection.Execute(sql6, new
                                {
                                    a = data.CONTRACT_NO,
                                    b = 1,
                                    c = 2,
                                });
                            }
                        }
                        else
                        {
                            var sql = @"declare surat clob; begin INSERT INTO PROP_SURAT (KODE_KLASIFIKASI,CONTRACT_NO, NO_SURAT, SUBJECT, ISI_SURAT, TTD, 
                            PARAF, LINK, ATAS_NAMA, STATUS_KIRIM, MEMO, USER_ID_CREATE, COUNT_SURAT, TANGGAL, 
                            STATUS_APV, KEPADA, TEMBUSAN, NAMA_KEPADA, NAMA_TEMBUSAN, PREV_SURAT, DIRECTORY, FILE_NAME, NAMA_TTD, BRANCH_ID, KEPADA_EKSTERNAL, TEMBUSAN_EKSTERNAL ) 
                            values(:z,:a, :b, :c, empty_clob() ,:e, :f, :g, :h, :i, :j, :k, :l,to_date(:m,'DD/MM/YYYY'), :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :kepadaEks, :tembusanEks ) returning isi_surat into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                            r = connection.Execute(sql, new
                            {
                                a = data.CONTRACT_NO,
                                b = nomor_surat,
                                c = data.SUBJECT,
                                d = "",
                                e = data.TTD,
                                f = data.PARAF,
                                g = data.LINK,
                                h = data.ATAS_NAMA,
                                i = data.STATUS_KIRIM,
                                j = data.MEMO,
                                k = UserID,
                                l = v + 1,
                                m = data.TANGGAL,
                                n = 0,
                                o = data.KEPADA,
                                p = data.TEMBUSAN,
                                q = data.NAMA_KEPADA,
                                r = data.NAMA_TEMBUSAN,
                                s = vv,
                                t = directory,
                                u = file_name,
                                v = data.NAMA_TTD,
                                w = KodeCabang,
                                z = data.KODE_KLASIFIKASI,
                                kepadaEks = data.KEPADA_EKSTERNAL,
                                tembusanEks = data.TEMBUSAN_EKSTERNAL,
                            });
                            var cari = @" SELECT MAX(ID) ID FROM PROP_SURAT WHERE USER_ID_CREATE = :a and CONTRACT_NO = :b and NO_SURAT = :c ";
                            var ID = connection.Query<int>(cari, new
                            {
                                a = UserID,
                                b = data.CONTRACT_NO,
                                c = nomor_surat,
                            });

                            var sql5 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                            w = connection.Execute(sql5, new
                            {
                                a = ID,
                                b = data.CONTRACT_NO,
                                c = UserID,
                                d = data.MEMO,
                                e = data.SUBJECT,
                                f = "Draft",
                                g = 0,
                                h = 0,
                                i = RoleName,
                            });

                            foreach (var item in listUrl)
                            {
                                var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                                int kt = connection.Execute(insertAttachment, new
                                {
                                    a = ID,
                                    b = Convert.ToString(item.file_name),
                                    c = Convert.ToString(item.directory),
                                });
                            }
                        }

                        if (r != 0)
                        {
                            transaction.Commit();
                            result.Msg = "Data Added Successfully";
                            result.Status = "S";
                        }
                        else
                        {
                            result.Msg = "Failed to Add Data";
                            result.Status = "E";

                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results EditDataSurat(DataSurat data, dynamic dataUser, dynamic directory, dynamic file_name, dynamic listUrl)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;

            string decodeId = UrlEncoder.Decode(data.ID);
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
                try
                {
                    //hapus data disposisi
                    var DeleteParaf = @"DELETE PROP_DISPOSISI_PARAF A WHERE A.SURAT_ID =:a AND A.ROLE_ID IS NULL AND CONTRACT_NO =:b AND IS_CREATE_SURAT = 0 AND USER_ID IS NOT NULL AND TYPE = 'disposisi' ";
                    var yyy = connection.Execute(DeleteParaf, new { a = data.SURAT_ID, b = data.CONTRACT_NO });

                    var LIST_PARAF = JsonConvert.DeserializeObject<dynamic>(data.LIST_PARAF);
                    int total_paraf = LIST_PARAF.Count;
                    int hirarki = 0;
                    foreach (var item in LIST_PARAF)
                    {
                        if(Convert.ToInt32(item.HIRARKI) == 1)
                        {
                            hirarki = total_paraf;
                        } else if (Convert.ToInt32(item.HIRARKI) == 2)
                        {
                            if(total_paraf == 3)
                            {
                                hirarki = 2 ;
                            } else if(total_paraf == 2)
                            {
                                hirarki = 1;
                            } else if(total_paraf == 4)
                            {
                                hirarki = 3;
                            } else if (total_paraf == 5)
                            {
                                hirarki = 4;
                            }

                        } else if (Convert.ToInt32(item.HIRARKI) == 3)
                        {
                            if (total_paraf == 3)
                            {
                                hirarki = 1;
                            }
                            else if (total_paraf == 4)
                            {
                                hirarki = 2;
                            }
                            else if (total_paraf == 5)
                            {
                                hirarki = 3;
                            }
                        } else if (Convert.ToInt32(item.HIRARKI) == 4)
                        {
                            if (total_paraf == 4)
                            {
                                hirarki = 1;
                            }
                            else if (total_paraf == 5)
                            {
                                hirarki = 2;
                            }
                        } else if (Convert.ToInt32(item.HIRARKI) == 5)
                        {
                            if (total_paraf == 5)
                            {
                                hirarki = 1;
                            }
                        } else
                        {
                            hirarki = 0;
                        }

                        var sqlInsertDisposisiParaf = @"INSERT INTO PROP_DISPOSISI_PARAF (USER_ID, URUTAN, TYPE, CONTRACT_NO, 
                                SURAT_ID, IS_CREATE_SURAT, COUNT_SURAT, NAMA, MAX_URUTAN, IS_ACTION, IS_ACTION_DISPOSISI) 
                                VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)";
                        var xxx = connection.Execute(sqlInsertDisposisiParaf, new
                        {
                            a = Convert.ToInt32(item.ID),
                            b = hirarki,
                            c = "disposisi",
                            d = data.CONTRACT_NO,
                            e = data.SURAT_ID,
                            f = 0,
                            g = 1,
                            h = Convert.ToString(item.NAMA),
                            i = total_paraf,
                            j = 0,
                            k = 1,
                        });
                    }

                    var sql3 = @"SELECT MAX(A.MAX_URUTAN) MAX_URUTAN FROM PROP_DISPOSISI_PARAF A WHERE A.CONTRACT_NO =:a  AND A.SURAT_ID =:b AND IS_CREATE_SURAT = 0";
                    var w = connection.ExecuteScalar(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        b = data.SURAT_ID,
                    });
                    int dataMaxUrutan = Convert.ToInt32(w);

                    if (data.STATUS_KIRIM == "Send")
                    {
                        var sql = @"declare surat clob; begin UPDATE PROP_SURAT A SET A.CONTRACT_NO =:a, A.NO_SURAT =:b, A.SUBJECT =:c, A.ISI_SURAT = empty_clob(), TTD =:e, 
                            PARAF =:f, LINK =:g, ATAS_NAMA =:h, STATUS_KIRIM =:i, MEMO =:j, TANGGAL = to_date(:k,'DD/MM/YYYY'), DISPOSISI_MAX =:l, APV_LEVEL =:n,
                            STATUS_APV =:o, KEPADA =:p, TEMBUSAN =:q, NAMA_KEPADA =:r, NAMA_TEMBUSAN =:s, DIRECTORY =:t, FILE_NAME=:u, NAMA_TTD =:v, TEMBUSAN_EKSTERNAL =:w, KEPADA_EKSTERNAL =:x 
                            WHERE A.ID =:m RETURNING ISI_SURAT into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = data.NO_SURAT,
                            c = data.SUBJECT,
                            d = "",
                            e = data.TTD,
                            f = data.PARAF,
                            g = data.LINK,
                            h = data.ATAS_NAMA,
                            i = data.STATUS_KIRIM,
                            j = data.MEMO,
                            k = data.TANGGAL,
                            l = 1,
                            m = decodeId,
                            n = dataMaxUrutan,
                            o = 0,
                            p = data.KEPADA,
                            q = data.TEMBUSAN,
                            r = data.NAMA_KEPADA,
                            s = data.NAMA_TEMBUSAN,
                            t = directory,
                            u = file_name,
                            v = data.NAMA_TTD,
                            w = data.TEMBUSAN_EKSTERNAL,
                            x = data.KEPADA_EKSTERNAL,
                        });

                        foreach (var item in listUrl)
                        {
                            var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                            int kt = connection.Execute(insertAttachment, new
                            {
                                a = decodeId,
                                b = Convert.ToString(item.file_name),
                                c = Convert.ToString(item.directory),
                            });
                        }

                        AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                        //query mencari approval
                        var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                        co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                        {
                            a = data.CONTRACT_NO
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        DataSurat datasuratbaru = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                        datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = decodeId
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        param.USER_LOGIN = "";
                        param.APPROVAL_NOTE = "";

                        var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                        JOIN (SELECT MAX(A.URUTAN) URUTAN FROM PROP_DISPOSISI_PARAF A
                        WHERE A.CONTRACT_NO =:a  AND A.IS_CREATE_SURAT = 0 AND a.SURAT_ID =:b) B ON B.URUTAN = A.URUTAN
                        WHERE A.CONTRACT_NO =:a  AND A.IS_CREATE_SURAT = 0 AND a.SURAT_ID =:b";
                        dynamic nextParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = data.SURAT_ID }).FirstOrDefault();

                        dynamic dataEmail = null;
                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                        {
                            var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                            dataEmail = conn.Query(cariEmail, new { b = nextParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                        }

                        EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();

                        var sendingemail = dalEmail.formateSendEmail(co_new, datasuratbaru, KodeCabang, "setujui", param, dataEmail, RoleName);

                        ListNotifikasiController listNotif = new ListNotifikasiController();
                        ParamNotification paramNotif = new ParamNotification();
                        paramNotif.username = dataEmail.USER_LOGIN;
                        paramNotif.type = "3"; //TYPE PARAF
                        paramNotif.notification = data.MEMO;
                        paramNotif.no_contract = data.CONTRACT_NO;
                        paramNotif.pengirim = userLogin;
                        var sendNotif = listNotif.addNewNotif(paramNotif);

                        var sql5 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                        w = connection.Execute(sql5, new
                        {
                            a = decodeId,
                            b = data.CONTRACT_NO,
                            c = UserID,
                            d = data.MEMO,
                            e = data.SUBJECT,
                            f = "Surat",
                            g = 0,
                            h = 0,
                            i = RoleName,
                        });

                        var updateDisposisi = @"UPDATE PROP_DISPOSISI_PARAF a set a.IS_ACTION =:c  WHERE USER_ID = :b AND CONTRACT_NO =:a";
                        int y = connection.Execute(updateDisposisi, new
                        {
                            a = data.CONTRACT_NO,
                            b = UserID,
                            c = 1,
                        });

                        if (dataMaxUrutan == 0)
                        {
                            var sql6 = @"UPDATE PROP_CONTRACT_OFFER a set a.APV_DISPOSISI =:b , a.COMPLETED_STATUS =:c  WHERE CONTRACT_OFFER_NO =:a ";
                            int zz = connection.Execute(sql6, new
                            {
                                a = data.CONTRACT_NO,
                                b = 1,
                                c = 2,
                            });
                        }
                    }
                    else
                    {
                        var sql = @"declare surat clob; begin UPDATE PROP_SURAT A SET A.CONTRACT_NO =:a, A.NO_SURAT =:b, A.SUBJECT =:c, A.ISI_SURAT = empty_clob(), TTD =:e, 
                            PARAF =:f, LINK =:g, ATAS_NAMA =:h, STATUS_KIRIM =:i, MEMO =:j, TANGGAL =to_date(:k,'DD/MM/YYYY'),
                            STATUS_APV =:o, KEPADA =:p, TEMBUSAN =:q, NAMA_KEPADA =:r, NAMA_TEMBUSAN =:s, DIRECTORY=:t, FILE_NAME =:u, NAMA_TTD =:v, TEMBUSAN_EKSTERNAL =:w, KEPADA_EKSTERNAL =:x 
                            WHERE A.ID =:m RETURNING ISI_SURAT into surat; dbms_lob.append(surat, '" + data.ISI_SURAT + "'); end;";
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = data.NO_SURAT,
                            c = data.SUBJECT,
                            d = "",
                            e = data.TTD,
                            f = data.PARAF,
                            g = data.LINK,
                            h = data.ATAS_NAMA,
                            i = data.STATUS_KIRIM,
                            j = data.MEMO,
                            k = data.TANGGAL,
                            m = decodeId,
                            o = 0,
                            p = data.KEPADA,
                            q = data.TEMBUSAN,
                            r = data.NAMA_KEPADA,
                            s = data.NAMA_TEMBUSAN,
                            t = directory,
                            u = file_name,
                            v = data.NAMA_TTD,
                            w = data.TEMBUSAN_EKSTERNAL,
                            x = data.KEPADA_EKSTERNAL,
                        });

                        foreach (var item in listUrl)
                        {
                            var insertAttachment = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                            int kt = connection.Execute(insertAttachment, new
                            {
                                a = decodeId,
                                b = Convert.ToString(item.file_name),
                                c = Convert.ToString(item.directory),
                            });
                        }

                        var sql5 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, 
                            USER_ID_CREATE, MEMO, SUBJECT, TYPE, IS_DELETE, IS_CREATE_SURAT, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
                        w = connection.Execute(sql5, new
                        {
                            a = decodeId,
                            b = data.CONTRACT_NO,
                            c = UserID,
                            d = data.MEMO,
                            e = data.SUBJECT,
                            f = "Draft",
                            g = 0,
                            h = 0,
                            i = RoleName,
                        });
                    }

                    if (r != 0)
                    {
                        transaction.Commit();
                        result.Msg = "Update Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results ApproveParaf(DataSurat data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;
            Results result = new Results();

            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var cari = @" SELECT URUTAN, MAX_URUTAN FROM PROP_DISPOSISI_PARAF WHERE USER_ID = :a AND CONTRACT_NO =:b AND IS_ACTION = 0 AND URUTAN =:c";
                    var urutan = connection.Query(cari, new { a = UserID, b = data.CONTRACT_NO, c = data.URUTAN }).FirstOrDefault();
                    if (urutan.MAX_URUTAN != data.URUTAN)
                    {
                        int next_paraf = data.URUTAN + 1;
                        var sql = @"UPDATE PROP_CONTRACT_OFFER a set a.PARAF_LEVEL =:b  where a.CONTRACT_OFFER_NO =:a";
                        r = connection.Execute(sql, new
                        {
                            a = data.CONTRACT_NO,
                            b = next_paraf,
                        });
                    }

                    if (urutan.MAX_URUTAN == data.URUTAN)
                    {
                        TransContractOfferDAL dalWf = new TransContractOfferDAL();
                        var hasil = dalWf.ReleaseToWorkflowNew(
                            Convert.ToString(name),
                            Convert.ToString(userLogin),
                            Convert.ToString(data.CONTRACT_NO),
                            Convert.ToString(KodeCabang),
                            Convert.ToString(data.MEMO)
                            );
                    }
                    else
                    {
                        var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a and a.count_surat = 1 AND a.urutan =:b ";
                        var nextParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = data.URUTAN + 1 }).FirstOrDefault();

                        dynamic dataEmail = null;
                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                        {
                            var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                            dataEmail = conn.Query(cariEmail, new { b = nextParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                        }

                        AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                        //query mencari approval
                        var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                        co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                        {
                            a = data.CONTRACT_NO
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        DataSurat datasuratbaru = null;
                        var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                        datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                        {
                            a = data.SURAT_ID
                        }).DefaultIfEmpty(null).FirstOrDefault();

                        ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                        param.USER_LOGIN = "";
                        param.APPROVAL_NOTE = "";

                        EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                        var sendingemail = dalEmail.emailParaf(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, RoleName, dataEmail, data.MEMO);

                        //SEND NOTIF MOBILE
                        ListNotifikasiController listNotif = new ListNotifikasiController();
                        ParamNotification paramNotif = new ParamNotification();
                        paramNotif.username = dataEmail.USER_LOGIN;
                        paramNotif.type = "3"; //TYPE PARAF
                        paramNotif.notification = data.MEMO;
                        paramNotif.no_contract = data.CONTRACT_NO;
                        paramNotif.pengirim = userLogin;
                        var sendNotif = listNotif.addNewNotif(paramNotif);

                    }

                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data.SURAT_ID,
                        b = data.CONTRACT_NO,
                        c = UserID,
                        d = data.MEMO,
                        e = "Paraf",
                        f = 0,
                        g = RoleName,
                    });

                    var sql3 = @"UPDATE PROP_DISPOSISI_PARAF a set a.IS_ACTION =:c  WHERE USER_ID = :b AND CONTRACT_NO =:a AND a.IS_ACTION = 0 AND URUTAN =:d";
                    int y = connection.Execute(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        b = UserID,
                        c = 1,
                        d = data.URUTAN,
                    });

                    if (w > 0 && y > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Paraf Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Paraf Data";
                        result.Status = "E";
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results ApproveParafDisposisi(DataSurat data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;
            Results result = new Results();

            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data.SURAT_ID,
                        b = data.CONTRACT_NO,
                        c = UserID,
                        d = data.MEMO,
                        e = "Paraf",
                        f = 0,
                        g = RoleName,
                    });

                    var sql3 = @"UPDATE PROP_DISPOSISI_PARAF a set a.IS_ACTION =:c  WHERE USER_ID = :b AND CONTRACT_NO =:a";
                    int y = connection.Execute(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        b = UserID,
                        c = 1,
                    });

                    var cari = @" SELECT APV_LEVEL FROM PROP_SURAT WHERE ID =:a ";
                    dynamic urutan = connection.Query(cari, new { a = data.SURAT_ID }).FirstOrDefault();
                    int z = 0;
                    dynamic nextParaf = null;
                    dynamic isType = null;
                    if (urutan.APV_LEVEL == 1)
                    {
                        var sql5 = @"UPDATE PROP_CONTRACT_OFFER a set a.APV_DISPOSISI =:b , a.COMPLETED_STATUS =:c  WHERE CONTRACT_OFFER_NO =:a ";
                        z = connection.Execute(sql5, new
                        {
                            a = data.CONTRACT_NO,
                            b = 1,
                            c = 2,
                        });

                        var cariDataRole = @"SELECT A.ROLE_ID FROM PROP_DISPOSISI_PARAF A
                                    JOIN (SELECT MAX(A.TIME_CREATE) TIME_CREATE FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a AND A.ROLE_ID IS NOT NULL) B ON B.TIME_CREATE = A.TIME_CREATE
                                    WHERE A.CONTRACT_NO =:a AND A.ROLE_ID IS NOT NULL";
                        dynamic approveRole = connection.Query(cariDataRole, new { a = data.CONTRACT_NO }).FirstOrDefault();

                        using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                        {
                            var cariDataAtasan = @"SELECT A.ID USER_ID FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID WHERE B.ROLE_ID =:a";
                            nextParaf = conn.Query(cariDataAtasan, new { a = approveRole.ROLE_ID }).FirstOrDefault();

                            conn.Close();
                        }
                        isType = 2;
                    }
                    else
                    {
                        int hasil = Convert.ToInt32(urutan.APV_LEVEL) - 1;
                        var sql4 = @"UPDATE PROP_SURAT a set a.APV_LEVEL =:b  WHERE ID =:a ";
                        z = connection.Execute(sql4, new
                        {
                            a = data.SURAT_ID,
                            b = hasil,
                        });

                        isType = 1;

                        var cariDataAtasan = @"SELECT A.USER_ID, MAX(A.TIME_CREATE) FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a  AND a.urutan =:b GROUP BY A.USER_ID";
                        nextParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = hasil }).FirstOrDefault();
                    }

                    dynamic dataEmail = null;
                    using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                    {
                        var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                        dataEmail = conn.Query(cariEmail, new { b = nextParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                    }

                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    DataSurat datasuratbaru = null;
                    var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                    datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                    {
                        a = data.SURAT_ID
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                    param.USER_LOGIN = "";
                    param.APPROVAL_NOTE = "";

                    EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                    var sendingemail = dalEmail.emailParaf(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, RoleName, dataEmail, data.MEMO, isType);

                    ListNotifikasiController listNotif = new ListNotifikasiController();
                    ParamNotification paramNotif = new ParamNotification();
                    paramNotif.username = dataEmail.USER_LOGIN;
                    paramNotif.type = "3"; //TYPE PARAF
                    paramNotif.notification = data.MEMO;
                    paramNotif.no_contract = data.CONTRACT_NO;
                    paramNotif.pengirim = userLogin;
                    var sendNotif = listNotif.addNewNotif(paramNotif);

                    if (w > 0 && y > 0 && z > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Paraf Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Paraf Data";
                        result.Status = "E";
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results RejectedParaf(DataSurat data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;
            Results result = new Results();

            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    //int prev_paraf = data.URUTAN - 1;
                    var sql = @"UPDATE PROP_CONTRACT_OFFER a set a.PARAF_LEVEL =:b  where a.CONTRACT_OFFER_NO =:a";
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO,
                        b = 0,
                    });

                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data.SURAT_ID,
                        b = data.CONTRACT_NO,
                        c = UserID,
                        d = data.MEMO,
                        e = "Rejected Paraf",
                        f = 0,
                        g = RoleName,
                    });

                    var sql3 = @"UPDATE PROP_DISPOSISI_PARAF a set a.IS_ACTION =:c  
                                WHERE CONTRACT_NO =:a  AND a.TYPE = 'surat' ";
                    int y = connection.Execute(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        c = 0,
                    });

                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    dynamic turnParaf = null;

                    var cariDataAtasan = @"SELECT A.USER_ID_CREATE USER_ID FROM PROP_SURAT A
                                    WHERE A.ID =:a ";
                    turnParaf = connection.Query(cariDataAtasan, new { a = data.SURAT_ID }).FirstOrDefault();
                    
                    dynamic dataEmail = null;
                    using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                    {
                        var cariEmail = @"SELECT USER_EMAIL, USER_LOGIN ,CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                        dataEmail = conn.Query(cariEmail, new { b = turnParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                    }

                    DataSurat datasuratbaru = null;
                    var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                    datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                    {
                        a = data.SURAT_ID
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                    param.USER_LOGIN = "";
                    param.APPROVAL_NOTE = "";

                    EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                    var sendingemail = dalEmail.emailRejected(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, RoleName, dataEmail, data.MEMO);

                    //SEND NOTIF MOBILE
                    ListNotifikasiController listNotif = new ListNotifikasiController();
                    ParamNotification paramNotif = new ParamNotification();
                    paramNotif.username = dataEmail.USER_LOGIN;
                    paramNotif.type = "5"; //TYPE REVISE
                    paramNotif.notification = data.MEMO;
                    paramNotif.no_contract = data.CONTRACT_NO;
                    paramNotif.pengirim = userLogin;
                    var sendNotif = listNotif.addNewNotif(paramNotif);

                    if (r > 0 && w > 0 && y > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Rejected Successfully";

                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Rejected Data";
                        result.Status = "E";
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results RejectedParafDisposisi(DataSurat data, dynamic dataUser)
        {
            string UserID = dataUser.UserID;
            string KodeCabang = dataUser.KodeCabang;
            string userLogin = dataUser.UserLoginID;
            string name = dataUser.Name;
            string RoleName = dataUser.UserRoleName;
            Results result = new Results();

            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql = @"UPDATE PROP_CONTRACT_OFFER a set a.APV_DISPOSISI =:b  where a.CONTRACT_OFFER_NO =:a";
                    r = connection.Execute(sql, new
                    {
                        a = data.CONTRACT_NO,
                        b = 0,
                    });

                    var updateSurat = @"UPDATE PROP_SURAT a set a.APV_LEVEL =:b, a.DISPOSISI_MAX =:c, A.STATUS_KIRIM =:d
                                        where a.ID =:a";
                    r = connection.Execute(updateSurat, new
                    {
                        a = data.SURAT_ID,
                        b = 0,
                        c = 0,
                        d = "Draft",
                    });

                    var sql2 = @"insert into PROP_HISTORY (SURAT_ID, CONTRACT_NO, USER_ID_CREATE, MEMO, TYPE, IS_DELETE, NAMA)
                            values(:a, :b, :c, :d, :e, :f, :g)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data.SURAT_ID,
                        b = data.CONTRACT_NO,
                        c = UserID,
                        d = data.MEMO,
                        e = "Rejected Paraf",
                        f = 0,
                        g = RoleName,
                    });

                    var BeforeDataSurat = @"SELECT A.COUNT_SURAT FROM PROP_SURAT A
                                    WHERE A.CONTRACT_NO =:a AND A.ID =:c ";
                    dynamic hasilBefore = connection.Query(BeforeDataSurat, new { a = data.CONTRACT_NO, c = data.SURAT_ID }).FirstOrDefault();

                    var dataSuratIni = @"SELECT A.ID FROM PROP_SURAT A
                                    WHERE A.CONTRACT_NO =:a AND A.COUNT_SURAT =:c ";
                    var hasilIni = connection.Query(dataSuratIni, new { a = data.CONTRACT_NO, c = hasilBefore.COUNT_SURAT - 1 }).FirstOrDefault();

                    var sql3 = @"UPDATE PROP_DISPOSISI_PARAF a set a.IS_ACTION =:c  
                                WHERE CONTRACT_NO =:a AND a.IS_CREATE_SURAT = 0  AND a.TYPE = 'disposisi' AND a.SURAT_ID =:b ";
                    int y = connection.Execute(sql3, new
                    {
                        a = data.CONTRACT_NO,
                        b = hasilIni.ID,
                        c = 0,
                    });

                    AUP_TRANS_CONTRACT_OFFER_WEBAPPS co_new = null;
                    //query mencari approval
                    var cariDataContract = @"select ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, BE_NAME AS BE_NAMES, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, 
                                to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, 
                                CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_NAME AS CUSTOMER_NAMES, 
                                TO_CHAR(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, 
                                CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, COMPLETED_STATUS, 
                                OFFER_STATUS, BE_CITY, LUAS_TANAH, LUAS_BANGUNAN, RO_ADDRESS, CREATION_BY, KODE_BAYAR, BRANCH_ID, TOP_APPROVED_LEVEL, UPDATED_BY
                                from V_CONTRACT_OFFER_HEADER where CONTRACT_OFFER_NO =:a";
                    co_new = connection.Query<AUP_TRANS_CONTRACT_OFFER_WEBAPPS>(cariDataContract, new
                    {
                        a = data.CONTRACT_NO
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    var cariDataUrutan = @"SELECT A.URUTAN, A.MAX_URUTAN FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a AND A.USER_ID =:b AND A.SURAT_ID =:c ";
                    var hasilDataAtasan = connection.Query(cariDataUrutan, new { a = data.CONTRACT_NO, b = UserID, c = hasilIni.ID }).FirstOrDefault();

                    dynamic turnParaf = null;
                    if (hasilDataAtasan.URUTAN == hasilDataAtasan.MAX_URUTAN)
                    {
                        var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a AND a.IS_CREATE_SURAT = 1 AND  A.SURAT_ID =:b";
                        turnParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = hasilIni.ID }).FirstOrDefault();
                    }
                    else
                    {
                        var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                                    WHERE A.CONTRACT_NO =:a AND a.IS_CREATE_SURAT = 1 AND  A.SURAT_ID =:b";
                        turnParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = hasilIni.ID }).FirstOrDefault();
                        //var cariDataAtasan = @"SELECT A.USER_ID FROM PROP_DISPOSISI_PARAF A
                        //            WHERE A.CONTRACT_NO =:a AND A.SURAT_ID =:b AND A.URUTAN =: c";
                        //turnParaf = connection.Query(cariDataAtasan, new { a = data.CONTRACT_NO, b = hasilIni.ID, c = hasilDataAtasan.URUTAN + 1 }).FirstOrDefault();
                    }

                    dynamic dataEmail = null;
                    using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                    {
                        var cariEmail = @"SELECT USER_EMAIL, CONCAT(CONCAT(USER_NAME,' | '),C.ROLE_NAME) AS USER_NAME, A.USER_LOGIN FROM APP_USER A JOIN APP_USER_ROLE B ON B.USER_ID = A.ID JOIN APP_ROLE C ON C.ROLE_ID = B.ROLE_ID WHERE ID =:b";
                        dataEmail = conn.Query(cariEmail, new { b = turnParaf.USER_ID }).DefaultIfEmpty(null).FirstOrDefault();
                    }

                    DataSurat datasuratbaru = null;
                    var dataSuratNew = @"SELECT * FROM PROP_SURAT A WHERE A.ID =:a";
                    datasuratbaru = connection.Query<DataSurat>(dataSuratNew, new
                    {
                        a = data.SURAT_ID
                    }).DefaultIfEmpty(null).FirstOrDefault();

                    ContractOfferApprovalParameter param = new ContractOfferApprovalParameter();
                    param.USER_LOGIN = "";
                    param.APPROVAL_NOTE = "";

                    EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                    var sendingemail = dalEmail.emailRejected(co_new, datasuratbaru, KodeCabang, "setujui", param, userLogin, RoleName, dataEmail, data.MEMO);

                    //SEND NOTIF MOBILE
                    ListNotifikasiController listNotif = new ListNotifikasiController();
                    ParamNotification paramNotif = new ParamNotification();
                    paramNotif.username = dataEmail.USER_LOGIN;
                    paramNotif.type = "5"; //TYPE REVISE
                    paramNotif.notification = data.MEMO;
                    paramNotif.no_contract = data.CONTRACT_NO;
                    paramNotif.pengirim = userLogin;
                    var sendNotif = listNotif.addNewNotif(paramNotif);

                    if (r > 0 && w > 0 && y > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Rejected Successfully";

                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Rejected Data";
                        result.Status = "E";
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DisposisiHistory> GetDataHistoryDisposisi(string id)
        {
            IEnumerable<DisposisiHistory> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT A.CONTRACT_NO, A.MEMO, A.SUBJECT, A.TYPE, A.TIME_CREATE, A.NAMA, A.INSTRUKSI, A.NAMA_KEPADA
                                FROM PROP_HISTORY A
                                WHERE A.CONTRACT_NO = :b ORDER BY A.TIME_CREATE";

                    listData = connection.Query<DisposisiHistory>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public IDataTable GetDataAttachment(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT * FROM PROP_SURAT_ATTACHMENT A WHERE A.SURAT_ID =:c ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<dynamic>(fullSql, new { a = start, b = end, c = id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT * FROM PROP_SURAT_ATTACHMENT A WHERE A.SURAT_ID =:c ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<dynamic>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = id,
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;

        }

        public IEnumerable<LIST_PARAF> ListDataParaf(dynamic id)
        {
            IEnumerable<LIST_PARAF> listData = null;

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT USER_ID, ROW_NUMBER() OVER (ORDER BY A.URUTAN desc) HIRARKI, NAMA
                                FROM PROP_DISPOSISI_PARAF A
                                WHERE A.SURAT_ID = :b AND ROLE_ID IS NULL AND IS_CREATE_SURAT = 0 AND TYPE = 'disposisi' ";

                    listData = connection.Query<LIST_PARAF>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }
        public IEnumerable<DisposisiListSurat> GetDataListDisposisiSurat(string id)
        {
            IEnumerable<DisposisiListSurat> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT A.CONTRACT_NO, A.ISI_SURAT, A.SUBJECT, A.COUNT_SURAT
                                FROM PROP_SURAT A
                                WHERE A.STATUS_KIRIM = 'Send' AND A.CONTRACT_NO = :b ORDER BY A.TIME_CREATE";

                    listData = connection.Query<DisposisiListSurat>(sql, new { b = id }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

        public string GetDataHiddenCreateSurat(dynamic contract_no, dynamic role_id)
        {
            dynamic data = null;
            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection())
                {
                    string sql = @"SELECT ROLE_ID FROM PROP_DISPOSISI_PARAF WHERE CONTRACT_NO =:a AND ROLE_ID =:b ";

                    data = connection.Query(sql, new { a = contract_no, b = role_id }).DefaultIfEmpty(null).FirstOrDefault();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            if (data == null)
            {
                return "0";
            }
            else
            {
                return Convert.ToString(data.ROLE_ID);
            }
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM PROP_SURAT_ATTACHMENT " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_SURAT_ATTACHMENT " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results AddDataAttachment(dynamic data, dynamic directory, dynamic file_name)
        {
            Results result = new Results();
            var ID = data[0];
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                    int w = connection.Execute(sql2, new
                    {
                        a = ID,
                        b = file_name,
                        c = directory,
                    });
                    if (w > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)

                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results AddDataAttachmentEdit(dynamic data, dynamic directory, dynamic file_name)
        {
            Results result = new Results();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
                try
                {
                    var sql2 = @"insert into PROP_SURAT_ATTACHMENT (SURAT_ID, FILE_NAME, 
                            DIRECTORY)
                            values(:a, :b, :c)";
                    int w = connection.Execute(sql2, new
                    {
                        a = data,
                        b = file_name,
                        c = directory,
                    });
                    if (w > 0)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex)

                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }

            connection.Close();
            connection.Dispose();
            return result;
        }
    }
}