﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransVB;
using Remote.Models.TransVBList;
using Remote.Models.Select2;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels; 
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransVBBillingDAL
    {
        //------------------------------ DEPRECATED ----------------------------------------
        public DataTablesTransVB GetDataVBTransBilling(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {

                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (KodeCabang == "0" && UserRole == "4")
                {
                    if (search.Length == 0)
                    {
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT t.ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, t.PROFIT_CENTER, t.PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, POSTING_DATE POS_DATE, COSTUMER_MDM, COSTUMER_ID, t.BE_ID, SERVICES_GROUP, COSTUMER_NAME, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, p.TERMINAL_NAME, SAP_DOCUMENT_NUMBER, BILLING_TYPE " +
                                         "FROM PROP_VB_TRANSACTION t, PROFIT_CENTER p WHERE t.PROFIT_CENTER = p.PROFIT_CENTER_ID(+) AND " +
                                         "((t.ID LIKE '%'||:c||'%') OR (t.PROFIT_CENTER LIKE '%' ||:c|| '%') OR (UPPER(TERMINAL_NAME) LIKE '%' ||:c|| '%') OR (UPPER(COSTUMER_NAME) LIKE '%' ||:c|| '%') OR (to_char(POSTING_DATE,'DD.MM.YYYY') LIKE '%'||:c||'%') OR (TOTAL LIKE '%'||:c||'%') AND (SAP_DOCUMENT_NUMBER LIKE '%' ||:c|| '%'))";
                            */

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                        }
                    }
                }
                else if (KodeCabang == "0" && UserRole == "20")
                {
                    if (search.Length == 0)
                    {
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE SAP_DOCUMENT_NUMBER IS NULL AND BRANCH_ID=:c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {

                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, d = KodeCabang });
                        }
                    }
                }
                else
                {
                    if (search.Length == 0)
                    {
                        /*
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:c AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                        */
                        string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                    "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:c AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        //count = connection.ExecuteScalar<int>(fullSqlCount);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });

                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 0)
                        {
                            /*
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, REF_DATA || ' - ' || REF_DESC AS SERVICES_GROUP, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, VAL2, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (REF_DATA) || (UPPER(REF_DESC)) || (TOTAL) || (GL_ACCOUNT) || (UPPER(VAL2)) LIKE '%' ||:c|| '%') AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                           */
                            string sql = "SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, GL_ACCOUNT, KELOMPOK_PENDAPATAN " +
                                     "FROM V_TRANSVB_NEW WHERE BRANCH_ID=:d AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' ||:c|| '%') AND SAP_DOCUMENT_NUMBER IS NULL ORDER BY ID DESC";
                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new {a = start, b = end, c = search.ToUpper(), d = KodeCabang });
                        }
                    }
                }

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesTransVB GetDataTransactionVB(int draw, int start, int length, string search, string idVB)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = "SELECT d.ID, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, START_DATE AS S_DATE, END_DATE AS E_DATE, " +
                                 "SERVICE_CODE, SERVICE_NAME, CURRENCY, PRICE, MULTIPLY_FACTOR, QUANTITY, UNIT || ' | ' || (SELECT DISTINCT ref_desc FROM prop_parameter_ref_d WHERE UNIT = REF_DATA AND REF_CODE = 'UNIT') AS UNIT, SURCHARGE, AMOUNT, REMARK, VB_ID, CURRENCY ||'  '|| PRICE AS CM_PRICE " +
                                 "FROM PROP_VB_TRANSACTION_DETAIL d, PROP_VB_TRANSACTION v WHERE d.VB_ID=v.ID AND VB_ID = :c";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, c = idVB });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = idVB });
                }
                else
                {
                    // Do nothing. Not implemented yet.
                }

                result = new DataTablesTransVB();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();
            }
            catch (Exception) { result = null; }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataReturnSAP_COCUMENT_NUMBER PostSAP(string BILL_ID, string BILL_TYPE, string POSTING_DATE, string KodeCabang, string UserRole)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string XDOC_NUMBER = string.Empty;
            string retVal = null;
            string tahun = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "UPDATE PROP_VB_TRANSACTION " +
                  "SET POSTING_DATE=TO_DATE(:a,'DD/MM/YYYY') " +
                  "WHERE ID = :b";
                connection.Execute(sql, new
                {
                    a = POSTING_DATE,
                    b = BILL_ID
                });
            }
            catch (Exception) { }


            try
            {
                var p = new DynamicParameters();
                p.Add(":XBILL_ID", BILL_ID);
                p.Add(":XBILL_TYPE", BILL_TYPE);
                p.Add(":XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);
                
                connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
                retVal = p.Get<string>(":XDOC_NUMBER");
                tahun = retVal;
            }
            catch (Exception ex)
            {
                string hhh = ex.ToString();
                retVal = null;
            }

            if (string.IsNullOrEmpty(retVal) == false && retVal != "/0000")
            {
                if (KodeCabang != "0")
                {
                    try
                    {
                        string sql = "UPDATE PROP_VB_TRANSACTION " +
                          "SET SAP_DOCUMENT_NUMBER =:a, POSTING_DATE=sysdate " +
                          "WHERE ID = :b";
                        connection.Execute(sql, new
                        {
                            a = retVal,
                            b = BILL_ID
                        });
                    }
                    catch (Exception) { }
                }
                else if(KodeCabang == "0")
                {
                    try
                    {
                        string sql = "UPDATE PROP_VB_TRANSACTION " +
                          "SET SAP_DOCUMENT_NUMBER =:a, POSTING_DATE = TO_DATE(:c,'DD/MM/YYYY') " +
                          "WHERE ID = :b";
                        connection.Execute(sql, new
                        {
                            a = retVal,
                            b = BILL_ID,
                            c = POSTING_DATE
                        });
                    }
                    catch (Exception ex) { }
                }
                else
                {

                }
            }
            else
            {
                retVal = null;
            }

            // Tambahan pengecekan apakah retVal == /0000
            if (tahun == "/0000")
            {
                // Update Value E_DOCNUMBER di ITGR
                string cekEDOCNUMBER = "UPDATE ITGR_LOG WHERE E_DOCNUMBER='/0000' AND REF_DOC_NO='" + BILL_ID + "'";

                string cekITGRHEADER = connection.ExecuteScalar<string>("SELECT HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'");

                if (string.IsNullOrEmpty(cekITGRHEADER) == false)
                {
                    string hapusITGRHEADER = "DELETE FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'";
                    string hapusITGRITEM = "DELETE FROM ITGR_ITEM WHERE HEADER_ID='" + cekITGRHEADER + "'";
                }
            }
            
            connection.Close();
            connection.Dispose();

            var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
            resultfull.DOCUMENT_NUMBER = retVal;
            return resultfull;

        }

        public Array GetDataMsgSAP(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string xml = "";
            try
            {
                /*string a = "SELECT MAX(HEADER_ID) HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO=:REF_DOC_NO";
                string getID = connection.ExecuteScalar<string>(a, new
                {
                    REF_DOC_NO = BILL_ID
                });*/

                string sql = "SELECT XML_DATA FROM VW_ITGR_LOG " +
                  "WHERE REF_DOC_NO = :REF_DOC_NO";

                xml = connection.ExecuteScalar<string>(sql, new
                {
                    REF_DOC_NO = BILL_ID
                });
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            string[] msg = xml.Split('|');
            return msg;
        }


        //----------------------------- NEW GET DATA ----------------------------------
        public DataTablesTransVB GetDataVBTransBillingnew(int draw, int start, int length, string search, string KodeCabang, string UserRole, string select_cabang, string UserID)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransVB result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANSVB_WEBAPPS> listDataOperator = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string selectedCabang = (select_cabang != null && select_cabang != "") ? select_cabang : KodeCabang;
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                string wheresearch = (search.Length >= 2 ? " AND ((ID) || (SAP_DOCUMENT_NUMBER) || (POSTING_DATE) || (UPPER(PROFIT_CENTER)) || (UPPER(TERMINAL_NAME)) || (UPPER(COSTUMER_MDM)) || (COSTUMER_ID) || (TOTAL) || (GL_ACCOUNT) LIKE '%' || '" + search.ToUpper() + "' || '%') " : "");
                QueryGlobal query = new QueryGlobal();
                string v_be = query.Query_BE();

                //string sql = @"SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, 
                //                COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, 
                //                GL_ACCOUNT, KELOMPOK_PENDAPATAN, BRANCH_ID 
                //            ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID =:u AND a.JENIS_TRANSAKSI = '4-Other Service') ROLE_OTHER
                //             FROM V_TRANSVB_NEW WHERE SAP_DOCUMENT_NUMBER IS NULL AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch; //+ " ORDER BY ID DESC";

                string sql = @"SELECT ID, to_char(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, COSTUMER_MDM, 
                                COSTUMER_ID, INSTALLATION_ADDRESS, CUSTOMER_SAP_AR, TOTAL, TERMINAL_NAME, SAP_DOCUMENT_NUMBER, TAX_CODE, BILLING_TYPE, CANCEL_STATUS, 
                                GL_ACCOUNT, KELOMPOK_PENDAPATAN, BRANCH_ID, RATE, TOTAL / NVL(RATE,1) TOTAL_USD
                             FROM V_TRANSVB_NEW WHERE SAP_DOCUMENT_NUMBER IS NULL AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch + "order by id desc";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANSVB_WEBAPPS>(fullSql, new { a = start, b = end, d = selectedCabang, u = UserID });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                //count = connection.ExecuteScalar<int>(fullSqlCount);
                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = selectedCabang });

                //result = new DataTablesTransVB();
                //result.draw = draw;
                //result.recordsFiltered = count;
                //result.recordsTotal = count;
                //result.data = listDataOperator.ToList();
            }
            catch (Exception e) { result = null; }
            //connection.Close();
            result = new DataTablesTransVB();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public string PostSilapor(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            var resultfull = "E";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("BILL_NO", BILL_ID);

                connection.Execute("SILAPOR_OTHER", p, null, null, commandType: CommandType.StoredProcedure);
                resultfull = "S";
            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return resultfull;
        }
    }
}