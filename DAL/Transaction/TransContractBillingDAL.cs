﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.TransContractBilling;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class TransContractBillingDAL
    {
        //------------------------------- SEND DATA TO SAP -----------------------------
        //public DataReturnSAP_COCUMENT_NUMBER PostSAP(string BILL_ID, string BILL_TYPE)
        //{
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    string XDOC_NUMBER = string.Empty;
        //    var p = new DynamicParameters();
        //    //p.Add("XCONTRACT_NO", CONTRACT_NO);
        //    p.Add("XBILL_ID", BILL_ID);
        //    p.Add("XBILL_TYPE", BILL_TYPE);
        //    p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

        //    connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
        //    string retVal = p.Get<string>("XDOC_NUMBER");

        //    string sql = "UPDATE PROP_CONTRACT_BILLING_POST " +
        //          "SET SAP_DOC_NO =:a " +
        //          "WHERE BILLING_NO = :b";

        //    connection.Execute(sql, new
        //    {
        //        a = retVal,
        //        b = BILL_ID
        //    });

        //    var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
        //    resultfull.DOCUMENT_NUMBER = retVal;
        //    //resultfull.RESULT_STAT = (retVal != null) ? true : false;
        //    return resultfull;
        //}
        public DataReturnSAP_COCUMENT_NUMBER PostSAPdummy(string BILL_ID, string BILL_TYPE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string XDOC_NUMBER = string.Empty;
            string retVal = null;
            //string tahun = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //try
            //{
            //    var p = new DynamicParameters();
            //    p.Add("XBILL_ID", BILL_ID);
            //    p.Add("XBILL_TYPE", BILL_TYPE);
            //    p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

            //    connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
            //    retVal = p.Get<string>("XDOC_NUMBER");
            //}
            //catch (Exception)
            //{
            //    retVal = null;
            //}

            retVal = "1234512345";
            if (string.IsNullOrEmpty(retVal) == false && retVal != "/0000")
            {
                try
                {
                    string sql = "UPDATE PROP_CONTRACT_BILLING_POST " +
                      "SET SAP_DOC_NO =:a " +
                      "WHERE BILLING_NO = :b";
                    connection.Execute(sql, new
                    {
                        a = retVal,
                        b = BILL_ID,
                    });
                }
                catch (Exception) { }
            }
            else
            {
                retVal = null;
            }

            // Tambahan pengecekan apakah retVal == /0000
            //if (tahun == "/0000")
            //{
            //    // Update Value E_DOCNUMBER di ITGR
            //    string cekEDOCNUMBER = "UPDATE ITGR_LOG WHERE E_DOCNUMBER='/0000' AND REF_DOC_NO='" + BILL_ID + "'";

            //    string cekITGRHEADER = connection.ExecuteScalar<string>("SELECT HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'");

            //    if (string.IsNullOrEmpty(cekITGRHEADER) == false)
            //    {
            //        string hapusITGRHEADER = "DELETE FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'";
            //        string hapusITGRITEM = "DELETE FROM ITGR_ITEM WHERE HEADER_ID='" + cekITGRHEADER + "'";
            //    }
            //}
            connection.Close();
            connection.Dispose();

            var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
            resultfull.DOCUMENT_NUMBER = retVal;
            resultfull.BILL_ID = BILL_ID;
            //resultfull.RESULT_STAT = (retVal != null) ? true : false;
            return resultfull;

        }

        public DataReturnSAP_COCUMENT_NUMBER PostSAP(string BILL_ID, string BILL_TYPE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string XDOC_NUMBER = string.Empty;
            string retVal = null;
            string tahun = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("XBILL_ID", BILL_ID);
                p.Add("XBILL_TYPE", BILL_TYPE);
                p.Add("XDOC_NUMBER", XDOC_NUMBER, dbType: DbType.String, direction: ParameterDirection.Output);

                //connection.Execute("ITGR_MAPPING_DEV", p, null, null, commandType: CommandType.StoredProcedure);
                connection.Execute("ITGR_MAPPING", p, null, null, commandType: CommandType.StoredProcedure);
                retVal = p.Get<string>("XDOC_NUMBER");
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
                retVal = null;
            }

            if (string.IsNullOrEmpty(retVal) == false && retVal != "/0000")
            {
                try
                {
                    string sql = "UPDATE PROP_CONTRACT_BILLING_POST " +
                      "SET SAP_DOC_NO =:a " +
                      "WHERE BILLING_NO = :b";
                    connection.Execute(sql, new
                    {
                        a = retVal,
                        b = BILL_ID
                    });
                }
                catch (Exception) { }
            }
            else
            {
                retVal = null;
            }

            // Tambahan pengecekan apakah retVal == /0000
            if (tahun == "/0000")
            {
                // Update Value E_DOCNUMBER di ITGR
                string cekEDOCNUMBER = "UPDATE ITGR_LOG WHERE E_DOCNUMBER='/0000' AND REF_DOC_NO='" + BILL_ID + "'";

                string cekITGRHEADER = connection.ExecuteScalar<string>("SELECT HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'");

                if (string.IsNullOrEmpty(cekITGRHEADER) == false)
                {
                    string hapusITGRHEADER = "DELETE FROM ITGR_HEADER WHERE REF_DOC_NO='" + BILL_ID + "'";
                    string hapusITGRITEM = "DELETE FROM ITGR_ITEM WHERE HEADER_ID='" + cekITGRHEADER + "'";
                }
            }
            connection.Close();
            connection.Dispose();

            var resultfull = new DataReturnSAP_COCUMENT_NUMBER();
            resultfull.DOCUMENT_NUMBER = retVal;
            resultfull.BILL_ID = BILL_ID;
            //resultfull.RESULT_STAT = (retVal != null) ? true : false;
            return resultfull;

        }

        public Array GetDataMsgSAP(string BILL_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string xml = "";
            try
            {
                /*string a = "SELECT MAX(HEADER_ID) HEADER_ID FROM ITGR_HEADER WHERE REF_DOC_NO=:REF_DOC_NO";
                string getID = connection.ExecuteScalar<string>(a, new
                {
                    REF_DOC_NO = BILL_ID
                });*/

                string sql = "SELECT XML_DATA FROM VW_ITGR_LOG " +
                  "WHERE REF_DOC_NO = :REF_DOC_NO";

                xml = connection.ExecuteScalar<string>(sql, new
                {
                    REF_DOC_NO = BILL_ID
                });
            }
            catch (Exception) { }
            connection.Close();
            connection.Dispose();
            string[] msg = xml.Split('|');

            return msg;
        }

        //--------------------- UPDATE SAP DOC NO ----------------------
        //public bool UpdateDocNo( DATAREF_D data)
        //{
        //    bool result = false;
        //    IDbConnection connection = DatabaseFactory.GetConnection();

        //    string sql = "UPDATE PROP_PARAMETER_REF_D SET " +
        //                 "REF_DESC=:b " +
        //                 "WHERE ID=:a";

        //    int r = connection.Execute(sql, new
        //    {
        //        a = data.ID,
        //        b = data.REF_DESC
        //    });

        //    result = (r > 0) ? true : false;

        //    return result;
        //}

        //-------------------- DEPRECATED --------------------
        public DataTablesTransContractBilling GetDataShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                 "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                 "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_CODE || ' - ' || PROFIT_CENTER_NAME AS PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO " +
                                 "FROM V_FILTER_CONTRACT_BILLING B WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) ORDER BY BILLING_PERIOD ASC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                 "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                 "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_CODE || ' - ' || PROFIT_CENTER_NAME AS PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO " +
                                 "FROM V_FILTER_CONTRACT_BILLING B WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) AND " +
                                 "((UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (BE_ID LIKE '%' ||:c|| '%') OR (BILLING_CONTRACT_NO LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%' ||:c|| '%') OR " +
                                 "(UPPER(BILLING_DUE_DATE) LIKE '%' ||:c|| '%') OR (CUSTOMER_CODE LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (INSTALLMENT_AMOUNT LIKE '%' ||:c|| '%')) " +
                                 "ORDER BY BILLING_PERIOD ASC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = search });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search }); 
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                 "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                 "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_CODE || ' - ' || PROFIT_CENTER_NAME AS PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                                 "FROM V_FILTER_CONTRACT_BILLING B WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) AND BRANCH_ID=:c ORDER BY BILLING_PERIOD ASC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_CODE || ' - ' || PROFIT_CENTER_NAME AS PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                                "FROM V_FILTER_CONTRACT_BILLING B WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) AND " +
                                "((UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (BE_ID LIKE '%' ||:c|| '%') OR (BILLING_CONTRACT_NO LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%' ||:c|| '%') OR " +
                                "(UPPER(BILLING_DUE_DATE) LIKE '%' ||:c|| '%') OR (CUSTOMER_CODE LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (INSTALLMENT_AMOUNT LIKE '%' ||:c|| '%')) " +
                                "AND BRANCH_ID = :d ORDER BY BILLING_PERIOD ASC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = search, d = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search, d = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesTransContractBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------- DEPRECATED ---------------------
        public DataTablesTransContractBilling GetDataFilterBilling(int draw, int start, int length, string contract, string customer, string periode, string date_end, string search, string KodeCabang)
        {
            string a_contract = string.IsNullOrEmpty(contract) ? "x" : contract;
            string b_customer = string.IsNullOrEmpty(customer) ? "x" : customer;
            string c_periode = string.IsNullOrEmpty(periode) ? "x" : periode;
            string d_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;
                      
            int count = 0;
            int end = start + length;
            DataTablesTransContractBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (a_contract.Length >= 0 && b_customer.Length >= 0 && c_periode.Length >= 0 && d_date_end.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                            "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                            "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                            "FROM V_FILTER_CONTRACT_BILLING B " +
                            "WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) " +
                            "AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) " +
                            "AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) " +
                            "AND (BILLING_PERIOD <= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) " +
                            "OR TRUNC(DUE_DATE) <= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " +
                            "ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);
                            listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                a_contract = a_contract,
                                b_customer = b_customer,
                                c_periode = c_periode,
                                d_date_end = d_date_end
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a_contract = a_contract,
                                b_customer = b_customer,
                                c_periode = c_periode,
                                d_date_end = d_date_end
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                    "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                    "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                                    "FROM V_FILTER_CONTRACT_BILLING B " +
                                    "WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) " +
                                    "AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) " +
                                    "AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) " +
                                    "AND (BILLING_PERIOD <= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) " +
                                    "OR TRUNC(DUE_DATE) <= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " +
                                    "AND ((UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (BE_ID LIKE '%' ||:c|| '%') OR (BILLING_CONTRACT_NO LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%' ||:c|| '%') OR " +
                                    "(UPPER(BILLING_DUE_DATE) LIKE '%' ||:c|| '%') OR (CUSTOMER_CODE LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (INSTALLMENT_AMOUNT LIKE '%' ||:c|| '%'))" +
                                    "ORDER BY ID DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    a_contract = a_contract,
                                    b_customer = b_customer,
                                    c_periode = c_periode,
                                    d_date_end = d_date_end
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    a_contract = a_contract,
                                    b_customer = b_customer,
                                    c_periode = c_periode,
                                    d_date_end = d_date_end
                                }); 

                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (a_contract.Length >= 0 && b_customer.Length >= 0 && c_periode.Length >= 0 && d_date_end.Length >= 0)
                {
                    if (search.Length == 0) 
                    {
                        try
                        {
                            string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                    "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                    "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                                    "FROM V_FILTER_CONTRACT_BILLING B " +
                                    "WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) " +
                                    "AND BRANCH_ID=:d AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) " +
                                    "AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) " +
                                    "AND (BILLING_PERIOD <= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) " +
                                    "OR TRUNC(DUE_DATE) <= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " +
                                    "ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);
                            listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new
                            {
                                a = start,
                                b = end,
                                a_contract = a_contract,
                                b_customer = b_customer,
                                c_periode = c_periode,
                                d_date_end = d_date_end,
                                d = KodeCabang
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a_contract = a_contract,
                                b_customer = b_customer,
                                c_periode = c_periode,
                                d_date_end = d_date_end,
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else 
                    {
                        if (search.Length > 2) 
                        {
                            try
                            {
                                string sql = "SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, " +
                                    "FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, " +
                                    "BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE " +
                                    "FROM V_FILTER_CONTRACT_BILLING B " +
                                    "WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) " +
                                    "AND BRANCH_ID=:d AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) " +
                                    "AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) " +
                                    "AND (BILLING_PERIOD <= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) " +
                                    "OR TRUNC(DUE_DATE) <= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " +
                                    "AND ((UPPER(BE_NAME) LIKE '%' ||:c|| '%') OR (BE_ID LIKE '%' ||:c|| '%') OR (BILLING_CONTRACT_NO LIKE '%' ||:c|| '%') OR (BILLING_PERIOD LIKE '%' ||:c|| '%') OR " +
                                    "(UPPER(BILLING_DUE_DATE) LIKE '%' ||:c|| '%') OR (CUSTOMER_CODE LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (INSTALLMENT_AMOUNT LIKE '%' ||:c|| '%')) " +
                                    "ORDER BY ID DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    d = KodeCabang,
                                    a_contract = a_contract,
                                    b_customer = b_customer,
                                    c_periode = c_periode,
                                    d_date_end = d_date_end
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    d = KodeCabang,
                                    a_contract = a_contract,
                                    b_customer = b_customer,
                                    c_periode = c_periode,
                                    d_date_end = d_date_end
                                });

                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                }
            }

            result = new DataTablesTransContractBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------ INSERT DATA HEADER BILLING ------------------
        public DATA_RETURN_SAP_DOCUMENT_NUMBER AddHeader(string updateBy, DataTransContractBilling data, string KodeCabang)
        {

            string billing_no = "";
            string billing_contract_no = "";
            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "INSERT INTO PROP_CONTRACT_BILLING_POST " +
                "(POSTING_DATE, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, FREQ_NO, " +
                "NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, INSTALLMENT_AMOUNT, BEGIN_BALANCE_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, LEGAL_CONTRACT_NO, COMPANY_CODE, BRANCH_ID, BILLING_ID, CREATION_BY) " +
                "VALUES (to_date(:a,'DD/MM/YYYY'), :b, :c, :d, :e, :f, :g, to_date(:h,'DD/MM/YYYY'), :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :x) ";

            int r = connection.Execute(sql, new
            {
                a = data.POSTING_DATE,
                b = data.BE_ID,
                c = data.PROFIT_CENTER_ID,
                d = data.PROFIT_CENTER_CODE,
                e = data.BILLING_CONTRACT_NO,
                f = data.CUSTOMER_CODE,
                g = data.CUSTOMER_CODE_SAP,
                h = data.BILLING_DUE_DATE,
                i = data.BILLING_PERIOD,
                j = data.FREQ_NO,
                k = data.NO_REF1,
                l = data.NO_REF2,
                m = data.NO_REF3,
                n = data.CURRENCY_CODE,
                o = data.INSTALLMENT_AMOUNT,
                p = data.BEGIN_BALANCE_AMOUNT,
                q = data.ENDING_BALANCE_AMOUNT,
                r = data.BILLING_FLAG_TAX1,
                s = data.BILLING_FLAG_TAX2,
                t = data.LEGAL_CONTRACT_NO,
                u = "1000",
                v = KodeCabang,
                w = data.ID,
                x = updateBy
            });

            billing_no = connection.ExecuteScalar<string>("SELECT BILLING_NO FROM PROP_CONTRACT_BILLING_POST WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT_BILLING_POST)");

            //iki gawe insert ke detail
            string ins = "INSERT INTO PROP_CONTRACT_BILLING_POST_D (SEQ_NO, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, OBJECT_ID, " +
                         "NO_REF3, NO_REF2, NO_REF1, LAST_UPDATE_DATE, LAST_UPDATE_BY, INSTALLMENT_AMOUNT, ID, GL_ACOUNT_NO, " +
                         "ENDING_BALANCE_AMOUNT, CUSTOMER_CODE_SAP, CUSTOMER_CODE, CURRENCY_CODE, CREATION_BY, " +
                         "CONDITION_TYPE, COMPANY_CODE, BILLING_UOM, BILLING_QTY, BILLING_PERIOD, BILLING_NO, BILLING_ID, BILLING_FLAG_TAX2, " +
                         "BILLING_FLAG_TAX1, BILLING_DUE_DATE, BILLING_CONTRACT_NO, BEGIN_BALANCE_AMOUNT, BE_ID) " +
                         "SELECT SEQ_NO, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, OBJECT_ID, " +
                         "NO_REF3, NO_REF2, NO_REF1, LAST_UPDATE_DATE, LAST_UPDATE_BY, INSTALLMENT_AMOUNT, ID, GL_ACOUNT_NO, " +
                         "ENDING_BALANCE_AMOUNT, CUSTOMER_CODE_SAP, CUSTOMER_CODE, CURRENCY_CODE, :a, " +
                         "CONDITION_TYPE, COMPANY_CODE, BILLING_UOM, BILLING_QTY, BILLING_PERIOD, '" + billing_no + "', BILLING_ID, " +
                         "(SELECT VAL4 FROM PROP_PARAMETER_REF_D WHERE CONDITION_TYPE = REF_DATA AND REF_CODE = 'CONDITION_TYPE') BILLING_FLAG_TAX2, " +
                         "BILLING_FLAG_TAX1, BILLING_DUE_DATE, BILLING_CONTRACT_NO, BEGIN_BALANCE_AMOUNT, BE_ID " +
                         "FROM PROP_CONTRACT_BILLING_D WHERE BILLING_ID = :ID";
            int s = connection.Execute(ins, new
            {
                ID = data.ID,
                a = updateBy

            });

            billing_contract_no = data.BILLING_CONTRACT_NO;

            var resultfull = new DATA_RETURN_SAP_DOCUMENT_NUMBER();
            resultfull.BILLING_NO = billing_no;
            resultfull.BILLING_CONTRACT_NO = billing_contract_no;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        //------------------------ INSERT DETAIL BILLING ----------------------
        //public bool AddDetail(DataTransContractBilling data)
        //{
        //    string billing_no = "";
        //    bool result = false;
        //    IDbConnection connection = DatabaseFactory.GetConnection();

        //    vb_id = connection.ExecuteScalar<string>("SELECT BILLING_NO FROM PROP_CONTRACT_BILLING_POST WHERE ID=(SELECT MAX(ID) FROM PROP_CONTRACT_BILLING_POST)");

        //    string sql = "INSERT INTO PROP_CONTRACT_CONDITION " +
        //        "(CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, " +
        //        "MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE, CONTRACT_NO) " +
        //        "VALUES (:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i, to_date(:j,'DD/MM/YYYY'), :k, :l, :m, :n, :o, :p, :q, :z, :s, :t)";

        //    int r = connection.Execute(sql, new
        //    {
        //        a = data.CALC_OBJECT,
        //        b = data.CONDITION_TYPE,
        //        c = data.VALID_FROM,
        //        d = data.VALID_TO,
        //        e = data.MONTHS,
        //        f = data.STATISTIC,
        //        g = data.UNIT_PRICE,
        //        h = data.AMT_REF,
        //        i = data.FREQUENCY,
        //        j = data.START_DUE_DATE,
        //        k = data.MANUAL_NO,
        //        l = data.FORMULA,
        //        m = data.MEASUREMENT_TYPE,
        //        n = data.LUAS,
        //        o = data.TOTAL,
        //        p = data.NJOP_PERCENT,
        //        q = data.KONDISI_TEKNIS_PERCENT,
        //        z = data.SALES_RULE,
        //        s = data.TOTAL_NET_VALUE,
        //        t = vb_id
        //    });
        //    result = (r > 0) ? true : false;

        //    return result;
        //}

        //-------------------- DATA DETAIL OBJECT --------------------------
        public DataTablesTransContractBilling GetDataDetailBilling(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT BILLING_CONTRACT_NO, CONDITION_TYPE_NAME, OBJECT_ID, INSTALLMENT_AMOUNT, CURRENCY_CODE, BILLING_UOM, " +
                             "BILLING_QTY, GL_ACOUNT_NO, BILLING_DUE_DATE, BILLING_FLAG_TAX1 " +
                             "FROM V_CONTRACT_BILLING_DETAIL " +
                             "WHERE BILLING_ID=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesTransContractBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        //insert to silapor
        public string PostSilapor(string BILLING_CONTRACT_NO)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            var resultfull = "E";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("contract_nomor", BILLING_CONTRACT_NO);

                connection.Execute("SILAPOR_PROPERTY", p, null, null, commandType: CommandType.StoredProcedure);
                resultfull = "S";
            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return resultfull;
        }

        //------------------------- FILTER DATA BILLING ---------------------
        public DataTablesTransContractBilling GetDataFilterBillingnew(int draw, int start, int length, string contract, string customer, string periode, string date_end, string search, string KodeCabang, string select_cabang, string UserID)
        {
            string a_contract = string.IsNullOrEmpty(contract) ? "x" : contract;
            string b_customer = string.IsNullOrEmpty(customer) ? "x" : customer;
            string c_periode = string.IsNullOrEmpty(periode) ? "x" : periode;
            string d_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;
            string selectedCabang = (select_cabang != null && select_cabang != "") ? select_cabang : KodeCabang;

            int count = 0;
            int end = start + length;
            DataTablesTransContractBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            
            string wheresql = search.Length >= 2 ? " AND ( UPPER(BE_NAME) || BE_ID || BILLING_CONTRACT_NO || BILLING_PERIOD || UPPER(BILLING_DUE_DATE) || CUSTOMER_CODE || UPPER(CUSTOMER_NAME) || INSTALLMENT_AMOUNT ) LIKE '%' ||'" + search.ToUpper() + "'|| '%' " : "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (a_contract.Length >= 0 && b_customer.Length >= 0 && c_periode.Length >= 0 && d_date_end.Length >= 0)
            {
                try
                {
                    //string sql = @"SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, 
                    //        FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, 
                    //        BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE 
                    //        ,(select COUNT(a.JENIS_TRANSAKSI) from SETTING_POSTING_DETAIL a join SETTING_POSTING_HEADER b on a.HEADER_ID = b.ID where b.USER_ID  = :user_id AND a.JENIS_TRANSAKSI = '1-Properti') ROLE_PROPERTY

                    //        FROM V_FILTER_CONTRACT_BILLING B 
                    //        WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) 
                    //        AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) 
                    //        AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) 
                    //        AND (BILLING_PERIOD <= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) 
                    //        OR TRUNC(DUE_DATE) <= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " + wheresql + v_be +
                    //        " ORDER BY ID DESC";

                    string sql = @"SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, 
                            FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, 
                            BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE 
                            
                            FROM V_FILTER_CONTRACT_BILLING B 
                            WHERE B.INACTIVE_DATE IS NULL and NOT EXISTS (SELECT 'X' FROM PROP_CONTRACT_BILLING_POST P WHERE P.BILLING_ID = B.ID) 
                            AND BILLING_CONTRACT_NO = DECODE(:a_contract,'x',BILLING_CONTRACT_NO,:a_contract) 
                            AND CUSTOMER_CODE = DECODE(:b_customer,'x',CUSTOMER_CODE,:b_customer) 
                            AND (BILLING_PERIOD >= DECODE(:c_periode,'x', TO_CHAR(SYSDATE, 'RRRRMM'),:c_periode) 
                            OR TRUNC(DUE_DATE) >= TRUNC(DECODE(:d_date_end,'x',SYSDATE,TO_DATE(:d_date_end,'DD/MM/RRRR')))) " + wheresql + v_be +
                            " ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new
                    {
                        a = start,
                        b = end,
                        d = selectedCabang,//KodeCabang
                        a_contract = a_contract,
                        b_customer = b_customer,
                        c_periode = c_periode,
                        d_date_end = d_date_end,
                        user_id = UserID
                    });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                    {
                        a = start,
                        b = end,
                        d = KodeCabang,
                        a_contract = a_contract,
                        b_customer = b_customer,
                        c_periode = c_periode,
                        d_date_end = d_date_end
                    });

                }
                catch (Exception)
                {

                    throw;
                }
            }
            result = new DataTablesTransContractBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }


        //-------------------- FILTER SHOW ALL DATA BILLING --------------------
        public DataTablesTransContractBilling GetDataShowAllnew(int draw, int start, int length, string search, string KodeCabang, string UserID)
        {
            int count = 0;
            int end = start + length;
            DataTablesTransContractBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";


            string wheresql = search.Length >= 2 ? " AND ( UPPER(BE_NAME) || BE_ID || BILLING_CONTRACT_NO || BILLING_PERIOD || UPPER(BILLING_DUE_DATE) || CUSTOMER_CODE || UPPER(CUSTOMER_NAME) || INSTALLMENT_AMOUNT ) LIKE '%' ||" + search.ToUpper() + "|| '%' " : "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
            {
                string sql = @"SELECT ID, BE_ID, PROFIT_CENTER_ID, PROFIT_CENTER_CODE, BILLING_CONTRACT_NO, CUSTOMER_CODE, CUSTOMER_CODE_SAP, BILLING_DUE_DATE, BILLING_PERIOD, 
                    FREQ_NO, NO_REF1, NO_REF2, NO_REF3, CURRENCY_CODE, BEGIN_BALANCE_AMOUNT, INSTALLMENT_AMOUNT, ENDING_BALANCE_AMOUNT, BILLING_FLAG_TAX1, BILLING_FLAG_TAX2, to_char(CREATION_DATE,'DD/MM/YYYY') CREATION_DATE, 
                    BE_ID || ' - ' || BE_NAME AS BE_NAME, PROFIT_CENTER_CODE || ' - ' || PROFIT_CENTER_NAME AS PROFIT_CENTER_NAME, CUSTOMER_CODE || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CONTRACT_NAME, LEGAL_CONTRACT_NO, OLD_CONTRACT, INACTIVE_DATE 
                    FROM V_FILTER_CONTRACT_BILLING B WHERE B.INACTIVE_DATE IS NULL " + wheresql + v_be +
                    @" and b.DUE_DATE < last_day(add_months(trunc(sysdate,'mm'),3)) 
                    ORDER BY BILLING_PERIOD DESC";//and (b.DUE_DATE < sysdate(EXTRACT(MONTH FROM sysdate) = EXTRACT(MONTH FROM  b.DUE_DATE) and  EXTRACT(YEAR FROM sysdate) = EXTRACT(YEAR FROM  b.DUE_DATE)))

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_TRANS_CONTRACT_BILLING_WEBAPPS>(fullSql, new { a = start, b = end, d = KodeCabang, user_id = UserID });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            result = new DataTablesTransContractBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //insert to silapor
        public string PostTerminatedSilapor(string BILLING_CONTRACT_NO, string STOP_CONTRACT_DATE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            var resultfull = "E";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var p = new DynamicParameters();
                p.Add("contract_nomor", BILLING_CONTRACT_NO);

                connection.Execute("SILAPOR_PROPERTY_CANCEL", p, null, null, commandType: CommandType.StoredProcedure);
                resultfull = "S";
            }
            catch (Exception)
            {
            }
            connection.Close();
            connection.Dispose();
            return resultfull;
        }
    }
}