﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.MasterRentalObject;

namespace Remote.Controllers
{
    internal class ReportRODAL
    {
        public IEnumerable<DDBusinessEntity> GetDataRO(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        //----------------- DEPRECATED --------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 and BE_ID = :b order by profit_center_id asc";

                    //listData = connection.Query<DDProfitCenter>(sql).ToList();
                    listData = connection.Query<DDProfitCenter>(sql, new { b = BE_ID }).ToList();

                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        //----------------- DEPRECATED --------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        public IEnumerable<FDDProfitCenter> GetDataProfitCenternew(string be_id)
        {
            string sql = "SELECT PROFIT_CENTER_ID, TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID = :a and STATUS = 1";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<FDDProfitCenter> listDetil = connection.Query<FDDProfitCenter>(sql, new
            {
                a = be_id
            });
            connection.Close();
            return listDetil;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterDnew(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where be_id=:a AND STATUS = 1 " + v_be + " order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID, d = KodeCabang }).ToList();
                }
                catch (Exception ex) { string aaa = ex.ToString(); }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDRo> GetDataRONumb()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRo> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "select RO_CODE FROM RENTAL_OBJECT";

                listData = connection.Query<DDRo>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDZoneRip> GetDataZoneRip()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                //string sql = "select ZONE_RIP_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE ZONE_RIP_ID = ID) ZONE_RIP_NAME FROM RENTAL_OBJECT";
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='ZONA_RIP' AND ACTIVE='1'";

                listData = connection.Query<DDZoneRip>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDUsageTypeRo> GetDataUsageType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDUsageTypeRo> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                //string sql = "select USAGE_TYPE_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE USAGE_TYPE_ID = ID) USAGE_TYPE FROM RENTAL_OBJECT";
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='USAGE_TYPE_RO' AND ACTIVE='1' ";
                listData = connection.Query<DDUsageTypeRo>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDRoFunction> GetDataFunction()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRoFunction> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                //string sql = "select FUNCTION_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE CAST(REGEXP_REPLACE(FUNCTION_ID, '[^0-9]+', '') AS NUMBER) = ID) FUNCTION FROM RENTAL_OBJECT";
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RO_FUNCTION'";

                listData = connection.Query<DDRoFunction>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDRoFunction> GetDataVacancyReason()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRoFunction> listData = null;

            try
            {
                string sql = "SELECT REF_DESC, REF_DATA || ' - ' || REF_DESC REF_DESCRIPTION FROM PROP_PARAMETER_REF_D WHERE REF_CODE='VACANCY_REASON'";
                listData = connection.Query<DDRoFunction>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        //-------------------------- DEPRECATED ----------------------------------
        public IEnumerable<AUP_REPORT_RO> ExportExcel(string be_id, string profit_center, string ro_number, string zone_rip, string usage_type, string function, string vacancy_status, string vacancy_reason, string status, string available_date, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_ro_number = string.IsNullOrEmpty(ro_number) ? "x" : ro_number;
            string x_zone_rip = string.IsNullOrEmpty(zone_rip) ? "x" : zone_rip;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_function = string.IsNullOrEmpty(function) ? "x" : function;
            string x_vacancy_status = string.IsNullOrEmpty(vacancy_status) ? "x" : vacancy_status;
            string x_vacancy_reason = string.IsNullOrEmpty(vacancy_reason) ? "x" : vacancy_reason;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, " +
                            "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, " +
                            "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO, CONTRACT_NO, CONTRACT_OFFER_NO " +
                            "FROM V_REPORT_RO " +
                            "WHERE BE_ID = DECODE(NVL(:x_be_id,'x'), 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(NVL(:x_profit_center,'x'), 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) " +
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) " +
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) " +
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) AND VALID_FROM <= TO_DATE (:available_date, 'DD/MM/RRRR') " +
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR')";

                        listData = connection.Query<AUP_REPORT_RO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, " +
                            "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, " +
                            "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO, CONTRACT_NO, CONTRACT_OFFER_NO " +
                            "FROM V_REPORT_RO WHERE BRANCH_ID = :c " +
                            "AND BE_ID = DECODE(NVL(:x_be_id,'x'), 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(NVL(:x_profit_center,'x'), 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) " +
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) " +
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) " +
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) AND VALID_FROM <= TO_DATE (:available_date, 'DD/MM/RRRR') " +
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR')";

                        listData = connection.Query<AUP_REPORT_RO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            var where = KodeCabang == "0" ? "" : " and a.branch_id=" + KodeCabang;
            string sql_c = @"select b.MPLG_KODE, b.MPLG_NAMA, b.MPLG_BADAN_USAHA, a.CONTRACT_NO from PROP_CONTRACT a
                    join VW_CUSTOMERS b on a.BUSINESS_PARTNER = b.MPLG_KODE and a.BRANCH_ID = b.KD_CABANG
                    where a.CONTRACT_NO = :a  " + where + " order by a.CONTRACT_NO";
            //var data_c = connection.Query<AUP_REPORT_RO>(sql_c).ToList();

            string sql_co = @"select a.CONTRACT_OFFER_NUMBER CONTRACT_OFFER_NO, c.MPLG_KODE, c.MPLG_NAMA, c.MPLG_BADAN_USAHA, a.CUSTOMER_ID, a.BRANCH_ID,b.OBJECT_ID RO_CODE
                    from PROP_RENTAL_REQUEST_DETIL b
                    join PROP_RENTAL_REQUEST a on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                    left join VW_CUSTOMERS c on a.CUSTOMER_ID  = c.MPLG_KODE and to_char(a.BRANCH_ID) = to_char(c.KD_CABANG)
                    and a.CONTRACT_START_DATE <= to_date(:available_date, 'DD/MM/RRRR') and a.CONTRACT_END_DATE >= to_date(:available_date, 'DD/MM/RRRR') where b.OBJECT_ID =:a " + where;
            //var data_co = connection.Query<AUP_REPORT_RO>(sql_co).ToList();
            foreach (var temp in listData)
            {
                if (temp.STATUS == "OCCUPIED")
                {
                    //listDataOperator.AddRange(ocp);
                    var data_c = connection.Query<AUP_REPORT_RO>(sql_c, new { a = temp.CONTRACT_NO }).FirstOrDefault();
                    temp.MPLG_KODE = data_c.MPLG_KODE;
                    temp.MPLG_NAMA = data_c.MPLG_NAMA;
                    temp.MPLG_BADAN_USAHA = data_c.MPLG_BADAN_USAHA;
                }
                else if (temp.STATUS == "BOOKED")
                {
                    var data_co = connection.Query<AUP_REPORT_RO>(sql_co, new { a = temp.RO_CODE, available_date = available_date }).FirstOrDefault();
                    temp.MPLG_KODE = data_co.MPLG_KODE;
                    temp.MPLG_NAMA = data_co.MPLG_NAMA;
                    temp.MPLG_BADAN_USAHA = data_co.MPLG_BADAN_USAHA;
                    temp.CONTRACT_OFFER_NO = data_co.CONTRACT_OFFER_NO;
                }
                else
                {
                    temp.MPLG_KODE = "";
                    temp.MPLG_NAMA = "";
                    temp.MPLG_BADAN_USAHA = "";
                }

            }
            connection.Close();
            return listData;

        }

        //-------------------------- DEPRECATED ------------------------------------
        public DataTableReportRO GetDataFilter(int draw, int start, int length, string be_id, string profit_center, string ro_number, string zone_rip, string usage_type, string function, string vacancy_status, string vacancy_reason, string status, string available_date, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_ro_number = string.IsNullOrEmpty(ro_number) ? "x" : ro_number;
            string x_zone_rip = string.IsNullOrEmpty(zone_rip) ? "x" : zone_rip;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_function = string.IsNullOrEmpty(function) ? "x" : function;
            string x_vacancy_status = string.IsNullOrEmpty(vacancy_status) ? "x" : vacancy_status;
            string x_vacancy_reason = string.IsNullOrEmpty(vacancy_reason) ? "x" : vacancy_reason;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportRO result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<AUP_REPORT_RO> listDataOperator = new List<AUP_REPORT_RO>();

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string sql = "";
            if (KodeCabang == "0") {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                         sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, "+
                            "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, "+
                            "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO,CONTRACT_NO, CONTRACT_OFFER_NO " +
                            
                            "FROM V_REPORT_RO " +
                            "WHERE BE_ID = DECODE(NVL(:x_be_id,'x'), 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(NVL(:x_profit_center,'x'), 'x', PROFIT_CENTER_ID,:x_profit_center) "+
                            "AND STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) "+
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) "+
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) "+
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) AND VALID_FROM <= TO_DATE (:available_date, 'DD/MM/RRRR') "+
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR')";// "CONTRACT_NO, CONTRACT_OFFER_NO, MPLG_KODE, MPLG_NAMA, MPLG_BADAN_USAHA " +

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date
                        }).ToList();

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date
                        });
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
            }
            else {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                         sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, " +
                            "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, " +
                            "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO,CONTRACT_NO, CONTRACT_OFFER_NO " +
                            
                            "FROM V_REPORT_RO WHERE BRANCH_ID = :c " +
                            "AND BE_ID = DECODE(NVL(:x_be_id,'x'), 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(NVL(:x_profit_center,'x'), 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) " +
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) " +
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) " +
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) AND VALID_FROM <= TO_DATE (:available_date, 'DD/MM/RRRR') " +
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR')"; //"CONTRACT_NO, CONTRACT_OFFER_NO, MPLG_KODE, MPLG_NAMA, MPLG_BADAN_USAHA " +

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            c = KodeCabang
                        }).ToList();

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            var where = KodeCabang == "0" ? "": " and a.branch_id=" + KodeCabang;
            string sql_c = @"select b.MPLG_KODE, b.MPLG_NAMA, b.MPLG_BADAN_USAHA, a.CONTRACT_NO from PROP_CONTRACT a
                    join VW_CUSTOMERS b on a.BUSINESS_PARTNER = b.MPLG_KODE and a.BRANCH_ID = b.KD_CABANG
                    where a.CONTRACT_NO = :a  " + where + " order by a.CONTRACT_NO";
            //var data_c = connection.Query<AUP_REPORT_RO>(sql_c).ToList();

            string sql_co = @"select a.CONTRACT_OFFER_NUMBER CONTRACT_OFFER_NO, c.MPLG_KODE, c.MPLG_NAMA, c.MPLG_BADAN_USAHA, a.CUSTOMER_ID, a.BRANCH_ID,b.OBJECT_ID RO_CODE
                    from PROP_RENTAL_REQUEST_DETIL b
                    join PROP_RENTAL_REQUEST a on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                    join VW_CUSTOMERS c on a.CUSTOMER_ID  = c.MPLG_KODE and to_char(a.BRANCH_ID) = to_char(c.KD_CABANG)
                    and a.CONTRACT_START_DATE <= to_date(:available_date, 'DD/MM/RRRR') and a.CONTRACT_END_DATE >= to_date(:available_date, 'DD/MM/RRRR') where b.OBJECT_ID =:a " + where;
            //var data_co = connection.Query<AUP_REPORT_RO>(sql_co).ToList();
            foreach (var temp in listDataOperator)
            {
                if (temp.STATUS == "OCCUPIED")
                {
                    //listDataOperator.AddRange(ocp);
                    var data_c = connection.Query<AUP_REPORT_RO>(sql_c, new {a = temp.CONTRACT_NO }).FirstOrDefault();
                    temp.MPLG_KODE = data_c.MPLG_KODE;
                    temp.MPLG_NAMA = data_c.MPLG_NAMA;
                    temp.MPLG_BADAN_USAHA = data_c.MPLG_BADAN_USAHA;
                }
                else if(temp.STATUS == "BOOKED")
                {
                    var data_co = connection.Query<AUP_REPORT_RO>(sql_co, new { a = temp.RO_CODE, available_date = available_date }).FirstOrDefault();
                    temp.MPLG_KODE = data_co.MPLG_KODE;
                    temp.MPLG_NAMA = data_co.MPLG_NAMA;
                    temp.MPLG_BADAN_USAHA = data_co.MPLG_BADAN_USAHA;
                    temp.CONTRACT_OFFER_NO = data_co.CONTRACT_OFFER_NO;
                }
                else
                {
                    temp.MPLG_KODE = "";
                    temp.MPLG_NAMA = "";
                    temp.MPLG_BADAN_USAHA = "";
                }
                
            }
            result = new DataTableReportRO();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //-------------------------- NEW FILTER ------------------------------------
        public DataTableReportRO GetDataFilternew(int draw, int start, int length, string be_id, string profit_center, string ro_number, string zone_rip, string usage_type, string function, string vacancy_status, string vacancy_reason, string status, string available_date, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_ro_number = string.IsNullOrEmpty(ro_number) ? "x" : ro_number;
            string x_zone_rip = string.IsNullOrEmpty(zone_rip) ? "x" : zone_rip;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_function = string.IsNullOrEmpty(function) ? "x" : function;
            string x_vacancy_status = string.IsNullOrEmpty(vacancy_status) ? "x" : vacancy_status;
            string x_vacancy_reason = string.IsNullOrEmpty(vacancy_reason) ? "x" : vacancy_reason;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;
            string x_available_date = string.IsNullOrEmpty(available_date) ? "x" : available_date;

            string where1 = "";

            int count = 0;
            int end = start + length;
            DataTableReportRO result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<AUP_REPORT_RO> listDataOperator = new List<AUP_REPORT_RO>();

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string sql = "";
                        
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            where1 += (profit_center != "0" ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += " and BRANCH_ID = " + be_id;
            where1 += v_be;

                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                        sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, " +
                           "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, " +
                           "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO,CONTRACT_NO, CONTRACT_OFFER_NO " +

                           "FROM V_REPORT_RO " +
                           "WHERE  " +
                            "STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) " +
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) " +
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) " +
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) " +
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR') " + where1;//"CONTRACT_NO, CONTRACT_OFFER_NO, MPLG_KODE, MPLG_NAMA, MPLG_BADAN_USAHA " +

                    fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            d = KodeCabang
                        }).ToList();

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

            string vbe1 = query.Query_BE("a");
            var where = KodeCabang == "0" ? "" : " and a.branch_id=" + KodeCabang;
            string sql_c = @"select b.MPLG_KODE, b.MPLG_NAMA, b.MPLG_BADAN_USAHA, a.CONTRACT_NO from PROP_CONTRACT a
                    left join VW_CUSTOMERS b on a.BUSINESS_PARTNER = b.MPLG_KODE and a.BRANCH_ID = b.KD_CABANG
                    where a.CONTRACT_NO = :a  " + vbe1 + " order by a.CONTRACT_NO";
            //var data_c = connection.Query<AUP_REPORT_RO>(sql_c).ToList();

            string sql_co = @"select a.CONTRACT_OFFER_NUMBER CONTRACT_OFFER_NO, c.MPLG_KODE, c.MPLG_NAMA, c.MPLG_BADAN_USAHA, a.CUSTOMER_ID, a.BRANCH_ID,b.OBJECT_ID RO_CODE
                    from PROP_RENTAL_REQUEST_DETIL b
                    left join PROP_RENTAL_REQUEST a on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                    left join VW_CUSTOMERS c on a.CUSTOMER_ID  = c.MPLG_KODE and to_char(a.BRANCH_ID) = to_char(c.KD_CABANG)
                    and a.CONTRACT_START_DATE <= to_date(:available_date, 'DD/MM/RRRR') and a.CONTRACT_END_DATE >= to_date(:available_date, 'DD/MM/RRRR') where b.OBJECT_ID =:a " + vbe1;
            //var data_co = connection.Query<AUP_REPORT_RO>(sql_co).ToList();
            foreach (var temp in listDataOperator)
            {
                if (temp.STATUS == "OCCUPIED")
                {
                    //listDataOperator.AddRange(ocp);
                    var data_c = connection.Query<AUP_REPORT_RO>(sql_c, new { a = temp.CONTRACT_NO, d = KodeCabang }).FirstOrDefault();
                    if (data_c != null)
                    {
                        temp.MPLG_KODE = data_c.MPLG_KODE;
                        temp.MPLG_NAMA = data_c.MPLG_NAMA;
                        temp.MPLG_BADAN_USAHA = data_c.MPLG_BADAN_USAHA;
                    }
                    else
                    {
                        temp.MPLG_KODE = "";
                        temp.MPLG_NAMA = "";
                        temp.MPLG_BADAN_USAHA = "";
                    }
                }
                else if (temp.STATUS == "BOOKED")
                {
                    var data_co = connection.Query<AUP_REPORT_RO>(sql_co, new { a = temp.RO_CODE, available_date = available_date, d = KodeCabang }).FirstOrDefault();
                    if(data_co != null) { 
                        temp.MPLG_KODE = data_co.MPLG_KODE;
                        temp.MPLG_NAMA = data_co.MPLG_NAMA;
                        temp.MPLG_BADAN_USAHA = data_co.MPLG_BADAN_USAHA;
                        temp.CONTRACT_OFFER_NO = data_co.CONTRACT_OFFER_NO;
                    } else
                    {
                        temp.MPLG_KODE = "";
                        temp.MPLG_NAMA = "";
                        temp.MPLG_BADAN_USAHA = "";
                        temp.CONTRACT_OFFER_NO = "";
                    }
                }
                else
                {
                    temp.MPLG_KODE = "";
                    temp.MPLG_NAMA = "";
                    temp.MPLG_BADAN_USAHA = "";
                }

            }
            result = new DataTableReportRO();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<AUP_REPORT_RO> ExportExcelnew(string be_id, string profit_center, string ro_number, string zone_rip, string usage_type, string function, string vacancy_status, string vacancy_reason, string status, string available_date, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_ro_number = string.IsNullOrEmpty(ro_number) ? "x" : ro_number;
            string x_zone_rip = string.IsNullOrEmpty(zone_rip) ? "x" : zone_rip;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_function = string.IsNullOrEmpty(function) ? "x" : function;
            string x_vacancy_status = string.IsNullOrEmpty(vacancy_status) ? "x" : vacancy_status;
            string x_vacancy_reason = string.IsNullOrEmpty(vacancy_reason) ? "x" : vacancy_reason;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (profit_center != "0" ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += " and BRANCH_ID = " + be_id;
            where1 += v_be;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_ro_number.Length >= 0 && x_zone_rip.Length >= 0 && x_usage_type.Length >= 0 && x_function.Length >= 0 && x_vacancy_status.Length >= 0 && x_vacancy_reason.Length >= 0 && x_status.Length >= 0 && available_date.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT RO_NUMBER, RO_CODE, RO_NAME, ZONE_RIP, LAND_DIMENSION, BUILDING_DIMENSION, RO_CERTIFICATE_NUMBER, USAGE_TYPE, FUNCTION_NAME, STATUS, " +
                            "REASON, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE, ACTIVE, BE_NAME, PROFIT_CENTER, BE_ID, PROFIT_CENTER_ID, ZONE_RIP_ID, USAGE_TYPE_ID, " +
                            "FUNCTION_ID, BRANCH_ID, TO_CHAR(VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(VALID_TO, 'DD.MM.RRRR') VALID_TO, CONTRACT_NO, CONTRACT_OFFER_NO " +
                            "FROM V_REPORT_RO WHERE " +
                            "STATUS = DECODE(NVL(:x_vacancy_status,'x'), 'x', STATUS,:x_vacancy_status) AND NVL(REASON,'x')= DECODE(NVL(:x_vacancy_reason,'x'), 'x', NVL(REASON,'x'),:x_vacancy_reason) " +
                            "AND RO_CODE = DECODE(:x_ro_number, 'x', RO_CODE,:x_ro_number) AND ZONE_RIP_ID = DECODE(:x_zone_rip, 'x', ZONE_RIP_ID,:x_zone_rip) " +
                            "AND USAGE_TYPE_ID = DECODE(:x_usage_type, 'x', USAGE_TYPE_ID,:x_usage_type) AND FUNCTION_ID = DECODE(:x_function, 'x', FUNCTION_ID, :x_function) " +
                            "AND ACTIVE = DECODE(:x_status, 'x', ACTIVE,:x_status) AND VALID_FROM <= TO_DATE (:available_date, 'DD/MM/RRRR') " +
                            "AND VALID_TO >= TO_DATE (:available_date, 'DD/MM/RRRR')" + where1;

                        listData = connection.Query<AUP_REPORT_RO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_ro_number = x_ro_number,
                            x_zone_rip = x_zone_rip,
                            x_usage_type = x_usage_type,
                            x_function = x_function,
                            x_vacancy_status = x_vacancy_status,
                            x_vacancy_reason = x_vacancy_reason,
                            x_status = x_status,
                            available_date = available_date,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            string vbe1 = query.Query_BE("a");
            var where = KodeCabang == "0" ? "" : " and a.branch_id=" + KodeCabang;
            string sql_c = @"select b.MPLG_KODE, b.MPLG_NAMA, b.MPLG_BADAN_USAHA, a.CONTRACT_NO from PROP_CONTRACT a
                    join VW_CUSTOMERS b on a.BUSINESS_PARTNER = b.MPLG_KODE and a.BRANCH_ID = b.KD_CABANG
                    where a.CONTRACT_NO = :a  " + vbe1 + " order by a.CONTRACT_NO";
            //var data_c = connection.Query<AUP_REPORT_RO>(sql_c).ToList();

            string sql_co = @"select a.CONTRACT_OFFER_NUMBER CONTRACT_OFFER_NO, c.MPLG_KODE, c.MPLG_NAMA, c.MPLG_BADAN_USAHA, a.CUSTOMER_ID, a.BRANCH_ID,b.OBJECT_ID RO_CODE
                    from PROP_RENTAL_REQUEST_DETIL b
                    join PROP_RENTAL_REQUEST a on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO
                    left join VW_CUSTOMERS c on a.CUSTOMER_ID  = c.MPLG_KODE and to_char(a.BRANCH_ID) = to_char(c.KD_CABANG)
                    and a.CONTRACT_START_DATE <= to_date(:available_date, 'DD/MM/RRRR') and a.CONTRACT_END_DATE >= to_date(:available_date, 'DD/MM/RRRR') where b.OBJECT_ID =:a " + vbe1;
            //var data_co = connection.Query<AUP_REPORT_RO>(sql_co).ToList();
            foreach (var temp in listData)
            {
                if (temp.STATUS == "OCCUPIED")
                {
                    //listDataOperator.AddRange(ocp);
                    var data_c = connection.Query<AUP_REPORT_RO>(sql_c, new { a = temp.CONTRACT_NO, d = KodeCabang }).FirstOrDefault();
                    if (data_c != null)
                    {
                        temp.MPLG_KODE = data_c.MPLG_KODE;
                        temp.MPLG_NAMA = data_c.MPLG_NAMA;
                        temp.MPLG_BADAN_USAHA = data_c.MPLG_BADAN_USAHA;
                    }
                }
                else if (temp.STATUS == "BOOKED")
                {
                    var data_co = connection.Query<AUP_REPORT_RO>(sql_co, new { a = temp.RO_CODE, available_date = available_date, d = KodeCabang }).FirstOrDefault();
                    if (data_co != null)
                    {
                        temp.MPLG_KODE = data_co.MPLG_KODE;
                        temp.MPLG_NAMA = data_co.MPLG_NAMA;
                        temp.MPLG_BADAN_USAHA = data_co.MPLG_BADAN_USAHA;
                        temp.CONTRACT_OFFER_NO = data_co.CONTRACT_OFFER_NO;
                    }
                }
                else
                {
                    temp.MPLG_KODE = "";
                    temp.MPLG_NAMA = "";
                    temp.MPLG_BADAN_USAHA = "";
                }

            }
            connection.Close();
            return listData;

        }
    }
}