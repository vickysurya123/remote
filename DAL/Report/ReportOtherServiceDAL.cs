﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportOtherService;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ReportOtherServiceDAL
    {
        //-------------------------- SELECT2 BUSINESS ENTITY -----------------
        public IEnumerable<VW_BUSINESS_ENTITY> getBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<VW_BUSINESS_ENTITY> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<VW_BUSINESS_ENTITY>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<VW_BUSINESS_ENTITY>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        //-------------------------- SELECT2 PROFIT CENTER -----------------
        public IEnumerable<VW_PROFIT_CENTER> getProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<VW_PROFIT_CENTER> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where STATUS = 1 order by profit_center_id ASC";

                    listData = connection.Query<VW_PROFIT_CENTER>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id ASC";

                    listData = connection.Query<VW_PROFIT_CENTER>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        //public IEnumerable<VW_PROFIT_CENTER> getProfitCenterD(string KodeCabang, string BE_ID)
        //{
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<VW_PROFIT_CENTER> listData = null;

        //    if (KodeCabang == "0")
        //    {
        //        try
        //        {
        //            string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id ASC";
        //            listData = connection.Query<VW_PROFIT_CENTER>(sql, new { a = BE_ID }).ToList();
        //        }
        //        catch (Exception) { }
        //    }
        //    else
        //    {
        //        try
        //        {
        //            string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id ASC";
        //            listData = connection.Query<VW_PROFIT_CENTER>(sql, new { a = KodeCabang }).ToList();
        //        }
        //        catch (Exception) { }
        //    }


        //    return listData;
        //}

        public IEnumerable<VW_PROFIT_CENTER> getProfitCenterD(string KodeCabang, string BE_ID)
        {
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            //IEnumerable<VW_PROFIT_CENTER> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            //string where1 = "";
            ////where1 += (profit_center != "" ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            ////where1 += " and BE_ID = " + be_id;
            ////where1 += v_be;

            //if (KodeCabang == "0")
            //{
            //    try
            //    {
            //        string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id ASC";
            //        listData = connection.Query<VW_PROFIT_CENTER>(sql, new { a = BE_ID }).ToList();
            //    }
            //    catch (Exception) { }
            //}
            //else
            //{
            //    try
            //    {
            //        string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id ASC";
            //        listData = connection.Query<VW_PROFIT_CENTER>(sql, new { a = KodeCabang }).ToList();
            //    }
            //    catch (Exception) { }
            //}


            //return listData;

            //string sql = "SELECT PROFIT_CENTER_ID, profit_center_id || ' - ' || TERMINAL_NAME AS terminal_name FROM PROFIT_CENTER WHERE BRANCH_ID = :a and STATUS = 1 order by profit_center_id ASC";
            string sql = "SELECT PROFIT_CENTER_ID, profit_center_id || ' - ' || TERMINAL_NAME AS terminal_name FROM PROFIT_CENTER WHERE STATUS = 1 and be_id=:a " + v_be + " order by profit_center_id ASC";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<VW_PROFIT_CENTER> listDetil = connection.Query<VW_PROFIT_CENTER>(sql, new
            {
                a = BE_ID,
                d = KodeCabang
            });
            connection.Close();
            return listDetil;

        }

        //-------------------------- SELECT2 SERVICE GROUP -----------------
        public IEnumerable<VW_SERVICE_GROUP> getServiceGroup()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<VW_SERVICE_GROUP> listData = null;

            try
            {
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='SERVICE_GROUP' ";
                listData = connection.Query<VW_SERVICE_GROUP>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        //----------------------- BUTTON SHOW ALL ----------------
        public DataTableReportOtherService ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTableReportOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE FROM V_REPORT_OTHER_SERVICE";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_OTHER_SERVICES>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    { }
                }
                else
                {

                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                "FROM V_REPORT_OTHER_SERVICE WHERE BRANCH_ID = :c";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_OTHER_SERVICES>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    { }
                }
                else
                {

                }
            }

            result = new DataTableReportOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------------ DEPRECATED ---------------------------
        public DataTableReportOtherService GetDataFilter(int draw, int start, int length, string be_id, string profit_center, string service_group, string status, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_service_group.Length >= 0 && x_status.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                    "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                    "FROM V_REPORT_OTHER_SERVICE " +
                                    "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status)";

                            fullSql = fullSql.Replace("sql", sql);
                            listDataFilterDetail = connection.Query<AUP_REPORT_OTHER_SERVICES>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_profit_center = x_profit_center,
                                x_service_group = x_service_group,
                                x_status = x_status
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_profit_center = x_profit_center,
                                x_service_group = x_service_group,
                                x_status = x_status
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_service_group.Length >= 0 && x_status.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                    "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                    "FROM V_REPORT_OTHER_SERVICE WHERE BRANCH_ID = :d " +
                                    "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status)";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_service_group = x_service_group,
                            x_status = x_status,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_service_group = x_service_group,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }

            result = new DataTableReportOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //------------------------------ DEPRECATED ----------------------------------
        public IEnumerable<AUP_REPORT_OTHER_SERVICES> ShowFilterTwo(string be_id, string profit_center, string service_group, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_service_group.Length >= 0 && x_status.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                    "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                    "FROM V_REPORT_OTHER_SERVICE " +
                                    "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status)";


                        listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_service_group = x_service_group,
                            x_status = x_status
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                    "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                    "FROM V_REPORT_OTHER_SERVICE WHERE BRANCH_ID = :d " +
                                    "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status)";

                        listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_service_group = x_service_group,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        //----------------------------- DEPRECATED ---------------------------------
        public IEnumerable<AUP_REPORT_OTHER_SERVICES> ShowFilterTwoD(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE FROM V_REPORT_OTHER_SERVICE";
                    listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql);
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                "FROM V_REPORT_OTHER_SERVICE WHERE BRANCH_ID = :d ";

                    listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql, new
                    {
                        d = KodeCabang
                    });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;

        }

        //------------------------------ NEW GET DATA---------------------------------
        public DataTableReportOtherService GetDataFilternew(int draw, int start, int length, string be_id, string profit_center, string service_group, string status, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string wheresearch = (search.Length >= 2 ? " AND ((UPPER(GL_ACCOUNT) LIKE '%' || '" + search.ToUpper() + "' || '%') or (UPPER(BE_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (profit_center != "" ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(be_id) ? "" : " and BE_ID = " + be_id;
            where1 += v_be;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_service_group.Length >= 0 && x_status.Length >= 0)
            {
                try
                {
                    string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                "FROM V_REPORT_OTHER_SERVICE WHERE " +
                                "SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status) " + where1 + wheresearch;

                    fullSql = fullSql.Replace("sql", sql);
                    listDataFilterDetail = connection.Query<AUP_REPORT_OTHER_SERVICES>(fullSql, new
                    {
                        a = start,
                        b = end,
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_service_group = x_service_group,
                        x_status = x_status,
                        d = KodeCabang
                    });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                    {
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_service_group = x_service_group,
                        x_status = x_status,
                        d = KodeCabang
                    });
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }


            result = new DataTableReportOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //----------------------------- NEW EXCEL ----------------------------
        public IEnumerable<AUP_REPORT_OTHER_SERVICES> ShowFilterTwoDnew(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            //string where1 = "";
            //where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            try
            {
                string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                            "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                            "FROM V_REPORT_OTHER_SERVICE WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)  ";

                listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql, new
                {
                    d = KodeCabang
                });
            }
            catch (Exception)
            {
                listData = null;
            }

            connection.Close();
            return listData;

        }

        public IEnumerable<AUP_REPORT_OTHER_SERVICES> ShowFilterTwonew(string be_id, string profit_center, string service_group, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_service_group.Length >= 0 && x_status.Length >= 0)
            {
                try
                {
                    string sql = "SELECT BE_NAME, SERVICE_CODE, PROFIT_CENTER_NAME, SERVICE_GROUP_NAME, SERVICE_NAME, GL_ACCOUNT, " +
                                "UNIT, ACTIVE, PRICE, CURRENCY, MULTIPLY_FUNCTION, LEGAL_CONTRACT_NO, to_char(CONTRACT_DATE, 'DD.MM.YYYY') AS CONTRACT_DATE " +
                                "FROM V_REPORT_OTHER_SERVICE WHERE " +
                                " BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " + where1 +
                                "AND SERVICE_GROUP = DECODE (:x_service_group,'x',SERVICE_GROUP,:x_service_group) AND ACTIVE = DECODE(:x_status,'x',ACTIVE,:x_status) " + v_be;

                    listData = connection.Query<AUP_REPORT_OTHER_SERVICES>(sql, new
                    {
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_service_group = x_service_group,
                        x_status = x_status,
                        d = KodeCabang
                    });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;
        }
    }
}