﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;

namespace Remote.DAL
{
    public class ReportContractOfferDAL
    {
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from  profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }


            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "select PROFIT_CENTER_ID, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "select PROFIT_CENTER_ID, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<AUP_CONFIGURATION_WEBAPPS> GetDataOfferStatus(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> listData = null;
            try
            {
                string sql = "SELECT REF_DATA, ATTRIB1 FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONTRACT_OFFER_STATUS' AND REF_DATA != '10' ORDER BY REF_DATA ASC";

                listData = connection.Query<AUP_CONFIGURATION_WEBAPPS>(sql).ToList();
            }
            catch (Exception) { }


            connection.Close();
            return listData;
        }

        public IEnumerable<AUP_CONFIGURATION_WEBAPPS> GetDataOfferType(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> listData = null;

            try
            {
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='OFFER' ORDER BY REF_DATA ASC";

                listData = connection.Query<AUP_CONFIGURATION_WEBAPPS>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT CUSTOMER_ID code, CUSTOMER_ID || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name FROM PROP_RENTAL_REQUEST " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT CUSTOMER_ID code, CUSTOMER_ID || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM PROP_RENTAL_REQUEST " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        public DataTableReportContractOffer GetDataFilters(int draw, int start, int length, string be_id, string profit_center, string customer_id, string offer_status, string offer_type, string contract_offer_no, string valid_from, string valid_to, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_offer_status = string.IsNullOrEmpty(offer_status) ? "x" : offer_status;
            string x_contract_offer_type = string.IsNullOrEmpty(offer_type) ? "x" : offer_type;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTableReportContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_CONTRACT_OFFER> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && x_offer_status.Length >= 0 && x_contract_offer_type.Length >= 0 && x_contract_offer_no.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                            "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                            "FROM V_REPORT_CONTRACT_OFFER " +
                            "WHERE t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', o.PROFIT_CENTER,:x_profit_center) " +
                            "AND r.BE_ID = DECODE(:x_be_id, 'x', o.BE_ID,:x_be_id) " +
                            "AND r.CUSTOMER_ID = DECODE(:x_customer_id, 'x', r.CUSTOMER_ID,:x_customer_id) " +
                            "AND o.CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', o.CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND o.CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', o.CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (o.CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND o.OFFER_STATUS = DECODE(:x_offer_status, 'x', o.OFFER_STATUS,:x_offer_status)";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_CONTRACT_OFFER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception ex)
                    { string aaa = ex.ToString(); }
                }
                else
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                            "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                            "FROM V_REPORT_CONTRACT_OFFER " +
                            "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                            "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) " +
                            "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                            "AND CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND OFFER_STATUS = DECODE(:x_offer_status, 'x', OFFER_STATUS,:x_offer_status) AND BRANCH_ID = :d";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_CONTRACT_OFFER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }

                }
            }
            else
            {

            }

            result = new DataTableReportContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //---------------------------- DEPRECATED --------------------------------
        public IEnumerable<AUP_REPORT_CONTRACT_OFFER> ShowFilterTwo(string be_id, string profit_center, string customer_id, string offer_status, string offer_type, string contract_offer_no, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_CONTRACT_OFFER> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_offer_status = string.IsNullOrEmpty(offer_status) ? "x" : offer_status;
            string x_contract_offer_type = string.IsNullOrEmpty(offer_type) ? "x" : offer_type;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_offer_status.Length >= 0 && x_contract_offer_type.Length >= 0 && x_contract_offer_no.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /*string sql = "SELECT o.CONTRACT_OFFER_NO, o.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, o.RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, " +
                                "o.CONTRACT_START_DATE, o.CONTRACT_END_DATE, r.CUSTOMER_NAME, o.CUSTOMER_AR, o.CONTRACT_OFFER_TYPE, " +
                                "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='CONTRACT_USAGE' AND REF_DATA=o.CONTRACT_USAGE) CONTRACT_USAGE, " +
                                "(SELECT REF_DATA || ' - ' || REF_DESC CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='OFFER' AND REF_DATA=o.CONTRACT_OFFER_TYPE) OFFER_TYPE, o.CURRENCY, d.OBJECT_ID, RO_NAME, d.INDUSTRY, " +
                                "LAND_DIMENSION, BUILDING_DIMENSION, o.CREATION_BY, h.CONDITION_TYPE, h.TOTAL_NET_VALUE, " +
                                "(SELECT ATTRIB1 CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='CONTRACT_OFFER_STATUS' AND REF_DATA=o.OFFER_STATUS) CONTRACT_OFFER_STATUS " +
                                "FROM PROP_CONTRACT_OFFER o, PROP_CONTRACT_OFFER_CONDITION h, PROFIT_CENTER p, PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL d, RENTAL_OBJECT t " +
                                "WHERE o.PROFIT_CENTER=p.PROFIT_CENTER_ID AND o.RENTAL_REQUEST_NO = r.RENTAL_REQUEST_NO(+) " +
                                "AND o.RENTAL_REQUEST_NO = d.RENTAL_REQUEST_NO(+) AND d.RO_NUMBER=t.RO_NUMBER(+) " +
                                "AND o.CONTRACT_OFFER_NO = h.CONTRACT_OFFER_NO(+) " +
                                "AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', o.PROFIT_CENTER,:x_profit_center) " +
                                "AND r.BE_ID = DECODE(:x_be_id, 'x', o.BE_ID,:x_be_id) " +
                                "AND r.CUSTOMER_ID = DECODE(:x_customer_id, 'x', r.CUSTOMER_ID,:x_customer_id) " +
                                "AND o.CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', o.CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                                "AND o.CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', o.CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                                "AND (o.CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                                "AND o.OFFER_STATUS = DECODE(:x_offer_status, 'x', o.OFFER_STATUS,:x_offer_status)";*/
                        string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                            "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                            "FROM V_REPORT_CONTRACT_OFFER " +
                            "WHERE t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', o.PROFIT_CENTER,:x_profit_center) " +
                            "AND r.BE_ID = DECODE(:x_be_id, 'x', o.BE_ID,:x_be_id) " +
                            "AND r.CUSTOMER_ID = DECODE(:x_customer_id, 'x', r.CUSTOMER_ID,:x_customer_id) " +
                            "AND o.CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', o.CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND o.CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', o.CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (o.CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND o.OFFER_STATUS = DECODE(:x_offer_status, 'x', o.OFFER_STATUS,:x_offer_status)";

                        listData = connection.Query<AUP_REPORT_CONTRACT_OFFER>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        /*string sql = "SELECT o.CONTRACT_OFFER_NO, o.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, o.RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, " +
                                "o.CONTRACT_START_DATE, o.CONTRACT_END_DATE, r.CUSTOMER_NAME, o.CUSTOMER_AR, o.CONTRACT_OFFER_TYPE, " +
                                "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='CONTRACT_USAGE' AND REF_DATA=o.CONTRACT_USAGE) CONTRACT_USAGE, " +
                                "(SELECT REF_DATA || ' - ' || REF_DESC CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='OFFER' AND REF_DATA=o.CONTRACT_OFFER_TYPE) OFFER_TYPE, o.CURRENCY, d.OBJECT_ID, RO_NAME, d.INDUSTRY, " +
                                "LAND_DIMENSION, BUILDING_DIMENSION, o.CREATION_BY, h.CONDITION_TYPE, h.TOTAL_NET_VALUE, " +
                                "(SELECT ATTRIB1 CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                                "WHERE REF_CODE='CONTRACT_OFFER_STATUS' AND REF_DATA=o.OFFER_STATUS) CONTRACT_OFFER_STATUS " +
                                "FROM PROP_CONTRACT_OFFER o, PROP_CONTRACT_OFFER_CONDITION h, PROFIT_CENTER p, PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL d, RENTAL_OBJECT t " +
                                "WHERE o.BRANCH_ID = :d AND o.PROFIT_CENTER=p.PROFIT_CENTER_ID AND o.RENTAL_REQUEST_NO = r.RENTAL_REQUEST_NO(+) " +
                                "AND o.RENTAL_REQUEST_NO = d.RENTAL_REQUEST_NO(+) AND d.RO_NUMBER=t.RO_NUMBER(+) " +
                                "AND o.CONTRACT_OFFER_NO = h.CONTRACT_OFFER_NO(+) " +
                                "AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', o.PROFIT_CENTER,:x_profit_center) " +
                                "AND r.BE_ID = DECODE(:x_be_id, 'x', o.BE_ID,:x_be_id) " +
                                "AND r.CUSTOMER_ID = DECODE(:x_customer_id, 'x', r.CUSTOMER_ID,:x_customer_id) " +
                                "AND o.CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', o.CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                                "AND o.CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', o.CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                                "AND (o.CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                                "AND o.OFFER_STATUS = DECODE(:x_offer_status, 'x', o.OFFER_STATUS,:x_offer_status)";*/
                        string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                            "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                            "FROM V_REPORT_CONTRACT_OFFER " +
                            "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                            "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) " +
                            "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                            "AND CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND OFFER_STATUS = DECODE(:x_offer_status, 'x', OFFER_STATUS,:x_offer_status) AND BRANCH_ID = :d";

                        listData = connection.Query<AUP_REPORT_CONTRACT_OFFER>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_offer_status = x_offer_status,
                            x_contract_offer_type = x_contract_offer_type,
                            x_contract_offer_no = x_contract_offer_no,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        public DataTableReportContractOffer GetDataFiltersnew(int draw, int start, int length, string be_id, string profit_center, string customer_id, string offer_status, string offer_type, string contract_offer_no, string valid_from, string valid_to, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_offer_status = string.IsNullOrEmpty(offer_status) ? "x" : offer_status;
            string x_contract_offer_type = string.IsNullOrEmpty(offer_type) ? "x" : offer_type;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTableReportContractOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_CONTRACT_OFFER> listDataOperator = null;

            //string where = "";
            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (!string.IsNullOrEmpty(profit_center) ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(be_id) ? "" : " and BE_ID = " + be_id;
            where1 += v_be;

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && x_offer_status.Length >= 0 && x_contract_offer_type.Length >= 0 && x_contract_offer_no.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
            {

                try
                {
                    string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                            "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                            "FROM V_REPORT_CONTRACT_OFFER " +
                            "WHERE CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                            "AND CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND OFFER_STATUS = DECODE(:x_offer_status, 'x', OFFER_STATUS,:x_offer_status)" + where1;

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORT_CONTRACT_OFFER>(fullSql, new
                    {
                        a = start,
                        b = end,
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_customer_id = x_customer_id,
                        x_offer_status = x_offer_status,
                        x_contract_offer_type = x_contract_offer_type,
                        x_contract_offer_no = x_contract_offer_no,
                        x_valid_from = x_valid_from,
                        x_valid_to = x_valid_to,
                        d = KodeCabang
                    });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                    {
                        a = start,
                        b = end,
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_customer_id = x_customer_id,
                        x_offer_status = x_offer_status,
                        x_contract_offer_type = x_contract_offer_type,
                        x_contract_offer_no = x_contract_offer_no,
                        x_valid_from = x_valid_from,
                        x_valid_to = x_valid_to,
                        d = KodeCabang
                    });
                }
                catch (Exception e)
                { string aaa = e.ToString(); }

            }
            else
            {

            }

            result = new DataTableReportContractOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<AUP_REPORT_CONTRACT_OFFER> ShowFilterTwonew(string be_id, string profit_center, string customer_id, string offer_status, string offer_type, string contract_offer_no, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_CONTRACT_OFFER> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_offer_status = string.IsNullOrEmpty(offer_status) ? "x" : offer_status;
            string x_contract_offer_type = string.IsNullOrEmpty(offer_type) ? "x" : offer_type;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            //string where = "";
            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (!string.IsNullOrEmpty(profit_center) ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(be_id) ? "" : " and BE_ID = " + be_id;
            where1 += v_be;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_offer_status.Length >= 0 && x_contract_offer_type.Length >= 0 && x_contract_offer_no.Length >= 0)
            {
                try
                {
                    /*string sql = "SELECT o.CONTRACT_OFFER_NO, o.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, o.RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, " +
                            "o.CONTRACT_START_DATE, o.CONTRACT_END_DATE, r.CUSTOMER_NAME, o.CUSTOMER_AR, o.CONTRACT_OFFER_TYPE, " +
                            "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D " +
                            "WHERE REF_CODE='CONTRACT_USAGE' AND REF_DATA=o.CONTRACT_USAGE) CONTRACT_USAGE, " +
                            "(SELECT REF_DATA || ' - ' || REF_DESC CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                            "WHERE REF_CODE='OFFER' AND REF_DATA=o.CONTRACT_OFFER_TYPE) OFFER_TYPE, o.CURRENCY, d.OBJECT_ID, RO_NAME, d.INDUSTRY, " +
                            "LAND_DIMENSION, BUILDING_DIMENSION, o.CREATION_BY, h.CONDITION_TYPE, h.TOTAL_NET_VALUE, " +
                            "(SELECT ATTRIB1 CONTRACT_USAGE FROM PROP_PARAMETER_REF_D " +
                            "WHERE REF_CODE='CONTRACT_OFFER_STATUS' AND REF_DATA=o.OFFER_STATUS) CONTRACT_OFFER_STATUS " +
                            "FROM PROP_CONTRACT_OFFER o, PROP_CONTRACT_OFFER_CONDITION h, PROFIT_CENTER p, PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL d, RENTAL_OBJECT t " +
                            "WHERE o.BRANCH_ID = :d AND o.PROFIT_CENTER=p.PROFIT_CENTER_ID AND o.RENTAL_REQUEST_NO = r.RENTAL_REQUEST_NO(+) " +
                            "AND o.RENTAL_REQUEST_NO = d.RENTAL_REQUEST_NO(+) AND d.RO_NUMBER=t.RO_NUMBER(+) " +
                            "AND o.CONTRACT_OFFER_NO = h.CONTRACT_OFFER_NO(+) " +
                            "AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', o.PROFIT_CENTER,:x_profit_center) " +
                            "AND r.BE_ID = DECODE(:x_be_id, 'x', o.BE_ID,:x_be_id) " +
                            "AND r.CUSTOMER_ID = DECODE(:x_customer_id, 'x', r.CUSTOMER_ID,:x_customer_id) " +
                            "AND o.CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', o.CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                            "AND o.CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', o.CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                            "AND (o.CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                            "AND o.OFFER_STATUS = DECODE(:x_offer_status, 'x', o.OFFER_STATUS,:x_offer_status)";*/
                    string sql = "SELECT CONTRACT_OFFER_NO, PROFIT_CENTER, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(CONTRACT_END_DATE, 'DD.MM.RRRR') CONTRACT_END_DATE, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, CUSTOMER_AR, CONTRACT_OFFER_TYPE, " +
                        "CONTRACT_USAGE, OFFER_TYPE, CURRENCY, OBJECT_ID, RO_NAME, INDUSTRY, LAND_DIMENSION, BUILDING_DIMENSION, CREATION_BY, CONDITION_TYPE, TOTAL_NET_VALUE, CONTRACT_OFFER_STATUS " +
                        "FROM V_REPORT_CONTRACT_OFFER " +
                        "WHERE CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                        "AND CONTRACT_OFFER_NO = DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                        "AND CONTRACT_OFFER_TYPE = DECODE(:x_contract_offer_type, 'x', CONTRACT_OFFER_TYPE,:x_contract_offer_type) " +
                        "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " +
                        "AND OFFER_STATUS = DECODE(:x_offer_status, 'x', OFFER_STATUS,:x_offer_status)" + where1;

                    listData = connection.Query<AUP_REPORT_CONTRACT_OFFER>(sql, new
                    {
                        x_be_id = x_be_id,
                        x_profit_center = x_profit_center,
                        x_customer_id = x_customer_id,
                        x_offer_status = x_offer_status,
                        x_contract_offer_type = x_contract_offer_type,
                        x_contract_offer_no = x_contract_offer_no,
                        x_valid_from = x_valid_from,
                        x_valid_to = x_valid_to,
                        d = KodeCabang
                    });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;

        }

    }
}