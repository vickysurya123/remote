﻿using Dapper;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.GeneralResult;
using Remote.Models.UserModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.DAL
{
    public class PranotaDAL
    {
        public AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS GetPropertiHeader(string kc, string cn, string bn)
        {
            string sql = "";
            string param = "";
            if (cn != null && cn != "")
            {
                sql = @"select ID,BILLING_CONTRACT_NO,BE_ID,PROFIT_CENTER_ID,PROFIT_CENTER_CODE,CUSTOMER_CODE,CUSTOMER_CODE_SAP,BILLING_DUE_DATE,DUE_DATE POSTING_DATE,BILLING_PERIOD,FREQ_NO,NO_REF1,NO_REF2,NO_REF3,CURRENCY_CODE,BEGIN_BALANCE_AMOUNT,INSTALLMENT_AMOUNT,ENDING_BALANCE_AMOUNT,BILLING_FLAG_TAX1,
                        BILLING_FLAG_TAX2,CREATION_DATE,LEGAL_CONTRACT_NO,CONTRACT_NAME,BE_NAME,
                        PROFIT_CENTER_NAME,CUSTOMER_NAME,BRANCH_ID,BILLING_ID,INACTIVE_DATE,BE_ADDRESS,BE_CITY,MPLG_ALAMAT,LIVE_1PELINDO
                        from V_FILTER_CONTRACT_BILLING where BRANCH_ID = :a and  BILLING_CONTRACT_NO = :b and BILLING_ID IS NULL";
                param = cn;
            }
            else
            {
                sql = @"SELECT BILLING_FLAG_TAX2,LEGAL_CONTRACT_NO,POSTING_DATE,CREATION_DATE,LAST_UPDATE_DATE,CANCEL_STATUS,
                        BE_NAME,CONTRACT_NAME,PROFIT_CENTER_NAME,CUSTOMER_NAME,BRANCH_ID,ID,BE_ID,PROFIT_CENTER_ID,BILLING_CONTRACT_NO,PROFIT_CENTER_CODE,CUSTOMER_CODE,CUSTOMER_CODE_SAP,BILLING_DUE_DATE,BILLING_PERIOD,FREQ_NO,BILLING_NO,SAP_DOC_NO,NO_REF1,NO_REF2,
                        NO_REF3,CURRENCY_CODE,BEGIN_BALANCE_AMOUNT,INSTALLMENT_AMOUNT,ENDING_BALANCE_AMOUNT,BILLING_FLAG_TAX1 , BE_ADDRESS ,BE_CITY ,MPLG_ALAMAT,LIVE_1PELINDO
                        from V_CONTRACT_LIST_BILLING where BRANCH_ID = :a and  BILLING_NO = :b";
                param = bn;
            }

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS listDetil = connection.Query<AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS>(sql, new
            {
                a = kc,
                b = param
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }
        public List<PRANOTA_PROPERTI_DETAIL> GetPropertiDetail(string kc, string cn, string bn)
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where = "";
            string param = "";
            if (cn != null && cn != "")
            {
                where = " a.BILLING_CONTRACT_NO = :b";
                param = cn;
            }
            else
            {
                where = " a.BILLING_NO = :b";
                //where = " BILLING_CONTRACT_NO = :b";
                param = bn;
            }
            //string sql = @"SELECT CONTRACT_NO,CONDITION_TYPE,JANGKA_WAKTU,UNIT_PRICE,LUAS,NJOP_PERCENT,KONDISI_TEKNIS_PERCENT,TOTAL_NET_VALUE,MEMO,
            //                BILLING_ID,BILLING_PERIOD , decode(AMT_REF , 'ONETIME' ,'Sekali Bayar' , 'MONTHLY' ,'Bulanan' ,'YEARLY' ,'Tahunan' ,'-') AMT_REF , CURRENCY
            //                from V_ANJUNGAN_DETAIL_PROPERTY where KD_CABANG = :a and " + where;

            string sql = @"SELECT A.BILLING_NO , A.BILLING_ID, A.CUSTOMER_CODE || ' - ' || E.MPLG_NAMA AS ATAS_BEBAN, E.MPLG_ALAMAT ALAMAT, C.PROFIT_CENTER_ID || ' - ' || C.TERMINAL_NAME AS PROFIT_CENTER,
                            D.CONDITION_TYPE PERHITUNGAN, TO_CHAR(D.VALID_FROM,'DD.MM.RRRR') || ' - ' || TO_CHAR(D.VALID_TO,'DD.MM.RRRR') AS JANGKA_WAKTU, D.UNIT_PRICE TARIF_DASAR,
                            B.INSTALLMENT_AMOUNT,
                            CASE 
                                WHEN D.AMT_REF = 'ONETIME' THEN 'Sekali Bayar'
                                WHEN D.AMT_REF = 'MONTHLY' THEN 'Bulanan'
                                WHEN D.AMT_REF = 'YEARLY' THEN 'Tahunan'
                            END AS DASAR_HARGA,
                            D.LUAS, D.NJOP_PERCENT NJOP, D.KONDISI_TEKNIS_PERCENT KONDISI_TEKNIS, D.TOTAL_NET_VALUE SUB_TOTAL , A.BRANCH_ID KD_CABANG,A.BILLING_CONTRACT_NO,D.AMT_REF
                            FROM PROP_CONTRACT_BILLING_POST A, PROP_CONTRACT_BILLING_POST_D B, PROFIT_CENTER C, PROP_CONTRACT_CONDITION D, VW_CUSTOMERS E
                            WHERE A.BILLING_NO = B.BILLING_NO
                            AND A.CUSTOMER_CODE = E.MPLG_KODE
                            AND A.BRANCH_ID = E.KD_CABANG
                            AND A.PROFIT_CENTER_ID = C.ID
                            AND B.NO_REF2 = D.ID AND KD_CABANG = :a and " + where;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_PROPERTI_DETAIL> listDetil = connection.Query<PRANOTA_PROPERTI_DETAIL>(sql, new
            {
                a = kc,
                b = param
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_PROPERTI_DETAIL> GetPropertiDetaildua(string kc, string cn, string bn, string bill_id)
        {
            string where = "";
            string param = "";
            if (cn != null && cn != "")
            {
                where = " BILLING_CONTRACT_NO = :b";
                param = cn;
            }
            //if (bill_id != null && bill_id != "")
            //{
            //    where = " BILLING_ID = :b";
            //    param = bill_id;
            //}
            else
            {
                //where = " BILLING_NO = :b";
                where = " BILLING_CONTRACT_NO = :b";
                param = bn;
            }
            //string sql = @"SELECT CONTRACT_NO,CONDITION_TYPE,JANGKA_WAKTU,UNIT_PRICE,LUAS,NJOP_PERCENT,KONDISI_TEKNIS_PERCENT,TOTAL_NET_VALUE,MEMO,
            //                BILLING_ID,BILLING_PERIOD , decode(AMT_REF , 'ONETIME' ,'Sekali Bayar' , 'MONTHLY' ,'Bulanan' ,'YEARLY' ,'Tahunan' ,'-') AMT_REF , CURRENCY
            //                from V_ANJUNGAN_DETAIL_PROPERTY where KD_CABANG = :a and " + where;

            //string sql = @"SELECT BILLING_CONTRACT_NO, CONDITION_TYPE_NAME, OBJECT_ID, INSTALLMENT_AMOUNT, CURRENCY_CODE, BILLING_UOM,
            //                 BILLING_QTY, GL_ACOUNT_NO, BILLING_DUE_DATE, BILLING_FLAG_TAX1
            //                 FROM V_CONTRACT_BILLING_DETAIL where " + where ;

            string sql = @" SELECT BILLING_CONTRACT_NO,CONDITION_TYPE,JANGKA_WAKTU,UNIT_PRICE,LUAS,NJOP_PERCENT,KONDISI_TEKNIS_PERCENT,TOTAL_NET_VALUE,
                           BILLING_ID,BILLING_PERIOD , decode(AMT_REF , 'ONETIME' ,'Sekali Bayar' , 'MONTHLY' ,'Bulanan' ,'YEARLY' ,'Tahunan' ,'-') AMT_REF , CURRENCY_CODE 
                           FROM V_PRANOTA_POST_BILLING where BRANCH_ID = :a and " + where;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_PROPERTI_DETAIL> listDetil = connection.Query<PRANOTA_PROPERTI_DETAIL>(sql, new
            {
                a = kc,
                b = param
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_PROPERTY_MEMO> GetPropertiMemo(string cn, string bn)
        {
            string where = (cn != null ? " BILLING_NO =:a" : " CONTRACT_NO = :a");
            string param = (cn != null ? bn : cn);
            string sql = @"SELECT BILLING_NO,CONTRACT_NO,PERHITUNGAN,MEMO from V_ANJUNGAN_MEMO_PROPERTY where " + where;
            //string sql = @"SELECT CONTRACT_NO,CONDITION_TYPE PERHITUNGAN,MEMO from PROP_CONTRACT_MEMO where " + where;


            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_PROPERTY_MEMO> listDetil = connection.Query<PRANOTA_PROPERTY_MEMO>(sql, new
            {
                a = param,
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_PROPERTY_MEMO_DUA> GetPropertiMemodua(string cn)
        {
            string where = (cn != null ? " CONTRACT_NO =:a" : " BILLING_NO = :a");
            //string param = (cn != null ? cn : bn);
            string param = cn;
            //string sql = @"SELECT BILLING_NO,CONTRACT_NO,PERHITUNGAN,MEMO from V_ANJUNGAN_MEMO_PROPERTY where " + where ;
            string sql = @"SELECT CONTRACT_NO,CONDITION_TYPE,MEMO from PROP_CONTRACT_MEMO where " + where;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_PROPERTY_MEMO_DUA> listDetil = connection.Query<PRANOTA_PROPERTY_MEMO_DUA>(sql, new
            {
                a = param,
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public DataUser GetManagerCabang(string kc)
        {
            string sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                                where a.PROPERTY_ROLE = 30 and a.KD_CABANG = :b and a.APP_ID = 1  and a.USER_NAME is not null";

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            //IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            DataUser listDetil = connection.Query<DataUser>(sql, new
            {
                b = kc,
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }

        public DataUser GetManagerCabangPdf(string kc, string pc)
        {
            string sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                                where a.PROPERTY_ROLE = 30 and a.KD_CABANG = :b and a.APP_ID = 1  and a.USER_NAME is not null
                                and a.PROFIT_CENTER_ID = :c";

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            //IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            DataUser listDetil = connection.Query<DataUser>(sql, new
            {
                b = kc,
                c = pc,
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }

        public Results GetCabang(string kc)
        {
            string sql = @"select BRANCH_ID from BUSINESS_ENTITY where BE_ID = :a";
            Results result = new Results();
            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            int listDetil = connection.ExecuteScalar<int>(sql, new
            {
                a = kc,
            });
            connection.Close();
            if (listDetil > 0)
            {
                result.Msg = listDetil.ToString();
                result.Status = "S";
            }
            else
            {
                result.Msg = "error";
                result.Status = "E";

            }
            return result;
        }

        //public dynamic GetOtherHeader(string kc, string bn)
        //{
        //    string sql = @"select COSTUMER_ID,COSTUMER_MDM,PROFIT_CENTER,TERMINAL_NAME,INSTALLATION_ADDRESS,TOTAL,BRANCH_ID,BE_CITY,BE_ADDRESS,BE_NAME,ID,to_char(posting_date,'DD MONTH YYYY') posting_date from V_PRANOTA_OTHER_HEADER where ID = :a and BRANCH_ID =:b";

        //    //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
        //    IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    dynamic listDetil = connection.Query<dynamic>(sql, new
        //    {
        //        a = bn,
        //        b = kc
        //    }).FirstOrDefault();
        //    connection.Close();
        //    return listDetil;
        //}

        public AUP_TRANSVB_WEBAPPS GetOtherHeader(string kc, string bn)
        {
            string sql = "";
            string param = "";

            sql = @"select COSTUMER_ID, COSTUMER_MDM, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROF_CENTER, TERMINAL_NAME, INSTALLATION_ADDRESS, TOTAL, BRANCH_ID, BE_CITY, BE_ADDRESS, BE_NAME, ID, to_char(posting_date, 'DD MONTH YYYY') posting_date,
            LIVE_1PELINDO from V_PRANOTA_OTHER_HEADER where ID = :a and BRANCH_ID =:b ";
            param = bn;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_TRANSVB_WEBAPPS listDetil = connection.Query<AUP_TRANSVB_WEBAPPS>(sql, new
            {
                a = param,
                b = kc
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_OTHER_DETAIL> GetOtherDetail(string bn)
        {
            string sql = @"select SERVICE_NAME,to_char(START_DATE,'DD.MM.YYYY') START_DATE,to_char(END_DATE,'DD.MM.YYYY') END_DATE,REMARK,QUANTITY,UNIT,PRICE,AMOUNT,VB_ID,multiply_factor 
            from V_PRANOTA_OTHER_DETAIL where VB_ID = :a";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_OTHER_DETAIL> listDetil = connection.Query<PRANOTA_OTHER_DETAIL>(sql, new
            {
                a = bn,
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public AUP_TRANS_WE_INSTALLATION_WEBAPPS GetWEHeader(string kc, string bn)
        {
            string sql = "";
            string param = "";

            sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS, A.PROFIT_CENTER ||'-'|| C.TERMINAL_NAME AS PROFIT_CENTER,
                        B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, D.BE_ADDRESS, B.BE_ID, D.BE_NAME, A.PERIOD, A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN,
                        D.BE_CITY, to_char(A.POSTING_DATE,'DD MONTH YYYY') POSTING_DATE, 
                        NVL(A.SURCHARGE,0) SURCHARGE, NVL(A.GRAND_TOTAL,NVL(A.SURCHARGE,0) + A.AMOUNT) GRAND_TOTAL,
                        CASE WHEN A.POSTING_DATE IS NOT NULL
                        THEN
                        CASE WHEN A.POSTING_DATE >=
                        TO_DATE ('01/10/2021', 'dd/mm/yyyy')
                        THEN
                           1
                        ELSE
                           0
                        END
                        WHEN A.POSTING_DATE IS NULL
                        THEN
                        CASE
                        WHEN a.CREATED_DATE >=
                            TO_DATE ('01/10/2021', 'dd/mm/yyyy')
                        THEN
                           1
                        ELSE
                           0
                        END
                        END AS LIVE_1PELINDO
                        FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B, PROFIT_CENTER C, BUSINESS_ENTITY D
                        WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND B.PROFIT_CENTER_NEW = C.PROFIT_CENTER_ID AND 
                        A.BRANCH_ID = D.BRANCH_ID AND A.BRANCH_ID = :a and A.ID = :b ";
            param = bn;


            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_TRANS_WE_INSTALLATION_WEBAPPS listDetil = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(sql, new
            {
                a = kc,
                b = param
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_WATER> GetWEDetail(string kc, string cn, string bn, string bill_id)
        {
            string where = "";
            string param = "";
            if (cn != null && cn != "")
            {
                where = " a.ID = :b";
                param = cn;
            }
            //if (bill_id != null && bill_id != "")
            //{
            //    where = " BILLING_ID = :b";
            //    param = bill_id;
            //}
            else
            {
                //where = " BILLING_NO = :b";
                where = " a.ID = :b";
                param = bn;
            }

            //string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
            //                        REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
            //                        CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING where BRANCH_ID = :a 
            //                        and " + where;
            string sql = @"select a.id, a.INSTALLATION_CODE, a.meter_from, a.meter_to, a.quantity, a.price, b.biaya_admin, a.sub_total, b.currency
                            from PROP_SERVICES_TRANSACTION a, PROP_SERVICES_INSTALLATION b
                            where b.INSTALLATION_NUMBER = a.INSTALLATION_NUMBER and " + where;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_WATER> listDetil = connection.Query<PRANOTA_WATER>(sql, new
            {
                a = kc,
                b = param
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public AUP_TRANS_WE_INSTALLATION_WEBAPPS GetElectricityHeader(string kc, string bn)
        {
            string sql = "";
            string param = "";

            //sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS, A.PROFIT_CENTER ||'-'|| C.TERMINAL_NAME AS PROFIT_CENTER,
            //            B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, D.BE_ADDRESS, B.BE_ID, D.BE_NAME, A.PERIOD, D.BE_CITY,
            //            A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN, B.POWER_CAPACITY, A.MULTIPLY_FACTOR, E.PERCENTAGE, F.TARIFF, F.PRICE_TYPE, B.BIAYA_BEBAN,
            //            to_char(A.POSTING_DATE,'DD MONTH YYYY') POSTING_DATE,
            //            (SELECT DISTINCT PERCENTAGE
            //             FROM PROP_SERVICES_TRANSACTION_C
            //            WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'PPJU')
            //             PPJU,
            //          (SELECT DISTINCT PERCENTAGE
            //             FROM PROP_SERVICES_TRANSACTION_C
            //            WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'REDUKSI')
            //             REDUKSI
            //            FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B, PROFIT_CENTER C, BUSINESS_ENTITY D,
            //            PROP_SERVICES_TRANSACTION_C E, PROP_SERVICES_TRANSACTION_D F
            //            WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND B.PROFIT_CENTER_NEW = C.PROFIT_CENTER_ID AND 
            //            A.BRANCH_ID = D.BRANCH_ID AND E.SERVICES_TRANSACTION_ID = A.ID AND F.SERVICES_TRANSACTION_ID = A.ID AND A.BRANCH_ID = :a and A.ID = :b ";
            sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS, A.PROFIT_CENTER ||'-'|| C.TERMINAL_NAME AS PROFIT_CENTER,
                        B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, D.BE_ADDRESS, B.BE_ID, D.BE_NAME, A.PERIOD, D.BE_CITY,
                        A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN, B.POWER_CAPACITY, A.MULTIPLY_FACTOR, F.TARIFF, F.PRICE_TYPE, B.BIAYA_BEBAN,
                        to_char(A.POSTING_DATE,'DD MONTH YYYY') POSTING_DATE,
                        (SELECT DISTINCT PERCENTAGE
                         FROM PROP_SERVICES_TRANSACTION_C
                        WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'PPJU')
                         PPJU,
                      (SELECT DISTINCT PERCENTAGE
                         FROM PROP_SERVICES_TRANSACTION_C
                        WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'REDUKSI')
                         REDUKSI,
                        CASE WHEN A.POSTING_DATE IS NOT NULL
                        THEN
                        CASE WHEN A.POSTING_DATE >=
                        TO_DATE ('01/10/2021', 'dd/mm/yyyy')
                        THEN
                           1
                        ELSE
                           0
                        END
                        WHEN A.POSTING_DATE IS NULL
                        THEN
                        CASE
                        WHEN a.CREATED_DATE >=
                            TO_DATE ('01/10/2021', 'dd/mm/yyyy')
                        THEN
                           1
                        ELSE
                           0
                        END
                        END AS LIVE_1PELINDO
                        FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B, PROFIT_CENTER C, BUSINESS_ENTITY D,
                        PROP_SERVICES_TRANSACTION_D F
                        WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND B.PROFIT_CENTER_NEW = C.PROFIT_CENTER_ID AND 
                        A.BRANCH_ID = D.BRANCH_ID AND F.SERVICES_TRANSACTION_ID = A.ID AND A.BRANCH_ID = :a and A.ID = :b AND C.STATUS = 1 ";
            param = bn;


            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_TRANS_WE_INSTALLATION_WEBAPPS listDetil = connection.Query<AUP_TRANS_WE_INSTALLATION_WEBAPPS>(sql, new
            {
                a = kc,
                b = param
            }).FirstOrDefault();
            connection.Close();
            return listDetil;
        }

        public List<PRANOTA_ELECTRICITY> GetElectricityDetail(string kc, string cn, string bn, string bill_id)
        {
            string where = "";
            string param = "";
            if (cn != null && cn != "")
            {
                where = " a.ID = :b";
                param = cn;
            }
            //if (bill_id != null && bill_id != "")
            //{
            //    where = " BILLING_ID = :b";
            //    param = bill_id;
            //}
            else
            {
                //where = " BILLING_NO = :b";
                where = " a.ID = :b";
                param = bn;
            }

            //string sql = @"SELECT ID, INSTALLATION_TYPE, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, TERMINAL_NAME, CUSTOMER, CUSTOMER_NAME, PERIOD, 
            //                        REPORT_ON, METER_FROM, METER_TO, QUANTITY, UNIT, MULTIPLY_FACTOR, PRICE, AMOUNT, STATUS, INSTALLATION_NUMBER, INSTALLATION_CODE, BILLING_TYPE, 
            //                        CANCEL_STATUS, SAP_DOCUMENT_NUMBER, BRANCH_ID, SUB_TOTAL, SURCHARGE, POWER_CAPACITY, STATUS_DINAS FROM V_TRANS_WE_BILLING where BRANCH_ID = :a 
            //                        and " + where;
            //string sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS,
            //            B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, A.PERIOD, F.METER_FROM, F.METER_TO, A.QUANTITY,
            //            A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN, B.POWER_CAPACITY, A.MULTIPLY_FACTOR, E.PERCENTAGE, E.DESCRIPTION, F.TARIFF, F.PRICE_TYPE, B.BIAYA_BEBAN
            //            FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B,
            //            PROP_SERVICES_TRANSACTION_C E, PROP_SERVICES_TRANSACTION_D F
            //            WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND E.SERVICES_TRANSACTION_ID = A.ID AND F.SERVICES_TRANSACTION_ID = A.ID and " + where;
            //string sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS,
            //            B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, A.PERIOD, F.METER_FROM, F.METER_TO, A.QUANTITY,
            //            A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN, B.POWER_CAPACITY, A.MULTIPLY_FACTOR, E.PERCENTAGE, E.DESCRIPTION, F.TARIFF, F.PRICE_TYPE, B.BIAYA_BEBAN,
            //            (SELECT DISTINCT PERCENTAGE
            //             FROM PROP_SERVICES_TRANSACTION_C
            //            WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'PPJU')
            //             PPJU,
            //          (SELECT DISTINCT PERCENTAGE
            //             FROM PROP_SERVICES_TRANSACTION_C
            //            WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'REDUKSI')
            //             REDUKSI
            //            FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B,
            //            PROP_SERVICES_TRANSACTION_C E, PROP_SERVICES_TRANSACTION_D F
            //            WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND E.SERVICES_TRANSACTION_ID = A.ID AND F.SERVICES_TRANSACTION_ID = A.ID and " + where;

            string sql = @"SELECT A.ID, A.INSTALLATION_TYPE, A.INSTALLATION_CODE, B.INSTALLATION_ADDRESS,
                        B.CUSTOMER_ID || ' - ' || B.CUSTOMER_NAME AS CUSTOMER_NAME, A.BRANCH_ID, A.PERIOD, F.METER_FROM, F.METER_TO, F.USED QUANTITY,
                        A.AMOUNT, B.CURRENCY, A.PRICE, B.BIAYA_ADMIN, B.POWER_CAPACITY, A.MULTIPLY_FACTOR, F.TARIFF, F.PRICE_TYPE, B.BIAYA_BEBAN,
                        (SELECT DISTINCT PERCENTAGE
                         FROM PROP_SERVICES_TRANSACTION_C
                        WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'PPJU')
                         PPJU,
                      (SELECT DISTINCT PERCENTAGE
                         FROM PROP_SERVICES_TRANSACTION_C
                        WHERE SERVICES_TRANSACTION_ID = A.ID AND DESCRIPTION = 'REDUKSI')
                         REDUKSI
                        FROM PROP_SERVICES_TRANSACTION A, PROP_SERVICES_INSTALLATION B, PROP_SERVICES_TRANSACTION_D F
                        WHERE B.INSTALLATION_NUMBER = A.INSTALLATION_NUMBER AND F.SERVICES_TRANSACTION_ID = A.ID and " + where;

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            List<PRANOTA_ELECTRICITY> listDetil = connection.Query<PRANOTA_ELECTRICITY>(sql, new
            {
                a = kc,
                b = param
            }).ToList();
            connection.Close();
            return listDetil;
        }

        public Results GetCabang2(string kc)
        {
            string sql = @"select BRANCH_ID from PROFIT_CENTER where BRANCH_ID = :a";
            Results result = new Results();
            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection =  DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            int listDetil = connection.ExecuteScalar<int>(sql, new
            {
                a = kc,
            });
            connection.Close();
            if (listDetil > 0)
            {
                result.Msg = listDetil.ToString();
                result.Status = "S";
            }
            else
            {
                result.Msg = "error";
                result.Status = "E";

            }
            return result;
        }
    }
}
