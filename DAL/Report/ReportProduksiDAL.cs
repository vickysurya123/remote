﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransWater;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Helpers;
using Remote.Models.ReportProduksi;

namespace Remote.Controllers
{
    class ReportProduksiDAL
    {
        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- DEPRECATED ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }

        //------------------- NEW LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE STATUS = 1 " + v_be + " ORDER BY PROFIT_CENTER_ID ASC";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- NEW AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomernew(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                  "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = '" + KodeCabang + "'" + ")";

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }


        public DataTableReportProduksi GetDataFilternew(int draw, int start, int length, string period, string year, string be)
        {
            string x_year = string.IsNullOrEmpty(year) ? "x" : year;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string xtgl = "";
            if(period == "01" || period == "03" || period == "05" || period == "07" || period == "08" || period == "10" || period == "12")
            {
                xtgl = "31";
            }
            else
            {
                xtgl = "30";
                if (period == "02")
                {
                    if (Int32.Parse(year) % 4 == 0)
                    {
                        xtgl = "29";
                    }
                    else
                    {
                        xtgl = "28";
                    }
                }
                
            }
            string tgl = xtgl+"-" +period+"-"+year;

            int count = 0;
            int end = start + length;
            DataTableReportProduksi result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default_live");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_PRODUKSI> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            if(be != "")
            {
                where1 = " AND A.BRANCH_ID = :c ";
            }
            try
            {
                string sql = @"SELECT ROW_NUMBER() OVER (ORDER BY A.CONTRACT_NO DESC) NO, A.CONTRACT_NO , 'M2' SATUAN ,B.LUAS, B.OBJECT_ID, TO_CHAR(A.CONTRACT_START_DATE, 'DD-MM-YYYY') CONTRACT_START_DATE, 
                    TO_CHAR(A.CONTRACT_END_DATE, 'DD-MM-YYYY') CONTRACT_END_DATE, C.RO_NAME, A.PROFIT_CENTER, A.CUSTOMER_AR,
                    D.PROFIT_CENTER_NAME, D.CUSTOMER_NAME, D.BILLING_NO, D.BE_ID, c.RO_CODE, NVL(e.COA_PROD,'-') COA_PROD
                    FROM PROP_CONTRACT A 
                    JOIN (SELECT A.CONTRACT_NO,A.OBJECT_ID,
                    SUM(CAST(REPLACE(NVL(A.LUAS_TANAH,0), ',', '.') AS DECIMAL(29, 2))) + SUM(CAST(REPLACE(NVL(A.LUAS_BANGUNAN,0), ',', '.') AS DECIMAL(29, 2))) LUAS
                    FROM PROP_CONTRACT_OBJECT A
                    GROUP BY A.CONTRACT_NO, A.OBJECT_ID) B ON A.CONTRACT_NO = B.CONTRACT_NO 
                    JOIN RENTAL_OBJECT C ON C.RO_CODE = B.OBJECT_ID
                    LEFT JOIN (SELECT A.BILLING_CONTRACT_NO, A.BE_ID, A.CUSTOMER_NAME ,A.BILLING_NO, 
                    A.PROFIT_CENTER_NAME,ROW_NUMBER() OVER (PARTITION BY A.BILLING_CONTRACT_NO ORDER BY A.ID DESC ) NOID
                    FROM V_CONTRACT_LIST_BILLING A) D ON D.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT  A.CONTRACT_NO, A.COA_PROD ,ROW_NUMBER() OVER (PARTITION BY CONTRACT_NO ORDER BY A.ID ) NOID
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-') E ON E.CONTRACT_NO = A.CONTRACT_NO
                    WHERE A.STATUS_MIGRASI IS NULL AND A.STATUS = 1
                    AND A.CONTRACT_END_DATE >= TO_DATE('" + tgl + @"', 'DD-MM-YYYY') " + where1 + " ORDER BY A.CONTRACT_NO, B.OBJECT_ID ";

                fullSql = fullSql.Replace("sql", sql);
                
                listDataOperator = connection.Query<AUP_REPORT_PRODUKSI>(fullSql, new
                {
                    a = start,
                    b = end,
                    x_tgl = tgl,
                    c = be,
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    x_tgl = tgl,
                    c = be,
                });
            }
            catch (Exception ex)
            {
                var cc = ex.ToString();
                throw;
            }

            //merubah luas menjadi 0 apabila nomor contract sama
            var temp = "";
            var temp_ro = "";
            foreach(var list in listDataOperator.ToList())
            {
                if (temp == list.CONTRACT_NO && temp_ro == list.OBJECT_ID)
                    list.LUAS = "0";
                temp = list.CONTRACT_NO;
                temp_ro = list.OBJECT_ID;
            }

            result = new DataTableReportProduksi();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        // --------------------------- NEW EXCEL -------------------------------
        public IEnumerable<AUP_REPORT_PRODUKSI> ShowFilterTwonew(string period, string year, string be)
        {
            IEnumerable<AUP_REPORT_PRODUKSI> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string xtgl = "";
            if (period == "01" || period == "03" || period == "05" || period == "07" || period == "08" || period == "10" || period == "12")
            {
                xtgl = "31";
            }
            else
            {
                xtgl = "30";
                if (period == "02")
                {
                    if (Int32.Parse(year) % 4 == 0)
                    {
                        xtgl = "29";
                    }
                    else
                    {
                        xtgl = "28";
                    }
                }

            }
            string tgl = xtgl+"-" + period + "-" + year;

            string where1 = "";
            if (be != "")
            {
                where1 = " AND A.BRANCH_ID = :c ";
            }
            //if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
            //{
            try
                {
                string sql = @"SELECT ROW_NUMBER() OVER (ORDER BY A.CONTRACT_NO DESC) NO, A.CONTRACT_NO , 'M2' SATUAN ,B.LUAS, B.OBJECT_ID, TO_CHAR(A.CONTRACT_START_DATE, 'DD-MM-YYYY') CONTRACT_START_DATE, 
                    TO_CHAR(A.CONTRACT_END_DATE, 'DD-MM-YYYY') CONTRACT_END_DATE, C.RO_NAME, A.PROFIT_CENTER, A.CUSTOMER_AR,
                    D.PROFIT_CENTER_NAME, D.CUSTOMER_NAME, D.BILLING_NO, D.BE_ID, c.RO_CODE, NVL(e.COA_PROD,'-') COA_PROD
                    FROM PROP_CONTRACT A 
                    JOIN (SELECT A.CONTRACT_NO,A.OBJECT_ID,
                    SUM(CAST(REPLACE(NVL(A.LUAS_TANAH,0), ',', '.') AS DECIMAL(29, 2))) + SUM(CAST(REPLACE(NVL(A.LUAS_BANGUNAN,0), ',', '.') AS DECIMAL(29, 2))) LUAS
                    FROM PROP_CONTRACT_OBJECT A
                    GROUP BY A.CONTRACT_NO, A.OBJECT_ID) B ON A.CONTRACT_NO = B.CONTRACT_NO 
                    JOIN RENTAL_OBJECT C ON C.RO_CODE = B.OBJECT_ID
                    LEFT JOIN (SELECT A.BILLING_CONTRACT_NO, A.BE_ID, A.CUSTOMER_NAME ,A.BILLING_NO, 
                    A.PROFIT_CENTER_NAME,ROW_NUMBER() OVER (PARTITION BY A.BILLING_CONTRACT_NO ORDER BY A.ID DESC ) NOID
                    FROM V_CONTRACT_LIST_BILLING A) D ON D.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT  A.CONTRACT_NO, A.COA_PROD ,ROW_NUMBER() OVER (PARTITION BY CONTRACT_NO ORDER BY A.ID ) NOID
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-') E ON E.CONTRACT_NO = A.CONTRACT_NO
                    WHERE A.STATUS_MIGRASI IS NULL AND A.STATUS = 1
                    AND A.CONTRACT_END_DATE >= TO_DATE('" + tgl + @"', 'DD-MM-YYYY')" + where1 + " ORDER BY A.CONTRACT_NO, B.OBJECT_ID ";

                listData = connection.Query<AUP_REPORT_PRODUKSI>(sql, new
                    {
                        a = be,
                        x_tgl = tgl,
                        c = be,
                });
                }
                catch (Exception)
                {
                    listData = null;
                }
            //}


            connection.Close();
            connection.Dispose();
            return listData;

        }


    }
}
