﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.DAL;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.MasterRentalObject;
using Remote.Models.ReportROInternalUsed;

namespace Remote.DAL
{
    public class ReportROInternalUsedDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDZoneRip> GetDataUsageType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listData = null;

            try
            {
                string sql = "SELECT A.ID, A.REF_DESC FROM PROP_PARAMETER_REF_D A WHERE A.REF_CODE = 'USAGE_TYPE_RO'";
                listData = connection.Query<DDZoneRip>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDZoneRip> GetDataZONE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listData = null;

            try
            {
                string sql = "SELECT A.ID, A.REF_DESC FROM PROP_PARAMETER_REF_D A WHERE A.REF_CODE = 'ZONA_RIP'";
                listData = connection.Query<DDZoneRip>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        //--------------------------- DEPRECATED ------------------------------
        public DataTablesReportROInternalUsed GetFilterTwo(int draw, int start, int length, string be_id, string val_years, string usage_type, string val_zone, string val_status, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_status = string.IsNullOrEmpty(val_status) ? "x" : val_status;

            int count = 0;
            int end = start + length;
            DataTablesReportROInternalUsed result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RO_INTERNAL_USED> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                        "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status)";

                            fullSql = fullSql.Replace("sql", sql);
                            listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                        "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) "+
                                        "AND ((UPPER(A.RO_CODE) LIKE '%' ||:c|| '%') OR (A.RO_NAME LIKE '%' ||:c|| '%') OR (D.BE_NAME LIKE '%' ||:c|| '%') OR (TO_CHAR(C.CREATION_DATE, 'RRRR') LIKE '%' ||:c|| '%') OR (UPPER(C.REASON) LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) LIKE '%' ||:c|| '%') OR (UPPER(A.RO_ADDRESS) LIKE '%' ||:c|| '%') OR (B.AMOUNT LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) LIKE '%' ||:c|| '%') OR (A.RO_CERTIFICATE_NUMBER LIKE '%' ||:c|| '%'))";

                                fullSql = fullSql.Replace("sql", sql);

                                listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_status = x_val_status
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_status = x_val_status
                                });

                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                        "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) "+
                                        "AND A.BRANCH_ID =:d";

                            fullSql = fullSql.Replace("sql", sql);
                            listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status,
                                d = KodeCabang
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status,
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                        "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) " +
                                        "AND ((UPPER(A.RO_CODE) LIKE '%' ||:c|| '%') OR (A.RO_NAME LIKE '%' ||:c|| '%') OR (D.BE_NAME LIKE '%' ||:c|| '%') OR (TO_CHAR(C.CREATION_DATE, 'RRRR') LIKE '%' ||:c|| '%') OR (UPPER(C.REASON) LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) LIKE '%' ||:c|| '%') OR (UPPER(A.RO_ADDRESS) LIKE '%' ||:c|| '%') OR (B.AMOUNT LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) LIKE '%' ||:c|| '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) LIKE '%' ||:c|| '%') OR (A.RO_CERTIFICATE_NUMBER LIKE '%' ||:c|| '%')) " +
                                        "AND A.BRANCH_ID =:d";

                                fullSql = fullSql.Replace("sql", sql);

                                listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_status = x_val_status,
                                    d = KodeCabang
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_status = x_val_status,
                                    d = KodeCabang
                                });

                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                }
            }

            result = new DataTablesReportROInternalUsed();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //--------------------------- DEPREATED --------------------------------
        public IEnumerable<AUP_REPORT_RO_INTERNAL_USED> ShowFilterTwo(string be_id, string val_years, string usage_type, string val_zone, string val_status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO_INTERNAL_USED> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_status = string.IsNullOrEmpty(val_status) ? "x" : val_status;

            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                     "WHERE A.RO_NUMBER = B.RO_NUMBER(+) "+
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) "+
                                        "AND A.BE_ID = D.BE_ID(+) "+
                                        "AND C.STATUS = 'VACANT' "+
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') "+
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) "+
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status)";

                        listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_status = x_val_status
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                     "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) "+
                                        "AND A.BRANCH_ID =:a";

                        listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_status = x_val_status,
                            a = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        //--------------------------- NEW FILTER --------------------------------
        public DataTablesReportROInternalUsed GetFilterTwonew(int draw, int start, int length, string be_id, string val_years, string usage_type, string val_zone, string val_status, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_status = string.IsNullOrEmpty(val_status) ? "x" : val_status;

            int count = 0;
            int end = start + length;
            DataTablesReportROInternalUsed result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RO_INTERNAL_USED> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((UPPER(A.RO_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (A.RO_NAME LIKE '%' || '" + search.ToUpper() + "' || '%') OR (D.BE_NAME LIKE '%' || '" + search.ToUpper() + "' || '%') OR (TO_CHAR(C.CREATION_DATE, 'RRRR') LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(C.REASON) LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(A.RO_ADDRESS) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (B.AMOUNT LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (A.RO_CERTIFICATE_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
                        
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {                    
                        try
                        {
                            string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                        "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch;

                            fullSql = fullSql.Replace("sql", sql);
                            listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status,
                                d = KodeCabang
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_status = x_val_status,
                                d = KodeCabang
                            });
                        }
                        catch (Exception e)
                        {
                            string aaa = e.ToString();
                        }
                                        
                }
                        
            result = new DataTablesReportROInternalUsed();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //--------------------------- NEW EXCEL ---------------------------------
        public IEnumerable<AUP_REPORT_RO_INTERNAL_USED> ShowFilterTwonew(string be_id, string val_years, string usage_type, string val_zone, string val_status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO_INTERNAL_USED> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_status = string.IsNullOrEmpty(val_status) ? "x" : val_status;
            
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_status.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT A.RO_CODE, A.RO_NAME, D.BE_NAME, TO_CHAR(C.VALID_FROM, 'RRRR') CREATION_DATE, C.REASON, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) OBJ, A.RO_ADDRESS, B.AMOUNT, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.FUNCTION_ID ) USAGES, (SELECT DISTINCT E.REF_DESC FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) ZONE_RIP, A.RO_CERTIFICATE_NUMBER FROM RENTAL_OBJECT A, RENTAL_OBJECT_DETAIL B, PROP_RO_OCCUPANCY C, BUSINESS_ENTITY D " +
                                     "WHERE A.RO_NUMBER = B.RO_NUMBER(+) " +
                                        "AND A.RO_NUMBER = C.RO_NUMBER(+) " +
                                        "AND A.BE_ID = D.BE_ID(+) " +
                                        "AND C.STATUS = 'VACANT' " +
                                        "AND (C.REASON LIKE '%Pemakaian Internal%' OR C.REASON LIKE '%No Reason%') " +
                                        "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) " +
                                        "AND TO_CHAR(C.VALID_FROM, 'RRRR') = DECODE(:x_val_years, 'x', TO_CHAR(C.VALID_FROM, 'RRRR'),:x_val_years) " +
                                        "AND (SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID) = DECODE(:x_usage_type,'x',(SELECT DISTINCT A.USAGE_TYPE_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.USAGE_TYPE_ID),:x_usage_type) " +
                                        "AND (SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID) = DECODE(:x_val_zone,'x',(SELECT DISTINCT A.ZONE_RIP_ID FROM PROP_PARAMETER_REF_D E WHERE E.ID = A.ZONE_RIP_ID),:x_val_zone) " +
                                        "AND C.REASON = DECODE(:x_val_status,'x',C.REASON ,:x_val_status) " +
                                        "AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";

                        listData = connection.Query<AUP_REPORT_RO_INTERNAL_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_status = x_val_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            
            connection.Close();
            return listData;

        }

    }
}