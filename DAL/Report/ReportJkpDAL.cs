﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransWater;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Helpers;
using Remote.Models.ReportJkp;
using Remote.Models.GeneralResult;

namespace Remote.Controllers
{
    class ReportJkpDAL
    {
        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- DEPRECATED ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }

        //------------------- NEW LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE STATUS = 1 " + v_be + " ORDER BY PROFIT_CENTER_ID ASC";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- NEW AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomernew(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                  "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = '" + KodeCabang + "'" + ")";

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }


        public DataTableReportJkp GetDataFilternew(int draw, int start, int length, string period, string year, string posting_date_from, string posting_date_to, string KodeCabang, string branchID, string profitCenter)
        {
            string x_year = string.IsNullOrEmpty(year) ? "x" : year;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string whereprofit = string.IsNullOrEmpty(profitCenter) ? "" : "A.PROFIT_CENTER = " + profitCenter;
            string wherebusiness = string.IsNullOrEmpty(branchID) ? "" : "A.BRANCH_ID = " + branchID;
            string where = string.IsNullOrEmpty(whereprofit) ? (string.IsNullOrEmpty(wherebusiness) ? "" : " WHERE " + wherebusiness) : (string.IsNullOrEmpty(wherebusiness) ? " WHERE " + whereprofit : " WHERE " + wherebusiness + " AND " + whereprofit);
            string xtgl = "";
            if (period == "01" || period == "03" || period == "05" || period == "07" || period == "08" || period == "10" || period == "12")
            {
                xtgl = "31";
            }
            else
            {
                xtgl = "30";
                if (period == "02")
                {
                    if (Int32.Parse(year) % 4 == 0)
                    {
                        xtgl = "29";
                    }
                    else
                    {
                        xtgl = "28";
                    }
                }

            }
            string tgl = xtgl + "/" + period + "/" + year;
            string tgl_jkp = "01/" + period + "/" + year;
            string tgl_accr = "01/" + "01" + "/" + year;
            string periodes = year + period;
            string periodeBefore = year + "01";

            int count = 0;
            int end = start + length;
            DataTableReportJkp result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default_live");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_JKP> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            try
            {
                string sql = @"SELECT ROW_NUMBER() OVER (ORDER BY A.CONTRACT_NO) NO, A.CONTRACT_NO,TO_CHAR(A.CONTRACT_START_DATE, 'DD-MM-YYYY') CONTRACT_START_DATE, TO_CHAR(A.CONTRACT_END_DATE, 'DD-MM-YYYY') CONTRACT_END_DATE, A.VAL_CONTRACT, A.VAL_PERMONTH, A.TERM_IN_MONTHS,
                    CASE WHEN A.PREV_PERIOD > 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 THEN A.PREV_PERIOD - A.VAL_PERMONTH
                    WHEN A.PREV_PERIOD > 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 THEN A.PREV_PERIOD + A.VAL_PERMONTH
                    ELSE A.PREV_PERIOD END PREV_PERIOD,
                    CASE WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK > 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH
                    WHEN A.NEXT_YEAR = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK > 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH
                    WHEN A.NEXT_YEAR = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH 
                    ELSE A.ACC_REVENUE END ACC_REVENUE, 
                    A.JANGKA_PNDK, A.NEXT_YEAR, 
                    CASE WHEN A.END_CONTRACT > 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 THEN A.END_CONTRACT - A.VAL_PERMONTH
                    WHEN A.END_CONTRACT > 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 THEN A.END_CONTRACT + A.VAL_PERMONTH
                    ELSE A.END_CONTRACT END END_CONTRACT,  A.NILAI_SELISIH,A.CARI_SELISIH,A.PROFIT_CENTER,
                    NVL(B.PAID_PERIOD,0) PAID_PERIOD, NVL(C.PAID_PDK,0) PAID_PDK
                    ,A.PROFIT_CENTER PROFIT_CENTER_NAMA, A.BUSINESS_PARTNER_NAME, E.COA_PROD,A.PROYEKSI_CONTRACT, A.BRANCH_ID
                    FROM (
                    SELECT A.CONTRACT_NO,A.CONTRACT_START_DATE, A.CONTRACT_END_DATE, A.VAL_CONTRACT,  A.VAL_PERMONTH, A.TERM_IN_MONTHS,A.PREV_PERIOD,
                    A.ACC_REVENUE, A.JANGKA_PNDK, A.NEXT_YEAR, A.END_CONTRACT,
                    CASE WHEN A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = -1 OR 
                    A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = 1 OR
                    A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = 0 
                    THEN 0 ELSE 1 END CARI_SELISIH, A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) NILAI_SELISIH,
                    A.BUSINESS_PARTNER_NAME, A.PROFIT_CENTER, A.PROYEKSI_CONTRACT, A.BRANCH_ID
                    FROM (
                    SELECT A.CONTRACT_NO,A.CONTRACT_START_DATE, A.CONTRACT_END_DATE, ROUND(A.TOTAL) VAL_CONTRACT,
                    ROUND(A.TOTAL/NVL(CEIL(A.SELISIH),1),0) VAL_PERMONTH,CASE WHEN BULAN_START <> BULAN_END THEN CEIL(A.SELISIH ) 
                    WHEN BULAN_START <> BULAN_END AND YEAR_START <> YEAR_END THEN ROUND(A.SELISIH) ELSE CEIL(A.SELISIH) END AS TERM_IN_MONTHS, 
                    CASE WHEN A.YEAR_START = YEAR_SKR THEN 0 
                    WHEN BULAN_START = BULAN_END AND A.YEAR_END <> YEAR_SKR 
                    THEN ROUND(A.TOTAL/CEIL(A.SELISIH) * (CEIL(A.SELISIH_START) - 1 - A.BULAN_ACCREV),0)
                    ELSE ROUND(A.TOTAL/CEIL(A.SELISIH) * (CEIL(A.SELISIH_START) - A.BULAN_ACCREV),0) END PREV_PERIOD,
                    CASE WHEN CEIL(A.SELISIH) = 1 AND A.YEAR_END > YEAR_SKR THEN 0 
                    WHEN BULAN_START = BULAN_END AND A.YEAR_START = YEAR_SKR AND A.YEAR_END <> A.YEAR_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV - 1) * A.TOTAL/CEIL(A.SELISIH),0)
                    WHEN BULAN_START <> BULAN_END AND A.YEAR_START = YEAR_SKR AND A.BULAN_START = A.BULAN_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV - 1) * A.TOTAL/CEIL(A.SELISIH),0)
                    WHEN BULAN_START <> BULAN_END AND A.YEAR_START = YEAR_SKR AND A.YEAR_END = A.YEAR_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV) * A.TOTAL/CEIL(A.SELISIH),0)
                    ELSE ROUND(A.BULAN_ACCREV * A.TOTAL/CEIL(A.SELISIH),0) END ACC_REVENUE,
                    ROUND(A.BULAN_JKP * A.TOTAL/CEIL(A.SELISIH),0) JANGKA_PNDK, 
                    CASE WHEN A.BULAN_NEXTYEAR < 0 THEN 0 WHEN A.BULAN_NEXTYEAR >= 0 AND A.BULAN_NEXTYEAR <= 12 
                    THEN ROUND(A.BULAN_NEXTYEAR * A.TOTAL/CEIL(A.SELISIH),0) ELSE ROUND(12 * A.TOTAL/CEIL(A.SELISIH),0) END NEXT_YEAR, 
                    CASE WHEN A.BULAN_NEXTYEAR > 13 THEN ROUND((A.BULAN_NEXTYEAR - 12) * A.TOTAL/CEIL(A.SELISIH),0) ELSE 0 END END_CONTRACT,
                    CASE WHEN A.YEAR_END = A.YEAR_SKR THEN ROUND(A.TOTAL/CEIL(A.SELISIH) * (12 - A.BULAN_END),0) 
                    ELSE 0 END PROYEKSI_CONTRACT,CEIL(A.SELISIH) SELISIH , A.BULAN_NEXTYEAR , CEIL(A.SELISIH_START) SELISIH_START, BULAN_ACCREV,
                    A.PROFIT_CENTER,A.BUSINESS_PARTNER_NAME, A.BRANCH_ID
                    FROM ( SELECT TO_CHAR(A.CONTRACT_START_DATE, 'MM') BULAN_START, TO_CHAR(A.CONTRACT_END_DATE, 'MM') BULAN_END,
                    TO_CHAR(A.CONTRACT_START_DATE, 'YYYY') YEAR_START, TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') YEAR_END,
                    TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'YYYY') YEAR_SKR, TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'MM') BULAN_SKR,
                    MONTHS_BETWEEN(A.CONTRACT_END_DATE,A.CONTRACT_START_DATE) SELISIH,MONTHS_BETWEEN(A.CONTRACT_END_DATE,A.CONTRACT_START_DATE) ASLI,
                     A.CONTRACT_NO, A.CONTRACT_END_DATE,A.CONTRACT_START_DATE,
                    MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE A.CONTRACT_END_DATE END,CONTRACT_START_DATE) SELISIH_START,
                    CEIL(MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE A.CONTRACT_END_DATE END,CASE WHEN TO_CHAR(A.CONTRACT_START_DATE, 'YYYY') = TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'YYYY') 
                    THEN A.CONTRACT_START_DATE ELSE TO_DATE(:tglAccr,'DD/MM/YYYY') END )) BULAN_ACCREV,
                    CEIL(MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') AND TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') = :year 
                    THEN A.CONTRACT_END_DATE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') AND TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') > :year
                    THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE TO_DATE(:x_tgl,'DD/MM/YYYY') END,TO_DATE(:x_tgl,'DD/MM/YYYY'))) BULAN_JKP,
                    CASE WHEN TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') = :year THEN 0 ELSE CEIL(MONTHS_BETWEEN(
                    CONTRACT_END_DATE,TO_DATE(:x_tgl,'DD/MM/YYYY'))) END BULAN_NEXTYEAR,
                    B.TOTAL, A.PROFIT_CENTER, A.BUSINESS_PARTNER_NAME, A.BRANCH_ID
                    FROM PROP_CONTRACT A
                    JOIN (SELECT SUM(A.TOTAL_NET_VALUE) TOTAL, A.CONTRACT_NO
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-'
                    GROUP BY A.CONTRACT_NO) B ON A.CONTRACT_NO = B.CONTRACT_NO 
                    WHERE A.STATUS_MIGRASI IS NULL AND A.STATUS = 1
                    ) A
                    WHERE A.YEAR_END >= :year AND A.CONTRACT_START_DATE <= TO_DATE(:x_tgl,'DD/MM/YYYY')  AND A.SELISIH <> 0)
                    A) A
                    LEFT JOIN (SELECT B.BILLING_CONTRACT_NO, SUM(B.INSTALLMENT_AMOUNT) PAID_PERIOD
                    FROM ITGR_HEADER A
                    JOIN PROP_CONTRACT_BILLING_POST B ON A.REF_DOC_NO = B.BILLING_NO
                    WHERE A.TGL_LUNAS IS NOT NULL AND B.BILLING_PERIOD < :periode_before
                    GROUP BY B.BILLING_CONTRACT_NO) B ON B.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT B.BILLING_CONTRACT_NO, SUM(B.INSTALLMENT_AMOUNT) PAID_PDK
                    FROM ITGR_HEADER A
                    JOIN PROP_CONTRACT_BILLING_POST B ON A.REF_DOC_NO = B.BILLING_NO
                    WHERE A.TGL_LUNAS IS NOT NULL AND B.BILLING_PERIOD BETWEEN :periode_before AND :periode_after
                    GROUP BY B.BILLING_CONTRACT_NO) C ON C.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT  A.CONTRACT_NO, A.COA_PROD ,ROW_NUMBER() OVER (PARTITION BY CONTRACT_NO ORDER BY A.ID ) NOID
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-') E ON E.CONTRACT_NO = A.CONTRACT_NO AND E.NOID = 1
                 " + where;

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_REPORT_JKP>(fullSql, new
                {
                    a = start,
                    b = end,
                    x_tgl = tgl,
                    tglAccr = tgl_accr,
                    year = x_year,
                    tglJkp = tgl_jkp,
                    periode_after = periodes,
                    periode_before = periodeBefore,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    x_tgl = tgl,
                    tglAccr = tgl_accr,
                    year = x_year,
                    tglJkp = tgl_jkp,
                    periode_after = periodes,
                    periode_before = periodeBefore,
                    d = KodeCabang
                });
            }
            catch (Exception ex)
            {
                var cc = ex.ToString();
                throw;
            }


            result = new DataTableReportJkp();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        // --------------------------- NEW EXCEL -------------------------------
        public IEnumerable<AUP_REPORT_JKP> ShowFilterTwonew(string period, string year, string KodeCabang, string branchID, string profitCenter)
        {
            Results result = new Results();
            IEnumerable<AUP_REPORT_JKP> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default_live");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_year = string.IsNullOrEmpty(year) ? "x" : year;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string whereprofit = string.IsNullOrEmpty(profitCenter) ? "" : "A.PROFIT_CENTER = " + profitCenter;
            string wherebusiness = string.IsNullOrEmpty(branchID) ? "" : "A.BRANCH_ID = " + branchID;
            string where = string.IsNullOrEmpty(whereprofit) ? (string.IsNullOrEmpty(wherebusiness) ? "" : " WHERE " + wherebusiness) : (string.IsNullOrEmpty(wherebusiness) ? " WHERE " + whereprofit : " WHERE " + wherebusiness + " AND " + whereprofit);
            string xtgl = "";
            if (period == "01" || period == "03" || period == "05" || period == "07" || period == "08" || period == "10" || period == "12")
            {
                xtgl = "31";
            }
            else
            {
                xtgl = "30";
                if (period == "02")
                {
                    if (Int32.Parse(year) % 4 == 0)
                    {
                        xtgl = "29";
                    }
                    else
                    {
                        xtgl = "28";
                    }
                }

            }
            string tgl = xtgl + "/" + period + "/" + year;
            string tgl_jkp = "01/" + period + "/" + year;
            string tgl_accr = "01/" + "01" + "/" + year;
            string periodes = year + period;
            string periodeBefore = year + "01";

            try
            {
                string sql = @"SELECT ROW_NUMBER() OVER (ORDER BY A.CONTRACT_NO) NO, A.CONTRACT_NO,TO_CHAR(A.CONTRACT_START_DATE, 'DD-MM-YYYY') CONTRACT_START_DATE, TO_CHAR(A.CONTRACT_END_DATE, 'DD-MM-YYYY') CONTRACT_END_DATE, A.VAL_CONTRACT, A.VAL_PERMONTH, A.TERM_IN_MONTHS,
                    CASE WHEN A.PREV_PERIOD > 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 THEN A.PREV_PERIOD - A.VAL_PERMONTH
                    WHEN A.PREV_PERIOD > 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 THEN A.PREV_PERIOD + A.VAL_PERMONTH
                    ELSE A.PREV_PERIOD END PREV_PERIOD,
                    CASE WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK > 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH
                    WHEN A.NEXT_YEAR = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE - A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK > 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH 
                    WHEN A.NEXT_YEAR > 0 AND A.JANGKA_PNDK = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH
                    WHEN A.NEXT_YEAR = 0 AND A.END_CONTRACT = 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 
                    THEN A.ACC_REVENUE + A.VAL_PERMONTH 
                    ELSE A.ACC_REVENUE END ACC_REVENUE, 
                    A.JANGKA_PNDK, A.NEXT_YEAR, 
                    CASE WHEN A.END_CONTRACT > 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH < 0 AND A.CARI_SELISIH = 1 THEN A.END_CONTRACT - A.VAL_PERMONTH
                    WHEN A.END_CONTRACT > 0 AND A.PREV_PERIOD = 0 AND A.NILAI_SELISIH > 0 AND A.CARI_SELISIH = 1 THEN A.END_CONTRACT + A.VAL_PERMONTH
                    ELSE A.END_CONTRACT END END_CONTRACT,  A.NILAI_SELISIH,A.CARI_SELISIH,A.PROFIT_CENTER,
                    NVL(B.PAID_PERIOD,0) PAID_PERIOD, NVL(C.PAID_PDK,0) PAID_PDK
                    ,A.PROFIT_CENTER PROFIT_CENTER_NAMA, A.BUSINESS_PARTNER_NAME, E.COA_PROD,A.PROYEKSI_CONTRACT
                    FROM (
                    SELECT A.CONTRACT_NO,A.CONTRACT_START_DATE, A.CONTRACT_END_DATE, A.VAL_CONTRACT,  A.VAL_PERMONTH, A.TERM_IN_MONTHS,A.PREV_PERIOD,
                    A.ACC_REVENUE, A.JANGKA_PNDK, A.NEXT_YEAR, A.END_CONTRACT,
                    CASE WHEN A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = -1 OR 
                    A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = 1 OR
                    A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) = 0 
                    THEN 0 ELSE 1 END CARI_SELISIH, A.VAL_CONTRACT - (A.PREV_PERIOD + A.ACC_REVENUE + A.JANGKA_PNDK + A.NEXT_YEAR + A.END_CONTRACT) NILAI_SELISIH,
                    A.BUSINESS_PARTNER_NAME, A.PROFIT_CENTER, A.PROYEKSI_CONTRACT, A.BRANCH_ID
                    FROM (
                    SELECT A.CONTRACT_NO,A.CONTRACT_START_DATE, A.CONTRACT_END_DATE, ROUND(A.TOTAL) VAL_CONTRACT,
                    ROUND(A.TOTAL/NVL(CEIL(A.SELISIH),1),0) VAL_PERMONTH,CASE WHEN BULAN_START <> BULAN_END THEN CEIL(A.SELISIH ) 
                    WHEN BULAN_START <> BULAN_END AND YEAR_START <> YEAR_END THEN ROUND(A.SELISIH) ELSE CEIL(A.SELISIH) END AS TERM_IN_MONTHS, 
                    CASE WHEN A.YEAR_START = YEAR_SKR THEN 0 
                    WHEN BULAN_START = BULAN_END AND A.YEAR_END <> YEAR_SKR 
                    THEN ROUND(A.TOTAL/CEIL(A.SELISIH) * (CEIL(A.SELISIH_START) - 1 - A.BULAN_ACCREV),0)
                    ELSE ROUND(A.TOTAL/CEIL(A.SELISIH) * (CEIL(A.SELISIH_START) - A.BULAN_ACCREV),0) END PREV_PERIOD,
                    CASE WHEN CEIL(A.SELISIH) = 1 AND A.YEAR_END > YEAR_SKR THEN 0 
                    WHEN BULAN_START = BULAN_END AND A.YEAR_START = YEAR_SKR AND A.YEAR_END <> A.YEAR_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV - 1) * A.TOTAL/CEIL(A.SELISIH),0)
                    WHEN BULAN_START <> BULAN_END AND A.YEAR_START = YEAR_SKR AND A.BULAN_START = A.BULAN_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV - 1) * A.TOTAL/CEIL(A.SELISIH),0)
                    WHEN BULAN_START <> BULAN_END AND A.YEAR_START = YEAR_SKR AND A.YEAR_END = A.YEAR_SKR AND CEIL(A.SELISIH) = 1
                    THEN ROUND((A.BULAN_ACCREV) * A.TOTAL/CEIL(A.SELISIH),0)
                    ELSE ROUND(A.BULAN_ACCREV * A.TOTAL/CEIL(A.SELISIH),0) END ACC_REVENUE,
                    ROUND(A.BULAN_JKP * A.TOTAL/CEIL(A.SELISIH),0) JANGKA_PNDK, 
                    CASE WHEN A.BULAN_NEXTYEAR < 0 THEN 0 WHEN A.BULAN_NEXTYEAR >= 0 AND A.BULAN_NEXTYEAR <= 12 
                    THEN ROUND(A.BULAN_NEXTYEAR * A.TOTAL/CEIL(A.SELISIH),0) ELSE ROUND(12 * A.TOTAL/CEIL(A.SELISIH),0) END NEXT_YEAR, 
                    CASE WHEN A.BULAN_NEXTYEAR > 13 THEN ROUND((A.BULAN_NEXTYEAR - 12) * A.TOTAL/CEIL(A.SELISIH),0) ELSE 0 END END_CONTRACT,
                    CASE WHEN A.YEAR_END = A.YEAR_SKR THEN ROUND(A.TOTAL/CEIL(A.SELISIH) * (12 - A.BULAN_END),0) 
                    ELSE 0 END PROYEKSI_CONTRACT,CEIL(A.SELISIH) SELISIH , A.BULAN_NEXTYEAR , CEIL(A.SELISIH_START) SELISIH_START, BULAN_ACCREV,
                    A.PROFIT_CENTER,A.BUSINESS_PARTNER_NAME, A.BRANCH_ID
                    FROM ( SELECT TO_CHAR(A.CONTRACT_START_DATE, 'MM') BULAN_START, TO_CHAR(A.CONTRACT_END_DATE, 'MM') BULAN_END,
                    TO_CHAR(A.CONTRACT_START_DATE, 'YYYY') YEAR_START, TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') YEAR_END,
                    TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'YYYY') YEAR_SKR, TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'MM') BULAN_SKR,
                    MONTHS_BETWEEN(A.CONTRACT_END_DATE,A.CONTRACT_START_DATE) SELISIH,MONTHS_BETWEEN(A.CONTRACT_END_DATE,A.CONTRACT_START_DATE) ASLI,
                     A.CONTRACT_NO, A.CONTRACT_END_DATE,A.CONTRACT_START_DATE,
                    MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE A.CONTRACT_END_DATE END,CONTRACT_START_DATE) SELISIH_START,
                    CEIL(MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE A.CONTRACT_END_DATE END,CASE WHEN TO_CHAR(A.CONTRACT_START_DATE, 'YYYY') = TO_CHAR(TO_DATE(:x_tgl,'DD/MM/YYYY'), 'YYYY') 
                    THEN A.CONTRACT_START_DATE ELSE TO_DATE(:tglAccr,'DD/MM/YYYY') END )) BULAN_ACCREV,
                    CEIL(MONTHS_BETWEEN(
                    CASE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') AND TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') = :year 
                    THEN A.CONTRACT_END_DATE WHEN A.CONTRACT_END_DATE > TO_DATE(:x_tgl,'DD/MM/YYYY') AND TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') > :year
                    THEN TO_DATE(:x_tgl,'DD/MM/YYYY') 
                    ELSE TO_DATE(:x_tgl,'DD/MM/YYYY') END,TO_DATE(:x_tgl,'DD/MM/YYYY'))) BULAN_JKP,
                    CASE WHEN TO_CHAR(A.CONTRACT_END_DATE, 'YYYY') = :year THEN 0 ELSE CEIL(MONTHS_BETWEEN(
                    CONTRACT_END_DATE,TO_DATE(:x_tgl,'DD/MM/YYYY'))) END BULAN_NEXTYEAR,
                    B.TOTAL, A.PROFIT_CENTER, A.BUSINESS_PARTNER_NAME, A.BRANCH_ID
                    FROM PROP_CONTRACT A
                    JOIN (SELECT SUM(A.TOTAL_NET_VALUE) TOTAL, A.CONTRACT_NO
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-'
                    GROUP BY A.CONTRACT_NO) B ON A.CONTRACT_NO = B.CONTRACT_NO 
                    WHERE A.STATUS_MIGRASI IS NULL AND A.STATUS = 1
                    ) A
                    WHERE A.YEAR_END >= :year AND A.CONTRACT_START_DATE <= TO_DATE(:x_tgl,'DD/MM/YYYY')  AND A.SELISIH <> 0)
                    A) A
                    LEFT JOIN (SELECT B.BILLING_CONTRACT_NO, SUM(B.INSTALLMENT_AMOUNT) PAID_PERIOD
                    FROM ITGR_HEADER A
                    JOIN PROP_CONTRACT_BILLING_POST B ON A.REF_DOC_NO = B.BILLING_NO
                    WHERE A.TGL_LUNAS IS NOT NULL AND B.BILLING_PERIOD < :periode_before
                    GROUP BY B.BILLING_CONTRACT_NO) B ON B.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT B.BILLING_CONTRACT_NO, SUM(B.INSTALLMENT_AMOUNT) PAID_PDK
                    FROM ITGR_HEADER A
                    JOIN PROP_CONTRACT_BILLING_POST B ON A.REF_DOC_NO = B.BILLING_NO
                    WHERE A.TGL_LUNAS IS NOT NULL AND B.BILLING_PERIOD BETWEEN :periode_before AND :periode_after
                    GROUP BY B.BILLING_CONTRACT_NO) C ON C.BILLING_CONTRACT_NO = A.CONTRACT_NO
                    LEFT JOIN (SELECT  A.CONTRACT_NO, A.COA_PROD ,ROW_NUMBER() OVER (PARTITION BY CONTRACT_NO ORDER BY A.ID ) NOID
                    FROM PROP_CONTRACT_CONDITION A
                    WHERE A.COA_PROD IS NOT NULL AND A.COA_PROD <> '-') E ON E.CONTRACT_NO = A.CONTRACT_NO AND E.NOID = 1
                     " + where;

                listData = connection.Query<AUP_REPORT_JKP>(sql, new
                {
                    x_tgl = tgl,
                    tglAccr = tgl_accr,
                    year = x_year,
                    tglJkp = tgl_jkp,
                    periode_after = periodes,
                    periode_before = periodeBefore,
                    d = KodeCabang
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            connection.Close();
            connection.Dispose();
            return listData;

        }
    }
}
