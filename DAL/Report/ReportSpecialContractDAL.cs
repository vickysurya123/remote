﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ReportSpecialContract;


namespace Remote.DAL
{
    public class ReportSpecialContractDAL
    {

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where be_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string BUSINESS_PARTNER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER_NAME name FROM PROP_CONTRACT " +
                        "WHERE DIRECT_CONTRACT = '1' AND ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + BUSINESS_PARTNER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + BUSINESS_PARTNER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER name, BRANCH_ID FROM PROP_CONTRACT " +
                        "WHERE DIRECT_CONTRACT = '1' AND ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + BUSINESS_PARTNER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + BUSINESS_PARTNER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        //--------------------------------- FILTER deprecated ----------------------
        public DataTablesReportSpecialContract ShowFilter(int draw, int start, int length, string be_id, string profit_center, string business_partner, string contract_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_business_partner = string.IsNullOrEmpty(business_partner) ? "x" : business_partner;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTablesReportSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT " +
                            "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, BRANCH_ID, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT WHERE BRANCH_ID = :d " +
                            "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }

                }
            }

            result = new DataTablesReportSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> GenerateExcelReportSpecialContract(string be_id, string profit_center, string business_partner, string contract_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_business_partner = string.IsNullOrEmpty(business_partner) ? "x" : business_partner;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;


            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT " +
                            "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, BRANCH_ID, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT WHERE BRANCH_ID = :d " +
                            "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        //--------------------- new filter ---------------------------
        public DataTablesReportSpecialContract ShowFilternew(int draw, int start, int length, string be_id, string profit_center, string business_partner, string contract_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_business_partner = string.IsNullOrEmpty(business_partner) ? "x" : business_partner;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTablesReportSpecialContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT " +
                            "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) "+v_be;

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                        
            result = new DataTablesReportSpecialContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //------------------------ NEW EXCEL ------------------------------
        public IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> GenerateExcelReportSpecialContractnew(string be_id, string profit_center, string business_partner, string contract_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_business_partner = string.IsNullOrEmpty(business_partner) ? "x" : business_partner;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_business_partner.Length >= 0 && x_contract_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, " +
                            "CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, OBJECT_ID, OBJECT_NAME, INDUSTRY, BRANCH_ID, " +
                            "LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, BE_ID, CREATION_DATE, PROFIT_CENTER_ID, BUSINESS_PARTNER " +
                            "FROM V_REPORT_SPECIAL_CONTRACT WHERE " +
                            "BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                            "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                            "AND BUSINESS_PARTNER= DECODE(:x_business_partner, 'x', BUSINESS_PARTNER,:x_business_partner) " +
                            "AND CONTRACT_STATUS = DECODE(:x_contract_status, 'x', CONTRACT_STATUS,:x_contract_status) " +
                            "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " + v_be;

                        listData = connection.Query<AUP_TRANS_SPECIAL_CONTRACT_WEBAPPS>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_business_partner = x_business_partner,
                            x_contract_no = x_contract_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }          

            connection.Close();
            return listData;

        }

    }
}