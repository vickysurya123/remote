﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models.ReportingRuleBilling;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ReportingRuleBillingDAL
    {
        //------------------------- DATA TABLE REPORTING RULE ------------------------------//
        public DataTableReportingRuleBilling GetDataReportingRule(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            DataTableReportingRuleBilling result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_REPORTING_RULE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                /*
                string sql = "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                         "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                         "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                         "c.PHONE_1, c.FAX_1, c.PHONE_2, c.FAX_2, c.EMAIL " +
                         "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID ORDER BY b.ID DESC";
                */
                string sql = "SELECT ID, CONTRACT_NO, RR_STATUS, to_char(REPORT_FROM, 'DD/MM/YYYY') REPORT_FROM, to_char(REPORT_TO,'DD/MM/YYYY')REPORT_TO, " +
                             "to_char(REPORT_ON,'DD/MM/YYYY')REPORT_ON, ZERO_SALES, QUANTITY_SALES, UNIT, NET_SALES, RR_TOTAL, BILLING_NO " +
                             "FROM V_CONTRACT_SR_D";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_REPORTING_RULE>(fullSql, new { a = start, b = end });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
            }
            else
            {
                //search filter data
                if (search.Length > 2)
                {
                    string sql = "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                         "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                         "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'MM/DD/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'MM/DD/YYYY') AS VAL_TO, " +
                         "c.PHONE_1, c.FAX_1, c.EMAIL " +
                         "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID AND " +
                         "((b.BE_ID LIKE '%'||:c||'%') OR (UPPER(b.BE_NAME) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_ADDRESS) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_CITY) LIKE '%' ||:c|| '%') OR (UPPER((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID)) LIKE '%' ||:c|| '%'))";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORTING_RULE>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }

            result = new DataTableReportingRuleBilling();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();

            return result;
        }
    }
}