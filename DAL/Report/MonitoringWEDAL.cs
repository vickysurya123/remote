﻿using Dapper;
using Remote.Controllers;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.DAL
{
    internal class MonitoringWEDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                listData = connection.Query<DDBusinessEntity>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            connection.Dispose();
            return listData;
        }

        public IEnumerable<DDHarbourClass> GetDataHarbourClass()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDHarbourClass> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT HARBOUR_CLASS HB_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS FROM BUSINESS_ENTITY";

                listData = connection.Query<DDHarbourClass>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
        //public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        //{
        //    string sql = "";

        //    if (KodeCabang == "0")
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
        //    }
        //    else
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
        //    }

        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
        //    connection.Close();
        //    return listDetil;
        //}
        public IEnumerable<VW_BUSINESS_ENTITY> getBEID(string xPARAM)
        {
            VW_BUSINESS_ENTITY result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_BUSINESS_ENTITY> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, BE_ADDRESS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_BUSINESS_ENTITY>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();

            return listData;
        }
        public IEnumerable<AUP_BE_WEBAPPS> getHARBOURClASS(string xPARAM)
        {
            AUP_BE_WEBAPPS result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_BE_WEBAPPS> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<AUP_BE_WEBAPPS>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();

            return listData;
        }
        public IDataTable ShowAll(int draw, int start, int length, string be, string periode, string tipe_trans, string status, string search, string KodeCabang)
        {

            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<MonitoringWE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                        TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<MonitoringWE>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                { }
            }
            else
            {
                if (search.Length > 2)
                {
                    try
                    {
                        string sql = @" select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                                   TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK WHERE ((INSTALLATION_CODE LIKE '%' ||:c|| '%') 
                                    OR ((POWER_CAPACITY) LIKE '%'||:c||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'||:c||'%') OR (CUSTOMER_ID LIKE '%' ||:c|| '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' ||:c|| '%') OR 
                                    (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%')) ORDER BY INSTALLATION_NUMBER DESC ";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<MonitoringWE>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search });
                    }
                    catch (Exception)
                    { }
                }
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<AUP_REPORT_BE> ShowAllTwo(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT B.BE_ID || ' - ' || B.BE_NAME AS BE, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, b.BE_ADDRESS, " +
                                     "(SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID, b.POSTAL_CODE, " +
                                     "d.amount, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, phone_1 || '  ' || PHONE_2 as phone, fax_1 || '   ' || FAX_2 as fax, c.EMAIL " +
                                     "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c, BUSINESS_ENTITY_DETAIL d WHERE c.BE_ID = b.BE_ID(+) and c.id = d.id ";

                listData = connection.Query<AUP_REPORT_BE>(sql, new
                {
                    c = KodeCabang
                });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            connection.Dispose();
            return listData;

        }

        //----------------------------- DEPRECATED --------------------------------------
        public IDataTable GetDataFilter(int draw, int start, int length, string be, string periode, string tipe_trans, string status, string search, string KodeCabang)
        {

            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<MonitoringWE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string v_be2 = query.Query_BE("a");

            string sql = "";
            string where1 = "";
            string where2 = "";
            string where3 = "";

            if (be != null && be != "")
            {
                where1 += " and z.BRANCH_ID = " + be;
                where2 += " and a.BRANCH_ID = " + be;
            }

            if (tipe_trans != null && tipe_trans != "")
            {
                if (tipe_trans == "AIR")
                {
                    where1 += " and z.INSTALLATION_TYPE = 'A-Sambungan Air' ";
                    where2 += " and a.INSTALLATION_TYPE = 'A-Sambungan Air' ";
                }
                else
                {
                    where1 += " and z.INSTALLATION_TYPE = 'L-Sambungan Listrik' ";
                    where2 += " and a.INSTALLATION_TYPE = 'L-Sambungan Listrik' ";
                }

            }
            if (status != null && status != "")
            {
                if (status == "Cancel")
                {
                    where2 += " and CANCEL_STATUS is not null ";
                }
                else if (status == "Posted")
                {
                    where2 += " and CANCEL_STATUS is null and SAP_DOCUMENT_NUMBER is not null ";
                }
                else if (status == "Entried")
                {
                    where2 += " and PERIOD is not null ";
                }
                else if (status == "Entried_Mobile")
                {
                    where2 += " and PERIOD is not null ";
                    where3 += " and PLATFORM = 'MOBILE_APPS' ";
                }
                else
                {
                    where2 += " and PERIOD is null ";
                }
            }

            string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");


            sql = @"select a.INSTALLATION_TYPE,a.BE_ID,a.CUSTOMER_ID,a.INSTALLATION_DATE,a.INSTALLATION_ADDRESS,a.TARIFF_CODE,a.MINIMUM_AMOUNT,a.CURRENCY,a.TAX_CODE,a.RO_CODE,a.STATUS,a.POWER_CAPACITY,a.INSTALLATION_NUMBER,a.ID,a.CUSTOMER_NAME,a.CUSTOMER_SAP_AR,a.INSTALLATION_CODE,a.PROFIT_CENTER || ' - ' || d.TERMINAL_NAME as PROFIT_CENTER,a.RO_NUMBER,a.MINIMUM_PAYMENT,a.MULTIPLY_FACT,a.BIAYA_BEBAN,a.MINIMUM_USED,a.BRANCH_ID,a.BIAYA_ADMIN, 
                    b.PERIOD,b.POSTING_DATE, b.SAP_DOCUMENT_NUMBER, b.CANCEL_STATUS,c.BE_NAME,a.serial_number,
                    case 
                      when PERIOD is not null then
                        'Entried'
                      else
                        'Not Entried'
                    end as STATUS_NOW ,
                    case 
                      when CANCEL_STATUS is not null then
                        'Cancel'
                      when SAP_DOCUMENT_NUMBER is not null then
                        'Posted'
                      else
                        'Not Posted'
                    end as STATUS_POSTING, b.id as no_billing,b.CREATED_BY
                    from prop_services_installation a
                    left join(select id, INSTALLATION_NUMBER, PERIOD, z.POSTING_DATE, z.CANCEL_STATUS, z.SAP_DOCUMENT_NUMBER,CREATED_BY from V_TRANS_WE_LIST_HEADER z
                        where z.PERIOD = :periode " + where3 + where1 + v_be + @" order by z.INSTALLATION_NUMBER
                        )b on a.INSTALLATION_NUMBER = b.INSTALLATION_NUMBER
                    join BUSINESS_ENTITY c on c.BRANCH_ID = a.BRANCH_ID join profit_center d on d.PROFIT_CENTER_ID = a.PROFIT_CENTER
                    where a.STATUS = 1 and d.status = 1" + where2 + v_be2 + wheresearch + @" order by a.INSTALLATION_NUMBER";


            try
            {
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<MonitoringWE>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang, periode = periode });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang, periode = periode });
            }
            catch (Exception e)
            { }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<MonitoringWE> GetDataFilterTwo(string be, string tipe_trans, string periode, string KodeCabang)
        {
            IEnumerable<MonitoringWE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_tipe_trans = string.IsNullOrEmpty(tipe_trans) ? "x" : tipe_trans;
            string x_periode = string.IsNullOrEmpty(periode) ? "x" : periode;

            if (KodeCabang == "0")
            {
                if (x_be.Length >= 0 && x_tipe_trans == "AIR")
                {
                    try
                    {
                        string sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                                        POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK 
                                        where INSTALLATION_TYPE = 'A-Sambungan Air'";

                        listData = connection.Query<MonitoringWE>(sql, new
                        {
                            x_be = x_be,
                            x_tipe_trans = x_tipe_trans,
                            x_periode = x_periode
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        string sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                                        POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK 
                                        where INSTALLATION_TYPE = 'L-Sambungan Listrik'";

                        listData = connection.Query<MonitoringWE>(sql, new
                        {
                            x_be = x_be,
                            x_tipe_trans = x_tipe_trans,
                            x_periode = x_periode
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }


            connection.Close();
            connection.Dispose();
            return listData;

        }

        public List<MonitoringWE> GetDetailExcel(string BE_NAME, string INSTALLATION_TYPE, string PERIOD, string STATUS_NOW, string KodeCabang)
        {
            List<MonitoringWE> listData = null;
            string sql = "";
            string where1 = "";
            string where2 = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string v_be2 = query.Query_BE("a");
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                if (BE_NAME != null && BE_NAME != "")
                {
                    where1 += " and z.BRANCH_ID = " + BE_NAME;
                    where2 += " and a.BRANCH_ID = " + BE_NAME;
                }

                if (INSTALLATION_TYPE != null && INSTALLATION_TYPE != "")
                {
                    if (INSTALLATION_TYPE == "AIR")
                    {
                        where1 += " and z.INSTALLATION_TYPE = 'A-Sambungan Air' ";
                        where2 += " and a.INSTALLATION_TYPE = 'A-Sambungan Air' ";
                    }
                    else
                    {
                        where1 += " and z.INSTALLATION_TYPE = 'L-Sambungan Listrik' ";
                        where2 += " and a.INSTALLATION_TYPE = 'L-Sambungan Listrik' ";
                    }

                }
                if (STATUS_NOW != null && STATUS_NOW != "")
                {
                    if (STATUS_NOW == "Cancel")
                    {
                        where2 += " and CANCEL_STATUS is not null ";
                    }
                    else if (STATUS_NOW == "Posted")
                    {
                        where2 += " and CANCEL_STATUS is null and SAP_DOCUMENT_NUMBER is not null ";
                    }
                    else if (STATUS_NOW == "Entried")
                    {
                        where2 += " and PERIOD is not null ";
                    }
                    else
                    {
                        where2 += " and PERIOD is null ";
                    }
                }



                sql = @"select a.INSTALLATION_TYPE,a.BE_ID,a.CUSTOMER_ID,a.INSTALLATION_DATE,a.INSTALLATION_ADDRESS,a.TARIFF_CODE,a.MINIMUM_AMOUNT,a.CURRENCY,a.TAX_CODE,a.RO_CODE,a.STATUS,a.POWER_CAPACITY,a.INSTALLATION_NUMBER,a.ID,a.CUSTOMER_NAME,a.CUSTOMER_SAP_AR,a.INSTALLATION_CODE,a.serial_number, a.PROFIT_CENTER,a.RO_NUMBER,a.MINIMUM_PAYMENT,a.MULTIPLY_FACT,a.BIAYA_BEBAN,a.MINIMUM_USED,a.BRANCH_ID,a.BIAYA_ADMIN, 
                    b.PERIOD,b.POSTING_DATE, b.SAP_DOCUMENT_NUMBER, b.CANCEL_STATUS,c.BE_NAME,
                    case 
                      when PERIOD is not null then
                        'Entried'
                      else
                        'Not Entried'
                    end as STATUS_NOW ,
                    case 
                      when CANCEL_STATUS is not null then
                        'Cancel'
                      when SAP_DOCUMENT_NUMBER is not null then
                        'Posted'
                      else
                        'Not Posted'
                    end as STATUS_POSTING,b.id as no_billing
                    end as STATUS_POSTING,b.id as no_billing  
                    from prop_services_installation a
                    left join(select id, INSTALLATION_NUMBER, PERIOD, z.POSTING_DATE, z.CANCEL_STATUS, z.SAP_DOCUMENT_NUMBER from V_TRANS_WE_LIST_HEADER z
                        where z.PERIOD = :periode " + where1 + v_be + @" order by z.INSTALLATION_NUMBER
                        )b on a.INSTALLATION_NUMBER = b.INSTALLATION_NUMBER
                    join BUSINESS_ENTITY c on c.BRANCH_ID = a.BRANCH_ID
                    where a.STATUS = 1 " + where2 + v_be2 + @" order by a.INSTALLATION_NUMBER";


                listData = connection.Query<MonitoringWE>(sql, new { periode = PERIOD, d = KodeCabang }).ToList();

            }
            catch (Exception ex)
            {
                listData = null;
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IDataTable GetDataFilterNew(int draw, int start, int length, string be, string periode, string tipe_trans, string status, string search, string KodeCabang)
        {
            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_periode = string.IsNullOrEmpty(periode) ? "x" : status == "Not Entried" ? "x" : periode;
            //string x_periode = string.IsNullOrEmpty(periode) ? "x" : periode;
            string x_tipe_trans = string.IsNullOrEmpty(tipe_trans) ? "x" : tipe_trans;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<MonitoringWE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_CODE LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) ORDER BY INSTALLATION_NUMBER DESC " : "");

            string sql = "";
            string where = "";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            //if(x_periode.Length > 0)
            where += x_be == "x" ? " " : " and BRANCH_ID = " + x_be;
            where += x_periode == "x" ? " " : " and PERIOD = '" + x_periode + "'";
            where += x_status == "x" ? " " : " and STATUS_NOW = '" + x_status + "'";

            //if(x_status == "Posted")
            //{
            //    where += " and SAP_DOCUMENT_NUMBER is not null ";
            //}
            //else if (x_status == "Entried")
            //{
            //    where += " and PERIOD is not null ";
            //}
            //else if (x_status == "Cancel")
            //{
            //    where += " and CANCEL_STATUS is not null ";
            //}
            //else if (x_status == "Not Entried")
            //{
            //    where += " and PERIOD is null ";
            //}

            if (x_tipe_trans == "AIR")
            {
                sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                        TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK 
                        where INSTALLATION_TYPE = 'A-Sambungan Air' " + v_be + where + wheresearch;

            }
            else if (x_tipe_trans == "LISTRIK")
            {
                sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                        TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK 
                        where INSTALLATION_TYPE = 'L-Sambungan Listrik' " + v_be + where + wheresearch;

            }
            //else if (x_periode != null)
            //{
            //    sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
            //            TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK WHERE 1 = 1 " + v_be + where + wheresearch;

            //}
            else
            {
                sql = @"select INSTALLATION_CODE, INSTALLATION_TYPE, PROFIT_CENTER, CUSTOMER_NAME, POWER_CAPACITY, INSTALLATION_ADDRESS,
                       TO_CHAR(POSTING_DATE,'DD-MM-YYYY') as POSTING_DATE, STATUS_NOW, PERIOD from V_MON_AIR_LISTRIK WHERE 1 = 1 " + v_be + where + wheresearch;

            }



            try
            {
                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<MonitoringWE>(fullSql, new { a = start, b = end, d = KodeCabang });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
            }
            catch (Exception e)
            { }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }
        public string GetNamaCabang(string kdCabang)
        {
            string namaCabang = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var sql = @"select BE_NAME FROM BUSINESS_ENTITY where BRANCH_ID=:a";
                namaCabang = connection.ExecuteScalar<string>(sql, new { a = kdCabang });
            }
            catch (Exception ex)
            {

            }

            connection.Close();
            connection.Dispose();

            return namaCabang;
        }
    }
}
