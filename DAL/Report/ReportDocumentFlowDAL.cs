﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.MasterRentalObject;
using Remote.Models.ReportDocumentFlow;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ReportDocumentFlowDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }
        public IEnumerable<DDDocumentFlow> GetDataContractOfferNumb(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDDocumentFlow> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT CONTRACT_OFFER_NO, CONTRACT_OFFER_NO || ' - ' || CONTRACT_OFFER_NAME AS OFFER_NAME FROM PROP_CONTRACT_OFFER";

                    listData = connection.Query<DDDocumentFlow>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT CONTRACT_OFFER_NO, CONTRACT_OFFER_NO || ' - ' || CONTRACT_OFFER_NAME AS OFFER_NAME FROM PROP_CONTRACT_OFFER where BRANCH_ID=:a";

                    listData = connection.Query<DDDocumentFlow>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }


            connection.Close();
            return listData;
        }
        public IEnumerable<DDDocumentFlow> GetDataRentalRequestNumb(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDDocumentFlow> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_NO || ' - ' || RENTAL_REQUEST_NAME AS REQUEST_NAME FROM PROP_RENTAL_REQUEST";

                    listData = connection.Query<DDDocumentFlow>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_NO || ' - ' || RENTAL_REQUEST_NAME AS REQUEST_NAME FROM PROP_RENTAL_REQUEST WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDDocumentFlow>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }
        public IEnumerable<DDDocumentFlow> GetDataContractOfferStatus()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDDocumentFlow> listData = null;

            try
            {
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_NAME FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT_OFFER_STATUS'";
                listData = connection.Query<DDDocumentFlow>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
        public IEnumerable<DDDocumentFlow> GetDataRentalRequestStatus()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDDocumentFlow> listData = null;

            try
            {
                string sql = "SELECT STATUS FROM PROP_RENTAL_REQUEST GROUP BY STATUS";
                listData = connection.Query<DDDocumentFlow>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
        public IEnumerable<DDDocumentFlow> GetDataContractNumb(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDDocumentFlow> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT CONTRACT_NO, CONTRACT_NO || ' - ' || CONTRACT_NAME AS CONTRACT_NAME FROM PROP_CONTRACT";

                    listData = connection.Query<DDDocumentFlow>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT CONTRACT_NO, CONTRACT_NO || ' - ' || CONTRACT_NAME AS CONTRACT_NAME FROM PROP_CONTRACT WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDDocumentFlow>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<AUP_REPORT_DOCUMENT_FLOW> ShowFilterTwo(string be_id, string rental_req_no, string rental_req_status, string contract_offer_no, string contract_offer_status, string contract_no, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_DOCUMENT_FLOW> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_request_no = string.IsNullOrEmpty(rental_req_no) ? "x" : rental_req_no;
            string x_request_status = string.IsNullOrEmpty(rental_req_status) ? "x" : rental_req_status;
            string x_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_offer_status = string.IsNullOrEmpty(contract_offer_status) ? "x" : contract_offer_status;
            string x_offer_contract = string.IsNullOrEmpty(contract_no) ? "x" : contract_no; 

            if (KodeCabang == "0") 
            {
                if (x_be_id.Length >= 0 && x_request_no.Length >= 0 && x_request_status.Length >= 0 && x_offer_no.Length >= 0 && x_offer_status.Length >= 0 && x_offer_contract.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT REPORT_DOCUMENT_FLOWS.* FROM (SELECT DISTINCT D.BRANCH_ID, A.BE_ID AS BUSINESS_ENTITY, A.RENTAL_REQUEST_NO AS RENTAL_REQUEST_NUMBER, A.CREATION_BY AS RENTAL_REQUEST_CREATED_BY, A.CREATION_DATE AS DATE_RENTAL_REQUEST_CREATE, " +
                                        "A.STATUS AS RENTAL_REQUEST_STATUS, A.APPROVED_DATE AS DATE_RENTAL_REQUEST_APPROVED, B.CONTRACT_OFFER_NO AS CONTRACT_OFFER_NUMBER, "+
                                        "C.STATUS_CONTRACT_DESC AS CONTRACT_OFFER_LAST_STATUS, B.OFFER_STATUS, "+
                                        "(CASE "+
                                            "WHEN A.APPROVED_DATE IS NOT NULL AND A.CREATION_DATE IS NOT NULL "+
                                            "THEN "+
                                            "CASE WHEN (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) - 1 END "+
                                            "ELSE NULL "+
                                        "END) DELAYS_RENTAL_REQUEST_APPROVED, "+
                                        "B.CREATION_DATE AS DATE_OFFER_CREATED, "+
                                        "(CASE "+
                                            "WHEN B.CREATION_DATE IS NOT NULL AND A.APPROVED_DATE IS NOT NULL "+
                                            "THEN "+
                                            "CASE WHEN (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) - 1 END "+
                                            "ELSE NULL "+
                                        "END) DELAYS_CONTRACT_OFFER_APPROVED, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_MANAGERS IS NOT NULL "+
                                            "THEN max(F.RELEASE_TO_WORKFLOW) over (partition by F.BUSINESS_ENTITY order by F.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) RELEASE_TO_WORKFLOW, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_MANAGERS IS NOT NULL "+
                                            "THEN F.DELAYS_RELEASE_TO_WORKFLOW "+
                                            "ELSE NULL "+
                                        "END) DELAYS_RELEASE_TO_WORKFLOW, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_DEPT_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Mgr%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Mgr%' "+
                                            "THEN max(G.APPROVED_BY_MANAGER) over (partition by G.BUSINESS_ENTITY order by G.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) APPROVED_BY_MANAGER, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_DEPT_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Mgr%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Mgr%' "+
                                            "THEN G.DELAYS_APPROVE_BY_MANAGER "+
                                            "ELSE NULL "+
                                        "END) DELAYS_APPROVE_BY_MANAGER, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Dept GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Dept GM%' "+
                                            "THEN max(H.APPROVED_BY_DEPT_GM) over (partition by H.BUSINESS_ENTITY order by H.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) APPROVED_BY_DEPT_GM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Dept GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Dept GM%' "+
                                            "THEN H.DELAYS_APPROVE_BY_DEPT_GM "+
                                            "ELSE NULL "+
                                        "END) DELAYS_APPROVE_BY_DEPT_GM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_SMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by GM%' "+
                                            "THEN max(I.APPROVED_BY_GM) over (partition by I.BUSINESS_ENTITY order by I.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) APPROVED_BY_GM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_SMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by GM%' "+
                                            "THEN I.DELAYS_APPROVE_BY_GM "+
                                            "ELSE NULL "+
                                        "END) DELAYS_APPROVE_BY_GM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_DIRS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by SM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by SM%' "+
                                            "THEN max(J.APPROVED_BY_SM) over (partition by J.BUSINESS_ENTITY order by J.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) APPROVED_BY_SM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_DIRS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by SM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by SM%' "+
                                            "THEN J.DELAYS_APPROVE_BY_SM "+
                                            "ELSE NULL "+
                                        "END) DELAYS_APPROVE_BY_SM, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_KOM IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Director%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Director%' "+
                                            "THEN max(K.APPROVED_BY_DIR) over (partition by K.BUSINESS_ENTITY order by K.BUSINESS_ENTITY) "+
                                            "ELSE NULL "+
                                        "END) APPROVED_BY_DIR, "+
                                        "(CASE "+
                                            "WHEN C.APPROVED_BY_KOM IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Director%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Director%' "+
                                            "THEN K.DELAYS_APPROVE_BY_DIR "+
                                            "ELSE NULL "+
                                        "END) DELAYS_APPROVE_BY_DIR, "+
                                        "C.APPROVED_BY_KOM,C.APPROVED_BY_RUPS,D.CONTRACT_NO,D.CREATION_DATE "+
                                        "FROM PROP_RENTAL_REQUEST A,PROP_CONTRACT_OFFER B, "+
                                        "(SELECT  C.STATUS_CONTRACT, C.CONTRACT_OFFER_NO, C.STATUS_CONTRACT_DESC, C.CREATION_DATE AS DATE_CONTRACT_CREATE "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 20 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) RELEASE_TO_WORKFLOWS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 30 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_MANAGERS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 40 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_DEPT_GMS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 50 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_GMS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 60 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_SMS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 70 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_DIRS "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 80 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_KOM "+
                                            ",(CASE "+
                                                "WHEN C.STATUS_CONTRACT = 90 "+
                                                "THEN C.CREATION_DATE "+
                                                "ELSE NULL "+
                                            "END) APPROVED_BY_RUPS "+
                                            "FROM PROP_WORKFLOW_LOG C) C, "+
                                        "PROP_CONTRACT D, BUSINESS_ENTITY E, V_A_RELEASE_TO_WORKFLOW F, V_A_APPROVE_BY_MANAGER G, V_A_APPROVE_BY_DEPT_GM H, V_A_APPROVE_BY_GM I, V_A_APPROVE_BY_SM J, V_A_APPROVE_BY_DIR K "+
                                        "WHERE "+
                                            "A.RENTAL_REQUEST_NO = B.RENTAL_REQUEST_NO(+) AND "+
                                            "B.CONTRACT_OFFER_NO = C.CONTRACT_OFFER_NO(+) AND "+
                                            "B.CONTRACT_OFFER_NO = D.CONTRACT_OFFER_NO(+) AND "+
                                            "A.RENTAL_REQUEST_NO = F.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "A.RENTAL_REQUEST_NO = G.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "A.RENTAL_REQUEST_NO = H.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "A.RENTAL_REQUEST_NO = I.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "A.RENTAL_REQUEST_NO = J.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "A.RENTAL_REQUEST_NO = K.RENTAL_REQUEST_NUMBER(+) AND "+
                                            "D.BE_ID = E.BE_ID(+) AND "+
                                            "(C.STATUS_CONTRACT_DESC LIKE '%Revised%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected%') "+
                                            "AND A.BE_ID = DECODE(:x_be_id, 'x', A.BE_ID,:x_be_id) "+
                                            "AND B.RENTAL_REQUEST_NO = DECODE(:x_request_no, 'x', B.RENTAL_REQUEST_NO,:x_request_no) "+
                                            "AND A.STATUS = DECODE(:x_request_status, 'x', A.STATUS,:x_request_status) "+
                                            "AND B.CONTRACT_OFFER_NO = DECODE(:x_offer_no, 'x', B.CONTRACT_OFFER_NO,:x_offer_no) "+
                                            "AND B.OFFER_STATUS = DECODE(:x_offer_status, 'x', B.OFFER_STATUS,:x_offer_status) "+
                                            "AND D.CONTRACT_NO = DECODE(:x_offer_contract, 'x', D.CONTRACT_NO,:x_offer_contract) "+
                                    "UNION ALL "+
                                        "SELECT DISTINCT A.BRANCH_ID ,A.BUSINESS_ENTITY, A.RENTAL_REQUEST_NUMBER, A.RENTAL_REQUEST_CREATED_BY, A.DATE_CREATED, A.RENTAL_REQUEST_STATUS, A.DATE_RENTAL_REQUEST_APPROVED, "+
                                            "A.CONTRACT_OFFER_NUMBER, A.CONTRACT_OFFER_LAST_STATUS, A.OFFER_STATUS, A.DELAYS_RENTAL_REQUEST_APPROVED, A.DATE_OFFER_CREATED, A.DELAYS_CONTRACT_OFFER_APPROVED, "+
                                            "max(B.RELEASE_TO_WORKFLOW) over (partition by B.BUSINESS_ENTITY order by B.BUSINESS_ENTITY) RELEASE_TO_WORKFLOW, B.DELAYS_RELEASE_TO_WORKFLOW, C.APPROVED_BY_MANAGER, C.DELAYS_APPROVE_BY_MANAGER, D.APPROVED_BY_DEPT_GM, D.DELAYS_APPROVE_BY_DEPT_GM, "+
                                            "E.APPROVED_BY_GM, E.DELAYS_APPROVE_BY_GM, F.APPROVED_BY_SM, F.DELAYS_APPROVE_BY_SM, G.APPROVED_BY_DIR, G.DELAYS_APPROVE_BY_DIR, A.APPROVED_BY_KOM,A.APPROVED_BY_RUPS,A.CONTRACT_NO,A.CREATION_DATE "+
                                        "FROM V_REPORT_CONTRACT_OFFER_LEFT A, V_A_RELEASE_TO_WORKFLOW B, V_A_APPROVE_BY_MANAGER C, V_A_APPROVE_BY_DEPT_GM D, V_A_APPROVE_BY_GM E, V_A_APPROVE_BY_SM F, V_A_APPROVE_BY_DIR G "+
                                        "WHERE A.BUSINESS_ENTITY=B.BUSINESS_ENTITY(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=B.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=B.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=C.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=C.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=D.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=D.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=E.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=E.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=F.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=F.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.RENTAL_REQUEST_NUMBER=G.RENTAL_REQUEST_NUMBER(+) "+
                                        "AND A.CONTRACT_OFFER_NUMBER=G.CONTRACT_OFFER_NUMBER(+) "+
                                        "AND A.BUSINESS_ENTITY = DECODE(:x_be_id, 'x', A.BUSINESS_ENTITY,:x_be_id) "+
                                        "AND A.RENTAL_REQUEST_NUMBER = DECODE(:x_request_no, 'x', A.RENTAL_REQUEST_NUMBER,:x_request_no) "+
                                        "AND A.RENTAL_REQUEST_STATUS = DECODE(:x_request_status, 'x', A.RENTAL_REQUEST_STATUS,:x_request_status) "+
                                        "AND A.CONTRACT_OFFER_NUMBER = DECODE(:x_offer_no, 'x', A.CONTRACT_OFFER_NUMBER,:x_offer_no) "+
                                        "AND A.OFFER_STATUS = DECODE(:x_offer_status, 'x', A.OFFER_STATUS,:x_offer_status) "+
                                        "AND A.CONTRACT_NO = DECODE(:x_offer_contract, 'x', A.CONTRACT_NO,:x_offer_contract)) REPORT_DOCUMENT_FLOWS";
                            
                            
                            //"SELECT DISTINCT A.BE_ID AS BUSINESS_ENTITY, A.RENTAL_REQUEST_NO AS RENTAL_REQUEST_NUMBER, A.CREATION_BY AS RENTAL_REQUEST_CREATED_BY, A.CREATION_DATE AS DATE_CREATED,A.STATUS AS RENTAL_REQUEST_STATUS, A.APPROVED_DATE AS DATE_RENTAL_REQUEST_APPROVED, B.CONTRACT_OFFER_NO AS CONTRACT_OFFER_NUMBER,C.STATUS_CONTRACT_DESC AS CONTRACT_OFFER_LAST_STATUS," +
                            //     "(CASE " +
                            //        "WHEN A.APPROVED_DATE IS NOT NULL AND A.CREATION_DATE IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) - 1 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_RENTAL_REQUEST_APPROVED,B.CREATION_DATE AS DATE_OFFER_CREATED," +
                            //     "(CASE " +
                            //        "WHEN B.CREATION_DATE IS NOT NULL AND A.APPROVED_DATE IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) - 1 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_CONTRACT_OFFER_APPROVED,C.RELEASE_TO_WORKFLOW," +
                            //     "(CASE " +
                            //        "WHEN C.RELEASE_TO_WORKFLOW IS NOT NULL AND B.CREATION_DATE IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY') - to_date(B.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY') - to_date(B.CREATION_DATE,'DD/MM/YYYY')) - 1 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_RELEASE_TO_WORKFLOW,C.APPROVED_BY_MANAGER," +
                            //     "(CASE " +
                            //        "WHEN C.APPROVED_BY_MANAGER IS NOT NULL AND C.RELEASE_TO_WORKFLOW IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY') - to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY') - to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY')) - 1 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_APPROVE_BY_MANAGER,C.APPROVED_BY_DEPT_GM," +
                            //     "(CASE " +
                            //        "WHEN C.APPROVED_BY_DEPT_GM IS NOT NULL AND C.APPROVED_BY_MANAGER IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY')) - 3 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_APPROVE_BY_DEPT_GM,C.APPROVED_BY_GM," +
                            //      "(CASE " +
                            //        "WHEN C.APPROVED_BY_GM IS NOT NULL AND C.APPROVED_BY_DEPT_GM IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.APPROVED_BY_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY')) - 3 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_SM," +
                            //      "(CASE " +
                            //        "WHEN C.APPROVED_BY_SM IS NOT NULL AND C.APPROVED_BY_GM IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.APPROVED_BY_SM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_GM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_SM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_GM,'DD/MM/YYYY')) - 4 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_DIR," +
                            //      "(CASE " +
                            //        "WHEN C.APPROVED_BY_DIR IS NOT NULL AND C.APPROVED_BY_SM IS NOT NULL " +
                            //        "THEN CASE WHEN (to_date(C.APPROVED_BY_DIR,'DD/MM/YYYY') - to_date(C.APPROVED_BY_SM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_DIR,'DD/MM/YYYY') - to_date(C.APPROVED_BY_SM,'DD/MM/YYYY')) - 4 END " +
                            //        "ELSE NULL " +
                            //        "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_KOM,C.APPROVED_BY_RUPS,D.CONTRACT_NO,D.CREATION_DATE " +
                            //      "FROM PROP_RENTAL_REQUEST A,PROP_CONTRACT_OFFER B," +
                            //      "(SELECT  C.STATUS_CONTRACT, C.CONTRACT_OFFER_NO, C.STATUS_CONTRACT_DESC, C.CREATION_DATE AS DATE_CONTRACT_CREATE" +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 20 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) RELEASE_TO_WORKFLOW" +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 30 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_MANAGER " +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 40 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_DEPT_GM" +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 50 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_GM" +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 60 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_SM " +
                            //      ",(CASE " +
                            //        "WHEN C.STATUS_CONTRACT = 70 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_DIR" +
                            //      ",(CASE " +
                            //      "WHEN C.STATUS_CONTRACT = 80 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_KOM" +
                            //      ",(CASE " +
                            //      "WHEN C.STATUS_CONTRACT = 90 " +
                            //         "THEN C.CREATION_DATE " +
                            //            "ELSE NULL " +
                            //            "END) APPROVED_BY_RUPS " +
                            //            "FROM PROP_WORKFLOW_LOG C) C,PROP_CONTRACT D, BUSINESS_ENTITY E " +
                            //     "WHERE A.RENTAL_REQUEST_NO = B.RENTAL_REQUEST_NO(+) AND B.CONTRACT_OFFER_NO = C.CONTRACT_OFFER_NO(+) AND B.CONTRACT_OFFER_NO = D.CONTRACT_OFFER_NO(+) AND D.BE_ID = E.BE_ID " +
                            //     "AND A.BE_ID = DECODE(:x_be_id, 'x', A.BE_ID,:x_be_id) " +
                            //     "AND A.RENTAL_REQUEST_NO = DECODE(:x_request_no, 'x', A.RENTAL_REQUEST_NO,:x_request_no) " +
                            //     "AND A.STATUS = DECODE(:x_request_status, 'x', A.STATUS,:x_request_status) " +
                            //     "AND B.CONTRACT_OFFER_NO = DECODE(:x_offer_no, 'x', B.CONTRACT_OFFER_NO,:x_offer_no) " +
                            //     "AND C.STATUS_CONTRACT = DECODE(:x_offer_status, 'x', C.STATUS_CONTRACT,:x_offer_status) " +
                            //     "AND D.CONTRACT_NO = DECODE(:x_offer_contract, 'x', D.CONTRACT_NO,:x_offer_contract)";

                        listData = connection.Query<AUP_REPORT_DOCUMENT_FLOW>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_request_no = x_request_no,
                            x_request_status = x_request_status,
                            x_offer_no = x_offer_no,
                            x_offer_status = x_offer_status,
                            x_offer_contract = x_offer_contract
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_request_no.Length >= 0 && x_request_status.Length >= 0 && x_offer_no.Length >= 0 && x_offer_status.Length >= 0 && x_offer_contract.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT REPORT_DOCUMENT_FLOWS.* FROM (SELECT DISTINCT D.BRANCH_ID, A.BE_ID AS BUSINESS_ENTITY, A.RENTAL_REQUEST_NO AS RENTAL_REQUEST_NUMBER, A.CREATION_BY AS RENTAL_REQUEST_CREATED_BY, A.CREATION_DATE AS DATE_CREATED, " +
                                        "A.STATUS AS RENTAL_REQUEST_STATUS, A.APPROVED_DATE AS DATE_RENTAL_REQUEST_APPROVED, B.CONTRACT_OFFER_NO AS CONTRACT_OFFER_NUMBER, " +
                                        "C.STATUS_CONTRACT_DESC AS CONTRACT_OFFER_LAST_STATUS, B.OFFER_STATUS, " +
                                        "(CASE " +
                                            "WHEN A.APPROVED_DATE IS NOT NULL AND A.CREATION_DATE IS NOT NULL " +
                                            "THEN " +
                                            "CASE WHEN (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) - 1 END " +
                                            "ELSE NULL " +
                                        "END) DELAYS_RENTAL_REQUEST_APPROVED, " +
                                        "B.CREATION_DATE AS DATE_OFFER_CREATED, " +
                                        "(CASE " +
                                            "WHEN B.CREATION_DATE IS NOT NULL AND A.APPROVED_DATE IS NOT NULL " +
                                            "THEN " +
                                            "CASE WHEN (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) - 1 END " +
                                            "ELSE NULL " +
                                        "END) DELAYS_CONTRACT_OFFER_APPROVED, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_MANAGERS IS NOT NULL " +
                                            "THEN max(F.RELEASE_TO_WORKFLOW) over (partition by F.BUSINESS_ENTITY order by F.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) RELEASE_TO_WORKFLOW, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_MANAGERS IS NOT NULL " +
                                            "THEN F.DELAYS_RELEASE_TO_WORKFLOW " +
                                            "ELSE NULL " +
                                        "END) DELAYS_RELEASE_TO_WORKFLOW, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_DEPT_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Mgr%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Mgr%' " +
                                            "THEN max(G.APPROVED_BY_MANAGER) over (partition by G.BUSINESS_ENTITY order by G.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) APPROVED_BY_MANAGER, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_DEPT_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Mgr%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Mgr%' " +
                                            "THEN G.DELAYS_APPROVE_BY_MANAGER " +
                                            "ELSE NULL " +
                                        "END) DELAYS_APPROVE_BY_MANAGER, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Dept GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Dept GM%' " +
                                            "THEN max(H.APPROVED_BY_DEPT_GM) over (partition by H.BUSINESS_ENTITY order by H.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) APPROVED_BY_DEPT_GM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_GMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Dept GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Dept GM%' " +
                                            "THEN H.DELAYS_APPROVE_BY_DEPT_GM " +
                                            "ELSE NULL " +
                                        "END) DELAYS_APPROVE_BY_DEPT_GM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_SMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by GM%' " +
                                            "THEN max(I.APPROVED_BY_GM) over (partition by I.BUSINESS_ENTITY order by I.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) APPROVED_BY_GM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_SMS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by GM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by GM%' " +
                                            "THEN I.DELAYS_APPROVE_BY_GM " +
                                            "ELSE NULL " +
                                        "END) DELAYS_APPROVE_BY_GM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_DIRS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by SM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by SM%' " +
                                            "THEN max(J.APPROVED_BY_SM) over (partition by J.BUSINESS_ENTITY order by J.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) APPROVED_BY_SM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_DIRS IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by SM%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by SM%' " +
                                            "THEN J.DELAYS_APPROVE_BY_SM " +
                                            "ELSE NULL " +
                                        "END) DELAYS_APPROVE_BY_SM, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_KOM IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Director%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Director%' " +
                                            "THEN max(K.APPROVED_BY_DIR) over (partition by K.BUSINESS_ENTITY order by K.BUSINESS_ENTITY) " +
                                            "ELSE NULL " +
                                        "END) APPROVED_BY_DIR, " +
                                        "(CASE " +
                                            "WHEN C.APPROVED_BY_KOM IS NOT NULL OR C.STATUS_CONTRACT_DESC LIKE '%Revised by Director%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected by Director%' " +
                                            "THEN K.DELAYS_APPROVE_BY_DIR " +
                                            "ELSE NULL " +
                                        "END) DELAYS_APPROVE_BY_DIR, " +
                                        "C.APPROVED_BY_KOM,C.APPROVED_BY_RUPS,D.CONTRACT_NO,D.CREATION_DATE " +
                                        "FROM PROP_RENTAL_REQUEST A,PROP_CONTRACT_OFFER B, " +
                                        "(SELECT  C.STATUS_CONTRACT, C.CONTRACT_OFFER_NO, C.STATUS_CONTRACT_DESC, C.CREATION_DATE AS DATE_CONTRACT_CREATE " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 20 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) RELEASE_TO_WORKFLOWS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 30 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_MANAGERS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 40 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_DEPT_GMS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 50 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_GMS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 60 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_SMS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 70 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_DIRS " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 80 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_KOM " +
                                            ",(CASE " +
                                                "WHEN C.STATUS_CONTRACT = 90 " +
                                                "THEN C.CREATION_DATE " +
                                                "ELSE NULL " +
                                            "END) APPROVED_BY_RUPS " +
                                            "FROM PROP_WORKFLOW_LOG C) C, " +
                                        "PROP_CONTRACT D, BUSINESS_ENTITY E, V_A_RELEASE_TO_WORKFLOW F, V_A_APPROVE_BY_MANAGER G, V_A_APPROVE_BY_DEPT_GM H, V_A_APPROVE_BY_GM I, V_A_APPROVE_BY_SM J, V_A_APPROVE_BY_DIR K " +
                                        "WHERE " +
                                            "A.RENTAL_REQUEST_NO = B.RENTAL_REQUEST_NO(+) AND " +
                                            "B.CONTRACT_OFFER_NO = C.CONTRACT_OFFER_NO(+) AND " +
                                            "B.CONTRACT_OFFER_NO = D.CONTRACT_OFFER_NO(+) AND " +
                                            "A.RENTAL_REQUEST_NO = F.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "A.RENTAL_REQUEST_NO = G.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "A.RENTAL_REQUEST_NO = H.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "A.RENTAL_REQUEST_NO = I.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "A.RENTAL_REQUEST_NO = J.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "A.RENTAL_REQUEST_NO = K.RENTAL_REQUEST_NUMBER(+) AND " +
                                            "D.BE_ID = E.BE_ID(+) AND " +
                                            "(C.STATUS_CONTRACT_DESC LIKE '%Revised%' OR C.STATUS_CONTRACT_DESC LIKE '%Rejected%') " +
                                            "AND A.BE_ID = DECODE(:x_be_id, 'x', A.BE_ID,:x_be_id) " +
                                            "AND B.RENTAL_REQUEST_NO = DECODE(:x_request_no, 'x', B.RENTAL_REQUEST_NO,:x_request_no) " +
                                            "AND A.STATUS = DECODE(:x_request_status, 'x', A.STATUS,:x_request_status) " +
                                            "AND B.CONTRACT_OFFER_NO = DECODE(:x_offer_no, 'x', B.CONTRACT_OFFER_NO,:x_offer_no) " +
                                            "AND B.OFFER_STATUS = DECODE(:x_offer_status, 'x', B.OFFER_STATUS,:x_offer_status) " +
                                            "AND D.CONTRACT_NO = DECODE(:x_offer_contract, 'x', D.CONTRACT_NO,:x_offer_contract) " +
                                            "AND B.BRANCH_ID = :d " +
                                    "UNION ALL " +
                                        "SELECT DISTINCT A.BRANCH_ID ,A.BUSINESS_ENTITY, A.RENTAL_REQUEST_NUMBER, A.RENTAL_REQUEST_CREATED_BY, A.DATE_CREATED, A.RENTAL_REQUEST_STATUS, A.DATE_RENTAL_REQUEST_APPROVED, " +
                                            "A.CONTRACT_OFFER_NUMBER, A.CONTRACT_OFFER_LAST_STATUS, A.OFFER_STATUS, A.DELAYS_RENTAL_REQUEST_APPROVED, A.DATE_OFFER_CREATED, A.DELAYS_CONTRACT_OFFER_APPROVED, " +
                                            "max(B.RELEASE_TO_WORKFLOW) over (partition by B.BUSINESS_ENTITY order by B.BUSINESS_ENTITY) RELEASE_TO_WORKFLOW, B.DELAYS_RELEASE_TO_WORKFLOW, C.APPROVED_BY_MANAGER, C.DELAYS_APPROVE_BY_MANAGER, D.APPROVED_BY_DEPT_GM, D.DELAYS_APPROVE_BY_DEPT_GM, " +
                                            "E.APPROVED_BY_GM, E.DELAYS_APPROVE_BY_GM, F.APPROVED_BY_SM, F.DELAYS_APPROVE_BY_SM, G.APPROVED_BY_DIR, G.DELAYS_APPROVE_BY_DIR, A.APPROVED_BY_KOM,A.APPROVED_BY_RUPS,A.CONTRACT_NO,A.CREATION_DATE " +
                                        "FROM V_REPORT_CONTRACT_OFFER_LEFT A, V_A_RELEASE_TO_WORKFLOW B, V_A_APPROVE_BY_MANAGER C, V_A_APPROVE_BY_DEPT_GM D, V_A_APPROVE_BY_GM E, V_A_APPROVE_BY_SM F, V_A_APPROVE_BY_DIR G " +
                                        "WHERE A.BUSINESS_ENTITY=B.BUSINESS_ENTITY(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=B.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=B.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=C.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=C.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=D.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=D.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=E.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=E.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=F.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=F.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.RENTAL_REQUEST_NUMBER=G.RENTAL_REQUEST_NUMBER(+) " +
                                        "AND A.CONTRACT_OFFER_NUMBER=G.CONTRACT_OFFER_NUMBER(+) " +
                                        "AND A.BUSINESS_ENTITY = DECODE(:x_be_id, 'x', A.BUSINESS_ENTITY,:x_be_id) " +
                                        "AND A.RENTAL_REQUEST_NUMBER = DECODE(:x_request_no, 'x', A.RENTAL_REQUEST_NUMBER,:x_request_no) " +
                                        "AND A.RENTAL_REQUEST_STATUS = DECODE(:x_request_status, 'x', A.RENTAL_REQUEST_STATUS,:x_request_status) " +
                                        "AND A.CONTRACT_OFFER_NUMBER = DECODE(:x_offer_no, 'x', A.CONTRACT_OFFER_NUMBER,:x_offer_no) " +
                                        "AND A.OFFER_STATUS = DECODE(:x_offer_status, 'x', A.OFFER_STATUS,:x_offer_status) " +
                                        "AND A.CONTRACT_NO = DECODE(:x_offer_contract, 'x', A.CONTRACT_NO,:x_offer_contract) " +
                                        "AND A.BRANCH_ID = :d) REPORT_DOCUMENT_FLOWS";

                        //string sql = "SELECT DISTINCT A.BE_ID AS BUSINESS_ENTITY, A.RENTAL_REQUEST_NO AS RENTAL_REQUEST_NUMBER, A.CREATION_BY AS RENTAL_REQUEST_CREATED_BY, A.CREATION_DATE AS DATE_CREATED,A.STATUS AS RENTAL_REQUEST_STATUS, A.APPROVED_DATE AS DATE_RENTAL_REQUEST_APPROVED, B.CONTRACT_OFFER_NO AS CONTRACT_OFFER_NUMBER,C.STATUS_CONTRACT_DESC AS CONTRACT_OFFER_LAST_STATUS," +
                        //         "(CASE " +
                        //            "WHEN A.APPROVED_DATE IS NOT NULL AND A.CREATION_DATE IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(A.APPROVED_DATE,'DD/MM/YYYY') - to_date(A.CREATION_DATE,'DD/MM/YYYY')) - 1 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_RENTAL_REQUEST_APPROVED,B.CREATION_DATE AS DATE_OFFER_CREATED," +
                        //         "(CASE " +
                        //            "WHEN B.CREATION_DATE IS NOT NULL AND A.APPROVED_DATE IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(B.CREATION_DATE,'DD/MM/YYYY') - to_date(A.APPROVED_DATE,'DD/MM/YYYY')) - 1 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_CONTRACT_OFFER_APPROVED,C.RELEASE_TO_WORKFLOW," +
                        //         "(CASE " +
                        //            "WHEN C.RELEASE_TO_WORKFLOW IS NOT NULL AND B.CREATION_DATE IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY') - to_date(B.CREATION_DATE,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY') - to_date(B.CREATION_DATE,'DD/MM/YYYY')) - 1 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_RELEASE_TO_WORKFLOW,C.APPROVED_BY_MANAGER," +
                        //         "(CASE " +
                        //            "WHEN C.APPROVED_BY_MANAGER IS NOT NULL AND C.RELEASE_TO_WORKFLOW IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY') - to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY') - to_date(C.RELEASE_TO_WORKFLOW,'DD/MM/YYYY')) - 1 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_APPROVE_BY_MANAGER,C.APPROVED_BY_DEPT_GM," +
                        //         "(CASE " +
                        //            "WHEN C.APPROVED_BY_DEPT_GM IS NOT NULL AND C.APPROVED_BY_MANAGER IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_MANAGER,'DD/MM/YYYY')) - 3 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_APPROVE_BY_DEPT_GM,C.APPROVED_BY_GM," +
                        //          "(CASE " +
                        //            "WHEN C.APPROVED_BY_GM IS NOT NULL AND C.APPROVED_BY_DEPT_GM IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.APPROVED_BY_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_GM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_DEPT_GM,'DD/MM/YYYY')) - 3 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_SM," +
                        //          "(CASE " +
                        //            "WHEN C.APPROVED_BY_SM IS NOT NULL AND C.APPROVED_BY_GM IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.APPROVED_BY_SM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_GM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_SM,'DD/MM/YYYY') - to_date(C.APPROVED_BY_GM,'DD/MM/YYYY')) - 4 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_DIR," +
                        //          "(CASE " +
                        //            "WHEN C.APPROVED_BY_DIR IS NOT NULL AND C.APPROVED_BY_SM IS NOT NULL " +
                        //            "THEN CASE WHEN (to_date(C.APPROVED_BY_DIR,'DD/MM/YYYY') - to_date(C.APPROVED_BY_SM,'DD/MM/YYYY')) = 0 THEN 0 ELSE (to_date(C.APPROVED_BY_DIR,'DD/MM/YYYY') - to_date(C.APPROVED_BY_SM,'DD/MM/YYYY')) - 4 END " +
                        //            "ELSE NULL " +
                        //            "END) DELAYS_APPROVE_BY_GM,C.APPROVED_BY_KOM,C.APPROVED_BY_RUPS,D.CONTRACT_NO,D.CREATION_DATE " +
                        //          "FROM PROP_RENTAL_REQUEST A,PROP_CONTRACT_OFFER B," +
                        //          "(SELECT  C.STATUS_CONTRACT, C.CONTRACT_OFFER_NO, C.STATUS_CONTRACT_DESC, C.CREATION_DATE AS DATE_CONTRACT_CREATE" +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 20 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) RELEASE_TO_WORKFLOW" +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 30 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_MANAGER " +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 40 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_DEPT_GM" +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 50 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_GM" +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 60 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_SM " +
                        //          ",(CASE " +
                        //            "WHEN C.STATUS_CONTRACT = 70 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_DIR" +
                        //          ",(CASE " +
                        //          "WHEN C.STATUS_CONTRACT = 80 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_KOM" +
                        //          ",(CASE " +
                        //          "WHEN C.STATUS_CONTRACT = 90 " +
                        //             "THEN C.CREATION_DATE " +
                        //                "ELSE NULL " +
                        //                "END) APPROVED_BY_RUPS " +
                        //                "FROM PROP_WORKFLOW_LOG C) C,PROP_CONTRACT D, BUSINESS_ENTITY E " +
                        //         "WHERE A.RENTAL_REQUEST_NO = B.RENTAL_REQUEST_NO(+) AND B.CONTRACT_OFFER_NO = C.CONTRACT_OFFER_NO(+) AND B.CONTRACT_OFFER_NO = D.CONTRACT_OFFER_NO(+) AND D.BE_ID = E.BE_ID " +
                        //         "AND A.BE_ID = DECODE(:x_be_id, 'x', A.BE_ID,:x_be_id) " +
                        //         "AND A.RENTAL_REQUEST_NO = DECODE(:x_request_no, 'x', A.RENTAL_REQUEST_NO,:x_request_no) " +
                        //         "AND A.STATUS = DECODE(:x_request_status, 'x', A.STATUS,:x_request_status) " +
                        //         "AND B.CONTRACT_OFFER_NO = DECODE(:x_offer_no, 'x', B.CONTRACT_OFFER_NO,:x_offer_no) " +
                        //         "AND C.STATUS_CONTRACT = DECODE(:x_offer_status, 'x', C.STATUS_CONTRACT,:x_offer_status) " +
                        //         "AND D.CONTRACT_NO = DECODE(:x_offer_contract, 'x', D.CONTRACT_NO,:x_offer_contract) " +
                        //         "AND D.BRANCH_ID = :d";

                        listData = connection.Query<AUP_REPORT_DOCUMENT_FLOW>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_request_no = x_request_no,
                            x_request_status = x_request_status,
                            x_offer_no = x_offer_no,
                            x_offer_status = x_offer_status,
                            x_offer_contract = x_offer_contract,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

    }
}