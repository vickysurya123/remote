﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportOtherService;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.Controllers
{
    class ReportMasterInstallationDAL
    {
        //----------------------- DEPRECATED ------------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id ASC";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }


            connection.Close();
            return listData;
        }
        public IEnumerable<VW_INSTALLATION_TYPE> getInstallationType(string xPARAM)
        {
            VW_INSTALLATION_TYPE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_INSTALLATION_TYPE> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT PROFIT_CENTER, INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_NAME, CUSTOMER_SAP_AR FROM PROP_SERVICES_INSTALLATION " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_INSTALLATION_TYPE>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }
        public IEnumerable<VW_INSTALLATION_TYPE> getInstallationCode(string xPARAM)
        {
            VW_INSTALLATION_TYPE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_INSTALLATION_TYPE> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT PROFIT_CENTER, INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_NAME, CUSTOMER_SAP_AR FROM PROP_SERVICES_INSTALLATION " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_INSTALLATION_TYPE>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }
        public IEnumerable<VW_INSTALLATION_TYPE> getCostumerMDM(string xPARAM)
        {
            VW_INSTALLATION_TYPE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_INSTALLATION_TYPE> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT PROFIT_CENTER, INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_NAME, CUSTOMER_SAP_AR FROM PROP_SERVICES_INSTALLATION " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_INSTALLATION_TYPE>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }
        public IEnumerable<VW_INSTALLATION_TYPE> getCostumerSAP(string xPARAM)
        {
            VW_INSTALLATION_TYPE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_INSTALLATION_TYPE> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT PROFIT_CENTER, INSTALLATION_TYPE, INSTALLATION_CODE, CUSTOMER_NAME, CUSTOMER_SAP_AR FROM PROP_SERVICES_INSTALLATION " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_INSTALLATION_TYPE>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }

        //-------------------- DEPRECATED ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT i.CUSTOMER_ID code, i.CUSTOMER_ID || ' - ' || i.CUSTOMER_NAME label, i.CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) " +
                        "AND ((UPPER(i.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (i.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT i.CUSTOMER_ID code, i.CUSTOMER_ID || ' - ' || i.CUSTOMER_NAME label, i.CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) " +
                        "AND ((UPPER(i.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (i.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND i.BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        public DataTableReportMasterInstallation ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT s.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                 "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, " +
                                 "BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                 "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                { }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------- DEPRECATED -------------------------
        public DataTableReportMasterInstallation GetDataFilter(int draw, int start, int length, string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string search, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                    "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                    "FROM V_REPORT_ELECTRIC_INSTALB " +
                                    "WHERE PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                                    "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                    "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) ";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });
                    }
                    catch (Exception)
                    { }
                }else
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                    "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                    "FROM V_REPORT_ELECTRIC_INSTALB " +
                                    "WHERE PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                                    "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                    "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) AND BRANCH_ID=:d";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    { }
                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //-------------------------- DEPRECATED ------------------------
        public IEnumerable<AUP_REPORT_MASTER_INSTALLATION> ShowFilterTwo(string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /* Queryne Adik'e Ageng Yg Membuat Gaduh
                        string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE(+) = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_TYPE = DECODE(:x_installation_type, 'x', s.INSTALLATION_TYPE,:x_installation_type) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";
                        */
                        /*string sql = "SELECT i.INSTALLATION_TYPE, i.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, i.INSTALLATION_CODE, i.TARIFF_CODE || ' - ' || s.AMOUNT BIAYA_BEBAN , i.CUSTOMER_ID || ' - ' || CUSTOMER_NAME CUSTOMER_NAME, " +
                                     "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE, i.STATUS, MINIMUM_USED, j.AMOUNT AS TARIF_BLOCK1, k.AMOUNT AS TARIF_BLOCK2, m.AMOUNT AS TARIF_KVARH, n.AMOUNT AS TARIF_LWBP, t.AMOUNT AS TARIF_WBP, q.PERCENTAGE ||'%' AS PPJU, r.PERCENTAGE ||'%' AS REDUKSI, i.MINIMUM_PAYMENT AS MINIMUM_PAYMENT, i.BIAYA_ADMIN AS BIAYA_ADMIN, i.RO_CODE || ' - ' ||  RO_NAME ASSIGNMENT_RO, i.BRANCH_ID " +
                                     "FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o, " +
                                     "V_REPORT_INSTALLATION_BLOK1 j, V_REPORT_INSTALLATION_BLOK2 k, V_REPORT_INSTALLATION_KVARH m, V_REPORT_INSTALLATION_LWBP n, V_REPORT_INSTALLATION_PPJU q, V_REPORT_INSTALLATION_REDUKSI r, V_REPORT_INSTALLATION_WBP t " +
                                     "WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) AND " +
                                     "i.INSTALLATION_CODE = j.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = k.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = m.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = n.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = q.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = r.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = t.INSTALLATION_CODE(+) AND " +
                                     "i.PROFIT_CENTER = DECODE(:x_profit_center, 'x', i.PROFIT_CENTER,:x_profit_center) " +
                                     "and i.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', i.INSTALLATION_CODE,:x_installation_code) " +
                                     "and i.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', i.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and i.STATUS = DECODE(:x_status, 'x', i.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                     "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                     "FROM V_REPORT_ELECTRIC_INSTALB " +
                                     "WHERE PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                                     "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status)";
                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        /* Queryne Adik'e Ageng Yg Membuat Gaduh
                        string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_TYPE = DECODE(:x_installation_type, 'x', s.INSTALLATION_TYPE,:x_installation_type) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status) AND s.BRANCH_ID=:a";
                        */

                        /*string sql = "SELECT i.INSTALLATION_TYPE, i.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, i.INSTALLATION_CODE, i.TARIFF_CODE || ' - ' || s.AMOUNT BIAYA_BEBAN , i.CUSTOMER_ID || ' - ' || CUSTOMER_NAME CUSTOMER_NAME, " +
                                     "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE, i.STATUS, MINIMUM_USED, j.AMOUNT AS TARIF_BLOCK1, k.AMOUNT AS TARIF_BLOCK2, m.AMOUNT AS TARIF_KVARH, n.AMOUNT AS TARIF_LWBP, t.AMOUNT AS TARIF_WBP, q.PERCENTAGE ||'%' AS PPJU, r.PERCENTAGE ||'%' AS REDUKSI, i.MINIMUM_PAYMENT AS MINIMUM_PAYMENT, i.BIAYA_ADMIN AS BIAYA_ADMIN, i.RO_CODE || ' - ' ||  RO_NAME ASSIGNMENT_RO, i.BRANCH_ID " +
                                     "FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o, " +
                                     "V_REPORT_INSTALLATION_BLOK1 j, V_REPORT_INSTALLATION_BLOK2 k, V_REPORT_INSTALLATION_KVARH m, V_REPORT_INSTALLATION_LWBP n, V_REPORT_INSTALLATION_PPJU q, V_REPORT_INSTALLATION_REDUKSI r, V_REPORT_INSTALLATION_WBP t " +
                                     "WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) AND " +
                                     "i.INSTALLATION_CODE = j.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = k.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = m.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = n.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = q.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = r.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = t.INSTALLATION_CODE(+) AND " +
                                     "i.PROFIT_CENTER = DECODE(:x_profit_center, 'x', i.PROFIT_CENTER,:x_profit_center) " +
                                     "and i.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', i.INSTALLATION_CODE,:x_installation_code) " +
                                     "and i.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', i.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and i.STATUS = DECODE(:x_status, 'x', i.STATUS,:x_status) AND i.BRANCH_ID=:a";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                    "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                    "FROM V_REPORT_ELECTRIC_INSTALB " +
                                    "WHERE PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                                    "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                    "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) AND BRANCH_ID=:a";

                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            a = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        //-------------------- AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomernew(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE("i");

                str = "SELECT DISTINCT i.CUSTOMER_ID code, i.CUSTOMER_ID || ' - ' || i.CUSTOMER_NAME label, i.CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) " +
                        "AND ((UPPER(i.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (i.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) " + v_be;
            
            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str, new { d = KodeCabang }).ToList();

            connection.Close();
            return exec;
        }

        //-------------------- NEW PROFIT CENTER -----------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 " + v_be;

                    listData = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang }).ToList();
                }
                catch (Exception ex) {
                    string aaa = ex.ToString();
                }

            connection.Close();
            return listData;
        }

        //----------------------- NEW GET DATA FILTER -----------------------------------
        public DataTableReportMasterInstallation GetDataFilternew(int draw, int start, int length, string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string search, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string where = "";
            where += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE, SERIAL_NUMBER, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                    "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                    "FROM V_REPORT_ELECTRIC_INSTALB " +
                                    "WHERE " +
                                    " INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                    "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) " + where + v_be;

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    { }
                
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------ NEW EXCEL ---------------------------------------
        public IEnumerable<AUP_REPORT_MASTER_INSTALLATION> ShowFilterTwonew(string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where = "";
            where += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                    try
                    {
                        /* Queryne Adik'e Ageng Yg Membuat Gaduh
                        string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'L-Sambungan Listrik' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_TYPE = DECODE(:x_installation_type, 'x', s.INSTALLATION_TYPE,:x_installation_type) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.ID = DECODE(:x_customer_mdm, 'x', s.ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status) AND s.BRANCH_ID=:a";
                        */

                        /*string sql = "SELECT i.INSTALLATION_TYPE, i.PROFIT_CENTER || ' - ' || TERMINAL_NAME PROFIT_CENTER, i.INSTALLATION_CODE, i.TARIFF_CODE || ' - ' || s.AMOUNT BIAYA_BEBAN , i.CUSTOMER_ID || ' - ' || CUSTOMER_NAME CUSTOMER_NAME, " +
                                     "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE, i.STATUS, MINIMUM_USED, j.AMOUNT AS TARIF_BLOCK1, k.AMOUNT AS TARIF_BLOCK2, m.AMOUNT AS TARIF_KVARH, n.AMOUNT AS TARIF_LWBP, t.AMOUNT AS TARIF_WBP, q.PERCENTAGE ||'%' AS PPJU, r.PERCENTAGE ||'%' AS REDUKSI, i.MINIMUM_PAYMENT AS MINIMUM_PAYMENT, i.BIAYA_ADMIN AS BIAYA_ADMIN, i.RO_CODE || ' - ' ||  RO_NAME ASSIGNMENT_RO, i.BRANCH_ID " +
                                     "FROM PROP_SERVICES_INSTALLATION i, PROFIT_CENTER p, SERVICES s, RENTAL_OBJECT o, " +
                                     "V_REPORT_INSTALLATION_BLOK1 j, V_REPORT_INSTALLATION_BLOK2 k, V_REPORT_INSTALLATION_KVARH m, V_REPORT_INSTALLATION_LWBP n, V_REPORT_INSTALLATION_PPJU q, V_REPORT_INSTALLATION_REDUKSI r, V_REPORT_INSTALLATION_WBP t " +
                                     "WHERE i.INSTALLATION_TYPE='L-Sambungan Listrik' AND i.PROFIT_CENTER=p.PROFIT_CENTER_ID(+) AND i.TARIFF_CODE=s.TARIFF_CODE(+) AND i.RO_CODE=O.RO_CODE(+) AND " +
                                     "i.INSTALLATION_CODE = j.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = k.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = m.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = n.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = q.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = r.INSTALLATION_CODE(+) AND i.INSTALLATION_CODE = t.INSTALLATION_CODE(+) AND " +
                                     "i.PROFIT_CENTER = DECODE(:x_profit_center, 'x', i.PROFIT_CENTER,:x_profit_center) " +
                                     "and i.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', i.INSTALLATION_CODE,:x_installation_code) " +
                                     "and i.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', i.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and i.STATUS = DECODE(:x_status, 'x', i.STATUS,:x_status) AND i.BRANCH_ID=:a";*/
                        string sql = "SELECT INSTALLATION_TYPE, PROFIT_CENTER, INSTALLATION_CODE,SERIAL_NUMBER, BIAYA_BEBAN , CUSTOMER_NAME, " +
                                    "CUSTOMER_SAP_AR, INSTALLATION_ADDRESS, INSTALLATION_DATE, STATUS, MINIMUM_USED, TARIF_BLOCK1, TARIF_BLOCK2, TARIF_KVARH, TARIF_LWBP, TARIF_WBP, PPJU, REDUKSI, MINIMUM_PAYMENT, BIAYA_ADMIN, ASSIGNMENT_RO, BRANCH_ID " +
                                    "FROM V_REPORT_ELECTRIC_INSTALB " +
                                    "WHERE " +
                                    " INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                    "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) " + where + v_be ;

                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                

            }

            connection.Close();
            return listData;

        }


    }
}
