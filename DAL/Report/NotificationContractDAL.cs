﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.NotificationContract;

namespace WebApps.DAL
{
    public class NotificationContractDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER_NAME name FROM PROP_CONTRACT " +
                        "WHERE ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER name, BRANCH_ID FROM PROP_CONTRACT " +
                        "WHERE ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        public IEnumerable<DDContractType> GetDataContractType(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractType> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONTRACT' AND REF_DATA != 'ZC06'";

                listData = connection.Query<DDContractType>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        //-------------------- FILTER & EXCEL UNTUK NOTIFICATION CONTRACT DEPRECATED----------
        public DataTablesNotificationContract ShowFilterNotification (int draw, int start, int length, string be_id, string contract_no, string contract_type, string customer_id, string date_start, string date_end, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;

            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "NVL(TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) "+
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "NVL(TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL AND BRANCH_ID = :d " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) "+
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                    
                }
            }

            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //------------------- deprecated ---------------------------
        public DataTablesNotificationContract ShowFilterHistory(int draw, int start, int length, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                            "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                            "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                {

                }
            }
            else
            //Pencarian untuk user cabang
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                            "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                            "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL AND BRANCH_ID = :d";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                }
                catch (Exception)
                {

                }
            }

            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }
        
        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelNotification (string be_id, string contract_no, string contract_type, string customer_id, string date_start, string date_end, string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;


            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, "+
                                "CASE " + 
                                "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' "+
                                "END AS STATUS " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) "+
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, " +
                                "CASE " +
                                "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' " +
                                "END AS STATUS " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL AND BRANCH_ID = :d " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelNotificationHistory(string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                            "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, " +
                            "CASE " +
                            "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' " +
                            "ELSE 'STOPPED' " +
                            "END AS STATUS " +
                            "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL";

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            else
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, " +
                        "CASE " +
                        "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' " +
                        "ELSE 'STOPPED' " +
                        "END AS STATUS " +
                        "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL AND BRANCH_ID = :d";

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { d = KodeCabang });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;

        }
        
        public bool ConfirmationDate(string updatedBy, DataNotificationContract data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE PROP_CONTRACT SET " +
                         "STOP_NOTIFICATION_DATE = TO_DATE(:a,'DD/MM/YYYY'), MEMO_NOTIFICATION = :b, STOP_NOTIFICATION_BY = :c " +
                         "WHERE CONTRACT_NO=:d";

            int r = connection.Execute(sql, new
            {
                a = data.STOP_NOTIFICATION_DATE,
                b = data.MEMO_NOTIFICATION,
                c = updatedBy,
                d = data.CONTRACT_NO
            });

            result = (r > 0) ? true : false;

            string sql1 = "UPDATE PROP_CONTRACT_NOTIFICATION SET " +
                         "STATUS_REMINDER = '1' " +
                         "WHERE CONTRACT_NO=:d";

            int r1 = connection.Execute(sql1, new
            {
                d = data.CONTRACT_NO
            });

            result = (r1 > 0) ? true : false;          

            connection.Close();
            return result;
        }

        //-------------------- FILTER & EXCEL UNTUK WARNING NOTIFICATION CONTRACT DEPRECATED----------
        public DataTablesNotificationContract ShowFilterWarning(int draw, int start, int length, string be_id_warning, string contract_no_warning, string contract_type_warning, string customer_id_warning, string date_start_warning, string date_end_warning, string KodeCabang)
        {
            string x_be_id_warning = string.IsNullOrEmpty(be_id_warning) ? "x" : be_id_warning;
            string x_contract_no_warning = string.IsNullOrEmpty(contract_no_warning) ? "x" : contract_no_warning;
            string x_contract_type_warning = string.IsNullOrEmpty(contract_type_warning) ? "x" : contract_type_warning;
            string x_customer_id_warning = string.IsNullOrEmpty(customer_id_warning) ? "x" : customer_id_warning;
            string x_date_start_warning = string.IsNullOrEmpty(date_start_warning) ? "x" : date_start_warning;
            string x_date_end_warning = string.IsNullOrEmpty(date_end_warning) ? "x" : date_end_warning;

            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {

                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, "+
                                "NVL(TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR'), '-') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) "+
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) "+
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) "+
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) "+
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {

                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "NVL(TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR'), '-') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL AND BRANCH_ID = :d " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }

                }
            }

            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //----------------------------- DEPRECATED ---------------------------------------
        public DataTablesNotificationContract ShowFilterWarningHistory(int draw, int start, int length, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                {

                }
            }
            else
            //Pencarian untuk user cabang
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL AND BRANCH_ID = :d";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                }
                catch (Exception)
                {

                }
            }

            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }
        
        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelWarning (string be_id_warning, string contract_no_warning, string contract_type_warning, string customer_id_warning, string date_start_warning, string date_end_warning, string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id_warning = string.IsNullOrEmpty(be_id_warning) ? "x" : be_id_warning;
            string x_contract_no_warning = string.IsNullOrEmpty(contract_no_warning) ? "x" : contract_no_warning;
            string x_contract_type_warning = string.IsNullOrEmpty(contract_type_warning) ? "x" : contract_type_warning;
            string x_customer_id_warning = string.IsNullOrEmpty(customer_id_warning) ? "x" : customer_id_warning;
            string x_date_start_warning = string.IsNullOrEmpty(date_start_warning) ? "x" : date_start_warning;
            string x_date_end_warning = string.IsNullOrEmpty(date_end_warning) ? "x" : date_end_warning;
            
            if (KodeCabang == "0")
            {
                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id_warning,'x',BE_ID,:x_be_id_warning) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no_warning,'x',CONTRACT_NO,:x_contract_no_warning) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type_warning,'x',CONTRACT_TYPE,:x_contract_type_warning) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id_warning,'x',CUSTOMER_ID,:x_customer_id_warning) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id_warning = x_be_id_warning,
                            x_contract_no_warning = x_contract_no_warning,
                            x_contract_type_warning = x_contract_type_warning,
                            x_customer_id_warning = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL AND BRANCH_ID = :d " +
                                "AND BE_ID = DECODE(:x_be_id_warning,'x',BE_ID,:x_be_id_warning) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no_warning,'x',CONTRACT_NO,:x_contract_no_warning) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type_warning,'x',CONTRACT_TYPE,:x_contract_type_warning) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id_warning,'x',CUSTOMER_ID,:x_customer_id_warning) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id_warning = x_be_id_warning,
                            x_contract_no_warning = x_contract_no_warning,
                            x_contract_type_warning = x_contract_type_warning,
                            x_customer_id_warning = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelWarningHistory(string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING, " +
                        "CASE " +
                        "WHEN STOP_WARNING_DATE IS NOT NULL THEN 'STOPPED' " +
                        "ELSE '' " +
                        "END AS STATUS " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL ";

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            else
            {
                try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING, " +
                        "CASE " +
                        "WHEN STOP_WARNING_DATE IS NULL THEN '' " +
                        "ELSE 'STOPPED' " +
                        "END AS STATUS " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL AND BRANCH_ID = :a ";

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { a = KodeCabang });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;

        }
        
        public bool CheckOutDate(string updatedBy, DataNotificationContract data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE PROP_CONTRACT SET " +
                         "STOP_WARNING_DATE = TO_DATE(:a,'DD/MM/YYYY'), MEMO_WARNING = :b, STOP_WARNING_BY = :c " +
                         "WHERE CONTRACT_NO=:d";

            int r = connection.Execute(sql, new
            {
                a = data.STOP_WARNING_DATE,
                b = data.MEMO_WARNING,
                c = updatedBy,
                d = data.CONTRACT_NO
            });

            result = (r > 0) ? true : false;

            string sql1 = "UPDATE PROP_CONTRACT_NOTIFICATION SET " +
                         "STATUS_REMINDER = '1' " +
                         "WHERE CONTRACT_NO=:d AND NOTIFICATION_TYPE IN (2, 3, 4)";

            int r1 = connection.Execute(sql1, new
            {
                d = data.CONTRACT_NO
            });

            result = (r1 > 0) ? true : false;    

            connection.Close();
            return result;
        }

        //-------------------- NEW FILTER ----------
        public DataTablesNotificationContract ShowFilterNotificationnew(int draw, int start, int length, string be_id, string contract_no, string contract_type, string customer_id, string date_start, string date_end, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;

            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "NVL(TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR'), '-') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR'))) " + v_be;

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                       
            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }
                
        //-------------------- NEW FILTER WARNING ------------------
        public DataTablesNotificationContract ShowFilterWarningnew(int draw, int start, int length, string be_id_warning, string contract_no_warning, string contract_type_warning, string customer_id_warning, string date_start_warning, string date_end_warning, string KodeCabang)
        {
            string x_be_id_warning = string.IsNullOrEmpty(be_id_warning) ? "x" : be_id_warning;
            string x_contract_no_warning = string.IsNullOrEmpty(contract_no_warning) ? "x" : contract_no_warning;
            string x_contract_type_warning = string.IsNullOrEmpty(contract_type_warning) ? "x" : contract_type_warning;
            string x_customer_id_warning = string.IsNullOrEmpty(customer_id_warning) ? "x" : customer_id_warning;
            string x_date_start_warning = string.IsNullOrEmpty(date_start_warning) ? "x" : date_start_warning;
            string x_date_end_warning = string.IsNullOrEmpty(date_end_warning) ? "x" : date_end_warning;

            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {

                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "NVL(TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR'), '-') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR'))) " + v_be;

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id_warning,
                            x_contract_no = x_contract_no_warning,
                            x_contract_type = x_contract_type_warning,
                            x_customer_id = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                        
            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //-------------------- NEW FILTER HISTORY -------------------
        public DataTablesNotificationContract ShowFilterHistorynew(int draw, int start, int length, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                            "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                            "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION " +
                            "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL " + v_be;

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                }
                catch (Exception)
                {

                }
                       
            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //---------------------- NEW FILTER HISTORY WARNING -------------------
        public DataTablesNotificationContract ShowFilterWarningHistorynew(int draw, int start, int length, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesNotificationContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL " + v_be;

                    fullSql = fullSql.Replace("sql", sql);

                    listDataFilterDetail = connection.Query<AUP_NOTIFICATION_CONTRACT>(fullSql, new { a = start, b = end, d = KodeCabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                }
                catch (Exception)
                {

                }
            
            
            result = new DataTablesNotificationContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //--------------------- NEW EXCEL NOTIF --------------------------------
        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelNotificationnew(string be_id, string contract_no, string contract_type, string customer_id, string date_start, string date_end, string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_date_start = string.IsNullOrEmpty(date_start) ? "x" : date_start;
            string x_date_end = string.IsNullOrEmpty(date_end) ? "x" : date_end;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                if (x_be_id.Length >= 0 && x_contract_no.Length >= 0 && x_contract_type.Length >= 0 && x_customer_id.Length >= 0 && x_date_start.Length >= 0 && x_date_end.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, " +
                                "CASE " +
                                "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' " +
                                "END AS STATUS " +
                                "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NULL " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no,'x',CONTRACT_NO,:x_contract_no) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type,'x',CONTRACT_TYPE,:x_contract_type) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id,'x',CUSTOMER_ID,:x_customer_id) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end,'DD/MM/RRRR'))) " + v_be;

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_contract_no = x_contract_no,
                            x_contract_type = x_contract_type,
                            x_customer_id = x_customer_id,
                            x_date_start = x_date_start,
                            x_date_end = x_date_end,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            
            connection.Close();
            return listData;

        }

        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelNotificationHistorynew(string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO,CONTRACT_TYPE_DESC, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_NOTIFICATION_DATE,'DD.MM.RRRR') STOP_NOTIFICATION_DATE, MEMO_NOTIFICATION, " +
                        "CASE " +
                        "WHEN STOP_NOTIFICATION_DATE IS NULL THEN '' " +
                        "ELSE 'STOPPED' " +
                        "END AS STATUS " +
                        "FROM V_NOTIF_CONTRACT WHERE STOP_NOTIFICATION_DATE IS NOT NULL "+ v_be;

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { d = KodeCabang });
                }
                catch (Exception)
                {
                    listData = null;
                }
            
            connection.Close();
            return listData;

        }

        //-------------------- NEW EXCEL WARNING -----------------------------
        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelWarningnew(string be_id_warning, string contract_no_warning, string contract_type_warning, string customer_id_warning, string date_start_warning, string date_end_warning, string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id_warning = string.IsNullOrEmpty(be_id_warning) ? "x" : be_id_warning;
            string x_contract_no_warning = string.IsNullOrEmpty(contract_no_warning) ? "x" : contract_no_warning;
            string x_contract_type_warning = string.IsNullOrEmpty(contract_type_warning) ? "x" : contract_type_warning;
            string x_customer_id_warning = string.IsNullOrEmpty(customer_id_warning) ? "x" : customer_id_warning;
            string x_date_start_warning = string.IsNullOrEmpty(date_start_warning) ? "x" : date_start_warning;
            string x_date_end_warning = string.IsNullOrEmpty(date_end_warning) ? "x" : date_end_warning;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

                if (x_be_id_warning.Length >= 0 && x_contract_no_warning.Length >= 0 && x_contract_type_warning.Length >= 0 && x_customer_id_warning.Length >= 0 && x_date_start_warning.Length >= 0 && x_date_end_warning.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                                "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                                "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING " +
                                "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NULL " + v_be +
                                "AND BE_ID = DECODE(:x_be_id_warning,'x',BE_ID,:x_be_id_warning) " +
                                "AND CONTRACT_NO = DECODE (:x_contract_no_warning,'x',CONTRACT_NO,:x_contract_no_warning) " +
                                "AND CONTRACT_TYPE = DECODE (:x_contract_type_warning,'x',CONTRACT_TYPE,:x_contract_type_warning) " +
                                "AND CUSTOMER_ID = DECODE (:x_customer_id_warning,'x',CUSTOMER_ID,:x_customer_id_warning) " +
                                "AND (CONTRACT_END_DATE BETWEEN DECODE(:x_date_start_warning, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_date_start_warning,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_date_end_warning,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_date_end_warning,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new
                        {
                            x_be_id_warning = x_be_id_warning,
                            x_contract_no_warning = x_contract_no_warning,
                            x_contract_type_warning = x_contract_type_warning,
                            x_customer_id_warning = x_customer_id_warning,
                            x_date_start_warning = x_date_start_warning,
                            x_date_end_warning = x_date_end_warning,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            
            connection.Close();
            return listData;

        }

        public IEnumerable<AUP_NOTIFICATION_CONTRACT> GenerateExcelWarningHistorynew(string KodeCabang)
        {
            IEnumerable<AUP_NOTIFICATION_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            try
                {
                    string sql = "SELECT TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') CONTRACT_END_DATE, '+' || WARNING_DAYS || ' DAYS' DAYS, CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, CONTRACT_NO, PHONE, " +
                        "EMAIL, CONTRACT_TYPE, LEGAL_CONTRACT_NO, CONTRACT_NAME, OBJECT_ID, OBJECT_NAME, RO_ADDRESS, BE_ID, BE_NAME, TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') CONTRACT_START_DATE, " +
                        "TO_CHAR(STOP_WARNING_DATE,'DD.MM.RRRR') STOP_WARNING_DATE, CONTRACT_TYPE_DESC, MEMO_WARNING, " +
                        "CASE " +
                        "WHEN STOP_WARNING_DATE IS NULL THEN '' " +
                        "ELSE 'STOPPED' " +
                        "END AS STATUS " +
                        "FROM V_WARNING_CONTRACT WHERE STOP_WARNING_DATE IS NOT NULL " + v_be;

                    listData = connection.Query<AUP_NOTIFICATION_CONTRACT>(sql, new { d = KodeCabang });
                }
                catch (Exception)
                {
                    listData = null;
                }
            
            connection.Close();
            return listData;

        }
    }
}