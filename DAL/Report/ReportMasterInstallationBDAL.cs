﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;

namespace Remote.Controllers
{
    internal class ReportMasterInstallationBDAL
    {
        public DataTableReportMasterInstallation ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT s.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                 "INSTALLATION_NUMBER, s.ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, " +
                                 "BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                 "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                { }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //-------------------------------- deprecated ------------------------------
        public DataTableReportMasterInstallation GetDataFilter(int draw, int start, int length, string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string search, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT PROFIT_CENTER_ID, INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status)";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });
                    }
                    catch (Exception)
                    { }
                }else
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) AND BRANCH_ID=:d";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
                    
                }
            }
            else
            {
                
            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------------- DEPRECATED ----------------------------------
        public IEnumerable<AUP_REPORT_MASTER_INSTALLATION> ShowFilterTwo(string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                    "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                    "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                    "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                    "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                    "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                    "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                    "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                    "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status)";

                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status) AND s.BRANCH_ID=:a";*/
                        string sql = "SELECT INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) AND BRANCH_ID=:a";


                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            a = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id ASC";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }


            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 " + v_be + " order by profit_center_id ASC";

                    listData = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang }).ToList();
                }
                catch (Exception ex) { string aaa = ex.ToString(); }

            connection.Close();
            return listData;
        }


        //-------------------- DEPRECATED ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT s.CUSTOMER_ID code, s.CUSTOMER_ID || ' - ' || s.CUSTOMER_NAME label, CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                        "AND ((UPPER(s.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (s.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT s.CUSTOMER_ID code, s.CUSTOMER_ID || ' - ' || s.CUSTOMER_NAME label, CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                        "AND ((UPPER(s.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (s.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND s.BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        //------------------- AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomernew(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE("s");

            
                str = "SELECT DISTINCT s.CUSTOMER_ID code, s.CUSTOMER_ID || ' - ' || s.CUSTOMER_NAME label, CUSTOMER_NAME name FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                        "AND ((UPPER(s.CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (s.CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) " + v_be;
            

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str, new { d = KodeCabang }).ToList();

            connection.Close();
            return exec;
        }


        //--------------------------- new filter -------------------------------
        public DataTableReportMasterInstallation GetDataFilternew(int draw, int start, int length, string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string search, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            int count = 0;
            int end = start + length;
            DataTableReportMasterInstallation result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string where = "";
            where += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {
                
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status)";*/
                        string sql = "SELECT INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE " +
                                     " INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) " + where + v_be;
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    { string aaa = e.ToString(); }

                
            }
            else
            {

            }

            result = new DataTableReportMasterInstallation();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //--------------------------- NEW EXCEL -----------------------------
        public IEnumerable<AUP_REPORT_MASTER_INSTALLATION> ShowFilterTwonew(string profit_center, string installation_code, string customer_mdm, string customer_sap, string status, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer_mdm = string.IsNullOrEmpty(customer_mdm) ? "x" : customer_mdm;
            string x_customer_sap = string.IsNullOrEmpty(customer_sap) ? "x" : customer_sap;
            string x_status = string.IsNullOrEmpty(status) ? "x" : status;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string where = "";
            where += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");

            if (x_profit_center.Length >= 0 && x_installation_code.Length >= 0 && x_customer_mdm.Length >= 0 && x_customer_sap.Length >= 0 && x_status.Length >= 0)
            {                
                    try
                    {
                        /*string sql = "SELECT c.INSTALLATION_TYPE, s.BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, s.TARIFF_CODE, MINIMUM_AMOUNT, s.CURRENCY, TAX_CODE, s.STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, s.CUSTOMER_ID || ' - ' || CUSTOMER_NAME as customer_name, CUSTOMER_SAP_AR, s.INSTALLATION_CODE, profit_center_id || ' - ' || terminal_name AS nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, c.AMOUNT, s.BRANCH_ID, BIAYA_ADMIN, " +
                                     "to_char(INSTALLATION_DATE, 'DD/MM/YYYY') AS INSTALLATION_DATE " +
                                     "FROM PROP_SERVICES_INSTALLATION s, profit_center p, SERVICES c WHERE s.profit_center = p.profit_center_id(+) AND s.TARIFF_CODE = c.TARIFF_CODE and c.INSTALLATION_TYPE = 'A-Sambungan Air' " +
                                     "and PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                     "and s.INSTALLATION_CODE = DECODE(:x_installation_code, 'x', s.INSTALLATION_CODE,:x_installation_code) " +
                                     "and s.CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', s.CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and s.STATUS = DECODE(:x_status, 'x', s.STATUS,:x_status) AND s.BRANCH_ID=:a";*/
                        string sql = "SELECT INSTALLATION_TYPE, BE_ID, CUSTOMER_ID, INSTALLATION_ADDRESS, TARIFF_CODE, MINIMUM_AMOUNT, CURRENCY, TAX_CODE, STATUS, RO_CODE, POWER_CAPACITY, " +
                                     "INSTALLATION_NUMBER, customer_name, CUSTOMER_SAP_AR, INSTALLATION_CODE, nama_profit_center, " +
                                     "RO_NUMBER, MINIMUM_PAYMENT, MULTIPLY_FACT, BIAYA_BEBAN, MINIMUM_USED, AMOUNT, BRANCH_ID, BIAYA_ADMIN, " +
                                     "INSTALLATION_DATE " +
                                     "FROM V_REPORT_WATER_INSTALLATIONB " +
                                     "WHERE  " +
                                     " INSTALLATION_CODE = DECODE(:x_installation_code, 'x', INSTALLATION_CODE,:x_installation_code) " +
                                     "and CUSTOMER_ID = DECODE(:x_customer_mdm, 'x', CUSTOMER_ID,:x_customer_mdm) and CUSTOMER_SAP_AR = DECODE(:x_customer_sap, 'x',CUSTOMER_SAP_AR, :x_customer_sap) " +
                                     "and STATUS = DECODE(:x_status, 'x', STATUS,:x_status) " + where + v_be;


                        listData = connection.Query<AUP_REPORT_MASTER_INSTALLATION>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_installation_code = x_installation_code,
                            x_customer_mdm = x_customer_mdm,
                            x_customer_sap = x_customer_sap,
                            x_status = x_status,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                

            }

            connection.Close();
            return listData;

        }


    }
}