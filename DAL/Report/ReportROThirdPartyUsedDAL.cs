﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.DAL;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ReportROThirdPartyUsed;

namespace Remote.DAL
{
    public class ReportROThirdPartyUsedDAL
    {
        //------------- deprecated ---------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        //------------------ new get data BE -------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
                       
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";

                    listData = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang }).ToList();
                }
                catch (Exception) { }

            connection.Close();
            return listData;
        }


        public IEnumerable<DDZoneRip> GetDataUsageType()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listData = null;

            try
            {
                string sql = "SELECT A.REF_DATA, A.REF_DESC FROM PROP_PARAMETER_REF_D A WHERE A.REF_CODE = 'USAGE_TYPE_RO'";
                listData = connection.Query<DDZoneRip>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDZoneRip> GetDataZONE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listData = null;

            try
            {
                string sql = "SELECT A.REF_DATA, A.REF_DESC FROM PROP_PARAMETER_REF_D A WHERE A.REF_CODE = 'ZONA_RIP'";
                listData = connection.Query<DDZoneRip>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listData = null;

            try
            {
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D A WHERE A.REF_CODE = 'CONTRACT_USAGE'";
                listData = connection.Query<DDContractUsage>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();

            return listData;
        }

        //---------------------- DEPRECATED -------------------------------
        public DataTablesReportROThirdPartyUsed GetFilterTwo(int draw, int start, int length, string be_id, string val_years, string usage_type, string val_zone, string val_contract_usage, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_contract_usage = string.IsNullOrEmpty(val_contract_usage) ? "x" : val_contract_usage;

            int count = 0;
            int end = start + length;
            DataTablesReportROThirdPartyUsed result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)";

                            fullSql = fullSql.Replace("sql", sql);
                            listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_contract_usage = x_val_contract_usage
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_contract_usage = x_val_contract_usage
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" +
                                        "AND ((UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(KELOMPOK_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(RO_CERTIFICATE_NUMBER) LIKE '%' ||:c|| '%') OR (UPPER(LEGAL_CONTRACT_NO) LIKE '%' ||:c|| '%'))";

                                fullSql = fullSql.Replace("sql", sql);

                                listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage
                                });

                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" +
                                        "AND BRANCH_ID =:d";

                            fullSql = fullSql.Replace("sql", sql);
                            listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_contract_usage = x_val_contract_usage,
                                d = KodeCabang
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_val_years = x_val_years,
                                x_usage_type = x_usage_type,
                                x_val_zone = x_val_zone,
                                x_val_contract_usage = x_val_contract_usage,
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" +
                                        "AND ((UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (UPPER(KELOMPOK_USAHA) LIKE '%' ||:c|| '%') OR (UPPER(RO_CERTIFICATE_NUMBER) LIKE '%' ||:c|| '%') OR (UPPER(LEGAL_CONTRACT_NO) LIKE '%' ||:c|| '%'))" +
                                        "AND BRANCH_ID =:d";

                                fullSql = fullSql.Replace("sql", sql);

                                listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage,
                                    d = KodeCabang
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage,
                                    d = KodeCabang
                                });

                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                }
            }

            result = new DataTablesReportROThirdPartyUsed();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //---------------------- DEPRECATED ------------------------------
        public IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> ShowFilterTwo(string be_id, string val_years, string usage_type, string val_zone, string val_contract_usage, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_contract_usage = string.IsNullOrEmpty(val_contract_usage) ? "x" : val_contract_usage;

            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)";

                        listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_contract_usage = x_val_contract_usage
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" +
                                        "AND A.BRANCH_ID =:a";

                        listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_contract_usage = x_val_contract_usage,
                            a = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        //--------------------------- new filter -----------------------
        public DataTablesReportROThirdPartyUsed GetFilterTwonew(int draw, int start, int length, string be_id, string val_years, string usage_type, string val_zone, string val_contract_usage, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_contract_usage = string.IsNullOrEmpty(val_contract_usage) ? "x" : val_contract_usage;

            int count = 0;
            int end = start + length;
            DataTablesReportROThirdPartyUsed result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(KELOMPOK_USAHA) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(RO_CERTIFICATE_NUMBER) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(LEGAL_CONTRACT_NO) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {                        
                    try
                    {
                        string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                            "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                            "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                            "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                            "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                            "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" + v_be + wheresearch;
                                        
                                fullSql = fullSql.Replace("sql", sql);

                                listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(fullSql, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage,
                                    d = KodeCabang
                                });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new
                                {
                                    a = start,
                                    b = end,
                                    c = search,
                                    x_be_id = x_be_id,
                                    x_val_years = x_val_years,
                                    x_usage_type = x_usage_type,
                                    x_val_zone = x_val_zone,
                                    x_val_contract_usage = x_val_contract_usage,
                                    d = KodeCabang
                                });

                    }
                    catch (Exception)
                    {

                    }                                            
                }
                       

            result = new DataTablesReportROThirdPartyUsed();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //------------------------- NEW EXCEL ---------------------------
        public IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> ShowFilterTwonew(string be_id, string val_years, string usage_type, string val_zone, string val_contract_usage, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_val_years = string.IsNullOrEmpty(val_years) ? "x" : val_years;
            string x_usage_type = string.IsNullOrEmpty(usage_type) ? "x" : usage_type;
            string x_val_zone = string.IsNullOrEmpty(val_zone) ? "x" : val_zone;
            string x_val_contract_usage = string.IsNullOrEmpty(val_contract_usage) ? "x" : val_contract_usage;
            
                if (x_be_id.Length >= 0 && x_val_years.Length >= 0 && x_usage_type.Length >= 0 && x_val_zone.Length >= 0 && x_val_contract_usage.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT * FROM V_REPORT_PIHAKTIGA_PENDAPATAN " +
                                        "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                                        "AND VALID_FROM = DECODE(:x_val_years, 'x', VALID_FROM,:x_val_years) " +
                                        "AND USAGE_TYPE = DECODE(:x_usage_type,'x',USAGE_TYPE,:x_usage_type) " +
                                        "AND ZONA_RIP = DECODE(:x_val_zone,'x',ZONA_RIP,:x_val_zone) " +
                                        "AND CONTRACT_USAGE = DECODE(:x_val_contract_usage,'x',CONTRACT_USAGE,:x_val_contract_usage)" +
                                        "AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";

                        listData = connection.Query<AUP_REPORT_RO_THIRD_PARTY_USED>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_val_years = x_val_years,
                            x_usage_type = x_usage_type,
                            x_val_zone = x_val_zone,
                            x_val_contract_usage = x_val_contract_usage,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            
            connection.Close();
            return listData;

        }

    }
}