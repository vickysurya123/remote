﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportOtherService;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels; 
using Remote.Models.MasterBusinessEntity;
using Remote.DAL;
using Remote.Helpers;

namespace Remote.Controllers 
{
    internal class ReportBEDAL
    {
        public IEnumerable<DDBusinessEntity> GetDataBE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                listData = connection.Query<DDBusinessEntity>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
        public IEnumerable<DDHarbourClass> GetDataHarbourClass()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDHarbourClass> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT HARBOUR_CLASS HB_ID, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS FROM BUSINESS_ENTITY";

                listData = connection.Query<DDHarbourClass>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
        //public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        //{
        //    string sql = "";

        //    if (KodeCabang == "0")
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
        //    }
        //    else
        //    {
        //        sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
        //    }

        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
        //    connection.Close();
        //    return listDetil;
        //}
        public IEnumerable<VW_BUSINESS_ENTITY> getBEID(string xPARAM)
        {
            VW_BUSINESS_ENTITY result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<VW_BUSINESS_ENTITY> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, BE_ADDRESS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_BUSINESS_ENTITY>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }
        public IEnumerable<AUP_BE_WEBAPPS> getHARBOURClASS(string xPARAM)
        {
            AUP_BE_WEBAPPS result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<AUP_BE_WEBAPPS> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT BE_ID id, BE_ID, BE_NAME, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE HARBOUR_CLASS = ID) HARBOUR_CLASS, BE_CITY FROM BUSINESS_ENTITY " +
                             "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<AUP_BE_WEBAPPS>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }
        public DataTableReportBE ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTableReportBE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_BE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT B.BE_ID || ' - ' || B.BE_NAME AS BE, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, b.BE_ADDRESS, " +
                                     "(SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID, b.POSTAL_CODE, " +
                                     "d.amount, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, phone_1 || '  ' || PHONE_2 as phone, fax_1 || '   ' || FAX_2 as fax, c.EMAIL " +
                                     "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c, BUSINESS_ENTITY_DETAIL d WHERE c.BE_ID = b.BE_ID(+) and c.id = d.id ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                { }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTableReportBE();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<AUP_REPORT_BE> ShowAllTwo(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT B.BE_ID || ' - ' || B.BE_NAME AS BE, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, b.BE_ADDRESS, " +
                                     "(SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID, b.POSTAL_CODE, " +
                                     "d.amount, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, phone_1 || '  ' || PHONE_2 as phone, fax_1 || '   ' || FAX_2 as fax, c.EMAIL " +
                                     "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c, BUSINESS_ENTITY_DETAIL d WHERE c.BE_ID = b.BE_ID(+) and c.id = d.id ";

                listData = connection.Query<AUP_REPORT_BE>(sql, new
                {
                    c = KodeCabang
                });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            return listData;

        }
        
        //---------------------------- DEPRECATED ------------------------------------
        public DataTableReportBE GetDataFilter(int draw, int start, int length, string be, string harbour_class, string search, string KodeCabang)
        {
            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_harbour_class = string.IsNullOrEmpty(harbour_class) ? "x" : harbour_class;

            int count = 0;
            int end = start + length;
            DataTableReportBE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_BE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (KodeCabang == "0")
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT BE, BE_CITY, BE_ID, BE_PROVINCE, BE_ADDRESS, " +
                                        "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                        "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                        "FROM V_REPORT_BE " +
                                        "WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) ";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class
                            });
                        }
                        catch (Exception e)
                        {
                            string aaa = e.ToString();
                        }
                    }
                }
                else
                {

                }
            }
            else
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT BE, BE_CITY, BE_ID, BE_PROVINCE, BE_ADDRESS, " +
                                        "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                        "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                        "FROM V_REPORT_BE " +
                                        "WHERE BRANCH_ID = :d AND BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) and branch_id=:d ";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class,
                                d = KodeCabang
                            });
                        }
                        catch (Exception e)
                        { string aaa = e.ToString(); }
                    }
                }
                else
                {

                }    

            }

            result = new DataTableReportBE();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //--------------------------- DEPRECATED -------------------------------------
        public IEnumerable<AUP_REPORT_BE> GetDataFilterTwo(string be, string harbour_class, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_harbour_class = string.IsNullOrEmpty(harbour_class) ? "x" : harbour_class;
                        
            if (KodeCabang == "0")
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE, BE_CITY, BE_PROVINCE, BE_ADDRESS, " +
                                     "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                     "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                     "FROM V_REPORT_BE WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) ";


                        listData = connection.Query<AUP_REPORT_BE>(sql, new
                        {
                            x_be = x_be,
                            x_harbour_class = x_harbour_class
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {

                }
            }
            else
            {
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE, BE_CITY, BE_PROVINCE, BE_ADDRESS, " +
                                     "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                     "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                     "FROM V_REPORT_BE WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) AND branch_id=:d ";


                        listData = connection.Query<AUP_REPORT_BE>(sql, new
                        {
                            x_be = x_be,
                            x_harbour_class = x_harbour_class,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {

                }
            }

            connection.Close();
            return listData;

        }

        //--------------------------- NEW GET DATA ---------------------------------
        public DataTableReportBE GetDataFilternew(int draw, int start, int length, string be, string harbour_class, string search, string KodeCabang)
        {
            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_harbour_class = string.IsNullOrEmpty(harbour_class) ? "x" : harbour_class;

            int count = 0;
            int end = start + length;
            DataTableReportBE result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_BE> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
             
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    
                        try
                        {
                            string sql = "SELECT BE, BE_CITY, BE_ID, BE_PROVINCE, BE_ADDRESS, " +
                                        "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                        "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                        "FROM V_REPORT_BE " +
                                        "WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) " + v_be;

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_REPORT_BE>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_be = x_be,
                                x_harbour_class = x_harbour_class,
                                d = KodeCabang
                            });
                        }
                        catch (Exception e)
                        { string aaa = e.ToString(); }
                    
                }
                else
                {

                }
                            
            result = new DataTableReportBE();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //--------------------------- NEW EXCEL ---------------------------------
        public IEnumerable<AUP_REPORT_BE> GetDataFilterTwonew(string be, string harbour_class, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be = string.IsNullOrEmpty(be) ? "x" : be;
            string x_harbour_class = string.IsNullOrEmpty(harbour_class) ? "x" : harbour_class;

            //string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
                      
            
                if (x_be.Length >= 0 && x_harbour_class.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE, BE_CITY, BE_PROVINCE, BE_ADDRESS, " +
                                     "HARBOUR_CLASS, HB_ID, BE_PROVINCE_ID, POSTAL_CODE, " +
                                     "amount, VAL_FROM, VAL_TO, phone, fax, EMAIL " +
                                     "FROM V_REPORT_BE WHERE BE_ID = DECODE(:x_be, 'x', BE_ID,:x_be) AND HB_ID = DECODE(:x_harbour_class,'x', HB_ID, :x_harbour_class) " + v_be;


                        listData = connection.Query<AUP_REPORT_BE>(sql, new
                        {
                            x_be = x_be,
                            x_harbour_class = x_harbour_class,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {

                }
            

            connection.Close();
            return listData;

        }


    }
}