﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Controllers;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;

namespace Remote.DAL
{
    public class ReportRentalRequestDAL
    {
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDRentalType> GetDataRentalRequestType(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRentalType> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC DESCRIPTION FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RENTAL_REQUEST_TYPE'";

                listData = connection.Query<DDRentalType>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT CUSTOMER_ID code, CUSTOMER_ID || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name FROM PROP_RENTAL_REQUEST " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT CUSTOMER_ID code, CUSTOMER_ID || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM PROP_RENTAL_REQUEST " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }
        
        public DataTableReportRentalRequest GetDataFilters(int draw, int start, int length, string be_id, string profit_center, string customer_id, string active_status, string rental_request_status, string rental_request_type, string rental_request_no, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_active_status = string.IsNullOrEmpty(active_status) ? "x" : active_status;
            string x_rental_request_status = string.IsNullOrEmpty(rental_request_status) ? "x" : rental_request_status;
            string x_rental_request_type = string.IsNullOrEmpty(rental_request_type) ? "x" : rental_request_type;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;

            int count = 0;
            int end = start + length;
            DataTableReportRentalRequest result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RENTAL_REQUEST> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && x_active_status.Length >= 0 && x_rental_request_status.Length >= 0 && x_rental_request_type.Length >= 0 && x_rental_request_no.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                    "USAGE_TYPE, " +
                                    "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM V_REPORT_RENTAL_REQUEST " +
                                    "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                    "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no)";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RENTAL_REQUEST>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no
                        });
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    try
                    {
                        string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                    "USAGE_TYPE, " +
                                    "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM V_REPORT_RENTAL_REQUEST " +
                                    "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                    "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) and branch_id=:d";
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RENTAL_REQUEST>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    { string aaa = e.ToString(); }

                }
            }
            else
            {

            }

            result = new DataTableReportRentalRequest();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        public IEnumerable<AUP_REPORT_RENTAL_REQUEST> ShowFilterTwo(string be_id, string profit_center, string customer_id, string active_status, string rental_request_status, string rental_request_type, string rental_request_no, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RENTAL_REQUEST> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_active_status = string.IsNullOrEmpty(active_status) ? "x" : active_status;
            string x_rental_request_status = string.IsNullOrEmpty(rental_request_status) ? "x" : rental_request_status;
            string x_rental_request_type = string.IsNullOrEmpty(rental_request_type) ? "x" : rental_request_type;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_active_status.Length >= 0 && x_rental_request_status.Length >= 0 && x_rental_request_type.Length >= 0 && x_rental_request_no.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        /*string sql = "SELECT r.RENTAL_REQUEST_NO, r.RENTAL_REQUEST_TYPE, (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.RENTAL_REQUEST_TYPE) REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE, 'DD/MM/RRRR') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.CONTRACT_USAGE) USAGE_TYPE, " +
                                    "CUSTOMER_ID || ' - ' ||CUSTOMER_NAME CUSTOMER, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL i, RENTAL_OBJECT t " +
                                    "WHERE r.RENTAL_REQUEST_NO = i.RENTAL_REQUEST_NO AND i.RO_NUMBER = t.RO_NUMBER AND " +
                                    "r.BE_ID = DECODE(:x_be_id, 'x', r.BE_ID,:x_be_id)  AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', t.PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND r.ACTIVE = DECODE(:x_active_status, 'x', r.ACTIVE,:x_active_status) AND r.STATUS = DECODE(:x_rental_request_status, 'x', r.STATUS,:x_rental_request_status) " +
                                    "AND r.RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', r.RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND r.RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', r.RENTAL_REQUEST_NO,:x_rental_request_no)";*/
                        string sql= "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                    "USAGE_TYPE, " +
                                    "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM V_REPORT_RENTAL_REQUEST " +
                                    "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                    "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no)";
                        listData = connection.Query<AUP_REPORT_RENTAL_REQUEST>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        /*string sql = "SELECT r.RENTAL_REQUEST_NO, r.RENTAL_REQUEST_TYPE, (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.RENTAL_REQUEST_TYPE) REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE, 'DD/MM/RRRR') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.CONTRACT_USAGE) USAGE_TYPE, " +
                                    "CUSTOMER_ID || ' - ' ||CUSTOMER_NAME CUSTOMER, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL i, RENTAL_OBJECT t " +
                                    "WHERE r.RENTAL_REQUEST_NO = i.RENTAL_REQUEST_NO AND i.RO_NUMBER = t.RO_NUMBER AND " +
                                    "r.BE_ID = DECODE(:x_be_id, 'x', r.BE_ID,:x_be_id)  AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', t.PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND r.ACTIVE = DECODE(:x_active_status, 'x', r.ACTIVE,:x_active_status) AND r.STATUS = DECODE(:x_rental_request_status, 'x', r.STATUS,:x_rental_request_status) " +
                                    "AND r.RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', r.RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND r.RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', r.RENTAL_REQUEST_NO,:x_rental_request_no) AND r.BRANCH_ID=:d";*/
                       string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                    "USAGE_TYPE, " +
                                    "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM V_REPORT_RENTAL_REQUEST " +
                                    "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id) AND PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                    "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) AND r.BRANCH_ID=:d";

                        listData = connection.Query<AUP_REPORT_RENTAL_REQUEST>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        //---------------------------- NEW GET DATA FILTER --------------------------------------
        public DataTableReportRentalRequest GetDataFiltersnew(int draw, int start, int length, string be_id, string profit_center, string customer_id, string active_status, string rental_request_status, string rental_request_type, string rental_request_no, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_active_status = string.IsNullOrEmpty(active_status) ? "x" : active_status;
            string x_rental_request_status = string.IsNullOrEmpty(rental_request_status) ? "x" : rental_request_status;
            string x_rental_request_type = string.IsNullOrEmpty(rental_request_type) ? "x" : rental_request_type;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;

            int count = 0;
            int end = start + length;
            DataTableReportRentalRequest result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_RENTAL_REQUEST> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string wheresearch = (search.Length >= 2 ? " AND ((UPPER(RENTAL_REQUEST_NO) LIKE '%' || '" + search.ToUpper() + "' || '%') or (UPPER(RENTAL_REQUEST_TYPE) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");

            string where1 = "";
            where1 += (!string.IsNullOrEmpty(profit_center) ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(be_id) ? "" : " and BE_ID = " + be_id;
            where1 += v_be;

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && x_active_status.Length >= 0 && x_rental_request_status.Length >= 0 && x_rental_request_type.Length >= 0 && x_rental_request_no.Length >= 0)
            {
                    try
                    {
                        string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                    "USAGE_TYPE, " +
                                    "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM V_REPORT_RENTAL_REQUEST " +
                                    "WHERE " +
                                    " CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                    "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) " + where1 + wheresearch;
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_RENTAL_REQUEST>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    { string aaa = e.ToString(); }                                
            }
            else
            {

            }

            result = new DataTableReportRentalRequest();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //---------------------------- NEW EXCEL --------------------------------
        public IEnumerable<AUP_REPORT_RENTAL_REQUEST> ShowFilterTwonew(string be_id, string profit_center, string customer_id, string active_status, string rental_request_status, string rental_request_type, string rental_request_no, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_RENTAL_REQUEST> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_active_status = string.IsNullOrEmpty(active_status) ? "x" : active_status;
            string x_rental_request_status = string.IsNullOrEmpty(rental_request_status) ? "x" : rental_request_status;
            string x_rental_request_type = string.IsNullOrEmpty(rental_request_type) ? "x" : rental_request_type;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            where1 += (!string.IsNullOrEmpty(profit_center) ? " and PROFIT_CENTER_ID_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(be_id) ? "" : " and BE_ID = " + be_id;
            where1 += v_be;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_active_status.Length >= 0 && x_rental_request_status.Length >= 0 && x_rental_request_type.Length >= 0 && x_rental_request_no.Length >= 0)
            {                
                    try
                    {
                        /*string sql = "SELECT r.RENTAL_REQUEST_NO, r.RENTAL_REQUEST_TYPE, (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.RENTAL_REQUEST_TYPE) REQUEST_TYPE, " +
                                    "RENTAL_REQUEST_NAME, to_char(CONTRACT_START_DATE, 'DD/MM/RRRR') CONTRACT_START_DATE, to_char(CONTRACT_END_DATE,'DD/MM/RRRR')CONTRACT_END_DATE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE ID=r.CONTRACT_USAGE) USAGE_TYPE, " +
                                    "CUSTOMER_ID || ' - ' ||CUSTOMER_NAME CUSTOMER, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                    "FROM PROP_RENTAL_REQUEST r, PROP_RENTAL_REQUEST_DETIL i, RENTAL_OBJECT t " +
                                    "WHERE r.RENTAL_REQUEST_NO = i.RENTAL_REQUEST_NO AND i.RO_NUMBER = t.RO_NUMBER AND " +
                                    "r.BE_ID = DECODE(:x_be_id, 'x', r.BE_ID,:x_be_id)  AND t.PROFIT_CENTER_ID = DECODE(:x_profit_center, 'x', t.PROFIT_CENTER_ID,:x_profit_center) " +
                                    "AND CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                    "AND r.ACTIVE = DECODE(:x_active_status, 'x', r.ACTIVE,:x_active_status) AND r.STATUS = DECODE(:x_rental_request_status, 'x', r.STATUS,:x_rental_request_status) " +
                                    "AND r.RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', r.RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                    "AND r.RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', r.RENTAL_REQUEST_NO,:x_rental_request_no) AND r.BRANCH_ID=:d";*/
                        string sql = "SELECT RENTAL_REQUEST_NO, RENTAL_REQUEST_TYPE, REQUEST_TYPE, " +
                                     "RENTAL_REQUEST_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, " +
                                     "USAGE_TYPE, " +
                                     "CUSTOMER, CUSTOMER_ID, CUSTOMER_AR, OLD_CONTRACT, STATUS, OBJECT_ID, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY " +
                                     "FROM V_REPORT_RENTAL_REQUEST " +
                                     "WHERE  " +
                                     " CUSTOMER_ID = DECODE(:x_customer_id, 'x', CUSTOMER_ID,:x_customer_id) " +
                                     "AND ACTIVE = DECODE(:x_active_status, 'x', ACTIVE,:x_active_status) AND STATUS = DECODE(:x_rental_request_status, 'x', STATUS,:x_rental_request_status) " +
                                     "AND RENTAL_REQUEST_TYPE = DECODE(:x_rental_request_type, 'x', RENTAL_REQUEST_TYPE,:x_rental_request_type) " +
                                     "AND RENTAL_REQUEST_NO = DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) " + where1;

                        listData = connection.Query<AUP_REPORT_RENTAL_REQUEST>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_active_status = x_active_status,
                            x_rental_request_status = x_rental_request_status,
                            x_rental_request_type = x_rental_request_type,
                            x_rental_request_no = x_rental_request_no,
                            d = KodeCabang
                        });
                    }
                    catch (Exception ex)
                    {
                        listData = null;
                    }                

            }

            connection.Close();
            return listData;

        }

    }
}