﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ReportGeneralContract;


namespace Remote.DAL
{
    public class ReportGeneralContractDAL
    {
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }


            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER_NAME name FROM PROP_CONTRACT " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER name, BRANCH_ID FROM PROP_RENTAL_REQUEST " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        public IEnumerable<DDContractType> GetDataContractTypeTwo(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractType> listData = null;

            try
            {
                string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONTRACT' AND REF_DATA != 'ZC06'";

                listData = connection.Query<DDContractType>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        //--------------------------------- deprecated ----------------------
        public DataTablesReportGeneralContract ShowFilter(int draw, int start, int length, string be_id, string profit_center, string customer_id, string contract_type, string contract_no, string contract_offer_no, string rental_request_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTablesReportGeneralContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_GENERAL_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, "+ 
                        "CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, "+
                        "OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, "+
                        "PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID "+
                        "FROM V_REPORT_GENERAL_CONTRACT "+
                        "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) "+
                        "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) "+
                        "AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) "+
                        "AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) "+
                        "AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) "+
                        "AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) "+
                        "AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) "+
                        "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) "+
                        "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, " +
                        "CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, " +
                        "OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, " +
                        "PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID " +
                        "FROM V_REPORT_GENERAL_CONTRACT WHERE BRANCH_ID = :d " +
                        "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                        "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                        "AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                        "AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) " +
                        "AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) " +
                        "AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) " +
                        "AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }

                }
            }

            result = new DataTablesReportGeneralContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }


        public IEnumerable<AUP_REPORT_GENERAL_CONTRACT> GenerateExcel(string be_id, string profit_center, string customer_id, string contract_type, string contract_no, string contract_offer_no, string rental_request_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_GENERAL_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            if (KodeCabang == "0") {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0) {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, " +
                        "CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, " +
                        "OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, " +
                        "PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID " +
                        "FROM V_REPORT_GENERAL_CONTRACT " +
                        "WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                        "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                        "AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                        "AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) " +
                        "AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) " +
                        "AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) " +
                        "AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to
                        });
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
            }
            else {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0) {
                    try
                    {
                        string sql = "SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, " +
                        "CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, " +
                        "OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, " +
                        "PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID " +
                        "FROM V_REPORT_GENERAL_CONTRACT WHERE BRANCH_ID = :d " +
                        "AND BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) " +
                        "AND CONTRACT_NO= DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) " +
                        "AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) " +
                        "AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) " +
                        "AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) " +
                        "AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) " +
                        "AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))";

                        listData = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        //----------------------- new filter ----------------------------
        public DataTablesReportGeneralContract ShowFilternew(int draw, int start, int length, string be_id, string profit_center, string customer_id, string contract_type, string contract_no, string contract_offer_no, string rental_request_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            int count = 0;
            int end = start + length;
            DataTablesReportGeneralContract result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_GENERAL_CONTRACT> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string filter = "";
            if (!string.IsNullOrEmpty(contract_no))
            {
                filter = " AND CONTRACT_NO = DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) ";
            }


                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = @"SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, 
                        CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, 
                        OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, 
                        PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID 
                        FROM V_REPORT_GENERAL_CONTRACT 
                        WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) 
                        " + filter + @" AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) 
                        AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) 
                        AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) 
                        AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) 
                        AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) 
                        AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) 
                        AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR'))) " + v_be;

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            //perubahan luas bangunan dan luas lahan menjadi 0 apabila ada kesamaan nomor contract
            string temp = "";
            foreach(var item in listDataFilterDetail)
            {
                if(temp == item.CONTRACT_NO)
                {
                    item.LUAS_BANGUNAN = "0";
                    item.LUAS_TANAH = "0";
                }
                temp = item.CONTRACT_NO;
            }
                
            result = new DataTablesReportGeneralContract();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //----------------------- NEW EXCEL ---------------------------
        public IEnumerable<AUP_REPORT_GENERAL_CONTRACT> GenerateExcelnew(string be_id, string profit_center, string customer_id, string contract_type, string contract_no, string contract_offer_no, string rental_request_no, string contract_status, string valid_from, string valid_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_GENERAL_CONTRACT> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_contract_type = string.IsNullOrEmpty(contract_type) ? "x" : contract_type;
            string x_contract_no = string.IsNullOrEmpty(contract_no) ? "x" : contract_no;
            string x_rental_request_no = string.IsNullOrEmpty(rental_request_no) ? "x" : rental_request_no;
            string x_contract_offer_no = string.IsNullOrEmpty(contract_offer_no) ? "x" : contract_offer_no;
            string x_contract_status = string.IsNullOrEmpty(contract_status) ? "x" : contract_status;
            string x_valid_from = string.IsNullOrEmpty(valid_from) ? "x" : valid_from;
            string x_valid_to = string.IsNullOrEmpty(valid_to) ? "x" : valid_to;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string filter = "";

            if (!string.IsNullOrEmpty(contract_no))
            {
                filter = " AND CONTRACT_NO = DECODE(:x_contract_no, 'x', CONTRACT_NO,:x_contract_no) ";
            }

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_customer_id.Length >= 0 && x_contract_type.Length >= 0 && x_contract_no.Length >= 0 && x_rental_request_no.Length >= 0 && x_contract_offer_no.Length >= 0 && x_contract_status.Length >= 0 && x_valid_from.Length >= 0 && x_valid_to.Length >= 0)
                {
                    try
                    {
                        string sql = @"SELECT CONTRACT_NO, CONTRACT_TYPE_DESC, CONTRACT_OFFER_NO, RENTAL_REQUEST_NO, CREATION_BY, CONTRACT_NAME, CONTRACT_START_DATE, 
                        CONTRACT_END_DATE, BUSINESS_PARTNER_NAME, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, IJIN_PRINSIP_NO, LEGAL_CONTRACT_NO, 
                        OBJECT_ID, OBJECT_NAME, INDUSTRY, LUAS_TANAH, LUAS_BANGUNAN, TOTAL_NET_VALUE, CONDITION_TYPE, CONTRACT_STATUS, STATUS_MIGRASI, BE_ID, 
                        PROFIT_CENTER, CONTRACT_TYPE, CREATION_DATE, BUSINESS_PARTNER, STATUS, BRANCH_ID 
                        FROM V_REPORT_GENERAL_CONTRACT 
                        WHERE BE_ID = DECODE(:x_be_id, 'x', BE_ID,:x_be_id)  AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center) 
                        " + filter + @" AND CONTRACT_OFFER_NO= DECODE(:x_contract_offer_no, 'x', CONTRACT_OFFER_NO,:x_contract_offer_no) 
                        AND RENTAL_REQUEST_NO= DECODE(:x_rental_request_no, 'x', RENTAL_REQUEST_NO,:x_rental_request_no) 
                        AND CONTRACT_TYPE= DECODE(:x_contract_type, 'x', CONTRACT_TYPE,:x_contract_type) 
                        AND STATUS = DECODE(:x_contract_status, 'x', STATUS,:x_contract_status) 
                        AND BUSINESS_PARTNER= DECODE(:x_customer_id, 'x', BUSINESS_PARTNER,:x_customer_id) 
                        AND (CREATION_DATE BETWEEN DECODE(:x_valid_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_valid_from,'DD/MM/RRRR')) 
                        AND DECODE(:x_valid_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_valid_to,'DD/MM/RRRR')))" + v_be;

                        listData = connection.Query<AUP_REPORT_GENERAL_CONTRACT>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_customer_id = x_customer_id,
                            x_contract_type = x_contract_type,
                            x_contract_no = x_contract_no,
                            x_rental_request_no = x_rental_request_no,
                            x_contract_offer_no = x_contract_offer_no,
                            x_contract_status = x_contract_status,
                            x_valid_from = x_valid_from,
                            x_valid_to = x_valid_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            
            connection.Close();
            return listData;

        }

    }
}