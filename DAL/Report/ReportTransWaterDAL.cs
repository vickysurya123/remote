﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransWater;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.Controllers
{
    class ReportTransWaterDAL
    {

        //----------------------------- BUTTON FILTER -------------------
        public DataTableReportTransWater GetDataFilter(int draw, int start, int length, string profit_center, string transaction_no, string sap_doc_no, string period, string posting_date_from, string posting_date_to, string installation_code, string customer, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_transaction_no = string.IsNullOrEmpty(transaction_no) ? "x" : transaction_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer = string.IsNullOrEmpty(customer) ? "x" : customer;

            int count = 0;
            int end = start + length;
            DataTableReportTransWater result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_WATER> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                                "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                                "FROM V_REPORT_TRANS_WATER " +
                                "WHERE PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                "AND ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                                "AND CUSTOMER = DECODE (:x_customer,'x',CUSTOMER,:x_customer) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " +
                                "ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer
                        });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            else
            {
                if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                                "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                                "FROM V_REPORT_TRANS_WATER " +
                                "WHERE PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                "AND ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                                "AND CUSTOMER = DECODE (:x_customer,'x',CUSTOMER,:x_customer) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " +
                                "AND BRANCH_ID = :c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            c = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }

            result = new DataTableReportTransWater();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //------------------------- DEPRECATED ---------------------------
        public IEnumerable<AUP_REPORT_TRANS_WATER> ShowFilterTwo(string profit_center, string transaction_no, string sap_doc_no, string period, string posting_date_from, string posting_date_to, string installation_code, string customer, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_WATER> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_transaction_no = string.IsNullOrEmpty(transaction_no) ? "x" : transaction_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer = string.IsNullOrEmpty(customer) ? "x" : customer;

            if (KodeCabang == "0")
            {
                if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                                "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                                "FROM V_REPORT_TRANS_WATER " +
                                "WHERE PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                "AND ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                                "AND CUSTOMER = DECODE (:x_customer,'x',CUSTOMER,:x_customer) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " +
                                "ORDER BY ID DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_WATER>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                                "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                                "FROM V_REPORT_TRANS_WATER " +
                                "WHERE PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                "AND ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                                "AND CUSTOMER = DECODE (:x_customer,'x',CUSTOMER,:x_customer) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " +
                                "AND BRANCH_ID = :c ORDER BY ID DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_WATER>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            c = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1 ORDER BY PROFIT_CENTER_ID ASC";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //-------------------- DEPRECATED ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                        "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        //------------------- NEW LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME, BRANCH_ID FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";
            

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE STATUS = 1 " + v_be + " ORDER BY PROFIT_CENTER_ID ASC";
            

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //-------------------- NEW AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomernew(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            str = "SELECT DISTINCT CUSTOMER code, CUSTOMER || ' - ' || CUSTOMER_NAME label, CUSTOMER_NAME name, BRANCH_ID FROM V_REPORT_TRANS_WATER " +
                  "WHERE ((UPPER(CUSTOMER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (CUSTOMER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = '" + KodeCabang + "'" + ")";

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        ////------------------------------- BUTTON SHOW ALL ---------------------
        //public DataTableReportTransWater ShowAll(int draw, int start, int length, string search, string KodeCabang)
        //{
        //    int count = 0;
        //    int end = start + length;
        //    DataTableReportTransWater result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<AUP_REPORT_TRANS_WATER> listDataOperator = null;

        //    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
        //    string fullSqlCount = "SELECT count(*) FROM (sql)";

        //    if (KodeCabang == "0")
        //    {
        //        if (search.Length == 0)
        //        {
        //            try
        //            {
        //                string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, "+
        //                        "INSTALLATION_CODE, INSTALLATION_ADDRESS,PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, "+
        //                        "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS "+
        //                        "FROM V_REPORT_TRANS_WATER";


        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new { a = start, b = end });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
        //            }
        //            catch (Exception)
        //            { }
        //        }
        //        else
        //        {
        //            // Do nothing. Not implemented yet. 
        //        }
        //    }
        //    else
        //    {
        //        if (search.Length == 0)
        //        {
        //            try
        //            {
        //                string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
        //                        "INSTALLATION_CODE, INSTALLATION_ADDRESS,PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
        //                        "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
        //                        "FROM V_REPORT_TRANS_WATER WHERE BRANCH_ID = :c";

        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new { a = start, b = end, c = KodeCabang });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
        //            }
        //            catch (Exception)
        //            { }
        //        }
        //        else
        //        {
        //            // Do nothing. Not implemented yet.
        //        }
        //    }

        //    result = new DataTableReportTransWater();
        //    result.draw = draw;
        //    result.recordsFiltered = count;
        //    result.recordsTotal = count;
        //    result.data = listDataOperator.ToList();
        //    connection.Close();
        //    return result;
        //}

        public DataTableReportTransWater GetDataFilternew(int draw, int start, int length, string profit_center, string transaction_no, string sap_doc_no, string period, string posting_date_from, string posting_date_to, string installation_code, string customer, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_transaction_no = string.IsNullOrEmpty(transaction_no) ? "x" : transaction_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer = string.IsNullOrEmpty(customer) ? "x" : customer;

            int count = 0;
            int end = start + length;
            DataTableReportTransWater result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_WATER> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            //where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(profit_center) ? "" : " and PROFIT_CENTER_NEW = " + profit_center;
           // where1 += string.IsNullOrEmpty(transaction_no) ? "" : " and ID = " + transaction_no;
            //where1 += string.IsNullOrEmpty(customer) ? "" : " and CUSTOMER_MDM = " + "'" + customer + "'";
            //where1 += string.IsNullOrEmpty(period) ? "" : " and PERIOD = " + period;

            if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                    string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                            "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                            "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                            "FROM V_REPORT_TRANS_WATER WHERE " +
                            "ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                            "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                            "AND CUSTOMER = DECODE(:x_customer, 'x', CUSTOMER,:x_customer) " +
                            "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992', 'DD/MM/RRRR'),TO_DATE(:x_posting_date_from, 'DD/MM/RRRR')) " +
                            "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                            "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " + where1 +
                            "ORDER BY ID DESC ";
                        
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            d = KodeCabang
                        });
                    }
                    catch (Exception ex)
                    {
                        var cc = ex.ToString();
                        throw;
                    }
                }

            result = new DataTableReportTransWater();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        // --------------------------- NEW EXCEL -------------------------------
        public IEnumerable<AUP_REPORT_TRANS_WATER> ShowFilterTwonew(string profit_center, string transaction_no, string sap_doc_no, string period, string posting_date_from, string posting_date_to, string installation_code, string customer, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_WATER> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_transaction_no = string.IsNullOrEmpty(transaction_no) ? "x" : transaction_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_installation_code = string.IsNullOrEmpty(installation_code) ? "x" : installation_code;
            string x_customer = string.IsNullOrEmpty(customer) ? "x" : customer;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            //where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(profit_center) ? "" : " and PROFIT_CENTER_NEW = " + profit_center;

            if (x_profit_center.Length >= 0 && x_transaction_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_installation_code.Length >= 0 && x_customer.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                "INSTALLATION_CODE, INSTALLATION_ADDRESS, PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
                                "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
                                "FROM V_REPORT_TRANS_WATER " +
                                "WHERE " +
                                " ID = DECODE (:x_transaction_no,'x',ID,:x_transaction_no) AND PERIOD = DECODE (:x_period,'x',PERIOD,:x_period) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE (:x_sap_doc_no,'x',SAP_DOCUMENT_NUMBER,:x_sap_doc_no) " +
                                "AND CUSTOMER = DECODE (:x_customer,'x',CUSTOMER,:x_customer) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND INSTALLATION_CODE = DECODE (:x_installation_code,'x',INSTALLATION_CODE,:x_installation_code) " + where1 + v_be +
                                " ORDER BY ID DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_WATER>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_transaction_no = x_transaction_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_installation_code = x_installation_code,
                            x_customer = x_customer,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            

            connection.Close();
            return listData;

        }


    }
}
