﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransElectricity;
using Oracle.ManagedDataAccess.Client;
using System.Data; 
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ReportTransElectricityDAL
    {

        public DataTableReportTransElectricity GetDataFilter(int draw, int start, int length, string profit_center, string billing_no, string customer_id, string power_capacity, string period, string posting_date_from, string posting_date_to, string sap_document_number, string condition_used, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_power_capacity = string.IsNullOrEmpty(power_capacity) ? "x" : power_capacity;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_sap_document_number = string.IsNullOrEmpty(sap_document_number) ? "x" : sap_document_number;

            int count = 0;
            int end = start + length;
            DataTableReportTransElectricity result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                                "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                                "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                                "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_customer_id = x_customer_id,
                            x_power_capacity = x_power_capacity,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_sap_document_number = x_sap_document_number
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_customer_id = x_customer_id,
                            x_power_capacity = x_power_capacity,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_sap_document_number = x_sap_document_number
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        if (condition_used == "0")
                        {

                            string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                                    "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                                    "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                                    "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "AND BRANCH_ID = :d ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                        else
                        {

                            string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITYS WHERE " +
                                    "SAP_DOCUMENT_NUMBER IS NOT NULL AND CANCEL_STATUS IS NULL AND " +
                                    "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                                    "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                                    "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "AND BRANCH_ID = :d ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            result = new DataTableReportTransElectricity();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //------------------- NEW LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();


            sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE STATUS = 1 " + v_be + " ORDER BY PROFIT_CENTER_ID ASC";


            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------------ DEPRECATED -----------------------------------
        public IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> ShowFilterTwo(string profit_center, string billing_no, string customer_id, string power_capacity, string period, string posting_date_from, string posting_date_to, string sap_document_number, string condition_used, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_power_capacity = string.IsNullOrEmpty(power_capacity) ? "x" : power_capacity;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_sap_document_number = string.IsNullOrEmpty(sap_document_number) ? "x" : sap_document_number;

            if (KodeCabang == "0") 
            {
                if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, "+
                                "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE "+
                                "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) "+ 
                                "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) "+
                                "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) "+
                                "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) "+
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) "+
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) "+
                                "ORDER BY ID DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(sql, new
                        {
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_customer_id = x_customer_id,
                            x_power_capacity = x_power_capacity,
                            x_period = x_period,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            x_sap_document_number = x_sap_document_number
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }
            else
            {
                if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        if (condition_used == "0")
                        {

                            string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                                    "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                                    "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                                    "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "AND BRANCH_ID = :d ORDER BY ID DESC";

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(sql, new
                            {
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                        else
                        { 

                            string sql = "SELECT DISTINCT ID, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITYS WHERE " +
                                    "SAP_DOCUMENT_NUMBER IS NULL AND CANCEL_STATUS IS NULL AND " +
                                    "PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                                    "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                                    "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "AND BRANCH_ID = :d ORDER BY ID DESC";

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(sql, new
                            {
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            }

            connection.Close();
            return listData;

        }

        public DataTableReportTransElectricity GetDataFilternew(int draw, int start, int length, string profit_center, string billing_no, string customer_id, string power_capacity, string period, string posting_date_from, string posting_date_to, string sap_document_number, string condition_used, string KodeCabang)
        {
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_power_capacity = string.IsNullOrEmpty(power_capacity) ? "x" : power_capacity;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_sap_document_number = string.IsNullOrEmpty(sap_document_number) ? "x" : sap_document_number;

            int count = 0;
            int end = start + length;
            DataTableReportTransElectricity result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            //where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            //where1 += (profit_center != null ? " AND PROFIT_CENTER = DECODE(:x_profit_center, 'x', PROFIT_CENTER,:x_profit_center));
            //where1 += " and BRANCH_ID = " + be_id;
            //where1 += v_be;
            where1 += string.IsNullOrEmpty(profit_center) ? "" : " and PROFIT_CENTER_NEW = " + profit_center;
            where1 += string.IsNullOrEmpty(billing_no) ? "" : " and ID = " + billing_no;
            where1 += string.IsNullOrEmpty(customer_id) ? "" : " and CUSTOMER_MDM = " + "'"+ customer_id + "'";
            where1 += string.IsNullOrEmpty(power_capacity) ? "" : " and POWER_CAPACITY = " + power_capacity;
            where1 += string.IsNullOrEmpty(period) ? "" : " and PERIOD = " + period;

            if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        if (condition_used == "0")
                        {

                            //string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                            //        "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                            //        "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                            //        "ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                            //        "AND STATUS_DINAS = 'NON KEDINASAN' " +
                            //        "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                            //        "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                            //        "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                            //        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            //        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " + where1 + v_be + " ORDER BY ID DESC";
                            string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    " (POSTING_DATE BETWEEN to_date(:x_posting_date_from, 'DD/MM/RRRR') and to_date(:x_posting_date_to, 'DD/MM/RRRR')) AND STATUS_DINAS = 'NON KEDINASAN' " + where1 + v_be + " ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                        else
                        {

                            //string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                            //        "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                            //        "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                            //        "SAP_DOCUMENT_NUMBER IS NOT NULL AND CANCEL_STATUS IS NULL AND " +
                            //        "ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                            //         "AND STATUS_DINAS = 'KEDINASAN' " +
                            //        "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                            //        "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                            //        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            //        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " + where1 + " ORDER BY ID DESC";

                            string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    " (POSTING_DATE BETWEEN to_date(:x_posting_date_from, 'DD/MM/RRRR') and to_date(:x_posting_date_to, 'DD/MM/RRRR')) AND STATUS_DINAS = 'KEDINASAN' " + where1 + " ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        listData = null;
                    String error3;
                    error3 = ex.Message;
                    }
                }
            
            result = new DataTableReportTransElectricity();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();
            connection.Close();
            return result;
        }

        //------------------------ NEW EXCEL ----------------------------------
        public IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> ShowFilterTwonew(string profit_center, string billing_no, string customer_id, string power_capacity, string period, string posting_date_from, string posting_date_to, string sap_document_number, string condition_used, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_ELECTRICITY> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_power_capacity = string.IsNullOrEmpty(power_capacity) ? "x" : power_capacity;
            string x_period = string.IsNullOrEmpty(period) ? "x" : period;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;
            string x_sap_document_number = string.IsNullOrEmpty(sap_document_number) ? "x" : sap_document_number;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            string where1 = "";
            //where1 += (profit_center != null ? " and PROFIT_CENTER_NEW = " + profit_center : "");
            where1 += string.IsNullOrEmpty(profit_center) ? "" : " and PROFIT_CENTER_NEW = " + profit_center;
            where1 += string.IsNullOrEmpty(billing_no) ? "" : " and ID = " + billing_no;
            where1 += string.IsNullOrEmpty(customer_id) ? "" : " and CUSTOMER_MDM = " + "'"+ customer_id + "'";
            where1 += string.IsNullOrEmpty(power_capacity) ? "" : " and POWER_CAPACITY = " + power_capacity;
            where1 += string.IsNullOrEmpty(period) ? "" : " and PERIOD = " + period;

            if (x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_customer_id.Length >= 0 && x_power_capacity.Length >= 0 && x_period.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0 && x_sap_document_number.Length >= 0)
                {
                    try
                    {
                        if (condition_used == "0")
                        {

                            //string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                            //        "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                            //        "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                            //        " ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                            //        "AND STATUS_DINAS = 'NON KEDINASAN' " +
                            //        "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                            //        "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                            //        "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                            //        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            //        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " + where1 + v_be +
                            //        " ORDER BY ID DESC";
                            string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    " (POSTING_DATE BETWEEN to_date(:x_posting_date_from, 'DD/MM/RRRR') and to_date(:x_posting_date_to, 'DD/MM/RRRR')) AND STATUS_DINAS = 'NON KEDINASAN' " + where1 + v_be + " ORDER BY ID DESC";


                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(sql, new
                            {
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                        else
                        {

                            //string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                            //        "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                            //        "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                            //        " ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) " +
                            //        "AND STATUS_DINAS = 'KEDINASAN' " +
                            //        "AND CUSTOMER_MDM = DECODE (:x_customer_id,'x',CUSTOMER_MDM,:x_customer_id) AND PERIOD = DECODE(:x_period,'x',PERIOD,:x_period) " +
                            //        "AND POWER_CAPACITY = DECODE(:x_power_capacity,'x',POWER_CAPACITY,:x_power_capacity) " +
                            //        "AND SAP_DOCUMENT_NUMBER = DECODE(:x_sap_document_number,'x',SAP_DOCUMENT_NUMBER,:x_sap_document_number) " +
                            //        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            //        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " + where1 + v_be +
                            //        " ORDER BY ID DESC";
                            string sql = "SELECT DISTINCT ID, STATUS_DINAS, PROFIT_CENTER, CUSTOMER_MDM, CUSTOMER_MDM || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, INSTALLATION_CODE, INSTALLATION_ADDRESS, POWER_CAPACITY, MINIMUM_USED, PERIOD, MULTIPLY_FACT, TO_CHAR(POSTING_DATE,'DD.MM.YYYY') POSTING_DATE, " +
                                    "SAP_DOCUMENT_NUMBER, CANCEL_STATUS, GRAND_TOTAL, BRANCH_ID, METER_FROM_TO_LWBP, USED_LWBP, TARIFF_LWBP, METER_FROM_TO_WBP, USED_WBP, TARIFF_WBP, " +
                                    "METER_FROM_TO_KVARH, USED_KVARH, TARIFF_KVARH, METER_FROM_TO_BLOK, USED_BLOK, TARIFF_BLOK, PPJU, REDUKSI FROM V_REPORT_TRANS_ELECTRICITY WHERE " +
                                    " (POSTING_DATE BETWEEN to_date(:x_posting_date_from, 'DD/MM/RRRR') and to_date(:x_posting_date_to, 'DD/MM/RRRR')) AND STATUS_DINAS = 'KEDINASAN' " + where1 + " ORDER BY ID DESC";


                            listData = connection.Query<AUP_REPORT_TRANS_ELECTRICITY>(sql, new
                            {
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_customer_id = x_customer_id,
                                x_power_capacity = x_power_capacity,
                                x_period = x_period,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to,
                                x_sap_document_number = x_sap_document_number,
                                d = KodeCabang
                            });
                        }
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
            
            connection.Close();
            return listData;

        }


    }
}