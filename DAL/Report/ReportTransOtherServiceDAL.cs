﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransOtherService;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.Controllers
{
    class ReportTransOtherServiceDAL
    {

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a AND STATUS IS NULL order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS IS NULL order by profit_center_id asc";
                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterTwo(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();

            return listData;
        }

        //------------------------------- BUTTON SHOW ALL ---------------------
        public DataTableReportTransOtherService ShowAll(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTableReportTransOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                    "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                    "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                    "FROM V_REPORT_TRANS_OTHER_SERVICE ORDER BY ID DESC";


                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    // Do nothing. Not implemented yet. 
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                    "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                    "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                    "FROM V_REPORT_TRANS_OTHER_SERVICE WHERE BRANCH_ID = :c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    // Do nothing. Not implemented yet.
                }
            }

            result = new DataTableReportTransOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }

        //----------------------------------- SHOW ALL
        public IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> ShowAllTwo(string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                         "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                         "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                         "FROM V_REPORT_TRANS_OTHER_SERVICE ORDER BY ID DESC";

                    listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new { c = KodeCabang });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                         "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                         "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                         "FROM V_REPORT_TRANS_OTHER_SERVICE WHERE BRANCH_ID = :c ORDER BY ID DESC";

                    listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new
                    {
                        c = KodeCabang
                    });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }

            connection.Close();
            return listData;

        }

        //---------------------------------- DEPRECATED -----------------------------
        public IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> ShowFilterTwo(string be_id, string profit_center, string billing_no, string sap_no, string customer_id, string service_group, string posting_date_from, string posting_date_to, string KodeCabang, string UserRole)
        {
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_no = string.IsNullOrEmpty(sap_no) ? "x" : sap_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            {
                if (KodeCabang == "0" && UserRole == "4")
                {
                    try
                    {
                        /*
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, B.REF_DESC AS UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A, PROP_PARAMETER_REF_D B " +
                                "WHERE A.UNIT = B.REF_DATA " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group) " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "ORDER BY ID DESC";
                        */

                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " + 
                                "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                "WHERE (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";
                        listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else if (KodeCabang == "0" && UserRole == "20")
                {
                    try
                    {
                        /*
                        string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                    "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                    "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                    "FROM V_REPORT_TRANS_OTHER_SERVICE WHERE BRANCH_ID = :d " +
                                    "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) AND CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',CUSTOMER_SAP_AR,:x_sap_no) " +
                                    "AND COSTUMER_ID = DECODE (:x_customer_id,'x',COSTUMER_ID,:x_customer_id) AND SERVICES_GROUP = DECODE (:x_service_group,'x',SERVICES_GROUP,:x_service_group) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "ORDER BY ID DESC";
                        */
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                "WHERE BRANCH_ID = :d " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";
                        listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        /*
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, B.REF_DESC AS UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A, PROP_PARAMETER_REF_D B " +
                                "WHERE A.UNIT = B.REF_DATA AND BRANCH_ID = :d " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group) " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "ORDER BY ID DESC";
                        */
                        if (connection.State.Equals(ConnectionState.Closed))
                            connection.Open();
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                "WHERE BRANCH_ID = :d " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";

                        listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            return listData;

        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang, string UserRole)
        {
            string sql = "";

            if (KodeCabang == "0" && UserRole == "4")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else if (KodeCabang == "0" && UserRole == "20")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN BUSINESS ENTITY ------------------------
        public IEnumerable<DDBusinessEntity> GetDataBEnew(string KodeCabang, string UserRole)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) ";            

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- DEPRECATED ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang, string UserRole)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (KodeCabang == "0" && UserRole == "4")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else if (KodeCabang == "0" && UserRole == "20")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1 AND STATUS = 1";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //------------------- NEW LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenternew(string KodeCabang, string UserRole)
        {
            string sql = "";
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) ";            

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { d = KodeCabang });
            connection.Close();
            return listDetil;
        }

        //--------------------- LIST DROP DOWN SERVICE GROUP -----------------------
        public IEnumerable<DDServiceGroup> GetDataService()
        {
            string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SERVICE_GROUP' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            IEnumerable<DDServiceGroup> listDetil = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                listDetil = connection.Query<DDServiceGroup>(sql);
            }
            catch (Exception) { }
            connection.Close();
            return listDetil;
        }

        //-------------------- AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT COSTUMER_ID code, COSTUMER_ID || ' - ' || COSTUMER_MDM_NAME label, COSTUMER_MDM_NAME name FROM V_REPORT_TRANS_OTHER_SERVICE " +
                        "WHERE ((UPPER(COSTUMER_MDM_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (COSTUMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT COSTUMER_ID code, COSTUMER_ID || ' - ' || COSTUMER_MDM_NAME label, COSTUMER_MDM_NAME name FROM V_REPORT_TRANS_OTHER_SERVICE " +
                        "WHERE ((UPPER(COSTUMER_MDM_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (COSTUMER_ID LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            return exec;
        }

        //------------------------------ BUTTON FILTER ---------------------------
        public DataTableReportTransOtherService GetDataFilter(int draw, int start, int length, string be_id, string profit_center, string billing_no, string sap_no, string customer_id, string service_group, string posting_date_from, string posting_date_to, string search, string KodeCabang)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_no = string.IsNullOrEmpty(sap_no) ? "x" : sap_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            int count = 0;
            int end = start + length;
            DataTableReportTransOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //Untuk Pusat
            if (KodeCabang == "0")
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                    "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                    "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                    "FROM V_REPORT_TRANS_OTHER_SERVICE " +
                                    "WHERE BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                    "AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) AND CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',CUSTOMER_SAP_AR,:x_sap_no) " +
                                    "AND COSTUMER_ID = DECODE (:x_customer_id,'x',COSTUMER_ID,:x_customer_id) AND SERVICES_GROUP = DECODE (:x_service_group,'x',SERVICES_GROUP,:x_service_group) " +
                                    "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);
                            listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                            {
                                a = start,
                                b = end,
                                x_be_id = x_be_id,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_sap_no = x_sap_no,
                                x_customer_id = x_customer_id,
                                x_service_group = x_service_group,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to
                            });
                            fullSqlCount = fullSqlCount.Replace("sql", sql);
                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                x_be_id = x_be_id,
                                x_profit_center = x_profit_center,
                                x_billing_no = x_billing_no,
                                x_sap_no = x_sap_no,
                                x_customer_id = x_customer_id,
                                x_service_group = x_service_group,
                                x_posting_date_from = x_posting_date_from,
                                x_posting_date_to = x_posting_date_to
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            //Pencarian untuk user cabang
            {
                if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
                {
                    try
                    {
                        string sql = "SELECT BE_ID, PROFIT_CENTER, ID, TO_CHAR(POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, CUSTOMER_SAP_AR, SERVICES_GROUP, SERVICE_NAME, " +
                                "QUANTITY, UNIT, PRICE, CURRENCY, MULTIPLY_FACTOR, SURCHARGE, AMOUNT, SAP_DOCUMENT_NUMBER, GL_ACCOUNT, " +
                                "SERVICES_GROUP_NAME, PROFIT_CENTER_NAME, COSTUMER_MDM_NAME, BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE WHERE BRANCH_ID = :d " +
                                "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) AND PROFIT_CENTER = DECODE (:x_profit_center,'x',PROFIT_CENTER,:x_profit_center) " +
                                "AND ID = DECODE (:x_billing_no,'x',ID,:x_billing_no) AND CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND COSTUMER_ID = DECODE (:x_customer_id,'x',COSTUMER_ID,:x_customer_id) AND SERVICES_GROUP = DECODE (:x_service_group,'x',SERVICES_GROUP,:x_service_group) " +
                                "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }

            result = new DataTableReportTransOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        public DataTableReportTransOtherService GetDataFilterTwo(int draw, int start, int length, string be_id, string profit_center, string billing_no, string sap_no, string customer_id, string service_group, string posting_date_from, string posting_date_to, string search, string KodeCabang, string UserRole)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_no = string.IsNullOrEmpty(sap_no) ? "x" : sap_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            int count = 0;
            int end = start + length;
            DataTableReportTransOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            {
                if (KodeCabang == "0" && UserRole == "4")
                {
                    try
                    {
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                    "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                    "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                    "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                    "WHERE (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                    "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                    "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                    "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                    "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                    "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";
                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
                }
                else if (KodeCabang == "0" && UserRole == "20")
                {
                    try
                    {
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                "WHERE BRANCH_ID = :d " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";
                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    try
                    {
                        if (connection.State.Equals(ConnectionState.Closed))
                            connection.Open();
                        string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                                "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                                "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                                "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                                "WHERE BRANCH_ID = :d " +
                                "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                                "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                                "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                                "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                                "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                                "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)";

                        fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
                }

            }

            result = new DataTableReportTransOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        ////------------------------------ SELECT2 BUSINESS ENTITY --------------------------------
        //public IEnumerable<VW_BUSINESS_ENTITY> getBusinessEntity(string xPARAM)
        //{
        //    VW_BUSINESS_ENTITY result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    IEnumerable<VW_BUSINESS_ENTITY> listData = null;
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    try
        //    {
        //        string sql = "SELECT BE_ID id, BE_ID, BE_NAME, BE_ADDRESS, BE_CITY FROM BUSINESS_ENTITY " +
        //                     "WHERE UPPER(BE_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
        //        listData = connection.Query<VW_BUSINESS_ENTITY>(sql, new { PARAM = xPARAM });
        //    }
        //    catch (Exception)
        //    {
        //        listData = null;
        //    }
        //    connection.Close();

        //    return listData;
        //}

        ////------------------------------ SELECT2 PROFIT CENTER --------------------------------
        //public IEnumerable<VW_PROFIT_CENTER> getProfitCenter(string xPARAM)
        //{
        //    VW_BUSINESS_ENTITY result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    IEnumerable<VW_PROFIT_CENTER> listData = null;
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    try
        //    {
        //        string sql = "SELECT PROFIT_CENTER_ID id, PROFIT_CENTER_ID, TERMINAL_NAME FROM PROFIT_CENTER " +
        //                     "WHERE UPPER(TERMINAL_NAME) LIKE '%' || UPPER(:PARAM) || '%' ";
        //        listData = connection.Query<VW_PROFIT_CENTER>(sql, new { PARAM = xPARAM });
        //    }
        //    catch (Exception)
        //    {
        //        listData = null;
        //    }
        //    connection.Close();

        //    return listData;
        //}

        ////------------------------------ SELECT2 SERVICE GROUP -----------------------------
        //public IEnumerable<VW_SERVICE_GROUP> getServiceGroup(string xPARAM)
        //{
        //    VW_SERVICE_GROUP result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    IEnumerable<VW_SERVICE_GROUP> listData = null;
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    try
        //    {
        //        string sql = "SELECT REF_DATA id, REF_DATA, REF_CODE, REF_DESC FROM PROP_PARAMETER_REF_D " +
        //                     "WHERE UPPER(REF_DESC) LIKE '%' || UPPER(:PARAM) || '%' AND REF_CODE='SERVICE_GROUP'";
        //        listData = connection.Query<VW_SERVICE_GROUP>(sql, new { PARAM = xPARAM });
        //    }
        //    catch (Exception)
        //    {
        //        listData = null;
        //    }
        //    connection.Close();

        //    return listData;
        //}

        ////------------------------------ SELECT2 CUSTOMER MDM -----------------------------
        //public IEnumerable<VW_CUSTOMER_MDM> getCustomerMDM(string xPARAM)
        //{
        //    VW_CUSTOMER_MDM result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    IEnumerable<VW_CUSTOMER_MDM> listData = null;
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    try
        //    {
        //        string sql = "SELECT COSTUMER_ID id, COSTUMER_ID, COSTUMER_MDM FROM PROP_VB_TRANSACTION " +
        //                     "WHERE UPPER(COSTUMER_MDM) LIKE '%' || UPPER(:PARAM) || '%'";
        //        listData = connection.Query<VW_CUSTOMER_MDM>(sql, new { PARAM = xPARAM });
        //    }
        //    catch (Exception)
        //    {
        //        listData = null;
        //    }
        //    connection.Close();

        //    return listData;
        //}

         


        //------------------------------- NEW GET DATA FILTER ------------------------

        public DataTableReportTransOtherService GetDataFilterTwonew(int draw, int start, int length, string be_id, string profit_center, string billing_no, string sap_no, string customer_id, string service_group, string posting_date_from, string posting_date_to, string search, string KodeCabang, string UserRole)
        {
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_no = string.IsNullOrEmpty(sap_no) ? "x" : sap_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            int count = 0;
            int end = start + length;
            DataTableReportTransOtherService result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listDataFilterDetail = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum >= :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string where = "";

            //if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            //{               
                    try
                    {
                where += string.IsNullOrEmpty(be_id) ? "" : " and a.be_id = " + be_id;
                where += string.IsNullOrEmpty(profit_center) ? "" : " and a.PROFIT_CENTER = " + profit_center;
                where += string.IsNullOrEmpty(billing_no) ? "" : " and a.ID = " + billing_no;
                where += string.IsNullOrEmpty(sap_no) ? "" : " and a.CUSTOMER_SAP_AR = " + sap_no;
                where += string.IsNullOrEmpty(x_customer_id) ? "" : " and a.COSTUMER_ID = '" + x_customer_id +"'";
                where += string.IsNullOrEmpty(service_group) ? "" : " and a.SERVICES_GROUP = " + service_group;
                //string sql = @"SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, 
                //            A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, 
                //            A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME 
                //            FROM V_REPORT_TRANS_OTHER_SERVICE A 
                //            WHERE (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) 
                //            AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) 
                //            AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) 
                //            AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) 
                //            AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) 
                //            AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group)" + v_be;
                string sql = @"SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, 
                                    A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, 
                                    A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME 
                                    FROM V_REPORT_TRANS_OTHER_SERVICE A 
                                    WHERE (A.POSTING_DATE BETWEEN to_date(:e, 'DD/MM/RRRR') and to_date(:f, 'DD/MM/RRRR'))" + where + v_be;
                fullSql = fullSql.Replace("sql", sql);
                        listDataFilterDetail = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            e = posting_date_from,
                            f = posting_date_to,
                            d = KodeCabang
                        });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            e = posting_date_from,
                            f = posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
                
            //}

            result = new DataTableReportTransOtherService();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataFilterDetail.ToList();
            connection.Close();
            return result;
        }

        //-------------------------------- NEW EXCEL ----------------------------------
        public IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> ShowFilterTwonew(string be_id, string profit_center, string billing_no, string sap_no, string customer_id, string service_group, string posting_date_from, string posting_date_to, string KodeCabang, string UserRole)
        {
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_no = string.IsNullOrEmpty(sap_no) ? "x" : sap_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_service_group = string.IsNullOrEmpty(service_group) ? "x" : service_group;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_no.Length >= 0 && x_customer_id.Length >= 0 && x_service_group.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            {                
                    try
                    {
                    /*
                    string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                            "A.QUANTITY, B.REF_DESC AS UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                            "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                            "FROM V_REPORT_TRANS_OTHER_SERVICE A, PROP_PARAMETER_REF_D B " +
                            "WHERE A.UNIT = B.REF_DATA " +
                            "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                            "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                            "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group) " +
                            "AND (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                            "ORDER BY ID DESC";
                    */

                    string sql = "SELECT A.BE_ID, A.PROFIT_CENTER, A.ID, TO_CHAR(A.POSTING_DATE, 'DD.MM.RRRR') POSTING_DATE, A.CUSTOMER_SAP_AR, A.SERVICES_GROUP, A.SERVICE_NAME, " +
                            "A.QUANTITY, A.UNIT, A.PRICE, A.CURRENCY, A.MULTIPLY_FACTOR, A.SURCHARGE, A.AMOUNT, A.SAP_DOCUMENT_NUMBER, A.GL_ACCOUNT, " +
                            "A.SERVICES_GROUP_NAME, A.PROFIT_CENTER_NAME, A.COSTUMER_MDM_NAME, A.BE_NAME " +
                            "FROM V_REPORT_TRANS_OTHER_SERVICE A " +
                            "WHERE (A.POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                            "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                            "AND A.BE_ID = DECODE(:x_be_id,'x',A.BE_ID,:x_be_id) AND A.PROFIT_CENTER = DECODE (:x_profit_center,'x',A.PROFIT_CENTER,:x_profit_center) " +
                            "AND A.ID = DECODE (:x_billing_no,'x',A.ID,:x_billing_no) AND A.CUSTOMER_SAP_AR = DECODE (:x_sap_no,'x',A.CUSTOMER_SAP_AR,:x_sap_no) " +
                            "AND A.COSTUMER_ID = DECODE (:x_customer_id,'x',A.COSTUMER_ID,:x_customer_id) " +
                            "AND A.SERVICES_GROUP = DECODE (:x_service_group,'x',A.SERVICES_GROUP,:x_service_group) " + v_be;
                        listData = connection.Query<AUP_REPORT_TRANS_OTHER_SERVICES>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_no = x_sap_no,
                            x_customer_id = x_customer_id,
                            x_service_group = x_service_group,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }                          

            }

            connection.Close();
            return listData;

        }

    }
}
