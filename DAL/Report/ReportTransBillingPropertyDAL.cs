﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using Remote.Models.Select2;
using Remote.Models.ReportTransBillingProperty;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.Controllers
{
    class ReportTransBillingPropertyDAL
    {

        //------------------- LIST DROP DOWN BUSINESS ENTITY -------------------------
        public IEnumerable<DDBusinessEntity> GetDataBE(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";
            }
            else
            {
                sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- LIST DROP DOWN PROFIT CENTER ------------------------
        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a AND STATUS = 1";
            }

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //-------------------- AUTO COMPLETE CUSTOMER NAME ------------------
        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string CUSTOMER_NAME, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string str = "";

            if (KodeCabang == "0")
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER_NAME name FROM V_REPORT_BILLING_PROPERTY " +
                        "WHERE ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%'))";
            }
            else
            {
                str = "SELECT DISTINCT BUSINESS_PARTNER code, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME label, BUSINESS_PARTNER_NAME name FROM V_REPORT_BILLING_PROPERTY " +
                        "WHERE ((UPPER(BUSINESS_PARTNER_NAME) LIKE '%" + CUSTOMER_NAME.ToUpper() + "%') OR (BUSINESS_PARTNER LIKE '%" + CUSTOMER_NAME.ToUpper() + "%')) AND BRANCH_ID = '" + KodeCabang + "'";
            }

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();

            connection.Close();
            connection.Dispose();
            return exec;
        }

        public IEnumerable<DDBusinessEntity> GetDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY";

                    listData = connection.Query<DDBusinessEntity>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

                    listData = connection.Query<DDBusinessEntity>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            connection.Dispose();

            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterD(string KodeCabang, string BE_ID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where be_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = BE_ID }).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    //string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS NAMA_PROFIT_CENTER from profit_center where branch_id=:a order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception ex) {
                    string aaa = ex.ToString();
                }
            }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenterTwo(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listData = null;

            if (KodeCabang == "0")
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql).ToList();
                }
                catch (Exception) { }
            }
            else
            {
                try
                {
                    // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                    string sql = "select profit_center_id, profit_center_id || ' - ' || terminal_name AS nama_profit_center from profit_center where branch_id=:a AND STATUS = 1 order by profit_center_id asc";

                    listData = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang }).ToList();
                }
                catch (Exception) { }
            }

            connection.Close();
            connection.Dispose();

            return listData;
        }

        public IEnumerable<DDContractType> GetDataConditionType(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractType> listData = null;

            try
            {
                string sql = "SELECT REF_DATA, REF_DATA || ' - ' || REF_DESC REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='CONDITION_TYPE' ORDER BY REF_DATA ASC";

                listData = connection.Query<DDContractType>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        //-------------------------- deprecated --------------------------
        public DataTableReportTransBillingPropertyTwo GetDataFilters(int draw, int start, int length, string be_id, string profit_center, string billing_no, string sap_doc_no, string customer_id, string condition_type, string posting_date_from, string posting_date_to, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_condition_type = string.IsNullOrEmpty(condition_type) ? "x" : condition_type;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            int count = 0;
            int end = start + length;
            DataTableReportTransBillingPropertyTwo result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && sap_doc_no.Length >= 0 && billing_no.Length >= 0 && condition_type.Length >= 0 && posting_date_from.Length >= 0 && posting_date_to.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, " +
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " +
                        "ORDER BY BILLING_NO DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, "+
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND BRANCH_ID = :d AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " +
                        "ORDER BY BILLING_NO DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }

                }
            }
            else
            {

            }

            result = new DataTableReportTransBillingPropertyTwo();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //----------------------------- DEPRECATED -------------------------------------
        public IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> ShowFilterTwo(string be_id, string profit_center, string billing_no, string sap_doc_no, string customer_id, string condition_type, string posting_date_from, string posting_date_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            //be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, KodeCabang
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_condition_type = string.IsNullOrEmpty(condition_type) ? "x" : condition_type;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_customer_id.Length >= 0 && x_condition_type.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            {
                if (KodeCabang == "0")
                {
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, "+
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " +
                        "ORDER BY BILLING_NO DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }
                else
                {
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, "+
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND BRANCH_ID = :d AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " +
                        "ORDER BY BILLING_NO DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }
                }

            }

            connection.Close();
            connection.Dispose();
            return listData;

        }

        ////------------------------------- BUTTON SHOW ALL ---------------------
        //public DataTableReportTransWater ShowAll(int draw, int start, int length, string search, string KodeCabang)
        //{
        //    int count = 0;
        //    int end = start + length;
        //    DataTableReportTransWater result = null;
        //    IDbConnection connection = DatabaseFactory.GetConnection();
        //    if (connection.State.Equals(ConnectionState.Closed))
        //        connection.Open();
        //    IEnumerable<AUP_REPORT_TRANS_WATER> listDataOperator = null;

        //    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
        //    string fullSqlCount = "SELECT count(*) FROM (sql)";

        //    if (KodeCabang == "0")
        //    {
        //        if (search.Length == 0)
        //        {
        //            try
        //            {
        //                string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, "+
        //                        "INSTALLATION_CODE, INSTALLATION_ADDRESS,PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, "+
        //                        "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS "+
        //                        "FROM V_REPORT_TRANS_WATER";


        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new { a = start, b = end });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
        //            }
        //            catch (Exception)
        //            { }
        //        }
        //        else
        //        {
        //            // Do nothing. Not implemented yet. 
        //        }
        //    }
        //    else
        //    {
        //        if (search.Length == 0)
        //        {
        //            try
        //            {
        //                string sql = "SELECT ID, PROFIT_CENTER || ' - ' || TERMINAL_NAME AS PROFIT_CENTER_NAME, CUSTOMER || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
        //                        "INSTALLATION_CODE, INSTALLATION_ADDRESS,PERIOD, METER_FROM || ' - ' || METER_TO AS METER_FROM_TO, QUANTITY, MIN_USED, TARIFF_CODE || ' - ' || AMOUNT AS TARIFF_CODE, " +
        //                        "SURCHARGE, SUB_TOTAL, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, SAP_DOCUMENT_NUMBER, CANCEL_STATUS " +
        //                        "FROM V_REPORT_TRANS_WATER WHERE BRANCH_ID = :c";

        //                fullSql = fullSql.Replace("sql", sql);

        //                listDataOperator = connection.Query<AUP_REPORT_TRANS_WATER>(fullSql, new { a = start, b = end, c = KodeCabang });

        //                fullSqlCount = fullSqlCount.Replace("sql", sql);

        //                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
        //            }
        //            catch (Exception)
        //            { }
        //        }
        //        else
        //        {
        //            // Do nothing. Not implemented yet.
        //        }
        //    }

        //    result = new DataTableReportTransWater();
        //    result.draw = draw;
        //    result.recordsFiltered = count;
        //    result.recordsTotal = count;
        //    result.data = listDataOperator.ToList();
        //    connection.Close();
        //    return result;
        //}

        //-------------------- filter new -----------------
        public DataTableReportTransBillingPropertyTwo GetDataFiltersnew(int draw, int start, int length, string be_id, string profit_center, string billing_no, string sap_doc_no, string customer_id, string condition_type, string posting_date_from, string posting_date_to, string search, string KodeCabang)
        {

            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_condition_type = string.IsNullOrEmpty(condition_type) ? "x" : condition_type;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            int count = 0;
            int end = start + length;
            DataTableReportTransBillingPropertyTwo result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (x_profit_center.Length >= 0 && x_be_id.Length >= 0 && x_customer_id.Length >= 0 && sap_doc_no.Length >= 0 && billing_no.Length >= 0 && condition_type.Length >= 0 && posting_date_from.Length >= 0 && posting_date_to.Length >= 0)
            {
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, " +
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " + v_be +
                        "ORDER BY BILLING_NO DESC";

                    fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(fullSql, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
                                    
            }
            else
            {

            }

            result = new DataTableReportTransBillingPropertyTwo();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //--------------------------- NEW EXCEL ----------------------------------
        public IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> ShowFilterTwonew(string be_id, string profit_center, string billing_no, string sap_doc_no, string customer_id, string condition_type, string posting_date_from, string posting_date_to, string KodeCabang)
        {
            IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> listData = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            //be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, KodeCabang
            string x_be_id = string.IsNullOrEmpty(be_id) ? "x" : be_id;
            string x_profit_center = string.IsNullOrEmpty(profit_center) ? "x" : profit_center;
            string x_billing_no = string.IsNullOrEmpty(billing_no) ? "x" : billing_no;
            string x_sap_doc_no = string.IsNullOrEmpty(sap_doc_no) ? "x" : sap_doc_no;
            string x_customer_id = string.IsNullOrEmpty(customer_id) ? "x" : customer_id;
            string x_condition_type = string.IsNullOrEmpty(condition_type) ? "x" : condition_type;
            string x_posting_date_from = string.IsNullOrEmpty(posting_date_from) ? "x" : posting_date_from;
            string x_posting_date_to = string.IsNullOrEmpty(posting_date_to) ? "x" : posting_date_to;

            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (x_be_id.Length >= 0 && x_profit_center.Length >= 0 && x_billing_no.Length >= 0 && x_sap_doc_no.Length >= 0 && x_customer_id.Length >= 0 && x_condition_type.Length >= 0 && x_posting_date_from.Length >= 0 && x_posting_date_to.Length >= 0)
            {                
                    try
                    {
                        string sql = "SELECT BILLING_NO, SAP_DOC_NO, TO_CHAR(POSTING_DATE,'DD.MM.RRRR') POSTING_DATE, CREATION_BY, BE_ID, BE_ID || ' - ' || BE_NAME AS BUSINESS_ENTITY, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS PROFIT_CENTER, " +
                        "BUSINESS_PARTNER, BUSINESS_PARTNER || ' - ' || BUSINESS_PARTNER_NAME AS BUSINESS_PARTNER_NAME, CUSTOMER_AR, BILLING_CONTRACT_NO, CONDITION_TYPE, GL_ACOUNT_NO, NVL(TO_CHAR(VALID_FROM, 'DD.MM.RRRR'), '-') VALID_FROM, " +
                        "NVL(TO_CHAR(VALID_TO, 'DD.MM.RRRR'), '-') VALID_TO, LUAS, PROFIT_CENTER_ID, CONDITION_TYPE || ' - ' || CONDITION_TYPE_DESC AS CONDITION_TYPE_DESC, OBJECT_ID, CURRENCY_CODE CURRENCY, INSTALLMENT_AMOUNT AMOUNT " +
                        "FROM V_REPORT_BILLING_PROPERTY WHERE CANCEL_STATUS IS NULL " +
                        "AND CONDITION_TYPE = DECODE(:x_condition_type,'x',CONDITION_TYPE,:x_condition_type) " +
                        "AND BE_ID = DECODE(:x_be_id,'x',BE_ID,:x_be_id) " +
                        "AND PROFIT_CENTER_ID = DECODE (:x_profit_center,'x',PROFIT_CENTER_ID,:x_profit_center) " +
                        "AND BILLING_NO = DECODE(:x_billing_no,'x',BILLING_NO,:x_billing_no) " +
                        "AND BUSINESS_PARTNER = DECODE(:x_customer_id,'x',BUSINESS_PARTNER,:x_customer_id) " +
                        "AND (POSTING_DATE BETWEEN DECODE(:x_posting_date_from, 'x', TO_DATE('01/01/1992','DD/MM/RRRR'),TO_DATE(:x_posting_date_from,'DD/MM/RRRR')) " +
                        "AND DECODE(:x_posting_date_to,'x',TO_DATE('01/01/2999','DD/MM/RRRR'),TO_DATE(:x_posting_date_to,'DD/MM/RRRR'))) " +
                        "AND SAP_DOC_NO = DECODE(:x_sap_doc_no,'x',SAP_DOC_NO,:x_sap_doc_no) " + v_be +
                        "ORDER BY BILLING_NO DESC";

                        listData = connection.Query<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO>(sql, new
                        {
                            x_be_id = x_be_id,
                            x_profit_center = x_profit_center,
                            x_billing_no = x_billing_no,
                            x_sap_doc_no = x_sap_doc_no,
                            x_customer_id = x_customer_id,
                            x_condition_type = x_condition_type,
                            x_posting_date_from = x_posting_date_from,
                            x_posting_date_to = x_posting_date_to,
                            d = KodeCabang
                        });
                    }
                    catch (Exception)
                    {
                        listData = null;
                    }                
            }

            connection.Close();
            connection.Dispose();
            return listData;

        }


    }
}
