﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.Controllers;
using Remote.Helpers;
using Remote.Models;
using Remote.Entities;
using Remote.Models.GeneralResult;

namespace Remote.DAL
{
    public class HomeDAL
    {
        public string global(string identifier ="")
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE(identifier);

            return v_be;
        }
        public string global_1()
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_Tingkat("a");

            return v_be;
        }
        private string queryDashboard(string kode)
        {
            string where = string.Empty;
            if (kode == "1")
            {
                where = " where STATUS_NOW = 0 ";
            }
            else if (kode == "2")
            {
                where = " where STATUS_NOW != 0 and STATUS_NOW != 5 ";
            }
            else if (kode == "3")
            {
                where = " where STATUS_NOW = 5 and to_char(CONTRACT_NO) not in (select OLD_CONTRACT from PROP_RENTAL_REQUEST where OLD_CONTRACT = to_char(CONTRACT_NO)) ";// and EXTRACT(YEAR FROM  CONTRACT_END_DATE)  = EXTRACT(YEAR FROM  SYSDATE)
            }
            else if (kode == "4")
            {
                where = @" where  CONTRACT_TYPE = 'ZC02' ";//EXTRACT(YEAR FROM  CREATION_DATE)  = EXTRACT(YEAR FROM  SYSDATE) and
            }
            else if (kode == "5")
            {
                where = @" where  OLD_CONTRACT is null ";//EXTRACT(YEAR FROM  CONTRACT_END_DATE)  = EXTRACT(YEAR FROM SYSDATE) and
            }
            else if (kode == "6")
            {
                where = @" where  CONTRACT_TYPE = 'ZC01' ";//EXTRACT(YEAR FROM  CREATION_DATE)  = EXTRACT(YEAR FROM  SYSDATE) and
            }
            return where;
        }
        public DataDashboard dataDashboard(SearchDashboard param, string kdCabang)
        {
            DataDashboard result = new DataDashboard();

            IDbConnection connection = DatabaseFactory.GetConnection("default");

            // QueryGlobal query = new QueryGlobal();
            string v_be = global();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string whereCabang = (param.BE_ID != "0" && param.BE_ID != null ? " AND BRANCH_ID = " + param.BE_ID : "");
                whereCabang += (param.start != "" && param.start != null ? " and CONTRACT_START_DATE < to_date('" + param.start + "', 'dd/mm/yyyy')" : "");
                whereCabang += (param.end != "" && param.end != null ? " and  CONTRACT_END_DATE < to_date('"+param.end+"', 'dd/mm/yyyy')" : "");
                string sql = "select count(distinct CONTRACT_NO) from DATA_DASHBOARD ";

                //dashboard 1
                // KONTRAK AKTIF - SUDAH POSTING
                string sql_data = sql + queryDashboard("1") + v_be + whereCabang;
                result.Data1 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                // KONTRAK YANG AKAN HABIS - SUDAH POSTING
                sql_data = sql + queryDashboard("2") + v_be + whereCabang;
                result.Data2 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                // KONTRAK HABIS - SUDAH POSTING
                sql_data = sql + queryDashboard("3") + v_be + whereCabang;
                result.Data3 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                result.Total = result.Data1 + result.Data2 + result.Data3;

                //dashboard 2
                // KONTRAK PERPANJANG
                // CREATED DATE - PERPANJANGAN - SUDAH POSTING
                sql_data = sql + queryDashboard("4") + v_be + whereCabang;
                //sql_data = sql + @" left join PROP_RENTAL_REQUEST b on TO_CHAR(a.CONTRACT_NO) = b.OLD_CONTRACT or a.LEGAL_CONTRACT_NO = b.OLD_CONTRACT
                //                    join PROP_CONTRACT_OFFER c on b.RENTAL_REQUEST_NO = c.RENTAL_REQUEST_NO
                //                    join PROP_CONTRACT d on c.CONTRACT_OFFER_NO = d.CONTRACT_OFFER_NO
                //                    where a.LEGAL_CONTRACT_NO != '-' and EXTRACT(YEAR FROM  a.CONTRACT_END_DATE)  = EXTRACT(YEAR FROM  SYSDATE) " + v_be + whereCabang;
                result.Data4 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                // KONTRAK TIDAK DIPERPANJANG
                // SUDAH POSTING - END DATE - NO OLD CONTRACT
                sql_data = sql + queryDashboard("5") + v_be + whereCabang;
                //sql_data = sql + @" left join PROP_RENTAL_REQUEST b on TO_CHAR(a.CONTRACT_NO) = b.OLD_CONTRACT or a.LEGAL_CONTRACT_NO LIKE '%' || b.OLD_CONTRACT || '%'
                //                    join PROP_CONTRACT_OFFER c on b.RENTAL_REQUEST_NO = c.RENTAL_REQUEST_NO
                //                    join PROP_CONTRACT d on c.CONTRACT_OFFER_NO = d.CONTRACT_OFFER_NO
                //                    where EXTRACT(YEAR FROM  a.CONTRACT_END_DATE)  = EXTRACT(YEAR FROM  SYSDATE) " + v_be + whereCabang;
                result.Data5 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                // KONTRAK BARU
                // CREATED DATE - PERMOHONAN BARU - SUDAH POSTING
                sql_data = sql + queryDashboard("6") + v_be + whereCabang;

                //sql_data = sql + @" where EXTRACT(YEAR FROM  a.CONTRACT_START_DATE)  = EXTRACT(YEAR FROM  SYSDATE)  " + v_be + whereCabang;
                result.Data6 = connection.ExecuteScalar<int>(sql_data, new { d = kdCabang });

                result.Status = "S";
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            connection.Close();
            connection.Dispose();

            return result;
        }
        public Results GetData(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            //int r = 0;
            List<DataDetailDashboard> list = new List<DataDetailDashboard>();
            string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //QueryGlobal query = new QueryGlobal();
            string v_be = global();
            try
            {
                where = queryDashboard(param.jenis) + v_be;

                where += (param.BE_ID != "" && param.BE_ID != null ? " AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = " + param.BE_ID + ") " : "");
                where += (param.start != "" && param.start != null ? "  and CONTRACT_START_DATE < to_date('" + param.start + "', 'dd/mm/yyyy')" : "");
                where += (param.end != "" && param.end != null ? " and CONTRACT_END_DATE < to_date('" + param.end + "', 'dd/mm/yyyy')" : "");
                var sql = @"select CONTRACT_NO, OLD_CONTRACT CONTRACT_NO_NEW
                            , CONTRACT_NAME, BUSINESS_PARTNER, BUSINESS_PARTNER_NAME
                            , MPLG_BADAN_USAHA, TO_CHAR(CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE 
                            , REF_CODE, BRANCH_ID, BE_NAME
                            , TO_CHAR(CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE
                            , REF_DATA, REF_DESC, STATUS_NOW, 
                            case 
                              when STATUS_NOW = 0 then
                                'Kontrak Aktif'
                              when STATUS_NOW = 1 then
                                'Pemberitahuan Ke 1'
                              when STATUS_NOW = 2 then
                                'Pemberitahuan Ke 2'
                              when STATUS_NOW = 3 then
                                'Pemberitahuan Ke 3'
                              when STATUS_NOW = 5 then
                                'Kontrak Sudah Habis'
                            end as STATUS
                            , TO_CHAR (TOTAL_NET_VALUE, '9G999G999G999G999', 'NLS_NUMERIC_CHARACTERS = '',.''') TOTAL_NET_VALUE
                            , LUAS_TANAH, LUAS_BANGUNAN
                            from DATA_DASHBOARD " + where + " order by branch_id asc";//D00 lanjutan numeric yang gak terpakai
                list = connection.Query<DataDetailDashboard>(sql, new { d = kdCabang}).ToList();

                result.Status = "S";
                result.Positionlist = list;
            }
            catch (Exception)
            {
                result.Status = "E";
            }
            connection.Close();
            connection.Dispose();
            return result;
        }
        public List<DataDetailDashboard> getExcel(string TYPE, string BUSINESS_ENTITY, string START_DATE, string END_DATE, string kdCabang)
        {
            List<DataDetailDashboard> listData = null;
            string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            // QueryGlobal query = new QueryGlobal();
            string v_be = global();

            try
            {
                where = queryDashboard(TYPE) + v_be;

                where += (BUSINESS_ENTITY != "" && BUSINESS_ENTITY != null ? " AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = " +BUSINESS_ENTITY + ") " : "");
                where += (START_DATE != "" && START_DATE != null ? " and CONTRACT_START_DATE < to_date('" + START_DATE + "', 'dd/mm/yyyy')" : "");
                where += (END_DATE != "" && END_DATE != null ? " and CONTRACT_START_DATE < to_date('" + END_DATE + "', 'dd/mm/yyyy')" : "");
                var sql = @"select CONTRACT_NO, CONTRACT_NAME,
                            BUSINESS_PARTNER, BUSINESS_PARTNER_NAME,
                            MPLG_BADAN_USAHA,TO_CHAR(CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE ,
                            REF_CODE, BRANCH_ID,
                            BE_NAME, TO_CHAR(CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE,
                            REF_DATA, REF_DESC, STATUS_NOW,
                            case 
                              when STATUS_NOW = 0 then
                                'Kontrak Aktif'
                              when STATUS_NOW = 1 then
                                'Pemberitahuan Ke 1'
                              when STATUS_NOW = 2 then
                                'Pemberitahuan Ke 2'
                              when STATUS_NOW = 3 then
                                'Pemberitahuan Ke 3'
                              when STATUS_NOW = 5 then
                                'Kontrak Sudah Habis'
                            end as STATUS , TO_CHAR (TOTAL_NET_VALUE, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') TOTAL_NET_VALUE,
                            LUAS_TANAH,LUAS_BANGUNAN
                            from DATA_DASHBOARD " + where;
                listData = connection.Query<DataDetailDashboard>(sql, new { d = kdCabang }).ToList();
            }
            catch(Exception)
            {
                listData = null;
            }
            
            
            connection.Close();

            connection.Dispose();
            return listData;
        }
        public Results dataPiutang(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<DashboardPiutang> list = new List<DashboardPiutang>();
            string where = " and a.doc_number is not null and doc_number != '/0000' and b.BRANCH_ID is not null and a.TGL_BATAL is null and a.TGL_LUNAS is null";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global("b");
            try
            {
                where += (param.BE_ID != "" && param.BE_ID != null ? " AND b.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = "+ param.BE_ID + ") " : "");
                //where += (param.end != "" && param.end != null ? " and  to_date(a.PSTNG_DATE, 'yyyy-mm-dd') <= to_date('" + param.end + "', 'dd/mm/yyyy')" : "");
                //where += (param.end != "" && param.end != null && param.start != "" && param.start != null ? " and  a.PSTNG_DATE BETWEEN 'param.start' AND 'param.end'");
                if(param.end != "" && param.end != null && param.start != "" && param.start != null)
                {
                    where = ("AND a.PSTNG_DATE BETWEEN '" + param.start + "' AND '" + param.end + "'");
                }
                var sql = @"select a.BRANCH_ID, a.BE_NAME, a.PROPERTY, b.WATER_ELECTRICITY, c.OTHER_SERVICE, 
                            TO_CHAR (a.PROPERTY, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') PROPERTY_TEXT,
                            TO_CHAR (b.WATER_ELECTRICITY, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') WATER_ELECTRICITY_TEXT, 
                            TO_CHAR (c.OTHER_SERVICE, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') OTHER_SERVICE_TEXT
                            from (
                              select 
                              b.BRANCH_ID, sum(b.INSTALLMENT_AMOUNT/1000000) PROPERTY, d.BE_NAME
                              from ITGR_HEADER a 
                              join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                              join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                              join BUSINESS_ENTITY d on d.BRANCH_ID = b.BRANCH_ID
                              where  a.DOC_TYPE = '1B' " + v_be + where + @"
                              group by b.BRANCH_ID, d.BE_NAME
                              order by b.BRANCH_ID asc
                              ) a
                            join (
                              select
                              b.BRANCH_ID, sum(b.AMOUNT/1000000) WATER_ELECTRICITY
                              from ITGR_HEADER a 
                              join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                              where (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') " + v_be + where + @"
                              group by b.BRANCH_ID
                              order by b.BRANCH_ID asc
                              ) b on a.BRANCH_ID = b.BRANCH_ID
                            join (
                              select 
                              b.BRANCH_ID, sum(b.TOTAL/1000000) OTHER_SERVICE
                              from ITGR_HEADER a 
                              join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
                              join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                              join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                              where a.BILL_TYPE = 'ZM03' " + v_be + where + @"
                              group by b.BRANCH_ID
                              order by b.BRANCH_ID asc
                              ) c on a.BRANCH_ID = c.BRANCH_ID and b.BRANCH_ID = c.BRANCH_ID
                            ";
                list = connection.Query<DashboardPiutang>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }
        public Results GetDetailPiutang(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<DashboardDetailPiutang> list = new List<DashboardDetailPiutang>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global("b");
            try
            {
                var sql = "";
                var where = "";
                where += (param.BE_ID != "" && param.BE_ID != null ? " AND b.BRANCH_ID = " + param.BE_ID : "");
                where += (param.end != "" && param.end != null && param.start != "" && param.start != null ? " and  a.PSTNG_DATE BETWEEN " + param.start + " AND " + param.end : "");
                if (param.jenis == "1")
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, 'Property' JENIS_TAGIHAN, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE , c.MPLG_NAMA CUSTOMER_NAME,to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                            from ITGR_HEADER a 
                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
                            where  doc_number is not null and doc_number != '/0000' and a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + v_be + where;
                }
                else if(param.jenis == "2")
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as JENIS_TAGIHAN, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME,to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.AMOUNT, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''')  AMOUNT
                            from ITGR_HEADER a 
                            join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
                            where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + v_be + where;
                }
                else
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, 'Other Service' JENIS_TAGIHAN, b.ID BILLING_NO,
	                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
	                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.TOTAL, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''')  AMOUNT from ITGR_HEADER a 
	                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
	                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
	                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
	                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + v_be + where;
                }
                list = connection.Query<DashboardDetailPiutang>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }
        public List<DashboardDetailPiutang> getExcelPiutang(string TYPE, string BUSINESS_ENTITY,string END_DATE, string kdCabang)
        {
            //Results result = new Results();
            List<DashboardDetailPiutang> list = null;
            string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global("b");

            try
            {
                var sql = "";
                where += (BUSINESS_ENTITY != "" && BUSINESS_ENTITY != null ? " AND b.BRANCH_ID = " + BUSINESS_ENTITY : "");
                where += (END_DATE != "" && END_DATE != null ? " and  to_date(a.PSTNG_DATE, 'yyyy-mm-dd') <= to_date('" + END_DATE + "', 'dd/mm/yyyy')" : "");
                if (TYPE == "1")
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, 'Property' JENIS_TAGIHAN, b.BILLING_NO, b.BILLING_CONTRACT_NO CODE, b.CUSTOMER_CODE , c.MPLG_NAMA CUSTOMER_NAME,to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.INSTALLMENT_AMOUNT, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''') AMOUNT 
                            from ITGR_HEADER a 
                            join PROP_CONTRACT_BILLING_POST b on a.REF_DOC_NO = b.BILLING_NO
                            join VW_CUSTOMERS c on b.CUSTOMER_CODE = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
                            where  doc_number is not null and doc_number != '/0000' and a.DOC_TYPE = '1B' and b.BRANCH_ID is not null " + v_be + where;
                }
                else if (TYPE == "2")
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, CASE WHEN b.BILLING_TYPE = 'ZM01' then 'Air' else 'Listrik' end as JENIS_TAGIHAN, b.ID BILLING_NO,b.INSTALLATION_CODE CODE,b.CUSTOMER CUSTOMER_CODE, b.CUSTOMER_NAME,to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.AMOUNT, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''')  AMOUNT
                            from ITGR_HEADER a 
                            join V_TRANS_WE_LIST_HEADER b on a.REF_DOC_NO = b.ID
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
                            where  doc_number is not null and doc_number != '/0000' and (a.BILL_TYPE = 'ZM01' or a.BILL_TYPE = 'ZM02') and b.BRANCH_ID is not null " + v_be + where;
                }
                else
                {
                    sql = @"select b.BRANCH_ID,be.BE_NAME, 'Other Service' JENIS_TAGIHAN, b.ID BILLING_NO,
	                        case when b.gl_account is null then d.REF_DATA || ' - ' || d.REF_DESC  else (SELECT DISTINCT CASE WHEN d.VAL1='4090709000' THEN  d.VAL1 || ' - ' ||'Pendapatan Penggantian Biaya TKBM' ELSE  d.VAL1 || ' - ' || d.VAL2 END FROM PROP_PARAMETER_REF_D WHERE VAL1=b.GL_ACCOUNT)  END  AS CODE ,
	                        b.COSTUMER_ID CUSTOMER_CODE, c.MPLG_NAMA CUSTOMER_NAME, to_char(to_date( a.PSTNG_DATE, 'yyyy-mm-dd'), 'dd/mm/yyyy') PSTNG_DATE, TO_CHAR (b.TOTAL, '9G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS = '',.''')  AMOUNT from ITGR_HEADER a 
	                        join PROP_VB_TRANSACTION b on a.REF_DOC_NO = b.ID
	                        join VW_CUSTOMERS c on b.COSTUMER_ID = c.MPLG_KODE and c.KD_CABANG = b.BRANCH_ID
	                        join PROP_PARAMETER_REF_D d on b.SERVICES_GROUP = d.REF_DATA 
                            join BUSINESS_ENTITY be on b.BRANCH_ID = be.BRANCH_ID 
	                        where  doc_number is not null and doc_number != '/0000' and a.BILL_TYPE = 'ZM03' and b.BRANCH_ID is not null " + v_be + where;
                }
                list = connection.Query<DashboardDetailPiutang>(sql, new { d = kdCabang }).ToList();
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }


            connection.Close();
            connection.Dispose();
            return list;
        }
        public Results dataRO(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global();
            try
            {
                where += (param.BE_ID != "" && param.BE_ID != null ? " AND a.BRANCH_ID = " + param.BE_ID : "");
                where += (param.end != "" && param.end != null ? " and  a.VALID_FROM <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND VALID_TO >= TO_DATE ('" + param.end + "', 'DD/MM/RRRR')" : "");
                var sql = @"select count(*) as JUMLAH, a.STATUS from V_REPORT_RO a
                            where a.BRANCH_ID is not null " + v_be + where + " group by a.STATUS order by a.STATUS";
                list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results DataPenggunaanLahan(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global();
            try
            {
               
                /*where += (param.BE_ID != "" && param.BE_ID != null ? " AND a.BE_ID = " + param.BE_ID : "");
                where += (param.end != "" && param.end != null ? " AND a.CONTRACT_END_DATE = " + param.end : "");*/
                var sql = @"SELECT COUNT(*) AS Pusat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) AND b.CONTRACT_END_DATE >= CURRENT_DATE";
                string sql_data = sql;
                result.dataPusat = connection.ExecuteScalar<int>(sql_data);

                var sqla = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                             WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) AND b.CONTRACT_END_DATE >= CURRENT_DATE";
                string lahan_pusat = sqla;
                result.LahanPusat = connection.ExecuteScalar<int>(lahan_pusat);

                var sqll = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED a WHERE a.CONTRACT_END_DATE >= CURRENT_DATE
                            AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                string sql_data2 = sqll;
                result.Customer = connection.ExecuteScalar<int>(sql_data2);

                var sqllb = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_OCCUPIED a WHERE a.CONTRACT_END_DATE >= CURRENT_DATE
                            AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                string sql_datab = sqllb;
                result.LahanCustomer = connection.ExecuteScalar<int>(sql_datab);

                var sqll2 = @"SELECT COUNT(*) AS Iddle FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO
                            WHERE b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE AND A.VALID_TO BETWEEN 
                            TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274 ";
                string sql_data3 = sqll2;
                result.Iddlekontrak = connection.ExecuteScalar<int>(sql_data3);

                var sqllc = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO
                            WHERE b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE AND A.VALID_TO BETWEEN 
                            TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274 ";
                string sql_datac = sqllc;
                result.LahanIddleKontrak = connection.ExecuteScalar<int>(sql_datac);

                var sqll3 = @"SELECT COUNT(*) AS Idlenokontrak FROM V_REPORT_RO_VACANT a
                WHERE a.CONTRACT_END_DATE < CURRENT_DATE AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                string sql_data9 = sqll3;
                result.Iddlenokontrak = connection.ExecuteScalar<int>(sql_data9);

                var sqlld = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_VACANT A 
                            WHERE a.CONTRACT_END_DATE < CURRENT_DATE AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                string sql_datad = sqlld;
                result.LahanIddlenoKontrak = connection.ExecuteScalar<int>(sql_datad);

                var sqll4 = @"SELECT COUNT(*) AS PusatBersertifikat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                              WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) AND b.CONTRACT_END_DATE >= CURRENT_DATE";
                /*var sqll4 = @"SELECT
                            sum(case WHEN A.RO_NAME != 'Pelindo' then 1 else 0 end) AS Customer
                            FROM V_REPORT_RO A 
                            WHERE A.CONTRACT_NO IS NOT NULL";*/
                string sql_data5 = sqll4;
                result.dataPusatBersertifikat = connection.ExecuteScalar<int>(sql_data5);

                var sqlle = @"SELECT SUM(LAND_DIMENSION) AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                              WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) AND b.CONTRACT_END_DATE >= CURRENT_DATE";
                string sql_datae = sqlle;
                result.LahanPusatBersertifikat = connection.ExecuteScalar<int>(sql_datae);

                where += @" WHERE a.CONTRACT_END_DATE >= CURRENT_DATE";
                var sqll5 = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED a";
                string sql_data6 = sqll5+where;
                result.CustomerBersertifikat = connection.ExecuteScalar<int>(sql_data6);

                var sqllf = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA 
                            FROM V_REPORT_RO_OCCUPIED A
                            WHERE A.CONTRACT_END_DATE >= CURRENT_DATE";
                string sql_dataf = sqllf;
                result.LahanCustomerBersertifikat = connection.ExecuteScalar<int>(sql_dataf);

                /*SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_NUMBER, A.VALID_FROM, A.VALID_TO FROM V_REPORT_RO A
                            WHERE A.FUNCTION_ID = 1274 AND A.SERTIFIKAT_OR_NOT = 1 AND A.CONTRACT_NO IS NOT NULL*/
                var sqll7 = @"SELECT COUNT(*) AS TOTAL FROM V_REPORT_RO a 
                LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = a.CONTRACT_NO 
                WHERE b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE OR A.FUNCTION_ID = 1274 OR A.SERTIFIKAT_OR_NOT = 1";
                string sql_data7 = sqll7;
                result.IddleBersertifikatkontrak = connection.ExecuteScalar<int>(sql_data7);

                var sqllg = @"SELECT SUM(a.LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO a 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = a.CONTRACT_NO 
                              WHERE b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE OR A.FUNCTION_ID = 1274 OR A.SERTIFIKAT_OR_NOT = 1";
                string sql_datag = sqllg;
                result.LahanIddleBersertifikatKontrak = connection.ExecuteScalar<int>(sql_datag);

               /* var sqll8 = @"SELECT COUNT(*) AS IddleBersertifikat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO WHERE A.CONTRACT_NO IS NOT NULL AND A.VALID_TO BETWEEN TRUNC(SYSDATE,'YEAR')
                AND CURRENT_DATE AND b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE";*/
                var sqll8 = @"SELECT COUNT(*) AS TOTAL FROM V_REPORT_RO_VACANT a 
                WHERE a.CONTRACT_END_DATE < CURRENT_DATE AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                string sql_data8 = sqll8;
                result.IddleBersertifikatnokontrak = connection.ExecuteScalar<int>(sql_data8);

               /* var sqllh = @"SELECT SUM(LAND_DIMENSION) AS TOTAL FROM V_REPORT_RO A 
                            LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO WHERE A.CONTRACT_NO IS NOT NULL AND A.VALID_TO BETWEEN TRUNC(SYSDATE,'YEAR')
                            AND CURRENT_DATE AND b.CONTRACT_END_DATE BETWEEN TRUNC(SYSDATE,'YEAR') AND CURRENT_DATE";*/
                var sqllh = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA FROM V_REPORT_RO_VACANT A 
                            WHERE a.CONTRACT_END_DATE < CURRENT_DATE AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                string sql_datah = sqllh;
                result.LahanIddleBersertifikatNoKontrak = connection.ExecuteScalar<int>(sql_datah);

                //result.dataPusatBersertifikat_Belum = hasil_datapusat + hasil_datapusatbersertifikat;
                //list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                //result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;

        }

        public Results DetailPenggunaanLahan(SearchDashboardLahan data, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            string where = "";
            string where1 = "";
            string where2 = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global();
            try
            {
                where1 += (data.BRANCH_ID != null && data.BRANCH_ID != null ? " AND A.BE_ID = " + data.BRANCH_ID : "");
                where += (data.end != null && data.end != null ? " AND b.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE ('" + data.end + "', 'DD/MM/RRRR')" : "");
                if (data.end != "" && data.BRANCH_ID != "")
                {
                    var sql = @"SELECT COUNT(*) AS Pusat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data = sql + where;
                    result.dataPusat = connection.ExecuteScalar<int>(sql_data);

                    var sqla = @"SELECT SUM(LAND_DIMENSION) AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string lahan_pusat = sqla + where;
                    result.LahanPusat = connection.ExecuteScalar<int>(lahan_pusat);

                    var sqll4 = @"SELECT COUNT(*) AS PusatBersertifikat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_data5 = sqll4 + where;
                    result.dataPusatBersertifikat = connection.ExecuteScalar<int>(sql_data5);

                    var sqlle = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_datae = sqlle + where;
                    result.LahanPusatBersertifikat = connection.ExecuteScalar<int>(sql_datae);

                    var sqll = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED a 
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data2 = sqll;
                    result.Customer = connection.ExecuteScalar<int>(sql_data2);

                    var sqllb = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_OCCUPIED A
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_datab = sqllb;
                    result.LahanCustomer = connection.ExecuteScalar<int>(sql_datab);

                    var sqll2 = @"SELECT COUNT(*) AS Iddle FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274 ";
                    string sql_data3 = sqll2;
                    result.Iddlekontrak = connection.ExecuteScalar<int>(sql_data3);

                    var sqllc = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A 
                            LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    string sql_datac = sqllc;
                    result.LahanIddleKontrak = connection.ExecuteScalar<int>(sql_datac);

                    var sqll9 = @"SELECT COUNT(*) AS Iddle  FROM V_REPORT_RO_VACANT a
                     WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')  AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data9 = sqll9;
                    result.Iddlenokontrak = connection.ExecuteScalar<int>(sql_data9);

                    var sqlld = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_VACANT A 
                    WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')  AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_datad = sqlld;
                    result.LahanIddlenoKontrak = connection.ExecuteScalar<int>(sql_datad);

                    var sqll8 = @"SELECT COUNT(*) AS TOTAL FROM V_REPORT_RO_VACANT a 
                     WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                    string sql_data8 = sqll8;
                    result.IddleBersertifikatnokontrak = connection.ExecuteScalar<int>(sql_data8);

                    var sqllh = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA FROM V_REPORT_RO_VACANT A 
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                    string sql_datah = sqllh;
                    result.LahanIddleBersertifikatNoKontrak = connection.ExecuteScalar<int>(sql_datah);

                    var sqll7 = @"SELECT COUNT(*) AS IddleBersertifikat FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID +" " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    string sql_data7 = sqll7;
                    result.IddleBersertifikatkontrak = connection.ExecuteScalar<int>(sql_data7);

                    var sqllg = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A  LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    string sql_datag = sqllg;
                    result.LahanIddleBersertifikatKontrak = connection.ExecuteScalar<int>(sql_datag);
                }
                else if(data.BRANCH_ID !="")
                {
                    var sql = @"SELECT COUNT(*) AS Pusat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data = sql;
                    result.dataPusat = connection.ExecuteScalar<int>(sql_data);

                    var sqla = @"SELECT SUM(LAND_DIMENSION) AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string lahan_pusat = sqla;
                    result.LahanPusat = connection.ExecuteScalar<int>(lahan_pusat);

                    var sqll4 = @"SELECT COUNT(*) AS PusatBersertifikat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_data5 = sqll4;
                    result.dataPusatBersertifikat = connection.ExecuteScalar<int>(sql_data5);

                    var sqlle = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_datae = sqlle;
                    result.LahanPusatBersertifikat = connection.ExecuteScalar<int>(sql_datae);

                    var sqll = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED a 
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data2 = sqll;
                    result.Customer = connection.ExecuteScalar<int>(sql_data2);

                    var sqllb = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_OCCUPIED A
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_datab = sqllb;
                    result.LahanCustomer = connection.ExecuteScalar<int>(sql_datab);

                    var sqll2 = @"SELECT COUNT(*) AS Iddle FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274 ";
                    string sql_data3 = sqll2;
                    result.Iddlekontrak = connection.ExecuteScalar<int>(sql_data3);

                    var sqllc = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A 
                            LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    string sql_datac = sqllc;
                    result.LahanIddleKontrak = connection.ExecuteScalar<int>(sql_datac);

                    var sqll9 = @"SELECT COUNT(*) AS Iddle  FROM V_REPORT_RO_VACANT a
                     WHERE A.BE_ID = " + data.BRANCH_ID + " AND (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data9 = sqll9;
                    result.Iddlenokontrak = connection.ExecuteScalar<int>(sql_data9);

                    var sqlld = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_VACANT A 
                    WHERE A.BE_ID = " + data.BRANCH_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_datad = sqlld;
                    result.LahanIddlenoKontrak = connection.ExecuteScalar<int>(sql_datad);

                    var sqll8 = @"SELECT COUNT(*) AS TOTAL FROM V_REPORT_RO_VACANT a 
                     WHERE A.BE_ID = " + data.BRANCH_ID + " AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                    string sql_data8 = sqll8;
                    result.IddleBersertifikatnokontrak = connection.ExecuteScalar<int>(sql_data8);

                    var sqllh = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA FROM V_REPORT_RO_VACANT A 
                            WHERE A.BE_ID = " + data.BRANCH_ID + " AND (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1)";
                    string sql_datah = sqllh;
                    result.LahanIddleBersertifikatNoKontrak = connection.ExecuteScalar<int>(sql_datah);

                    var sqll7 = @"SELECT COUNT(*) AS IddleBersertifikat FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    string sql_data7 = sqll7;
                    result.IddleBersertifikatkontrak = connection.ExecuteScalar<int>(sql_data7);

                    var sqllg = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A  LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE A.BE_ID = " + data.BRANCH_ID + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    string sql_datag = sqllg;
                    result.LahanIddleBersertifikatKontrak = connection.ExecuteScalar<int>(sql_datag);

                }
                else if(data.end !="")
                {
                    var sql = @"SELECT COUNT(*) AS Pusat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string sql_data = sql+where;
                    result.dataPusat = connection.ExecuteScalar<int>(sql_data);

                    var sqla = @"SELECT SUM(LAND_DIMENSION) AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    string lahan_pusat = sqla+where;
                    result.LahanPusat = connection.ExecuteScalar<int>(lahan_pusat);

                    var sqll4 = @"SELECT COUNT(*) AS PusatBersertifikat FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_data5 = sqll4 + where;
                    result.dataPusatBersertifikat = connection.ExecuteScalar<int>(sql_data5);

                    var sqlle = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    string sql_datae = sqlle + where;
                    result.LahanPusatBersertifikat = connection.ExecuteScalar<int>(sql_datae);

                    var sqll1 = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED a 
                            WHERE (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) AND a.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_data22 = sqll1;
                    result.Customer = connection.ExecuteScalar<int>(sql_data22);

                    var sqllb1 = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_OCCUPIED A
                            WHERE (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) AND a.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_datab1 = sqllb1;
                    result.LahanCustomer = connection.ExecuteScalar<int>(sql_datab1);

                    var sqll21 = @"SELECT COUNT(*) AS Iddle FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE b.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + data.end + "', 'dd/mm/yyyy') AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274 ";
                    string sql_data31 = sqll21;
                    result.Iddlekontrak = connection.ExecuteScalar<int>(sql_data31);

                    var sqllc1 = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A 
                            LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE b.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + data.end + "', 'dd/mm/yyyy') AND  A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    string sql_datac1 = sqllc1;
                    result.LahanIddleKontrak = connection.ExecuteScalar<int>(sql_datac1);

                    var sqll91 = @"SELECT COUNT(*) AS Iddle  FROM V_REPORT_RO_VACANT a
                     WHERE (a.RO_CERTIFICATE_NUMBER IS NULL OR a.SERTIFIKAT_OR_NOT = 0) AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_data91 = sqll91;
                    result.Iddlenokontrak = connection.ExecuteScalar<int>(sql_data91);

                    var sqlld1 = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO_VACANT A 
                    WHERE (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_datad1 = sqlld1;
                    result.LahanIddlenoKontrak = connection.ExecuteScalar<int>(sql_datad1);

                    var sqll81 = @"SELECT COUNT(*) AS TOTAL FROM V_REPORT_RO_VACANT a 
                    WHERE (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1) AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_data81 = sqll81;
                    result.IddleBersertifikatnokontrak = connection.ExecuteScalar<int>(sql_data81);

                    var sqllh1 = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA FROM V_REPORT_RO_VACANT A 
                            WHERE (a.RO_CERTIFICATE_NUMBER IS NOT NULL OR a.SERTIFIKAT_OR_NOT = 1) AND a.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy')";
                    string sql_datah1 = sqllh1;
                    result.LahanIddleBersertifikatNoKontrak = connection.ExecuteScalar<int>(sql_datah1);

                    var sqll7 = @"SELECT COUNT(*) AS IddleBersertifikat FROM V_REPORT_RO A 
                              LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE b.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') " +
                              "AND A.VALID_TO <= TO_DATE('" + data.end + "', 'dd/mm/yyyy') AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    string sql_data7 = sqll7;
                    result.IddleBersertifikatkontrak = connection.ExecuteScalar<int>(sql_data7);

                    var sqllg = @"SELECT SUM(LAND_DIMENSION)  AS TOTAL FROM V_REPORT_RO A  LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO =A.CONTRACT_NO WHERE b.CONTRACT_END_DATE <= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') " +
                                  "AND A.VALID_TO <= TO_DATE('" + data.end + "', 'dd/mm/yyyy') AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    string sql_datag = sqllg;
                    result.LahanIddleBersertifikatKontrak = connection.ExecuteScalar<int>(sql_datag);
                }
                

                if(data.end !="" && data.BRANCH_ID !=""){
                    where2 += @" WHERE A.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE('" + data.end + "', 'dd/mm/yyyy') AND A.BE_ID = ('" + data.BRANCH_ID + "')";
                }
                else if (data.end != "")
                {
                    where2 += @" WHERE A.CONTRACT_START_DATE >= TO_DATE ('" + data.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE('" + data.end + "', 'dd/mm/yyyy')";
                }
                else if (data.BRANCH_ID != "")
                {
                    where2 += @" WHERE A.BE_ID = ('" + data.BRANCH_ID + "')";
                }
                var sqll5 = @"SELECT COUNT(*) AS CusBersertifikat FROM V_REPORT_RO_OCCUPIED A";
                string sql_data6 = sqll5 + where2;
                result.CustomerBersertifikat = connection.ExecuteScalar<int>(sql_data6);

                var sqllf = @"SELECT SUM(CAST(REPLACE(A.LAND_DIMENSION, ',', '.') AS DECIMAL(29, 2))) AA 
                            FROM V_REPORT_RO_OCCUPIED A";
                string sql_dataf = sqllf + where2;
                result.LahanCustomerBersertifikat = connection.ExecuteScalar<int>(sql_dataf);

                //list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                //result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;

        }




        public Results dataRODetail(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            string where = "";
            string join = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global();
            try
            {
                where += (param.jenis != "" && param.jenis != null ? " AND a.STATUS = '" + param.jenis + "'" : "");
                where += (param.BE_ID != "" && param.BE_ID != null ? " AND a.BRANCH_ID = " + param.BE_ID : "");
                where += (param.end != "" && param.end != null ? " and  a.VALID_FROM <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND VALID_TO >= TO_DATE ('" + param.end + "', 'DD/MM/RRRR')" : "");
                if(param.jenis == "OCCUPIED")
                {
                    join = @" left join PROP_CONTRACT b on a.CONTRACT_NO = b.CONTRACT_NO
                            join VW_CUSTOMERS c on b.BUSINESS_PARTNER = c.MPLG_KODE and b.BRANCH_ID = c.KD_CABANG ";
                }
                else if (param.jenis == "BOOKED")
                {
                    join = @" join PROP_RENTAL_REQUEST_DETIL b on a.RO_CODE = b.OBJECT_ID 
                            join PROP_RENTAL_REQUEST d on d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO  and d.CONTRACT_START_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') and d.CONTRACT_END_DATE >= TO_DATE ('" + param.end + @"', 'dd/mm/yyyy')  and d.CONTRACT_OFFER_NUMBER is not null
                            join VW_CUSTOMERS c on d.CUSTOMER_ID = c.MPLG_KODE and d.BRANCH_ID = c.KD_CABANG ";
                }
                var sql = @"select COUNT(*) AS JUMLAH, c.MPLG_BADAN_USAHA from V_REPORT_RO a " + join + @"
                            where a.BRANCH_ID is not null and c.MPLG_BADAN_USAHA is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + where + " group by c.MPLG_BADAN_USAHA";
                list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }
        public Results GetDetailRO(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global_1();
            try
            {
                var sql = "";
                var join = "";
                var select = "";
                var where = " where a.BRANCH_ID is not null";
                where += (param.jenis != "" && param.jenis != null ? " AND a.STATUS = '" + param.jenis + "'" : "");
                where += (param.jenis2 != "" && param.jenis2 != null ? " AND c.MPLG_BADAN_USAHA = '" + param.jenis2 + "'" : "");
                where += (param.BE_ID != "" && param.BE_ID != null ? " AND a.BRANCH_ID = " + param.BE_ID : "");
                where += (param.end != "" && param.end != null ? " AND  a.VALID_FROM <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND VALID_TO >= TO_DATE ('" + param.end + "', 'DD/MM/RRRR')" : "");
                if (param.jenis == "OCCUPIED")
                {
                    join = @" join PROP_CONTRACT b on a.CONTRACT_NO = b.CONTRACT_NO 
                            join VW_CUSTOMERS c on b.BUSINESS_PARTNER = c.MPLG_KODE and b.BRANCH_ID = c.KD_CABANG ";
                    select = " ,c.MPLG_KODE, c.MPLG_NAMA,c.MPLG_BADAN_USAHA ";
                }
                else if (param.jenis == "BOOKED")
                {
                    join = @" join PROP_RENTAL_REQUEST_DETIL b on a.RO_CODE = b.OBJECT_ID 
                            join PROP_RENTAL_REQUEST d on d.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO and d.CONTRACT_START_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') and d.CONTRACT_END_DATE >= TO_DATE ('" + param.end + @"', 'dd/mm/yyyy')  and d.CONTRACT_OFFER_NUMBER is not null
                            join VW_CUSTOMERS c on d.CUSTOMER_ID = c.MPLG_KODE and d.BRANCH_ID = c.KD_CABANG ";
                    select = " ,c.MPLG_KODE, c.MPLG_NAMA,c.MPLG_BADAN_USAHA,d.CONTRACT_OFFER_NUMBER ";
                }
                sql = @"SELECT a.RO_NUMBER, a.RO_CODE, a.RO_NAME, a.ZONE_RIP, a.LAND_DIMENSION, a.BUILDING_DIMENSION, a.RO_CERTIFICATE_NUMBER, a.USAGE_TYPE, a.FUNCTION_NAME, a.STATUS, 
  a.REASON, a.RO_ADDRESS, a.RO_POSTALCODE, a.RO_CITY, a.PROVINCE, a.ACTIVE, a.BE_NAME, a.PROFIT_CENTER, a.BE_ID, a.PROFIT_CENTER_ID, a.ZONE_RIP_ID, a.USAGE_TYPE_ID, 
  a.FUNCTION_ID, a.BRANCH_ID, TO_CHAR(a.VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(a.VALID_TO, 'DD.MM.RRRR') VALID_TO, a.CONTRACT_NO, a.SERTIFIKAT_TYPE
                            " + select + " FROM V_REPORT_RO a " + join + where + " AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d)";
                list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();

                
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results GetDetailExcel(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global_1();
            try
            {
                var sql = "";
                var sqll = "";
                var keyword = "";
                var select = "";
                var where = "";
                var where1 = "";
                
                if (param.jenis2 == "Customer")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0";
                    }
                    else if (param.end != "")
                    {
                        where += @"AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0";
                    }
                    else if (param.end == "")
                    {
                        where += @"AND A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if (param.jenis2 == "KantorPusat")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end != "")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if (param.jenis2 == "Iddley")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if (param.jenis2 == "Iddlex")
                {
                    if (param.end != "" && param.BE_ID != "")
                    {
                        where += @" AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " " + where + "";
                    }
                    else if (param.end == "")
                    {
                        where += @" A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE " + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + "";
                    }
                    else if (param.end != "")
                    {
                        where += @" A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyy";
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE " + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if (param.jenis2 == "CustomerBersertifikat")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end != "")
                    {
                        where += @"AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where += @"AND A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE
                            (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if(param.jenis2 == "KantorPusatBersertifikat")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end != "")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();

                }
                else if (param.jenis2== "CustBersertifikatTidak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + "";
                    }
                    else if (param.end != "")
                    {
                        where += @" A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE
                            " + where + "";
                    }
                    else if (param.end == "")
                    {
                        where += @" A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE
                            " + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else if (param.jenis2== "PstBersertifikatTidak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86";
                    }
                    else if (param.end != "")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 " + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 ";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sqll = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE AND A.FUNCTION_ID = 86 " + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();

                }
                else if (param.jenis2== "IddleBersertifikatkontrak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274" + where;
                    }
                    list = connection.Query<AUP_REPORT_RO>(sql, new { d = kdCabang }).ToList();


                }
                else if (param.jenis2 == "IddleBersertifikatnokontrak")
                {
                    where += @" AND  A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                    if (param.end != "" && param.BE_ID != "")
                    {
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where += @" AND A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE  A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1";
                    }
                    else if (param.end != "")
                    {

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                
                else if (param.jenis2 == "Iddlekontrak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sqll = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();
                }
                else
                {
                    where += @" AND  A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                    if (param.end != "" && param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end == "")
                    {
                        where += @" AND A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end != "")
                    {

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0" + where;
                    }

                    list = connection.Query<AUP_REPORT_RO>(sqll, new { d = kdCabang }).ToList();

                }
                
                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results GetDetailSertifikat(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global_1();
            try
            {
                var sql = "";
                var join = "";
                var select = "";
                var where = "";
                var where1 = "";


                if (param.jenis == "Customer")
                {
                    if (param.BE_ID !="" && param.end !="")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end != "")
                    {
                        where += @"AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)"+where;
                    }else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end == "")
                    {
                        where += @"AND A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    
                    
                }
                else if (param.jenis == "KantorPusat")
                {
                    if (param.BE_ID !="" && param.end !="")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " +where+ " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end !="")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    else if (param.BE_ID !="")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where+ " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }

                    
                }
                else if(param.jenis == "Iddlekontrak")
                {
                    if(param.BE_ID !="" && param.end !="")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID !="")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0) OR A.FUNCTION_ID = 1274" + where;
                    }
                    
                }
                else
                {
                    where += @" AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                    if (param.end !="" && param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " "+where+" AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }
                    else if (param.end == "")
                    {
                        where += @" AND A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)";
                    }else if(param.end !=""){

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE (A.RO_CERTIFICATE_NUMBER IS NULL OR A.SERTIFIKAT_OR_NOT = 0)" + where;
                    }
                    
                }
                

                list = connection.Query<AUP_REPORT_RO>(sql).ToList();


                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results GetDetailSertifikat2(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global_1();
            try
            {
                var sql = "";
                var join = "";
                var select = "";
                var where = "";
                
                if (param.jenis == "CustomerBersertifikat")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end != "")
                    {
                        where += @"AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where += @"AND A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }

                }
                else if (param.jenis == "KantorPusatBersertifikat")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end != "")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }

                }
                else if (param.jenis == "IddleBersertifikatkontrak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1) OR A.FUNCTION_ID = 1274" + where;
                    }

                }
                else
                {

                    where += @" AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                    if (param.end != "" && param.BE_ID != "" && param.jenis == "IddleBersertifikatnokontrak")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " " + where + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end == "")
                    {
                        where += @" AND A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " AND (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)";
                    }
                    else if (param.end != "")
                    {

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE (A.RO_CERTIFICATE_NUMBER IS NOT NULL OR A.SERTIFIKAT_OR_NOT = 1)" + where;
                    }

                }

                list = connection.Query<AUP_REPORT_RO>(sql).ToList();


                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

        public Results GetDetailSertifikat3(SearchDashboard param, string kdCabang)
        {
            Results result = new Results();
            List<AUP_REPORT_RO> list = new List<AUP_REPORT_RO>();
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global_1();
            try
            {
                var sql = "";
                var join = "";
                var select = "";
                var where1 = "";
                var where = "";
                
                if (param.jenis == "CustBersertifikatTidak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where += @"A.BE_ID = " + param.BE_ID + " AND A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A  WHERE
                            " + where + "";
                    }
                    else if (param.end != "")
                    {
                        where += @" A.CONTRACT_START_DATE >= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE" + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where += @" A.BE_ID = " + param.BE_ID + " ";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE
                            " + where + "";
                    }
                    else if (param.end == "")
                    {
                        where += @" A.CONTRACT_END_DATE >= CURRENT_DATE ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE, A.TERM_IN_MONTHS, A.BUSINESS_PARTNER_NAME, TO_CHAR(A.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE , TO_CHAR(A.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE FROM V_REPORT_RO_OCCUPIED A WHERE
                            " + where;
                    }

                }
                else if (param.jenis == "PstBersertifikatTidak")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86";
                    }
                    else if (param.end != "")
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.FUNCTION_ID = 86 " + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + "  ";
                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.FUNCTION_ID = 86 ";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT A.RO_NAME, A.RO_ADDRESS, A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE AND A.FUNCTION_ID = 86 " + where;
                    }

                }
                else if (param.jenis == "Iddley")
                {
                    if (param.BE_ID != "" && param.end != "")
                    {
                        where = @" A.BE_ID = " + param.end + " AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.BE_ID != "")
                    {
                        where = @" A.BE_ID = " + param.BE_ID + " ";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE " + where + " AND A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274";
                    }
                    else if (param.end == "")
                    {
                        where = @" AND b.CONTRACT_END_DATE >= CURRENT_DATE";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274" + where;
                    }
                    else
                    {
                        where = @" AND b.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyyy') AND A.VALID_TO <= TO_DATE('" + param.end + "', 'dd/mm/yyyy')";

                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO A LEFT JOIN PROP_CONTRACT b ON b.CONTRACT_NO = A.CONTRACT_NO
                            WHERE A.CONTRACT_NO IS NULL OR A.FUNCTION_ID = 1274" + where;
                    }

                } 
                else
                {
                    
                    if (param.end != "" && param.BE_ID != "")
                    {
                        where += @" AND A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyy";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + " " + where + "";
                    }
                    else if (param.end == "")
                    {
                        where += @" A.CONTRACT_END_DATE >= CURRENT_DATE";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE " + where;
                    }
                    else if (param.BE_ID != "")
                    {
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE A.BE_ID = " + param.BE_ID + "";
                    }
                    else if (param.end != "")
                    {
                        where += @" A.CONTRACT_END_DATE <= TO_DATE ('" + param.end + "', 'dd/mm/yyy";
                        sql = @"SELECT DISTINCT A.RO_NAME, A.RO_ADDRESS,A.LAND_DIMENSION, A.RO_CERTIFICATE_NUMBER, A.KODE_ASET, A.ZONE_RIP, 
                            A.CONTRACT_NO, A.RO_CODE FROM V_REPORT_RO_VACANT A 
                           WHERE " + where;
                    }

                }

                list = connection.Query<AUP_REPORT_RO>(sql).ToList();


                result.Status = "S";
                result.Data = list;
            }
            catch (Exception ex)
            {
                result.Status = "E";
                result.Data = ex;
            }
            connection.Close();
            connection.Dispose();
            return result;
        }


        public Results GetDataRkap(string kdCabang,int triwulan, int tahun)
        {
            Results result = new Results();
            //int r = 0;
            int a = 0;
            int b = 0;
            //int c = 0;
            //int d = 0;
            List<DataRkapDetail> list = new List<DataRkapDetail>();
            //string where = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string v_be = global();
            if (triwulan == 1)
            {
                a = 1;
                b = 3;
            }else if (triwulan == 2)
            {
                a = 4;
                b = 6;
            }
            else if (triwulan == 3)
            {
                a = 7;
                b = 9;
            }
            else if (triwulan == 4)
            {
                a = 10;
                b = 12;
            }

            try
            {
               
                var sql = @"select nvl(luas_triwulan1_tahun_ini,0) tritahunini ,nvl(luas_triwulan1_tahun_lalu,0) tritahunlalu,coa_produksi,urutan from (

                                select luas_triwulan1_tahun_ini,luas_triwulan1_tahun_lalu,a.coa_produksi,1 urutan 
                                from (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_ini,'lahan' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO

                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070100000000000000' and '4070109999999999999' 
                                order by posting_date asc
                                ) a
                                join (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_lalu,'lahan' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO

                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070100000000000000' and '4070109999999999999' 
                                order by posting_date asc) b on a.coa_produksi=b.coa_produksi
                                union
                                select luas_triwulan1_tahun_ini,luas_triwulan1_tahun_lalu,a.coa_produksi ,2 urutan
                                from (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_ini,'air' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO

                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070200000000000000' and '4070209999999999999' 
                                order by posting_date asc
                                ) a
                                join (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_lalu,'air' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO
                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                               where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070200000000000000' and '4070209999999999999' 
                                order by posting_date asc) b on a.coa_produksi=b.coa_produksi
                                union
                                select luas_triwulan1_tahun_ini,luas_triwulan1_tahun_lalu,a.coa_produksi ,3 urutan
                                from (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_ini,'bangunan' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO

                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070300000000000000' and '4070309999999999999' 
                                order by posting_date asc
                                ) a
                                join (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_lalu,'bangunan' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO
                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070300000000000000' and '4070309999999999999' 
                                order by posting_date asc) b on a.coa_produksi=b.coa_produksi
                                union
                                select luas_triwulan1_tahun_ini,luas_triwulan1_tahun_lalu,a.coa_produksi ,4 urutan
                                from (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_ini,'barang' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE ,c.BRANCH_ID
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO

                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070400000000000000' and '4070409999999999999' 
                                order by posting_date asc
                                ) a
                                join (
                                select sum(nvl(luas,0)) luas_triwulan1_tahun_lalu,'barang' as coa_produksi from 
                                (select a.id id_a,nvl(b.LUAS,0) LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID 
                                from V_CONTRACT_LIST_BILLING a
                                join PROP_CONTRACT_CONDITION b on a.BILLING_CONTRACT_NO=b.CONTRACT_NO
                                join PROP_CONTRACT c on c.CONTRACT_NO=a.BILLING_CONTRACT_NO
                                where b.COA_PROD is not null and b.COA_PROD !='-' and a.SAP_DOC_NO is not null and c.BRANCH_ID=:d
                                group by a.id,b.LUAS,b.COA_PROD,a.POSTING_DATE,c.BRANCH_ID
                                ) 
                                where extract(month from POSTING_DATE) between :a and :b 
                                and extract(year from POSTING_DATE)=:c 
                                 and coa_prod between '4070400000000000000' and '4070409999999999999' 
                                order by posting_date asc) b on a.coa_produksi=b.coa_produksi

                                ) table_realisasi order by urutan asc";//D00 lanjutan numeric yang gak terpakai
                list = connection.Query<DataRkapDetail>(sql, new { a=a,b=b,c=tahun,d = kdCabang }).ToList();

                result.Status = "S";
                result.Positionlist = list;
            }
            catch (Exception)
            {
                result.Status = "E";
            }
            connection.Close();
            connection.Dispose();
            return result;
        }

    }
}