﻿using Dapper;
using Remote.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using Remote.Helpers;
using Remote.Models;
using RestSharp;
using Microsoft.AspNetCore.Http;

namespace Remote.DAL 
{
    public class AuthDAL
    {
        public int _APP_ID = 1;

        public APP_USER GetUserInformation(string username, string password, string generateID)
        {
            
            //System.Web.HttpCookie UserInfo = new System.Web.HttpCookie("UserInfo");
           // string gen = Guid.NewGuid().ToString("N");
           // UserInfo["CookiesID"] = gen;
          //  UserInfo.Expires = DateTime.Now.AddDays(2);
          //  HttpContext.Current.Response.Cookies.Set(UserInfo);

            string xNamaCabang = string.Empty;
            string xPROFIT_CENTER = "";
            string xUSER_NAMA = "";
            string xROLE_NAME = "";
            string _password = string.Empty;
            string _unProtect = string.Empty;

            APP_USER result = new APP_USER();

            UserAkunInfoNew xAkun = new UserAkunInfoNew();
            //APP_USER xAkun = new APP_USER();//development


            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                result = new APP_USER();
                result.ID = 9999999;
                result.USER_LOGIN = "0001";
                result.USER_NAMA = "User name or password cannot be empty.";
                result.USER_ROLE_ID = "99999";
            } else
            {
                xAkun = AuthenticateToMiddleware("93", username, password);
               //xAkun = ceklogindev(username, password);//development

                if (xAkun != null)
                {
                    if (xAkun.HAKAKSES != null)
                    {
                        xPROFIT_CENTER = xAkun.PROFIT_CENTER;
                        xNamaCabang = xAkun.NAMA_CABANG;
                        xUSER_NAMA = xAkun.NAMA;

                        //Insert LOG User LOGIN --
                        using (var connection = DatabaseFactory.GetConnection())
                        {
                            try
                            {
                                string sql =
                                            "INSERT INTO LOGIN_LOG " +
                                            "(USER_LOGIN, DATE_LOGIN, SESSION_ID) VALUES (:a, sysdate, :b)";
                                result = connection.Query<APP_USER>(sql, new
                                {
                                    a = username,
                                    b = "test"

                                }).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                string bbbb = ex.ToString();
                            }
                        }
                        //--!

                        using (var con = DatabaseFactory.GetConnection("app_repo"))
                        {
                            if (con.State.Equals(ConnectionState.Closed))
                                con.Open();
                            try
                            {
                                //string sql =
                                //    "SELECT ID, USER_LOGIN, USER_PASSWORD, USER_ROLE_ID, KD_CABANG, KD_TERMINAL, '" +
                                //    xPROFIT_CENTER + "' PROFIT_CENTER, '" + xUSER_NAMA + "' USER_NAMA " +
                                //    "FROM VW_APP_USER WHERE user_login = :a and APP_ID=:APP_ID";
                                string sql =
                                   "SELECT * " +
                                   "FROM VW_APP_USER " +
                                   "WHERE USER_LOGIN = :A AND APP_ID=:APP_ID";
                                result = con.Query<APP_USER>(sql, new
                                {
                                    a = username,
                                    APP_ID = _APP_ID
                                }).FirstOrDefault();
                                if (result != null)
                                {
                                    result.NAMA_CABANG = xNamaCabang;
                                    result.PROFIT_CENTER = xPROFIT_CENTER;
                                    result.USER_NAMA = xUSER_NAMA;
                                    result.ROLE_NAME = result.ROLE_NAME;
                                }
                            }
                            catch (Exception ex)
                            {
                                string aaa;
                                aaa = ex.ToString();
                            }
                            finally
                            {
                                con.Close();
                            }
                        }
                        // IDbConnection connection = ServiceConfig.AppRepoDbConnection;

                    }
                    else
                    {
                        result = new APP_USER();
                        result.ID = 9999999;
                        result.USER_LOGIN = xAkun.kode;
                        result.USER_NAMA = xAkun.pesan;
                        result.USER_ROLE_ID = "99999";
                    }
                } else
                {
                    result.ID = 9999999;
                    result.USER_LOGIN = "Tidak Bisa Konek KE Portalsi";
                    result.USER_NAMA = xAkun.pesan;
                }
                
            }

            return result;
        }

        private UserAkunInfoNew AuthenticateToMiddleware(string applicationId, string username, string password)
        {
            //UserAkunInfoNew userAkunInfo = null;
            UserAkunInfoNew userAkunInfo = new UserAkunInfoNew();

            //string mwUrl = "http://eap-prsi.pelindo.co.id/portalsi-ws/portalsi/loginVal";
            string mwUrl = "http://eap-prsi-19c.pelindo.co.id/portalsi-ws/portalsi/loginVal";
            RestClient client = new RestClient(mwUrl);
            RestRequest request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            MiddlewareCredentials cred = new MiddlewareCredentials
            {
                application_id = applicationId,
                user_name = username,
                user_password = password
            };

            request.AddBody(cred);


            IRestResponse<UserAkunInfoNew> restResponse = client.Execute<UserAkunInfoNew>(request);

            userAkunInfo = restResponse.Data;

            return userAkunInfo;
        }

        internal List<VW_MENU_ACTIVITIES> GetUserActivities(string nIPP)
        {
            List<VW_MENU_ACTIVITIES> listActivity = null;
            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    var sql = "SELECT * FROM vw_menu_activities WHERE user_login = :a and APP_ID=:APP_ID";
                    listActivity = connection.Query<VW_MENU_ACTIVITIES>(sql,
                        new
                        {
                            a = nIPP,
                            APP_ID = _APP_ID
                        }).ToList();
                }
                catch (Exception)
                {
                }
                finally
                { connection.Close(); }
            }
            return listActivity;

        }

        public List<MainMenu> GenerateMainMenuList(string UserId)
        {
            var sql = "SELECT U.KD_CABANG, U.ID, U.USER_LOGIN, R.ROLE_ID, R.ROLE_NAME, M.MENU_NAMA, M.MENU_URL, M.MENU_ICON, M.MENU_ACTIVITY, MP.PERMISSION_ADD, MP.PERMISSION_DEL, MP.PERMISSION_UPD, MP.PERMISSION_VIW, (SELECT COUNT (-1) FROM APP_MENU XM, APP_USER_MENU XUM WHERE XM.MENU_ID = XUM.MENU_ID AND XUM.USER_ID = U.ID AND XM.APP_ID = XUM.APP_ID AND XUM.MENU_PARENT_ID = M.MENU_ID AND XUM.STATUS = 1) AS CHILD_COUNT, M.MENU_ID, UM.MENU_PARENT_ID, LEVEL AS LEVEL_ID, R.APP_ID,um.ordered_by FROM (SELECT * FROM APP_USER where ID=:ID AND APP_ID=:APP_ID) U, APP_ROLE R, APP_USER_ROLE UR, APP_MENU_PERMISSION MP, (SELECT * FROM APP_MENU WHERE MENU_VISIBLE = 1 AND MENU_STATUS = 1) M, (SELECT * FROM APP_USER_MENU WHERE STATUS = 1) UM WHERE U.ID = UR.USER_ID AND U.APP_ID = UR.APP_ID AND UR.ROLE_ID = R.ROLE_ID AND UR.APP_ID = R.APP_ID AND R.APP_ID = MP.APP_ID AND MP.MENU_ID = M.MENU_ID AND MP.ROLE_ID = R.ROLE_ID AND MP.APP_ID = M.APP_ID AND M.MENU_ID = UM.MENU_ID AND M.APP_ID = UM.APP_ID AND U.ID = UM.USER_ID AND U.APP_ID = UM.APP_ID  and UM.ROLE_ID = R.ROLE_ID START WITH MENU_PARENT_ID = 0 CONNECT BY PRIOR MP.MENU_ID = UM.MENU_PARENT_ID ORDER SIBLINGS BY ID, USER_LOGIN,ORDERED_BY ";
            //var sql = "SELECT KD_CABANG, ID, USER_LOGIN, ROLE_ID, ROLE_NAME, MENU_NAMA, MENU_URL, MENU_ICON, MENU_ACTIVITY, PERMISSION_ADD, " +
            //          "PERMISSION_DEL, PERMISSION_UPD, PERMISSION_VIW, CHILD_COUNT, MENU_ID, MENU_PARENT_ID, LEVEL_ID FROM VW_MENU WHERE ID=:ID and APP_ID=:APP_ID ";

            //BACKUP QUERY

            //            SELECT U.KD_CABANG, U.ID, U.USER_LOGIN, R.ROLE_ID, R.ROLE_NAME, M.MENU_NAMA, M.MENU_URL, M.MENU_ICON, M.MENU_ACTIVITY, MP.PERMISSION_ADD, 
            //MP.PERMISSION_DEL, MP.PERMISSION_UPD, MP.PERMISSION_VIW, (SELECT COUNT (-1) FROM APP_MENU XM, APP_USER_MENU XUM 
            //WHERE XM.MENU_ID = XUM.MENU_ID AND XUM.USER_ID = U.ID AND XM.APP_ID = XUM.APP_ID AND XUM.MENU_PARENT_ID = M.MENU_ID AND XUM.STATUS = 1) AS CHILD_COUNT, 
            //M.MENU_ID, UM.MENU_PARENT_ID, LEVEL AS LEVEL_ID, R.APP_ID,um.ordered_by FROM (SELECT * FROM APP_USER where ID=:ID AND APP_ID=:APP_ID) U, 
            //APP_ROLE R, APP_USER_ROLE UR, APP_MENU_PERMISSION MP, (SELECT * FROM APP_MENU WHERE MENU_VISIBLE = 1 AND MENU_STATUS = 1) M,
            // (SELECT * FROM APP_USER_MENU WHERE STATUS = 1) UM WHERE U.ID = UR.USER_ID AND U.APP_ID = UR.APP_ID AND UR.ROLE_ID = R.ROLE_ID AND UR.APP_ID = R.APP_ID 
            // AND R.ROLE_ID = MP.ROLE_ID AND R.APP_ID = MP.APP_ID AND MP.MENU_ID = M.MENU_ID AND MP.APP_ID = M.APP_ID AND M.MENU_ID = UM.MENU_ID 
            List<MainMenu> res = null;
            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();

                try
                {
                    res = connection.Query<MainMenu>(sql, new
                    {
                        ID = UserId,
                        APP_ID = _APP_ID
                    }).ToList();
                }
                catch (Exception)
                {
                }
                finally
                {
                    connection.Close();
                }
            }
            // IDbConnection connection = ServiceConfig.AppRepoDbConnection;


            return res;
        }

        public VW_APP_USER_MENU GetSingleMenu(string userId, int menuId)
        {
            var sql = "Select * from VW_APP_USER_MENU WHERE USER_ID = :USER_ID AND MENU_ID=:MENU_ID AND MENU_GROUP_ID=1";
            //IDbConnection connection = ServiceConfig.AppRepoDbConnection;
            VW_APP_USER_MENU res = null;
            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    res = connection.Query<VW_APP_USER_MENU>(sql,
                        new
                        {
                            USER_ID = userId,
                            MENU_ID = menuId
                        }).SingleOrDefault();
                }
                catch (Exception)
                {
                }
                finally
                {
                    connection.Close();
                }

            }
            return res;
        }

        public VW_APP_USER_MENU GetSingleHeaderMenu(string userId, int menuId)
        {
            var sql = "Select * from VW_APP_USER_MENU WHERE USER_ID = :USER_ID AND MENU_ID=:MENU_ID AND MENU_GROUP_ID=1";
            // IDbConnection connection = ServiceConfig.AppRepoDbConnection;

            VW_APP_USER_MENU res = null;
            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    res = connection.Query<VW_APP_USER_MENU>(sql,
                        new
                        {
                            USER_ID = userId,
                            MENU_ID = menuId
                        }).SingleOrDefault();
                }
                catch (Exception)
                {
                }
                finally
                { connection.Close(); }
            }
            return res;

        }

        public APP_USER ceklogindev(string username, string password)
        {
            APP_USER result = new APP_USER();
            LoginDev checkexist;
            string TERMINAL_NAME = "";
            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                string sql = @"SELECT ROLE_NAME HAKAKSES, BE_NAME NAMA_CABANG, USER_NAME NAMA, BE_ID, USER_LOGIN
                               FROM VW_APP_USER
                               WHERE USER_LOGIN = :A AND USER_PASSWORD=:b and APP_ID =:APP_ID";
                checkexist = connection.Query<LoginDev>(sql, new
                {
                    a = username,
                    b = password,
                    APP_ID = 1
                }).FirstOrDefault();
                if (checkexist != null)
                {
                    using (var connection1 = new OracleConnection(DatabaseHelper.GetConnectionString("default")))
                    {
                        sql = "select TERMINAL_NAME from PROFIT_CENTER where BE_ID =:a and ROWNUM = 1 AND STATUS = 1 ";
                        TERMINAL_NAME = connection1.ExecuteScalar<string>(sql, new
                        {
                            a = checkexist.BE_ID
                        });
                    }

                    result.HAKAKSES = checkexist.HAKAKSES;
                    result.NAMA_CABANG = checkexist.NAMA_CABANG;
                    result.USER_NAMA = checkexist.NAMA;
                    result.PROFIT_CENTER = TERMINAL_NAME;
                    result.USER_LOGIN = checkexist.USER_LOGIN;
                    result.NAMA = checkexist.NAMA;
                }
            }


            return result;
        }
    }
}