﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Chester_it21;
using Oracle.ManagedDataAccess.Client;
using Remote.Helper;
using Remote.Helpers;

namespace Remote.DAL
{
    public class DatabaseHelper
    {
        public static string GetConnectionString(string name = "default")
        {
            Chester chester = new Chester();
            //var connectionStringEncrypted = ConfigurationFactory.GetConfiguration("ConnectionStrings", name);
            var connectionString = ConfigurationFactory.GetConfiguration("ConnectionStrings", name);

            return (connectionString);
        }

        public static string Query(string ConnName, string sql, params string[] parameter)
        {
            using (var connection = DatabaseFactory.GetConnection(ConnName))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {

                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return String.Empty;
            }
        }

        public static string GetPenomoranData(string BEId, string ModuleType, string varParam1, int PaddingLength)
        {
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            string ret = string.Empty;
            using (var connection = DatabaseFactory.GetConnection())
            {
                //connection.Open();
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                OracleCommand cmd = (OracleCommand)connection.CreateCommand();
                cmd.CommandText = "GENERATE_NOMOR_TRANS";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;
                cmd.Parameters.Add("returnVal", OracleDbType.Varchar2, RETURN_VALUE_BUFFER_SIZE);
                cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                OracleParameter pBE_ID = new OracleParameter("P_BE_ID", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = BEId,
                    Size = 100
                };
                cmd.Parameters.Add(pBE_ID);

                OracleParameter P_MODULE_TYPE = new OracleParameter("P_MODULE_TYPE", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = ModuleType,
                    Size = 100
                };
                cmd.Parameters.Add(P_MODULE_TYPE);
                OracleParameter pPARAM1 = new OracleParameter("P_PARAM1", OracleDbType.Varchar2,
                    ParameterDirection.Input)
                {
                    Value = varParam1,
                    Size = 100
                };
                cmd.Parameters.Add(pPARAM1);
                //cmd.Parameters.Add(P_MODULE_TYPE);
                OracleParameter P_PADDING = new OracleParameter("P_PADDING", OracleDbType.Int16,
                    ParameterDirection.Input)
                {
                    Value = PaddingLength,
                    Size = 100
                };
                cmd.Parameters.Add(P_PADDING);
                try
                {
                    cmd.ExecuteNonQuery();
                    ret = cmd.Parameters["returnVal"].Value.ToString();
                }
                catch (Exception ex)
                {
                    ret = string.Empty;
                    string aaa = ex.ToString();
                }
                finally
                {
                    connection.Close();
                    cmd.Dispose();
                }
            }
            return ret;
        }
    }
}