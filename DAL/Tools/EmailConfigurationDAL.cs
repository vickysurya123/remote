﻿
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.EmailConfiguration;
using Remote.Models.GeneralResult;
using Remote.Models.ListNotifikasi;
using Remote.Models.PendingList;
using Remote.Models.UserModels;
using Remote.Helper;
using Remote.Models;
using System.Globalization;
using Remote.Controllers;

namespace Remote.DAL
{
    public class EmailConfigurationDAL : Controller
    {

        public string link = "Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a> untuk melihat detail isi permohonan kontrak<br>";

        private readonly IHttpClientFactory _clientFactory;

        //public EmailConfigurationDAL(IHttpClientFactory clientFactory)
        //{
        //    _clientFactory = clientFactory;
        //}

        public Results SendMail(EmailConf data)
        {
            Results result = new Results();
            //panggil procedur kirim email

            try
            {
                //buildhtml
                string html = @"<html>
                                    <head>
                                        <meta http-equiv='Content - Type'content='text / html; charset = us - ascii'>
                                        <style>#p01 {color: black; font-family: verdana;font-size:16px;}table{border-collapse: collapse;} .t01 td,th {border: 1px solid black;}
                                        </style>
                                    </head>
                                    <body style='font - family:verdana; font - size:13px'>
                                        " + data.BODY + @" 
                                    </body>
                                </html>";

                using (var connection = DatabaseFactory.GetConnection("default_live"))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    var parameter = new DynamicParameters();

                    switch (data.CASE)
                    {
                        case "1": // user

                            string cc = data.GROUP_CC != null ? string.Join(", ", data.GROUP_CC) : "";
                            string bcc = data.GROUP_BCC != null ? string.Join(", ", data.GROUP_BCC) : "";

                            parameter.Add(":p_to", data.EMAIL_TO);
                            parameter.Add(":p_cc", cc);
                            parameter.Add(":p_bcc", bcc);
                            parameter.Add(":p_subject", data.SUBJECT);
                            parameter.Add(":p_text_msg", data.DESC);
                            parameter.Add(":p_html_msg", html);

                            break;
                    }

                    connection.Execute("SEND_MAIL_HTML", parameter, commandType: CommandType.StoredProcedure);

                    result.Status = "S";
                    result.Msg = "Sukses";
                }
            }
            catch (Exception ex)
            {
                result.Status = "E";

                result.Msg = "Failed Send Mail to. " + ex.Message;
            }

            return result;
        }

        public Results SendMailAPI(EmailConf data)
        {
            Results result = new Results();
            // kirim email via API
            string url = Helper.ConfigurationFactory.GetConfiguration("urlEmailService", "url");

            try
            {
                //buildhtml
                string html = @"<html>
                                    <head>
                                        <meta http-equiv='Content - Type'content='text / html; charset = us - ascii'>
                                        <style>#p01 {color: black; font-family: verdana;font-size:16px;}table{border-collapse: collapse;} .t01 td,th {border: 1px solid black;}
                                        </style>
                                    </head>
                                    <body style='font - family:verdana; font - size:13px'>
                                        " + data.BODY + @" 
                                    </body>
                                </html>";

                using (var connection = DatabaseFactory.GetConnection("default"))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    var service = new RestClient(url);
                    var request = new RestRequest("/api/MailService", Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.RequestFormat = DataFormat.Json;

                    //blm th multi recepient, cc dan bcc
                    object mail = new
                    {
                        emailTo = data.EMAIL_TO,
                        subject = data.SUBJECT,
                        body = html,
                        apiKey = "201"
                    };

                    var param = JsonConvert.SerializeObject(mail);
                    request.AddParameter("text/json", param, ParameterType.RequestBody);

                    // execute the request
                    IRestResponse response = service.Execute(request);
                    var content = response.Content; // raw content as string

                    result.Status = "S";
                    result.Msg = "Sukses";
                }
            }
            catch (Exception)
            {
                result.Status = "E";
                result.Msg = "Failed Send Mail to";
            }

            return result;
        }

        public Results SearchUserRole(int id, string kodeCabang, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param, int apvDetail = 0, dynamic dataSurat = null, string namaEmpl = null, string nameRole = null, dynamic isType = null, dynamic userLogin = null)
        {
            Results result = new Results();
            IEnumerable<DataUser> res = null;
            var subject = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Released" : (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1" ? "Approved" : data.COMPLETED_STATUS == "3" ? "Revised" : "Rejected"));
            var approval = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Mohon dilakukan Approval atas nomor permohonan tersebut<br>" : (data.COMPLETED_STATUS == "2" ? "Mohon dilakukan Approval atas nomor permohonan tersebut<br>" : "<br>"));
            try
            {
                IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                string sql = "";
                if(nameRole == null)
                {
                    sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME, a.USER_LOGIN from VW_APP_USER a
                            where a.PROPERTY_ROLE in (:a) and a.KD_CABANG in (
                                select distinct branch_where from REMOTE.V_BE where branch_id in (:b)
                            )  and a.APP_ID = 1 and a.STATUS = 1 ";
                    res = connection.Query<DataUser>(sql, new
                    {
                        a = id,
                        b = kodeCabang
                    });
                } else
                {
                    sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME, a.USER_LOGIN from VW_APP_USER a
                            where a.PROPERTY_ROLE in (:a)  and a.APP_ID = 1 and a.STATUS = 1 and a.role_name = :b ";
                    res = connection.Query<DataUser>(sql, new
                    {
                        a = id,
                        b = nameRole
                    });
                }

                TestEmail t = new TestEmail();

                EmailConf email = new EmailConf();
                if (isType == null)
                {
                    isType = 2;
                }
                if (res.Count() > 0)
                {
                    string roleName = "";
                    List<string> bcc = new List<string>();
                    List<string> cc = new List<string>();
                    foreach (DataUser temp in res)
                    {
                        roleName = temp.ROLE_NAME;
                        if (!string.IsNullOrWhiteSpace(temp.USER_EMAIL))
                        {
                            string email_to = (string.IsNullOrEmpty(t.TEST_EMAIL_APPROVE()) ? temp.USER_EMAIL : t.TEST_EMAIL_APPROVE());

                            //if (!string.IsNullOrWhiteSpace(email.EMAIL_TO))
                            //{
                                // user lebih dari satu akan dimasukkan BCC
                                bcc.Add(email_to);
                            //}
                            //else
                            //{
                            //    // user pertama sebagai penerima
                            //    email.EMAIL_TO = email_to;
                            //}

                            //if (string.IsNullOrWhiteSpace(email.DESC))
                            //{
                                email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                            //}
                            
                            ListNotifikasiController listNotif = new ListNotifikasiController();
                            ParamNotification paramNotif = new ParamNotification();
                            paramNotif.username = temp.USER_LOGIN;
                            if(isType == 2)
                            {
                                paramNotif.type = "1"; //TYPE APPROVAL
                            } else
                            {
                                paramNotif.type = "6"; //MELAKUKAN DISPOSISI
                            }
                            paramNotif.notification = param.APPROVAL_NOTE;
                            paramNotif.no_contract = data.CONTRACT_OFFER_NO;
                            paramNotif.pengirim = userLogin;
                            var sendNotif = listNotif.addNewNotif(paramNotif);
                        }

                    }

                    var body = isiSurat(dataSurat, roleName, isType, namaEmpl, param.APPROVAL_NOTE, data.CREATION_BY);
                    body += bodyheader(roleName, data, type, param);
                    body += bodydetail(data.CONTRACT_OFFER_NO);
                    body += link;
                    body += approval;
                    body += DataCabang(kodeCabang);
                    body += emailfooter();
                    email.GROUP_BCC = bcc.Distinct();
                    email.GROUP_CC = cc.Distinct();
                    email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                    email.BODY = body;
                    email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                    var sendingemail = SendMail(email);

                    connection.Close();
                    connection.Dispose();

                    result.Msg = "Sukses kirim email. (0)";
                    result.Status = "S";

                    if (dataSurat.TEMBUSAN != null)
                    {
                        var tembusan = emailTembusan(data, dataSurat, body);
                    }

                }
                else
                {
                    namaEmpl = ""; dynamic emailto = null; string note = "";
                    dynamic dataParaf;
                    using (IDbConnection conn = DatabaseFactory.GetConnection("default"))
                    {
                        string cariDataParaf = @"SELECT A.USER_ID, B.BRANCH_ID FROM PROP_DISPOSISI_PARAF A
                            JOIN PROP_SURAT B on b.ID = A.SURAT_ID
                            WHERE A.CONTRACT_NO =:a AND IS_ACTION = 0 AND IS_CREATE_SURAT = 0";
                        dataParaf = conn.Query<dynamic>(cariDataParaf, new
                        {
                            a = data.CONTRACT_OFFER_NO
                        }).FirstOrDefault();
                    }

                    string cariData = @"SELECT A.KD_CABANG, A.ROLE_NAME, A.STATUS, A.USER_ROLE_ID, A.USER_EMAIL, A.USER_NAME, A.USER_LOGIN FROM VW_APP_USER A
                        WHERE A.PROPERTY_ROLE IN (:a) AND A.KD_CABANG IN (
                        SELECT DISTINCT BRANCH_WHERE FROM REMOTE.V_BE WHERE BRANCH_ID IN (:b)
                        )  AND A.APP_ID = 1 AND A.STATUS = 1";
                    emailto = connection.Query<dynamic>(cariData, new
                    {
                        a = id,
                        b = dataParaf.BRANCH_ID,
                    }).FirstOrDefault();

                    var body = isiSurat(dataSurat, emailto.ROLE_NAME, 5, emailto.ROLE_NAME, param.APPROVAL_NOTE, data.CREATION_BY);
                    body += bodyheader(emailto.ROLE_NAME, data, type, param);
                    body += bodydetail(data.CONTRACT_OFFER_NO);
                    body += link;
                    body += approval;
                    body += DataCabang(kodeCabang);
                    body += emailfooter();

                    email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? emailto.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                    email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                    email.BODY = body;
                    email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                    var sendingemail = SendMail(email);
                    result.Msg = "Sukses kirim email";
                    result.Status = "S";

                    if (dataSurat.TEMBUSAN != null)
                    {
                        var tembusan = emailTembusan(data, dataSurat, body);
                    }

                    //send notif mobile
                    ListNotifikasiController listNotif = new ListNotifikasiController();
                    ParamNotification paramNotif = new ParamNotification();
                    paramNotif.username = emailto.USER_LOGIN;
                    paramNotif.type = "1"; //TYPE APPROVE
                    paramNotif.notification = param.APPROVAL_NOTE;
                    paramNotif.no_contract = data.CONTRACT_OFFER_NO;
                    paramNotif.pengirim = userLogin == null ? emailto.USER_LOGIN : userLogin;
                    var sendNotif = listNotif.addNewNotif(paramNotif);

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }

            return result;
        }

        public string bodyheader(string rolename, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param)
        {
            string res = "";
            var sql = "";
            string offer_status = "";
            var top = "";
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1")
                {
                    sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else if (data.COMPLETED_STATUS == "3")
                {
                    sql = @"SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else
                {
                    sql = @"SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                offer_status = connection.ExecuteScalar<string>(sql, new
                {
                    a = data.OFFER_STATUS
                });

            }

            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {

                if (data.TOP_APPROVED_LEVEL == "50")
                {
                    top = "CEO REGIONAL";
                }
                else
                {
                    sql = @"select ROLE_NAME from APP_ROLE where PROPERTY_ROLE = :a and APP_ID = 1";

                    top = connection.ExecuteScalar<string>(sql, new
                    {
                        a = data.TOP_APPROVED_LEVEL
                    });
                }
            }

            if (data != null)
            {
                res = string.Format(@"Kepada Yth.{0} <br><br>
                        Dengan hormat Kami sampaikan pengajuan permohonan approval/persetujuan penggunaan tanah HPL Pelabuhan di aplikasi REMOTE dengan status terakhir telah dilakukan approval/persetujuan oleh : <br>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Status Approval</td>
                                    <td> : {1}</td>
                                </tr>   
                                <tr>
                                    <td>Top Level Approval</td>
                                    <td> : {11}</td>
                                </tr>   
                                <tr>
                                    <td>Keterangan di{10}</td>
                                    <td> : {2}</td>
                                </tr> 
                                <tr>
                                    <td>Di{10} Oleh</td>
                                    <td> : {3}</td>
                                </tr> 
                                <tr>
                                    <td>Dibuat Oleh</td>
                                    <td> : {4}</td>
                                </tr> 
                                <tr>
                                    <td>No Contract Offer</td>
                                    <td> : {5}</td>
                                </tr>   
                                <tr>
                                    <td>Nama Contract Offer</td>
                                    <td> : {6}</td>
                                </tr> 
                                <tr>
                                    <td>Pemohon </td>
                                    <td> : {7}</td>
                                </tr>   
                                <tr>
                                    <td>Jenis Permohonan</td>
                                    <td> : {8}</td>
                                </tr> 
                                <tr>
                                    <td>Masa Sewa</td>
                                    <td> : {9}</td>
                                </tr>
                                <tr>
                                    <td>Nama Cabang</td>
                                    <td> : {12}</td>
                                </tr>
                                <tr>
                                    <td>Profit Center</td>
                                    <td> : {13}</td>
                                </tr>
                            </tbody>
                        </table><br>
                        ",
                        rolename, offer_status, param.APPROVAL_NOTE,
                        param.LAST_UPDATE_BY, (data.UPDATED_BY == null) ? data.CREATION_BY : data.UPDATED_BY, data.CONTRACT_OFFER_NO,
                        data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_TYPE,
                        data.CONTRACT_START_DATE + " s/d " + data.CONTRACT_END_DATE + " (" + data.TERM_IN_MONTHS + " Bulan)",
                        type, top, data.BE_NAMES, data.PROFIT_CENTER_NAME);
            }

            return res;
        }

        public string bodyHeaderSurat(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param, dynamic dataSurat)
        {
            string res = "";
            var sql = "";
            string offer_status = "";
            var top = "";
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1")
                {
                    sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else if (data.COMPLETED_STATUS == "3")
                {
                    sql = @"SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else
                {
                    sql = @"SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                offer_status = connection.ExecuteScalar<string>(sql, new
                {
                    a = data.OFFER_STATUS
                });

            }

            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {

                if (data.TOP_APPROVED_LEVEL == "50")
                {
                    top = "CEO REGIONAL";
                }
                else
                {
                    sql = @"select ROLE_NAME from APP_ROLE where PROPERTY_ROLE = :a and APP_ID = 1";

                    top = connection.ExecuteScalar<string>(sql, new
                    {
                        a = data.TOP_APPROVED_LEVEL
                    });
                }
            }

            if (data != null)
            {
                res = string.Format(@"<br><br><table>
                            <tbody>
                                <tr>
                                    <td>Status Approval</td>
                                    <td> : {1}</td>
                                </tr>   
                                <tr>
                                    <td>Top Level Approval</td>
                                    <td> : {11}</td>
                                </tr>   
                                <tr>
                                    <td>Keterangan di{10}</td>
                                    <td> : {2}</td>
                                </tr> 
                                <tr>
                                    <td>Di{10} Oleh</td>
                                    <td> : {3}</td>
                                </tr> 
                                <tr>
                                    <td>Dibuat Oleh</td>
                                    <td> : {4}</td>
                                </tr> 
                                <tr>
                                    <td>No Contract Offer</td>
                                    <td> : {5}</td>
                                </tr>   
                                <tr>
                                    <td>Nama Contract Offer</td>
                                    <td> : {6}</td>
                                </tr> 
                                <tr>
                                    <td>Pemohon </td>
                                    <td> : {7}</td>
                                </tr>   
                                <tr>
                                    <td>Jenis Permohonan</td>
                                    <td> : {8}</td>
                                </tr> 
                                <tr>
                                    <td>Masa Sewa</td>
                                    <td> : {9}</td>
                                </tr>
                                <tr>
                                    <td>Nama Cabang</td>
                                    <td> : {12}</td>
                                </tr>
                                <tr>
                                    <td>Profit Center</td>
                                    <td> : {13}</td>
                                </tr>
                            </tbody>
                        </table><br>
                        ",
                        dataSurat.NAMA_KEPADA, offer_status, param.APPROVAL_NOTE,
                        param.LAST_UPDATE_BY, (data.UPDATED_BY == null) ? data.CREATION_BY : data.UPDATED_BY, data.CONTRACT_OFFER_NO,
                        data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_TYPE,
                        data.CONTRACT_START_DATE + " s/d " + data.CONTRACT_END_DATE + " (" + data.TERM_IN_MONTHS + " Bulan)",
                        type, top, data.BE_NAMES, data.PROFIT_CENTER_NAME);
            }

            return res;
        }

        public string bodyHeaderParaf(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param, string kepada)
        {
            string res = "";
            var sql = "";
            string offer_status = "";
            var top = "";
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1")
                {
                    sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else if (data.COMPLETED_STATUS == "3")
                {
                    sql = @"SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else
                {
                    sql = @"SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                offer_status = connection.ExecuteScalar<string>(sql, new
                {
                    a = data.OFFER_STATUS
                });

            }

            using (var connection = DatabaseFactory.GetConnection("app_repo"))
            {
                if(data.TOP_APPROVED_LEVEL == "50")
                {
                    top = "CEO REGIONAL";
                } else
                {
                    sql = @"select ROLE_NAME from APP_ROLE where PROPERTY_ROLE = :a and APP_ID = 1";

                    top = connection.ExecuteScalar<string>(sql, new
                    {
                        a = data.TOP_APPROVED_LEVEL
                    });
                }
            }

            if (data != null)
            {
                res = string.Format(@"<table>
                            <tbody>
                                <tr>
                                    <td>Status Approval</td>
                                    <td> : {1}</td>
                                </tr>   
                                <tr>
                                    <td>Top Level Approval</td>
                                    <td> : {11}</td>
                                </tr>   
                                <tr>
                                    <td>Keterangan di{10}</td>
                                    <td> : {2}</td>
                                </tr> 
                                <tr>
                                    <td>Di{10} Oleh</td>
                                    <td> : {3}</td>
                                </tr> 
                                <tr>
                                    <td>Dibuat Oleh</td>
                                    <td> : {4}</td>
                                </tr> 
                                <tr>
                                    <td>No Contract Offer</td>
                                    <td> : {5}</td>
                                </tr>   
                                <tr>
                                    <td>Nama Contract Offer</td>
                                    <td> : {6}</td>
                                </tr> 
                                <tr>
                                    <td>Pemohon </td>
                                    <td> : {7}</td>
                                </tr>   
                                <tr>
                                    <td>Jenis Permohonan</td>
                                    <td> : {8}</td>
                                </tr> 
                                <tr>
                                    <td>Masa Sewa</td>
                                    <td> : {9}</td>
                                </tr>
                                <tr>
                                    <td>Nama Cabang</td>
                                    <td> : {12}</td>
                                </tr>
                                <tr>
                                    <td>Profit Center</td>
                                    <td> : {13}</td>
                                </tr>
                            </tbody>
                        </table><br>
                        ",
                        kepada, offer_status, param.APPROVAL_NOTE,
                        param.LAST_UPDATE_BY, (data.UPDATED_BY == null) ? data.CREATION_BY : data.UPDATED_BY, data.CONTRACT_OFFER_NO,
                        data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_TYPE,
                        data.CONTRACT_START_DATE + " s/d " + data.CONTRACT_END_DATE + " (" + data.TERM_IN_MONTHS + " Bulan)",
                        type, top, data.BE_NAMES, data.PROFIT_CENTER_NAME);
            }

            return res;
        }

        public string bodydetail(string contract_offer)
        {
            string res = "";

            if (contract_offer != null)
            {
                using (var connection = DatabaseFactory.GetConnection())
                {
                    //object
                    var sql = @"select ID,CONTRACT_OFFER_NO,
                                OBJECT_TYPE,OBJECT_NAME,
                                to_char(START_DATE, 'DD/MM/YYYY') START_DATE,
                                to_char(END_DATE, 'DD/MM/YYYY') END_DATE,
                                LUAS,INDUSTRY,
                                LUAS_TANAH,LUAS_BANGUNAN,
                                OBJECT_ID,RO_NAME from PROP_CONTRACT_OFFER_OBJECT 
                                where CONTRACT_OFFER_NO =:a ";
                    var objec = connection.Query<dynamic>(sql, new
                    {
                        a = contract_offer
                    });
                    if (objec.Count() > 0)
                    {
                        res += "<br>OBJECT<br>";
                        res += @"<table width='100%' style='font-family:verdana; font-size:13px; border-collapse: collapse; border: 1px solid black; ' class='t01' CELLPADDING='10'>
                                    <thead>
                                    <tr style='text-align:center; color: #fff; background: #36c6d3;'><td width = '20%'>Rental Object</td><td width = '20%'>Zona RIP</td><td width = '20%'>Luas Tanah</td><td width = '20%'>Luas Bangunan</td><td width = '20%'>Peruntukan</td></tr>
                                    </thead>
                                    <tbody>";
                        foreach (var temp in objec)
                        {
                            res += string.Format(@"<tr><td>{0}</td><td style='text-align : center'>{1}</td><td style='text-align : center'>{2}</td><td style='text-align : center'>{3}</td><td style='text-align : center'>{4}</td></tr>",
                                temp.OBJECT_ID + "/" + temp.OBJECT_NAME, temp.OBJECT_TYPE, temp.LUAS_TANAH, temp.LUAS_BANGUNAN, temp.INDUSTRY);
                        }
                        res += "</tbody></table>";

                    }

                    sql = @"select ID,CALC_OBJECT,CONDITION_TYPE,
                            to_char(VALID_FROM,'DD/MM/RRRR') VALID_FROM,
                            to_char(VALID_TO,'DD/MM/RRRR') VALID_TO,
                            MONTHS,STATISTIC,UNIT_PRICE,
                            AMT_REF,FREQUENCY,to_char(START_DUE_DATE,'DD/MM/YYYY') START_DUE_DATE,
                            MANUAL_NO,FORMULA,MEASUREMENT_TYPE,LUAS,TOTAL,
                            NJOP_PERCENT,KONDISI_TEKNIS_PERCENT,
                            SALES_RULE,to_char(TOTAL_NET_VALUE,'999,999,999,999,999') TOTAL_NET_VALUE from PROP_CONTRACT_OFFER_CONDITION
                                where CONTRACT_OFFER_NO =:a ";
                    var condition = connection.Query<dynamic>(sql, new
                    {
                        a = contract_offer
                    });
                    if (condition.Count() > 0)
                    {
                        res += "<br>CONDITION<br>";
                        res += @"<table width='100%' style='font-family:verdana; font-size:13px; border-collapse: collapse; border: 1px solid black; ' class='t01' CELLPADDING='10'>
                                    <thead>
                                    <tr style='text-align:center; color:#fff; background:#36c6d3;'><td  width = '20%'>RO NUMBER</td><td width = '20%'>CONDITION TYPE</td><td width = '20%'>VALID FROM</td><td width = '20%'>VALID TO</td><td width = '20%'>TOTAL NET VALUE</td></tr>
                                    </thead>
                                    <tbody>";
                        foreach (var temp in condition)
                        {
                            res += string.Format(@"<tr><td>{0}</td><td style='text-align : center'>{1}</td><td style='text-align : center'>{2}</td><td style='text-align : center'>{3}</td><td style='text-align : right'>Rp {4}</td></tr>",
                                temp.CALC_OBJECT, temp.CONDITION_TYPE, temp.VALID_FROM, temp.VALID_TO, temp.TOTAL_NET_VALUE);
                        }
                        res += "</tbody></table>";
                    }
                    sql = @"select ID,CONTRACT_OFFER_NO,CALC_OBJECT,CONDITION_TYPE,MEMO from PROP_CONTRACT_OFFER_MEMO
                                where CONTRACT_OFFER_NO =:a ";
                    var memo = connection.Query<dynamic>(sql, new
                    {
                        a = contract_offer
                    });
                    if (memo.Count() > 0)
                    {
                        res += "<br>MEMO<br>";
                        res += @"<table width='100%' style='font-family:verdana; font-size:13px; border-collapse: collapse; border: 1px solid black; ' class='t01' CELLPADDING='10'>
                                    <thead>
                                    <tr style='text-align:center; color: #fff; background: #36c6d3;'><td  width = '30%'>CALC OBJECT</td><td width = '70%'>MEMO</td></tr>
                                    </thead>
                                    <tbody>";
                        foreach (var temp in memo)
                        {
                            res += string.Format(@"<tr><td>{0}</td><td>{1}</td></tr>",
                                temp.CALC_OBJECT, temp.MEMO);
                        }
                        res += "</tbody></table>";
                    }

                }
            }
            return res;
        }
        public string emailfooter()
        {
            string res = "";
            res = "";
            return res;
        }

        public string kodeBayar(string kode)
        {
            string res = "";
            res = string.Format(@"<br>
                <br>Berikut adalah Kode Bayar dari Permohonan Kontrak yang telah dibuat: <strong>{0}</strong><br>", kode);

            return res;
        }

        public string isiSurat(dynamic dataSurat, string role_name, dynamic type, string nama, string note, string creation_by)
        {
            string res = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = @"SELECT A.USER_LOGIN || ' | ' || A.USER_NAME CREATIONBY, USER_NAME FROM APP_USER A WHERE A.ID = :a ";
            dynamic creation = connection.Query<dynamic>(sql, new
            {
                a = dataSurat.USER_ID_CREATE
            }).DefaultIfEmpty(null).FirstOrDefault();

            creation_by = creation.CREATIONBY;

            //disposisi
            if (type == 0)
            {
                res = string.Format(@"Yth " + role_name + @" <br>
                Anda menerima <b>Disposisi</b> dari " + nama + @" perihal " + dataSurat.SUBJECT + @" Mohon segera disposisi, pembuat surat: " + creation_by + @"
                </br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //paraf
            else if (type == 1)
            {
                res = string.Format(@"Yth " + role_name + @" <br>
                Anda menerima <b>Permohonan Paraf</b> dari " + nama + @" perihal " + dataSurat.SUBJECT + @" Mohon segera diparaf, pembuat surat: " + creation_by + @"
                </br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //approve
            else if (type == 2)
            {
                res = string.Format(@"Yth " + role_name + @" <br>
                Anda menerima <b>Permohonan Approve</b> dari " + nama + @" perihal " + dataSurat.SUBJECT + @" Mohon segera ditindak lanjuti, pembuat surat: " + creation_by + @"
                </br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //revise
            else if (type == 3)
            {
                res = string.Format(@"Yth " + role_name + @" <br>
                Anda menerima <b>Revise</b> dari " + nama + @" perihal " + dataSurat.SUBJECT + @" Mohon segera ditindak lanjuti, pembuat surat: " + creation_by + @"
                </br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //rejected
            else if (type == 4)
            {
                res = string.Format(@"Yth " + dataSurat.NAMA_KEPADA + @" <br>
                Anda menerima <b>Rejected</b> dari " + role_name + @" perihal " + dataSurat.SUBJECT + @" Mohon segera ditindak lanjuti, pembuat surat: " + creation_by + @"
                </br>   
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }

            else if (type == 5)
            {
                res = string.Format(@"Yth " + role_name + @" <br> Dengan hormat Kami sampaikan pengajuan permohonan approval/persetujuan penggunaan tanah HPL Pelabuhan di aplikasi REMOTE dengan status terakhir telah dilakukan paraf oleh " + nama + @" <br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //reject-paraf
            else if (type == 6)
            {
                res = string.Format(@"Yth " + role_name + @" <br>
                Anda menerima <b>Revise</b> dari " + nama + @" perihal " + dataSurat.SUBJECT + @" Mohon segera ditindak lanjuti, pembuat surat: " + creation_by + @"
                </br>
                Mohon <a href='http://remote.pelindo.co.id'>Login ke aplikasi Remote</a></br>
                <b>Catatan: </b>" + note);
            }
            //spam email
            else if (type == 7)
            {
                res = string.Format(@"Dengan hormat Kami sampaikan permohonan penggunaan tanah HPL Pelabuhan di aplikasi REMOTE dengan status terakhir telah dilakukan void oleh " + nama);
            }

            //spam email
            else if (type == 8)
            {
                res = string.Format(@"Dengan hormat Kami sampaikan permohonan penggunaan tanah HPL Pelabuhan di aplikasi REMOTE sudah disetujui oleh " + nama);
            }

            var nomor_surat = "-";
            if (dataSurat.STATUS_APV == 1 || type == 5 || type == 8)
            {
                nomor_surat = dataSurat.NO_SURAT;
            }
            res += string.Format(@"<br>
                                    <div style='padding-top:20px; padding-left:60px;'><img src='https://milea.pelindo.co.id/images/logo-pelindo3.png'width='300px' height='100px'></div>
                                    <table class='headerss' width='100%'>
                                        <tr>
                                            <td align='center'>
                                                <span>
                                                    <b>
                                                        SURAT
						                                </b>
                                                        <br>No Surat : " + nomor_surat + @"                  
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class='headerss' width='60%'>
                                            <tr>
                                                <td width='10%' align='left'>Kepada : </td>
			                                    <td>Yth.</td>
                                            </tr>
                                    </table>");
            int count = 0;
            if(dataSurat.NAMA_KEPADA != null)
            {
                string[] namaKepada = dataSurat.NAMA_KEPADA.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var temp in namaKepada)
                {
                    count++;
                    res += string.Format(@"<table class='headerss' width='60%'>
                                            <tr>
			                                    <td align='left'></td>
			                                    <td>" + count + ". " + temp + @"</td>
		                                    </tr>
                                         </table>");
                }
            }
            
            if(dataSurat.KEPADA_EKSTERNAL != null)
            {
                string[] kepadaEksternal = dataSurat.KEPADA_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var temp in kepadaEksternal)
                {
                    count++;
                    res += string.Format(@"<table class='headerss' width='60%'>
                                            <tr>
			                                    <td align='left'></td>
			                                    <td>" + count + ". " + temp + @"</td>
		                                    </tr>
                                         </table>");
                }
            }

            res += string.Format(@"<table class='headerss' width='60%'>
		                                    <tr>
                                                <td align='left'>Perihal &nbsp;: </td>
			                                    <td>" + dataSurat.SUBJECT + @"</td>
                                            </tr>
                                    </table>
                                    <br><hr>" + dataSurat.ISI_SURAT + @"<br><br>");
            if (dataSurat.NAMA_TTD != null)
            {
                string[] dataTTD = dataSurat.NAMA_TTD.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                if (dataSurat.STATUS_APV == 1 || type == 5 || type == 8)
                {
                    if (dataSurat.TANGGAL != null)
                    {
                        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                        var tgl = Convert.ToDateTime(dataSurat.TANGGAL, usDtfi);
                        res += string.Format(@"<table class='headerss' width='100%'>
                            <tr>
                                <td align='center'>
                                    <span>
                                            " + tgl.ToString("dd-MM-yyyy") + @"						
                                    </span>
                                </td>
                            </tr>
                    </table>");
                    }
                    res += string.Format(@"<table class='headerss' width='100%'>
                            <tr>
                                <td align='center'>
                                    <span>
                                        <b>
                                            " + dataTTD[1] + @"
                                            <br>
                                            <img src='https://remote.pelindo.co.id/assets/global/img/qrcode-remote.png'width='50px' height='50px'>
                                            <br>" + dataTTD[0] + @" 
					                    </b>						
                                    </span>
                                </td>
                            </tr>
                    </table>");
                }
                else
                {
                    res += string.Format(@"<br><table class='headerss' width='100%'>
                            <tr>
                                <td align='center'>
                                    <span>
                                        <b>
                                            " + dataTTD[1] + @"
                                            <br>
                                            <br>" + dataTTD[0] + @" 
					                    </b>						
                                    </span>
                                </td>
                            </tr>
                    </table>");
                }
            }

            if (dataSurat.NAMA_TEMBUSAN != null || dataSurat.TEMBUSAN_EKSTERNAL != null)
            {
                int no = 0;
                res += string.Format(@"<br><br>
                    <table class='headerss' width='50%'>
                            <tr>
                                <td width='12%' align='left'>Tembusan : </td>
			                    <td></td>
                            </tr>
                    </table>");
                if (dataSurat.NAMA_TEMBUSAN != null)
                {
                    string[] dataKepada = dataSurat.NAMA_TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var temp in dataKepada)
                    {
                        no++;
                        res += string.Format(@"
                    <table class='headerss' width='50%'>
                            <tr>
                                <td width='12%' align='center'>" + no +". "+ temp + @"</td>
                            </tr>
                    </table>");
                    }
                }

                if (dataSurat.TEMBUSAN_EKSTERNAL != null)
                {
                    string[] tembusanEksternal = dataSurat.TEMBUSAN_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var temp in tembusanEksternal)
                    {
                        no++;
                        res += string.Format(@"
                    <table class='headerss' width='50%'>
                            <tr>
                                <td width='12%' align='center'>" + no + ". " + temp + @"</td>
                            </tr>
                    </table>");
                    }
                }

            }
            dynamic dataAttachment = null;
            using (IDbConnection conn = DatabaseFactory.GetConnection())
            {
                var cariAttachment = @"SELECT DIRECTORY, FILE_NAME FROM PROP_SURAT_ATTACHMENT WHERE SURAT_ID =:b";
                dataAttachment = conn.Query(cariAttachment, new { b = dataSurat.ID }).ToList();
            }
            //if (dataAttachment.Count() > 0)
            //{ 
            //}
            if(dataAttachment != null)
            {
                res += string.Format(@"<table class='headerss' width='50%'>
                        <tr>
                            <td width = '12%' align='left'>Lampiran : </td>
			                <td></td>
                        </tr>
                    </table>");
            }
            foreach (dynamic temp in dataAttachment)
            {
                string link = "<a href='" + temp.DIRECTORY + "'> " + temp.FILE_NAME + " </a> ";
                
                res += string.Format(@"<table class='headerss' width='25%'>
                        <tr>
                            <td width='12%' align='center'>" + link + @"</td>
                        </tr>
                    </table>");
            }
            //}

            connection.Close();

            connection.Dispose();
            return res;
        }

        public Results formateSendEmail(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, string kodeCabang, string type, ContractOfferApprovalParameter param, dynamic userKepada, string namaEmpl)
        {
            Results result = new Results();
            var subject = dataSurat.SUBJECT;
            try
            {
                TestEmail t = new TestEmail();
                
                var body = isiSurat(dataSurat, userKepada.USER_NAME, 1, namaEmpl, dataSurat.MEMO, data.CREATION_BY);
                body += bodyHeaderSurat(data, type, param, dataSurat);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                body += link;
                body += DataCabang(kodeCabang);
                body += emailfooter();
                //send email
                EmailConf email = new EmailConf();
                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? userKepada.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.DESC = "name : " + userKepada.USER_NAME + ", email : " + userKepada.USER_EMAIL;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                var sendingemail = dal.SendMail(email);

                result.Msg = "Sukses kirim email";
                result.Status = "S";

                if (dataSurat.TEMBUSAN != null)
                {
                    var tembusan = dal.emailTembusan(data, dataSurat, body);
                }

            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }
            return result;
        }

        public Results mailToMaker(string id, string kodeCabang, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param, dynamic dataSurat, string namaEmpl, dynamic emailto, dynamic type_surat, dynamic userLogin = null)
        {
            Results result = new Results();
            IEnumerable<DataUser> res = null;
            var subject = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Released" : (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1" ? "Approved" : data.COMPLETED_STATUS == "3" ? "Revised" : "Rejected"));
            try
            {
                //IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
                //if (connection.State.Equals(ConnectionState.Closed))
                //    connection.Open();
                //string sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                //                where a.USER_LOGIN = :a and a.KD_CABANG = :b and a.APP_ID = 1";
                //res = connection.Query<DataUser>(sql, new
                //{
                //    a = id,
                //    b = kodeCabang
                //});
                dynamic typeSurat = Convert.ToInt32(type_surat);
                TestEmail t = new TestEmail();
                var body = "";
                if (typeSurat == 2)
                {
                    body = isiSurat(dataSurat, namaEmpl, typeSurat, emailto.USER_NAME, param.APPROVAL_NOTE, data.CREATION_BY);
                }
                else
                {
                    body = isiSurat(dataSurat, emailto.USER_NAME, typeSurat, namaEmpl, param.APPROVAL_NOTE, data.CREATION_BY);
                }
                body += bodyheader(emailto.USER_NAME, data, type, param);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                body += link;
                body += DataCabang(kodeCabang);
                body += emailfooter();
                //send email
                EmailConf email = new EmailConf();
                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? emailto.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.DESC = "name : " + emailto.USER_NAME + ", email : " + emailto.USER_EMAIL;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                var sendingemail = dal.SendMail(email);

                //send notif mobile
                ListNotifikasiController listNotif = new ListNotifikasiController();
                ParamNotification paramNotif = new ParamNotification();
                paramNotif.username = emailto.USER_LOGIN;
                paramNotif.type = "5"; //REVISE
                paramNotif.notification = param.APPROVAL_NOTE;
                paramNotif.no_contract = data.CONTRACT_OFFER_NO;
                paramNotif.pengirim = userLogin;
                var sendNotif = listNotif.addNewNotif(paramNotif);
                //if (res.Count() > 0)
                //{
                //    foreach (DataUser temp in res)
                //    {
                //        if (temp.USER_EMAIL != null)
                //        {
                //            var body = "";
                //            if (typeSurat == 2)
                //            {
                //                body = isiSurat(dataSurat, namaEmpl, typeSurat, emailto.USER_NAME, param.APPROVAL_NOTE, data.CREATION_BY);
                //            }
                //            else
                //            {
                //                body = isiSurat(dataSurat, emailto.USER_NAME, typeSurat, namaEmpl, param.APPROVAL_NOTE, data.CREATION_BY);
                //            }
                //            body += bodyheader(temp.ROLE_NAME, data, type, param);
                //            body += bodydetail(data.CONTRACT_OFFER_NO);
                //            body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                //            body += link;
                //            body += DataCabang(kodeCabang);
                //            body += emailfooter();
                //            //send email
                //            EmailConf email = new EmailConf();
                //            email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? temp.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                //            email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                //            email.BODY = body;
                //            email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                //            email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                //            EmailConfigurationDAL dal = new EmailConfigurationDAL();
                //            var sendingemail = dal.SendMail(email);
                //        }

                //    }
                //    result.Msg = "Sukses kirim email";
                //    result.Status = "S";
                //}
                //else
                //{
                //    result.Msg = "Tidak Ada User";
                //    result.Status = "E";
                //}

            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }
            return result;
        }

        public IEnumerable<DataUser> searchUserNotifWarning(string kdCabang, string propertyRole)
        {
            QueryGlobal query = new QueryGlobal();
            var v_be = query.Query_Tingkat();
            IEnumerable<DataUser> res = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string roleString = string.Empty;
            try
            {
                string sql = @"select ROLE_ID 
                                from V_LIST_NOTIFICATION b 
                                where approval_no <= (
                                    select approval_no from V_LIST_NOTIFICATION 
                                    where property_role = :a and be_id = '1011' and rownum = 1
                                ) ";
                //var roleId = connection.Query<int>(sql, new { a = kdCabang });
                var roleId = connection.Query<int>(sql, new { a = propertyRole });
                roleString = string.Join(", ", roleId);
            }
            catch (Exception ex)
            {
                roleString = "-1";
                string aaa = ex.ToString();
            }

            try
            {
                string sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from APP_REPO.VW_APP_USER a
                                where a.USER_ROLE_ID in (roleString) and a.KD_CABANG in (
                                    select distinct branch_where from REMOTE.V_BE where branch_id = :b
                                )  and a.APP_ID = 1 and a.STATUS = 1 ";
                sql = sql.Replace("roleString", roleString);
                res = connection.Query<DataUser>(sql, new
                {
                    b = kdCabang
                });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            connection.Close();

            connection.Dispose();
            return res;
        }
        public Results WarningContractEndMail(string contract_no, string peringatan_no, string kd_cabang, string type)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IDbConnection connectionRepo = DatabaseFactory.GetConnection("app_repo");
            if (connectionRepo.State.Equals(ConnectionState.Closed))
                connectionRepo.Open();
            string email_to = "";
            string name = "";
            Results result = new Results();
            DataUser res = null;
            var sql = @"select a.CREATION_BY
                        , COALESCE(d.BUSINESS_PARTNER, a.BUSINESS_PARTNER) BUSINESS_PARTNER
                        , COALESCE(d.CONTRACT_NAME, a.CONTRACT_NAME) CONTRACT_NAME 
                        from PROP_CONTRACT a
                        left join PROP_RENTAL_REQUEST c on c.OLD_CONTRACT = TO_CHAR(a.CONTRACT_NO)
                        left join PROP_CONTRACT_OFFER b on c.CONTRACT_OFFER_NUMBER = b.CONTRACT_OFFER_NO
                        left join PROP_CONTRACT d on b.CONTRACT_NUMBER = d.CONTRACT_NO
                        where a.CONTRACT_NO =:a";
            dynamic temp = connection.Query<dynamic>(sql, new
            {
                a = contract_no
            }).DefaultIfEmpty(null).FirstOrDefault();
            if (temp != null)
            {
                if (type == "pejabat")
                {
                    sql = "select a.TOP_APPROVED_LEVEL from PROP_CONTRACT_OFFER a where a.CONTRACT_NUMBER =:a ";
                    var topapv = connection.ExecuteScalar<string>(sql, new { a = contract_no });
                    var users = searchUserNotifWarning(kd_cabang, topapv);

                    if (users != null)
                    {
                        foreach (var user in users)
                        {
                            email_to = user.USER_EMAIL;
                            name = user.USER_NAME;

                            if (email_to != "")// && email_to != null
                            {
                                var body = bodyContract(contract_no, peringatan_no, type, name);
                                body += DataCabang(kd_cabang);
                                body += emailfooter();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = email_to; //"melin.anggrainirh@gmail.com"; // // // //"hafidz.lazuardy@gmail.com";// 
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                //email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                                email.SUBJECT = "Pemberitahuan " + peringatan_no + " " + temp.CONTRACT_NAME + " ( No. Contract : " + contract_no + " )";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                var sendingemail = dal.SendMail(email);
                            }
                        }
                    }
                }
                else
                {
                    if (type == "user")
                    {
                        var createby = temp.CREATION_BY.Split(new string[] { " | " }, StringSplitOptions.None);
                        sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                            where a.USER_LOGIN = :a and a.APP_ID = 1";
                        res = connectionRepo.Query<DataUser>(sql, new
                        {
                            a = createby[0],
                            b = kd_cabang
                        }).DefaultIfEmpty(null).FirstOrDefault();
                        if (res != null)
                        {
                            email_to = res.USER_EMAIL;
                            name = createby[1];
                        }
                    }
                    else // pj
                    {
                        sql = @"select MPLG_NAMA, MPLG_EMAIL_ADDRESS from VW_CUSTOMERS where MPLG_KODE =:a and KD_CABANG =:b ";
                        dynamic customer = connection.Query<dynamic>(sql, new
                        {
                            a = temp.BUSINESS_PARTNER,
                            b = kd_cabang
                        }).DefaultIfEmpty(null).FirstOrDefault();
                        if (customer != null)
                        {
                            email_to = customer.MPLG_EMAIL_ADDRESS;
                            name = customer.MPLG_NAMA;
                        }
                    }

                    if (email_to != "")// && email_to != null
                    {
                        var body = bodyContract(contract_no, peringatan_no, type, name);
                        body += DataCabang(kd_cabang);
                        body += emailfooter();
                        //send email
                        EmailConf email = new EmailConf();
                        email.EMAIL_TO = email_to; //"melinaarh6@gmail.com"; // ////email_to; //"hafidz.lazuardy@gmail.com";// 
                        email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                        email.BODY = body;
                        //email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                        email.SUBJECT = "Pemberitahuan " + peringatan_no + " " + temp.CONTRACT_NAME + " ( No. Contract : " + contract_no + " )";
                        EmailConfigurationDAL dal = new EmailConfigurationDAL();
                        var sendingemail = dal.SendMail(email);
                    }
                }
            }

            connection.Close();
            connection.Dispose();
            connectionRepo.Close();

            return result;
        }

        public Results NotifContractEndMail(string contract_no, string peringatan_no, string kd_cabang, string type, int propertyRole)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IDbConnection connectionRepo = DatabaseFactory.GetConnection("app_repo");
            if (connectionRepo.State.Equals(ConnectionState.Closed))
                connectionRepo.Open();
            string email_to = "";
            string name = "";
            Results result = new Results();
            DataUser res = null;
            var sql = @"select a.CREATION_BY
                        , COALESCE(d.BUSINESS_PARTNER, a.BUSINESS_PARTNER) BUSINESS_PARTNER
                        , COALESCE(d.CONTRACT_NAME, a.CONTRACT_NAME) CONTRACT_NAME 
                        from PROP_CONTRACT a
                        left join PROP_RENTAL_REQUEST c on c.OLD_CONTRACT = TO_CHAR(a.CONTRACT_NO)
                        left join PROP_CONTRACT_OFFER b on c.CONTRACT_OFFER_NUMBER = b.CONTRACT_OFFER_NO
                        left join PROP_CONTRACT d on b.CONTRACT_NUMBER = d.CONTRACT_NO
                        where a.CONTRACT_NO =:a";
            dynamic temp = connection.Query<dynamic>(sql, new
            {
                a = contract_no
            }).DefaultIfEmpty(null).FirstOrDefault();
            if (temp != null)
            {
                if (type == "pejabat")
                {
                    //sql = "select a.TOP_APPROVED_LEVEL from PROP_CONTRACT_OFFER a where a.CONTRACT_NUMBER =:a ";
                    sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from APP_REPO.VW_APP_USER a
                            where a.USER_ROLE_ID in (select ROLE_ID from LIST_NOTIFICATION_DETAIL_ROW
                                where APPROVAL_NO <= (select a.APPROVAL_NO from LIST_NOTIFICATION_DETAIL_ROW a
                                where a.ROLE_ID = (select ROLE_ID from APP_REPO.APP_ROLE where PROPERTY_ROLE = :b))
                            ) and a.KD_CABANG in (
                                select distinct branch_where from V_BE where branch_id = :a
                            )  and a.APP_ID = 1 and a.STATUS = 1 ";
                    //var topapv = connection.ExecuteScalar<string>(sql, new { a = kd_cabang, b = propertyRole});
                    //var users = searchUserNotifWarning(kd_cabang, topapv);

                    var approv = connection.Query<DataUser>(sql, new
                    {
                        a = kd_cabang,
                        b = propertyRole
                    });

                    //var approv = connection.ExecuteScalar<string>(sql, new
                    //{
                    //    a = kd_cabang,
                    //    b = propertyRole
                    //});

                    if (approv != null)
                    {
                        foreach (var user in approv)
                        {
                            email_to = user.USER_EMAIL; //"melin.anggrainirh@gmail.com"; //
                            name = user.USER_NAME;

                            if (email_to != "")// && email_to != null
                            {
                                var body = bodyContractNotif(contract_no, peringatan_no, type, name);
                                body += DataCabang(kd_cabang);
                                body += emailfooter();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = email_to; //"melin.anggrainirh@gmail.com"; // // //"hafidz.lazuardy@gmail.com";// 
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                //email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                                email.SUBJECT = "Pemberitahuan " + peringatan_no + " " + temp.CONTRACT_NAME + " ( No. Contract : " + contract_no + " )";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                var sendingemail = dal.SendMail(email);
                            }
                        }
                    }
                }
                else
                {
                    if (type == "user")
                    {
                        var createby = temp.CREATION_BY.Split(new string[] { " | " }, StringSplitOptions.None);
                        sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from app_repo.VW_APP_USER a
                            where a.USER_LOGIN = :a and a.APP_ID = 1";
                        res = connectionRepo.Query<DataUser>(sql, new
                        {
                            a = createby[0],
                            b = kd_cabang
                        }).DefaultIfEmpty(null).FirstOrDefault();
                        if (res != null)
                        {
                            email_to = res.USER_EMAIL;
                            name = createby[1];
                        }
                    }
                    else // pj
                    {
                        sql = @"select MPLG_NAMA, MPLG_EMAIL_ADDRESS from VW_CUSTOMERS where MPLG_KODE =:a and KD_CABANG =:b ";
                        dynamic customer = connection.Query<dynamic>(sql, new
                        {
                            a = temp.BUSINESS_PARTNER,
                            b = kd_cabang
                        }).DefaultIfEmpty(null).FirstOrDefault();
                        if (customer != null)
                        {
                            email_to = customer.MPLG_EMAIL_ADDRESS;
                            name = customer.MPLG_NAMA;
                        }
                    }

                    if (email_to != "")// && email_to != null
                    {
                        var body = bodyContractNotif(contract_no, peringatan_no, type, name);
                        body += DataCabang(kd_cabang);
                        body += emailfooter();
                        //send email
                        EmailConf email = new EmailConf();
                        email.EMAIL_TO = email_to; //"melinaarh6@gmail.com"; // // //"hafidz.lazuardy@gmail.com";// 
                        email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                        email.BODY = body;
                        //email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                        email.SUBJECT = "Pemberitahuan " + peringatan_no + " " + temp.CONTRACT_NAME + "( No. Contract : " + contract_no + " )";
                        EmailConfigurationDAL dal = new EmailConfigurationDAL();
                        var sendingemail = dal.SendMail(email);
                    }
                }
            }

            connection.Close();

            connection.Dispose();
            connectionRepo.Close();
            return result;
        }

        public string bodyContract(string contract_no, string peringatan_no, string type, string name)
        {
            string res = "";
            AUP_TRANS_CONTRACT_WEBAPPS contract = new AUP_TRANS_CONTRACT_WEBAPPS();
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            var sql = @"select *
                    from V_TRANS_SPECIAL_CONT_HEADER where CONTRACT_NO =:a";
            contract = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new
            {
                a = contract_no
            }).DefaultIfEmpty(null).FirstOrDefault();

            var luass = "";

            if (contract.LUAS_BANGUNAN == null || contract.LUAS_BANGUNAN == "0" || contract.LUAS_BANGUNAN == "")
            {
                luass = contract.LUAS_TANAH;
            }
            else
            {
                luass = contract.LUAS_BANGUNAN;
            }

            if (contract != null)
            {
                if (type == "user")
                {
                    res += string.Format(@"Kepada:<br> Yth.<b>{0}</b> <br><br>
                        Dengan ini disampaikan bahwa perjanjian kerjasama Penggunaan Tanah HPL {1}<br>
                        An. {2} No : {3} Seluas {4} m2 untuk kantor <br>  
                        akan berakhir masa berlakunya pada tanggal <b>{5}</b> <br><br><br>
                        
                        Sehubungan hal tersebut diatas agar {2} diberikan surat pemeberitahuan <br>
                        untuk melakukan perpanjangan kepada PT Pelabuhan Indonesia III (Persero)  {6}
                        " + link + "", name, contract.CONTRACT_NAME, contract.BUSINESS_PARTNER_NAME, contract.LEGAL_CONTRACT_NO, luass, contract.CONTRACT_END_DATE, contract.BE_NAME);
                }
                else
                {
                    res += string.Format(@"Kepada:<br> Yth.<b>{0}</b> <br><br>
                        Dengan ini disampaikan bahwa sesuai Perjanjian Penyerahan Penggunaan Bagian tanah HPL Perpanjangan Sewa Tanah {1}<br>
                        seluas {2}, a.n {3} No. {4} tanggal {5}<br>
                        akan berakhir masa berlakunya pada tanggal {6}<br><br><br>
                        Sehubungan hal tersebut diatas, apabila {3}, <br>
                        bermaksud menggunakan kembali bagian tanah HPL seluas {2}, <br>
                        mohon agar dapat segera mengajukan permohonan perpanjangan kepada PT. Pelabuhan Indonesia III (Persero) {7} <br>
                        namun apabila {3} tidak ingin menggunakan kembali bagian tanah HPL seluas {2}, mohon agar dapat menyampaikan kepada PT. Pelindo III {7} <br><br><br>
                        Demikian disampaikan atas perhatiannya diucapkan Terima Kasih.
                        ", name, contract.CONTRACT_NAME, luass, contract.BUSINESS_PARTNER_NAME, contract.LEGAL_CONTRACT_NO, contract.LEGAL_CONTRACT_DATE, contract.CONTRACT_END_DATE, contract.BE_NAME);
                }


            }
            sql = @"select b.CONTRACT_OFFER_NO from PROP_RENTAL_REQUEST a join PROP_CONTRACT_OFFER b on a.CONTRACT_OFFER_NUMBER = b.CONTRACT_OFFER_NO where a.OLD_CONTRACT = :a";
            string contract_offer = connection.ExecuteScalar<string>(sql, new
            {
                a = contract_no.ToString()
            });
            if (contract_offer != null)
            {
                res += "<br>";
                res += bodydetail(contract_offer);
            }
            connection.Close();

            connection.Dispose();
            return res;
        }

        public string bodyContractNotif(string contract_no, string peringatan_no, string type, string name)
        {
            string res = "";
            AUP_TRANS_CONTRACT_WEBAPPS contract = new AUP_TRANS_CONTRACT_WEBAPPS();
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            var sql = @"select *
                    from V_TRANS_SPECIAL_CONT_HEADER where CONTRACT_NO =:a";
            contract = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new
            {
                a = contract_no
            }).DefaultIfEmpty(null).FirstOrDefault();

            var luass = "";

            if (contract.LUAS_BANGUNAN == null || contract.LUAS_BANGUNAN == "0" || contract.LUAS_BANGUNAN == "")
            {
                luass = contract.LUAS_TANAH;
            }
            else
            {
                luass = contract.LUAS_BANGUNAN;
            }

            if (contract != null)
            {
                if (type == "user" || type == "pejabat")
                {
                    res += string.Format(@"Kepada:<br> Yth.<b>{0}</b> <br><br>
                        Dengan ini disampaikan bahwa perjanjian kerjasama Penggunaan Tanah HPL {1}<br>
                        An. {2} No : {3} Seluas {4} m2 untuk kantor <br>  
                        telah berakhir masa berlakunya dan melebihi batas pada tanggal <b>{5}</b> <br><br><br>
                        
                        Sehubungan hal tersebut diatas agar {2} diberikan surat pemeberitahuan <br>
                        untuk melakukan perpanjangan kepada PT Pelabuhan Indonesia III (Persero)  {6}
                        " + link + "", name, contract.CONTRACT_NAME, contract.BUSINESS_PARTNER_NAME, contract.LEGAL_CONTRACT_NO, luass, contract.CONTRACT_END_DATE, contract.BE_NAME);
                }
                else
                {
                    res += string.Format(@"Kepada:<br> Yth.<b>{0}</b> <br><br>
                        Dengan ini disampaikan bahwa sesuai Perjanjian Penyerahan Penggunaan Bagian tanah HPL Perpanjangan Sewa Tanah {1}<br>
                        seluas {2}, a.n {3} No. {4} tanggal {5}<br>
                        telah berakhir masa berlakunya dan melebihi batas pada tanggal {6}<br><br><br>
                        Sehubungan hal tersebut diatas, apabila {3}, <br>
                        bermaksud menggunakan kembali bagian tanah HPL seluas {2} m2, <br>
                        mohon agar dapat segera mengajukan permohonan perpanjangan kepada PT. Pelabuhan Indonesia III (Persero) {7} <br>
                        namun apabila {3} tidak ingin menggunakan kembali bagian tanah HPL seluas {2} m2, mohon agar dapat menyampaikan kepada PT. Pelindo III {7} <br><br><br>
                        Demikian disampaikan atas perhatiannya diucapkan Terima Kasih.
                        ", name, contract.CONTRACT_NAME, luass, contract.BUSINESS_PARTNER_NAME, contract.LEGAL_CONTRACT_NO, contract.LEGAL_CONTRACT_DATE, contract.CONTRACT_END_DATE, contract.BE_NAME);
                }


            }
            sql = @"select b.CONTRACT_OFFER_NO from PROP_RENTAL_REQUEST a join PROP_CONTRACT_OFFER b on a.CONTRACT_OFFER_NUMBER = b.CONTRACT_OFFER_NO where a.OLD_CONTRACT = :a";
            string contract_offer = connection.ExecuteScalar<string>(sql, new
            {
                a = contract_no.ToString()
            });
            if (contract_offer != null)
            {
                res += "<br>";
                res += bodydetail(contract_offer);
            }
            connection.Close();

            connection.Dispose();
            return res;
        }

        public string DataCabang(string kc)
        {
            string res = "";
            dynamic cabang = null;
            try
            {
                IDbConnection connection = DatabaseFactory.GetConnection();
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                var sql = @"select a.BE_ADDRESS, a.BE_CITY, a.BE_NAME, a.POSTAL_CODE, a.BE_CONTACT_PERSON from BUSINESS_ENTITY a where a.BRANCH_ID =:a ";
                cabang = connection.Query<dynamic>(sql, new { a = kc }).FirstOrDefault();

                res += string.Format(@"<br>
                <table align='center' width='90%'>
                        <tr>
                            <td align='left'><b>{3}</b></td>
			                <td align='left'></td>
			                <td align='left'></td>
                        </tr>
		                <tr>
                            <td align='left'>{0}</td>
                            <td align='center'>T: 031 - 3298631 - 37</td>
			                <td align='left'></td>
                        </tr>
		                <tr>
                            <td align='left'>Jawa Timur {2}</td>
			                <td align='center'>F: 031 - 3295204,3295207</td>
			                <td align='right'>www.pelindo.co.id</td>
                        </tr>
                </table>", cabang.BE_ADDRESS, cabang.BE_CITY, cabang.POSTAL_CODE, cabang.BE_NAME);

                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            return res;
        }

        public List<string> buildNotification(int kodeCabang, int apvDetail, int roleProp)
        {

            List<int> roleId = new List<int>();
            List<string> bcc = new List<string>();
            ListNotifikasiDAL dal = new ListNotifikasiDAL();

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                IEnumerable<ListRoleRow> userNotif = dal.GetUserNotification(kodeCabang, apvDetail, roleProp);
                if (userNotif != null)
                {
                    foreach (ListRoleRow user in userNotif)
                    {
                        roleId.Add(user.ROLE_ID);
                    }
                    string roleString = string.Join(",", roleId);

                    string sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                                where a.USER_ROLE_ID in (roleString) and a.KD_CABANG in (
                                    select distinct branch_where from REMOTE.V_BE where branch_id = :b
                                )  and a.APP_ID = 1 and a.STATUS = 1 ";
                    sql = sql.Replace("roleString", roleString);
                    IEnumerable<DataUser> res = connection.Query<DataUser>(sql, new
                    {
                        b = kodeCabang
                    });

                    TestEmail t = new TestEmail();
                    foreach (DataUser temp in res)
                    {
                        if (!string.IsNullOrWhiteSpace(temp.USER_EMAIL))
                        {
                            bcc.Add(
                                string.IsNullOrEmpty(t.TEST_EMAIL_NOTIF()) ? temp.USER_EMAIL : t.TEST_EMAIL_NOTIF()
                            );
                        }
                    }
                }

            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            connection.Close();
            connection.Dispose();
            return bcc;
        }
        public string pushNotif(int roleProperty, string kdCabang)
        {
            string res = "";
            List<DataUser> User = new List<DataUser>();

            object newData = new { };
            dynamic result;
            //var url = _clientFactory.CreateClient("urlRemoteApi");
            string url = Helper.ConfigurationFactory.GetConfiguration("urlRemoteApi", "url");
            string action = "/api/Notification";
            RestClient client = new RestClient(url + action);
            RestRequest request = new RestRequest("", Method.POST);

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = @"select a.USER_LOGIN, a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER a
                                where a.PROPERTY_ROLE =:a and a.KD_CABANG in (
                                    select distinct branch_where from REMOTE.V_BE where branch_id = :b
                                )  and a.APP_ID = 1 and a.STATUS = 1";
                User = connection.Query<DataUser>(sql, new
                {
                    a = roleProperty,
                    b = kdCabang
                }).ToList();

                foreach (DataUser temp in User)
                {
                    //send notifikasi untuk mobile

                    request.AddHeader("Content-Type", "application/json");
                    request.RequestFormat = DataFormat.Json;
                    newData = new
                    {
                        userName = temp.USER_LOGIN,
                        KodeCabang = temp.KD_CABANG,
                        Topic = "P3R-UN-" + temp.USER_LOGIN,
                        Body = "Yth " + temp.ROLE_NAME + ", Kami informasikan terdapat pengajuan permohonan persetujuan izin prinsip penggunaan tanah HPL melalui aplikasi Remote. Mohon untuk di periksa dan di approve/ revise bila ada koreksi, terima kasih.",
                        Title = "Pemberitahuan Contract Offer",
                        Type = "4",
                        NotifName = "Pemberitahuan Contract Offer",
                        Pin = "r3m0tE-p3l1nd03"
                    };
                    var paramFirebase = JsonConvert.SerializeObject(newData);
                    request.AddParameter("text/json", paramFirebase, ParameterType.RequestBody);

                    // execute the request
                    IRestResponse responseOrg = client.Execute(request);
                    result = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content); // raw content as string
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            connection.Close();
            connection.Dispose();
            return res;
        }

        public Results mailKodeBayar(string id, string kodeCabang, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param)
        {
            Results result = new Results();
            IEnumerable<Customer> res = null;
            // var subject = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Released" : (data.COMPLETED_STATUS == "2" ? "Approved" : data.COMPLETED_STATUS == "3" ? "Revised" : "Rejected"));
            var subject = "Payment";
            try
            {
                IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                CustomerDAL dal = new CustomerDAL();
                res = dal.GetAllByKodePelanggan(int.Parse(kodeCabang), data.CUSTOMER_ID);
                TestEmail t = new TestEmail();
                if (res.Count() > 0)
                {
                    foreach (Customer temp in res)
                    {
                        if (temp.MPLG_EMAIL_ADDRESS != null)
                        {
                            var body = string.Empty; // = bodyPayment(temp.ROLE_NAME, data, type, param);
                            body += link;
                            body += DataCabang(kodeCabang);
                            body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                            body += emailfooter();
                            //send email
                            EmailConf email = new EmailConf();
                            email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? temp.MPLG_EMAIL_ADDRESS : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                            email.CASE = "2";//1.email to user || 2.email to pengguna jasa
                            email.BODY = body;
                            email.DESC = "name : " + temp.MPLG_NAMA + ", email : " + temp.MPLG_EMAIL_ADDRESS;
                            email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                            EmailConfigurationDAL dal1 = new EmailConfigurationDAL();
                            var sendingemail = dal1.SendMail(email);
                        }

                    }
                    result.Msg = "Sukses kirim email";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Tidak Ada User";
                    result.Status = "E";
                }

                connection.Close();
                connection.Dispose();

            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }

            return result;
        }
        public string bodyPayment(string rolename, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param)
        {
            string res = "";
            var sql = "";
            string offer_status = "";
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1")
                {
                    sql = @"SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else if (data.COMPLETED_STATUS == "3")
                {
                    sql = @"SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                else
                {
                    sql = @"SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE REF_DATA = :a AND REF_CODE = 'CONTRACT_OFFER_STATUS'";
                }
                offer_status = connection.ExecuteScalar<string>(sql, new
                {
                    a = data.OFFER_STATUS
                });

            }


            if (data != null)
            {
                res = string.Format(@"Kepada Yth.{0} <br><br>
                        Dengan hormat Kami sampaikan pengajuan permohonan approval/persetujuan penggunaan tanah HPL Pelabuhan di aplikasi REMOTE dengan status terakhir telah dilakukan approval/persetujuan oleh : <br>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Status Approval</td>
                                    <td> : {1}</td>
                                </tr>   
                                <tr>
                                    <td>Keterangan di{10}</td>
                                    <td> : {2}</td>
                                </tr> 
                                <tr>
                                    <td>Di{10} Oleh</td>
                                    <td> : {3}</td>
                                </tr>   
                                <tr>
                                    <td>Dibuat Oleh</td>
                                    <td> : {4}</td>
                                </tr> 
                                <tr>
                                    <td>No Contract Offer</td>
                                    <td> : {5}</td>
                                </tr>   
                                <tr>
                                    <td>Nama Contract Offer</td>
                                    <td> : {6}</td>
                                </tr> 
                                <tr>
                                    <td>Pemohon </td>
                                    <td> : {7}</td>
                                </tr>   
                                <tr>
                                    <td>Jenis Permohonan</td>
                                    <td> : {8}</td>
                                </tr> 
                                <tr>
                                    <td>Masa Sewa</td>
                                    <td> : {9}</td>
                                </tr> 
                            </tbody>
                        </table><br>
                        ",
                        rolename, offer_status, param.APPROVAL_NOTE,
                        param.LAST_UPDATE_BY, data.CREATION_BY, data.CONTRACT_OFFER_NO,
                        data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_NAME, data.CONTRACT_OFFER_TYPE,
                        data.CONTRACT_START_DATE + " s/d " + data.CONTRACT_END_DATE + " (" + data.TERM_IN_MONTHS + " Bulan)", type);
            }

            return res;
        }
        public Results emailParaf(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, string kodeCabang, string type, ContractOfferApprovalParameter param, string userLogin, string namaEmpl, dynamic emailto, string note, dynamic isType = null)
        {
            Results result = new Results();
            var subject = dataSurat.SUBJECT;
            if(isType == null)
            {
                isType = 1;
            } 
            try
            {
                TestEmail t = new TestEmail();
                
                var body = isiSurat(dataSurat, emailto.USER_NAME, isType, namaEmpl, note, data.CREATION_BY);
                body += bodyHeaderParaf(data, type, param, emailto.USER_NAME);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                body += link;
                body += DataCabang(kodeCabang);
                body += emailfooter();
                //send email
                EmailConf email = new EmailConf();
                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? emailto.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.DESC = "name : " + emailto.USER_NAME + ", email : " + emailto.USER_EMAIL;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                var sendingemail = dal.SendMail(email);
                //    }
                //}
                result.Msg = "Sukses kirim email";
                result.Status = "S";

                if (dataSurat.TEMBUSAN != null)
                {
                    var tembusan = dal.emailTembusan(data, dataSurat, body);
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }
            return result;
        }

        public Results emailDisposisi(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, string kodeCabang, string type, ContractOfferApprovalParameter param, string userLogin, string namaEmpl, dynamic emailto, string note)
        {
            Results result = new Results();
            EmailConfigurationDAL dal = new EmailConfigurationDAL();

            var subject = dataSurat.SUBJECT;
            try
            {

                TestEmail t = new TestEmail();
                var dataCabang = (dataSurat.BRANCH_ID == null) ? kodeCabang : Convert.ToString(dataSurat.BRANCH_ID);
                var body = isiSurat(dataSurat, emailto.USER_NAME, 0, namaEmpl, note, data.CREATION_BY);
                body += bodyHeaderParaf(data, type, param, emailto.USER_NAME);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                body += link;
                body += DataCabang(dataCabang);
                body += emailfooter();
                //send email
                EmailConf email = new EmailConf();
                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? emailto.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.DESC = "name : " + emailto.USER_NAME + ", email : " + emailto.USER_EMAIL;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;

                var sendingemail = dal.SendMail(email);

                result.Msg = "Sukses kirim email";
                result.Status = "S";

                //var spamEmail = dal.spamEmail(data, dataSurat, kodeCabang, type, param, userLogin, namaEmpl, emailto, note, 0);

            }
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }
            return result;
        }

        public Results emailRejected(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, string kodeCabang, string type, ContractOfferApprovalParameter param, string userLogin, string namaEmpl, dynamic emailto, string note)
        {
            EmailConfigurationDAL dal = new EmailConfigurationDAL();
            Results result = new Results();
            var subject = dataSurat.SUBJECT;
            try
            {

                TestEmail t = new TestEmail();
               
                var body = isiSurat(dataSurat, emailto.USER_NAME, 6, namaEmpl, note, data.CREATION_BY);
                body += bodyHeaderParaf(data, type, param, emailto.USER_NAME);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += !string.IsNullOrWhiteSpace(data.KODE_BAYAR) ? kodeBayar(data.KODE_BAYAR) : "";
                body += link;
                body += DataCabang(kodeCabang);
                body += emailfooter();
                //send email
                EmailConf email = new EmailConf();
                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? emailto.USER_EMAIL : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.DESC = "name : " + emailto.USER_NAME + ", email : " + emailto.USER_EMAIL;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                var sendingemail = dal.SendMail(email);
                //    }
                //}
                result.Msg = "Sukses kirim email";
                result.Status = "S";

                if (dataSurat.TEMBUSAN != null)
                {
                    var tembusan = emailTembusan(data, dataSurat, body);
                }

            }   
            catch (Exception ex)
            {
                result.Msg = "Error" + ex.Message;
                result.Status = "E";
            }
            return result;
        }

        public Results emailTembusan(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, dynamic body)
        {
            EmailConfigurationDAL dal = new EmailConfigurationDAL();
            Results result = new Results();
            var subject = dataSurat.SUBJECT;
            List<string> bcc = new List<string>();
            List<string> cc = new List<string>();
            if (dataSurat.TEMBUSAN != null)
            {
                string[] dataTembusan = dataSurat.TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (dataTembusan != null)
                {
                    foreach (var temp in dataTembusan)
                    {
                        /* if (temp != null)
                         {*/
                        bcc.Add(temp);
                        //}
                    }
                }
            }
            
            //send email
            EmailConf email = new EmailConf();
            email.GROUP_BCC = bcc.Distinct();
            email.GROUP_CC = cc.Distinct();
            email.CASE = "1";//1.email to user || 2.email to pengguna jasa
            email.BODY = body;
            email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
            var sendingemail = dal.SendMail(email);

            result.Msg = "Sukses kirim email";
            result.Status = "S";
            return result;
        }

        //semua pelaku approval, pembuat surat dapat email
        public Results spamEmail(AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, dynamic dataSurat, string kodeCabang, string type, ContractOfferApprovalParameter param, string namaEmpl,  dynamic istype)
        {
            Results result = new Results();
            EmailConf email = new EmailConf();
            IEnumerable<DataUser> res = null;
            var subject = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Released" : (data.COMPLETED_STATUS == "2" || data.COMPLETED_STATUS == "1" ? "Approved" : data.COMPLETED_STATUS == "3" ? "Revised" : "Rejected"));
            var approval = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Mohon dilakukan Approval atas nomor permohonan tersebut<br>" : (data.COMPLETED_STATUS == "2" ? "Mohon dilakukan Approval atas nomor permohonan tersebut<br>" : "<br>"));

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = @"SELECT * FROM V_NOTIF_EMAIL_HISTORY WHERE CONTRACT_NO =:a ";
            res = connection.Query<DataUser>(sql, new
            {
                a = data.CONTRACT_OFFER_NO,
            });

            TestEmail t = new TestEmail();

            if (res.Count() > 0)
            {
                string roleName = "";
                List<string> bcc = new List<string>();
                List<string> cc = new List<string>();
                foreach (DataUser temp in res)
                {
                    roleName = temp.ROLE_NAME;
                    if (!string.IsNullOrWhiteSpace(temp.USER_EMAIL))
                    {
                        string email_to = (string.IsNullOrEmpty(t.TEST_EMAIL_APPROVE()) ? temp.USER_EMAIL : t.TEST_EMAIL_APPROVE());

                        bcc.Add(email_to);

                        //if (string.IsNullOrWhiteSpace(email.DESC))
                        //{
                            email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                        //}
                    }
                }

                //String[] dataTembusan = dataSurat.TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (dataSurat.TEMBUSAN != null)
                {
                    string[] dataTembusan = dataSurat.TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (dataTembusan != null)
                    {
                        foreach (var temp in dataTembusan)
                        {
                            /* if (temp != null)
                             {*/
                            bcc.Add(temp);
                            //}
                        }
                    }
                }
            
                var body = isiSurat(dataSurat, namaEmpl, istype, namaEmpl, param.APPROVAL_NOTE, data.CREATION_BY);
                body += bodyheader(namaEmpl, data, type, param);
                body += bodydetail(data.CONTRACT_OFFER_NO);
                body += link;
                body += approval;
                body += DataCabang(kodeCabang);
                body += emailfooter();
                email.GROUP_BCC = bcc.Distinct();
                email.GROUP_CC = cc.Distinct();
                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                email.BODY = body;
                email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                var sendingemail = SendMail(email);
                result.Msg = "Sukses kirim email. (0)";
                result.Status = "S";
            }

            connection.Close();
            connection.Dispose();
            return result;
        }
    }
}