﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models.MasterRentalObject;
using Remote.Models.TarifPedoman;
using Remote.Models.UserModels;
using Remote.Models.MasterInstallation;
using FDDProfitCenter = Remote.Models.MasterRentalObject.FDDProfitCenter;

namespace Remote.DAL
{
    public class TarifPedomanDAL
    {

        //--------------------------------- ADD NEW DATA ---------------------------
        public DataReturnTarifPedoman AddPedoman(string name, DataTarifPedoman data)
        {
            DataReturnTarifPedoman result = new DataReturnTarifPedoman();

            result.RESULT_STAT = false;
            result.MessageInfo = string.Empty;

            using (var connection = DatabaseFactory.GetConnection("default"))
            {

                var sqlPedoman = @"INSERT INTO PROP_TARIF_LAHAN 
                            (BRANCH_ID, PROFIT_CENTER_ID, TIPE_TARIF, START_ACTIVE_DATE, END_ACTIVE_DATE, CREATED_BY, CREATED_TIME) 
                            VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, CURRENT_TIMESTAMP) ";
                var sqlPedomanEdit = @"update PROP_TARIF_LAHAN 
                            set BRANCH_ID = :a, PROFIT_CENTER_ID = :b, TIPE_TARIF = :c, START_ACTIVE_DATE = to_date(:d,'DD/MM/YYYY'), 
                            END_ACTIVE_DATE = to_date(:e,'DD/MM/YYYY'), UPDATED_BY = :f, UPDATED_TIME = CURRENT_TIMESTAMP 
                            where ID = :i";
                var sql = @"INSERT INTO PROP_TARIF_LAHAN_DETAIL 
                            (TARIF_LAHAN_ID, INDUSTRY_ID, BATAS_LUAS, TARIF, PERCENTAGE, CREATED_BY, CREATED_TIME) 
                            VALUES (:a, :b, :c, :d, :e, :f, CURRENT_TIMESTAMP) ";
                var sqlEdit = @"update PROP_TARIF_LAHAN_DETAIL a
                                set INDUSTRY_ID = :b, BATAS_LUAS = :c, TARIF = :d, PERCENTAGE = :e, CREATED_BY = :f, CREATED_TIME = CURRENT_TIMESTAMP
                                where ID = :i";

                
                
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                using (var transaction = connection.BeginTransaction())
                {

                    try
                    {

                        int r = 0;

                        if (data.ID != 0)
                        {
                            r = connection.Execute(sqlPedomanEdit, new
                            {
                                i = data.ID,
                                a = data.BRANCH_ID,
                                b = data.PROFIT_CENTER_ID,
                                c = data.TIPE_TARIF,
                                d = data.START_ACTIVE_DATE,
                                e = data.END_ACTIVE_DATE,
                                f = name
                            }, transaction: transaction);

                        } else
                        {
                            r = connection.Execute(sqlPedoman, new
                            {
                                a = data.BRANCH_ID,
                                b = data.PROFIT_CENTER_ID,
                                c = data.TIPE_TARIF,
                                d = data.START_ACTIVE_DATE,
                                e = data.END_ACTIVE_DATE,
                                f = name
                            }, transaction: transaction);

                        }

                        if (data.ID == 0)
                        {
                            string sqlSeq = "select SEQ_PROP_TARIF_LAHAN.CURRVAL from dual";
                            int id_tarif_lahan = connection.ExecuteScalar<int>(sqlSeq);
                            //coba mas,ini bukanp
                            result.ID = id_tarif_lahan;
                        }

                        foreach (var d in data.tarif)
                        {
                            if (d.id != 0)
                            {
                                r = connection.Execute(sqlEdit, new {
                                    i = d.id,
                                    b = d.kode,
                                    c = d.batas,
                                    d = d.value,
                                    e = d.percentage,
                                    f = name
                                }, transaction: transaction);
                            } else
                            {
                                r = connection.Execute(sql, new
                                {
                                    a = result.ID,
                                    b = d.kode,
                                    c = d.batas,
                                    d = d.value,
                                    e = d.percentage,
                                    f = name
                                }, transaction: transaction);
                            }
                            
                        }

                        transaction.Commit();
                        result.RESULT_STAT = true;
                        result.MessageInfo += "Tarif Pedoman telah disimpan!";
                    }
                    catch (Exception ex) {
                        transaction.Rollback();
                        result.MessageInfo += "{0} " + ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
                
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return result;
        }

        //--------------------------------- ADD NEW DATA NJOP ---------------------------
        public DataReturnTarifPedoman AddNJOP(string name, DataNJOP data)
        {
            DataReturnTarifPedoman result = new DataReturnTarifPedoman();

            result.RESULT_STAT = false;
            result.MessageInfo = string.Empty;
            using (var connection = DatabaseFactory.GetConnection())
            {

                var sqlPedoman = @"INSERT INTO PROP_TARIF_LAHAN_NJOP 
                            (BRANCH_ID, KEYWORD, VALUE, CREATED_BY, CREATED_TIME, ACTIVE_YEAR) 
                            VALUES (:a, :b, :c, :d, CURRENT_TIMESTAMP, :e) ";
                var sqlPedomanEdit = @"update PROP_TARIF_LAHAN_NJOP 
                            set BRANCH_ID = :a, KEYWORD = :b, VALUE = :c, CREATED_BY = :d, CREATED_TIME = CURRENT_TIMESTAMP, ACTIVE_YEAR = :e
                            where ID = :i";



                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                using (var transaction = connection.BeginTransaction())
                {

                    try
                    {

                        int r = 0;

                        if (data.ID != 0)
                        {
                            r = connection.Execute(sqlPedomanEdit, new
                            {
                                i = data.ID,
                                a = data.BRANCH_ID,
                                b = data.KEYWORD,
                                c = data.VALUE,
                                d = name,
                                e = data.ACTIVE_YEAR
                            }, transaction: transaction);

                        }
                        else
                        {
                            r = connection.Execute(sqlPedoman, new
                            {
                                a = data.BRANCH_ID,
                                b = data.KEYWORD,
                                c = data.VALUE,
                                d = name,
                                e = data.ACTIVE_YEAR
                            }, transaction: transaction);

                        }
                        
                        transaction.Commit();
                        result.RESULT_STAT = true;
                        result.MessageInfo += "Tarif NJOP telah disimpan!";
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        result.MessageInfo += "{0} " + ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }

                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return result;
        }

        //--------------------------------- GET PEDOMAN DATA (get detail) ---------------------------
        public DataTarifPedoman GetPedoman(int id)
        {
            DataTarifPedoman result = new DataTarifPedoman();
            
            result.ID = id;

            string sqlHeader = @"select
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID
                                    where a.ID=:a ";
            string sql = @"select ID as id, INDUSTRY_ID as kode, BATAS_LUAS as batas, TARIF as value, PERCENTAGE as percentage
                            from PROP_TARIF_LAHAN_DETAIL where TARIF_LAHAN_ID=:a";
            try {
                using (var connection = DatabaseFactory.GetConnection())
                {
                    if (connection.State.Equals(ConnectionState.Closed))
                        connection.Open();

                    IEnumerable<TARIF_PEDOMAN_HEADER> header = connection.Query<TARIF_PEDOMAN_HEADER>(sqlHeader, new { a = id });
                    TARIF_PEDOMAN_HEADER data = header.First();
                    result.ID = data.ID;
                    result.BE_ID = data.BE_ID;
                    result.BE_NAME = data.BE_NAME;
                    result.BRANCH_ID = data.BRANCH_ID;
                    result.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                    result.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                    result.START_ACTIVE_DATE = data.START_ACTIVE_DATE;
                    result.END_ACTIVE_DATE = data.END_ACTIVE_DATE;
                    result.TIPE_TARIF = data.TIPE_TARIF;

                    result.tarif = connection.Query<Tarif>(sql, new { a = id }).ToList();

                    if (connection.State.Equals(ConnectionState.Open))
                        connection.Close();
                }
            } catch (Exception ex) {
                string aaa = ex.ToString();
            }
            return result;
        }

        //--------------------------------- GET ACTIVE PEDOMAN DATA BY BE ID (contract offer) ---------------------------
        public DataTarifPedoman GetActivePedoman(string beid, string profitcenterid)
        {
            DataTarifPedoman result = new DataTarifPedoman();
            

            string sqlHeader = @"select
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where 
                                    SYSDATE >= a.START_ACTIVE_DATE 
                                    and SYSDATE <= a.END_ACTIVE_DATE 
                                    and b.BE_ID=:a 
                                    and (
                                        a.PROFIT_CENTER_ID=:b
                                        or a.PROFIT_CENTER_ID=0
                                    ) order by a.PROFIT_CENTER_ID desc, a.END_ACTIVE_DATE desc, a.ID desc ";
            string sql = @"select ID as id, INDUSTRY_ID as kode, BATAS_LUAS as batas, TARIF as value, PERCENTAGE as percentage
                            from PROP_TARIF_LAHAN_DETAIL where TARIF_LAHAN_ID=:a";
            try
            {

                using (var connection = DatabaseFactory.GetConnection())
                {
                    if (connection.State.Equals(ConnectionState.Closed))
                        connection.Open();

                    IEnumerable<TARIF_PEDOMAN_HEADER> header = connection.Query<TARIF_PEDOMAN_HEADER>(sqlHeader, new { a = beid, b = profitcenterid });
                    TARIF_PEDOMAN_HEADER data = header.First();
                    result.ID = data.ID;
                    result.BE_ID = data.BE_ID;
                    result.BE_NAME = data.BE_NAME;
                    result.BRANCH_ID = data.BRANCH_ID;
                    result.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                    result.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                    result.START_ACTIVE_DATE = data.START_ACTIVE_DATE;
                    result.END_ACTIVE_DATE = data.END_ACTIVE_DATE;
                    result.TIPE_TARIF = data.TIPE_TARIF;

                    result.tarif = connection.Query<Tarif>(sql, new { a = data.ID }).ToList();

                    if (connection.State.Equals(ConnectionState.Open))
                        connection.Close();
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            return result;
        }

        //--------------------------------- GET NJOP DATA ---------------------------
        public DataNJOP GetNjop(int id)
        {
            DataNJOP result = new DataNJOP();

            result.ID = id;

            string sqlHeader = @"select
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID
                                    where a.ID=:a ";
            try
            {

                using (var connection = DatabaseFactory.GetConnection())
                {
                    if (connection.State.Equals(ConnectionState.Closed))
                        connection.Open();

                    IEnumerable<DataNJOP> header = connection.Query<DataNJOP>(sqlHeader, new { a = id });

                    if (connection.State.Equals(ConnectionState.Open))
                        connection.Close();
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            return result;
        }

        //--------------------------------- DEPRECATED ---------------------------
        public IDataTable GetData(int draw, int start, int length, string search, int KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                if (KodeCabang == 0)
                {
                    if (search.Length == 0)
                    {
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY') as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where 
                                        SYSDATE >= a.START_ACTIVE_DATE
                                        and SYSDATE <= a.END_ACTIVE_DATE 
                                    order by a.END_ACTIVE_DATE DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new { a = start, b = end }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { });
                    }
                    else
                    {
                        //search filter data
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where CURRENT_DATE >= a.START_ACTIVE_DATE
                                    and CURRENT_DATE <= a.END_ACTIVE_DATE
                                 and ((UPPER(b.BE_NAME) LIKE '%' ||:c|| '%') or (UPPER(a.TIPE_TARIF) LIKE '%' ||:c|| '%')) ORDER BY a.END_ACTIVE_DATE DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search.ToUpper() });
                    }
                } else
                {

                    if (search.Length == 0)
                    {
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY') as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where 
                                        SYSDATE >= a.START_ACTIVE_DATE
                                        and SYSDATE <= a.END_ACTIVE_DATE 
                                        and a.BRANCH_ID = :c
                                    order by a.END_ACTIVE_DATE DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new { a = start, b = end, c = KodeCabang }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                    }
                    else
                    {
                        //search filter data
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where CURRENT_DATE >= a.START_ACTIVE_DATE
                                    and CURRENT_DATE <= a.END_ACTIVE_DATE
                                    and a.BRANCH_ID = :d
                                    and ((UPPER(a.TIPE_TARIF) LIKE '%' ||:c|| '%')) ORDER BY a.END_ACTIVE_DATE DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search.ToUpper(), d = KodeCabang });
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //--------------------------------- DEPRECATED ---------------------------
        public IDataTable GetDataHistory(int draw, int start, int length, string search, int KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (KodeCabang == 0)
                {
                    if (search.Length == 0)
                    {
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where (
                                        CURRENT_DATE < a.START_ACTIVE_DATE
                                        or CURRENT_DATE > a.END_ACTIVE_DATE
                                    )
                                    order by a.BRANCH_ID desc ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new { a = start, b = end }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { });
                    }
                    else
                    {
                        //search filter data
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where (
                                        CURRENT_DATE < a.START_ACTIVE_DATE
                                        or CURRENT_DATE > a.END_ACTIVE_DATE
                                    )
                                    and ((UPPER(a.TIPE_TARIF) LIKE '%' ||:c|| '%') or (UPPER(b.BE_NAME) LIKE '%' ||:c|| '%')) ORDER BY a.BRANCH_ID DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search.ToUpper() });
                    }
                } else
                {
                    if (search.Length == 0)
                    {
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where (
                                        CURRENT_DATE < a.START_ACTIVE_DATE
                                        or CURRENT_DATE > a.END_ACTIVE_DATE
                                    ) AND a.BRANCH_ID = :c
                                    order by a.BRANCH_ID desc ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new { a = start, b = end, c = KodeCabang }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = KodeCabang });
                    }
                    else
                    {
                        //search filter data
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where (
                                        CURRENT_DATE < a.START_ACTIVE_DATE
                                        or CURRENT_DATE > a.END_ACTIVE_DATE
                                    ) AND a.BRANCH_ID = :d
                                    and ((UPPER(a.TIPE_TARIF) LIKE '%' ||:c|| '%') or (UPPER(b.BE_NAME) LIKE '%' ||:c|| '%')) ORDER BY a.BRANCH_ID DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper(),
                            d = KodeCabang
                        }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search.ToUpper(), d = KodeCabang });
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //--------------------------------- VIEW DATATABLE NJOP ---------------------------
        public IDataTable GetDataNJOP(int be_id, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.VALUE, a.KEYWORD, a.ACTIVE, a.ACTIVE_YEAR, b.BE_NAME from PROP_TARIF_LAHAN_NJOP a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    where a.BRANCH_ID = :be_id
                                    order by a.ACTIVE_YEAR desc ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DataNJOP>(fullSql, new { a = start, b = end, be_id = be_id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, be_id = be_id });
                }
                else
                {
                    //search filter data
                    string sql = @"select a.ID, a.BRANCH_ID, b.BE_ID, a.VALUE, a.KEYWORD, a.ACTIVE, a.ACTIVE_YEAR, b.BE_NAME from                       PROP_TARIF_LAHAN_NJOP a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                 WHERE a.BRANCH_ID = :be_id AND UPPER(a.KEYWORD) LIKE '%' ||:c|| '%' ORDER BY a.BRANCH_ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DataNJOP>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        be_id = be_id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, be_id = be_id });
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //--------------------------------- VIEW DATATABLE ACTIVE NJOP ---------------------------
        public IDataTable GetDataActiveNJOP(int be_id, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.VALUE, a.KEYWORD, a.ACTIVE, b.BE_NAME from PROP_TARIF_LAHAN_NJOP a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    where b.BE_ID = :be_id
                                    and a.ACTIVE = 1
                                    order by a.BRANCH_ID desc ";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<DataNJOP>(fullSql, new { a = start, b = end, be_id = be_id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2, be_id = be_id });
                }
                else
                {
                    //search filter data
                    string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.VALUE, a.KEYWORD, a.ACTIVE, b.BE_NAME from PROP_TARIF_LAHAN_NJOP a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                 WHERE b.BE_ID = :be_id AND a.ACTIVE = 1 AND UPPER(b.KEYWORD) LIKE '%' ||:c|| '%')) ORDER BY a.BRANCH_ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        be_id = be_id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search, be_id = be_id });
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //--------------------------------- VIEW DATATABLE ACTIVE PEDOMAN ---------------------------
        public IDataTable GetDataActivePedoman(int be_id, string kode_industri, int batas_luas, int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                
                //search filter data
                string sql = @"SELECT ID, TIPE_TARIF
                                , to_char(START_ACTIVE_DATE, 'DD/MM/YYYY') START_ACTIVE_DATE
                                , to_char(END_ACTIVE_DATE, 'DD/MM/YYYY') END_ACTIVE_DATE
                                , INDUSTRY_TYPE, PROFIT_CENTER_ID, TERMINAL_NAME, VALUE 
                                FROM V_ACTIVE_PEDOMAN 
                                WHERE BE_ID = :d AND INDUSTRY_ID = :e AND BATAS_LUAS = :f ORDER BY END_ACTIVE_DATE DESC";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<ActivePedoman>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = be_id,
                    e = kode_industri,
                    f = batas_luas
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = be_id, e = kode_industri, f = batas_luas });
                
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public Results InactiveNJOP(string name, DataNJOP data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_TARIF_LAHAN_NJOP a
                            set a.ACTIVE = 0, a.INACTIVE_MEMO = :b, a.INACTIVE_AT = CURRENT_TIMESTAMP, a.INACTIVE_BY = :c
                            where a.ID =:a";
                r = connection.Execute(sql, new
                {
                    a = data.ID,
                    b = data.INACTIVE_MEMO,
                    c = name,
                });
                
                if (r > 0)
                {
                    result.Msg = "Data has been Inactive";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Inactive Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results ActiveNJOP(string name, DataNJOP data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_TARIF_LAHAN_NJOP a
                            set a.ACTIVE = 1, a.INACTIVE_MEMO = NULL, a.INACTIVE_AT = NULL, a.INACTIVE_BY = NULL
                            where a.ID =:a";
                r = connection.Execute(sql, new
                {
                    a = data.ID
                });

                if (r > 0)
                {
                    result.Msg = "Data has been activate";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Activate Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results InactivePedoman(string name, TARIF_PEDOMAN_HEADER data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_TARIF_LAHAN a
                            set a.INACTIVE = 1, a.INACTIVE_MEMO = :b, a.INACTIVE_AT = CURRENT_TIMESTAMP, a.INACTIVE_BY = :c
                            where a.ID =:a";
                r = connection.Execute(sql, new
                {
                    a = data.ID,
                    b = data.INACTIVE_MEMO,
                    c = name,
                });

                if (r > 0)
                {
                    result.Msg = "Data has been Inactive";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Inactive Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results ActivePedoman(string name, TARIF_PEDOMAN_HEADER data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update PROP_TARIF_LAHAN a
                            set a.INACTIVE = 0, a.INACTIVE_MEMO = NULL, a.INACTIVE_AT = NULL, a.INACTIVE_BY = NULL
                            where a.ID =:a";
                r = connection.Execute(sql, new
                {
                    a = data.ID
                });

                if (r > 0)
                {
                    result.Msg = "Data has been activate";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Activate Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        //--------------------------------- NEW VIEW DATATABLE PEDOMAN ---------------------------
        public IDataTable GetDatanew(int draw, int start, int length, string search, int KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                string wheresearch = (search.Length >= 2 ? " AND ((UPPER(b.BE_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%') or (UPPER(a.TIPE_TARIF) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
                QueryGlobal query = new QueryGlobal();
                string v_be = query.Query_Tingkat();
                                    
                        string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY') as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where 
                                        SYSDATE >= a.START_ACTIVE_DATE
                                        and SYSDATE <= a.END_ACTIVE_DATE AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) " + wheresearch +
                                    "order by a.END_ACTIVE_DATE DESC ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new { a = start, b = end, d = KodeCabang }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
                                                    
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //--------------------------------- NEW VIEW HISTORY --------------------------------------
        public IDataTable GetDataHistorynew(int draw, int start, int length, string search, int KodeCabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                string wheresearch = (search.Length >= 2 ? " AND ((UPPER(a.TIPE_TARIF) LIKE '%' || '" + search.ToUpper() + "' || '%') or (UPPER(b.BE_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
                QueryGlobal query = new QueryGlobal();
                string v_be = query.Query_Tingkat("a");

                //search filter data
                string sql = @"select 
                                    a.ID, a.BRANCH_ID, b.BE_ID, a.TIPE_TARIF, b.BE_NAME, a.PROFIT_CENTER_ID, c.TERMINAL_NAME as PROFIT_CENTER_NAME, 
                                    to_char(a.START_ACTIVE_DATE, 'DD/MM/YYYY') as START_ACTIVE_DATE, to_char(a.END_ACTIVE_DATE, 'DD/MM/YYYY')  as END_ACTIVE_DATE
                                    from PROP_TARIF_LAHAN a 
                                    left join BUSINESS_ENTITY b on b.BRANCH_ID = a.BRANCH_ID
                                    left join PROFIT_CENTER c on c.PROFIT_CENTER_ID = a.PROFIT_CENTER_ID 
                                    where (
                                        CURRENT_DATE < a.START_ACTIVE_DATE
                                        or CURRENT_DATE > a.END_ACTIVE_DATE
                                    ) " + v_be + wheresearch;

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<TARIF_PEDOMAN_HEADER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            //c = search.ToUpper()
                            d = KodeCabang
                        }).ToList();
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
                                                    
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IEnumerable<FDDProfitCenter> GetProfitCenter(string be_id)
        {
            string sql = "SELECT PROFIT_CENTER_ID, TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID = :a";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<FDDProfitCenter> listDetil = connection.Query<FDDProfitCenter>(sql, new
            {
                a = be_id
            });
            connection.Close();
            return listDetil;
        }

        public bool AddFiles(string sFile, string file2, string id, string type)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = string.Empty;
            if(type == "PEDOMAN")
            {
                sql = @"INSERT INTO PROP_TARIF_LAHAN_ATTACHMENT 
                    (DIRECTORY, FILE_NAME, TARIF_LAHAN_ID) 
                    VALUES (:a, :b, :c)";
            } else if (type == "NJOP")
            {
                sql = @"INSERT INTO PROP_TARIF_LAHAN_ATTACHMENT 
                    (DIRECTORY, FILE_NAME, BRANCH_ID) 
                    VALUES (:a, :b, :c)";
            }

            if (!string.IsNullOrEmpty(sql)) { 
                int r = connection.Execute(sql, new
                {
                    a = file2,
                    b = sFile,
                    c = id,
                    //d = type,
                });

                result = (r > 0) ? true : false;
            }
            connection.Close();
            return result;
        }

        public IDataTable GetDataAttachment(int draw, int start, int length, string search, string id_special, string type)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_OFFER_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = string.Empty;
                    if (type == "PEDOMAN")
                    {
                        sql = @"SELECT ID_ATTACHMENT, DIRECTORY, FILE_NAME, TARIF_LAHAN_ID, ID 
                            FROM PROP_TARIF_LAHAN_ATTACHMENT 
                            WHERE TARIF_LAHAN_ID =:c ORDER BY ID_ATTACHMENT DESC";
                    }
                    else if (type == "NJOP")
                    {
                        sql = @"SELECT ID_ATTACHMENT, DIRECTORY, FILE_NAME, BRANCH_ID , ID
                            FROM PROP_TARIF_LAHAN_ATTACHMENT 
                            WHERE BRANCH_ID =:c ORDER BY ID_ATTACHMENT DESC";
                    }

                    if (!string.IsNullOrEmpty(sql))
                    {
                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_OFFER_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                    }
                    
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }
        
        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"SELECT ID 
                        FROM PROP_TARIF_LAHAN_ATTACHMENT 
                        WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = @"DELETE FROM PROP_TARIF_LAHAN_ATTACHMENT 
                        WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            return result;
        }

    }
}