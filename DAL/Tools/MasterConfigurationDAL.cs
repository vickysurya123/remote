﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
using Remote.Models.MasterConfiguration;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class MasterConfigurationDAL
    {
        
        //--------------------------------- VIEW DATA CONFIG ---------------------------
        public DataTablesConfiguration GetDataConfig(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            DataTablesConfiguration result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT ref_code_c, ref_data, ref_desc_c, id, active FROM V_CONFIG ORDER BY REF_CODE_C, REF_DATA ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_CONFIGURATION_WEBAPPS>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                //search filter data
                if (search.Length > 2)
                {
                    try
                    {
                        string sql = "SELECT ref_code_c, ref_data, ref_desc_c, id, active FROM V_CONFIG  " +
                                 "WHERE ((UPPER(ref_code_c) LIKE '%' ||:c|| '%') OR (UPPER(ref_data) LIKE '%' ||:c|| '%') OR (UPPER(REF_DESC_C) LIKE '%' ||:c|| '%')) ORDER BY REF_CODE_C, REF_DATA ASC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_CONFIGURATION_WEBAPPS>(fullSql, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            a = start,
                            b = end,
                            c = search.ToUpper()
                        });
                    }
                    catch (Exception)
                    {

                    }
                } 
            }
            result = new DataTablesConfiguration();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        //---------------------- EDIT VIEW CONFIGURATION ---------------------------
        public AUP_CONFIGURATION_WEBAPPS GetDataForEdit(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "SELECT id, ref_code_c, ref_data, ref_desc_c, attrib1, val1, attrib2, val2, attrib3, val3, attrib4, val4 " +
                         "FROM V_CONFIG " +
                         "WHERE id = :a";

            AUP_CONFIGURATION_WEBAPPS result = connection.Query<AUP_CONFIGURATION_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            return result;
        }

        //------------------------------------ DELETE DATA CONFIG -----------------------------
        public dynamic DeleteData(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM PROP_PARAMETER_REF_D " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_PARAMETER_REF_D " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            return result;
        }

        //------------------------------- EDIT DATA CONFIG --------------------------------
        public bool Edit(string name, DATAREF_D data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "UPDATE PROP_PARAMETER_REF_D SET " +
                         "REF_DESC=:b, ATTRIB1=:c, VAL1=:d, ATTRIB2=:e, VAL2=:f, ATTRIB3=:g, VAL3=:h, ATTRIB4=:i, VAL4=:j " +
                         "WHERE ID=:a";

            int r = connection.Execute(sql, new
            {
                a = data.ID,
                b = data.REF_DESC,
                c = data.ATTRIB1,
                d = data.VAL1,
                e = data.ATTRIB2,
                f = data.VAL2,
                g = data.ATTRIB3,
                h = data.VAL3,
                i = data.ATTRIB4,
                j = data.VAL4
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        //---------------------- LIST DROP DOWN CONFIGURATION ---------------------------
        public IEnumerable<DDConfig> GetDataListConfig()
        {
            string sql = "SELECT DISTINCT REF_CODE FROM PROP_PARAMETER_REF_D";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDConfig> listDetil = connection.Query<DDConfig>(sql);
            connection.Close();
            return listDetil;
        }

        //---------------------------- INSERT DATA CONFIG -----------------------
        public bool Add(string name, DATAREF_D data)
        {

            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_PARAMETER_REF_D " +
                "(ref_code, ref_data, ref_desc, active, attrib1, val1, attrib2, val2, attrib3, val3, attrib4, val4) " +
                "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l)";

            int r = connection.Execute(sql, new
            {
                a = data.REF_CODE,
                b = data.REF_DATA,
                c = data.REF_DESC,
                d = "1",
                e = data.ATTRIB1,
                f = data.VAL1,
                g = data.ATTRIB2,
                h = data.VAL2,
                i = data.ATTRIB3,
                j = data.VAL3,
                k = data.ATTRIB4,
                l = data.VAL4
            });

            if (data.REF_CODE == "SERVICE_GROUP")
            {
                string sqlTwo = "INSERT INTO PROP_PENOMORAN(BE_ID, MODULE_TYPE, PARAM1, NOMOR_AKTIF) " +
                                "VALUES (:a, :b, :c, :d)";
                int x = connection.Execute(sqlTwo, new
                {
                    a = "1",
                    b = "VARIOUS_BUSINESS",
                    c = data.REF_DATA,
                    d = "1"
                });
            }

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        //--------------------

        public DataTablesConfiguration GetDataDetilConfig(int draw, int start, int length, string search, string ref_code_c)
        {
            int count = 0;
            int end = start + length;
            DataTablesConfiguration result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT ref_code_c, ref_data, ref_desc_c, id " +
                        "FROM V_CONFIG WHERE ID = ref_code_c ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_CONFIGURATION_WEBAPPS>(fullSql, new { a = start, b = end });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesConfiguration();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        //----------------------------- UBAH STATUS -----------------------

        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ACTIVE " +
                "FROM PROP_PARAMETER_REF_D " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE PROP_PARAMETER_REF_D " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            return result;
        }

        public IEnumerable<AUP_CONFIGURATION_WEBAPPS> GetLastID(string REF_CODE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> listData = null;

            try
            {

                string sql = "SELECT * FROM(select REF_DATA from PROP_PARAMETER_REF_D WHERE REF_CODE=:a order by id desc) where ROWNUM = 1";

                listData = connection.Query<AUP_CONFIGURATION_WEBAPPS>(sql, new { a = REF_CODE }).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
    }
}