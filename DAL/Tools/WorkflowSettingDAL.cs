﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.Models;
using Remote.Models.WorkflowSetting;
using Remote.ViewModels;
using Remote.Helpers;

namespace Remote.DAL
{
    public class WorkflowSettingDAL
    {
        //--------------------------------- VIEW DATA CONFIG ---------------------------
        public DataTablesWorkflowSetting GetDataWorkflowSetting(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            DataTablesWorkflowSetting result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            IEnumerable<AUP_WORKFLOW_SETTING> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                string sql = "SELECT BE_ID, OPERATOR_CABANG_EMAIL, OPERATOR_CABANG_PHONE, OPERATOR_PUSAT_EMAIL, OPERATOR_PUSAT_PHONE, MAX_SEQ_APPROVER_CABANG, " +
                            "IS_REJECT_PUSAT_INFO_CABANG, IS_REVISE_PUSAT_INFO_CABANG " +
                            "FROM V_WORKFLOW_SETTING";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_WORKFLOW_SETTING>(fullSql, new { a = start, b = end });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount);
            }
            else
            {
                //search filter data
                if (search.Length > 2)
                {
                    string sql = "SELECT BE_ID, OPERATOR_CABANG_EMAIL, OPERATOR_CABANG_PHONE, OPERATOR_PUSAT_EMAIL, OPERATOR_PUSAT_PHONE, MAX_SEQ_APPROVER_CABANG, " +
                                 "IS_REJECT_PUSAT_INFO_CABANG, IS_REVISE_PUSAT_INFO_CABANG FROM V_WORKFLOW_SETTING " +
                                 "WHERE ((UPPER(OPERATOR_CABANG_EMAIL) LIKE '%' ||:c|| '%') OR (UPPER(OPERATOR_PUSAT_EMAIL) LIKE '%' ||:c|| '%') OR (OPERATOR_PUSAT_PHONE) LIKE '%' ||:c|| '%') OR (OPERATOR_CABANG_PHONE) LIKE '%' ||:c|| '%') ORDER BY BE_ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_WORKFLOW_SETTING>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            result = new DataTablesWorkflowSetting();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            return result;
        }
        public IEnumerable<DDBusinessEntity> getDataBusinessEntity(string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBusinessEntity> listData = null;

            try
            {
                // string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC, REF_DATA || ' - ' || REF_DESC AS SALES_TYPE, ATTRIB1 AS UNIT, VAL1 AS TARIF FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'SALES_TYPE'";
                string sql = "SELECT BE_ID, BE_NAME FROM BUSINESS_ENTITY ORDER BY BE_ID ASC";

                listData = connection.Query<DDBusinessEntity>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }
    }
}