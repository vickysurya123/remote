﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;

namespace Remote.DAL
{
    public class ParameterDAL
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"SELECT ID, PARAMETER_NAME, PARAMETER_COLUMN, PARAMETER_TABLE FROM APPROVAL_SETTING_PARAMETER";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<APPROVAL_SETTING_PARAMETER>(fullSql, new { a = start, b = end }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = 2 });
                }
                else
                {
                    //search filter data
                    string sql = @"SELECT ID, PARAMETER_NAME, PARAMETER_COLUMN, PARAMETER_TABLE FROM APPROVAL_SETTING_PARAMETER
                                     WHERE ((UPPER(PARAMETER_NAME) LIKE '%' ||:c|| '%') or (UPPER(PARAMETER_COLUMN) LIKE '%' ||:c|| '%')) ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<APPROVAL_SETTING_PARAMETER>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper()
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }
        public Results AddDataHeader(APPROVAL_SETTING_PARAMETER data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"select count(*) from APPROVAL_SETTING_PARAMETER where PARAMETER_NAME =:a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.PARAMETER_NAME
                });

                if (r > 0)
                {
                    result.Msg = "Approval Setting for Business Entity Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into APPROVAL_SETTING_PARAMETER (PARAMETER_NAME, PARAMETER_TABLE, PARAMETER_COLUMN)
                            values(:a, :b, :c)";
                    r = connection.Execute(sql, new
                    {
                        a = data.PARAMETER_NAME,
                        b = data.PARAMETER_TABLE,
                        c = data.PARAMETER_COLUMN
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }
        public Results EditDataHeader(APPROVAL_SETTING_PARAMETER data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sql = @"update APPROVAL_SETTING_PARAMETER a set a.PARAMETER_NAME =:a,  a.PARAMETER_TABLE =:b, a.PARAMETER_COLUMN =:c where a.ID =:d";
                    r = connection.Execute(sql, new
                    {
                        a = data.PARAMETER_NAME,
                        b = data.PARAMETER_TABLE,
                        c = data.PARAMETER_COLUMN,
                        d = data.ID,
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Updated Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Update Data";
                        result.Status = "E";

                    }
                
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }
        public Results DeleteDataHeader(APPROVAL_SETTING_PARAMETER data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {

                var sql = @"delete from APPROVAL_SETTING_PARAMETER a where a.ID =:c";
                r = connection.Execute(sql, new
                {
                    c = data.ID
                });
                if (r > 0)
                {
                    result.Msg = "Data Deleted Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Delete Data";
                    result.Status = "E";

                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }
    }
}