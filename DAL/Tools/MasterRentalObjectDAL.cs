﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
using Remote.Models;
using Remote.Models.MasterRentalObject;
using Remote.ViewModels;
using Remote.Controllers;
using Remote.Helpers;
using Microsoft.AspNetCore.Authorization;
using Remote.Models.RentalObject;

namespace Remote.DAL
{
    public class MasterRentalObjectDAL
    {
        //---------------------------------------- DEPRECATED --------------------------------
        public DataTablesMasterRentalObject GetDataRentalObject(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterRentalObject result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_RENTALOBJECT_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.BRANCH_ID, q.RO_NAME, q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, " +
                                 "q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, TO_CHAR (q.VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR (q.VALID_TO, 'DD.MM.RRRR') VALID_TO, q.LINI, q.PERUNTUKAN " +
                                 "w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE CAST (REGEXP_REPLACE (q.FUNCTION_ID, '[^0-9]+', '') AS NUMBER) = ID) FUNCTION_ID, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                 "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                                 "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c " +
                                 "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW " +
                                 "ORDER BY q.RO_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = 2 });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {

                        try
                        {
                            string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.RO_NAME, q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, " +
                                     "q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, " +
                                     "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                     "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                     "w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                     "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                                     "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c " +
                                     "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID=q.PROFIT_CENTER_ID_NEW " +
                                     "AND ((q.RO_NUMBER LIKE '%'||:c||'%') OR (q.RO_CODE LIKE '%'||:c||'%') OR (upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) LIKE '%'||:c||'%') " +
                                     "OR (UPPER(q.RO_NAME) LIKE '%'||:c||'%') " +
                                     "OR ((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%'||:c||'%') " +
                                     "OR (c.TERMINAL_NAME LIKE '%'||:c||'%')) " +
                                     "ORDER BY q.RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }

            }

            else {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.BRANCH_ID," +
                                   "q.RO_NAME, " +
                                   "q.RO_CERTIFICATE_NUMBER, " +
                                   "q.RO_ADDRESS, " +
                                   "q.RO_POSTALCODE, " +
                                   "q.MEMO, " +
                                   "q.RO_CITY, " +
                                   "q.ACTIVE, " +
                                   "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                   "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                   "w.BE_ID, " +
                                   "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                   "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                            "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c " +
                            "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID=q.PROFIT_CENTER_ID_NEW AND q.BRANCH_ID=:c ORDER BY q.RO_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {

                        try
                        {
                            string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE," +
                                     "q.RO_NAME, " +
                                     "q.RO_CERTIFICATE_NUMBER, " +
                                     "q.RO_ADDRESS, " +
                                     "q.RO_POSTALCODE, " +
                                     "q.MEMO, " +
                                     "q.RO_CITY, " +
                                     "q.ACTIVE, " +
                                     "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                     "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                     "w.BE_ID, " +
                                     "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                     "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                                     "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c " +
                                     "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID=q.PROFIT_CENTER_ID_NEW " +
                                     "AND ((q.RO_NUMBER LIKE '%'||:c||'%') OR (q.RO_CODE LIKE '%'||:c||'%') OR (upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) LIKE '%'||:c||'%') " +
                                     "OR (UPPER(q.RO_NAME) LIKE '%'||:c||'%') " +
                                     "OR ((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%'||:c||'%') " +
                                     "OR (c.TERMINAL_NAME LIKE '%'||:c||'%')) " +
                                     "AND q.BRANCH_ID=:d ORDER BY q.RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesMasterRentalObject();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;

        }

        public DataTablesFixtureFitting2 GetDataFixtureFitting2(int draw, int start, int length, string search, string idRO)
        {
            int count = 0;
            int end = start + length;
            DataTablesFixtureFitting2 result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_FIXTUREFITTING2_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT f.ID, RO_NUMBER, FIXTURE_FITTING_CODE, REF_DESC AS DESCRIPTION, REF_DATA, " +
                    "POWER_CAPACITY, to_char(VALID_FROM,'DD.MM.YYYY') AS VALID_FROM, " +
                    " to_char(VALID_TO,'DD.MM.YYYY') AS VALID_TO, to_char(VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(VALID_TO, 'DD/MM/YYYY') AS VAL_TO, f.ACTIVE FROM RENTAL_OBJECT_FIXTURE_FITTING f, PROP_PARAMETER_REF_D d WHERE f.FIXTURE_FITTING_CODE=d.ID AND f.RO_NUMBER=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_FIXTUREFITTING2_Remote>(fullSql, new { a = start, b = end, c = idRO });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idRO });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesFixtureFitting2();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataTablesMeasurement GetDataMeasurement(int draw, int start, int length, string search, string idRO)
        {
            int count = 0;
            int end = start + length;
            DataTablesMeasurement result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_MEASUREMENT_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT f.ID, RO_NUMBER, MEASUREMENT_TYPE, REF_DESC AS DESCRIPTION, REF_DATA, AMOUNT, UNIT, to_char(VALID_FROM,'DD.MM.YYYY') AS VALID_FROM, to_char(VALID_TO,'DD.MM.YYYY') AS VALID_TO, to_char(VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(VALID_TO, 'DD/MM/YYYY') AS VAL_TO, f.ACTIVE FROM RENTAL_OBJECT_DETAIL f, PROP_PARAMETER_REF_D d WHERE f.MEASUREMENT_TYPE=d.ID AND f.RO_NUMBER=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_MEASUREMENT_Remote>(fullSql, new { a = start, b = end, c = idRO });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idRO });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesMeasurement();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }
        [AllowAnonymous]
        public DataTablesOccupancy GetDataOccupancy(int draw, int start, int length, string search, string idRO)
        {
            int count = 0;
            int end = start + length;
            DataTablesOccupancy result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_OCCUPANCY_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT * FROM V_OCCUPANCY WHERE RO_NUMBER=:c ORDER BY ID ASC";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_OCCUPANCY_Remote>(fullSql, new { a = start, b = end, c = idRO });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idRO });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesOccupancy();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }

        // Ubah Status Header
        public dynamic UbahStatus(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ACTIVE " +
                "FROM RENTAL_OBJECT " +
                "WHERE RO_CODE = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE RENTAL_OBJECT " +
                    "SET ACTIVE = :a " +
                    "WHERE RO_CODE = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic UbahStatusMeasurement(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ACTIVE " +
                "FROM RENTAL_OBJECT_DETAIL " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE RENTAL_OBJECT_DETAIL " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic UbahStatusFixtureFitting(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ACTIVE " +
                "FROM RENTAL_OBJECT_FIXTURE_FITTING " +
                "WHERE ID = :a";

            string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(kdAktif))
            {
                sql = "UPDATE RENTAL_OBJECT_FIXTURE_FITTING " +
                    "SET ACTIVE = :a " +
                    "WHERE ID = :b";

                if (kdAktif == "1")
                {
                    connection.Execute(sql, new
                    {
                        a = "0",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        a = "1",
                        b = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Something went wrong."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public DataReturnRONumber Add(string name, DataRentalObject data, string KodeCabang)
        {
            string qr = "";
            string id_ro = "";
            string ro_code = "";
            int RETURN_VALUE_BUFFER_SIZE = 32767;
            IDbConnection connectionOne = DatabaseFactory.GetConnection("default");
            if (connectionOne.State.Equals(ConnectionState.Closed))
                connectionOne.Open();

            //patch 100: by redyarman. remark : change how to get ro_code
            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                //OracleCommand cmd = connection.CreateCommand();
                //cmd.CommandText = "GEN_RO_CODE";
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.BindByName = true;
                //cmd.Parameters.Add("returnVal", OracleDbType.Varchar2, RETURN_VALUE_BUFFER_SIZE);
                //cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                //OracleParameter pBE_ID = new OracleParameter("P_BE_ID", OracleDbType.Varchar2,
                //   ParameterDirection.Input)
                //{
                //    Value = data.BE_ID,
                //    Size = 100
                //};
                //cmd.Parameters.Add(pBE_ID);

                var parameter = new DynamicParameters();
                parameter.Add(":P_BE_ID", data.BE_ID);
                parameter.Add(":returnVal", dbType: DbType.String, direction: ParameterDirection.ReturnValue, size: RETURN_VALUE_BUFFER_SIZE);


                try
                {
                    //cmd.ExecuteNonQuery();
                    //ro_code = cmd.Parameters["returnVal"].Value.ToString();

                    connection.Execute("GEN_RO_CODE", parameter, commandType: CommandType.StoredProcedure);
                    ro_code = parameter.Get<string>(":returnVal");
                }
                catch (Exception)
                {
                    
                    throw;
                }

                //ro_code = Convert.ToString(cmd.Parameters["Return_Value"].Value);
                string sql = "INSERT INTO RENTAL_OBJECT " +
                "(BE_ID, RO_TYPE_ID, RO_NAME, ZONE_RIP_ID, VALID_FROM, VALID_TO, RO_CERTIFICATE_NUMBER," +
                "FUNCTION_ID, USAGE_TYPE_ID, RO_ADDRESS, RO_POSTALCODE, RO_CITY, PROVINCE_ID, MEMO, CREATION_DATE, PROFIT_CENTER_ID, ACTIVE, RO_CODE, BRANCH_ID, KODE_ASET, PROFIT_CENTER_ID_NEW, LINI, PERUNTUKAN,SERTIFIKAT_OR_NOT,KETERANGAN) " +
                "VALUES (:a, :b, :c, :d, to_date(:e,'DD/MM/YYYY'), to_date(:f,'DD/MM/YYYY'), :g, :i, :j, :k, :l, :m, :n, :o, sysdate, :p, :q, :s, :t, :u, :p, :w, :z, :zq, :zw)";

               // ro_code = connection.ExecuteScalar<string>("SELECT GEN_RO_CODE AS RO_CODE FROM DUAL");
                int r = connection.Execute(sql, new
                {
                    a = data.BE_ID,
                    b = data.RO_TYPE_ID,
                    c = data.RO_NAME,
                    d = data.ZONE_RIP_ID,
                    e = data.VALID_FROM,
                    f = data.VALID_TO,
                    g = data.RO_CERTIFICATE_NUMBER,
                    //h = data.LOCATION_ID,
                    i = data.FUNCTION_ID,
                    j = data.USAGE_TYPE_ID,
                    k = data.RO_ADDRESS,
                    l = data.RO_POSTALCODE,
                    m = data.RO_CITY,
                    n = data.RO_PROVINCE,
                    o = data.MEMO,
                    p = data.PROFIT_CENTER,
                    q = 1,
                    s = ro_code,
                    t = KodeCabang,
                    u = data.KODE_ASET,
                    w = data.LINI,
                    /* w = '1',*/
                    z = data.PERUNTUKAN,
                    zq = data.SERTIFIKAT,
                    zw = data.KETERANGAN
                    /*z = '1',*/

                });
            }
            //Query last id dan return value nya untuk step selanjutnya
            qr = "SELECT RO_NUMBER FROM RENTAL_OBJECT WHERE BRANCH_ID=:a AND RO_NUMBER=(SELECT MAX(RO_NUMBER) FROM RENTAL_OBJECT)";
            id_ro = connectionOne.ExecuteScalar<string>(qr, new { a = KodeCabang });

            
            var resultfull = new DataReturnRONumber();
            resultfull.RO_NUMBER = ro_code;
            resultfull.RO_ID = id_ro;
            resultfull.RESULT_STAT = (ro_code.Length > 0) ? true : false;

            connectionOne.Close();
            return resultfull;
        }

        // List DropDown Business Entity
        public IEnumerable<DataDDBusinessEntity> GetDataBusinessEntity()
        {
            string sql = string.Empty;
            object param = null;

                sql = "SELECT * FROM BUSINESS_ENTITY";
            
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDBusinessEntity> listDetil = connection.Query<DataDDBusinessEntity>(sql, param);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public bool AddDataFixtureFitting(string name, DataFixtureFitting data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "INSERT INTO RENTAL_OBJECT_FIXTURE_FITTING " +
                "(FIXTURE_FITTING_CODE, VALID_FROM, VALID_TO, POWER_CAPACITY, RO_NUMBER, ACTIVE) " +
                "VALUES (:a, to_date(:b,'DD/MM/YYYY'), to_date(:c,'DD/MM/YYYY'), :d, :e, :f)";

            int r = connection.Execute(sql, new
            {
                a = data.FIXTURE_FITTING_CODE,
                b = data.VALID_FROM,
                c = data.VALID_TO,
                d = data.POWER_CAPACITY,
                e = data.RO_NUMBER,
                f = "1"
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------ ADD DATA MEASUREMENT -------------
        public bool AddDataMeasurement(string name, DataMeasurement data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "INSERT INTO RENTAL_OBJECT_DETAIL " +
                "(RO_NUMBER, MEASUREMENT_TYPE, AMOUNT, UNIT, VALID_FROM, VALID_TO, ACTIVE) " +
                "VALUES (:a, :b, :c, :d, to_date(:e,'DD/MM/YYYY'), to_date(:f,'DD/MM/YYYY'), :g)";

            int r = connection.Execute(sql, new
            {
                a = data.RO_NUMBER,
                b = data.MEASUREMENT_TYPE,
                c = data.AMOUNT,
                d = data.UNIT,
                e = data.VALID_FROM,
                f = data.VALID_TO,
                g = "1"
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        //----------------------- ADD DATA OCCUPANCY -----------------
        public bool AddDataOccupancy(string name, DataOccupancy data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string sql = "INSERT INTO PROP_RO_OCCUPANCY " +
                "(RO_NUMBER, STATUS, REASON, VALID_FROM, VALID_TO, ACTIVE_STATUS) " +
                "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f)";

            int r = connection.Execute(sql, new
            {
                a = data.RO_NUMBER,
                b = "VACANT",
                c = data.VACANCY_REASON,
                d = data.VALID_FROM,
                e = data.VALID_TO,
                f = "1"
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool Edit(string name, DataRentalObject data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE RENTAL_OBJECT SET " +
                         "BE_ID=:a, RO_NAME=:b, RO_CERTIFICATE_NUMBER=:c, RO_ADDRESS=:d, RO_POSTALCODE=:e, RO_CITY=:f, RO_PROVINCE=:g, MEMO=:i, LAST_UPDATE_DATE=sysdate, " +
                         "VALID_FROM=to_date(:j,'DD/MM/YYYY'), VALID_TO=to_date(:k,'DD/MM/YYYY'), PROFIT_CENTER_ID=:l, RO_TYPE_ID=:m, ZONE_RIP_ID=:n, FUNCTION_ID=:p, USAGE_TYPE_ID=:q, LONGITUDE=:r, LATITUDE=:s, KODE_ASET=:t, PROFIT_CENTER_ID_NEW =:l" +
                         "WHERE RO_NUMBER=:h";

            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.RO_NAME,
                c = data.RO_CERTIFICATE_NUMBER,
                d = data.RO_ADDRESS,
                e = data.RO_POSTALCODE,
                f = data.RO_CITY,
                g = data.RO_PROVINCE,
                h = data.RO_NUMBER,
                i = data.MEMO,
                j = data.VALID_FROM,
                k = data.VALID_TO,
                l = data.PROFIT_CENTER,
                m = data.RO_TYPE_ID,
                n = data.ZONE_RIP_ID,
                //o = data.LOCATION_ID,
                p = data.FUNCTION_ID,
                q = data.USAGE_TYPE_ID,
                r = data.LONGITUDE,
                s = data.LATITUDE,
                t = data.KODE_ASET

            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool EditNew(string name, DataRentalObject data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = @"UPDATE RENTAL_OBJECT A SET 
                         BE_ID=:a, RO_NAME=:b, RO_CERTIFICATE_NUMBER=:c, RO_ADDRESS=:d, RO_POSTALCODE=:e, RO_CITY=:f, RO_PROVINCE=:g, MEMO=:i, LAST_UPDATE_DATE=sysdate, 
                         VALID_FROM=to_date(:j,'DD/MM/YYYY'), VALID_TO=to_date(:k,'DD/MM/YYYY'), PROFIT_CENTER_ID=:l, RO_TYPE_ID=:m, ZONE_RIP_ID=:n, FUNCTION_ID=:p, USAGE_TYPE_ID=:q, LONGITUDE=:r, LATITUDE=:s, KODE_ASET=:t, PROFIT_CENTER_ID_NEW =:l, LINI =:v, PERUNTUKAN =:w,
                          SERTIFIKAT_ID =:x, KETERANGAN =:y, SERTIFIKAT_OR_NOT =:z WHERE RO_NUMBER=:u";

            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.RO_NAME,
                c = data.RO_CERTIFICATE_NUMBER,
                d = data.RO_ADDRESS,
                e = data.RO_POSTALCODE,
                f = data.RO_CITY,
                g = data.RO_PROVINCE,
                /*h = data.RO_NUMBER,*/
                i = data.MEMO,
                j = data.VALID_FROM,
                k = data.VALID_TO,
                l = data.PROFIT_CENTER,
                m = data.RO_TYPE_ID,
                n = data.ZONE_RIP_ID,
                //o = data.LOCATION_ID,
                p = data.FUNCTION_ID,
                q = data.USAGE_TYPE_ID,
                r = data.LONGITUDE,
                s = data.LATITUDE,
                t = data.KODE_ASET,
                u = data.RO_NUMBER,
                v = data.LINI,
                w = data.PERUNTUKAN,
                x = data.SERTIFIKAT_ID,
                y = data.KETERANGAN,
                z = data.SERTIFIKAT
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------Edit Data Occupancy
        public bool EditOccupancy(string name, DataOccupancy data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "UPDATE PROP_RO_OCCUPANCY SET " +
                     "REASON=:d WHERE RO_NUMBER=:c";

                int r = connection.Execute(sql, new
                {
                    c = data.RO_NUMBER,
                    d = data.VACANCY_REASON
                });
                result = (r > 0) ? true : false;
            }
            catch (Exception)
            {

            }

            return result;
        }

        //------------Edit Data Measurement
        public bool EditMeasurement(string name, DataMeasurement data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (data.MEASUREMENT_TYPE == null)
            {
                try
                {
                    string sql = "UPDATE RENTAL_OBJECT_DETAIL SET " +
                         "AMOUNT=:b, UNIT=:c, VALID_FROM=to_date(:d,'DD/MM/YYYY'), VALID_TO=to_date(:e,'DD/MM/YYYY') " +
                         "WHERE ID=:f";

                    int r = connection.Execute(sql, new
                    {
                        b = data.AMOUNT,
                        c = data.UNIT,
                        d = data.VALID_FROM,
                        e = data.VALID_TO,
                        f = data.ID
                    });
                    result = (r > 0) ? true : false;
                }
                catch (Exception)
                {

                }

            }
            else
            {
                try
                {
                    string sql = "UPDATE RENTAL_OBJECT_DETAIL SET " +
                         "MEASUREMENT_TYPE=:a, AMOUNT=:b, UNIT=:c, VALID_FROM=to_date(:d,'DD/MM/YYYY'), VALID_TO=to_date(:e, 'DD/MM/YYYY') " +
                         "WHERE ID=:f";

                    int r = connection.Execute(sql, new
                    {
                        a = data.MEASUREMENT_TYPE,
                        b = data.AMOUNT,
                        c = data.UNIT,
                        d = data.VALID_FROM,
                        e = data.VALID_TO,
                        f = data.ID
                    });
                    result = (r > 0) ? true : false;
                }
                catch (Exception) 
                {

                }
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------Edit Data Fixture Fitting
        public bool EditFixtureFitting(string name, DataFixtureFitting data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            if (data.FIXTURE_FITTING_CODE == null)
            {
                try
                {
                    string sql = "UPDATE RENTAL_OBJECT_FIXTURE_FITTING SET " +
                         "POWER_CAPACITY=:b, VALID_FROM=to_date(:c,'DD/MM/YYYY'), VALID_TO=to_date(:d, 'DD/MM/YYYY') " +
                         "WHERE ID=:e";

                    int r = connection.Execute(sql, new
                    {
                        b = data.POWER_CAPACITY,
                        c = data.VALID_FROM,
                        d = data.VALID_TO,
                        e = data.ID
                    });

                    result = (r > 0) ? true : false;
                }
                catch (Exception)
                {

                }
            }
            else
            {
                try
                {
                    string sql = "UPDATE RENTAL_OBJECT_FIXTURE_FITTING SET " +
                         "FIXTURE_FITTING_CODE=:a, POWER_CAPACITY=:b, VALID_FROM=to_date(:c,'DD/MM/YYYY'), VALID_TO=to_date(:d, 'DD/MM/YYYY') " +
                         "WHERE ID=:e";

                    int r = connection.Execute(sql, new
                    {
                        a = data.FIXTURE_FITTING_CODE,
                        b = data.POWER_CAPACITY,
                        c = data.VALID_FROM,
                        d = data.VALID_TO,
                        e = data.ID
                    });

                    result = (r > 0) ? true : false;
                }
                catch (Exception)
                {

                }
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public List<DATA_LINI> GetLini()
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string sql = "SELECT LINI FROM RENTAL_OBJECT WHERE LINI IS NOT NULL";

            List<DATA_LINI> result = connection.Query<DATA_LINI>(sql).ToList();

            connection.Close();
            connection.Dispose();
            return result;
        }
        public AUP_RENTALOBJECT_Remote GetDataForEdit(int id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            /* SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.BRANCH_ID," +
                                    "q.RO_NAME, " +
                                    "q.RO_CERTIFICATE_NUMBER, " +
                                    "q.RO_ADDRESS, " +
                                    "q.RO_POSTALCODE, " +
                                    "q.MEMO, " +
                                    "q.RO_CITY, " +
                                    "q.ACTIVE, " +
                                    "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                    "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                    "q.LINI," +
                                    "q.PERUNTUKAN," +
                                    "w.BE_ID, " +
                                    "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                    "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                    *//* "(SELECT PERUNTUKAN_NAME FROM PERUNTUKAN WHERE q.PERUNTUKAN = ID) PERUNTUKAN, " +*//*
                                    "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                                    "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, V_BE a " +
                                    "WHERE RO_NUMBER = :a.BE_ID AND q.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) AND c.PROFIT_CENTER_ID=q.PROFIT_CENTER_ID_NEW " + wheresearch + " ORDER BY q.RO_NUMBER DESC*/

            string sql = "SELECT RO_NUMBER, RO_CODE, BE_ID, RO_NAME, RO_CERTIFICATE_NUMBER, RO_ADDRESS, RO_POSTALCODE, " +
                          "RO_CITY, RO_PROVINCE, MEMO, to_char(VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                          "ZONE_RIP_ID, RO_TYPE_ID, USAGE_TYPE_ID, PROVINCE_ID, PROFIT_CENTER_ID, FUNCTION_ID, KODE_ASET, LINI, PERUNTUKAN, SERTIFIKAT_OR_NOT, KETERANGAN FROM RENTAL_OBJECT " +
                         "WHERE RO_NUMBER = :a";

            AUP_RENTALOBJECT_Remote result = connection.Query<AUP_RENTALOBJECT_Remote>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteData(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT RO_NUMBER " +
                "FROM RENTAL_OBJECT " +
                "WHERE RO_NUMBER = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id});

            if (!string.IsNullOrEmpty(recStat))
            {
                /*
                sql = "DELETE FROM RENTAL_OBJECT " +
                    "WHERE RO_NUMBER = :a";
                */
                sql = "UPDATE RENTAL_OBJECT SET " +
                         "ACTIVE=:b " +
                         "WHERE RO_NUMBER=:a";
 
                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {
                       
                        a = id,
                        b = 0
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {
                        
                        a = id,
                        b = 0
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataFixtureFitting(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM RENTAL_OBJECT_FIXTURE_FITTING " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM RENTAL_OBJECT_FIXTURE_FITTING " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataMeasurement(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM RENTAL_OBJECT_DETAIL " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM RENTAL_OBJECT_DETAIL " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DataDDBusinessEntity> GetDataDDBO()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDBusinessEntity> listData = null;

            try
            {
                string sql = "SELECT BE_ID, BE_NAME " +
                              "FROM BUSINESS_ENTITY ";

                listData = connection.Query<DataDDBusinessEntity>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DataDDZONA_RIP> GetDataDDZONA_RIP()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDZONA_RIP> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC " +
                              "FROM PROP_PARAMETER_REF_D WHERE REF_CODE='ZONA_RIP' AND ACTIVE='1' ";

                listData = connection.Query<DataDDZONA_RIP>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }
 
        public IEnumerable<DataDDRO_TYPE> GetDataRO_TYPE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDRO_TYPE> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RO_TYPE' AND ACTIVE='1' ";

                listData = connection.Query<DataDDRO_TYPE>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DataDDUSAGE_TYPE> GetDataUSAGE_TYPE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDUSAGE_TYPE> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC " +
                              "FROM PROP_PARAMETER_REF_D WHERE REF_CODE='USAGE_TYPE_RO' AND ACTIVE='1' ";

                listData = connection.Query<DataDDUSAGE_TYPE>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DataDDProvince> GetDataPROVINCE()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataDDProvince> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC " +
                              "FROM PROP_PARAMETER_REF_D WHERE REF_CODE='PROVINCE' AND ACTIVE='1' ";

                listData = connection.Query<DataDDProvince>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<getGenerateRONumber> GenerateRoNumber()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<getGenerateRONumber> listData = null;

            try
            {
                string sql = "SELECT GEN_RO_NUMBER as RO_NUMBER FROM DUAL";

                listData = connection.Query<getGenerateRONumber>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }


        //----------------GET DROPDOWN HERE

        //-------------------------- LIST DROP DOWN MEASUREMENT ----------------
        public IEnumerable<DDMeasurement> GetDataMeasurement()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDMeasurement> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'MEASUREMENT_TYPE' AND ACTIVE='1' ";

                listData = connection.Query<DDMeasurement>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            connection.Dispose();
            return listData;
        }

        public IEnumerable<DDBusinessEntity> GetDataBE2(string KodeCabang)
        {
            string sql = "SELECT BE_ID, BE_ID||' - ' ||BE_NAME AS BE_NAME FROM BUSINESS_ENTITY WHERE BRANCH_ID=:a";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DDBusinessEntity> listDetil = connection.Query<DDBusinessEntity>(sql, new { a=KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        //------------------- DROP DOWN YANG DIPAKAI ------------------
        public IEnumerable<DDRoType> GetDataRoType()
        {
            string sql = "SELECT ID, ID AS RO_TYPE_ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RO_TYPE' AND ACTIVE='1'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRoType> listDetil = connection.Query<DDRoType>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDZoneRip> GetDataZoneRip()
        {
            string sql = "SELECT ID, ID AS ZONE_RIP_ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='ZONA_RIP' AND ACTIVE='1'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDZoneRip> listDetil = connection.Query<DDZoneRip>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDRoFunction> GetDataRoFunction()
        {
            string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RO_FUNCTION'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRoFunction> listDetil = connection.Query<DDRoFunction>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDUsageTypeRo> GetDataUsageTypeRo()
        {
            string sql = "SELECT ID, ID AS USAGE_TYPE_ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='USAGE_TYPE_RO' AND ACTIVE='1' ORDER BY REF_DATA ASC";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDUsageTypeRo> listDetil = connection.Query<DDUsageTypeRo>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDLini> GetDataLini()
        {
            string sql = "SELECT LINI FROM RENTAL_OBJECT WHERE LINI IS NOT NULL ORDER BY LINI ASC";
            //if (sql=="0")
            //{
                
            //}
            //else
            //{

            //}
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDLini> listDetil = connection.Query<DDLini>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDPeruntukan> GetDataPeruntukan()
        {
            string sql = "SELECT* FROM PERUNTUKAN ORDER BY ID ASC";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDPeruntukan> listDetil = connection.Query<DDPeruntukan>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }
        public IEnumerable<DDProvince> GetDataProvince()
        {
            string sql = "SELECT ID, ID AS PROVINCE_ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='PROVINCE'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProvince> listDetil = connection.Query<DDProvince>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDRoLocation> GetDataRoLocation()
        {
            string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE='RO_LOCATION'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRoLocation> listDetil = connection.Query<DDRoLocation>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string KodeCabang)
        {
            string sql = "";

            if (KodeCabang == "0"){
                
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else if (KodeCabang == "2")
            {
                
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER " +
                    "WHERE (REGIONAL_ID = 100 OR BRANCH_ID=:a) and STATUS = 1";

            } else if (KodeCabang == "9")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER " +
                    "WHERE (REGIONAL_ID = 110 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else if (KodeCabang == "12")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER " +
                    "WHERE (REGIONAL_ID = 130 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else if (KodeCabang == "24")
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER " +
                    "WHERE (REGIONAL_ID = 120 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a and STATUS = 1";
            }

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new { a = KodeCabang });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDMeasurementType> GetDataMeasurementType()
        {
            string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'MEASUREMENT_TYPE' AND REF_DATA!='Z001'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDMeasurementType> listDetil = connection.Query<DDMeasurementType>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDVacancyReason> GetDataVacancyReason()
        {
            string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'VACANCY_REASON'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDVacancyReason> listDetil = connection.Query<DDVacancyReason>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }

        public IEnumerable<DDFixFit> GetDataFF()
        {
            string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'FIXTURE_AND_FITTING'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDFixFit> listDetil = connection.Query<DDFixFit>(sql);
            connection.Close();
            connection.Dispose();
            return listDetil;
        }
        public IEnumerable<FDDProfitCenter> GetProfitCenter(string be_id)
        {
            string sql = "";
            if (be_id == "0")
            {

                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER where STATUS = 1";
            }
            else if (be_id == "2")
            {

                sql = @"SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER
                        WHERE (REGIONAL_ID = 100 OR BRANCH_ID=:a) and STATUS = 1";

            }
            else if (be_id == "9")
            {
                sql = @"SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER 
                    WHERE (REGIONAL_ID = 110 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else if (be_id == "12")
            {
                sql = @"SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER
                    WHERE (REGIONAL_ID = 130 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else if (be_id == "24")
            {
                sql = @"SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER
                    WHERE (REGIONAL_ID = 120 OR BRANCH_ID=:a) and STATUS = 1";
            }
            else
            {
                sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID=:a and STATUS = 1";
            }

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<FDDProfitCenter> listDetil = connection.Query<FDDProfitCenter>(sql, new {
                a = be_id
            });
            connection.Close();
            connection.Dispose();
            return listDetil;
        }
        public List<AUP_RENTALOBJECT_Remote> getDataPrintQr(string id = "", string kodeCabang = "", string kodeProfit = "")
        {
            List<AUP_RENTALOBJECT_Remote> listDetil = null;
            string where = "";
            //string param = "";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            if (id != "")
            {
                where = " and q.RO_CODE = " + id;
            }
            else
            {
                if(kodeProfit != "" && kodeProfit != "0")
                {
                    where = " and c.PROFIT_CENTER_ID =" + kodeProfit;
                }
                else if (kodeCabang != "")
                {
                    where = " and c.BRANCH_ID = " + kodeCabang;
                }
            }
            string sql = @"SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, q.BRANCH_ID, q.RO_NAME, q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, 
                    q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, TO_CHAR(q.VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR(q.VALID_TO, 'DD.MM.RRRR') VALID_TO, 
                    w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME,
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, 
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, 
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, 
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, 
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE CAST (REGEXP_REPLACE(q.FUNCTION_ID, '[^0-9]+', '') AS NUMBER) = ID) FUNCTION_ID, 
                    (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, 
                    q.PROFIT_CENTER_ID || ' - ' || c.TERMINAL_NAME PROFIT_CENTER
                    FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c
                    WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW " + where + " ORDER BY q.RO_NUMBER DESC";
            listDetil = connection.Query<AUP_RENTALOBJECT_Remote>(sql, new{}).ToList();

            sql = @"select ID,RO_NUMBER, VALID_FROM, VALID_TO, STATUS from PROP_RO_OCCUPANCY
                    where VALID_FROM <=current_date AND VALID_TO >= current_date
                    order by ID desc";
            var ocp = connection.Query<dynamic>(sql).ToList();
            dynamic ocpData;
            foreach(var temp in listDetil)
            {
                var asd = ocp.Where(a => a.RO_NUMBER.ToString() == temp.RO_NUMBER).Max(a => a.ID);
                ocpData = ocp.Where(a => a.ID == asd).DefaultIfEmpty(null).FirstOrDefault();
                temp.OCP_VALID_FROM = ocpData == null ? "No Data" : ocpData.VALID_FROM.ToString("dd-MM-yyyy");
                temp.OCP_VALID_TO = ocpData == null ? "No Data" : ocpData.VALID_TO.ToString("dd-MM-yyyy");
                temp.ACTIVE_STATUS = ocpData == null ? "No Data" : ocpData.STATUS;
            }


            connection.Close();
            connection.Dispose();
            return listDetil;
        }
        public DataTablesMasterRentalObject GetDataRentalObjectfilter(int draw, int start, int length, string search, string KodeCabang, int be, int pc, string occupancy)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterRentalObject result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_RENTALOBJECT_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string whereoccupancy = string.Empty;
            //if ( ! string.IsNullOrEmpty(occupancy))
            //{
            //    whereoccupancy = " and b.STATUS = '" + occupancy + "' ";
            //}

            if (KodeCabang == "0")
            {
                string where = (pc != 0 ? " and q.PROFIT_CENTER_ID = " + pc : (be != 0 ? " and q.BRANCH_ID = " + be : ""));
                where += whereoccupancy;
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, q.BRANCH_ID, q.RO_NAME, q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, " +
                                 "q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, TO_CHAR (q.VALID_FROM, 'DD.MM.RRRR') VALID_FROM, TO_CHAR (q.VALID_TO, 'DD.MM.RRRR') VALID_TO, " +
                                 "b.STATUS OCCUPANCY, b.CONTRACT_NO, b.CONTRACT_OFFER_NO, " +
                                 "w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE CAST (REGEXP_REPLACE (q.FUNCTION_ID, '[^0-9]+', '') AS NUMBER) = ID) FUNCTION_ID, " +
                                 "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                 "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER " +
                                 "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, PROP_RO_OCCUPANCY b " +
                                 "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW AND b.RO_NUMBER = q.RO_NUMBER AND CURRENT_DATE >= b.VALID_FROM AND CURRENT_DATE <= b.VALID_TO " + where +
                                 "ORDER BY q.RO_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new { a = start, b = end });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = 2 });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {

                        try
                        {
                            string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, q.RO_NAME, q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, " +
                                     "q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, " +
                                     "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                     "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                     "b.STATUS OCCUPANCY, b.CONTRACT_NO, b.CONTRACT_OFFER_NO, " +
                                     "w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                     "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER " +
                                     "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, PROP_RO_OCCUPANCY b " +
                                     "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW AND b.RO_NUMBER = q.RO_NUMBER AND CURRENT_DATE >= b.VALID_FROM AND CURRENT_DATE <= b.VALID_TO " + where +
                                     "AND ((q.RO_NUMBER LIKE '%'||:c||'%') OR (q.RO_CODE LIKE '%'||:c||'%') OR (upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) LIKE '%'||:c||'%') " +
                                     "OR (UPPER(q.RO_NAME) LIKE '%'||:c||'%') " +
                                     "OR ((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%'||:c||'%') " +
                                     "OR (c.TERMINAL_NAME LIKE '%'||:c||'%')) " +
                                     "ORDER BY q.RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }

            }

            else {
                string where = (pc != 0 ? " and q.PROFIT_CENTER_ID_NEW = " + pc : "");
                where += whereoccupancy;
                if (search.Length == 0)
                {
                    try
                    {
                        
                        string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, q.BRANCH_ID," +
                                   "q.RO_NAME, " +
                                   "q.RO_CERTIFICATE_NUMBER, " +
                                   "q.RO_ADDRESS, " +
                                   "q.RO_POSTALCODE, " +
                                   "q.MEMO, " +
                                   "q.RO_CITY, " +
                                   "q.ACTIVE, " +
                                   "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                   "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                   //"b.STATUS OCCUPANCY, b.CONTRACT_NO, b.CONTRACT_OFFER_NO, " +
                                   "b.STATUS OCCUPANCY, b.CONTRACT_NO, " +
                                   "w.BE_ID, " +
                                   "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                   "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER " +
                                   "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, PROP_RO_OCCUPANCY b " +
                                   "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW AND b.RO_NUMBER = q.RO_NUMBER AND CURRENT_DATE >= b.VALID_FROM AND CURRENT_DATE <= b.VALID_TO " + where + " AND q.BRANCH_ID=:c ORDER BY q.RO_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);    

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                    }
                    catch (Exception ex)
                    {
                        string aaa = ex.ToString();
                    }
                }
                else
                {
                    //search filter data
                    if (search.Length > 2)
                    {

                        try
                        {
                            string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, " +
                                     "q.RO_NAME, " +
                                     "q.RO_CERTIFICATE_NUMBER, " +
                                     "q.RO_ADDRESS, " +
                                     "q.RO_POSTALCODE, " +
                                     "q.MEMO, " +
                                     "q.RO_CITY, " +
                                     "q.ACTIVE, " +
                                     "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                     "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                     "b.STATUS OCCUPANCY, b.CONTRACT_NO, b.CONTRACT_OFFER_NO, " +
                                     "w.BE_ID, " +
                                     "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                     "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                     "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER " +
                                     "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, PROP_RO_OCCUPANCY b " +
                                     "WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW AND b.RO_NUMBER = q.RO_NUMBER AND CURRENT_DATE >= b.VALID_FROM AND CURRENT_DATE <= b.VALID_TO " +
                                     "AND ((q.RO_NUMBER LIKE '%'||:c||'%') OR (q.RO_CODE LIKE '%'||:c||'%') OR (upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) LIKE '%'||:c||'%') " +
                                     "OR (UPPER(q.RO_NAME) LIKE '%'||:c||'%') " +
                                     "OR ((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%'||:c||'%') " + where +
                                     "OR (c.TERMINAL_NAME LIKE '%'||:c||'%')) " +
                                     " AND q.BRANCH_ID=:d ORDER BY q.RO_NUMBER DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper(),
                                d = KodeCabang
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            result = new DataTablesMasterRentalObject();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;

        }
        public dynamic PinPoint(string RO_NUMBER)
        {
            dynamic result = null;
            DataMaps coordinate = new DataMaps();
            //IDbConnection connection = DatabaseFactory.GetConnection("default");
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "select B.LATITUDE, B.LONGITUDE, A.BE_ID, B.RO_NUMBER from RENTAL_OBJECT  A LEFT JOIN RENTAL_OBJECT_MAPS B ON B.RO_NUMBER= A.RO_NUMBER where A.RO_NUMBER =:a";
                    coordinate = connection.Query<DataMaps>(sql, new { a = RO_NUMBER }).DefaultIfEmpty().FirstOrDefault();

                    if (coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                    {
                        sql = "select LATITUDE, LONGITUDE from BUSINESS_ENTITY where BE_ID =:a";
                        coordinate = connection.Query<DataMaps>(sql, new { a = coordinate.BE_ID }).DefaultIfEmpty().FirstOrDefault();

                        if(coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                        {
                            coordinate = connection.Query<DataMaps>(sql, new { a = 1011 }).DefaultIfEmpty().FirstOrDefault();
                        }
                        
                    }

                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception)
                {
                    throw;
                }
            }

            //connection.Close();
            return result;
        }

        public dynamic PinPoint2(string RO_CODE)
        {
            dynamic result = null;
            DataMaps coordinate = new DataMaps();
            //IDbConnection connection = DatabaseFactory.GetConnection("default");
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {

                    string sql = "select B.LATITUDE, B.LONGITUDE, A.BE_ID, B.RO_NUMBER from RENTAL_OBJECT  A LEFT JOIN RENTAL_OBJECT_MAPS B ON B.RO_NUMBER= A.RO_NUMBER where A.RO_CODE =:a";
                    coordinate = connection.Query<DataMaps>(sql, new { a = RO_CODE }).DefaultIfEmpty().FirstOrDefault();

                    if (coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                    {
                        sql = "select LATITUDE, LONGITUDE from BUSINESS_ENTITY where BE_ID =:a";
                        coordinate = connection.Query<DataMaps>(sql, new { a = coordinate.BE_ID }).DefaultIfEmpty().FirstOrDefault();

                        if (coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                        {
                            coordinate = connection.Query<DataMaps>(sql, new { a = 1011 }).DefaultIfEmpty().FirstOrDefault();
                        }

                    }

                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception)
                {
                    throw;
                }
            }

            //connection.Close();
            return result;
        }

        public dynamic GetMarker(string RO_NUMBER)
        {
            dynamic result = null;
            List<DataMaps> coordinate = new List<DataMaps>();

            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "select A.LATITUDE, A.LONGITUDE, A.RO_NUMBER from RENTAL_OBJECT_MAPS A where A.RO_NUMBER =:a order by NO_URUT ASC";
                    coordinate = connection.Query<DataMaps>(sql, new { a = RO_NUMBER }).ToList();
                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception)
                {
                    throw;
                }
            }

            //connection.Close();
            return result;
        }

        public dynamic GetMarker2(string RO_CODE)
        {
            dynamic result = null;
            List<DataMaps> coordinate = new List<DataMaps>();

            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = @"SELECT A.LATITUDE, A.LONGITUDE, A.RO_NUMBER FROM RENTAL_OBJECT_MAPS A 
                                JOIN RENTAL_OBJECT B ON B.RO_NUMBER = A.RO_NUMBER WHERE B.RO_CODE =:a ORDER BY NO_URUT ASC";
                    coordinate = connection.Query<DataMaps>(sql, new { a = RO_CODE }).ToList();
                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception)
                {
                    throw;
                }
            }

            //connection.Close();
            return result;
        }
        public dynamic GetROStatus(string RO_CODE)
        {
            dynamic result = null;
            string type = null;
            AUP_REPORT_RO ro = new AUP_REPORT_RO();
            AUP_TRANS_CONTRACT_WEBAPPS temp = new AUP_TRANS_CONTRACT_WEBAPPS();
            //IDbConnection connection = DatabaseFactory.GetConnection("default");
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();

            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = @"select CONTRACT_NO,CONTRACT_OFFER_NO, STATUS from V_REPORT_RO_NEW a
                                    where a.BRANCH_ID is not null and a.RO_CODE = :a";
                    ro = connection.Query<AUP_REPORT_RO>(sql, new { a = RO_CODE }).DefaultIfEmpty().FirstOrDefault();
                    type = ro.STATUS;
                    if (ro.CONTRACT_NO != null && ro.CONTRACT_NO != "")
                    {
                        //cari data contract
                        sql = @"select a.CONTRACT_NO, a.CONTRACT_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, BUSINESS_PARTNER,BUSINESS_PARTNER_NAME, a.CUSTOMER_AR  from PROP_CONTRACT a where a.CONTRACT_NO = :a";
                        temp = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new { a = ro.CONTRACT_NO }).DefaultIfEmpty().FirstOrDefault();
                        
                    }
                    else if(ro.CONTRACT_OFFER_NO != null && ro.CONTRACT_OFFER_NO != "")
                    {
                        //cari data contract offer
                        sql = @"select a.CONTRACT_OFFER_NO CONTRACT_NO, a.CONTRACT_OFFER_NAME CONTRACT_NAME, a.CONTRACT_START_DATE, a.CONTRACT_END_DATE, b.CUSTOMER_ID BUSINESS_PARTNER, b.CUSTOMER_NAME BUSINESS_PARTNER_NAME, b.CUSTOMER_AR from PROP_CONTRACT_OFFER a
join PROP_RENTAL_REQUEST b on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO where a.CONTRACT_OFFER_NO = :a";
                        temp = connection.Query<AUP_TRANS_CONTRACT_WEBAPPS>(sql, new { a = ro.CONTRACT_OFFER_NO }).DefaultIfEmpty().FirstOrDefault();
                    }
                    else
                    {
                        temp = null;
                    }
                    result = new
                    {
                        status = "S",
                        type = type,
                        message = temp
                    };

                }
                catch (Exception)
                {
                    throw;
                }
            }

            //connection.Close();
            return result;
        }


        public DataTablesMasterRentalObject GetDataRentalObjectfilternew(int draw, int start, int length, string search, string KodeCabang, int be, int pc, string occupancy)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterRentalObject result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_RENTALOBJECT_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string whereoccupancy = string.Empty;
            QueryGlobal query = new QueryGlobal();
            string wheresql = "";
            string wheresearch = search.Length >= 2 ? " AND (c.TERMINAL_NAME || q.RO_NUMBER || q.RO_CODE || upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) || UPPER(q.RO_NAME) || (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%' || '" + search.ToUpper() + "'|| '%') " : "";
            
            string where = (pc != 0 ? " and q.PROFIT_CENTER_ID_NEW = " + pc : "");
            if (be == 0 || be == 2 || be == 9 || be == 12 || be == 24)
            {
                where += "";
            }
            else
            {
                wheresql = query.Query_BE("q");
                where += " and w.branch_id = " + be;
            }

            where += wheresql;
            where += wheresearch;
            where += whereoccupancy;

            try
            {
                string sql = @"SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.KODE_ASET, q.RO_NAME, 
                            q.RO_CERTIFICATE_NUMBER, q.RO_ADDRESS, q.RO_POSTALCODE, q.MEMO, q.RO_CITY, q.ACTIVE, 
                            to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, 
                            to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, 
                            b.STATUS OCCUPANCY, b.CONTRACT_NO, b.CONTRACT_OFFER_NO, 
                            w.BE_ID, w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, 
                            (SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, 
                            q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER 
                            FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, PROP_RO_OCCUPANCY b 
                            WHERE w.BE_ID = q.BE_ID AND c.PROFIT_CENTER_ID = q.PROFIT_CENTER_ID_NEW AND b.RO_NUMBER = q.RO_NUMBER AND CURRENT_DATE >= b.VALID_FROM AND CURRENT_DATE <= b.VALID_TO  
                            " + where + " ORDER BY q.RO_NUMBER DESC ";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    a = start,
                    b = end,
                    d = KodeCabang
                });
            }
            catch (Exception)
            {

            }
            result = new DataTablesMasterRentalObject();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;

        }


        //-------------------------------- GET DATA NEW -------------------------------
        public DataTablesMasterRentalObject GetDataRentalObjectnew(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesMasterRentalObject result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_RENTALOBJECT_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            string wheresearch = (search.Length >= 2 ? " AND ((q.RO_NUMBER LIKE '%'||'" + search.ToUpper() + "'||'%') OR (q.RO_CODE LIKE '%'||'" + search.ToUpper() + "'||'%') OR (upper(w.BE_ID) || ' - ' || UPPER(w.BE_NAME) LIKE '%'||'" + search.ToUpper() + "'||'%') OR (UPPER(q.RO_NAME) LIKE '%'||'" + search.ToUpper() + "'||'%') OR ((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) LIKE '%'||'" + search.ToUpper() + "'||'%') OR (c.TERMINAL_NAME LIKE '%'||'" + search.ToUpper() + "'||'%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
 
                    try
                    {
                        string sql = "SELECT DISTINCT q.RO_NUMBER, q.RO_CODE, q.BRANCH_ID," +
                                   "q.RO_NAME, " +
                                   "q.RO_CERTIFICATE_NUMBER, " +
                                   "q.RO_ADDRESS, " +
                                   "q.RO_POSTALCODE, " +
                                   "q.MEMO, " +
                                   "q.RO_CITY, " +
                                   "q.ACTIVE, " +
                                   "to_char(q.VALID_FROM,'DD.MM.YYYY') VALID_FROM, " +
                                   "to_char(q.VALID_TO, 'DD.MM.YYYY') VALID_TO, " +
                                   "q.LINI,"+
                                   "q.PERUNTUKAN,"+
                                   "q.SERTIFIKAT_OR_NOT," +
                                   "q.KETERANGAN," +
                                   "w.BE_ID, " +
                                   "w.BE_ID || ' - ' || w.BE_NAME AS BE_NAME, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.USAGE_TYPE_ID = ID) USAGE_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.PROVINCE_ID = ID) PROVINCE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.ZONE_RIP_ID = ID) ZONE_RIP, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.RO_TYPE_ID = ID) RO_TYPE, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE cast(regexp_replace(q.FUNCTION_ID, '[^0-9]+', '') as number) = ID) FUNCTION_ID, " +
                                   "(SELECT REF_DATA || ' - ' || REF_DESC FROM PROP_PARAMETER_REF_D WHERE q.LOCATION_ID = ID) LOCATION_ID, " +
                                  /* "(SELECT PERUNTUKAN_NAME FROM PERUNTUKAN WHERE q.PERUNTUKAN = ID) PERUNTUKAN, " +*/
                                   "q.PROFIT_CENTER_ID_NEW || ' - ' || c.TERMINAL_NAME PROFIT_CENTER, q.KODE_ASET " +
                            "FROM RENTAL_OBJECT q, BUSINESS_ENTITY w, PROFIT_CENTER c, V_BE a " +
                            "WHERE w.BE_ID = q.BE_ID AND q.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) AND c.PROFIT_CENTER_ID=q.PROFIT_CENTER_ID_NEW " + wheresearch + " ORDER BY q.RO_NUMBER DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_RENTALOBJECT_Remote>(fullSql, new { a = start, b = end, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, d = KodeCabang });
                    }
                    catch (Exception e)
                    {
                        string aaa = e.ToString();
                    }
               
                    
            result = new DataTablesMasterRentalObject();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------------------------- INSERT DATA FILES --------------------------------
        public bool AddFiles(string sFile, string directory, string KodeCabang, string RO_NUMBER)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO RENTAL_OBJECT_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, RO_NUMBER) " +
                "VALUES (:a, :b, :c)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = RO_NUMBER
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM RENTAL_OBJECT_ATTACHMENT " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM RENTAL_OBJECT_ATTACHMENT " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------- DATA TABLE ATTACHMENT --------------------------
        public DataTablesRoAttachment GetDataAttachment(int draw, int start, int length, string search, string idBE)
        {
            int count = 0;
            int end = start + length;
            DataTablesRoAttachment result = null;

            IEnumerable<AUP_RO_ATTACHMENT> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    string fullSqlCount = "SELECT count(*) FROM (sql)";

                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID, DIRECTORY, FILE_NAME, RO_NUMBER FROM RENTAL_OBJECT_ATTACHMENT WHERE RO_NUMBER=:c ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_RO_ATTACHMENT>(fullSql, new { a = start, b = end, c = idBE });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idBE });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        // Do nothing. Not implemented yet.
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }


            result = new DataTablesRoAttachment();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            // connection.Close();
            return result;
        }

        //------------------------------- DATA TABLE ATTACHMENT --------------------------
        public DataTablesMaps GetDataMaps(int draw, int start, int length, string search, string idBE)
        {
            int count = 0;
            int end = start + length;
            DataTablesMaps result = null;

            IEnumerable<AUP_MAPS_Remote> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    string fullSqlCount = "SELECT count(*) FROM (sql)";

                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID, NO_URUT ,LATITUDE, LONGITUDE, RO_NUMBER FROM RENTAL_OBJECT_MAPS WHERE RO_NUMBER=:c ORDER BY ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_MAPS_Remote>(fullSql, new { a = start, b = end, c = idBE });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idBE });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        // Do nothing. Not implemented yet.
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }


            result = new DataTablesMaps();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            // connection.Close();
            return result;
        }

        public bool AddDataMaps(DataMaps data, dynamic dataUser)
        {
            int q = 0;
            int r = 0;
            string UserID = dataUser.UserID;
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            var cek = "SELECT * FROM RENTAL_OBJECT_MAPS WHERE LATITUDE=:a AND USER_ID_CREATE=:b AND RO_NUMBER=:c";
            var lat = connection.ExecuteScalar<String>(cek, new { a = data.LATITUDE, b = UserID, c = data.RO_NUMBER });
            
            if (lat != null)
            {
                r = 0;
            } else
            {
                
                string sql = "INSERT INTO RENTAL_OBJECT_MAPS " +
                "(RO_NUMBER, LATITUDE, LONGITUDE, USER_ID_CREATE, NO_URUT) " +
                "VALUES (:a, :b, :c, :d, :e)";

                try
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.RO_NUMBER,
                        b = data.LATITUDE,
                        c = data.LONGITUDE,
                        d = UserID,
                        e = data.NO_URUT,
                    });
                }
                catch (Exception)
                {
                    r = 0;
                    throw;
                }
            }
            
            result = ( r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool EditMaps(DataMaps data, dynamic dataUser)
        {
            bool result = false;
            string UserID = dataUser.UserID;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE RENTAL_OBJECT_MAPS SET " +
                         "RO_NUMBER=:a, LATITUDE=:b, LONGITUDE=:c, USER_ID_UPDATE=:d, NO_URUT=:f " +
                         "WHERE ID=:e";

            int r = connection.Execute(sql, new
            {
                a = data.RO_NUMBER,
                b = data.LATITUDE,
                c = data.LONGITUDE,
                d = UserID,
                e = data.ID,
                f = data.NO_URUT,
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteMaps(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM RENTAL_OBJECT_MAPS " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM RENTAL_OBJECT_MAPS " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        public IEnumerable<DDAsset> GetDataAsset(string q)
        {
            IEnumerable<DDAsset> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection("db_asset"))
                {
                    string sql = @"select * from (
                                select asset_no, asset_no || ' - ' || asset_desc as asset from ESIAT_MASTER_ASSET ) a 
                                WHERE LOWER(asset) like :b ";

                    listData = connection.Query<DDAsset>(sql, new { b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

    }
}