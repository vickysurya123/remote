﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ApprovalSetting;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;

namespace Remote.DAL
{
    public class ApprovalSettingDAL
    {
        //--------------------------------- VIEW DATA ApprovalSetting ---------------------------
        public IDataTable GetData(int draw, int start, int length, string search, string kodecabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            dynamic listData = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                
                //search filter data
                string where = search.Length >= 2 ? " AND ((UPPER(b.BE_NAME) LIKE '%' || '"+search.ToUpper()+"'|| '%')) " : "";
                string sql = @"select a.ID,MODUL_ID,a.BE_ID, b.BE_NAME from APPROVAL_SETTING_HEADER a
                                join BUSINESS_ENTITY b on a.BE_ID = b.BE_ID
                                WHERE b.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :branch UNION SELECT DISTINCT BRANCH_WHERE BRANCH_ID FROM V_BE WHERE BRANCH_ID = :branch) 
                                " +where+ " ORDER BY a.BE_ID ASC";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<APPROVAL_SETTING_HEADER>(fullSql, new
                {
                    a = start,
                    b = end,
                    branch = kodecabang
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount, new { branch = kodecabang });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        //------------------------ LIST DROP DOWN BRANCH --------------------- 
        public IEnumerable<DDBranch> GetDataListBranch(string kodecabang)
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string sql = "";
            if(kodecabang == "2")
            {
                 sql = @"SELECT a.BRANCH_ID, a.BE_NAME, a.BE_ID FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE REG_GROUP_ONLY = 1 and BRANCH_WHERE = 100) 
                        ORDER BY BE_NAME ASC";
            } else if (kodecabang == "9")
            {
                 sql = @"SELECT a.BRANCH_ID, a.BE_NAME, a.BE_ID FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE REG_GROUP_ONLY = 1 and BRANCH_WHERE = 110) 
                        ORDER BY BE_NAME ASC";
            }
            else if (kodecabang == "12")
            {
                 sql = @"SELECT a.BRANCH_ID, a.BE_NAME, a.BE_ID FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE REG_GROUP_ONLY = 1 and BRANCH_WHERE = 130) 
                        ORDER BY BE_NAME ASC";
            }
            else if (kodecabang == "24")
            {
                 sql = @"SELECT a.BRANCH_ID, a.BE_NAME, a.BE_ID FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE REG_GROUP_ONLY = 1 and BRANCH_WHERE = 120) 
                        ORDER BY BE_NAME ASC";
            } else
            {
                 sql = @"SELECT a.BRANCH_ID, a.BE_NAME, a.BE_ID FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) 
                        ORDER BY BE_NAME ASC";
            }
            
            //string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY " + where + " ORDER BY BE_NAME ASC";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql, new { d = kodecabang });
            connection.Close();
            return listDetil;

        }

        public IEnumerable<DDBranch> GetDataListBusinessEntity(string kodecabang)
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string sql = @"SELECT a.BE_ID, a.BE_NAME FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null AND a.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) 
                        ORDER BY BE_NAME ASC";
            //string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY " + where + " ORDER BY BE_NAME ASC";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql, new { d = kodecabang });
            connection.Close();
            return listDetil;

        }

        //------------------------ LIST DROP DOWN POSITION ---------------------
        public IEnumerable<DDPosition> GetDataListPosition()
        {
            string sql = "SELECT ROLE_ID, ROLE_NAME, PROPERTY_ROLE FROM APP_ROLE WHERE APP_ID=1 ORDER BY ROLE_NAME ASC";

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            //IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDPosition> listDetil = connection.Query<DDPosition>(sql);
            connection.Close();
            return listDetil;
        }

        //------------------- LIST DROP DOWN USAGE TYPE ------------------------
        public IEnumerable<DDContractUsage> GetDataContractUsage()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'CONTRACT_USAGE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDContractUsage> listDetil = connection.Query<DDContractUsage>(sql);

            connection.Close();
            return listDetil;
        }

        //------------------- LIST DROP DOWN RENTAL REQUEST TYPE ------------------------
        public IEnumerable<DDRentalType> GetDataRentalType()
        {
            string sql = "SELECT ID, REF_DATA, REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'RENTAL_REQUEST_TYPE' AND ACTIVE = '1'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRentalType> listDetil = connection.Query<DDRentalType>(sql);
            connection.Close();
            return listDetil;
        }

        public IEnumerable<APPROVAL_SETTING_PARAMETER> GetDataListParam()
        {
            string sql = "select ID, PARAMETER_NAME from APPROVAL_SETTING_PARAMETER";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<APPROVAL_SETTING_PARAMETER> listDetil = connection.Query<APPROVAL_SETTING_PARAMETER>(sql);
            connection.Close();
            return listDetil;
        }
        public Results AddDataHeader(DataApprovalSettingHeader data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"select BE_ID from BUSINESS_ENTITY a where a.BRANCH_ID = :a ";
                var be_id = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.BE_ID
                });

                sql = @"select count(ID) from APPROVAL_SETTING_HEADER a where a.BE_ID = :a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = be_id
                });

                if (r > 0)
                {
                    result.Msg = "Approval Setting for Business Entity Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into APPROVAL_SETTING_HEADER (BE_ID, MODUL_ID, CREATED_DATE, CREATED_BY)
                            values(:a, :b, :c, :d)";
                    r = connection.Execute(sql, new
                    {
                        a = be_id,
                        b = data.MODUL_ID,
                        c = DateTime.Now,
                        d = name,

                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }
        public Results EditDataHeader(DataApprovalSettingHeader data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"select BE_ID from BUSINESS_ENTITY a where a.BRANCH_ID = :a ";
                var be_id = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.BE_ID
                });

                sql = @"select count(ID) from APPROVAL_SETTING_HEADER a where a.BE_ID = :a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = be_id
                });

                if (r > 0)
                {
                    result.Msg = "Approval Setting for Business Entity Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"update APPROVAL_SETTING_HEADER a set a.BE_ID =:a,  a.MODUL_ID =:b, a.UPDATED_DATE =:d, a.UPDATED_BY =:e where a.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        a = be_id,
                        b = data.MODUL_ID,
                        c = data.ID,
                        d = DateTime.Now,
                        e = name
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Updated Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Update Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }
            connection.Close();
            return result;
        }
        public Results DeleteDataHeader(DataApprovalSettingHeader data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    var sql = @"delete from APPROVAL_SETTING_DETAIL_ROW a where a.DETAIL_ID IN ( 
                        select b.ID from APPROVAL_SETTING_DETAIL b 
                        join APPROVAL_SETTING_DETAIL_ROW c on b.ID = c.DETAIL_ID and b.HEADER_ID = :c )";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    sql = @"delete from APPROVAL_SETTING_DETAIL_PARAM a where a.DETAIL_ID IN ( 
                        select b.ID from APPROVAL_SETTING_DETAIL b 
                        join APPROVAL_SETTING_DETAIL_PARAM c on b.ID = c.DETAIL_ID and b.HEADER_ID = :c )";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    sql = @"delete from APPROVAL_SETTING_DETAIL a where a.HEADER_ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    sql = @"delete from APPROVAL_SETTING_HEADER a where a.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Deleted Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Delete Data";
                        result.Status = "E";

                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        //------------------------------ VIEW EDIT DATA --------------------------------
        public APPROVAL_SETTING_HEADER GetData(string id)
        {
            //string xCount = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            APPROVAL_SETTING_HEADER result = new APPROVAL_SETTING_HEADER();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = @"select a.ID, a.MODUL_ID,a.BE_ID, b.BE_NAME from APPROVAL_SETTING_HEADER a
                                 join BUSINESS_ENTITY b on a.BE_ID = b.BE_ID where a.ID =:a";

                result = connection.Query<APPROVAL_SETTING_HEADER>(sql, new { a = id }).FirstOrDefault();

            }
            catch (Exception)
            {

                throw;
            }


            connection.Close();
            return result;
        }

        public IDataTable GetDataDetail(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {

                string whereSearch = search.Length >= 2 ? " and (UPPER(Q.CONTRACT_USAGE)||UPPER(Q.CONTRACT_TYPE)||UPPER(Q.TOP_APPROVAL) LIKE '%' ||'" + search.ToUpper() + "'|| '%')  " : "";
                string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b " + whereSearch + " ) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                
                //string sql = @"SELECT ID, TOP_APPROVAL, HEADER_ID, CONTRACT_USAGE, CONTRACT_TYPE,
                //                TO_CHAR(END_DATE,'DD/MM/YYYY') as END_DATE, TO_CHAR(START_DATE,'DD/MM/YYYY') as START_DATE
                //                FROM V_APV_DETAIL_HEADER
                //                WHERE HEADER_ID =:id_header 
                //                " + whereSearch + " ORDER BY CONTRACT_TYPE ASC, APPROVAL_NO DESC";
                string sql = @"SELECT B.ID,C.APPROVAL_NO,C.ROLE_NAME TOP_APPROVAL,B.HEADER_ID,
(SELECT ref_data || ' - ' || ref_desc AS ref_desc FROM prop_parameter_ref_d WHERE B.CONTRACT_TYPE = REF_DATA AND REF_CODE = 'CONTRACT_REQUEST') CONTRACT_TYPE, 
(SELECT REF_DATA || ' - ' || REF_DESC AS REF_DESC FROM PROP_PARAMETER_REF_D WHERE B.CONTRACT_USAGE = REF_DATA AND REF_CODE = 'CONTRACT_USAGE') CONTRACT_USAGE,
to_char(B.END_DATE,'DD/MM/YYYY HH24:MI:SS') END_DATE, to_char(B.START_DATE, 'DD/MM/YYYY HH24:MI:SS') START_DATE
FROM APPROVAL_SETTING_HEADER A
JOIN APPROVAL_SETTING_DETAIL B ON B.HEADER_ID = A.ID
JOIN (SELECT A.APPROVAL_NO, A.ROLE_NAME, A.DETAIL_ID, A.PROPERTY_ROLE FROM APPROVAL_SETTING_DETAIL_ROW  A
JOIN ( SELECT MAX(A.APPROVAL_NO) APPROVAL_NO, A.DETAIL_ID FROM APPROVAL_SETTING_DETAIL_ROW A
GROUP BY A.DETAIL_ID) B ON B.DETAIL_ID = A.DETAIL_ID AND A.APPROVAL_NO = B.APPROVAL_NO) C ON C.DETAIL_ID = B.ID
JOIN APP_REPO.APP_ROLE D ON C.PROPERTY_ROLE = D.PROPERTY_ROLE AND C.ROLE_NAME = D.ROLE_NAME
WHERE A.ID = :id_header order by TOP_APPROVAL asc, contract_type asc, approval_no desc";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<APPROVAL_SETTING_DETAIL>(fullSql, new
                {
                    a = start,
                    b = end,
                    id_header = id
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    id_header = id
                });
                
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public Results AddDataDetail(DataApprovalSettingDetail data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string usage = string.IsNullOrWhiteSpace(data.CONTRACT_USAGE) ? "NULL" : "'" + data.CONTRACT_USAGE + "'";
                var sql = @"insert into APPROVAL_SETTING_DETAIL (HEADER_ID, START_DATE, END_DATE, CONTRACT_USAGE, CREATED_DATE, CREATED_BY, CONTRACT_TYPE)
                            values(:a, to_date(:b, 'DD/MM/YYYY'), to_date(:c, 'DD/MM/YYYY'), "+usage+", :e, :f, :g)";
                r = connection.Execute(sql, new
                {
                    a = data.HEADER_ID,
                    b = data.START_DATE,
                    c = data.END_DATE,
                    e = DateTime.Now,
                    f = name,
                    g = data.CONTRACT_TYPE,
                });
                if (r > 0)
                {
                    var sql3 = "select max(ID) from APPROVAL_SETTING_DETAIL";
                    var id_detail = connection.ExecuteScalar<int>(sql3, new { });
                    var sqli = @"insert into APPROVAL_SETTING_DETAIL_ROW (APPROVAL_NO,PROPERTY_ROLE,DETAIL_ID,ROLE_NAME,CREATED_DATE,CREATED_BY) 
                        values(:b, :c, :e, :f, :g, :h)";
                    foreach (RoleRow temp in data.Role)
                    {
                        r = connection.Execute(sqli, new
                        {
                            b = temp.APPROVAL_NO,
                            c = temp.PROPERTY_ROLE,
                            e = id_detail,
                            f = temp.ROLE_NAME,
                            g = DateTime.Now,
                            h = name,
                        });
                    }

                    var sql2 = @"insert into APPROVAL_SETTING_DETAIL_PARAM (PARAM_ID,MIN,MAX,PARAM_NAME,DETAIL_ID,CREATED_DATE,CREATED_BY) 
                        values(:a, :b, :c, :d, :e, :f, :g)";
                    foreach (ParamRow temp in data.Param)
                    {
                        r = connection.Execute(sql2, new
                        {
                            a = temp.PARAM_ID,
                            b = temp.MIN,
                            c = temp.MAX,
                            d = temp.PARAM_NAME,
                            e = id_detail,
                            f = DateTime.Now,
                            g = name,
                        });
                    }

                    result.Msg = "Data Added Successfully";
                    result.Status = "S";

                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public IDataTable GetViewDetail(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"select APPROVAL_NO,PROPERTY_ROLE,DETAIL_ID,ROLE_NAME from APPROVAL_SETTING_DETAIL_ROW a
                                   where a.DETAIL_ID = :id order by a.APPROVAL_NO asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<RoleRow>(fullSql, new { a = start, b = end, id = id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { id = id });
                }
                else
                {
                    //search filter data
                    string sql = @"select APPROVAL_NO,PROPERTY_ROLE,DETAIL_ID,ROLE_NAME from APPROVAL_SETTING_DETAIL_ROW a
                                    WHERE a.DETAIL_ID =:id order by a.APPROVAL_NO asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<RoleRow>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        id = id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IDataTable GetViewDetailParam(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"select PARAM_ID,PARAM_NAME,MIN,MAX from APPROVAL_SETTING_DETAIL_PARAM a
                                   where a.DETAIL_ID = :id order by a.ID asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<ParamRow>(fullSql, new { a = start, b = end, id = id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { id = id });
                }
                else
                {
                    //search filter data
                    string sql = @"select PARAM_ID,PARAM_NAME,MIN,MAX from APPROVAL_SETTING_DETAIL_PARAM a
                                   where a.DETAIL_ID = :id order by a.ID asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<ParamRow>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        id = id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public Results DeleteDataDetail(DataApprovalSettingDetail data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    var sql = @"delete from APPROVAL_SETTING_DETAIL_ROW a where a.DETAIL_ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    sql = @"delete from APPROVAL_SETTING_DETAIL_PARAM a where a.DETAIL_ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    sql = @"delete from APPROVAL_SETTING_DETAIL a where a.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Deleted Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Delete Data";
                        result.Status = "E";

                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public IEnumerable<RoleRow> GetDataDetailRow(string id)
        {
            string sql = @"select ID, APPROVAL_NO,PROPERTY_ROLE,DETAIL_ID,ROLE_NAME from APPROVAL_SETTING_DETAIL_ROW a
                        where a.DETAIL_ID = :id order by a.APPROVAL_NO asc";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<RoleRow> listDetil = connection.Query<RoleRow>(sql, new
            {
                id = id
            });
            connection.Close();
            return listDetil;
        }

        public IEnumerable<ParamRow> GetDataDetailParam(string id)
        {
            string sql = @"select ID,PARAM_ID,PARAM_NAME,MIN,MAX from APPROVAL_SETTING_DETAIL_PARAM a
                        where a.DETAIL_ID = :id order by a.ID asc";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<ParamRow> listDetil = connection.Query<ParamRow>(sql, new
            {
                id = id
            });
            connection.Close();
            return listDetil;
        }

        public Results EditDataDetail(DataApprovalSettingDetail data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            string usage = string.IsNullOrWhiteSpace(data.CONTRACT_USAGE) ? " CONTRACT_USAGE = NULL " : " CONTRACT_USAGE = '" + data.CONTRACT_USAGE + "' ";
            
            try
            {
                var sql = @"update APPROVAL_SETTING_DETAIL a 
                    set START_DATE = TO_DATE(:b, 'DD/MM/YYYY'), 
                    END_DATE = TO_DATE(:c, 'DD/MM/YYYY'), 
                    NOTIF_START=:d, 
                    UPDATED_DATE =:e, 
                    UPDATED_BY =:f, 
                    CONTRACT_TYPE =:g, " + usage + " where a.ID = :id_detail ";
                r = connection.Execute(sql, new
                {
                    b = data.START_DATE,
                    c = data.END_DATE,
                    d = data.NOTIF_START,
                    id_detail = data.ID,
                    e = DateTime.Now,
                    f = name,
                    g = data.CONTRACT_TYPE
                });
                if (r > 0)
                {
                    //delrole
                    if(data.delRole != null)
                    {
                        sql = "delete from APPROVAL_SETTING_DETAIL_ROW where ID =:a";
                        foreach (RoleRow temp in data.delRole)
                        {
                            r = connection.Execute(sql, new
                            {
                                a = temp.ID
                            });
                        }
                    }

                    //delrole
                    if (data.delParam != null)
                    {
                        sql = "delete from APPROVAL_SETTING_DETAIL_PARAM where ID =:a";
                        foreach (ParamRow temp in data.delParam)
                        {
                            r = connection.Execute(sql, new
                            {
                                a = temp.ID
                            });
                        }
                    }
                    var sqlinsert = @"insert into APPROVAL_SETTING_DETAIL_ROW (APPROVAL_NO,PROPERTY_ROLE,DETAIL_ID,ROLE_NAME,UPDATED_DATE,UPDATED_BY) 
                        values( :b, :c,  :e, :f, :h, :i)";
                    var sqlupdate = @"update APPROVAL_SETTING_DETAIL_ROW set APPROVAL_NO=:b, PROPERTY_ROLE=:c, DETAIL_ID=:e, ROLE_NAME=:f, UPDATED_DATE=:h,UPDATED_BY=:i where ID =:g";
                    foreach (RoleRow temp in data.Role)
                    {
                        //sql = (temp.ID == null ? sqlinsert : sqlupdate);
                        if (temp.ID == null)
                        {
                            r = connection.Execute(sqlinsert, new
                            {
                                b = temp.APPROVAL_NO,
                                c = temp.PROPERTY_ROLE,
                                e = data.ID,
                                f = temp.ROLE_NAME,
                                h = DateTime.Now,
                                i = name
                            });
                        }
                        else
                        {
                            r = connection.Execute(sqlupdate, new
                            {
                                b = temp.APPROVAL_NO,
                                c = temp.PROPERTY_ROLE,
                                e = data.ID,
                                f = temp.ROLE_NAME,
                                g = temp.ID,
                                h = DateTime.Now,
                                i = name
                            });
                        }
                    }

                    //del all param
                    //sql = "delete from APPROVAL_SETTING_DETAIL_PARAM a where a.DETAIL_ID =:c";
                    //r = connection.Execute(sql, new
                    //{
                    //    c = data.ID
                    //});
                    sqlinsert = @"insert into APPROVAL_SETTING_DETAIL_PARAM (PARAM_ID,PARAM_NAME,MIN,MAX,DETAIL_ID,UPDATED_DATE,UPDATED_BY) 
                        values( :a, :b,  :c, :d, :e, :g, :h)";
                    sqlupdate = @"update APPROVAL_SETTING_DETAIL_PARAM set PARAM_ID=:a, PARAM_NAME=:b, MIN=:c, MAX=:d, DETAIL_ID=:e,UPDATED_DATE=:g,UPDATED_BY=:h where ID =:f";
                    foreach (ParamRow temp in data.Param)
                    {
                        if (temp.ID == null)
                        {
                            r = connection.Execute(sqlinsert, new
                            {
                                a = temp.PARAM_ID,
                                b = temp.PARAM_NAME,
                                c = temp.MIN,
                                d = temp.MAX,
                                e = data.ID,
                                g = DateTime.Now,
                                h = name
                            });
                        }
                        else
                        {
                            r = connection.Execute(sqlupdate, new
                            {
                                a = temp.PARAM_ID,
                                b = temp.PARAM_NAME,
                                c = temp.MIN,
                                d = temp.MAX,
                                e = data.ID,
                                f = temp.ID,
                                g = DateTime.Now,
                                h = name
                            });
                        }

                    }

                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";

                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results SearchBranch(DataApprovalSettingHeader data)
        {
            Results result = new Results();
            string r = "";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                var sql = @"select a.BRANCH_ID, a.REGIONAL_ID from BUSINESS_ENTITY a where a.BE_ID = :a";
                r = connection.ExecuteScalar<string>(sql, new
                {
                    a = data.BE_ID,
                });
                result.Msg = r;
                result.Status = "S";
            }
            catch (Exception)
            {
                result.Msg = "Error";
                result.Status = "S";
            }

            connection.Close();
            return result;
        }
        
        public Results SetApprovalRole(string data, string type)
        {
            Results result = new Results();
           // string r = "";
            object dyn = new { };
            int apv_pos;
            //string apv_position = "";
            V_APV_SETTING approval = null;
            string paramquery = ""; string paramwhere = "";
            var det_id = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
              {
                var sql = @"select * from V_APV_CONTRACT_OFFER where CONTRACT_OFFER_NO =:a";
                approval = connection.Query<V_APV_SETTING>(sql, new
                {
                    a = data
                }).DefaultIfEmpty(null).FirstOrDefault();
               

                //apv_pos = (approval.OFFER_STATUS == 10 || type == "3" ? 0 : approval.APV_POSITION + 1);

                if (type == "2" && approval.OFFER_STATUS == 20)
                {
                    apv_pos = approval.APV_POSITION + 1;
                }
                else if (approval.OFFER_STATUS == 10 || approval.COMPLETED_STATUS == 3)
                {
                    apv_pos = approval.APV_POSITION;
                }
              /*  else if (type == "2" && approval.OFFER_STATUS >= 50) // edited dss 29 okt 2019
                {
                    apv_pos = approval.APV_POSITION;
                }

               if (type == "2" && approval.OFFER_STATUS >= 50 && approval.APV_POSITION >= 1) // edited dss 29 okt 2019
                {
                    apv_pos = approval.APV_POSITION + 1;
                }*/

                //else if (type != "3" && approval.OFFER_STATUS != 10)
                //else if (type == "2" && approval.OFFER_STATUS != 10)
                //{
                //    apv_pos = approval.APV_POSITION;
                //}
                else
                {
                    apv_pos = approval.APV_POSITION + 1;
                }

                if (approval.COMPLETED_STATUS == 3 && approval.OFFER_STATUS >= 50)
                {
                    apv_pos = 0; //edited dss 6 nov 2019
                }
                var date = approval.CREATION_DATE;//.Split(new[] { " " }, StringSplitOptions.None);

                //query param
                //                sql = @"select d.* from APPROVAL_SETTING_HEADER a
                //join APPROVAL_SETTING_DETAIL b on a.ID = b.HEADER_ID
                //join APPROVAL_SETTING_DETAIL_PARAM c on b.ID = c.DETAIL_ID
                //join APPROVAL_SETTING_PARAMETER d on d.ID = c.PARAM_ID";

                //                dynamic paramList = connection.Query<dynamic>(sql).ToList();
                //                int i = 0;
                //                string joinQuery = "";
                //                foreach (var temp in paramList)
                //                {
                //                    var aliasJoin = "param" + i;
                //                    joinQuery += @"join " + temp.PARAMETER_TABLE + " " + aliasJoin + " on " + aliasJoin + "." + temp.PARAMETER_NAME + @" > c.MIN and 
                //                        " + aliasJoin + "." + temp.PARAMETER_NAME + @" < c.MAX";

                //                    if (temp.PARAMETER_NAME == "LUAS")
                //                    {
                //                        paramwhere += " and " + approval.LUAS + " BETWEEN " + temp.PARAMETER_NAME + "_MIN AND " + temp.PARAMETER_NAME + "_MAX ";
                //                    }
                //                    else if (temp.PARAMETER_NAME == "WAKTU")
                //                    {
                //                        paramwhere += " and " + approval.WAKTU + " BETWEEN " + temp.PARAMETER_NAME + "_MIN AND " + temp.PARAMETER_NAME + "_MAX ";
                //                    }
                //                    i++;
                //                }


                sql = @"SELECT ID, PARAMETER_NAME, PARAMETER_COLUMN, PARAMETER_TABLE FROM APPROVAL_SETTING_PARAMETER ";
                var param = connection.Query<APPROVAL_SETTING_PARAMETER>(sql).ToList();
                foreach (APPROVAL_SETTING_PARAMETER temp in param)
                {
                    paramquery += @" left join (
                                      select x.ID, x.PARAMETER_NAME,x.PARAMETER_TABLE, x.PARAMETER_COLUMN, z.DETAIL_ID, z.MIN " + temp.PARAMETER_COLUMN + @"_MIN , z.MAX " + temp.PARAMETER_COLUMN + @"_MAX from APPROVAL_SETTING_PARAMETER  x
                                      left join APPROVAL_SETTING_DETAIL_PARAM z on x.ID = z.PARAM_ID and x.ID = " + temp.ID + @"
                                    ) " + temp.PARAMETER_COLUMN + " on " + temp.PARAMETER_COLUMN + ".DETAIL_ID = b.ID ";

                    if (temp.PARAMETER_COLUMN == "LUAS")
                    {
                        paramwhere += " AND " + temp.PARAMETER_COLUMN + "_MIN <= "+ approval.LUAS + " AND " + temp.PARAMETER_COLUMN + "_MAX >=" + approval.LUAS;
                    }
                    else if (temp.PARAMETER_COLUMN == "JANGKA_WAKTU")
                    {
                        paramwhere += " AND " + temp.PARAMETER_COLUMN + "_MIN <= " + approval.WAKTU + " AND " + temp.PARAMETER_COLUMN + "_MAX >=" + approval.WAKTU;
                    }
                }

                paramwhere += " AND b.CONTRACT_TYPE = '" + approval.CONTRACT_TYPE + "' ";
                paramwhere += " AND (b.CONTRACT_USAGE IS NULL or b.CONTRACT_USAGE = '" + approval.CONTRACT_USAGE + "') ";

                // CHECK RULE CABANG
                string cabangOnly = " AND BE_GROUP_ONLY = 1 ";
                sql = @"select b.ID from APPROVAL_SETTING_HEADER a
                        join APPROVAL_SETTING_DETAIL b on a.ID = b.HEADER_ID " + 
                        paramquery + 
                      @" where a.BE_ID IN (
                        SELECT DISTINCT BE_WHERE
                        FROM V_BE 
                        WHERE BE_ID = " + approval.BE_ID + cabangOnly + 
                        " ) and b.START_DATE <= :a AND b.END_DATE >= :a " + paramwhere +
                        " ORDER BY b.CONTRACT_USAGE ASC ";
                det_id = connection.ExecuteScalar<int>(sql, new { a = date });

                // IF CABANG NOT EXIST
                if ( det_id == 0 )
                {
                    // CHECK RULE REGIONAL
                    string regionalOnly = " AND REG_GROUP_ONLY = 1 ";
                    sql = @"select b.ID from APPROVAL_SETTING_HEADER a
                            join APPROVAL_SETTING_DETAIL b on a.ID = b.HEADER_ID" +
                            paramquery +
                            @" where a.BE_ID IN (
                            SELECT DISTINCT BE_WHERE
                            FROM V_BE 
                            WHERE BE_ID = " + approval.BE_ID + regionalOnly +
                            " ) and b.START_DATE <= :a AND b.END_DATE >= :a " + paramwhere +
                            " ORDER BY b.CONTRACT_USAGE ASC ";
                    det_id = connection.ExecuteScalar<int>(sql, new { a = date });
                }

                // IF REGIONAL NOT EXIST
                if (det_id == 0)
                {
                    // CHECK RULE PUSAT
                    string pusatOnly = " AND PUSAT_GROUP_ONLY = 1 ";
                    sql = @"select b.ID from APPROVAL_SETTING_HEADER a
                            join APPROVAL_SETTING_DETAIL b on a.ID = b.HEADER_ID" +
                            paramquery +
                            @" where a.BE_ID IN (
                            SELECT DISTINCT BE_WHERE
                            FROM V_BE 
                            WHERE BE_ID = " + approval.BE_ID + pusatOnly +
                            " ) and b.START_DATE <= :a AND b.END_DATE >= :a " + paramwhere +
                            " ORDER BY b.CONTRACT_USAGE ASC ";
                    det_id = connection.ExecuteScalar<int>(sql, new { a = date });
                }
                
                // RUN APPROVAL IF RULE IS FOUND
                if (det_id != 0)
                {
                    sql = @"select * from APPROVAL_SETTING_DETAIL_ROW
                        where DETAIL_ID = :a ORDER by APPROVAL_NO";
                    var role = connection.Query<RoleRow>(sql, new
                    {
                        a = det_id
                    });
                    if (type == "2")//approve atau release
                    {
                        dynamic dataRoleIni = null;
                        var now_apv = 20;
                        var next_apv = 0;
                        var top_apv = 0;
                        sql = @"select PROPERTY_ROLE
                        from APPROVAL_SETTING_DETAIL_ROW
                        where DETAIL_ID = :a and APPROVAL_NO = :b and rownum = 1 
                        ORDER by APPROVAL_NO";

                        if (apv_pos > 0)// selain release
                        {
                            now_apv = connection.ExecuteScalar<int>(sql, new
                            {
                                a = det_id,
                                b = apv_pos
                            });
                        }

                        if (role.Count() > apv_pos)
                        {
                            next_apv = connection.ExecuteScalar<int>(sql, new
                            {
                                a = det_id,
                                b = apv_pos + 1
                            });

                            var sql2 = @"select ROLE_NAME
                            from APPROVAL_SETTING_DETAIL_ROW
                            where DETAIL_ID = :a and APPROVAL_NO = :b and rownum = 1 
                            ORDER by APPROVAL_NO";

                            dataRoleIni = connection.ExecuteScalar(sql2, new
                            {
                                a = det_id,
                                b = apv_pos + 1
                            });
                        }
                        else
                        {
                            next_apv = now_apv;
                            apv_pos = role.Count();
                        }


                        top_apv = connection.ExecuteScalar<int>(sql, new
                        {
                            a = det_id,
                            b = role.Count()
                        });
                        dyn = new
                        {
                            det_id = det_id,
                            now_apv = now_apv,
                            next_apv = next_apv,
                            top_apv = top_apv,
                            apv_position = apv_pos,
                            role_name = dataRoleIni
                        };

                        result.Data = dyn;
                        result.Status = "S";
                        result.Msg = "ok";
                    }
                    else if (type == "3" || type == "4")//revisi atau reject
                    {
                        sql = @"select PROPERTY_ROLE
                        from APPROVAL_SETTING_DETAIL_ROW
                        where DETAIL_ID = :a and APPROVAL_NO = :b ORDER by APPROVAL_NO";                                                 

                        var now_apv = connection.ExecuteScalar<int>(sql, new
                        {
                            a = det_id,
                            b = apv_pos
                        });
                        dyn = new
                        {
                            det_id = det_id,
                            now_apv = now_apv,

                            apv_position = ""
                        };
                    
                            sql = @"select PROPERTY_ROLE
                        from APPROVAL_SETTING_DETAIL_ROW
                        where DETAIL_ID = :a and APPROVAL_NO < :b ORDER by APPROVAL_NO";
                        var positionlist = connection.Query(sql, new
                        {
                            a = det_id,
                            b = apv_pos
                        }).ToList();
                        result.Positionlist = positionlist;
                        result.Data = dyn;
                        result.Status = "S";
                        result.Msg = "ok";
                    }
                }
                else
                {
                    result.Status = "E";
                    result.Msg = "Approval Setting tidak ditemukan";
                }
            }
            catch (Exception)
            {
                result.Msg = "Error";
                result.Status = "E";
            }
            connection.Close();
            return result;
        }
        public IEnumerable<DDBranch> GetDataListAll()
        {
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();
            string sql = @"SELECT a.BRANCH_ID, a.BE_NAME FROM BUSINESS_ENTITY a
                        where a.BE_ID is not null 
                        ORDER BY BE_NAME ASC";
            //string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY " + where + " ORDER BY BE_NAME ASC";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql);
            connection.Close();
            return listDetil;

        }
        public int IsCabang(string kodecabang)
        {
            int res = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = @"select count(1) from BUSINESS_ENTITY where REGIONAL_FLAG = 0 and BRANCH_ID != 0 and BRANCH_ID = :a";
                res = connection.Query<int>(sql, new { a = kodecabang }).FirstOrDefault();
            }
            catch(Exception ex)
            {
                string aaa = ex.ToString();
            }
            
            connection.Close();
            return res;

        }
        public IDataTable GetDataExp(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            dynamic listData = null;

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                //search filter data
                string where = search.Length >= 2 ? " where ((UPPER(STATUS) LIKE '%' || '" + search.ToUpper() + "'|| '%') OR (UPPER(DAYS) LIKE '%' || '" + search.ToUpper() + "'|| '%') OR (to_char(START_DATE, 'DD/MM/YYYY') LIKE '%' || '" + search.ToUpper() + "'|| '%') OR (to_char(END_DATE, 'DD/MM/YYYY') LIKE '%' || '" + search.ToUpper() + "'|| '%')) " : "";
                string sql = @"select ID,DAYS,to_char(START_DATE, 'DD/MM/YYYY')START_DATE,to_char(END_DATE, 'DD/MM/YYYY')END_DATE,STATUS from EXP_CONTRACT_NOTIFICATION " + where + @" order by status desc";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<EXP_NOTIF>(fullSql, new
                {
                    a = start,
                    b = end,
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount);
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }
        public Results StatusExpNotif(EXP_NOTIF data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            var upd = 0;
            var sql = "";
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    upd = (data.STATUS == 0 ? 1 : 0);
                    if(upd == 1)
                    {
                        sql = @"update EXP_CONTRACT_NOTIFICATION set status =:b";
                        r = connection.Execute(sql, new
                        {
                            a = data.ID,
                            b = 0
                        }, transaction: transaction);
                    }
                    sql = @"update EXP_CONTRACT_NOTIFICATION set status =:b where ID = :a";
                    r = connection.Execute(sql, new
                    {
                        a = data.ID,
                        b = upd
                    }, transaction: transaction);

                    
                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Updated Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Update Data";
                        result.Status = "E";

                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }
        public Results InsertExpNotif(EXP_NOTIF data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            var stat = 0;

            try
            {
                string sql = @"select Count(1) from EXP_CONTRACT_NOTIFICATION where STATUS = 1 ";
                r = connection.ExecuteScalar<int>(sql);
                stat = r > 0 ? 0 : 1;

                sql = @"insert into EXP_CONTRACT_NOTIFICATION (DAYS, START_DATE, END_DATE, STATUS)
                            values(:a, to_date(:b,'DD/MM/YYYY'), to_date(:c,'DD/MM/YYYY'), :d)";
                r = connection.Execute(sql, new
                {
                    a = data.DAYS,
                    b = data.START_DATE,
                    c = data.END_DATE,
                    d = stat,

                });
                if (r > 0)
                {
                    result.Msg = "Data Added Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();

            return result;
        }
    }
}