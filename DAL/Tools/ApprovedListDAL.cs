﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.TransContractOffer;
using Remote.Models.ApprovedList;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ApprovedListDAL
    {

        //-------------------------------------- DEPRECATED ------------------------------
        public DataTablesApprovedList GetDataHeaderContractOffer(int draw, int start, int length, string search, string KodeCabang, string UserRole)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (UserRole == "16") //SM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '60' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by SM' OR STATUS_CONTRACT_DESC = 'Rejected by SM') " +
                                    "ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '60' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by SM' OR STATUS_CONTRACT_DESC = 'Rejected by SM') " +
                                    "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                    "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper() });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "17") //DIREKTUR OPERASIONAL
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '70' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Director' OR STATUS_CONTRACT_DESC = 'Rejected by Director') " +
                                    "ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '70' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Director' OR STATUS_CONTRACT_DESC = 'Rejected by Director') " +
                                    "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +                                    
                                    "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper() });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "4" || UserRole == "7") //STAFF PUSAT & ADMIN
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                     "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                     "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE COMPLETED_STATUS = '1' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                          "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                          "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                          "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                          "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE " +
                                          "FROM V_CONTRACT_OFFER_HEADER WHERE COMPLETED_STATUS = '1' AND " +
                                          "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +                                                                              
                                          "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper() });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new {  a = start, b = end, c = search });
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                }
            }
            else
            {
                if (UserRole == "12") //MANAGER
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, "+
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE "+
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B "+
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO "+
                                    "AND STATUS_CONTRACT = '30' "+
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Mgr' OR STATUS_CONTRACT_DESC = 'Rejected by Mgr') " +
                                    "AND A.BRANCH_ID = :c ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '30' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Mgr' OR STATUS_CONTRACT_DESC = 'Rejected by Mgr') " +
                                    "AND A.BRANCH_ID = :d "+
                                    "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +                                    
                                    "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "4") //STAFF PUSAT & ADMIN
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                     "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                     "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                     "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                     "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE " +
                                     "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :c AND COMPLETED_STATUS = '1' ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                          "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                          "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                          "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                          "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE " +
                                          "FROM V_CONTRACT_OFFER_HEADER WHERE BRANCH_ID = :d AND COMPLETED_STATUS = '1' AND " +
                                          "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +
                                          "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search, d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                }
                else if (UserRole == "13") //DEPUTI GM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '40' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Dept GM' OR STATUS_CONTRACT_DESC = 'Rejected by Dept GM') " +
                                    "AND A.BRANCH_ID = :c ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '40' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by Dept GM' OR STATUS_CONTRACT_DESC = 'Rejected by Dept GM') " +
                                    "AND A.BRANCH_ID = :d "+
                                    "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +                                    
                                    "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                else if (UserRole == "15") //GM
                {
                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '50' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by GM' OR STATUS_CONTRACT_DESC = 'Rejected by GM') " +
                                    "AND A.BRANCH_ID = :c ORDER BY ID_INC DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = KodeCabang });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = KodeCabang });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        if (search.Length > 2)
                        {
                            try
                            {
                                string sql = "SELECT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                    "CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') " +
                                    "ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, " +
                                    "COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, " +
                                    "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE " +
                                    "FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B " +
                                    "WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " +
                                    "AND STATUS_CONTRACT = '50' " +
                                    "AND (STATUS_CONTRACT_DESC = 'Approved by GM' OR STATUS_CONTRACT_DESC = 'Rejected by GM') " +
                                    "AND A.BRANCH_ID = :d "+
                                    "AND ((A.CONTRACT_OFFER_NO LIKE '%'||:c||'%') OR (UPPER(A.CONTRACT_OFFER_TYPE) LIKE '%' ||:c|| '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%') OR (TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%' ||:c|| '%')) " +                                                                        
                                    "ORDER BY ID_INC DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = search.ToUpper(), d = KodeCabang });
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }       

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        //--------------------------------------- DEPRECATED ----------------------
        public DataTablesApprovedList GetDataHistoryWorkflow(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (KodeCabang == "0")
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT_DESC AS STATUS_CONTRACT, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }
            else
            {
                if (search.Length == 0)
                {
                    try
                    {
                        string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT || ' - ' || STATUS_CONTRACT_DESC AS STATUS_CONTRACT, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                                 "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                                 "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c AND BRANCH_ID = :d ORDER BY ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id, d = KodeCabang });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new { a = start, b = end, c = id, d = KodeCabang });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    //search filter data
                }
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }
        
        //------------------------------ DATA TABLE CONTRACT OFFER OBJECT ----------------------
        public DataTablesApprovedList GetDataDetailObject(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER CONDITION ----------------------
        public DataTablesApprovedList GetDataDetailCondition(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        //------------------------------ DATA TABLE CONTRACT OFFER MANUALY ----------------------
        public DataTablesApprovedList GetDataDetailManualy(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, to_char(DUE_DATE,'DD.MM.YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                            "FROM V_CONTRACT_OFFER_MANUAL " +
                            "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        //------------------------------ DATA TABLE CONTRACT OFFER MEMO ----------------------
        public DataTablesApprovedList GetDataDetailMemo(int draw, int start, int length, string search, string id_special)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, MEMO " +
                             "FROM V_CONTRACT_OFFER_MEMO " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;

        }

        public DataTablesAttachmentOffer GetDataAttachment(int draw, int start, int length, string search, string id_special)
        {

            int count = 0;
            int end = start + length;
            DataTablesAttachmentOffer result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_OFFER_ATTACHMENT> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = @"SELECT ID_ATTACHMENT, CASE WHEN ID_ATTACHMENT <= 8847 THEN 'https://s3.pelindo.co.id/remote/REMOTE/ContractOffer/' || CONTRACT_OFFER_ID || '/' || FILE_NAME
ELSE DIRECTORY END AS DIRECTORY, FILE_NAME, CONTRACT_OFFER_ID FROM PROP_CONTRACT_OFFER_ATTACHMENT WHERE CONTRACT_OFFER_ID=:c ORDER BY ID_ATTACHMENT DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_OFFER_ATTACHMENT>(fullSql, new { a = start, b = end, c = id_special });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id_special });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesAttachmentOffer();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();
            connection.Close();
            connection.Dispose();
            return result;
        }

        public bool AddFiles(string sFile, string directory, string KodeCabang, string subPath)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO PROP_CONTRACT_OFFER_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, CONTRACT_OFFER_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = subPath
            });

            result = (r > 0) ? true : false;

            connection.Close();
            connection.Dispose();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM PROP_CONTRACT_OFFER_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            connection.Dispose();
            return result;
        }

        //-------------------------------------- NEW DATA TABLE CONTRACT OFFER ------------------------------
        public DataTablesApprovedList GetDataHeaderContractOffernew(int draw, int start, int length, string search, string KodeCabang, string UserRole, string UserProperty)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            string where = string.Empty;
            string whereProperty = UserProperty != "0" ? " AND STATUS_CONTRACT = '" + UserProperty + "' " : "";
            where += whereProperty;
            string whereBranch = "";
            if (UserProperty == "50")
            {
                whereBranch = @" AND A.BRANCH_ID IN (SELECT  B.BRANCH_ID
                FROM BUSINESS_ENTITY A
                JOIN(SELECT DISTINCT BRANCH_ID, BE_WHERE  FROM V_BE) B ON B.BE_WHERE = A.REGIONAL_ID
                WHERE A.BRANCH_ID = :d) ";
            }
            else
            {
                whereBranch = " AND A.BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = :d) ";
            }

            QueryGlobal q = new QueryGlobal();
            //string whereBranch = q.Query_BE("A");
            //where += whereBranch;

            string whereSearch = search.Length >= 2 ? " AND ( A.CONTRACT_OFFER_NO||UPPER(A.CONTRACT_OFFER_TYPE)||UPPER(CUSTOMER_NAME)||TO_CHAR(CONTRACT_START_DATE,'DD.MM.RRRR')||TO_CHAR(CONTRACT_END_DATE,'DD.MM.RRRR') LIKE '%'||'"+search.ToUpper()+"'||'%' ) " : "";
            where += whereSearch;

            try
            {
                string sql = @"SELECT DISTINCT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, 
                        CASE WHEN COMPLETED_STATUS = 4 THEN (SELECT ATTRIB3 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS')
                        WHEN COMPLETED_STATUS = 3 THEN (SELECT ATTRIB2 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS')
                        ELSE(SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') END AS OFFER_STATUS, 
                        COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, 
                        to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE ,ID_INC
                        FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B 
                        WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO AND COMPLETED_STATUS != 3 " + where + " " + whereBranch +
                    "ORDER BY ID_INC DESC";

                //string sql = @"SELECT DISTINCT A.CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID, A.RENTAL_REQUEST_NO, A.CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, A.CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, 
                //        A.STATUS AS OFFER_STATUS, COMPLETED_STATUS, (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL, 
                //        to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, A.TERM_IN_MONTHS, A.CUSTOMER_AR, A.PROFIT_CENTER_NAME, A.CONTRACT_USAGE, A.CURRENCY, A.CONTRACT_OFFER_TYPE, A.CONTRACT_NUMBER, A.ACTIVE ,ID_INC
                //        FROM V_CONTRACT_OFFER_HEADER A, PROP_WORKFLOW_LOG B 
                //        WHERE A.CONTRACT_OFFER_NO = B.CONTRACT_OFFER_NO " + where + " " +
                //    "ORDER BY ID_INC DESC";

                fullSql = fullSql.Replace("sql", sql);

                listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, d = KodeCabang });

                fullSqlCount = fullSqlCount.Replace("sql", sql);

                count = connection.ExecuteScalar<int>(fullSqlCount, new { d = KodeCabang });
            }
            catch (Exception e)
            {
                throw e;
            }

            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

        //--------------------------------------- NEW DATA TABLE HISTORY ----------------------
        public DataTablesApprovedList GetDataHistoryWorkflownew(int draw, int start, int length, string search, string KodeCabang, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesApprovedList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_APPROVED_LIST_Remote> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            //string wheresearch = (search.Length >= 2 ? " AND ((INSTALLATION_NUMBER LIKE '%' || '" + search.ToUpper() + "' || '%') OR ((POWER_CAPACITY) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (UPPER(INSTALLATION_TYPE) LIKE '%'|| '" + search.ToUpper() + "' ||'%') OR (CUSTOMER_ID LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(INSTALLATION_CODE) LIKE '%' || '" + search.ToUpper() + "' || '%') OR (UPPER(CUSTOMER_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "");
            QueryGlobal query = new QueryGlobal();
            string v_be = query.Query_BE();

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT ID, CONTRACT_OFFER_NO, STATUS_CONTRACT || ' - ' || STATUS_CONTRACT_DESC AS STATUS_CONTRACT, USER_LOGIN || ' | ' || USER_NAME AS USER_LOGIN, BRANCH_ID, TO_CHAR(CREATION_DATE,'DD.MM.RRRR / HH24:MI') CREATION_DATE, CREATION_BY, " +
                             "to_char(LAST_UPDATE_DATE,'DD.MM.YYYY') LAST_UPDATE_DATE, LAST_UPDATE_BY, LOG_STATUS, NOTES, BE_ID, BE_NAME " +
                             "FROM V_HISTORY_WORKFLOW WHERE CONTRACT_OFFER_NO = :c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_APPROVED_LIST_Remote>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {

            }
                
            result = new DataTablesApprovedList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            connection.Dispose();

            return result;
        }

    }
}