﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.PendingList;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class ApprovalEmailDAL
    {
        public IEnumerable<DDOffer> GetDataHeader(string idOffer)
        {
            //string sql = "SELECT RO_CODE, RO_CODE || ' - ' || RO_NAME AS RO_NAME FROM RENTAL_OBJECT WHERE BRANCH_ID=:a ORDER BY RO_CODE DESC";
            string sql = "SELECT ID_INC, CONTRACT_OFFER_NO, BE_ID, RENTAL_REQUEST_NO, CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE, OFFER_STATUS, CUSTOMER_ID, CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME, " +
                                 "to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE " +
                                 "FROM V_CONTRACT_OFFER_HEADER WHERE CONTRACT_OFFER_NO=:a ORDER BY ID_INC DESC";
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDOffer> listDetil = connection.Query<DDOffer>(sql, new { a = idOffer });
            return listDetil;
        }

        public DataTablesPendingList GetDataObject(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {

                try
                {
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        public DataTablesPendingList GetDataCondition(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {

                try
                {
                    string sql = "SELECT CALC_OBJECT, CONDITION_TYPE, to_char(VALID_FROM, 'DD.MM.YYYY') VALID_FROM, to_char(VALID_TO, 'DD.MM.YYYY') VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, to_char(START_DUE_DATE, 'DD.MM.YYYY') START_DUE_DATE, " +
                             "MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, SALES_RULE, TOTAL_NET_VALUE " +
                             "FROM V_CONTRACT_OFFER_CONDITION " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        public DataTablesPendingList GetDataManual(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    string sql = "SELECT MANUAL_NO, CONDITION, to_char(DUE_DATE,'DD.MM.YYYY') DUE_DATE, NET_VALUE, QUANTITY, UNIT " +
                             "FROM V_CONTRACT_OFFER_MANUAL " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID ASC";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }

        public DataTablesPendingList GetDataMemo(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            DataTablesPendingList result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<AUP_PENDING_LIST_WEBAPPS> listDataOperator = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (search.Length == 0)
            {
                try
                {
                    /*
                    string sql = "SELECT OBJECT_ID, OBJECT_TYPE, OBJECT_NAME, to_char(START_DATE, 'DD.MM.YYYY') START_DATE, to_char(END_DATE, 'DD.MM.YYYY') END_DATE, LUAS, LUAS_TANAH, LUAS_BANGUNAN, INDUSTRY " +
                             "FROM V_CONTRACT_OFFER_OBJECT " +
                             "WHERE CONTRACT_OFFER_NO=:c ORDER BY ID DESC";
                     */
                    string sql = "SELECT * FROM PROP_CONTRACT_OFFER_MEMO WHERE CONTRACT_OFFER_NO=:c";


                    fullSql = fullSql.Replace("sql", sql);

                    listDataOperator = connection.Query<AUP_PENDING_LIST_WEBAPPS>(fullSql, new { a = start, b = end, c = id });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = id });
                }
                catch (Exception)
                {

                }
            }
            else
            {
                // Do nothing. Not implemented yet.
            }

            result = new DataTablesPendingList();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            connection.Close();
            return result;
        }
    }
}