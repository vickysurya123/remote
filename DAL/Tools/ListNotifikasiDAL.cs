﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ApprovalSetting;
using Remote.Models.DynamicDatatable;
using Remote.Models.EmailConfiguration;
using Remote.Models.GeneralResult;
using Remote.Models.ListNotifikasi;
using Remote.Models.PendingList;
using Remote.Models.UserModels;

namespace Remote.DAL
{
    public class ListNotifikasiDAL
    {
        #region "Notifikasi Mobile API"
        public Results regFirebase(ParamFirebase param)
        {
            Results res = new Results();
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                var cek = @"select nip from firebase_token where nip = :a and token = :b";
                int count = connection.Query(cek, new { a = param.username, b = param.token_firebase }).Count();
                if (count > 0)
                {
                    var upd = @"update firebase_token set is_active = 1, update_time = sysdate where nip = :a and token = :b";
                    int r = connection.Execute(upd, new { a = param.username, b = param.token_firebase });
                    if (r > 0)
                    {
                        res.Status = "S";
                        res.Msg = "Berhasil";
                    }
                    else
                    {
                        res.Status = "E";
                        res.Msg = "Gagal";
                    }
                }
                else
                {
                    var ins = @"insert into firebase_token (token, device_info, nip, is_active, update_time) 
                                values (:a, :b, :c, :d, :e)";
                    int r = connection.Execute(ins, new {
                        a = param.token_firebase,
                        b = param.device_info,
                        c = param.username,
                        d = 1,
                        e = DateTime.Now
                    });
                    if (r > 0)
                    {
                        res.Status = "S";
                        res.Msg = "Berhasil";
                    }
                    else
                    {
                        res.Status = "E";
                        res.Msg = "Gagal";
                    }
                }
            }
            return res;
        }

        public Results unRegFirebase(ParamFirebase param)
        {
            Results res = new Results();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                var upd = @"update firebase_token set is_active = 0, update_time = sysdate where nip = :a and token = :b";
                int r = connection.Execute(upd, new { a = param.username, b = param.token_firebase });
                if (r > 0)
                {
                    res.Status = "S";
                    res.Msg = "Berhasil";
                }
                else
                {
                    res.Status = "E";
                    res.Msg = "Gagal";
                }
            }
            return res;
        }

        public IEnumerable<ListFirebase> listTokenFirebase(string username)
        {
            List<ListFirebase> res = null;
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                var sql = @"select nip username, token token_firebase from firebase_token where nip = :a and is_active = 1";
                res = connection.Query<ListFirebase>(sql, new {
                    a = username,
                }).ToList();
            }
            return res;
        }

        public Results addNewNotif(ParamNotification param)
        {
            Results res = new Results();
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                var sql = @"insert into notification_center (nip, type, notification, no_contract, user_pengirim, update_time, is_read)
                            values (:a, :b, :c, :d, :e, :f, :g)";
                int r = connection.Execute(sql, new { 
                    a = param.username,
                    b = param.type,
                    c = param.notification,
                    d = Convert.ToInt32(param.no_contract),
                    e = param.pengirim,
                    f = DateTime.Now,
                    g = 0,
                });
                if(r > 0)
                {
                    res.Status = "S";
                    res.Msg = "Berhasil";
                }
                else
                {
                    res.Status = "E";
                    res.Msg = "Gagal";
                }
            }
            return res;
        }

        public ResultsNotif getNotifByID(string username, string last_id)
        {
            ResultsNotif res = new ResultsNotif();
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    var sql = @"select no_contract, user_pengirim pengirim, notification, type, id, nip username from notification_center where nip = :a and id > :b";
                    res.data = connection.Query<ListNotif>(sql, new { a = username, b = last_id }).ToList();
                    var upd = @"update notification_center set is_read = 1 where nip = :a";
                    connection.Execute(upd, new { a = username });
                    res.status = "S";
                    res.message = "sukses";
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }

        public ResultsNotif getNotifNotRead(string username)
        {
            ResultsNotif res = new ResultsNotif();
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    var sql = @"select type, notification, no_contract, user_pengirim pengirim, id, nip username from notification_center where nip = :a and is_read = 0";
                    res.data = connection.Query<ListNotif>(sql, new { a = username }).ToList();
                    var upd = @"update notification_center set is_read = 1 where nip = :a";
                    connection.Execute(sql, new { a = username });
                    res.status = "S";
                    res.message = "sukses";
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }

        public ResultsNotif getCountDisposisi(string username, string user_id, string role_id)
        {
            ResultsNotif res = new ResultsNotif();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    int hitung = 0;

                    var sql = "SELECT COUNT(*) FROM ( " +
                        "SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, " +
                        //                        "A.ISI_SURAT," +
                        " B.CONTRACT_OFFER_TYPE OFFER_TYPE, " +
                        "   TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, " +
                        "   TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, B.CUSTOMER_NAME, " +
                        "   C.IS_CREATE_SURAT, IS_ACTION_DISPOSISI, A.STATUS_KIRIM, C.ROLE_ID, 1 EDIT,c.IS_ACTION, C.USER_ID " +
                        "FROM PROP_SURAT A " +
                        "LEFT JOIN V_CONTRACT_OFFER_HEADER B ON (B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO) " +
                        "LEFT JOIN (SELECT A.USER_ID, A.SURAT_ID,A.CONTRACT_NO, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION " +
                        "FROM PROP_DISPOSISI_PARAF A  WHERE A.TYPE = 'disposisi' " +
                        "GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.IS_CREATE_SURAT, ROLE_ID, IS_ACTION_DISPOSISI, IS_ACTION) " +
                        "C ON C.CONTRACT_NO = A.CONTRACT_NO AND c.SURAT_ID = A.ID " +
                        "UNION ALL " +
                        "SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT," +
                        //                        " A.ISI_SURAT, " +
                        "B.CONTRACT_OFFER_TYPE OFFER_TYPE, " +
                        " TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, " +
                        "    TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, B.CUSTOMER_NAME, " +
                        "    1 IS_CREATE_SURAT, 0 IS_ACTION_DISPOSISI, A.STATUS_KIRIM,  NULL ROLE_ID, 0 EDIT, " +
                        "    1 IS_ACTION, A.USER_ID_CREATE USER_ID " +
                        " FROM PROP_SURAT A " +
                        " LEFT JOIN V_CONTRACT_OFFER_HEADER B ON (B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO) " +
                        "                   ) Z WHERE (Z.USER_ID = :c OR Z.ROLE_ID =:d) AND (((IS_ACTION_DISPOSISI IS NULL OR IS_ACTION_DISPOSISI = 0) AND IS_CREATE_SURAT = 0) OR (IS_CREATE_SURAT = 1 AND IS_ACTION = 0) OR (IS_CREATE_SURAT = 1 AND IS_ACTION = 1 AND STATUS_KIRIM = 'draft')) ORDER BY Z.ID DESC";
                    hitung = connection.ExecuteScalar<int>(sql, new
                    {
                        c = user_id,
                        d = role_id,
                    });

                    res.status = "S";
                    res.message = hitung.ToString();
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }

        public ResultsNotif getCountParaf(string username, string user_id, string role_id)
        {
            ResultsNotif res = new ResultsNotif();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    int hitung = 0;

                    var sql = @"SELECT COUNT(*) FROM (SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, 
                        B.CONTRACT_OFFER_TYPE OFFER_TYPE,
                        TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                            TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, b.CUSTOMER_NAME, 
                            B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL, A.STATUS_APV, A.COUNT_SURAT
                        FROM PROP_SURAT A
                        JOIN V_CONTRACT_OFFER_HEADER B ON(B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO)
                        LEFT JOIN(SELECT A.USER_ID, A.SURAT_ID, A.CONTRACT_NO, A.URUTAN, A.IS_ACTION, A.IS_ACTION_DISPOSISI FROM PROP_DISPOSISI_PARAF A
                        WHERE A.TYPE = 'surat' GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.URUTAN, A.IS_ACTION, A.IS_ACTION_DISPOSISI) C ON C.SURAT_ID = A.ID AND C.CONTRACT_NO = A.CONTRACT_NO
                        WHERE A.COUNT_SURAT = 1 AND C.USER_ID = :c
                        UNION ALL
                        SELECT A.ID,A.NO_SURAT,A.CONTRACT_NO, A.SUBJECT, 
                        B.CONTRACT_OFFER_TYPE OFFER_TYPE,
                            TO_CHAR(B.CONTRACT_START_DATE, 'DD/MM/YYYY') CONTRACT_START_DATE, 
                            TO_CHAR(B.CONTRACT_END_DATE, 'DD/MM/YYYY') CONTRACT_END_DATE, b.CUSTOMER_NAME, 
                            B.PARAF_LEVEL, B.DISPOSISI_PARAF, C.URUTAN, C.IS_ACTION, A.DISPOSISI_MAX, A.APV_LEVEL, A.STATUS_APV, A.COUNT_SURAT
                        FROM PROP_SURAT A
                        JOIN V_CONTRACT_OFFER_HEADER B ON(B.CONTRACT_NUMBER = A.CONTRACT_NO OR B.CONTRACT_OFFER_NO = A.CONTRACT_NO)
                        LEFT JOIN(SELECT A.USER_ID, A.SURAT_ID, A.CONTRACT_NO, A.URUTAN, A.IS_ACTION, IS_ACTION_DISPOSISI FROM PROP_DISPOSISI_PARAF A
                        WHERE A.TYPE = 'disposisi' AND A.ROLE_ID IS NULL AND A.IS_CREATE_SURAT = 0 GROUP BY A.USER_ID, A.CONTRACT_NO, A.SURAT_ID, A.URUTAN, A.IS_ACTION, IS_ACTION_DISPOSISI) C ON C.CONTRACT_NO = A.CONTRACT_NO
                        WHERE A.COUNT_SURAT > 1 AND A.DISPOSISI_MAX IS NOT NULL AND C.USER_ID = :c) Z
                        WHERE(Z.IS_ACTION = 0 AND Z.URUTAN = APV_LEVEL AND STATUS_APV = 0 AND z.DISPOSISI_MAX > 0) OR(Z.IS_ACTION = 0 AND COUNT_SURAT = 1 AND Z.PARAF_LEVEL = Z.URUTAN) ORDER BY Z.ID DESC";
                    hitung = connection.ExecuteScalar<int>(sql, new
                    {
                        c = user_id,
                    });

                    res.status = "S";
                    res.message = hitung.ToString();
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }

        public ResultsNotif getCountApproval(string username, string user_id, string role_id, string propertyRole, string kdcabang)
        {
            ResultsNotif res = new ResultsNotif();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    int hitung = 0;

                    String whereProperty = (role_id == "4") ? "" : " and APPROVAL_NEXT_LEVEL = '" + propertyRole + "' ";

                    String whereBranch = "";
                    if (propertyRole == "50")
                    {
                        whereBranch = " AND BRANCH_ID IN (SELECT  B.BRANCH_ID " +
                        " FROM BUSINESS_ENTITY A " +
                        " JOIN(SELECT DISTINCT BRANCH_ID, BE_WHERE  FROM V_BE) B ON B.BE_WHERE = A.REGIONAL_ID " +
                        " WHERE A.BRANCH_ID = '" + kdcabang + "') ";
                    }
                    else
                    {
                        whereBranch = " AND BRANCH_ID IN (SELECT DISTINCT BRANCH_ID FROM V_BE WHERE BRANCH_WHERE = '"+ kdcabang + "') ";
                    }

                    var sql = "SELECT count(*)  " +
                    //", CONTRACT_OFFER_NO, BE_ID || ' - ' || BE_NAME AS BE_ID " +
                    //"   , RENTAL_REQUEST_NO " +
                    //"           , CONTRACT_OFFER_NAME, to_char(CONTRACT_START_DATE,'DD.MM.YYYY') CONTRACT_START_DATE " +
                    //"   , CUSTOMER_ID " +
                    //"   , CUSTOMER_ID || ' - ' || CUSTOMER_NAME AS CUSTOMER_NAME " +
                    //"           , (SELECT ATTRIB1 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE OFFER_STATUS = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') OFFER_STATUS " +
                    //"   , (SELECT ATTRIB4 AS OFFER_STATUS FROM PROP_PARAMETER_REF_D WHERE TOP_APPROVED_LEVEL = REF_DATA AND REF_CODE = 'CONTRACT_OFFER_STATUS') TOP_APPROVED_LEVEL " +
                    //"   , to_char(CONTRACT_END_DATE,'DD.MM.YYYY') CONTRACT_END_DATE " +
                    //"   , TERM_IN_MONTHS, CUSTOMER_AR, PROFIT_CENTER_NAME, CONTRACT_USAGE, CURRENCY, CONTRACT_OFFER_TYPE, CONTRACT_NUMBER, ACTIVE, APV_DISPOSISI, APPROVAL_NEXT_LEVEL " +
                    "FROM V_CONTRACT_OFFER_HEADER " +
                    "WHERE COMPLETED_STATUS = '2' AND APV_DISPOSISI = 1" + whereBranch + whereProperty;
                    hitung = connection.ExecuteScalar<int>(sql);

                    res.status = "S";
                    res.message = hitung.ToString();
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }

        public ResultsNotif getCountNotif(string username)
        {
            ResultsNotif res = new ResultsNotif();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    int hitung = 0;


                    var sql = @"select count (*)
                        from NOTIFICATION_CENTER A
                        WHERE A.NIP = '" + username + "' AND A.IS_READ = 0";
                    hitung = connection.ExecuteScalar<int>(sql);

                    res.status = "S";
                    res.message = hitung.ToString();
                }
                catch (Exception e)
                {
                    res.status = "E";
                    res.message = e.Message;
                    res.data = null;
                }
            }
            return res;
        }
        public ResultsNotif getAllNotif(string username, int start, int length)
        {
            int end = start + length;
            ResultsNotif result = new ResultsNotif();
            using(IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    string fullSql =
                       "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    var sql = @"select A.ID, A.NIP USERNAME, A. TYPE, A.NOTIFICATION, A.NO_CONTRACT, A.USER_PENGIRIM PENGIRIM,
								CASE WHEN A.TYPE = '1' THEN 'Permohonan Approval '
                                    WHEN A.TYPE = '2' THEN 'Menerima Disposisi'  
                                    WHEN A.TYPE = '3' THEN 'Permohonan Paraf' 
                                    WHEN A.TYPE = '4' THEN 'Informasi Void' 
                                    WHEN A.TYPE = '5' THEN 'Informasi Revise'
                                    WHEN A.TYPE = '6' THEN 'Melakukan Disposisi'
                                    ELSE '' END ||' dari '|| C.USER_NAME ||' untuk kontrak '||a.NO_CONTRACT||' dengan memo '||a.NOTIFICATION
                                    DESKRIPSI,
                                CASE WHEN A.TYPE = '1' THEN 'APPROVAL'
                                    WHEN A.TYPE = '2' THEN 'DISPOSISI'  
                                    WHEN A.TYPE = '3' THEN 'PARAF' 
                                    WHEN A.TYPE = '4' THEN 'VOID' 
                                    WHEN A.TYPE = '5' THEN 'REVISE'
                                    WHEN A.TYPE = '6' THEN 'DISPOSISI'
                                    ELSE '' END JUDUL,
                                B.USER_NAME NAMA_PENERIMA, C.USER_NAME NAMA_PENGIRIM, A.IS_READ
                                from NOTIFICATION_CENTER A
                                JOIN APP_REPO.APP_USER B on A.NIP = B.USER_LOGIN
                                JOIN APP_REPO.APP_USER C on A.USER_PENGIRIM = C.USER_LOGIN
                                WHERE A.NIP = :c
                                ORDER BY A.ID desc";
                    fullSql = fullSql.Replace("sql", sql);
                    result.data = connection.Query<ListNotif>(fullSql, new { a = start, b = end, c = username  }).ToList();
                    result.status = "S";
                    result.message = "Sukses";
                }
                catch(Exception e)
                {
                    result.status = "E";
                    result.message = e.Message;
                    result.data = null;
                }
            }
            return result;
        }

        public Results readNotif(string username, string no_contract)
        {
            Results res = new Results();
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    var sql = @"update notification_center set is_read = 1 where nip = :a and no_contract = :b";
                    int r = connection.Execute(sql, new
                    {
                        a = username,
                        b = no_contract,
                    });
                    if (r > 0)
                    {
                        res.Status = "S";
                        res.Msg = "Berhasil";
                    }
                    else
                    {
                        res.Status = "E";
                        res.Msg = "Gagal";
                    }
                }
                catch (Exception e)
                {
                    res.Status = "E";
                    res.Msg = e.Message;
                }
            }
            return res;
        }
        #endregion

        public IDataTable GetData(int draw, int start, int length, string search, string kodecabang)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            string where = @" AND b.BRANCH_ID IN (
                                  select distinct branch_where as branch_id from v_be where branch_id = :d
                            union select distinct branch_id from v_be where branch_where = :d) ";
            where += search.Length >= 2 ? " and ((UPPER(b.BE_NAME) LIKE '%' || '" + search.ToUpper() + "' || '%')) " : "";

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                
                //search filter data
                string sql = @"select a.ID,MODUL_ID,a.BE_ID, b.BE_NAME, b.BRANCH_ID from LIST_NOTIFICATION_HEADER a
                                join BUSINESS_ENTITY b on a.BE_ID = b.BE_ID
                                WHERE 1 = 1 " + where + " ORDER BY  a.ID DESC";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<LIST_NOTIFIKASI_HEADER>(fullSql, new
                {
                    a = start,
                    b = end,
                    d = kodecabang
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount, new
                {
                    d = kodecabang
                });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public LIST_NOTIFIKASI_HEADER GetData(string id)
        {
            //string xCount = "";
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            LIST_NOTIFIKASI_HEADER result = new LIST_NOTIFIKASI_HEADER();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = @"select a.ID, a.MODUL_ID,a.BE_ID, b.BE_NAME from LIST_NOTIFICATION_HEADER a
                                 join BUSINESS_ENTITY b on a.BE_ID = b.BE_ID where a.ID =:a";

                result = connection.Query<LIST_NOTIFIKASI_HEADER>(sql, new { a = id }).FirstOrDefault();

            }
            catch (Exception)
            {

                throw;
            }


            connection.Close();
            return result;
        }

        public Results AddDataHeader(DataListNotifikasiHeader data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"select BE_ID from BUSINESS_ENTITY a where a.BRANCH_ID = :a ";
                //string sql = @"select BE_ID from BUSINESS_ENTITY a ";
                var be_id = connection.ExecuteScalar<int>(sql, new
                {
                    a = data.BE_ID
                    //a = 11
                });

                sql = @"select count(ID) from LIST_NOTIFICATION_HEADER a where a.BE_ID = :a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = be_id
                });

                if (r > 0)
                {
                    result.Msg = "Approval Setting for Business Entity Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into LIST_NOTIFICATION_HEADER (BE_ID, MODUL_ID, CREATED_DATE, CREATED_BY)
                            values(:a, :b, :c, :d)";
                    r = connection.Execute(sql, new
                    {
                        a = be_id,
                        //b = data.MODUL_ID,
                        b = 11,
                        c = DateTime.Now,
                        d = name,

                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Added Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results AddDataDetail(DataListNotifikasiDetail data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "";
                sql = "select max(ID) from LIST_NOTIFICATION_HEADER";
                var id_header = connection.ExecuteScalar<int>(sql, new { });
                sql = @"insert into LIST_NOTIFICATION_DETAIL_ROW (APPROVAL_NO,ROLE_ID,HEADER_ID,ROLE_NAME,CREATED_DATE,CREATED_BY) 
                            values(:b, :c, :e, :f, :g, :h)";
                //foreach (ListRoleRow temp in data.Role)
                //{
                //    r = connection.Execute(sql, new
                //    {
                //        b = temp.APPROVAL_NO,
                //        c = temp.ROLE_ID,
                //        e = id_header,
                //        f = temp.ROLE_NAME,
                //        g = DateTime.Now,
                //        h = name,
                //    });
                //}

                if (r > 0)
                {
                    result.Msg = "Data Added Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
            
        }

        public IDataTable GetViewDetail(int draw, int start, int length, string search, string id)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                if (search.Length == 0)
                {
                    string sql = @"select APPROVAL_NO,PROPERTY_ROLE,ROLE_ID,HEADER_ID,ROLE_NAME from LIST_NOTIFICATION_DETAIL_ROW a
                                   where a.HEADER_ID = :id order by a.APPROVAL_NO asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<ListRoleRow>(fullSql, new { a = start, b = end, id = id }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { id = id });
                }
                else
                {
                    //search filter data
                    string sql = @"select APPROVAL_NO,PROPERTY_ROLE,ROLE_ID,HEADER_ID,ROLE_NAME from LIST_NOTIFICATION_DETAIL_ROW a
                                    WHERE a.HEADER_ID =:id order by a.APPROVAL_NO asc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<ListRoleRow>(fullSql, new
                    {
                        a = start,
                        b = end,
                        c = search.ToUpper(),
                        id = id
                    }).ToList();
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { c = search });
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public IEnumerable<ListRoleRow> GetDataDetailRow(string id)
        {
            string sql = @"select ID, APPROVAL_NO,PROPERTY_ROLE,ROLE_ID,HEADER_ID,ROLE_NAME from LIST_NOTIFICATION_DETAIL_ROW a
                        where a.HEADER_ID = :id order by a.APPROVAL_NO asc";

            //IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection(); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<ListRoleRow> listDetil = connection.Query<ListRoleRow>(sql, new
            {
                //id = id
                id = id
            });
            connection.Close();
            return listDetil;
        }

        public Results EditDataDetail(DataListNotifikasiDetail data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                var sqlinsert = @"insert into LIST_NOTIFICATION_DETAIL_ROW (APPROVAL_NO,ROLE_ID,ROLE_NAME,UPDATED_DATE,UPDATED_BY,HEADER_ID) 
                        values( :b, :c, :d, :e, :f, :g)";
                var sqlupdate = @"update LIST_NOTIFICATION_DETAIL_ROW set APPROVAL_NO=:b, ROLE_ID=:c, ROLE_NAME=:f, UPDATED_DATE=:h, UPDATED_BY=:i, HEADER_ID=:j where ID =:g";
                foreach (ListRoleRow temp in data.Role)
                {
                    //sql = (temp.ID == null ? sqlinsert : sqlupdate);
                    if (temp.ID == null)
                    {
                        r = connection.Execute(sqlinsert, new
                        {
                            b = temp.APPROVAL_NO,
                            c = temp.ROLE_ID,
                            d = temp.ROLE_NAME,
                            e = DateTime.Now,
                            f = name,
                            g = temp.HEADER_ID
                        });
                    }
                    else
                    {
                        r = connection.Execute(sqlupdate, new
                        {
                            b = temp.APPROVAL_NO,
                            c = temp.ROLE_ID,
                            f = temp.ROLE_NAME,
                            h = DateTime.Now,
                            i = name,
                            j = temp.HEADER_ID,
                            g = temp.ID
                        });
                    }
                }

                if (data.delRole != null)
                {
                    sqlupdate = "delete from LIST_NOTIFICATION_DETAIL_ROW where ID =:a";
                    foreach (ListRoleRow temp in data.delRole)
                    {
                        r = connection.Execute(sqlupdate, new
                        {
                            a = temp.ID
                        }); ;
                    }
                }

                if (r > 0)
                { 

                    result.Msg = "Data Updated Successfully";
                    result.Status = "S";

                }
                else
                {
                    result.Msg = "Failed to Update Data";
                    result.Status = "E";

                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public IEnumerable<DDPosition> GetDataListPosition()
        {
            string sql = "SELECT ROLE_ID, ROLE_NAME, PROPERTY_ROLE FROM APP_ROLE WHERE APP_ID=1 ORDER BY ROLE_NAME ASC";

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            //IDbConnection connection = DatabaseFactory.GetConnection(); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDPosition> listDetil = connection.Query<DDPosition>(sql);
            connection.Close();
            return listDetil;
        }

        public Results EditDataHeader(DataListNotifikasiHeader data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"select BE_ID from BUSINESS_ENTITY a where a.BRANCH_ID = :a ";
                var be_id = connection.ExecuteScalar<int>(sql, new
                {
                    //a = data.BE_ID
                    a = 1
                });

                sql = @"select count(ID) from APPROVAL_SETTING_HEADER a where a.BE_ID = :a ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    a = be_id
                });

                if (r > 0)
                {
                    result.Msg = "Approval Setting for Business Entity Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"update APPROVAL_SETTING_HEADER a set a.BE_ID =:a,  a.MODUL_ID =:b, a.UPDATED_DATE =:d, a.UPDATED_BY =:e where a.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        a = be_id,
                        //b = data.MODUL_ID,
                        //c = data.ID,
                        d = DateTime.Now,
                        e = name
                    });
                    if (r > 0)
                    {
                        result.Msg = "Data Updated Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Update Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public IEnumerable<DDBranch> GetDataListBranch(object kodecabang)
        {
            string where = (kodecabang == "0" ? "" : " where BRANCH_ID = " + kodecabang);
            string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY  ORDER BY BE_NAME ASC";
            //string sql = "SELECT BRANCH_ID, BE_NAME FROM BUSINESS_ENTITY " + where + " ORDER BY BE_NAME ASC";

            //IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection(); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql);
            connection.Close();
            return listDetil;
        }

        public Results DeleteDataHeader(DataListNotifikasiDetail data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    var sql = @"delete from LIST_NOTIFICATION_DETAIL_ROW a where a.HEADER_ID =:c";
                    r = connection.Execute(sql, new
                    {
                        //c = data.ID
                        c = 11
                    }, transaction: transaction);

                    sql = @"delete from LIST_NOTIFICATION_HEADER a where a.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        //c = data.ID
                        c = 11
                    }, transaction: transaction);

                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Deleted Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Delete Data";
                        result.Status = "E";

                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public IEnumerable<ListRoleRow> GetUserNotification(int kodeCabang, int apvDetailId, int propRole)
        {
            // CHECK RULE CABANG
            IEnumerable<ListRoleRow> res = null;
            int approval_no = 0;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string cabangOnly = "SELECT DISTINCT BE_WHERE FROM V_BE WHERE BRANCH_ID = :branch AND BE_GROUP_ONLY = 1 ";
                string regionalOnly = "SELECT DISTINCT BE_WHERE FROM V_BE WHERE BRANCH_ID = :branch AND REG_GROUP_ONLY = 1 ";
                string pusatOnly = "SELECT DISTINCT BE_WHERE FROM V_BE WHERE BRANCH_ID = :branch AND PUSAT_GROUP_ONLY = 1 ";
                string whereSql = string.Empty;
                string sqlSearch = @"SELECT b.APPROVAL_NO FROM V_APPROVAL_SETTING a
                            JOIN V_LIST_NOTIFICATION b on a.ROLE_ID = b.ROLE_ID
                            WHERE a.DETAIL_ID = :detail 
                            AND a.PROPERTY_ROLE = :prop 
                            AND b.BE_ID IN (whereSql)";
                string sql = sqlSearch.Replace("whereSql", cabangOnly);
                approval_no = connection.ExecuteScalar<int>(sql, new {
                    branch = kodeCabang,
                    prop = propRole,
                    detail = apvDetailId
                });

                // IF CABANG EXIST
                if (approval_no > 0)
                {
                    whereSql = cabangOnly;
                }
                else
                {
                    // ELSE CHECK RULE REGIONAL
                    sql = sqlSearch.Replace("whereSql", regionalOnly);
                    approval_no = connection.ExecuteScalar<int>(sql, new
                    {
                        branch = kodeCabang,
                        prop = propRole,
                        detail = apvDetailId
                    });

                    // IF REGIONAL EXIST
                    if (approval_no > 0)
                    {
                        whereSql = regionalOnly;
                    }
                    else
                    {
                        // ELSE CHECK RULE PUSAT
                        sql = sqlSearch.Replace("whereSql", pusatOnly);
                        approval_no = connection.ExecuteScalar<int>(sql, new
                        {
                            branch = kodeCabang,
                            prop = propRole,
                            detail = apvDetailId
                        });

                        if (approval_no > 0)
                        {
                            whereSql = pusatOnly;
                        }
                    }
                }


                if (approval_no > 0 && ! string.IsNullOrEmpty(whereSql))
                {
                    string sqlTemplate = @"SELECT ID, ROLE_ID, HEADER_ID, ROLE_NAME, PROPERTY_ROLE, APPROVAL_NO 
                                        FROM V_LIST_NOTIFICATION 
                                        WHERE BE_ID IN (whereSql) AND APPROVAL_NO < :no";
                    sql = sqlTemplate.Replace("whereSql", whereSql);
                    res = connection.Query<ListRoleRow>(sql, new { branch = kodeCabang, no = approval_no });
                }
            }
            catch (Exception e) { string aaa = e.ToString(); };

            connection.Close();
            return res;
        }

        public Results SendNotification(Results result, int userRoleId, string kodeCabang, AUP_TRANS_CONTRACT_OFFER_WEBAPPS data, string type, ContractOfferApprovalParameter param)
        {
            EmailConfigurationDAL dal = new EmailConfigurationDAL();
            var subject = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Released" : (data.COMPLETED_STATUS == "2" ? "Approved" : data.COMPLETED_STATUS == "3" ? "Revised" : "Rejected"));
            var approval = (data.COMPLETED_STATUS == "2" && data.OFFER_STATUS == "20" ? "Perlu dilakukan proses Approval atas nomor permohonan tersebut<br>" : (data.COMPLETED_STATUS == "2" ? "Mohon dilakukan Approval atas nomor permohonan tersebut<br>" : "<br>"));
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DataUser> res = null;

            String sql = @"select a.KD_CABANG, a.ROLE_NAME, a.STATUS, a.USER_ROLE_ID, a.USER_EMAIL, a.USER_NAME from VW_APP_USER_A a
                                join REMOTE.BUSINESS_ENTITY b on a.REGIONAL_ID = b.REGIONAL_ID
                                where a.USER_ROLE_ID = :a and b.BRANCH_ID in (
                                    select distinct branch_where from v_be where branch_id = :b
                                ) and a.APP_ID = 1 and a.STATUS = 1";
            res = connection.Query<DataUser>(sql, new
            {
                a = userRoleId,
                b = kodeCabang
            });
            if (res.Count() > 0)
            {
                foreach (DataUser temp in res)
                {
                    if (temp.USER_EMAIL != null)
                    {
                        var body = dal.bodyheader(temp.ROLE_NAME, data, type, param);
                        body += dal.bodydetail(data.CONTRACT_OFFER_NO);
                        body += dal.link;
                        body += approval;
                        body += dal.DataCabang(kodeCabang);
                        body += dal.emailfooter();
                        //send email
                        EmailConf email = new EmailConf();
                        email.EMAIL_TO = temp.USER_EMAIL;//"hafidz.lazuardy@gmail.com";// "guntur.herdiawan@pelindo.co.id"; //
                        email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                        email.BODY = body;
                        email.DESC = "name : " + temp.USER_NAME + ", email : " + temp.USER_EMAIL;
                        email.SUBJECT = "[" + subject + "] Permohonan Kontrak offer no." + data.CONTRACT_OFFER_NO;
                        var sendingemail = dal.SendMail(email);
                    }

                }
                result.Msg = "Sukses kirim email. ";
                result.Status = "S";
            }
            else
            {
                result.Msg = "Tidak Ada User";
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public string getNamaDariNipp(string username)
        {
            string res = "";
            using (IDbConnection connection = DatabaseFactory.GetConnection())
            {
                try
                {
                    var sql = @"SELECT USER_NAME from APP_REPO.APP_USER 
	                            WHERE USER_LOGIN = :a";
                    res = connection.ExecuteScalar<string>(sql, new
                    {
                        a = username,
                    });
                }
                catch (Exception e)
                {
                    res = "-";
                }
            }
            return res;
        }
    }
}