﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.MasterBusinessEntity;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class MasterBusinessEntityDAL
    {
        //------------------------- DATA TABLE BE ------------------------------//
        public DataTablesBusinessEntity GetDataBE(int draw, int start, int length, string search, string KodeCabang)
        {
            int count = 0;
            int end = start + length;
            DataTablesBusinessEntity result = null;
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            IEnumerable<AUP_BE_WEBAPPS> listDataOperator = null;
            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {

                    if (KodeCabang == "0")
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                                    "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                                    "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                                    "c.PHONE_1, c.FAX_1, c.PHONE_2, c.FAX_2, c.EMAIL " +
                                    "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID ORDER BY b.ID DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_BE_WEBAPPS>(fullSql, new {a = start, b = end});

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount);
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            //search filter data
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                                        "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                                        "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'MM/DD/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'MM/DD/YYYY') AS VAL_TO, " +
                                        "c.PHONE_1, c.FAX_1, c.EMAIL " +
                                        "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID AND " +
                                        "((b.BE_ID LIKE '%'||:c||'%') OR (UPPER(b.BE_NAME) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_ADDRESS) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_CITY) LIKE '%' ||:c|| '%') OR (UPPER((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID)) LIKE '%' ||:c|| '%'))";

                                    fullSql = fullSql.Replace("sql", sql);

                                    listDataOperator = connection.Query<AUP_BE_WEBAPPS>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper()
                                    });

                                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                                    count = connection.ExecuteScalar<int>(fullSqlCount, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper()
                                    });
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        if (search.Length == 0)
                        {
                            try
                            {
                                string sql =
                                    "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                                    "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                                    "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                                    "c.PHONE_1, c.FAX_1, c.PHONE_2, c.FAX_2, c.EMAIL " +
                                    "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID AND BRANCH_ID=:c ORDER BY b.ID DESC";

                                fullSql = fullSql.Replace("sql", sql);

                                listDataOperator = connection.Query<AUP_BE_WEBAPPS>(fullSql,
                                    new {a = start, b = end, c = KodeCabang});

                                fullSqlCount = fullSqlCount.Replace("sql", sql);

                                count = connection.ExecuteScalar<int>(fullSqlCount, new {c = KodeCabang});
                            }
                            catch (Exception)
                            {

                            }
                        }
                        else
                        {
                            //search filter data
                            if (search.Length > 2)
                            {
                                try
                                {
                                    string sql =
                                        "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                                        "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                                        "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'MM/DD/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'MM/DD/YYYY') AS VAL_TO, " +
                                        "c.PHONE_1, c.FAX_1, c.EMAIL " +
                                        "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID AND " +
                                        "((b.BE_ID LIKE '%'||:c||'%') OR (UPPER(b.BE_NAME) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_ADDRESS) LIKE '%' ||:c|| '%') OR (UPPER(b.BE_CITY) LIKE '%' ||:c|| '%') OR (UPPER((SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID)) LIKE '%' ||:c|| '%')) AND BRANCH_ID=:d";

                                    fullSql = fullSql.Replace("sql", sql);

                                    listDataOperator = connection.Query<AUP_BE_WEBAPPS>(fullSql, new
                                    {
                                        a = start,
                                        b = end,
                                        c = search.ToUpper(),
                                        d = KodeCabang
                                    });

                                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                                    count = connection.ExecuteScalar<int>(fullSqlCount, new {c = search});
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                }


                result = new DataTablesBusinessEntity();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataOperator.ToList();

                //connection.Close();
                return result;
            }
        }

        //------------------------------- DATA TABLE MEASUREMENT --------------------------
        public DataTablesMeasurement GetDataMeasurement(int draw, int start, int length, string search, string idBE)
        {
            int count = 0;
            int end = start + length;
            DataTablesMeasurement result = null;
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();

            IEnumerable<AUP_BE_WEBAPPS> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    string fullSqlCount = "SELECT count(*) FROM (sql)";

                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT m.ID, BE_ID, AMOUNT, UNIT, to_char(VALID_FROM,'DD.MM.YYYY') AS VALID_FROM," +
                                 "to_char(VALID_TO,'DD.MM.YYYY') AS VALID_TO, to_char(VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                                 "(SELECT REF_DATA FROM PROP_PARAMETER_REF_D WHERE m.MEASUREMENT_TYPE = ID) MEASUREMENT_TYPE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE m.MEASUREMENT_TYPE = ID) DESCRIPTION, m.ACTIVE " +
                                 "FROM BUSINESS_ENTITY_DETAIL m WHERE BE_ID=:c";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_BE_WEBAPPS>(fullSql, new { a = start, b = end, c = idBE });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idBE });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        // Do nothing. Not implemented yet.
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            

            result = new DataTablesMeasurement();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

           // connection.Close();
            return result;
        }

        //------------------------------- DATA TABLE ATTACHMENT --------------------------
        public DataTablesAttachment GetDataAttachment(int draw, int start, int length, string search, string idBE)
        {
            int count = 0;
            int end = start + length;
            DataTablesAttachment result = null;

            IEnumerable<AUP_BE_ATTACHMENT> listDataOperator = null;
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    string fullSqlCount = "SELECT count(*) FROM (sql)";

                    if (search.Length == 0)
                    {
                        try
                        {
                            string sql = "SELECT ID_ATTACHMENT, DIRECTORY, FILE_NAME, BE_ID FROM BUSINESS_ENTITY_ATTACHMENT WHERE BE_ID=:c ORDER BY ID_ATTACHMENT DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_BE_ATTACHMENT>(fullSql, new { a = start, b = end, c = idBE });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new { c = idBE });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        // Do nothing. Not implemented yet.
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }


            result = new DataTablesAttachment();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            // connection.Close();
            return result;
        }

        //------------------------------ VIEW EDIT DATA BE --------------------------------
        public AUP_BE_WEBAPPS GetDataForEdit(string id)
        {
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            AUP_BE_WEBAPPS result = new AUP_BE_WEBAPPS();
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "SELECT b.BE_ID, b.BE_NAME, b.BE_CITY, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.BE_PROVINCE = ID) BE_PROVINCE, " +
                         "b.BE_ADDRESS, b.BE_CONTACT_PERSON, b.BE_NPWP, b.BE_PPKP_DATE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE b.HARBOUR_CLASS = ID) HARBOUR_CLASS,  b.HARBOUR_CLASS HB_ID, b.BE_PROVINCE BE_PROVINCE_ID," +
                         "b.POSTAL_CODE, to_char(b.VALID_FROM,'DD.MM.YYYY') VALID_FROM, to_char(b.VALID_TO, 'DD.MM.YYYY') VALID_TO, to_char(b.VALID_FROM, 'DD/MM/YYYY') AS VAL_FROM, to_char(b.VALID_TO, 'DD/MM/YYYY') AS VAL_TO, " +
                         "c.PHONE_1, c.FAX_1, c.PHONE_2, c.FAX_2, c.EMAIL " +
                         "FROM BUSINESS_ENTITY b, BE_CONTACT_DETAIL c WHERE c.BE_ID(+) = b.BE_ID AND b.BE_ID = :a";

                     result = connection.Query<AUP_BE_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

                }
                catch (Exception)
                {

                    throw;
                }
            }
            
            //connection.Close();
            return result;
        }

        //--------------------------------- VIEW EDIT DATA MEASUREMENT -----------------------------------
        public AUP_BE_WEBAPPS GetDataForMeasurement(string id)
        {
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            AUP_BE_WEBAPPS result = new AUP_BE_WEBAPPS();
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "SELECT be_id, be_code, be_name, be_address, be_city, be_province, " +
                        "be_contact_person, be_npwp, be_noppkp, be_ppkp_date, creation_by, creation_date, last_update_by, last_update_date, " +
                        "valid_from, valid_to, harbour_class, postal_code " +
                        "FROM V_BE " +
                         "WHERE BE_ID = :a";


                    /*
                    string sql = "SELECT m.ID, BE_ID, AMOUNT, UNIT, to_char(VALID_FROM,'DD.MM.YYYY') AS VALID_FROM," +
                                 "to_char(VALID_TO,'DD.MM.YYYY') AS VALID_TO, to_char(VALID_FROM, 'MM/DD/YYYY') AS VAL_FROM, to_char(VALID_TO, 'MM/DD/YYYY') AS VAL_TO, " +
                                 "(SELECT REF_CODE FROM PROP_PARAMETER_REF_D WHERE m.MEASUREMENT_TYPE = ID) MEASUREMENT_TYPE, (SELECT REF_DESC FROM PROP_PARAMETER_REF_D WHERE m.MEASUREMENT_TYPE = ID) DESCRIPTION " +
                                 "FROM BUSINESS_ENTITY_DETAIL m";
                     * */

                     result = connection.Query<AUP_BE_WEBAPPS>(sql, new { a = id }).FirstOrDefault();
                    connection.Close();
                }
                catch (Exception)
                {

                    throw;
                }
                finally { connection.Close();}
            }
            
            return result;
        }

        public dynamic UbahStatusMeasurement(string id)
        {
            dynamic result = null;

            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "SELECT ACTIVE " +
                "FROM BUSINESS_ENTITY_DETAIL " +
                "WHERE ID = :a";

                    string kdAktif = connection.ExecuteScalar<string>(sql, new { a = id });

                    if (!string.IsNullOrEmpty(kdAktif))
                    {
                        sql = "UPDATE BUSINESS_ENTITY_DETAIL " +
                            "SET ACTIVE = :a " +
                            "WHERE ID = :b";

                        if (kdAktif == "1")
                        {
                            connection.Execute(sql, new
                            {
                                a = "0",
                                b = id
                            });

                            result = new
                            {
                                status = "S",
                                message = "Status is Inactive."
                            };
                        }
                        else
                        {
                            connection.Execute(sql, new
                            {
                                a = "1",
                                b = id
                            });

                            result = new
                            {
                                status = "S",
                                message = "Status is Active."
                            };
                        }
                    }
                    else
                    {
                        result = new
                        {
                            status = "E",
                            message = "Something went wrong."
                        };
                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            
            //connection.Close();
            return result;
        }

        //----------------------------------- VIEW DETIL DATA BE -------------------------------
        public AUP_BE_WEBAPPS GetDataForDetil(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT be_id, be_name, be_address, be_city, be_province, " +
                         "valid_from, valid_to, harbour_class, postal_code, be_city, be_province, phone_1, fax_1, email, ref_desc " +
                         "FROM V_BE " +
                         "WHERE BE_ID = :a";

            AUP_BE_WEBAPPS result = connection.Query<AUP_BE_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            return result;
        }

        //------------------------------------ INSERT DATA BE -------------------------------//
        public DataReturnBENumber Add(string name, DataBE data)
        {
            string id_be = "";
           // bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO business_entity " +
                "(be_id, be_name, harbour_class, valid_from, valid_to, be_address, postal_code, be_city, be_province) " +
                "VALUES (:a, :b, :c, to_date(:d,'DD/MM/YYYY'), to_date(:e,'DD/MM/YYYY'), :f, :g, :h, :i)";

            string sql1 = "INSERT INTO be_contact_detail " +
                "(be_id, phone_1, fax_1, phone_2, fax_2, email) " +
                "VALUES (:a,:j, :k, :m, :n, :l)";

            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.BE_NAME,
                c = data.HARBOUR_CLASS,
                d = data.VALID_FROM,
                e = data.VALID_TO,
                f = data.BE_ADDRESS,
                g = data.POSTAL_CODE,
                h = data.BE_CITY,
                i = data.BE_PROVINCE,
                j = data.PHONE_1,
                k = data.FAX_1,
                l = data.EMAIL
            });

            int r1 = connection.Execute(sql1, new
            {
                a = data.BE_ID,
                j = data.PHONE_1,
                k = data.FAX_1,
                l = data.EMAIL,
                m = data.PHONE_2,
                n = data.FAX_2

            });

            //Query last id dan return value nya untuk step selanjutnya
            id_be = connection.ExecuteScalar<string>("SELECT BE_ID, ID FROM BUSINESS_ENTITY WHERE ID=(SELECT MAX(ID) FROM BUSINESS_ENTITY)");

            var resultfull = new DataReturnBENumber();
            resultfull.BE_NUMBER = id_be;
            resultfull.RESULT_STAT = (r > 0) ? true : false;
            resultfull.RESULT_STAT = (r1 > 0) ? true : false;

            connection.Close();
            return resultfull;
            /*
            result = (r > 0) ? true : false;

            result = (r1 > 0) ? true : false;

            return result;
             * */
        }

        //------------------------------------- INSERT DATA MEASUREMENT --------------------------------
        public bool AddM(string name, DataMeasurement data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO business_entity_detail " +
                "(be_id, measurement_type, description, amount, unit, valid_from, valid_to, active) " +
                "VALUES (:a, :b, :c, :d, :e, to_date(:f,'DD/MM/YYYY'), to_date(:g,'DD/MM/YYYY'), :h)";

            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.MEASUREMENT_TYPE,
                c = data.DESCRIPTION,
                d = data.AMOUNT,
                e = data.UNIT,
                f = data.VALID_FROM,
                g = data.VALID_TO,
                h = "1"
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        //-------------------------------------------- EDIT DATA BE -----------------------------
        public bool Edit(string name, DataBE data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE BUSINESS_ENTITY SET " +
                         "BE_NAME=:b, BE_ADDRESS=:c, BE_CITY=:d, BE_PROVINCE=:e, BE_CONTACT_PERSON=:f, BE_NPWP=:g, " +
                         "BE_NOPPKP=:h, CREATION_BY=:i, LAST_UPDATE_BY=:j, LAST_UPDATE_DATE=:k, VALID_FROM=to_date(:l,'DD/MM/YYYY'), " +
                         "VALID_TO=to_date(:m,'DD/MM/YYYY'), HARBOUR_CLASS=:n, POSTAL_CODE=:o, LONGITUDE=:p,  LATITUDE=:q " +
                         "WHERE BE_ID=:a";


            int r = connection.Execute(sql, new
            {
                a = data.BE_ID,
                b = data.BE_NAME,
                c = data.BE_ADDRESS,
                d = data.BE_CITY,
                e = data.BE_PROVINCE,
                f = data.BE_CONTACT_PERSON,
                g = data.BE_NPWP,
                h = data.BE_NOPPKP,
                i = data.CREATION_BY,
                j = data.LAST_UPDATE_BY,
                k = data.LAST_UPDATE_DATE,
                l = data.VALID_FROM,
                m = data.VALID_TO,
                n = data.HARBOUR_CLASS,
                o = data.POSTAL_CODE,
                p = data.LONGITUDE,
                q = data.LATITUDE
            });

            string query = @"select count(*) from BE_CONTACT_DETAIL  WHERE BE_ID=:a";
            int check = connection.ExecuteScalar<int>(query, new { a = data.BE_ID });

            string sql1 = "";
            if(check > 0)
            {
                sql1 = "UPDATE BE_CONTACT_DETAIL SET PHONE_1=:p, FAX_1=:q, EMAIL=:s, PHONE_2=:t, FAX_2=:u WHERE BE_ID=:a";
            }
            else
            {
                sql1 = @"insert into BE_CONTACT_DETAIL (BE_ID , PHONE_1 , FAX_1 , EMAIL , PHONE_2 , FAX_2)
                            values(:a, :p,:q,:s,:t,:u)";
            }
            
            int r1 = connection.Execute(sql1, new
            {
                a = data.BE_ID,
                p = data.PHONE_1,
                q = data.FAX_1,
                s = data.EMAIL,
                t = data.PHONE_2,
                u = data.FAX_2

            });
             

            

            result = (r > 0) ? true : false;
            result = (r1 > 0) ? true : false;

            connection.Close();
            return result;
        }

        //---------------------------------- DELETE DATA MEASUREMENT --------------------------------
        public dynamic DeleteData(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID " +
                "FROM BUSINESS_ENTITY_DETAIL " +
                "WHERE ID = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM BUSINESS_ENTITY_DETAIL " +
                    "WHERE ID = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "Data Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            return result;
        }

        //-------------------- LIST DATA DROPDOWN HARBOUR CLASS -----------------------------
        public IEnumerable<DDHarbour1> GetDataListHarbour()
        {
            string sql = "SELECT ID, ID AS HB_ID, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'HARBOUR_CLASS'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDHarbour1> listDetil = connection.Query<DDHarbour1>(sql);
            connection.Close();
            return listDetil;
        }

        //------------------------ LIST DROP DOWN PROVINCE ---------------------
        public IEnumerable<DDProvince> GetDataListProvince()
        {
            string sql = "SELECT ID, ID AS PROVINCE_ID, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'PROVINCE'";

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProvince> listDetil = connection.Query<DDProvince>(sql);
            connection.Close();
            return listDetil;
        }

        //-------------------------- LIST DROP DOWN MEASUREMENT ----------------
        public IEnumerable<DDHarbour> GetDataMeasurement()
        {

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            IEnumerable<DDHarbour> listData = null;

            try
            {
                string sql = "SELECT ID, REF_CODE, REF_DATA, REF_DESC FROM PROP_PARAMETER_REF_D WHERE REF_CODE = 'MEASUREMENT_TYPE' AND REF_DATA='Z001' ";

                listData = connection.Query<DDHarbour>(sql).ToList();
            }
            catch (Exception) { }
            connection.Close();
            return listData;
        }

        //------------Edit Data Measurement
        public bool EditMeasurement(string name, DataMeasurement data)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "UPDATE BUSINESS_ENTITY_DETAIL SET " +
                         "MEASUREMENT_TYPE=:a, AMOUNT=:b, UNIT=:c, VALID_FROM=to_date(:d,'DD/MM/YYYY'), VALID_TO=to_date(:e, 'DD/MM/YYYY') " +
                         "WHERE ID=:f";

            int r = connection.Execute(sql, new
            {
                a = data.MEASUREMENT_TYPE,
                b = data.AMOUNT,
                c = data.UNIT,
                d = data.VALID_FROM,
                e = data.VALID_TO,
                f = data.ID
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        //------------------------------------- INSERT DATA FILES --------------------------------
        public bool AddFiles(string sFile, string directory, string KodeCabang, string BE_ID)
        {
            bool result = false;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "INSERT INTO BUSINESS_ENTITY_ATTACHMENT " +
                "(DIRECTORY, FILE_NAME, BRANCH_ID, BE_ID) " +
                "VALUES (:a, :b, :c, :d)";

            int r = connection.Execute(sql, new
            {
                a = directory,
                b = sFile,
                c = KodeCabang,
                d = BE_ID
            });

            result = (r > 0) ? true : false;

            connection.Close();
            return result;
        }

        public dynamic DeleteDataAttachment(string id)
        {
            dynamic result = null;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT ID_ATTACHMENT " +
                "FROM BUSINESS_ENTITY_ATTACHMENT " +
                "WHERE ID_ATTACHMENT = :a";

            string recStat = connection.ExecuteScalar<string>(sql, new { a = id });

            if (!string.IsNullOrEmpty(recStat))
            {
                sql = "DELETE FROM BUSINESS_ENTITY_ATTACHMENT " +
                    "WHERE ID_ATTACHMENT = :a";

                if (recStat == "Y")
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
                else
                {
                    connection.Execute(sql, new
                    {

                        a = id
                    });

                    result = new
                    {
                        status = "S",
                        message = "File Deleted Successfully."
                    };
                }
            }
            else
            {
                result = new
                {
                    status = "E",
                    message = "Error Deleting Data."
                };
            }

            connection.Close();
            return result;
        }
        public dynamic PinPoint(string BE_ID)
        {
            dynamic result = null;
            DataBE coordinate = new DataBE();
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection())
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "select LATITUDE, LONGITUDE from BUSINESS_ENTITY where BE_ID =:a";

                    coordinate = connection.Query<DataBE>(sql, new { a = BE_ID }).DefaultIfEmpty().FirstOrDefault();

                    if (coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                    {
                        coordinate = connection.Query<DataBE>(sql, new { a = 1011 }).DefaultIfEmpty().FirstOrDefault();
                    }

                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                    throw;
                }
            }

            //connection.Close();
            return result;
        }
    }
}