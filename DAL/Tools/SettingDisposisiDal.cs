﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Models.GeneralResult;
using Remote.Models;
using Remote.Models.MonitoringKronologisMaster;
using Oracle.ManagedDataAccess.Client;
using Remote.Models.SettingDisposisi;

namespace Remote.DAL
{
    public class SettingDisposisiDal
    {
        public IDataTable GetData(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IDataTable result = null;
            IDbConnection connection = DatabaseFactory.GetConnection();
            dynamic listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";

                //search filter data
                string where = search.Length >= 2 ? " AND ((UPPER(A.NAME) LIKE '%' || '" + search.ToUpper() + "'|| '%')) " : "";
                string sql = @"select A.ID,TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE, A.USER_ID, A.NAME, A.IS_ACTIVE, A.IS_DELETE 
                                FROM SETTING_DISPOSISI A
                                WHERE A.IS_DELETE = 0  
                                " + where + " ORDER BY a.ID ASC";

                fullSql = fullSql.Replace("sql", sql);
                listData = connection.Query<SettingDisposisi>(fullSql, new
                {
                    a = start,
                    b = end
                }).ToList();
                fullSqlCount = fullSqlCount.Replace("sql", sql);
                count = connection.ExecuteScalar<int>(fullSqlCount, new {  });
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }
            finally
            {
                connection.Close();
            }

            result = new IDataTable();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;

            return result;
        }

        public dynamic GetDataHeader(string id)
        {
            string sql = @"SELECT ID, TO_CHAR(START_DATE,'DD/MM/YYYY') START_DATE, TO_CHAR(END_DATE,'DD/MM/YYYY') END_DATE,
                        USER_ID,NAME, MEMO, INSTRUKSI FROM SETTING_DISPOSISI WHERE ID = :a ";

            //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection(); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            dynamic data = connection.Query<dynamic>(sql, new
            {
                a = id
            }).FirstOrDefault();
            connection.Close();
            return data;
        }

        public Results AddDataHeader(SettingDisposisi data, string user_id_create)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
               
                string sql = @"select count(ID) from SETTING_DISPOSISI a where a.USER_ID = :a AND a.is_delete = 0 and a.is_active = 1 ";
                r = connection.ExecuteScalar<int>(sql, new
                {
                    //a = user_id
                    a = data.USER_ID
                });

                if (r > 0)
                {
                    result.Msg = "Username Already Exist";
                    result.Status = "E";
                }
                else
                {
                    sql = @"insert into SETTING_DISPOSISI (USER_ID, NAME, START_DATE, END_DATE, MEMO, INSTRUKSI,
                            IS_ACTIVE, IS_DELETE, USER_ID_CREATE)
                            values(:a, :b, to_date(:c,'DD/MM/YYYY'), to_date(:d,'DD/MM/YYYY'), :e, :f, :g, :h, :i)";
                    r = connection.Execute(sql, new
                    {
                        a = data.USER_ID,
                        b = data.NAME,
                        c = data.START_DATE,
                        d = data.END_DATE,
                        e = data.MEMO,
                        f = data.INSTRUKSI,
                        g = 1,
                        h = 0,
                        i = user_id_create,

                    });
                    if (r > 0)
                    {
                        sql = @"select MAX(id) ID from SETTING_DISPOSISI where USER_ID_CREATE = :a  ";
                        string id = connection.ExecuteScalar<string>(sql, new
                        {
                            a = user_id_create,

                        });
                        result.Msg = id;
                        result.Status = "S";
                    }
                    else
                    {
                        result.Msg = "Failed to Add Data";
                        result.Status = "E";

                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;
        }

        public Results EditDataHeader(SettingDisposisi data, string user_id_create)
        {
            Results result = new Results();
            int r = 0;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sqlUpdate = @"UPDATE SETTING_DISPOSISI A SET A.USER_ID =:a, A.NAME =:b, A.START_DATE = to_date(:c,'DD/MM/YYYY'), 
                            A.END_DATE =to_date(:d,'DD/MM/YYYY'),A.MEMO =:e , A.INSTRUKSI =:f, A.IS_ACTIVE =:g, A.IS_DELETE =:h, 
                            A.USER_ID_UPDATE =:i WHERE ID =:j ";
                r = connection.Execute(sqlUpdate, new
                {
                    a = data.USER_ID,
                    b = data.NAME,
                    c = data.START_DATE,
                    d = data.END_DATE,
                    e = data.MEMO,
                    f = data.INSTRUKSI,
                    g = 1,
                    h = 0,
                    i = user_id_create,
                    j = data.ID,

                });

                if (r > 0)
                {
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Failed to Add Data";
                    result.Status = "E";
                }

            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();

            return result;
        }

        public Results AddDataDetail(SettingDetail data, string name)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "";
                sql = @"DELETE from SETTING_DISPOSISI_DETAIL where SETTING_DISPOSISI_ID = :a and USER_ID = :b";
                foreach (SettingDeleteDetail temp in data.DeleteD)
                {
                    r = connection.Execute(sql, new
                    {
                        a = data.HEADER_ID,
                        b = temp.USER_ID,
                    });
                }

                sql = @"insert into SETTING_DISPOSISI_DETAIL (SETTING_DISPOSISI_ID,USER_ID,NAME,URUTAN) 
                            values(:b, :c, :e, :f)";

                var sqlD = "select count(1) from SETTING_DISPOSISI_DETAIL where SETTING_DISPOSISI_ID = :a and USER_ID = :b ";
                foreach (SettingDetailD temp in data.Service)
                {
                    int check = connection.ExecuteScalar<int>(sqlD, new { a = data.HEADER_ID, b = temp.USER_ID });
                    if (check == 0)
                    {
                        r = connection.Execute(sql, new
                        {
                            b = data.HEADER_ID,
                            c = temp.USER_ID,
                            e = temp.NAME,
                            f = temp.URUTAN
                        });
                    }
                }

                if (r > 0)
                {
                    result.Msg = "Data Added Successfully";
                    result.Status = "S";
                }
                else
                {
                    result.Msg = "Data Edit Successfully";
                    result.Status = "S";
                }
            }
            catch (Exception ex)
            {
                result.Msg = "Terjadi error. " + ex.Message;
                result.Status = "E";
            }

            connection.Close();
            return result;

        }

        public Results DeleteDataHeader(SettingDisposisi data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    var sql = @"UPDATE SETTING_DISPOSISI A SET A.IS_DELETE = 1 where A.ID =:c";
                    r = connection.Execute(sql, new
                    {
                        c = data.ID
                    }, transaction: transaction);

                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Deleted Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Delete Data";
                        result.Status = "E";

                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public Results EditDataStatus(SettingDisposisi data)
        {
            Results result = new Results();
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    var sql = @"UPDATE SETTING_DISPOSISI A SET A.IS_ACTIVE =:a  where A.ID =:b";
                    r = connection.Execute(sql, new
                    {
                        a = data.IS_ACTIVE,
                        b = data.ID
                    }, transaction: transaction);

                    if (r > 0)
                    {
                        transaction.Commit();
                        result.Msg = "Data Update Successfully";
                        result.Status = "S";
                    }
                    else
                    {
                        transaction.Rollback();
                        result.Msg = "Failed to Delete Data";
                        result.Status = "E";

                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Msg = "Terjadi error. " + ex.Message;
                    result.Status = "E";
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public IEnumerable<SettingDetailD> GetDataDetail(string id)
        {
            IEnumerable<SettingDetailD> listDetil = null;
            if (id != "undefined")
            {
                string sql = @"select USER_ID, NAME, URUTAN from SETTING_DISPOSISI_DETAIL a
                        where a.SETTING_DISPOSISI_ID = :a order by a.ID asc";

                //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
                IDbConnection connection = DatabaseFactory.GetConnection(); //---CONN REMOTE DB
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                listDetil = connection.Query<SettingDetailD>(sql, new
                {
                    a = id
                });
                connection.Close();
            }

            return listDetil;
        }

        public IEnumerable<DDCabang> GetDataCabang()
        {
            string sql = "SELECT BE_ID, BE_NAME FROM BUSINESS_ENTITY WHERE BE_NAME LIKE '%Cabang%'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDCabang> listDetil = connection.Query<DDCabang>(sql);
            connection.Close();
            return listDetil;
        }

        public IEnumerable<DDRegional> GetDataRegional()
        {
            string sql = "SELECT BE_ID, BE_NAME FROM BUSINESS_ENTITY WHERE BE_NAME LIKE '%Regional%'";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDRegional> listDetil = connection.Query<DDRegional>(sql);
            connection.Close();
            return listDetil;
        }

        public IEnumerable<DDEmailEmployee> GetDataEmployee()
        {
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDEmailEmployee> listData = null;

            try
            {
                string sql = @"SELECT ID,USER_EMAIL EMAIL , KD_CABANG BE_ID, CONCAT(CONCAT(USER_NAME,' - '),C.ROLE_NAME) EMPLOYEE_NAME 
                                FROM APP_USER A
                                JOIN APP_USER_ROLE B ON B.USER_ID = A.ID
                                JOIN APP_ROLE c on C.ROLE_ID = B.ROLE_ID and B.APP_ID = A.APP_ID";
                listData = connection.Query<DDEmailEmployee>(sql).ToList();
            }
            catch (Exception) { }

            connection.Close();
            return listData;
        }

        public IEnumerable<DDEmailEmployee> GetDataEmployee(string KodeCabang, string q)
        {
            IEnumerable<DDEmailEmployee> listData = null;

            QueryGlobal query = new QueryGlobal();

            try
            {
                using (IDbConnection connection = DatabaseFactory.GetConnection("app_repo"))
                {
                    string sql = @"SELECT ID,USER_EMAIL EMAIL , KD_CABANG BE_ID, CONCAT(CONCAT(USER_NAME,' - '),C.ROLE_NAME) EMPLOYEE_NAME 
                                FROM APP_USER A
                                JOIN APP_USER_ROLE B ON B.USER_ID = A.ID
                                JOIN APP_ROLE c on C.ROLE_ID = B.ROLE_ID and B.APP_ID = A.APP_ID 
                                WHERE LOWER(USER_NAME) like :b ";

                    listData = connection.Query<DDEmailEmployee>(sql, new { b = $"%{q.ToLower()}%" }).ToList();
                }

            }
            catch (Exception) { }

            return listData;
        }

    }

}