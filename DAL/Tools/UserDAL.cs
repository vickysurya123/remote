﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.MasterBusinessEntity;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Entities;
//using System.Web.UI;
//using System.Runtime.Remoting.Contexts;
using Remote.ViewModels;
using Remote.Models;
using Remote.Models.DataTablesUser;
using Remote.Models.UserModels;
using Remote.Helpers;
using Remote.Models.MasterInstallation;

namespace Remote.DAL
{
    public class UserDAL
    {
        //------------------------ LIST DROP DOWN BRANCH --------------------- 
        public IEnumerable<DDBranch> GetDataListBranch()
        {
            string sql = "SELECT BRANCH_ID, BE_NAME, BE_ID FROM BUSINESS_ENTITY ORDER BY BE_NAME ASC";

            //IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDBranch> listDetil = connection.Query<DDBranch>(sql);
            connection.Close();
            return listDetil; 
            
        }

        public IEnumerable<DDProfitCenter> GetDataProfitCenter(string be_id)
        {
            string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BE_ID = :a and STATUS = 1 order by profit_center_id asc";
            // string sql = "SELECT PROFIT_CENTER_ID, PROFIT_CENTER_ID || ' - ' || TERMINAL_NAME AS TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID = :a and STATUS = 1 order by profit_center_id asc";
            //string sql = "SELECT profit_center_id, profit_center_id || ' - ' || terminal_name AS terminal_name FROM PROFIT_CENTER WHERE BRANCH_ID = :a AND STATUS IS NULL";
            //string sql = "SELECT PROFIT_CENTER_ID, TERMINAL_NAME FROM PROFIT_CENTER WHERE BRANCH_ID = :a AND STATUS IS NULL order by profit_center_id asc";

            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDProfitCenter> listDetil = connection.Query<DDProfitCenter>(sql, new
            {
                a = be_id
                //a = 1061
            });
            connection.Close();
            return listDetil;
        }

        //------------------------ LIST DROP DOWN POSITION ---------------------
        public IEnumerable<DDPosition> GetDataListPosition()
        {
            string sql = "SELECT ROLE_ID, ROLE_NAME FROM APP_ROLE WHERE APP_ID=1 ORDER BY ROLE_NAME ASC"; 

            IDbConnection connection = DatabaseFactory.GetConnection("app_repo"); //---CONN APP_REPO DB
            //IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            IEnumerable<DDPosition> listDetil = connection.Query<DDPosition>(sql);
            connection.Close();
            return listDetil;
        }

        //------------------------- DATA TABLE USER ------------------------------//
        public DataTablesUser GetDataUser(int draw, int start, int length, string kd_cabang, string search)
        {
            int count = 0;
            int end = start + length;
            DataTablesUser result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            IEnumerable<AUP_DATA_USER> listDataUser = null;
            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                //string where = kd_cabang == "0" ? "" : " and KD_CABANG = " + kd_cabang; 
                if (string.IsNullOrEmpty(search))
                {
                    string sql = "SELECT ID, APP_ID, USER_LOGIN, USER_PASSWORD, USER_EMAIL, USER_PHONE, USER_NAME, STATUS, "+
                                 "USER_ROLE_ID, KD_CABANG, KD_TERMINAL, PROFIT_CENTER_ID, PROPERTY_ROLE, PARAM1, PARAM2, PARAM3, ROLE_NAME, BE_NAME " +
                                 "FROM VW_APP_USER where APP_ID = 1  AND KD_CABANG IN (SELECT DISTINCT BRANCH_ID FROM REMOTE.V_BE WHERE BRANCH_WHERE = :d)";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataUser = connection.Query<AUP_DATA_USER>(fullSql, new { a = start, b = end, d = kd_cabang });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { d = kd_cabang });
                }
                else
                {
                    string sql = "SELECT ID, APP_ID, USER_LOGIN, USER_PASSWORD, USER_EMAIL, USER_PHONE, USER_NAME, STATUS, " +
                                 "USER_ROLE_ID, KD_CABANG, KD_TERMINAL, PROFIT_CENTER_ID, PROPERTY_ROLE, PARAM1, PARAM2, PARAM3, ROLE_NAME, BE_NAME " +
                                "FROM VW_APP_USER " +
                                "WHERE UPPER(USER_LOGIN||USER_NAME||BE_NAME||USER_EMAIL||USER_PHONE||ROLE_NAME) LIKE '%' || :param || '%'  AND KD_CABANG IN (SELECT DISTINCT BRANCH_ID FROM REMOTE.V_BE WHERE BRANCH_WHERE = :d) ";

                    fullSql = fullSql.Replace("sql", sql);

                    listDataUser = connection.Query<AUP_DATA_USER>(fullSql, new { a = start, b = end, param = search.ToUpper(), d = kd_cabang  });

                    fullSqlCount = fullSqlCount.Replace("sql", sql);

                    count = connection.ExecuteScalar<int>(fullSqlCount, new { param = search.ToUpper(), d = kd_cabang });
                }
            }
            catch (Exception e){
                string aaa = e.ToString();
                throw;
            }
            finally{
                connection.Close();
            }
            
                result = new DataTablesUser();
                result.draw = draw;
                result.recordsFiltered = count;
                result.recordsTotal = count;
                result.data = listDataUser.ToList();

                //connection.Close();
                return result;

            
        }
        
        //------------------------------ VIEW EDIT DATA --------------------------------
        public AUP_DATA_USER GetDataForEdit(string id)
            {
            //string xCount = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_DATA_USER result = new AUP_DATA_USER();
            
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "SELECT * " +
                                 "FROM VW_APP_USER " +
                                 "WHERE ID = :a ";

                     result = connection.Query<AUP_DATA_USER>(sql, new { a = id }).FirstOrDefault();
                    
                }
                catch (Exception)
                {

                    throw;
                }
            finally
            {
                connection.Close();
            }
            
            return result;
        }

        //------------------------------ GET DATA ROLE --------------------------------
        public AUP_DATA_USER GetDataRole(string id)
        {
            //string xCount = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            AUP_DATA_USER result = new AUP_DATA_USER();

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT * " +
                             "FROM VW_APP_USER " +
                             "WHERE ID = :a ";

                result = connection.Query<AUP_DATA_USER>(sql, new { a = id }).FirstOrDefault();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                connection.Close();
            }

            return result;
        //------------------------------ VIEW EDIT DATA --------------------------------
        }

        //------------------------------ GET DATA FOR DETAIL --------------------------------
        public AUP_BE_WEBAPPS GetDataForDetil(string id)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql = "SELECT be_id, be_name, be_address, be_city, be_province, " +
                         "valid_from, valid_to, harbour_class, postal_code, be_city, be_province, phone_1, fax_1, email, ref_desc " +
                         "FROM V_BE " +
                         "WHERE BE_ID = :a";

            AUP_BE_WEBAPPS result = connection.Query<AUP_BE_WEBAPPS>(sql, new { a = id }).FirstOrDefault();

            connection.Close();
            return result;
        }

        //----------------------------------- INSERT DATA USER -------------------------------
        public bool AddUserDAL(DataUser data)
        {
            //insert app_user
            bool result = false;
            //var xID = "";
            var xIDRole = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            /*string sql = "INSERT INTO APP_USER(ID, USER_LOGIN, USER_PASSWORD, USER_NAME, USER_EMAIL, USER_PHONE, KD_CABANG, KD_TERMINAL, APP_ID, STATUS) " +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)";*/
            string sql = "INSERT INTO APP_USER(USER_LOGIN, USER_PASSWORD, USER_NAME, USER_EMAIL, USER_PHONE, KD_CABANG, KD_TERMINAL, APP_ID, STATUS, PROFIT_CENTER_ID) " +
                         "VALUES (:b, :c, :d, :e, :f, :g, :h, :i, :l, :m)";
            int r = connection.Execute(sql, new
            {
                b = data.USER_LOGIN,
                c = "", //USER_PASSWORD
                d = data.USER_NAME,
                e = data.USER_EMAIL,
                f = data.USER_PHONE,
                g = data.KD_CABANG,
                h = data.KD_CABANG, //KD_TERMINAL = KD_CABANG
                i = "1", //APP_ID = "1"
                l = "1", //STATUS
                m = data.PROFIT_CENTER_ID
            });

            result = (r > 0) ? true : false;
            connection.Close();

            //select ID > APP_USER
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            xIDRole = connection.ExecuteScalar<string>("SELECT ID FROM APP_USER WHERE ID = ( SELECT max(ID) FROM APP_USER )");
            
            //insert app_user_role
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql2 = "INSERT INTO APP_USER_ROLE(USER_ID, ROLE_ID, APP_ID) " +
                         "VALUES (:a, :b, :c)";

            int r2 = connection.Execute(sql2, new
            {
                a = xIDRole, //USER_ID
                b = data.ROLE_ID, //ROLE_ID
                c = "1", //APP_ID = "1"
            });

            result = (r2 > 0) ? true : false;
            connection.Close();

            //insert select all role
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            string sql3 = "INSERT INTO APP_USER_MENU (USER_ID, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, CREATION_DATE) " +
                         "SELECT :b, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, sysdate " +
                         "FROM APP_ROLE_MENU WHERE ROLE_ID=:a";
            
            int r3 = connection.Execute(sql3, new
            {
                a = data.ROLE_ID,
                b = xIDRole
            });

            result = (r3 > 0) ? true : false;
            connection.Close();

            return result;
        }

        //----------------------------------- UPDATE DATA USER -------------------------------
        public bool UpdateUserDAL(DataUser data)
        {
            bool result = false;
            int r = 0;
            string xIDBranch = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            IDbConnection connection1 = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open(); //APP_REPO
            if (connection1.State.Equals(ConnectionState.Closed))
                connection1.Open(); //REMOTE
            try
            {
                //update APP_USER
                //xIDBranch = connection1.ExecuteScalar<string>("SELECT BRANCH_ID FROM BUSINESS_ENTITY WHERE BE_NAME ='" + data.KD_CABANG2 + "'");
                xIDBranch = connection1.ExecuteScalar<string>("SELECT BRANCH_ID FROM BUSINESS_ENTITY WHERE BRANCH_ID ='" + data.KD_CABANG + "'");
                string sql = @"UPDATE APP_USER 
                            SET USER_LOGIN = :a, USER_NAME = :c, USER_EMAIL = :d, USER_PHONE = :e, KD_CABANG = :f, KD_TERMINAL = :g, PROFIT_CENTER_ID = :m 
                            WHERE ID = :h ";

                r = connection.Execute(sql, new
                {
                    h = data.ID,
                    a = data.USER_LOGIN,
                    // b = "", //USER_PASSWORD
                    c = data.USER_NAME,
                    d = data.USER_EMAIL,
                    e = data.USER_PHONE,
                    f = xIDBranch,
                    g = xIDBranch, //KD_TERMINAL = KD_CABANG
                    m = data.PROFIT_CENTER_ID
                });

                result = (r > 0) ? true : false;
                try
                {
                    //APP_USER_ROLE
                    string sql1 = "UPDATE APP_USER_ROLE " +
                                  "SET ROLE_ID=:a " +
                                  "WHERE USER_ID=:h";

                    r = connection.Execute(sql1, new
                    {
                        h = data.ID,
                        a = data.ROLE_ID //ROLE_ID
                    });

                    result = (r > 0) ? true : false;

                }
                catch (Exception)
                {
                    throw;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();

                connection1.Close();
            }
            return result;
        }
        
        //----------------------------------- UPDATE STATUS USER -------------------------------
        public bool UpdateStatusUserDAL(DataUser data)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "UPDATE APP_REPO.APP_USER " +
                            "SET STATUS=:a " +
                            "WHERE ID=:b";

                r = connection.Execute(sql, new
                {
                    b = data.ID,
                    a = data.STATUS
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        //------------------------------ UPDATE STATUS USER MENU --------------------------------
        public bool UpdateStatusUserMenuDAL(DataUserMenu[] data)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < data.Length; i++)
                    {
                        string sql = "UPDATE APP_USER_MENU " +
                                "SET STATUS=:a " +
                                "WHERE USER_ID=:b AND MENU_ID=:c AND ROLE_ID=:d";

                        r = connection.Execute(sql, new
                        {
                            b = data[i].xID,
                            a = data[i].STATUS,
                            c = data[i].MENU_ID,
                            d = data[i].ROLE_ID
                        }, transaction: transaction);

                        result = (r > 0) ? true : false;
                    }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }

            }
            connection.Close();
            return result;
        }

        //------------------------------ UPDATE STATUS USER MENU --------------------------------
        public bool UpdateStatusUserMenuDAL(int xID, int MENU_ID, int STATUS, int ROLE_ID)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "UPDATE APP_USER_MENU " +
                            "SET STATUS=:a " +
                            "WHERE USER_ID=:b AND MENU_ID=:c AND ROLE_ID=:d";

                r = connection.Execute(sql, new
                {
                    b = xID,
                    a = STATUS,
                    c = MENU_ID,
                    d = ROLE_ID
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                connection.Close();
            }
            return result;
        }

        //------------------------------ DELETE AND UPDATE USER MENU --------------------------------
        public bool DeleteUpdateMenuDAL(DataUserMenu data)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "DELETE FROM APP_USER_MENU " +
                             "WHERE USER_ID=:a AND ROLE_ID=:b";

                r = connection.Execute(sql, new
                {
                    a = data.xID,
                    b = data.ROLE_ID
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //insert select all role
            try
            {
                string status_code = connection.ExecuteScalar<string>("SELECT STATUS FROM APP_ROLE_MENU WHERE ROLE_ID='" + data.ROLE_ID2 + "'");
                //int xStatus = 1;
                string sql3 = "INSERT INTO APP_USER_MENU (USER_ID, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, CREATION_DATE) " +
                             "SELECT :b, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, sysdate " +
                             "FROM APP_ROLE_MENU WHERE ROLE_ID=:a";

                int r3 = connection.Execute(sql3, new
                {
                    a = data.ROLE_ID2,
                    b = data.xID
                });

                result = (r3 > 0) ? true : false;
                connection.Close();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        //------------------------------ UPDATE USER MENU --------------------------------
        public bool UpdateMenuDAL(DataUserMenu data)
        {
            bool result = false;
            //int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //insert select all role
            try
            {
                string sql3 = "INSERT INTO APP_USER_MENU (USER_ID, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, CREATION_DATE) " +
                             "SELECT :b, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, sysdate " +
                             "FROM APP_ROLE_MENU WHERE ROLE_ID=:a";

                int r3 = connection.Execute(sql3, new
                {
                    a = data.ROLE_ID,
                    b = data.xID
                });

                result = (r3 > 0) ? true : false;
                connection.Close();

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        //------------------------------ CLONE USER --------------------------------
        public bool CloneUserDAL(DataUser data)
        {
            bool result = false;
            int r = 0;
            string xIDBranch = "";
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            IDbConnection connection1 = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open(); //APP_REPO
            if (connection1.State.Equals(ConnectionState.Closed))
                connection1.Open(); //REMOTE
            try
            {
                //update APP_USER
                xIDBranch = connection1.ExecuteScalar<string>("SELECT BRANCH_ID FROM BUSINESS_ENTITY WHERE BRANCH_ID ='" + data.KD_CABANG + "'");
                /*string sql = "INSERT INTO APP_USER(ID, USER_LOGIN, USER_PASSWORD, USER_NAME, USER_EMAIL, USER_PHONE, KD_CABANG, KD_TERMINAL, APP_ID, STATUS) " +
                         "VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)";*/
                //string sql = "INSERT INTO APP_USER(USER_LOGIN, USER_PASSWORD, USER_NAME, USER_EMAIL, USER_PHONE, KD_CABANG, KD_TERMINAL, APP_ID, ROLE_PORTALSI, PROPERTY_ROLE, STATUS, PROFIT_CENTER_ID) " +
                //             "VALUES (:b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m)";
                string sql = "INSERT INTO APP_USER(USER_LOGIN, USER_PASSWORD, USER_NAME, USER_EMAIL, USER_PHONE, KD_CABANG, KD_TERMINAL, APP_ID, STATUS, PROFIT_CENTER_ID) " +
                         "VALUES (:b, :c, :d, :e, :f, :g, :h, :i, :l, :m)";
                r = connection.Execute(sql, new
                {
                    b = data.USER_LOGIN,
                    c = "", //USER_PASSWORD
                    d = data.USER_NAME,
                    e = data.USER_EMAIL,
                    f = data.USER_PHONE,
                    g = data.KD_CABANG,
                    h = data.KD_CABANG, //KD_TERMINAL = KD_CABANG
                    i = "1", //APP_ID = "1"
                    l = "1", //STATUS = "1"
                    m = data.PROFIT_CENTER_ID
                });

                result = (r > 0) ? true : false;

                string xIDRole = connection.ExecuteScalar<string>("SELECT ID FROM APP_USER WHERE ID = ( SELECT max(ID) FROM APP_USER )");

                //insert app_user_role
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();

                string sql2 = "INSERT INTO APP_USER_ROLE(USER_ID, ROLE_ID, APP_ID) " +
                             "VALUES (:a, :b, :c)";

                int r2 = connection.Execute(sql2, new
                {
                    a = xIDRole, //USER_ID
                    b = data.ROLE_ID, //ROLE_ID
                    c = "1", //APP_ID = "1"
                });

                result = (r2 > 0) ? true : false;

                //insert select all role
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();

                string sql3 = "INSERT INTO APP_USER_MENU (USER_ID, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, CREATION_DATE) " +
                             "SELECT :b, MENU_ID, MENU_PARENT_ID, ORDERED_BY, APP_ID, STATUS, ROLE_ID, sysdate " +
                             "FROM APP_ROLE_MENU WHERE ROLE_ID=:a";

                int r3 = connection.Execute(sql3, new
                {
                    a = data.ROLE_ID,
                    b = xIDRole
                });

                result = (r3 > 0) ? true : false;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                connection.Close();
                connection1.Close();
            }
            return result;
        }

        //------------------------------ CLONE STATUS USER MENU --------------------------------
        public bool CloneStatusUserMenuDAL(int MENU_ID, int STATUS, int ROLE_ID)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string xIDRole = connection.ExecuteScalar<string>("SELECT ID FROM APP_USER WHERE ID = ( SELECT max(ID) FROM APP_USER )");
                string sql = "UPDATE APP_USER_MENU " +
                            "SET STATUS=:a " +
                            "WHERE USER_ID=:b AND MENU_ID=:c AND ROLE_ID=:d";

                r = connection.Execute(sql, new
                {
                    b = xIDRole,
                    a = STATUS,
                    c = MENU_ID,
                    d = ROLE_ID
                });

                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        //------------------------------ GET DATA ROLE MENU --------------------------------
        public IEnumerable<VW_ROLE_MENU> getDataRoleMenu(string xID)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            IEnumerable<VW_ROLE_MENU> listRoleMenu = null;
            try
            {
                string sql = "SELECT * " +
                             "FROM VW_ROLE_MENU WHERE ROLE_ID='" + xID + "' " +
                             "ORDER BY ORDERED_BY ASC";

                listRoleMenu = connection.Query<VW_ROLE_MENU>(sql);
            }
            catch (Exception)
            {
                listRoleMenu = null;
            }
            finally
            {
                connection.Close();
            }
            return listRoleMenu;
        }

        //------------------------------ GET DATA ROLE --------------------------------
        public IEnumerable<VW_USER_MENU> getDataRole(String xID, string xIDRole)
        {
            IDbConnection connection = DatabaseFactory.GetConnection("app_repo");
            IEnumerable<VW_USER_MENU> listUserMenu = null;
            try
            {
                string sql = "SELECT * " +
                             "FROM APP_USER_MENU WHERE USER_ID='" + xID + "' AND ROLE_ID='" + xIDRole + "' " +
                             "ORDER BY ORDERED_BY ASC";

                listUserMenu = connection.Query<VW_USER_MENU>(sql);
            }
            catch (Exception e)
            {
                listUserMenu = null;

                string aaa = e.ToString();
            }
            finally
            {
                connection.Close();
            }
            return listUserMenu;
        }
    }
}