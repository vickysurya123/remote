﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.Role;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using Remote.Entities;
using Remote.Models;
using Remote.Helpers;

namespace Remote.DAL
{
    public class RoleDAL
    {

        //--------------------------------- VIEW DATA ROLE ---------------------------
        public DataTablesRole GetDataRole(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            DataTablesRole result = null;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            IEnumerable<AUP_ROLE_Remote> listDataOperator = null;
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string fullSql =
                        "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                    string fullSqlCount = "SELECT count(*) FROM (sql)";

                    if (search.Length == 0)
                    {
                        string sql =
                            "SELECT ROLE_ID, ROLE_NAME, APP_ID, PROPERTY_ROLE FROM APP_REPO.APP_ROLE WHERE APP_ID = '1' ORDER BY ROLE_ID DESC";

                        fullSql = fullSql.Replace("sql", sql);

                        listDataOperator = connection.Query<AUP_ROLE_Remote>(fullSql, new {a = start, b = end});

                        fullSqlCount = fullSqlCount.Replace("sql", sql);

                        count = connection.ExecuteScalar<int>(fullSqlCount, new {c = 2});
                    }
                    else
                    {
                        //search filter data
                        if (search.Length > 2)
                        {
                            string sql = "SELECT ROLE_ID, ROLE_NAME, APP_ID, PROPERTY_ROLE FROM APP_REPO.APP_ROLE " +
                                         "WHERE APP_ID = '1' AND ((UPPER(ROLE_NAME) LIKE '%' ||:c|| '%')) ORDER BY ROLE_ID DESC";

                            fullSql = fullSql.Replace("sql", sql);

                            listDataOperator = connection.Query<AUP_ROLE_Remote>(fullSql, new
                            {
                                a = start,
                                b = end,
                                c = search.ToUpper()
                            });

                            fullSqlCount = fullSqlCount.Replace("sql", sql);

                            count = connection.ExecuteScalar<int>(fullSqlCount, new {c = search});
                        }
                    }
                }
                catch (Exception ex)
                {
                string aaa = ex.ToString();
            }
                finally
                {
                    connection.Close();
                }

            result = new DataTablesRole();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listDataOperator.ToList();

            return result;
        }

        //------------------------------ INSERT DATA ROLE --------------------
        public bool AddDataRole(string name, DataRole data)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "INSERT INTO APP_REPO.APP_ROLE " +
                                "(ROLE_NAME, APP_ID, PROPERTY_ROLE) " +
                                "VALUES (:a, :b, :c)";

                r = connection.Execute(sql, new
                {
                    a = data.ROLE_NAME,
                    b = "1",
                    c = data.PROPERTY_ROLE
                });
                result = (r > 0) ? true : false;

            }
            catch (Exception)
            {
                throw;
            }

            connection.Close();
            return result;
        }

        //------------------------------ UPDATE DATA ROLE --------------------
        public bool UpdateDataRole(string name, DataRole data)
        {
            bool result = false;
            int r = 0;
            IDbConnection connection = DatabaseFactory.GetConnection("default");
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "UPDATE APP_REPO.APP_ROLE " +
                                 "SET ROLE_NAME=:a, PROPERTY_ROLE=:b " +
                                 "WHERE ROLE_ID=:c";

                    r = connection.Execute(sql, new
                    {
                        a = data.ROLE_NAME,
                        b = data.PROPERTY_ROLE,
                        c = data.ROLE_ID
                    });
                    result = (r > 0) ? true : false;

                }
                catch (Exception)
                {
                    throw;
                }

            connection.Close();
            return result;
        }
    }
}