﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using Remote.Entities;
using Remote.Helpers;

namespace Remote.DAL
{
    public class CustomerDAL
    {
        public IEnumerable<Customer> GetAll()
        {
            //ServiceConfig.CreateProductionDbConnection();
            //IDbConnection connection = ServiceConfig.MasterDataDbConnection;
            IDbConnection connection = DatabaseFactory.GetConnection("OracleProdDbContext");

            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                         "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                         "FROM SAFM_PELANGGAN";
            var res = connection.Query<Customer>(sql);

            connection.Close();
            return res;
        }

        public IEnumerable<Customer> GetAllBySAPCode(string ProfitCenter, string kodePelangganSAP)
        {
            //ServiceConfig.CreateProductionDbConnection();
            //IDbConnection connection = ServiceConfig.MasterDataDbConnection;
            IDbConnection connection = DatabaseFactory.GetConnection("OracleProdDbContext"); 
            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                         "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                         "FROM SAFM_PELANGGAN WHERE MPLG_PROFIT_CENTER = :a AND MPLG_KODE_SAP=:b AND KD_AKTIF='A'";
            var res = connection.Query<Customer>(sql, new { a = ProfitCenter, b = kodePelangganSAP });

            connection.Close();
            return res;
        }
        public IEnumerable<Customer> GetAllByKodePelanggan(int kodeCabang, string kodePelanggan)
        {

            IDbConnection connection = DatabaseFactory.GetConnection("OracleProdDbContext");
            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                         "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                         "FROM SAFM_PELANGGAN WHERE KD_CABANG=:a AND MPLG_KODE=:b AND KD_AKTIF='A'";
            var res = connection.Query<Customer>(sql, new { a = kodeCabang, b = kodePelanggan });
            connection.Close();
            return res;
        }

        public IEnumerable<Customer> GetAllLikeNamaPelanggan(string namaPelanggan)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                         "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                         "FROM vw_customers WHERE UPPER(MPLG_NAMA) LIKE '%'||:a||'%' AND KD_AKTIF='A'";
            var res = connection.Query<Customer>(sql, new { a = namaPelanggan.ToUpper() });
            connection.Close();
            return res;
        }

        public IEnumerable<Customer> GetAllBySAPbyNamaPelanggan(string profitCenter, string namaPelanggan)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                          "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                          "FROM SAFM_PELANGGAN WHERE MPLG_PROFIT_CENTER=:a AND UPPER(MPLG_NAMA) LIKE '%'||:b||'%' AND KD_AKTIF='A'";
            var res = connection.Query<Customer>(sql, new { a = profitCenter, b = namaPelanggan.ToUpper() });
            connection.Close();
            return res;
        }


        //----- Coba get kode dan nama pelanggan untuk auto complete
        public IEnumerable<Customer> GetAllLikeNamadanKode(string namaPelanggan, string kodePel)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string sql = "SELECT KD_CABANG,MPLG_PROFIT_CENTER,MPLG_TIPE,MPLG_KODE_SAP,MPLG_KODE,MPLG_NAMA,MPLG_ALAMAT,MPLG_KOTA,MPLG_JENIS_USAHA,MPLG_BADAN_USAHA,MPLG_CONT_PERSON,MPLG_TELEPON," +
                         "MPLG_EMAIL_ADDRESS,MPLG_FAX,MPLG_NPWP,MPLG_SIUP " +
                         "FROM vw_customers WHERE UPPER(MPLG_NAMA) LIKE '%'||:a||'%' AND UPPER(MPLG_KODE) LIKE '%'||:a||'%' AND KD_AKTIF='A'";
            var res = connection.Query<Customer>(sql, new { a = namaPelanggan.ToUpper(), b = kodePel.ToUpper() });
            connection.Close();
            return res;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer(string MPLG_NAMA, string KodeCabang)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            
            string str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                         "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A' AND KD_CABANG = '" + KodeCabang + "'";
            
            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();
            connection.Close();

            return exec;
        }

        public List<AUTOCOMPLETE_CUSTOMER> DDCustomer_S2(string MPLG_NAMA)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();

            string str = "SELECT MPLG_KODE id, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                         "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%" + MPLG_NAMA.ToUpper() + "%') OR (MPLG_KODE LIKE '%" + MPLG_NAMA.ToUpper() + "%')) AND KD_AKTIF='A'";


            /*
            string str = "SELECT MPLG_KODE code, MPLG_KODE || ' - ' ||MPLG_NAMA AS label, MPLG_NAMA AS name, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                         "FROM vw_customers WHERE ((UPPER(MPLG_NAMA) LIKE '%'|| UPPER(:MPLG_NAMA) || '%') OR (MPLG_KODE LIKE '%'||UPPER(:MPLG_NAMA)||'%')) AND KD_AKTIF='A'";
             * */
            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str).ToList();
            connection.Close();

            return exec;
        }


        public List<AUTOCOMPLETE_CUSTOMER> GetDDCustomer(string MPLG_NAMA, string MPLG_KODE)
        {
            IDbConnection connection = DatabaseFactory.GetConnection();
            string str = "SELECT MPLG_KODE code, MPLG_NAMA label, MPLG_ALAMAT address, MPLG_KODE_SAP sap " +
                         "FROM vw_customers WHERE MPLG_KODE LIKE '%'|| :MPLG_KODE || '%' AND UPPER(MPLG_NAMA) LIKE '%'|| UPPER(:MPLG_NAMA) || '%' AND KD_AKTIF='A'";

            List<AUTOCOMPLETE_CUSTOMER> exec = connection.Query<AUTOCOMPLETE_CUSTOMER>(str, new
            {
                MPLG_NAMA = MPLG_NAMA,
                MPLG_KODE = MPLG_KODE
            }).ToList();
            connection.Close();

            return exec;
        }

    }
}