﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Remote.Controllers;
using Remote.Helpers;
using Remote.Models.MasterBusinessEntity;
using Remote.Models.MasterRentalObject;
using Remote.Entities;

namespace Remote.DAL
{
    public class MapsGeoDAL
    {
        public dynamic PinPoint(string BRANCH_ID)
        {
            dynamic result = null;
            DataBE coordinate = new DataBE();
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string sql = "select LATITUDE, LONGITUDE from BUSINESS_ENTITY where BRANCH_ID =:a";

                    coordinate = connection.Query<DataBE>(sql, new { a = BRANCH_ID }).DefaultIfEmpty().FirstOrDefault();
                    if(coordinate == null)
                    {
                        coordinate = connection.Query<DataBE>(sql, new { a = 0 }).DefaultIfEmpty().FirstOrDefault();
                    }
                    else
                    {
                        if (coordinate.LATITUDE == null || coordinate.LONGITUDE == null)
                        {
                            coordinate = connection.Query<DataBE>(sql, new { a = 0 }).DefaultIfEmpty().FirstOrDefault();
                        }
                    }
                    

                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                    throw;
                }
            }

            //connection.Close();
            return result;
        }
        public dynamic PinRO(string BRANCH_ID, string STATUS, string ADDRESS)
        {
            dynamic result = null;
            
            List<AUP_REPORT_RO> coordinate = new List<AUP_REPORT_RO>();
            //IDbConnection connection = DatabaseFactory.GetConnection();
            //if (connection.State.Equals(ConnectionState.Closed))
            //    connection.Open();
            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string where = " where a.LONGITUDE is not null and a.LATITUDE is not null ";
                    where += BRANCH_ID == null || BRANCH_ID == "" ? "" : " and a.BRANCH_ID = " + BRANCH_ID;
                    where += STATUS == null || STATUS == "" ? "" : " and STATUS = '" + STATUS + "'";
                    where += ADDRESS == null || ADDRESS == "" ? "" : "  and lower(a.RO_ADDRESS) like '%" + ADDRESS.ToLower() + "%' ";
                    //string sql = @"select a.STATUS,a.CONTRACT_NO,a.LONGITUDE, a.LATITUDE, a.RO_NUMBER, a.RO_NAME, a.RO_CODE,a.RO_ADDRESS from V_REPORT_RO a " + where;
                    string sql = @"SELECT A.STATUS,A.CONTRACT_NO, A.RO_NUMBER, A.RO_NAME, A.RO_CODE, A.RO_ADDRESS, A.ZONE_RIP, A.LAND_DIMENSION,
                                TO_CHAR(nvl(B.CONTRACT_START_DATE,D.CONTRACT_START_DATE),'DD-MM-YYYY') START_DATE , TO_CHAR(NVL(B.CONTRACT_END_DATE,D.CONTRACT_END_DATE),'DD-MM-YYYY') END_DATE ,
                                NVL(C.LATITUDE,A.LATITUDE) LATITUDE, NVL(C.LONGITUDE, A.LATITUDE) LONGITUDE
                                FROM (select NVL(b.STATUS,'VACANT') STATUS,NVL(b.CONTRACT_NO,b.CONTRACT_OFFER_NO)CONTRACT_NO,NVL(b.LONGITUDE,a.LONGITUDE)LONGITUDE, NVL(b.LATITUDE,a.LATITUDE)LATITUDE
                                , NVL(b.RO_NUMBER, a.RO_NUMBER)RO_NUMBER, NVL(b.RO_NAME, a.RO_NAME)RO_NAME, NVL(b.RO_CODE, a.RO_CODE)RO_CODE,NVL(b.RO_ADDRESS, a.RO_ADDRESS)RO_ADDRESS 
                                , B.ZONE_RIP,CASE WHEN B.LAND_DIMENSION = 0 THEN B.BUILDING_DIMENSION ELSE B.LAND_DIMENSION END LAND_DIMENSION from RENTAL_OBJECT a
                                left join (select a.STATUS,a.CONTRACT_NO,a.CONTRACT_OFFER_NO,a.LONGITUDE, a.LATITUDE, a.RO_NUMBER, a.RO_NAME, a.RO_CODE,a.RO_ADDRESS, A.ZONE_RIP, A.LAND_DIMENSION, A.BUILDING_DIMENSION 
                                from V_REPORT_RO a " + where + " and VALID_FROM <=current_date AND VALID_TO >= current_date )b on a.RO_CODE = b.RO_CODE " + where +
                                @") A LEFT JOIN PROP_CONTRACT B ON B.CONTRACT_NO = A.CONTRACT_NO 
                                left join RENTAL_OBJECT_MAPS C ON C.RO_NUMBER = A.RO_NUMBER 
                                LEFT JOIN PROP_CONTRACT_OFFER D ON D.CONTRACT_OFFER_NO = A.CONTRACT_NO AND D.COMPLETED_STATUS <> 1
                                ORDER BY A.RO_NUMBER, C.NO_URUT";

                    coordinate = connection.Query<AUP_REPORT_RO>(sql).ToList();
                    //string sql_co = @"select a.CONTRACT_OFFER_NUMBER CONTRACT_OFFER_NO, c.MPLG_KODE, c.MPLG_NAMA, c.MPLG_BADAN_USAHA, a.CUSTOMER_ID, a.BRANCH_ID,b.OBJECT_ID RO_CODE
                    //from PROP_RENTAL_REQUEST_DETIL b
                    //join PROP_RENTAL_REQUEST a on a.RENTAL_REQUEST_NO = b.RENTAL_REQUEST_NO 
                    
                    //left join VW_CUSTOMERS c on a.CUSTOMER_ID  = c.MPLG_KODE and to_char(a.BRANCH_ID) = to_char(c.KD_CABANG)
                    //where b.OBJECT_ID =:a ";//and a.CONTRACT_START_DATE <= current_date and a.CONTRACT_END_DATE >= current_date
                    ////var data_co = connection.Query<AUP_REPORT_RO>(sql_co).ToList();
                    //foreach (var temp in coordinate) {

                    //    if (temp.STATUS == "BOOKED")
                    //    {
                    //        var data_co = connection.Query<AUP_REPORT_RO>(sql_co, new { a = temp.RO_CODE }).FirstOrDefault();
                    //        if (data_co != null)
                    //        {
                    //            temp.MPLG_KODE = data_co.MPLG_KODE;
                    //            temp.MPLG_NAMA = data_co.MPLG_NAMA;
                    //            temp.MPLG_BADAN_USAHA = data_co.MPLG_BADAN_USAHA;
                    //            temp.CONTRACT_OFFER_NO = data_co.CONTRACT_OFFER_NO;
                    //        }
                            
                    //    }
                    //}
                    result = new
                    {
                        status = "S",
                        message = coordinate
                    };

                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                    throw;
                }
            }

            //connection.Close();
            return result;
        }

        public dynamic GetImage(string RO_NUMBER)
        {
            dynamic result = null;
            List<AUP_RO_ATTACHMENT> gambar = new List<AUP_RO_ATTACHMENT>();

            using (var connection = DatabaseFactory.GetConnection("default"))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                   
                    string sql = @"SELECT ID, DIRECTORY, FILE_NAME, RO_NUMBER FROM RENTAL_OBJECT_ATTACHMENT WHERE RO_NUMBER=:a ORDER BY ID DESC ";

                    gambar = connection.Query<AUP_RO_ATTACHMENT>(sql,new { a = RO_NUMBER }).ToList();
                    
                    result = new
                    {
                        status = "S",
                        message = gambar,
                    };

                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                    throw;
                }
            }

            return result;

        }
    }
}