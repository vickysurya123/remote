﻿namespace Remote.Entities
{
    public class VW_APP_USER_MENU
    {
        public long? MENU_ID { get; set; }
        public string MENU_NAMA { get; set; }
        public string MENU_URL { get; set; }
        public string MENU_ALIAS { get; set; }
        public long? MENU_VISIBLE { get; set; }
        public long? MENU_STATUS { get; set; }
        public string MENU_ACTIVITY { get; set; }
        public string MENU_ICON { get; set; }
        public long? MENU_GROUP_ID { get; set; }
        public long? USER_ID { get; set; }
        public long? MENU_PARENT_ID { get; set; }
        public long? ORDERED_BY { get; set; }
        public long? STATUS { get; set; }
        public int CHILD_COUNT { get; set; }
    }
}
