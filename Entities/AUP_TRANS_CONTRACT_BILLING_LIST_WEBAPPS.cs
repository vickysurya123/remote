﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS
    {
        //HEADER DATA CONTRACT BILLING POST
        public long? ENDING_BALANCE_AMOUNT { get; set; }
        public string BILLING_FLAG_TAX1 { get; set; }
        public string BILLING_FLAG_TAX2 { get; set; }
        public string CREATION_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string PROFIT_CENTER_CODE { get; set; }
        public long? COMPANY_CODE { get; set; }
        public long? FREQ_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string NO_REF1 { get; set; }
        public string NO_REF2 { get; set; }
        public long? PROFIT_CENTER_ID { get; set; }
        public long? BE_ID { get; set; }
        public string BILLING_DUE_DATE { get; set; }
        public long? BILLING_PERIOD { get; set; }
        public string NO_REF3 { get; set; }
        public string CURRENCY_CODE { get; set; }
        public long? BEGIN_BALANCE_AMOUNT { get; set; }
        public long? INSTALLMENT_AMOUNT { get; set; }
        public string BILLING_NO { get; set; }
        public long? ID { get; set; }
        public long? BILLING_ID { get; set; }
        public long? BILLING_CONTRACT_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_CODE_SAP { get; set; }
        public string BE_NAME { get; set; }
        public string POSTING_DATE { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CANCEL_STATUS { get; set; }

        //DETAIL DATA CONTRACT BILLING POST D
        public long? BILLING_QTY { get; set; }
        public string BILLING_UOM { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string GL_ACOUNT_NO { get; set; }
        public long? SEQ_NO { get; set; }
        public string OBJECT_ID { get; set; }
        public string REF_DESC { get; set; }
        public string BRANCH_ID { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_CITY { get; set; }
        public string MPLG_ALAMAT { get; set; }
        public int LIVE_1PELINDO { get; set; }

    }
}