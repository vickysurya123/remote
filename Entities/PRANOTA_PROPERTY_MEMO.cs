﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class PRANOTA_PROPERTY_MEMO
    {
        public string BILLING_NO { get; set; }
        public decimal CONTRACT_NO { get; set; }
        public string PERHITUNGAN { get; set; }
        public string MEMO { get; set; }
    }
}