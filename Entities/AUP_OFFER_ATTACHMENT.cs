﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{ 
    public class AUP_OFFER_ATTACHMENT
    {
        public string ID_ATTACHMENT { get; set; }
        public Nullable<int> ID { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME { get; set; }
        public string BRANCH_ID { get; set; }
        public string TARIF_LAHAN_ID { get; set; }
        public string CONTRACT_OFFER_ID { get; set; }
    }
}