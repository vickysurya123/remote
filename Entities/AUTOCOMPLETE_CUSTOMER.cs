﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUTOCOMPLETE_CUSTOMER
    {
        public string code { get; set; }
        public string label { get; set; }
        public string address { get; set; }
        public string sap { get; set; }
        public string name { get; set; }

        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string BRANCH_ID { get; set; }

        public string BUSINESS_PARTNER { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
    }
}