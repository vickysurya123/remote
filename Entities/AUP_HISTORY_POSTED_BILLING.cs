﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_HISTORY_POSTED_BILLING
    {
        public string BILLING_ID { get; set; }
        public string BILLING_CONTRACT_NO { get; set; }
        public string BILLING_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string BILLING_DUE_DATE { get; set; }
        public string BILLING_PERIOD { get; set; }

        // Detail Data
        public string CONDITION_TYPE { get; set; }
        public string OBJECT_ID { get; set; }
        public string INSTALLMENT_AMOUNT { get; set; }
    }
}