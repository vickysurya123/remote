﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class EXP_NOTIF
    {
        public int ID { get; set; }
        public int DAYS { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public int STATUS { get; set; }
    }
}