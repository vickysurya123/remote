﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_WE_LIST_WEBAPPS
    {
        public string ID { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string INSTALLATION_ID { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PERIOD { get; set; }
        public string F_PERIOD { get; set; }
        public string REPORT_ON { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string PRICE { get; set; }
        public string AMOUNT { get; set; }
        public string RATE { get; set; }
        public string GRANDTOTAL { get; set; }
        public string GRAND_TOTAL_USD { get; set; }
        public string STATUS { get; set; }
        public string STATUS_DINAS { get; set; }
        public string BILLING_TYPE { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string POSTING_DATE { get; set; }
        public string F_REPORT_ON { get; set; }
        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public Int64 GRAND_TOTAL { get; set; }
        public string MINIMUM_AMOUNT { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string BRANCH_ID_1 { get; set; }
        public string BRANCH_ID { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string MINIMUM_USED { get; set; }
        public string MINIMUM_PAYMENT { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string MULTIPLY_FACT { get; set; }
        public string BIAYA_ADMIN { get; set; }
        public string DESCRIPTION { get; set; }
        public string TARIFF_CODE { get; set; }
        public string BIAYA_BEBAN { get; set; }

        // HEADER DARI V_ANJUNGAN
        public string KD_TRANSAKSI { get; set; }
        public string KODE_MDM { get; set; }
        public string NAMA_CUSTOMER_MDM { get; set; }
        public string DESKRIPSI_PROFIT_CENTER { get; set; }
        public string PPJU { get; set; }
        public string REDUKSI { get; set; }
        public string KD_CABANG { get; set; }

        // DETAIL DARI V_ANJUNGAN_DETAIL
        public string PRICE_TYPE { get; set; }
        public string CURRENCY { get; set; }
        public string TARIFF { get; set; }
        public string USED { get; set; }
        public string COA_PROD { get; set; }
        public string URL_FOTO { get; set; }

        // DETAIL DARI USED_DETAIL
        public string SUMBER { get; set; }
        public string FAKTOR_PENGALI { get; set; }
        public string TGL_STAN_AWAL { get; set; }
        public string TGL_STAN_AKHIR { get; set; }
        public string STAN_AWAL { get; set; }
        public string STAN_AKHIR { get; set; }
        public string SELISIH { get; set; }
        public string KWH_TENAN { get; set; }
        public string KWH_DITAGIHKAN { get; set; }
        public string TOTAL { get; set; }
        public string ID_TRANSACTION { get; set; }
    }
}