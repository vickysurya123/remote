﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class LIST_NOTIFIKASI_HEADER
    {
        public int ID { get; set; }
        public int MODUL_ID { get; set; }
        public int BE_ID { get; set; }
        public int BRANCH_ID { get; set; }
        public string BE_NAME { get; set; }
    }
}