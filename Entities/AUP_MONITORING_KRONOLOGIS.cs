﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_MONITORING_KRONOLOGIS
    {

        public string ID { get; set; }
        public string JUDUL { get; set; }
        public string BE_ID { get; set; }
        public string TANGGAL { get; set; }
        public string NO_SURAT { get; set; }
        public string TANGGAL_SURAT { get; set; }
        public string KENDALA { get; set; }
        public string CABANG { get; set; }
        public string REGIONAL { get; set; }

        public string TINDAK_LANJUT { get; set; }
        public string PROGRESS { get; set; }
        public string FILE_NAME { get; set; }
        public string KETERANGAN { get; set; }
        public string DIRECTORY { get; set; }
        public string KRONOLOGIS_ID { get; set; }

    }
}