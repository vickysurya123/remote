﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Entities
{
    public class AUP_REPORT_PRODUKSI
    {
        public string CONTRACT_NO { get; set; }
        public string SATUAN { get; set; }
        public string LUAS { get; set; }
        public string OBJECT_ID { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CODE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string BILLING_NO { get; set; }
        public string BE_ID { get; set; }
        public int NO { get; set; }
        public string COA_PROD { get; set; }
    }
}
