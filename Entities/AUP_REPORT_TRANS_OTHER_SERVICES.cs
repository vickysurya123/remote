﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_TRANS_OTHER_SERVICES
    {
        public string ID { get; set; }
        public string POSTING_DATE { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string SERVICES_GROUP { get; set; }
        public string SERVICE_CODE { get; set; }
        public string COSTUMER_ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string TOTAL { get; set; }
        public string FI_POST_STATUS { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string BILLING_TYPE { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string COMPANY_CODE { get; set; }
        public string TAX_CODE { get; set; }
        public string NO_TAX_TOTAL { get; set; }
        public string BRANCH_ID { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string UNIT { get; set; }
        public string ACTIVE { get; set; }
        public string PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string MULTIPLY_FUNCTION { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string CONTRACT_DATE { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string QUANTITY { get; set; }
        public string SURCHARGE { get; set; }
        public string AMOUNT { get; set; }
        public string COSTUMER_MDM_NAME { get; set; }
        public string SERVICES_GROUP_NAME { get; set; }
        public string BE_NAME { get; set; }
 
    }
}