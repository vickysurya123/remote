﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_WE_WEBAPPS
    {
        public string ID { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string PERIOD { get; set; }
        public string F_PERIOD { get; set; }
        public string REPORT_ON { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string PRICE { get; set; }
        public float AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string STATUS { get; set; }
        public string BILLING_TYPE { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string FI_POST_STATUS { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_ID { get; set; }
        public float X_FACTOR { get; set; }
        public string MINIMUM_AMOUNT { get; set; }
        public string STATUS_DINAS { get; set; }
    }

    public class AUP_TRANS_WE_INSTALLATION_WEBAPPS
    {
        public string ID { set; get; }
        public string INSTALLATION_NUMBER { set; get; }
        public string INSTALLATION_TYPE { set; get; }
        public string BE_ID { set; get; }
        public string CUSTOMER_ID { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string INSTALLATION_DATE { set; get; }
        public string INSTALLATION_ADDRESS { set; get; }
        public string TARIFF_CODE { set; get; }
        public string MINIMUM_AMOUNT { set; get; }
        public decimal SURCHARGE { set; get; }
        public decimal GRAND_TOTAL { set; get; }
        public string CURRENCY { set; get; }
        public string TAX_CODE { set; get; }
        public string RO_CODE { set; get; }
        public string STATUS { set; get; }
        public string POWER_CAPACITY { set; get; }
        public string BE_ID_1 { get; set; }
        public string BE_NAME { get; set; }
        public string RO_NAME { get; set; }
        public decimal AMOUNT { get; set; }
        public string CURRENCY_1 { get; set; }
        public string ID_INSTALASI { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string BE_ADDRESS { get; set; }
        public string PERIOD { get; set; }
        public decimal PRICE { get; set; }
        public decimal BIAYA_ADMIN { get; set; }
        public string PERCENTAGE { get; set; }
        public string TARIFF { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string BIAYA_BEBAN { get; set; }
        public string PPJU { get; set; }
        public string REDUKSI { get; set; }
        public string BE_CITY { get; set; }
        public string POSTING_DATE { get; set; }
        public string BRANCH_ID { get; set; }
        public int LIVE_1PELINDO { get; set; }
    }

}