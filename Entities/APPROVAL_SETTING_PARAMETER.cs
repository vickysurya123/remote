﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class APPROVAL_SETTING_PARAMETER
    {
        public Nullable<int> ID { get; set; }
        public string PARAMETER_NAME { get; set; }
        public string PARAMETER_COLUMN { get; set; }
        public string PARAMETER_TABLE { get; set; }
    }
}