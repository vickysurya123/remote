﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANSVB_WEBAPPS
    {
        // Header Data
        public string ID { get; set; }
        public string POSTING_DATE { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string COSTUMER_ID { get; set; }
        public string BUSINESS_ENTITY { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string SERVICES_GROUP { get; set; }
        public string COSTUMER_NAME { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string TAX_CODE { get; set; }
        //public string TOTAL { get; set; }
        public decimal TOTAL { get; set; }
        public string POS_DATE { get; set; }
        public string PROF_CENTER { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string BILLING_TYPE { get; set; }
        public string BRANCH_ID { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string VAL2 { get; set; }
        public string KELOMPOK_PENDAPATAN { get; set;  }
        public string BE_NAME { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_CITY { get; set; }

        //Detail Data
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string CURRENCY { get; set; }
        public string PRICE { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string SURCHARGE { get; set; }
        public string AMOUNT { get; set; }
        public string RATE { get; set; }
        public string TOTAL_USD { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string SUB_TOTAL { get; set; }
        public string REMARK { get; set; }
        public string VB_ID { get; set; }

        public string S_DATE { get; set;  }
        public string E_DATE { get; set; }
        public string CM_PRICE { get; set; }
        public string PPH_CODE { get; set; }
        public string COA_PROD { get; set; }

        public int ROLE_OTHER { get; set; }
        public int LIVE_1PELINDO { get; set; }
    }

    public class AUP_RETURN_TRANS_NUMBER
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }

    public class AUP_RETURN_SAP_DOCUMENT_NUMBER
    {
        public string DOCUMENT_NUMBER { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}