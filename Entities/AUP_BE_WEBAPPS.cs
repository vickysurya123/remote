﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_BE_WEBAPPS
    {
        public string BE_ID { get; set; }

        public string BE_CODE { get; set; }
        
        public string BE_NAME { get; set; }

        public string BE_ADDRESS { get; set; }

        public string BE_CITY { get; set; }

        public string BE_PROVINCE { get; set; }

        public string BE_CONTACT_PERSON { get; set; }

        public string BE_NPWP { get; set; }

        public string BE_NOPPKP { get; set; }

        public string BE_PPKP_DATE { get; set; }

        public string CREATION_BY { get; set; }

        public string CREATION_DATE { get; set; }

        public string LAST_UPDATE_BY { get; set; }

        public string LAST_UPDATE_DATE { get; set; }

        public string VALID_FROM { get; set; }

        public string VALID_TO { get; set; }

        public string HARBOUR_CLASS { get; set; }

        public string POSTAL_CODE { get; set; }

        public string MEASUREMENT_TYPE { get; set; }

        public string DESCRIPTION { get; set; }

        public string AMOUNT { get; set; }

        public string UNIT { get; set; }

        public string ID { get; set; }

        public string PHONE_1 { get; set; }

        public string PHONE_2 { get; set; }

        public string FAX_1 { get; set; }
        
        public string FAX_2 { get; set; }

        public string EMAIL { get; set; }

        public string REF_DESC { get; set; }

        public string VAL_FROM { set; get; }

        public string VAL_TO { set; get; }

        public string HB_ID { set; get; }

        public string BE_PROVINCE_ID { set; get; }

        public string ACTIVE { set; get; }
        public string fileName { get; set; }
    }

    public class AUP_RETURN_BE_NUMBER
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}