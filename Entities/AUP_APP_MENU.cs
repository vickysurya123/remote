﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 

namespace Remote.Entities
{
    public class AUP_APP_MENU
    {
        public string MENU_ID { get; set; }
        public string MENU_NAMA { get; set; }
        public string MENU_URL { get; set; }
        public string MENU_ALIAS { get; set; }
        public string MENU_ICON { get; set; }
    }
}