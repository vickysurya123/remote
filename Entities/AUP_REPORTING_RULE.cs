﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORTING_RULE
    {
        public string ID { get; set; }
        public string CONTRACT_NO { get; set; }
        public string RR_STATUS { get; set; }
        public string REPORT_FROM { get; set; }
        public string REPOT_TO { get; set; }
        public string REPORT_ON { get; set; }
        public string ZERO_SALES { get; set; }
        public string UNIT { get; set; }
        public string NET_SALES { get; set; }
        public string RR_TOTA { get; set; }
        public string BILLING_NO { get; set; }
    }
}