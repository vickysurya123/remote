﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
	public class PRANOTA_OTHER_DETAIL
	{
        public string SERVICE_NAME { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string REMARK { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public double PRICE { get; set; }
        public double AMOUNT { get; set; }
        public string VB_ID { get; set; }
        public string multiply_factor { get; set; }
    }
}