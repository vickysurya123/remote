﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;  

namespace Remote.Entities
{
    public class AUP_REPORT_DOCUMENT_FLOW
    {
        public string BUSINESS_ENTITY { get; set; }
        public string RENTAL_REQUEST_NUMBER { get; set; }
        public string RENTAL_REQUEST_CREATED_BY { get; set; } 
        public string DATE_RENTAL_REQUEST_CREATE { get; set; }
        public string RENTAL_REQUEST_STATUS { get; set; }
        public string DATE_RENTAL_REQUEST_APPROVED { get; set; }
        public string CONTRACT_OFFER_NUMBER { get; set; }
        public string CONTRACT_OFFER_LAST_STATUS { get; set; }
        public string DELAYS_RENTAL_REQUEST_APPROVED { get; set; }
        public string DATE_OFFER_CREATED { get; set; }
        public string DELAYS_CONTRACT_OFFER_APPROVED { get; set; }
        public string RELEASE_TO_WORKFLOW { get; set; }
        public string DELAYS_RELEASE_TO_WORKFLOW { get; set; }
        public string APPROVED_BY_MANAGER { get; set; }
        public string DELAYS_APPROVE_BY_MANAGER { get; set; }
        public string APPROVED_BY_DEPT_GM { get; set; }
        public string DELAYS_APPROVE_BY_DEPT_GM { get; set; }
        public string APPROVED_BY_GM { get; set; }
        public string DELAYS_APPROVE_BY_GM { get; set; }
        public string APPROVED_BY_SM { get; set; }
        public string DELAYS_APPROVE_BY_SM { get; set; }
        public string APPROVED_BY_DIR { get; set; }
        public string DELAYS_APPROVE_BY_DIR { get; set; }
        public string APPROVED_BY_KOM { get; set; }
        public string APPROVED_BY_RUPS { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CREATION_DATE { get; set; }

    }
}