﻿namespace Remote.Controllers
{
    public class AUP_REPORT_RO
    {
        public string RO_NUMBER { get; set; }
        public string BRANCH_ID { get; set; }
        public string RO_CODE { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }  
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string MEMO { get; set; }
        public string RO_CITY { get; set; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string BE_ID { get; set; }
        public string BE_NAME { set; get; }
        public string USAGE_TYPE { set; get; }
        public string RO_PROVINCE { get; set; }
        public string PROVINCE { set; get; }
        public string ZONE_RIP { set; get; }
        public string KODE_ASET { set; get; }
        public string FUNCTION_NAME { set; get; }
        public string FUNCTION_ID { set; get; }
        public string PROFIT_CENTER { set; get; }
        public string PROFIT_CENTER_ID { set; get; }
        public string PROVINCE_ID { set; get; }
        public string USAGE_TYPE_ID { set; get; }
        public string LOCATION_ID { set; get; }
        public string ZONE_RIP_ID { set; get; }
        public string VAL_FROM { set; get; }
        public string VAL_TO { set; get; }
        public string ACTIVE { set; get; }
        public string STATUS { set; get; }
        public string TERMINAL_NAME { set; get; }
        public string REASON { set; get; }
        public string LAND_DIMENSION { set; get; }
        public string BUILDING_DIMENSION { set; get; }
        public string AVAILABLE_DATE { set; get; }
        public string CONTRACT_NO { set; get; }
        public string CONTRACT_OFFER_NO { set; get; }
        public string MPLG_KODE { set; get; }
        public string MPLG_NAMA { set; get; }
        public string MPLG_BADAN_USAHA { set; get; }
        public int JUMLAH { set; get; }
        public string LATITUDE { set; get; }
        public string LUAS_LAHAN { set; get; }
        public string LUAS_TANAH { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string TERM_IN_MONTHS { set; get; }
        public string LONGITUDE { set; get; }
        public string START_DATE { set; get; }
        public string END_DATE { set; get; }
        public string KantorPusat { set; get; }
        public string Customer { set; get; }
        public string Iddle { set; get; }
        public string KantprPusatBersertifikat { set; get; }
        public string CustomerBersertifikat { set; get; }
        public string IddleBersertifikat { set; get; }
        public string BULAN { set; get; }
        public string BUSINESS_PARTNER_NAME { set; get; }
        public string SERTIFIKAT_ID { set; get; }
        public string SERTIFIKAT_TYPE { set; get; }
        public string CONTRACT_START_DATE { set; get; }
        public string CONTRACT_END_DATE { set; get; }
        public string TOTAL { set; get; }
    }
}