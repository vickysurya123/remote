﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_VARIOUS_BUSINESS_PRICING_Remote
    {
        public string ID { set; get; }
        public string BE_ID { set; get; }
        public string BE_NAME { set; get; }
        public string SERVICE_CODE { set; get; }
        public string SERVICE_NAME { set; get; }
        public string CONDITION_TYPE { set; get; }
        public string PROFIT_CENTER_ID { set; get; }
        public string PROFIT_CENTER_NAME { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string MULTIPLY_FUNCTION { set; get; }
        public string CONDITION_PRICING_UNIT { set; get; }
        public string ACTIVE { set; get; }

        //------------------------

        public string PROFIT_CENTER { set; get; }

        public string SERVICE_GROUP { set; get; }

        public string UNIT { set; get; }

        public string GL_ACCOUNT { set; get; }

        //-------------------------
        public string P_CENTER { set; get; }

    }
}