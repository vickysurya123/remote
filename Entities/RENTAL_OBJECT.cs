﻿using System;
namespace Remote.Models
{
    public class RENTAL_OBJECT
    {
        public long? RO_NUMBER { get; set; }
        public string BE_ID { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string RO_CITY { get; set; }
        public string RO_PROVINCE { get; set; }
        public string MEMO { get; set; }
        public string CREATION_BY { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string RO_TYPE_ID { get; set; }
        public string ASSET_SAP_NUMBER { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public DateTime? VALID_TO { get; set; }
        public string ZONE_RIP_ID { get; set; }
        public string LOCATION_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string USAGE_TYPE_ID { get; set; }
        public string PROVINCE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public long? ACTIVE { get; set; }
    }
}