﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_CONTRACT_OFFER_Remote
    {
        //Data Header
        public string BE_ID { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string CONTRACT_OFFER_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CURRENCY { get; set; }
        public string CONTRACT_OFFER_TYPE { get; set; }
        public string OFFER_STATUS { get; set; }

        //Data Detail
        public string ID { get; set; }
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string MONTHS { get; set; }
        public string UNIT_PRICE { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string CONTRACT_OFFER_NO_1 { get; set; }
        public string MEMO { get; set; }
             
    }
}