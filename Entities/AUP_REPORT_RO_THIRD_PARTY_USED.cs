﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_RO_THIRD_PARTY_USED
    {
        public string CONTRACT_NO { get; set; }
        public string BE_ID { get; set; }
        public string BE_NAME { get; set; }
        public string VALID_FROM { get; set; }
        public string USAGE_TYPE { get; set; }
        public string USAGE_TYPES { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string KELOMPOK_USAHA { get; set; }
        public string RO_ADDRESS { get; set; }
        public string LUAS { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CONTRACT_USAGES { get; set; }
        public string ZONA_RIP { get; set; }
        public string ZONA_RIPS { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string JENIS_TARIF_Z003 { get; set; }
        public string INSTALLMENT_AMOUNT_Z003 { get; set; }
        public string JENIS_TARIF_Z005 { get; set; }
        public string INSTALLMENT_AMOUNT_Z005 { get; set; }
        public string JENIS_TARIF_Z007 { get; set; }
        public string INSTALLMENT_AMOUNT_Z007 { get; set; }
        public string JENIS_TARIF_Z008 { get; set; }
        public string INSTALLMENT_AMOUNT_Z008 { get; set; }
        public string JENIS_TARIF_Z013 { get; set; }
        public string INSTALLMENT_AMOUNT_Z013 { get; set; }
        public string JENIS_TARIF_Z014 { get; set; }
        public string INSTALLMENT_AMOUNT_Z014 { get; set; }
        public string INSTALLMENT_AMOUNT { get; set; }
    }
}