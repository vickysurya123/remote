﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace Remote.Entities
{
    public class AUP_NOTIFICATION_CONTRACT 
    {
        public string CONTRACT_END_DATE { get; set; }
        public string WARNING_DAYS { get; set; }
        public string DAYS { get; set; }
        public int BRANCH_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string ADDRESS {get; set;}
        public string CONTRACT_NO {get; set;}
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_TYPE_DESC { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_NAME { get; set; }
        public string RO_ADDRESS { get; set; }
        public string BE_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string STOP_NOTIFICATION_DATE { get; set; }
        public string STOP_NOTIFICATION_BY { get; set; }
        public string STOP_WARNING_DATE { get; set; }
        public string STOP_WARNING_BY { get; set; }
        public string LEAVE_OUT_DATE { get; set; }
        public string LEAVE_OUT_BY { get; set; }
        public string MEMO_NOTIFICATION { get; set; }
        public string MEMO_WARNING { get; set; }
        public string STATUS { get; set; }
        public string BADAN_USAHA { get; set; }
        public bool FIRST { get; set; }
        public bool SECOND { get; set; }
        public bool THIRD { get; set; }
    }
}