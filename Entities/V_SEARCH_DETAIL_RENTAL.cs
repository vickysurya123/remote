﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class V_SEARCH_DETAIL_RENTAL
    {
        public long? RO_NUMBER { get; set; }
        public string RO_NAME { get; set; }
        public string LUAS_BANGUNAN_RO { get; set; }
        public string LUAS_TANAH_RO { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_TYPE_ID { get; set; }
    }
}