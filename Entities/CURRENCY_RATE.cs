﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class CURRENCY_RATE
    {
        public Nullable<int> ID { get; set; }
        public string CODE { get; set; }
        public string CODE_X { get; set; }
        public string CODE_Y { get; set; }
        public int CURRENCY_ID { get; set; }
        public int CURRENCY_FROM { get; set; }
        public int CURRENCY_TO { get; set; }
        public int MULTIPLY_RATE { get; set; }
        public int DEVIDE_RATE { get; set; }
        public int USER_ID_CREATE { get; set; }
        public Nullable<int> USER_ID_UPDATE { get; set; }
        public string VALIDTY_TO { get; set; }
        public string VALIDITY_FROM { get; set; }
    }
}