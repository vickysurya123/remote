﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class Customer
    {
        public int KD_CABANG { get; set; }
        public string MPLG_PROFIT_CENTER { get; set; }
        public string MPLG_TIPE { get; set; }
        public string MPLG_KODE_SAP { get; set; }
        public string MPLG_KODE { get; set; }
        public string MPLG_NAMA { get; set; }
        public string MPLG_ALAMAT { get; set; }
        public string MPLG_KOTA { get; set; }
        public string MPLG_JENIS_USAHA { get; set; }
        public string MPLG_BADAN_USAHA { get; set; }
        public string MPLG_CONT_PERSON { get; set; }
        public string MPLG_TELEPON { get; set; }
        public string MPLG_EMAIL_ADDRESS { get; set; }
        public string MPLG_FAX { get; set; }
        public string MPLG_NPWP { get; set; }
        public string MPLG_SIUP { get; set; }
    }
}