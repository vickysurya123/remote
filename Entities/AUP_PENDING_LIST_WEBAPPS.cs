﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_PENDING_LIST_WEBAPPS
    {
        //DATA HEADER OFFER
        public string CONTRACT_OFFER_TYPE { get; set; }
        public long? CONTRACT_NUMBER { get; set; }
        public long? ID_INC { get; set; }
        public long? CONTRACT_OFFER_NO { get; set; }
        public string STATUS_CONTRACT_DESC { get; set; }
        public string BE_ID { get; set; }
        public long? RENTAL_REQUEST_NO { get; set; }
        public string CONTRACT_OFFER_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public long? TERM_IN_MONTHS { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CURRENCY { get; set; }
        public string OFFER_STATUS { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ACTIVE { get; set; }
        public string TOP_APPROVED_LEVEL { get; set; }
        public string APPROVAL_NEXT_LEVEL { get; set; }
        public string APV_DISPOSISI { get; set; }
        public string DISPOSISI_PARAF { get; set; }

        //DATA DETAIL OBJECT
        public string OBJECT_ID { get; set; }
        public long? ID { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_NAME { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string LUAS { get; set; }
        public string INDUSTRY { get; set; }
        public long? LUAS_TANAH { get; set; }
        public long? LUAS_BANGUNAN { get; set; }

        //DATA DETAIL CONDITION
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string CONDITION { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public long? MONTHS { get; set; }
        public long? STATISTIC { get; set; }
        public long? UNIT_PRICE { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string FORMULA { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public long? TOTAL { get; set; }
        public long? NJOP_PERCENT { get; set; }
        public long? KONDISI_TEKNIS_PERCENT { get; set; }
        public string SALES_RULE { get; set; }
        public long? TOTAL_NET_VALUE { get; set; }
        public string NET_VALUE { get; set; }
        public string COA_PROD { get; set; }

        //DATA DETAIL MEMO
        public string MEMO { get; set; }

        //DATA SALES RULE
        public string SALES_TYPE { get; set; }
        public string NAME_OF_TERM { get; set; }
        public string CALC_FROM { get; set; }
        public string CALC_TO { get; set; }
        public string UNIT { get; set; }
        public string PERCENTAGE { get; set; }
        public string TARIF { get; set; }
        public string RR { get; set; }
        public string MINSALES { get; set; }
        public string SR { get; set; }
        public string MINPRODUCTION { get; set; }
        public string PAYMENT_TYPE { get; set; }

        //DATA REPORTING RULE
        public string REPORTING_RULE_NO { get; set; }
        public string TERM_OF_REPORTING_RULE { get; set; }
        public string QUANTITY { get; set; }


        //WORKFLOW LOG
        public string STATUS_CONTRACT { get; set; }
        public string USER_LOGIN { get; set; }
        public string BRANCH_ID { get; set; }
        public string CREATION_DATE { get; set; }
        public string CREATION_BY { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string NOTES { get; set; }
        public string USER_NAME { get; set; }	
            
    }
}