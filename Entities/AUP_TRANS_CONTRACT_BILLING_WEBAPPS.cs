﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_CONTRACT_BILLING_WEBAPPS
    {
        //HEADER DATA
        public string LAST_UPDATE_DATE { get; set; }
        public string NO_REF1 { get; set; }
        public string NO_REF2 { get; set; }
        public string NO_REF3 { get; set; }
        public string CURRENCY_CODE { get; set; }
        public long? BEGIN_BALANCE_AMOUNT { get; set; }
        public long? INSTALLMENT_AMOUNT { get; set; }
        public long? ENDING_BALANCE_AMOUNT { get; set; }
        public string BILLING_FLAG_TAX1 { get; set; }
        public string BILLING_FLAG_TAX2 { get; set; }
        public string CREATION_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public long? FREQ_NO { get; set; }
        public long? ID { get; set; }
        public long? BE_ID { get; set; }
        public long? PROFIT_CENTER_ID { get; set; }
        public string PROFIT_CENTER_CODE { get; set; }
        public long? COMPANY_CODE { get; set; }
        public long? BILLING_CONTRACT_NO { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_CODE_SAP { get; set; }
        public string BILLING_DUE_DATE { get; set; }
        public long? BILLING_PERIOD { get; set; }
        public string BE_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string OLD_CONTRACT { get; set; }

        //DETAIL DATA
        public long? BILLING_QTY { get; set; }
        public string BILLING_UOM { get; set; }
        public long? BILLING_ID { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string GL_ACOUNT_NO { get; set; }
        public long? SEQ_NO { get; set; }
        public string OBJECT_ID { get; set; }
        public string CONDITION_TYPE_NAME { get; set; }
        public int ROLE_PROPERTY { get; set; }

    }

    public class AUP_RETURN_SAP_DOCUMENT_NUMBER_BILLING
    {
        public string DOCUMENT_NUMBER { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}