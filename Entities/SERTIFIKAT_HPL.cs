﻿using System;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Entities
{
    public class SERTIFIKAT_HPL
    {
        public Nullable<int> ID { get; set; }
        public string TANGGAL { get; set; }
        public string NO_SERTIFIKAT { get; set; }
        public string SERTIFIKAT_TYPE { get; set; }
        public int LUAS_LAHAN { get; set; }
        public string BE_NAME { get; set; }
        public string BUSINESS_ENTITY_ID { get; set; }
        public string MEMO { get; set; }
        public string ATTACHMENT { get; set; }
        public string PATCH { get; set; }

    }
}