﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Entities
{
    public class APP_USER
    {
        public int ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string USER_PASSWORD { get; set; }
        public string USER_EMAIL { get; set; }
        public string USER_PHONE { get; set; }
        public string USER_ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string KD_CABANG { get; set; }
        public string NAMA_CABANG { get; set; }
        public string KD_TERMINAL { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string USER_NAMA { get; set; }
        public string PROPERTY_ROLE { get; set; }
        public string PARAM1 { get; set; }
        public string PARAM2 { get; set; }
        public string PARAM3 { get; set; }
        //tambahan test user dev
        public string HAKAKSES { get; set; }
        public string NAMA { get; set; }
        public string kode { get; set; }
        public string pesan { get; set; }
    }

    public class DataReturnPortalSI
    {
        public string RESPON_TEXT { get; set; }
        public string ID_USER { get; set; }
        public string USERNAME { get; set; }
        public string NAMA { get; set; }     
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_PORTALSI> datas { get; set; }
    }
}