﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class MASTER_DATA_LAHAN
    {
        public Nullable<int> ID { get; set; }
        public int TAHUN { get; set; }
        public int LUAS_LAHAN { get; set; }
        public string BE_NAME { get; set; }
        public int USER_ID_CREATE { get; set; }
        public Nullable<int> USER_ID_UPDATE { get; set; }
        public Nullable<int> USER_ID_DELETE { get; set; }
        public int BUSINESS_ENTITY_ID { get; set; }
        public string IS_ACTIVE { get; set; }
    }
}