﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_DATA_USER
    {
        public int ID { get; set; } 
         
        public int APP_ID { get; set; }
        
        public string USER_LOGIN { get; set; }

        public string USER_PASSWORD { get; set; }

        public string USER_EMAIL { get; set; }

        public string USER_PHONE { get; set; }

        public string USER_NAME { get; set; }

        public string STATUS { get; set; }

        public int USER_ROLE_ID { get; set; }

        public int KD_CABANG { get; set; }

        public int KD_TERMINAL { get; set; }

        public string PROFIT_CENTER_ID { get; set; }
        
        public int PROPERTY_ROLE { get; set; }

        public string PARAM1 { get; set; }

        public string PARAM2 { get; set; }

        public string PARAM3 { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string BE_ID { get; set; }
        public string BE_NAME { get; set; }
    }

    public class AUP_RETURN_USER_NUMBER
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}