﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class MiddlewareCredentials
    {
        public string application_id { get; set; }
        public string user_name { get; set; }
        public string user_password { get; set; }
    }
}