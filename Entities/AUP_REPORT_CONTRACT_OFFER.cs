﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_CONTRACT_OFFER
    {
        public string CONTRACT_OFFER_NO { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string CONTRACT_OFFER_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string OFFER_TYPE { get; set; }
        public string CURRENCY { get; set; }
        public string OBJECT_ID { get; set; }
        public string RO_NAME { get; set; }
        public string INDUSTRY { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string CONTRACT_OFFER_STATUS { get; set; }
        public string CREATION_BY { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
    }
}