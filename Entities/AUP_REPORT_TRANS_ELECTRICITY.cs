﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_TRANS_ELECTRICITY
    {
        public string ID { get; set; }
        public string STATUS_DINAS { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER_MDM { get; set; }
        public string CUSTOMER_NAME {get; set;}
        public string INSTALLATION_CODE { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string MINIMUM_USED { get; set; }
        public string PERIOD { get; set; }
        public string MULTIPLY_FACT { get; set; }
        public string POSTING_DATE { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string GRAND_TOTAL { get; set; }
        public string BRANCH_ID { get; set; }
        public string METER_FROM_TO_LWBP { get; set; }
        public string USED_LWBP { get; set; }
        public string TARIFF_LWBP { get; set; }
        public string METER_FROM_TO_WBP { get; set; }
        public string USED_WBP { get; set; }
        public string TARIFF_WBP { get; set; }
        public string METER_FROM_TO_KVARH { get; set; }
        public string USED_KVARH { get; set; }
        public string TARIFF_KVARH { get; set; }
        public string METER_FROM_TO_BLOK { get; set; }
        public string USED_BLOK { get; set; }
        public string TARIFF_BLOK { get; set; }
        public string PPJU { get; set; }
        public string REDUKSI { get; set; }
        public string TARIFF_BLOK2 { get; set; }
        public string EXPENSE_TARIFF { get; set; }

    }
}