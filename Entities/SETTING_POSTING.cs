﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class SETTING_POSTING
    {
        // Header Data
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string USER_NAME { get; set; }
        public string CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        
        //Detail Data
        public int ID_DETAIL { get; set; }
        public int HEADER_ID { get; set; }
        public string JENIS_TRANSAKSI { get; set; }
        public string KODE_JENIS_TRANSAKSI { get; set; }

    }

}