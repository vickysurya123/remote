﻿using System;
namespace Remote.Models
{
    public class APP_USER_ACTIVITY
    {
        public long? ID { get; set; }
        public long? KD_CABANG { get; set; }
        public string USERNAME { get; set; }
        public string ID_ACTIVITY { get; set; }
        public string NAMA_ACTIVITY { get; set; }
        public long? IS_ACTIVE { get; set; }
        public DateTime? DATE_CREATED { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? DATE_MODIFIED { get; set; }
        public string MODIFIED_BY { get; set; }
    }
}