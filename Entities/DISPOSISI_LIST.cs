﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class DISPOSISI_LIST
    {
        public Nullable<int> ID { get; set; }
        public string CONTRACT_NO { get; set; }
        public string OFFER_TYPE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string CURRENT_STATUS { get; set; }
        public string NO_SURAT { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string SUBJECT { get; set; }
        public string ISI_SURAT { get; set; }
        public int IS_CREATE_SURAT { get; set; }
        public int IS_ACTION { get; set; }
        public int IS_ACTION_DISPOSISI { get; set; }
        public int PARAF_LEVEL { get; set; }
        public int URUTAN { get; set; }
        public string DISPOSISI_PARAF { get; set; }
        public string STATUS_KIRIM { get; set; }
        public string TANGGAL { get; set; }
        public string MEMO { get; set; }
        public string ATASNAMA { get; set; }
        public string DISPOSISI_MAX { get; set; }
        public string encodedId { get; set; }
        public int ROLE_ID { get; set; }
        public int APV_LEVEL { get; set; }
        public int EDIT { get; set; }
        public int STATUS_APV { get; set; }

    }
}