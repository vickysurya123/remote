﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class PRANOTA_ELECTRICITY
    {
        public string ID { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public double QUANTITY { get; set; }
        public string PRICE { get; set; }
        public string SUB_TOTAL { get; set; }
        public double AMOUNT { get; set; }
        public string BIAYA_ADMIN { get; set; }
        public string PRICE_TYPE { get; set; }
        public double TARIFF { get; set; }        

    }
}