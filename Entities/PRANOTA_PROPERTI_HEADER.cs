﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class PRANOTA_PROPERTI_HEADER
    {
      public string KD_CABANG { get; set; }
      public string BILLING_CONTRACT_NO { get; set; }
      public string BILLING_NO { get; set; }
      public string BILLING_ID { get; set; }
      public string ATAS_BEBAN { get; set; }
      public string ALAMAT { get; set; }
      public string PROFIT_CENTER { get; set; }
      public string PERHITUNGAN { get; set; }
      public string JANGKA_WAKTU { get; set; }
      public decimal TARIF_DASAR { get; set; }
      public string DASAR_HARGA { get; set; }
      public int LUAS { get; set; }
      public string NJOP { get; set; }
    }
}