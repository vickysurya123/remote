﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class LoginDev
    {
        public string HAKAKSES { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string NAMA_CABANG { get; set; }
        public string NAMA { get; set; }
        public string USER_LOGIN { get; set; }
        public int BE_ID { get; set; }
    }
}