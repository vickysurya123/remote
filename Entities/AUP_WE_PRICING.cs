﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_WE_PRICING
    {
        public string SERVICES_ID { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public string CURRENCY { get; set; }
        public string AMOUNT { get; set; }
        public string X_FACTOR { get; set; }
        public string FALID_FROM { get; set; }
        public string FALID_TO { get; set; }
        public string ACTIVE { get; set; }
        public string PER { get; set; }
        public string UNIT { get; set; }
        public string TARIFF_CODE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string BRANCH_ID { get; set; }
        public string NAMA_PROFIT_CENTER { get; set; }

    }
}