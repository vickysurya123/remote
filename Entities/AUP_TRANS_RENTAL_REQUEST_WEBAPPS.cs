﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_RENTAL_REQUEST_Remote
    {
        //Data Header
        public string ID { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string RENTAL_REQUEST_TYPE { get; set; }
        public string RENTAL_REQUEST_ID { get; set; }
        public string BE_ID { get; set; }
        public string ID_BE { get; set; }
        public string BE_NAME { get; set; }
        public string RENTAL_REQUEST_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string R_CONTRACT_END_DATE { get; set; }
        public string R_CONTRACT_START_DATE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string STATUS { get; set; }
        public string OLD_CONTRACT { get; set; }
        public string DOCUMENT_STATUS { get; set; }
        public string CONTRACT_OFFER_NUMBER { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CONTRACT_USAGE_NAME { get; set; }
        public string CONTRACT_USAGE_CODE { get; set; }
        public string REF_DESC { get; set; }
        public string OBJECT_ID { get; set; }
        public string ACTIVE { get; set; }
        public string BRANCH_ID { get; set; }

        //Data Detail
        public string ID_1 { get; set; }
        public string RENTAL_REQUEST_NO_1 { get; set; }
        public string RO_NAME { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string F_PROFIT_CENTER { get; set; }
    }

    public class AUP_RETURN_TRANS_RENTAL
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }

}