﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class WARNING_CONTRACT_END
    {
        public int DAYS { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string MPLG_BADAN_USAHA { get; set; }
        public int BRANCH_ID { get; set; }
    }
}