﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_VARIOUSBUSINESS_Remote
    {
        public string ID { set; get; }

        public string BE_ID { set; get; }

        public string SERVICE_CODE { set; get; }

        public string SERVICE_NAME { set; get; }

        public string PROFIT_CENTER { set; get; }

        public string SERVICE_GROUP { set; get; }

        public string UNIT { set; get; }

        public string GL_ACCOUNT { set; get; }

        public string ACTIVE { set; get; }
        
        public string TAX_CODE { set; get; }

        public string BE_NAME { get; set; }

        public string TERMINAL_NAME { get; set; }

        public string REF_DESC { get; set; }

        public string VAL1 { get; set; }

        public string VALID_FROM { set; get; }

        public string VALID_TO { set; get; }

        public string MULTIPLY_FUNCTION { set; get; }

        public string CONDITION_PRICING_UNIT { set; get; }

        public string PRICE { get; set; }

        public string SERVICE_NAME_1 { get; set; }

        public string UNIT_1 { get; set; }

        public string ACTIVE_1 { get; set; }

        public string ID_BP { get; set; }

        public string VB_ID { get; set; }

        public string LEGAL_CONTRACT_NO { get; set; }

        public string CONTRACT_DATE { get; set; }

        public string CONTRACT_VALIDITY { get; set; }
    }

    public class DataDDBE {

        public string BE_ID { set; get; }

        public string BE_NAME { set; get; }

    }

    public class DataListProfitCenter
    {
        public string PROFIT_CENTER_ID { set; get; }

        public string TERMINAL_NAME { set; get; }
    }

    public class DataListService
    {
        public string REF_DATA { set; get; }

        public string REF_DESC { set; get; }
    }

    public class AUP_RETURN_VARIOUS_BUSINESS
    {
        public string SERVICE_CODE { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}