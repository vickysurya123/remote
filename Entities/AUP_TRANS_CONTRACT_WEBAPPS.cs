﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_CONTRACT_WEBAPPS
    {
        //DATA PROP_CONTRACT
        public string IJIN_PRINSIP_NO { get; set; }
        public string IJIN_PRINSIP_DATE { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string LEGAL_CONTRACT_DATE { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public long STATUS { get; set; }
        public string CONTRACT_STATUS { get; set; }
        public long CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string COMPANY_CODE { get; set; }
        public string CURRENCY { get; set; }
        public long RENTAL_REQUEST_NO { get; set; }
        public long CONTRACT_OFFER_NO { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
        //public long CUSTOMER_AR { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_ID { get; set; }
        public long TERM_IN_MONTHS { get; set; }
        public string BUSINESS_PARTNER_NAMES { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string BE_NAME { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string OLD_CONTRACT { get; set; }
        public string REGIONAL_NAME { get; set; }
        public string USER_NAME { get; set; }


        //FULL DATA CONCATE
        public string CONTRACT_USAGE_F { get; set; }
        public string CONTRACT_OFFER_TYPE_F { get; set; }
        public string BUSINESS_ENTITY_F { get; set; }
        public string PROFIT_CENTER_F { get; set; }

        // DATA PROP_CONTRACT_OBJECT
        public string ID { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_NAME { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string LUAS { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string INDUSTRY { get; set; }

        // DATA PROP_CONTRACT_CONDITION
        public string ID_1 { get; set; }
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public long MONTHS { get; set; }
        public long STATISTIC { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string FORMULA { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string LUAS_1 { get; set; }
        public string TOTAL { get; set; }
        public string NJOP_PERCENT { get; set; }
        public string SALES_RULE { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string UNIT_PRICE { get; set; }
        public string KONDISI_TEKNIS_PERCENT { get; set; }
        public string COA_PROD { get; set; }

        //DATA PROP CONTRACT MANUAL
        public string CONDITION { get; set; }
        public string DUE_DATE { get; set; }
        public string NET_VALUE { get; set; }
        public string BILLING_NO { get; set; }
        public string CONDITION_ID { get; set; }

        //DATA PROP CONTRACT MEMO
        public string MEMO { get; set; }
        public string OBJECT_CALC { get; set; }

        //FILTER DATA
        public string CONTRACT_OFFER_NAME { get; set; }

        public string UNIT { get; set; }
        public string QUANTITY { get; set; }

        //EDIT CONTRACT
        public string CHANGE_DATE { get; set; }
        public string CHANGE_BY { get; set; }

        // STRING DATE TYPE DD/MM/YYYY
        public string LEGAL_CONTRACT_DATE_S { get; set; }
        public string CONTRACT_START_DATE_S { get; set; }
        public string CONTRACT_END_DATE_S { get; set; }
    }
}