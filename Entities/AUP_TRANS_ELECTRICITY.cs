﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_ELECTRICITY
    {
        public string ID {get; set;}
        public string INSTALLATION_TYPE{get; set;}
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER_ID {get; set;} 
        public string INSTALLATION_DATE {get; set;} 
        public string INSTALLATION_ADDRESS {get; set;}
        public string TARIFF_CODE {get; set;}
        public string MINIMUM_AMOUNT {get; set;} 
        public string CURRENCY {get; set;}
        public string TAX_CODE {get; set;} 
        public string RO_CODE {get; set;} 
        public string STATUS {get; set;} 
        public string POWER_CAPACITY {get; set;}
        public string INSTALLATION_NUMBER {get; set;}
        public string CUSTOMER_NAME {get; set;}
        public string CUSTOMER_SAP_AR {get; set;}
        public string INSTALLATION_CODE { get; set; }
        public string MINIMUM_USED { get; set; }
        public string MINIMUM_PAYMENT { get; set; }
        public string BIAYA_ADMIN { get; set; }
        public string BIAYA_BEBAN { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string INSTALLATION_ID { get; set; }
        public string STATUS_DINAS { get; set; }
    }
}