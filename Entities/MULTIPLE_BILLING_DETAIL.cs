﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class MULTIPLE_BILLING_DETAIL
    {
        public string CONTRACT_NO { get; set; }
        public string BILLING_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string DUE_DATE { get; set; }
        public float NET_VALUE { get; set; }
        public string POSTING_DATE { get; set; }
        public string PAID_DATE { get; set; }
        public int MANUAL_NO { get; set; }
        public string STATUS { get; set; }
    }
}