﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_TRANS_BILLING_PROPERTY 
    {
        public string ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string PERIOD { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string MIN_USED { get; set; }
        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public string POSTING_DATE { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string TARIFF_CODE { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string AMOUNT { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string METER_FROM_TO { get; set; }        
 
    }
}