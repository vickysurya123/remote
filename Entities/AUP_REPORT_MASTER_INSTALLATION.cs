﻿using System;
namespace Remote.Entities
{
    public class AUP_REPORT_MASTER_INSTALLATION
    {
        public string INSTALLATION_TYPE { get; set; }
        public string BE_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string INSTALLATION_DATE { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string TARIFF_CODE { get; set; }
        public string MINIMUM_AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string TAX_CODE { get; set; }
        public string RO_CODE { get; set; }
        public string STATUS { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string SERIAL_NUMBER { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string RO_NUMBER { get; set; }
        public string MINIMUM_PAYMENT { get; set; }
        public string MULTIPLY_FACT { get; set; }
        public string BIAYA_BEBAN { get; set; }
        public string MINIMUM_USED { get; set; }
        public string BRANCH_ID { get; set; }
        public string BIAYA_ADMIN { get; set; }
        public string AMOUNT { get; set; }
        public string profit_center_id { get; set; }
        public string terminal_name { get; set; }
        public string nama_profit_center { get; set; }
        public string ASSIGNMENT_RO { get; set; }


        public string TARIF_BLOCK1 { get; set; }
        public string TARIF_BLOCK2 { get; set; }
        public string TARIF_KVARH { get; set; }
        public string TARIF_LWBP { get; set; }
        public string TARIF_WBP { get; set; }
        public string PPJU { get; set; }
        public string REDUKSI { get; set; }
    }
}