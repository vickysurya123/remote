﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_WORKFLOW_SETTING
    {
        public string BE_ID { get; set; }

        public string OPERATOR_CABANG_EMAIL { get; set; }

        public string OPERATOR_CABANG_PHONE { get; set; }

        public string OPERATOR_PUSAT_EMAIL { get; set; }

        public string OPERATOR_PUSAT_PHONE { get; set; }

        public string MAX_SEQ_APPROVER_CABANG { get; set; }

        public string IS_REJECT_PUSAT_INFO_CABANG { get; set; }

        public string IS_REVISE_PUSAT_INFO_CABANG { get; set; }
    }
}