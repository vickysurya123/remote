﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUTOCOMPLETE_CUSTOMER_S2
    {
        public string id { get; set; }
        public string label { get; set; }
        public string address { get; set; }
        public string sap { get; set; }
        public string name { get; set; }
    }
}