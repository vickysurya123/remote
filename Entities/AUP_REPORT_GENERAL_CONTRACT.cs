﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_GENERAL_CONTRACT
    {
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_TYPE_DESC { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string CREATION_BY { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CURRENCY { get; set; }
        public string IJIN_PRINSIP_NO { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_NAME { get; set; }
        public string INDUSTRY { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string CONTRACT_STATUS { get; set; }
        public string STATUS_MIGRASI { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CREATION_DATE { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string STATUS { get; set; }
        public string BRANCH_ID { get; set; }

    }
}