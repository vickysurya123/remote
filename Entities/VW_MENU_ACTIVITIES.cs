﻿namespace Remote.Entities
{
    public class VW_MENU_ACTIVITIES
    {
        public int ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string NAMA_ACTIVITY { get; set; }
        public string PERMISSION_ADD { get; set; }
        public string PERMISSION_DEL { get; set; }
        public string PERMISSION_UPD { get; set; }
        public string PERMISSION_VIW { get; set; }

    }
}