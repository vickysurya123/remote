﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_SERVICE_CODE_WEBAPPS
    {
        public string code { get; set; }
        public string label { get; set; }
        public string unit { get; set; }
        public string unit_d { get; set; }
        public string currency { get; set; }
        public string price { get; set; }
        public string multiply { get; set; }
        public string tax_code { get; set; }
        public string service_name { get; set; }
        public string service_group { get; set; }
        public string gl_account { get; set;  }
    }
}