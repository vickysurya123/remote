﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_RENTALOBJECT_Remote
    {

        public string RO_NUMBER { get; set; }
        public string BRANCH_ID { get; set; }
        public string RO_CODE { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string MEMO { get; set; }
        public string RO_CITY { get; set; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string BE_ID { get; set; }
        public string BE_NAME { set; get; }
        public string USAGE_TYPE { set; get; }
        public string RO_PROVINCE { get; set; }
        public string PROVINCE { set; get; }
        public string ZONE_RIP { set; get; }
        public string RO_TYPE { set; get; }
        public string KODE_ASET { set; get; }
        public string FUNCTION_ID { set; get; }
        public string PROFIT_CENTER { set; get; }
        public string LINI { set; get; }
        public string SERTIFIKAT_ID { set; get; }
        public string PERUNTUKAN { set; get; }
        public string PERUNTUKAN_NAME { set; get; }
        public string OCCUPANCY { set; get; }
        public string CONTRACT_NO { set; get; }
        public string CONTRACT_OFFER_NO { set; get; }

        // TAMBAHAN UNTUK EDIT (SESUAI NAMA DI DB)
        public string PROFIT_CENTER_ID { set; get; }
        public string PROFIT_CENTER_ID_NEW { set; get; }
        public string PROVINCE_ID { set; get; }
        public string USAGE_TYPE_ID { set; get; }
        public string LOCATION_ID { set; get; }
        public string ZONE_RIP_ID {set; get;}
        public string RO_TYPE_ID { set; get; }
        public string VAL_FROM { set; get; }
        public string VAL_TO { set; get; }
        public string ACTIVE { set; get; }
        public string OCP_VALID_FROM { set; get; }
        public string OCP_VALID_TO { set; get; }
        public string ACTIVE_STATUS { set; get; }
        public string SERTIFIKAT_OR_NOT { set; get; }
        public string KETERANGAN { set; get; }

    }

    public class AUP_FIXTUREFITTING_Remote
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string DESCRIPTION { set; get; }
        public string FIXTURE_FITTING_CODE { set; get; }
        public string POWER_CAPACITY { set; get; }
        public string RO_NUMBER_1 { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
    }

    public class AUP_FIXTUREFITTING2_Remote
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string FIXTURE_FITTING_CODE { set; get; }
        public string POWER_CAPACITY { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string DESCRIPTION { set; get; }
        public string VAL_FROM { set; get; }
        public string VAL_TO { set; get; }

        public string ACTIVE { set; get; }
        public string REF_DATA { set; get; }
    }
    public class AUP_MEASUREMENT_Remote
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string MEASUREMENT_TYPE { set; get; }
        public string AMOUNT { set; get; }
        public string UNIT { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string DESCRIPTION { set; get; }
        public string VAL_FROM { set; get; }
        public string VAL_TO { set; get; }
        public string ACTIVE { set; get; }
        public string REF_DATA { set; get; }
    }

    public class AUP_MAPS_Remote
    {
        public string ID { set; get; }
        public string LATITUDE { set; get; }
        public string LONGITUDE { set; get; }
        public string RO_NUMBER { set; get; }
        public string NO_URUT { set; get; }
        public string BE_ID { set; get; }

    }

    public class AUP_OCCUPANCY_Remote
    {
        public string ID { get; set; }
        public string RO_NUMBER { get; set; }
        public string STATUS { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string REASON { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string F_VALID_FROM { get; set; }
        public string F_VALID_TO { get; set; }
    }

    public class AUP_RETURN_RO_NUMBER
    {
        public string RO_NUMBER { set; get; }
        public string RO_ID { set; get; }
        public bool RESULT_STAT { set; get; }  
    }
}