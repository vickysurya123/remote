﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_TRANS_BILLING_PROPERTY_TWO
    {
        public string BUSINESS_ENTITY { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string BILLING_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string BILLING_CONTRAC_NO { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string CONDITION_TYPE_DESC { get; set; }
        public string BILLING_TYPE { get; set; }
        public string POSTING_DATE { get; set; }
        public string BILLING_CONTRACT_NO { get; set; }
        public string OBJECT_ID { get; set; }
        public string CURRENCY { get; set; }
        public string AMOUNT { get; set; }
        public string CREATION_BY { get; set; }
        public string GL_ACOUNT_NO { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string LUAS { get; set; }
    }
}