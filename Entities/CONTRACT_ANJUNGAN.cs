﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class CONTRACT_ANJUNGAN_HEADER
    {
        //Data Header
        public string ID { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string STATUS { get; set; }
        public string OLD_CONTRACT { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string KODE_BAYAR { get; set; }
        public string REJECT_NOTE { get; set; }
    }

    public class CONTRACT_ANJUNGAN_DETAIL
    {

        //Data Detail
        public string ID { get; set; }
        public string CONTRACT_ANJUNGAN_ID { get; set; }
        public string RO_NAME { get; set; }
        public string RO_NUMBER { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string INDUSTRY { get; set; }

        public string OBJECT_ID { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_TYPE_ID { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string F_PROFIT_CENTER { get; set; }
    }
}