﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class VW_ROLE_MENU
    {
        public int xID { get; set; }
        public int ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public int MENU_ID { get; set; }
        public string MENU_NAMA { get; set; }
        public int MENU_PARENT_ID { get; set; }
        public string MENU_PARENT_NAMA { get; set; }
        public int ORDERED_BY { get; set; }
        public int APP_ID { get; set; }
        public int STATUS { get; set; }

    }
}