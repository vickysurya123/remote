﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class APPROVAL_SETTING_DETAIL
    {
        public int ID { get; set; }
        public int HEADER_ID { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public int CREATED_DATE { get; set; }
        public int UPDATED_DATE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_USAGE { get; set; }

        // ADDITIONAL
        public string ACTIVE_DATE { get; set; }
        public string PARAM_DESC { get; set; }
        public string TOP_APPROVAL { get; set; }
    }
}