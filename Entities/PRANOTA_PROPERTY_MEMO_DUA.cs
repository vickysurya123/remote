﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class PRANOTA_PROPERTY_MEMO_DUA
    {
        public decimal CONTRACT_NO { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string MEMO { get; set; }
    }
}