﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class CURRENCY
    {
        public Nullable<int> ID { get; set; }
        public string CODE { get; set; }
        public string SYMBOL { get; set; }
        public int USER_ID_CREATE { get; set; }
        public Nullable<int> USER_ID_UPDATE { get; set; }
        public string VALIDTY_TO { get; set; }
        public string VALIDITY_FROM { get; set; }
    }
}