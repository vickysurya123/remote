﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class PRANOTA_PROPERTI_DETAIL
    {
        public string BILLING_NO { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string JANGKA_WAKTU { get; set; }
        public decimal UNIT_PRICE { get; set; }
        public string AMT_REF { get; set; }
        public string DASAR_HARGA { get; set; }
        public string NJOP_PERCENT { get; set; }
        public string NJOP { get; set; }
        public string LUAS { get; set; }
        public string KONDISI_TEKNIS_PERCENT { get; set; }
        public string KONDISI_TEKNIS { get; set; }
        public decimal TOTAL_NET_VALUE { get; set; }
        public string MEMO { get; set; }
        public string BILLING_ID { get; set; }
        public string BILLING_PERIOD { get; set; }
        public string KD_CABANG { get; set; }
        public string CURRENCY { get; set; }
        public string BILLING_CONTRACT_NO { get; set; }
        public decimal INSTALLMENT_AMOUNT { get; set; }
        public string PERHITUNGAN { get; set; }
    }
}