﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_WE_BILLING_WEBAPPS
    {
        public string ID { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PERIOD { get; set; }
        public string F_PERIOD { get; set; }
        public string REPORT_ON { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string PRICE { get; set; }
        public string AMOUNT { get; set; }
        public string RATE { get; set; }
        public string TOTAL_USD { get; set; }
        public string STATUS { get; set; }
        public string BILLING_TYPE { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string F_REPORT_ON { get; set; }
        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string STATUS_DINAS { get; set; }
        public string CURRENCY_CODE { get; internal set; }
        public string BIAYA_ADMIN { get; set; }
        public string BE_ID { get; set; }
        public string BRANCH_ID { get; set; }
        public int ROLE_ELECTRICITY { get; set; }
        public int ROLE_WATER { get; set; }

    }
}