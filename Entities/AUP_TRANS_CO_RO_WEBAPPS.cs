﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_TRANS_CO_RO_Remote
    {
        public string RENTAL_REQUEST_NO { get; set; }
        public string RO_NUMBER { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_ID { get; set; }
        public string RO_CODE { get; set; }
        public string RO_NAME { get; set; }
        public string OBJECT_TYPE_ID { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string ACTIVE { get; set; }
        public string OBJECT_TYPE { get; set; }
    }

    public class AUP_RETURN_TRANS_NUMBER_CO
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}