﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_ROLE_Remote
    {
        public string ROLE_ID { get; set; }

        public string ROLE_NAME { get; set; }

        public string APP_ID { get; set; }

        public string PROPERTY_ROLE { get; set; }
    
    }
}