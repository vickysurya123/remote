﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace Remote.Entities
{
    public class AUP_RO_ATTACHMENT
    {
        public string ID { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME { get; set; }
        public string RO_NUMBER { get; set; }
    }
}