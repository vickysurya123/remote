﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_WATERANDELECTRICITY_Remote
    {
        public string SERVICES_ID { set; get; }
        public string INSTALLATION_TYPE { set; get; }
        public string DESCRIPTION { set; get; }
        public string AMOUNT { set; get; }
        public string CURRENCY { set; get; }
        public string X_FACTOR { set; get; }
        public string FALID_FROM { set; get; }
        public string FALID_TO { set; get; }
        public string ACTIVE { set; get; }
        public string PER { set; get; }
        public string UNIT { set; get; }
        public string TARIFF_CODE { set; get; }
        public string STAT { set; get; }
        public string NAMA_PROFIT_CENTER { get; set; }
    }

    public class AUP_RETURN_WATER_AND_ELECTRICITY
    {
        public string TARIFF_CODE { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}