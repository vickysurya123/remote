﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class UserAkunInfoNew
    {
        public string pesan { get; set; }
        public string kode { get; set; }
        public string ApplicationId { get; set; }

        public string responType { get; set; }
        public string responText { get; set; }
        public string IDUSER { get; set; }
        public string USERNAME { get; set; }
        public string NAMA { get; set; }
        public string KELAS { get; set; }
        public string NAMA_JABATAN { get; set; }
        public string SANDI { get; set; }
        public string KD_CABANG { get; set; }
        public string NAMA_CABANG { get; set; }
        public string KD_DIT { get; set; }
        public string NAMA_DIT { get; set; }
        public string KD_SUB { get; set; }
        public string NAMA_SUB { get; set; }
        public string KD_SEK { get; set; }
        public string NAMA_SEK { get; set; }
        public string HAKAKSES { get; set; }
        public string HAKAKSES_DESC { get; set; }
        public string PARENT_HAKAKSES { get; set; }
        public string PARENT_HAKAKSES_DESC { get; set; }
        public string EMAIL { get; set; }
        public string HP { get; set; }
        public string STATUS { get; set; }
        public string URL { get; set; }
        public string URL_CP { get; set; }
        public string NORUT { get; set; }
        public string DGT { get; set; }
        public string KD_TERMINAL { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string PIN { get; set; }
        public string PERSON_AREA { get; set; }
        public string PERSON_SUB_AREA { get; set; }
        public string REGIONAL { get; set; }
    }
}