﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_KRONOLOGIS_WEBAPPS
    {
        // Header Data
        public string ID { get; set; }
        public string BE_ID { get; set; }
        public string TANGGAL { get; set; }
        public string JUDUL { get; set; }
        public string USER_ID_CREATE { get; set; }
        public string USER_ID_UPDATE { get; set; }
        public string TIME_CREATE { get; set; }
        public string TIME_UPDATE { get; set; }
        public string TIME_DELETE { get; set; }
        public string IS_DELETE { get; set; }

        //Detail Data
        public string NO_SURAT { get; set; }
        public string TANGGAL_SURAT { get; set; }
        public string KETERANGAN { get; set; }
        public string TINDAK_LANJUT { get; set; }
        public string KENDALA { get; set; }
        public string PROGRESS { get; set; }
        public string FILE_NAME { get; set; }
        public string DIRECTORY { get; set; }
    }

    public class AUP_RETURN_KRONOLOGIS_NUMBER
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }

    /*public class AUP_RETURN_SAP_DOCUMENT_NUMBER
    {
        public string DOCUMENT_NUMBER { set; get; }
        public bool RESULT_STAT { set; get; }
    }*/
}