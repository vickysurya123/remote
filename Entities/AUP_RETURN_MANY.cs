﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_RETURN_MANY
    {
        public string ID { set; get; }
        public string LAST_LWBP { get; set; }
        public string LAST_WBP { get; set; }
        public string LAST_KVARH { get; set; }
        public string LAST_BLOK1 { get; set; }
        public string LAST_BLOK2 { get; set; }
        public bool RESULT_STAT { set; get; }
    }
}