﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{

    public class MULTIPLE_BILLING_HEADER
    {
        public string BE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string BE_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }

        public string CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string RO_ADDRESS { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string SAP_DOC_NO { get; set; }
        public float TOTAL_AMOUNT { get; set; }
        public float POSTED_AMOUNT { get; set; }
        public float UNPOSTED_AMOUNT { get; set; }
        public float CONTRACT_OFFER_NO { get; set; }
        public float TOTAL_AMOUNT_CO { get; set; }
    }
}