﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_RO_INTERNAL_USED
    {
        public string RO_CODE { get; set; }
        public string RO_NAME { get; set; }
        public string BE_NAME { get; set; }
        public string CREATION_DATE { get; set; }
        public string REASON { get; set; }
        public string OBJ { get; set; }
        public string RO_ADDRESS { get; set; }
        public string AMOUNT { get; set; }
        public string USAGES { get; set; }
        public string ZONE_RIP { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
    }
}