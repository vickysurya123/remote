﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_REPORT_JKP
    {
        public string CONTRACT_NO { get; set; }
        public decimal VAL_CONTRACT { get; set; }
        public decimal VAL_PERMONTH { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public decimal PREV_PERIOD { get; set; }
        public decimal ACC_REVENUE { get; set; }
        public decimal JANGKA_PNDK { get; set; }
        public decimal NEXT_YEAR { get; set; }
        public decimal END_CONTRACT { get; set; }
        public decimal PAID_PERIOD { get; set; }
        public decimal PAID_PDK { get; set; }      
        public decimal PROYEKSI_CONTRACT { get; set; }      
        public string PROFIT_CENTER_NAMA { get; set; }      
        public string BUSINESS_PARTNER_NAME { get; set; }      
        public string COA_PROD { get; set; }      
        public string NO { get; set; }      
 
    }
}