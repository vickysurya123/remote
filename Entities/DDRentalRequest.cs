﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class DDRentalRequest
    {
        public string RENTAL_REQUEST_NO { get; set; }
        public string RENTAL_REQUEST_TYPE { get; set; }
        public string BE_ID { get; set; }
        public string RENTAL_REQUEST_NAME { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string STATUS { get; set; }
        public string OLD_CONTRACT { get; set; }
        public string label { get; set; }
        public string code { get; set; }
    }
}