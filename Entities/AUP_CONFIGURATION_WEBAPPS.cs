﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Entities
{
    public class AUP_CONFIGURATION_WEBAPPS
    {
        public string REF_CODE_P { get; set; }

        public string REF_DESC_P { get; set; }

        public string REF_CODE_C { get; set; }

        public string REF_DATA { get; set; }
        public string REF_DESC { get; set; }
        public string REF_CODE { get; set; }

        public string REF_DESC_C { get; set; }

        public string ID { get; set; }

        public string ACTIVE { get; set; }
        public string ATTRIB1 { get; set; }
        public string ATTRIB2 { get; set; }
        public string ATTRIB3 { get; set; }
        public string ATTRIB4 { get; set; }
        public string VAL1 { get; set; }
        public string VAL2 { get; set; }
        public string VAL3 { get; set; }
        public string VAL4 { get; set; }
    
    }
}