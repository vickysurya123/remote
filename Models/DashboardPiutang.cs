﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DashboardPiutang
    {
        public string BE_NAME { get; set; }
        public decimal BRANCH_ID { get; set; }
        public decimal PROPERTY { get; set; }
        public string PROPERTY_TEXT { get; set; }
        public decimal WATER_ELECTRICITY { get; set; }
        public string WATER_ELECTRICITY_TEXT { get; set; }
        public decimal OTHER_SERVICE { get; set; }
        public string OTHER_SERVICE_TEXT { get; set; }
    }
}