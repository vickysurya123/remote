﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransContractBilling
{
    public class DataTablesTransContractBilling
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_TRANS_CONTRACT_BILLING_WEBAPPS> data { get; set; }
    }

    public class DataReturnSAP_COCUMENT_NUMBER
    {
        public string DOCUMENT_NUMBER { get; set; }
        public string BILL_ID { get; set; }
        public string RESULT_STAT { get; set; }
        public List<AUP_RETURN_SAP_DOCUMENT_NUMBER_BILLING> datas { get; set; }
    }
}