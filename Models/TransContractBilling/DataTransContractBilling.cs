﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransContractBilling
{
    public class DataTransContractBilling
    {
        //HEADER DATA
        public long? ENDING_BALANCE_AMOUNT { get; set; }
        public string BILLING_FLAG_TAX1 { get; set; }
        public string BILLING_FLAG_TAX2 { get; set; }
        public string CREATION_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string PROFIT_CENTER_CODE { get; set; }
        public long? COMPANY_CODE { get; set; }
        public long? FREQ_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string NO_REF1 { get; set; }
        public string NO_REF2 { get; set; }
        public long? PROFIT_CENTER_ID { get; set; }
        public long? BE_ID { get; set; }
        public string BILLING_DUE_DATE { get; set; }
        public long? BILLING_PERIOD { get; set; }
        public string NO_REF3 { get; set; }
        public string CURRENCY_CODE { get; set; }
        public long? BEGIN_BALANCE_AMOUNT { get; set; }
        public long? INSTALLMENT_AMOUNT { get; set; }
        public string BILLING_NO { get; set; }
        public long? ID { get; set; }
        public long? BILLING_ID { get; set; }
        public string BILLING_CONTRACT_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_CODE_SAP { get; set; }
        public string BE_NAME { get; set; }
        public string POSTING_DATE { get; set; }
        public string CONTRACT_NAME { get; set; }

        //DETAIL DATA
        public long? BILLING_QTY { get; set; }
        public string BILLING_UOM { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string GL_ACOUNT_NO { get; set; }
        public long? SEQ_NO { get; set; }
        public string OBJECT_ID { get; set; }
    }

    public class DATA_RETURN_SAP_DOCUMENT_NUMBER
    {
        public string BILLING_CONTRACT_NO { get; set; }
        public string BILLING_NO { set; get; }
        public string DOCUMENT_NUMBER { set; get; }
        public bool RESULT_STAT { set; get; }
    }
}