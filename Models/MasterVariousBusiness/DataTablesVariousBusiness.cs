﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.MasterVariousBusiness
{
    public class DataTablesVariousBusiness
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_VARIOUSBUSINESS_Remote> data { get; set; }
    }

    public class DataReturnVariousBusiness
    {

        public string SERVICE_CODE { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_VARIOUS_BUSINESS> datas { get; set; }
    }
}