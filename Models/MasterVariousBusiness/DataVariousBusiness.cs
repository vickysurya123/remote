﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterVariousBusiness
{
    public class DataVariousBusiness
    {
        public string ID { set; get; }

        public string BE_ID { set; get; }

        public string SERVICE_CODE { set; get; }

        public string SERVICE_NAME { set; get; }

        public string PROFIT_CENTER { set; get; }

        public string SERVICE_GROUP { set; get; }

        public string UNIT { set; get; }

        public string TAX_CODE { set; get; }

        public string GL_ACCOUNT { set; get; }

        public string ACTIVE { set; get; }

        public string PRICE { get; set;}

        public string CONDITION_PRICING_UNIT { get; set;}

        public string MULTIPLY_FUNCTION { get; set; }

        public string UNIT_1 { get; set; }

        public string VALID_FROM { get; set;}

        public string VALID_TO { get; set;}

        public string ID_BP { get; set; }

        public string VB_ID { get; set; }

        public string LEGAL_CONTRACT_NO { get; set; }

        public string CONTRACT_DATE { get; set; }

        public string CONTRACT_VALIDITY { get; set; }
    }
}