﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_SALES_BASED
    {
        public string SALES_TYPE { get; set; }
        public string NAME_OF_TERM { get; set; }
        public string CALC_FROM { get; set; }
        public string CALC_TO { get; set; }
        public string UNIT { get; set; }
        public string PERCENTAGE { get; set; }
        public string TARIF { get; set; }
        public string RR { get; set; }
        public string MINSALES { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string SR { get; set; }
        public string PAYMENT_TYPE { get; set; }
        public string MINPRODUCTION { get; set; }
    }
}