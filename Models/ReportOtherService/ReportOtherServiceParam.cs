﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportOtherServiceParam
    {
        public string BE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string SERVICE_GROUP_ID { get; set; }
        public string STATUS { get; set; }
    }
}
