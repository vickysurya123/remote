﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ReportOtherService
{
    public class DataTableReportOtherService
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_OTHER_SERVICES> data { get; set; }
    }
}