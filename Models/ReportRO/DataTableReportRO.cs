﻿using System.Collections.Generic;

namespace Remote.Controllers
{
    internal class DataTableReportRO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_RO> data { get; set; }
    
    }
}