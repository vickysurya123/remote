﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportTransElectricityParam
    {
        public string PROFIT_CENTER { get; set; }
        public string BILLING_NO { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string PERIOD { get; set; }
        public string POSTING_DATE_FROM { get; set; }
        public string POSTING_DATE_TO { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string CONDITION_USED { get; set; }
    }
}
