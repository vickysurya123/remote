﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ReportTransElectricity
{
    public class DataTableReportTransElectricity
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_TRANS_ELECTRICITY> data { get; set; }
    }
}