﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MonitoringKronologisMaster
{
    public class FDDRegional
    {
        public string BE_ID { set; get; }
        public string BE_NAME { set; get; }
        public FDDRegional DDRegional { set; get; }
    }
}