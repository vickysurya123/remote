﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MonitoringKronologisMaster
{
    public class DataMonitoringKronologis
    {
        public string ID { get; set; }
        public string BE_ID { get; set; }
        public string REGIONAL { get; set; }
        public string BE_NAME { get; set; }
        public string JUDUL { get; set; }
        public string TANGGAL { get; set; }
        public string USER_ID_CREATE { get; set; }
        public string USER_ID_UPDATE { get; set; }
        public string TIME_CREATE { get; set; }
        public string TIME_UPDATE { get; set; }
        public string TIME_DELETE { get; set; }
        public string IS_DELETE { get; set; }
        public string IS_APPROVE { get; set; }
        
        public string FDDCabang { get; set; }
        public string FDDRegional { get; set; }

        /*DETAIL DATA*/
        public List<string> NO_SURAT { get; set; }
        public List<string> TANGGAL_SURAT { get; set; }
        public List<string> KETERANGAN { get; set; }
        public List<string> TINDAK_LANJUT { get; set; }
        public List<string> KENDALA { get; set; }
        public List<string> PROGRESS { get; set; }
        public string FILE_NAME { get; set; }
        public string DIRECTORY { get; set; }
        public string KRONOLOGIS_ID { get; set; }
        public List<detailKronologis> detailKronologis { get; set; }
        public List<int> delete_line { get; set; }
    }

    public class detailKronologis
    {
        //Detail Data
        public string ID { get; set; }
        public string NO_SURAT { get; set; }
        public string TANGGAL_SURAT { get; set; }
        public string KETERANGAN { get; set; }
        public string TINDAK_LANJUT { get; set; }
        public string KENDALA { get; set; }
        public string PROGRESS { get; set; }
        public string FILE_NAME { get; set; }
        public string DIRECTORY { get; set; }
        public string KRONOLOGIS_ID { get; set; }
        public IFormFile FILE { get; set; }
        public string FILE_DELETE { get; set; }
    }

    public class DashExcel
    {
        public string ID { get; set; }
        public string JUDUL { get; set; }
        public string CABANG { get; set; }
        public string REGIONAL { get; set; }
        public string TANGGAL { get; set; }
    }

    public class detailKronologisExcel
    {
        //Detail Data
        public string ID { get; set; }
        public string NO_SURAT { get; set; }
        public string TANGGAL_SURAT { get; set; }
        public string KETERANGAN { get; set; }
        public string TINDAK_LANJUT { get; set; }
        public string KENDALA { get; set; }
        public string PROGRESS { get; set; }
        public string FILE_NAME { get; set; }
        public string DIRECTORY { get; set; }
        public string KRONOLOGIS_ID { get; set; }
        public string TANGGAL { get; set; }
        public string JUDUL { get; set; }
        public string BE_ID { get; set; }
        public string CABANG { get; set; }
        public string REGIONAL { get; set; }
    }

    public class DataReturnMonitoring
    {
        public string ID_KRONOLOGIS { get; set; }
        public string ID { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_KRONOLOGIS_NUMBER> datas { get; set; }

        public string MessageResult { get; set; }
    }

    public class AUP_RETURN_KRONOLOGIS_NUMBER
    {
        public string ID { set; get; }
        public bool RESULT_STAT { set; get; }
    }

    /*public class DataReturnTransNumber
    {
        public string ID_TRANS { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_TRANS_NUMBER> datas { get; set; }
    }*/
}