﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DashboardDetailPiutang
    {
        public string JENIS_TAGIHAN { get; set; }
        public string BILLING_NO { get; set; }
        public string CODE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PSTNG_DATE { get; set; }
        public string TGL_LUNAS { get; set; }
        public string TGL_BATAL { get; set; }
        public string DOC_NUMBER { get; set; }
        public string AMOUNT { get; set; }
        public string STATUS_LUNAS { get; set; }
        public string BE_NAME { get; set; }
    }
}