﻿using System;

namespace Remote.Models.PendingList
{
    public class ContractOfferApprovalParameter
    { 
        public string CONTRACT_OFFER_NO { get; set; }    
        public string USER_LOGIN { get; set; }
        public string LEVEL_NO { get; set; } 
        public string APPROVAL_TYPE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string BRANCH_ID { get; set; }
        public string APPROVAL_NOTE { get; set; }
        public string DISPOSISI_PARAF { get; set; }
        public DateTime LAST_UPDATE_DATE { 
            get
            {
                return DateTime.Now;
            }
        }
        public string TOP_APPROVAL { get; set; }  
    }
}