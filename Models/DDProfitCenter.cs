﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDProfitCenter
    {
        public string PROFIT_CENTER_ID { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string BRANCH_ID { get; set; }
        public string NAMA_PROFIT_CENTER { get; set; }
        public string BE_ID { get; set; }
    }
}