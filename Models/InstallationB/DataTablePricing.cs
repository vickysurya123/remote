﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.InstallationB;

namespace Remote.Models.InstallationB
{
    public class DataTablePricing
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<InstallationBPricing> data { get; set; }
    }
}