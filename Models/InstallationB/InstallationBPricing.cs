﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.InstallationB
{
    public class InstallationBPricing
    {
        public string ID { get; set; }
        public string INSTALLATION_ID { get; set; }
        public string PRICE_TYPE { get; set; }
        public string PRICE_CODE { get; set; }
        public string MAX_RANGE_USED { get; set; }
        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }
        public string SERVICES_INSTALLATION_ID { get; set; }
        public string INSTALLATION_CODE { get; set; }

    }
}