﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDTariffCode
    {
        public string TARIFF_CODE { set; get; }
        public string AMOUNT { set; get; }
        public string CURRENCY { set; get; }
        public string DESCRIPTION { get; set; }
    }
}