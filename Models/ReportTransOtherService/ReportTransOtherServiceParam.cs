﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportTransOtherServiceParam
    {
        public string BUSINESS_ENTITY { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string BILLING_NO { get; set; }
        public string SAP_NO { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string SERVICE_GROUP_ID { get; set; }
        public string POSTING_DATE_FROM { get; set; }
        public string POSTING_DATE_TO { get; set; }
    }
}
