﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.MasterConfiguration
{
    public class DataTablesConfiguration
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_CONFIGURATION_WEBAPPS> data { get; set; }
    }
}