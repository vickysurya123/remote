﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransElectricity
{
    public class DataTransElectricity
    {
        public string ID { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string PERIOD { get; set; }
        public string REPORT_ON { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string PRICE { get; set; }
        public string AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string BILLING_TYPE { get; set; }
        public string FI_POST_STATUS { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string ID_INSTALASI { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string RO_NUMBER { get; set; }
        public string POSTING_DATE { get; set; }
        public string MIN_USED { get; set; }
        public string MIN_PAYMENT { get; set; }
        public string BRANCH_ID { get; set; }
        public string INSTALLATION_CODE { get; set; }

        // Transaction C
        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }
        public string SERVICES_TRANSACTION_ID { get; set; }

        // Transaction D
        public string PRICE_TYPE { get; set; }
        public string TARIFF { get; set; }
        public string MULTIPLY { get; set; }
        public string PRICE_CODE { get; set; }
        public string USED { get; set; }
    }
}