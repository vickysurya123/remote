﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;


namespace Remote.Models.TransElectricity
{
    public class DataReturnAddElectricityTrans
    {
        public string RO_NUMBER { get; set; }
        public string RO_ID { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_TRANS_ELECTRICITY> datas { get; set; }
    }
}