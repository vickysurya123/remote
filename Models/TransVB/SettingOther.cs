﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransVB
{
    public class SettingOther
    {
        public SettingOther()
        {
            Service = new List<SettingOtherD>();
            DeleteD = new List<SettingDelete>();
        }
        public string HEADER_ID { get; set; }
        public List<SettingOtherD> Service { get; set; }
        public List<SettingDelete> DeleteD { get; set; }

    }
}