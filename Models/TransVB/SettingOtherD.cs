﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransVB
{
    public class SettingOtherD
    {
        public string COA { get; set; }
        public string DESCRIPTION { get; set; }
        public string WAKTU_MULAI { get; set; }
        public string WAKTU_SELESAI { get; set; }
    }
}