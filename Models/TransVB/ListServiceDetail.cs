﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransVB
{
    public class ListServiceDetail
    {
        
        public int ID { get; set; }
        public int HEADER_ID { get; set; }
        public string NO_URUT { get; set; }
        public string VAL1 { get; set; }
        public string VAL3 { get; set; }
        public string VAL4 { get; set; }
        public string PENDAPATAN { get; set; }

    }

    //public class DataServiceDetail
    //{
    //    //Detail Data
    //    public int ID_DETAIL { get; set; }
    //    public int HEADER_ID { get; set; }
    //    public string VAL1 { get; set; }
    //    public string VAL3 { get; set; }
    //    public string VAL4 { get; set; }
    //    public string PENDAPATAN { get; set; }
    //}


}