﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransVB
{
    public class DataTransVB
    {
        // Header Data
        public string ID_TRANS { get; set; }
        public string ID { get; set; }
        public string POSTING_DATE { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string SERVICES_GROUP { get; set; }
        public string COSTUMER_ID { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string TAX_CODE { get; set; }
        public string PPH_CODE { get; set; }
        public string TOTAL { get; set; }
        public string RATE { get; set; }
        public string NO_TAX_TOTAL { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string BILLING_TYPE { get; set; }

        //Detail Data
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string CURRENCY { get; set; }
        public string PRICE { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string SURCHARGE { get; set; }
        public string AMOUNT { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string SUB_TOTAL { get; set; }
        public string SUB_TOTAL_NO_TAX { get; set; }
        public string REMARK { get; set; }
        public string VB_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string KD_CABANG { get; set; }
        public string KD_CABANG_EDIT { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_DATE { get; set; }
        public List<details> details { get; set; }
        public string RAW_JSON_DATA { get; set; }
    }

    public class details
    {
        //Detail Data
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string CURRENCY { get; set; }
        public string PRICE { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string SURCHARGE { get; set; }
        public string AMOUNT { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string SUB_TOTAL { get; set; }
        public string SUB_TOTAL_NO_TAX { get; set; }
        public string REMARK { get; set; }
        public string VB_ID { get; set; }
        public string TAX_CODE { get; set;  }
        public string PPH_CODE { get; set; }
        public string COA_PROD { get; set; }
    }

    public class DataReturnTransNumber
    {
        public string ID_TRANS { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_TRANS_NUMBER> datas { get; set; }
    }
  
    public class DataReturnSAP_COCUMENT_NUMBER
    {
        public string DOCUMENT_NUMBER { get; set; }
        public string RESULT_STAT { get; set; }
        public List<AUP_RETURN_SAP_DOCUMENT_NUMBER> datas { get; set; }

    }
}