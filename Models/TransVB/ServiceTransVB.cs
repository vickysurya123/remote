﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TransVB
{
    public class ServiceTransVB
    {
        public string SERVICE_NAME { get; set; }
        public string SERVICE_GROUP { get; set; }
    }
}
