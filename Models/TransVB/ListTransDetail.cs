﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransVB
{
    public class ListTransDetail
    {

        public int ID { get; set; }
        public int HEADER_ID { get; set; }
        public string NO_URUT { get; set; }
        public string JENIS_TRANSAKSI { get; set; }
        public string KODE_JENIS_TRANSAKSI { get; set; }

    }


}