﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransVB
{
    public class DataTablesService
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        //public List<SETTING_OTHER_SERVICE> data { get; set; }
        public dynamic data { get; set; }
    }


}