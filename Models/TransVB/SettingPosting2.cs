﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransVB
{
    public class SettingPosting2
    {
        public SettingPosting2()
        {
            Service = new List<SettingPosting>();
            DeleteD = new List<SettingPostingDelete>();
        }
        public string HEADER_ID { get; set; }
        public List<SettingPosting> Service { get; set; }
        public List<SettingPostingDelete> DeleteD { get; set; }

    }
}