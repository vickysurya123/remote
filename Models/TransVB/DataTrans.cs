﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransVB
{
    public class DataTrans
    {
        // Header Data
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string USER_NAME { get; set; }
        public string CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }

        //public int ID_DETAIL { get; set; }
        //public int HEADER_ID { get; set; }
        //public string VAL1 { get; set; }
        //public string VAL3 { get; set; }
        //public string VAL4 { get; set; }
        //public string PENDAPATAN { get; set; }

        //public List<DataServiceDetail> DetailService { get; set; }
    }

    //public class DataServiceDetail
    //{
    //    //Detail Data
    //    public int ID_DETAIL { get; set; }
    //    public int HEADER_ID { get; set; }
    //    public string VAL1 { get; set; }
    //    public string VAL3 { get; set; }
    //    public string VAL4 { get; set; }
    //    public string PENDAPATAN { get; set; }
    //}


}