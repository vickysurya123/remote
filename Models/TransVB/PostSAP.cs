﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TransVB
{
    public class PostSAP
    {

        public string BILL_ID { get; set; }
        public string BILL_TYPE { get; set; }
        public string POSTING_DATE { get; set; }
    }
}
