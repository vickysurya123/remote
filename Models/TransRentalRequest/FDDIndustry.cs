﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransRentalRequest
{
    public class FDDIndustry
    {
        public string ID { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
        public DDIndustry DDIndustry { set; get; }
    }
}