﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransRentalRequest
{

    public class FDDCurrency
    {
        public string ID { get; set; }

        public string CODE { get; set; }

        public DDCurrency DDCurrency { set; get; }
    }
}