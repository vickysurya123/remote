﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.EmailConfiguration
{
    public class EmailConf
    {
        public string EMAIL_TO { get; set; }
        public string DESC { get; set; }
        public string CASE { get; set; }
        public string SUBJECT { get; set; }
        public string BODY { get; set; }
        public string APIKEY { get; set; }
        public IEnumerable<string> GROUP_CC { get; set; }
        public IEnumerable<string> GROUP_BCC { get; set; }
        public string GROUPCC { get; set; }
        public string SURAT_ID { get; set; }
    }
}