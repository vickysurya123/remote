﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDContractUsage
    {
        public string ID { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
    }
}