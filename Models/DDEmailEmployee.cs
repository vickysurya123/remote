﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDEmailEmployee
    {
        public int ID { set; get; }
        public string BE_ID { set; get; }
        public string EMPLOYEE_NAME { set; get; }
        public string EMAIL { set; get; }
        public string KODE { set; get; }
    }
}