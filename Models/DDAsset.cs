﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDAsset
    {
        public string ASSET_NO { set; get; }
        public string PROFIT_CENTER { set; get; }
        public string ASSET { set; get; }
    }
}