﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class MonitoringPembayaran
    {
        public string BUSINESS_ENTITY { get; set; }
        public string JENIS_TAGIHAN { get; set; }
        public string NOTA_TYPE { get; set; }
    }
}
