﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class MonitoringWE
    {
        public string INSTALLATION_CODE { get; set; }
        public string SERIAL_NUMBER { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string PERIOD { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string POSTING_DATE { get; set; }
        public string STATUS_NOW { get; set; }
        public string STATUS_POSTING { get; set; }
        public string BE_NAME { get; set; }
        public string NO_BILLING { get; set; }
        public string CREATED_BY { get; set; }

    }
}
