﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class MonitoringWEParam
    {
        public string BE_NAME { get; set; }
        public string TYPE_TRANSACTION { get; set; }
        public string PERIOD { get; set; }
        public string STATUS_NOW { get; set; }
    }
}
