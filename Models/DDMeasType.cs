﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDMeasType
    {
        public string RO_NUMBER { get; set; }
        public string ID_DETAIL { get; set; }
        public string RO_NAME { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string LUAS { get; set; }
        public string UNIT { get; set; }
        public string MEAS_TYPE { get; set; }
        public string MEAS_DESC { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string OBJECT_ID { get; set; }
        public string RO_CODE { get; set; }
    }
}