﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDContractType
    {
        public string ID { get; set; }
        public string REF_DATA { get; set; }
        public string REF_DESC { get; set; }
    }
}