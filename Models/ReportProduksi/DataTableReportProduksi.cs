﻿using Remote.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ReportProduksi
{
    public class DataTableReportProduksi
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_PRODUKSI> data { get; set; }
    }
}
