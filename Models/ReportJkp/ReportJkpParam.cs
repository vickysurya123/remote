﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportJkpParam
    {
        public string PROFIT_CENTER { get; set; }
        public string TRANSACTION_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string PERIOD { get; set; }
        public string POSTING_DATE_FROM { get; set; }
        public string POSTING_DATE_TO { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string CUSTOMER_ID { get; set; }
    }
}
