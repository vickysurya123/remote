﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDServiceGroup
    {
        public string REF_DATA { get; set; }

        public string REF_DESC { get; set; }

        public string VAL2 { get; set; }
        public string VAL3 { get; set; }
        public string VAL4 { get; set; }
        public string VAL1 { get; set; }
        public string ID { get; set; }
        public string PENDAPATAN { get; set;  }
    }
}