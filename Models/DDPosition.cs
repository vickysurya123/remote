﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDPosition
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string PROPERTY_ROLE { get; set; }
    }
}