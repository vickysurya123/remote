﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.Role
{
    public class DataTablesRole
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_ROLE_Remote> data { get; set; }
    }
}