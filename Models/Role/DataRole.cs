﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Role
{
    public class DataRole 
    {
        public string ROLE_ID { get; set; }

        public string ROLE_NAME { get; set; }

        public string APP_ID { get; set; }
        public string PROPERTY_ROLE { get; set; }
        public string PARAM1 { get; set; }
        public string PARAM2 { get; set; }
        public string PARAM3 { get; set; }
    }

}