﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.GeneralResult
{
    public class Results
    {
        public string Msg { get; set; }
        public string Status { get; set; }
        public object Data { get; set; }
        public object Positionlist { get; set; }
        public object dataPusat { get; set; }
        public object Customer { get; set; }
        public object Iddlekontrak { get; set; }
        public object Iddlenokontrak { get; set; }
        public object dataPusatBersertifikat { get; set; }
        public object CustomerBersertifikat { get; set; }
        public object IddleBersertifikatkontrak { get; set; }
        public object IddleBersertifikatnokontrak { get; set; }
        public object IddleBersertifikat_Belum { get; set; }
        public object CustomerBersertifikat_Belum { get; set; }
        public object dataPusatBersertifikat_Belum { get; set; }
        public object LahanPusat { get; set; }
        public object LahanCustomer { get; set; }
        public object LahanIddleKontrak { get; set; }
        public object LahanIddlenoKontrak { get; set; }
        public object LahanPusatBersertifikat { get; set; }
        public object LahanCustomerBersertifikat { get; set; }
        public object LahanIddleBersertifikatKontrak { get; set; }
        public object LahanIddleBersertifikatNoKontrak { get; set; }
    }
}