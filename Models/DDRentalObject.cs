﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDRentalObject
    {
        public string RO_CODE { set; get; }
        public string RO_NUMBER { set; get; }
        public string RO_NAME { set; get; }
    }
}