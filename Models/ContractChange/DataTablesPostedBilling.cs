﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ContractChange
{
    public class DataTablesPostedBilling
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_HISTORY_POSTED_BILLING> data { get; set; }
    }
}