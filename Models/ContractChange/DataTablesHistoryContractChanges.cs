﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ContractChange
{
    public class DataTablesHistoryContractChanges
    {  
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_TRANS_CONTRACT_WEBAPPS> data { get; set; }
    }
}