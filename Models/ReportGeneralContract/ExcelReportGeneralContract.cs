﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ReportGeneralContract
{
    public class ExcelReportGeneralContract
    {
        public string BUSINESS_ENTITY { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string CONTRACT_STATUS { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
    }
}
