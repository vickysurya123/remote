﻿using Remote.Entities;
using System.Collections.Generic;
namespace Remote.Models.MasterInstallation
{
    public class DataTablesInstallation
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_INSTALLATION_Remote> data { get; set; }
    }

    public class DataReturnInstallation
    {

        public string INSTALLATION_NUMBER { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_INSTALLATION> datas { get; set; }
    }
}