﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterInstallation
{
    public class DataInstallation
    {
        public string INSTALLATION_NUMBER { set; get; }
        public string INSTALLATION_TYPE { set; get; }
        public string BE_ID { set; get; }
        public string CUSTOMER_ID { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string CUSTOMER_SAP_AR { set; get; }
        public string INSTALLATION_DATE { set; get; }
        public string INSTALLATION_ADDRESS { set; get; }
        public string TARIFF_CODE { set; get; }
        public string MINIMUM_AMOUNT { set; get; }
        public string CURRENCY { set; get; }
        public string TAX_CODE { set; get; }
        public string RO_NUMBER { set; get; }
        public string STATUS { set; get; }
        public string POWER_CAPACITY { set; get; }
        public string BE_ID_1 { get; set; }
        public string BE_NAME { get; set; }
        public string RO_NAME { get; set; }
        public string AMOUNT { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CURRENCY_1 { get; set; }
        public string MINIMUM_PAYMENT { get; set; }
        public string MULTIPLY_FACT { get; set; }
        public string BIAYA_BEBAN { get; set; }
        public string MINIMUM_USED { get; set; }
        public string BIAYA_ADMIN { get; set; }
        public string RO_CODE { get; set; }
        public string ID { get; set; }
        public string SERIAL_NUMBER { get; set; }
    }
}