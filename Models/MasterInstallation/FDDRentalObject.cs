﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterInstallation
{
    public class FDDRentalObject
    {
        public string RO_NUMBER { set; get; }
        public string RO_NAME { set; get; }
        public DDRentalObject DDRentalObject { set; get; }
    }
}