﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterInstallation
{
    public class FDDTariffCode
    {
        public string TARIFF_CODE { set; get; }
        public string AMOUNT { set; get; }
        public string CURRENCY { set; get; }
        public DDTariffCode DDTariffCode { set; get; }
    }
}