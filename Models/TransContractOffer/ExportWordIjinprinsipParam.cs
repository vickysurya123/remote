﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TransContractOffer
{
    public class ExportWordIjinprinsipParam
    {
        public string CONTRACT_OFFER_NO { get; set; }
        public string CONTRACT_OFFER_TYPE { get; set; }
        public string BE_ID { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string BE_CITY { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string RO_ADDRESS { get; set; }
        public string VALID_FROM_DARATAN { get; set; }
        public string VALID_TO_DARATAN { get; set; }
        public string REGIONAL_NAME { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string NJOP_DARATAN { get; set; }
        public string NJOP_BANGUNAN { get; set; }
        public string UNIT_PRICE { get; set; }
        public string SEWA_DARATAN { get; set; }
        public string SEWA_BANGUNAN { get; set; }
        public string NORMALISASI_TANAH { get; set; }
        public string NORMALISASI_BANGUNAN { get; set; }
        public string PENGALIHAN { get; set; }
        public string ADMINISTRASI { get; set; }
        public string WARMERKING { get; set; }
        public string TOTAL_SEWA_BANGUNAN { get; set; }
        public string TOTAL_SEWA_DARATAN { get; set; }
        public string VALID_FROM_BANGUNAN { get; set; }
        public string VALID_FROM_NORMALISASI { get; set; }
        public string LUAS_NOR_DARATAN { get; set; }
        public string LUAS_NOR_BANGUNAN { get; set; }
        public string TOTAL_NOR_BANGUNAN { get; set; }
        public string TOTAL_NOR_DARATAN { get; set; }
        public string VALID_TO_BANGUNAN { get; set; }
        public string VALID_TO_NORMALISASI { get; set; }
        public string NJOP_NORMALISASI { get; set; }
    }
}
