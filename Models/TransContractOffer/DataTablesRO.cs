﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransContractOffer
{
    public class DataTablesRO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public string ro_id { set; get; }
        public List<AUP_TRANS_CO_RO_Remote> data { get; set; }
    }
}