﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;


namespace Remote.Models.TransContractOffer
{
    public class DataTablesGetRO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<V_SEARCH_DETAIL_RENTAL> data { get; set; }
    }
}