﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransContractOffer
{
    public class DataTransContrOffApv
    {
        public int APV_POSITION { get; set; }
        public int BE_ID { get; set; }
        public int TERM_IN_MONTHS { get; set; }
        public int LUAS { get; set; }
        public int CONTRACT_OFFER_NO { get; set; }
    }
}