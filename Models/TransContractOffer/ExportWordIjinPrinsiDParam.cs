﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TransContractOffer
{
    public class ExportWordIjinPrinsiDParam
    {
        public string SEWA_DARATAN { get; set; }
        public string SEWA_BANGUNAN { get; set; }
        public string PENGALIHAN { get; set; }
        public string ADMINISTRASI { get; set; }
        public string WARMERKING { get; set; }
        public string CONTRACT_OFFER_TYPE { get; set; }
        public string BE_ID { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string NORMALISASI { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string BE_CITY { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string RO_ADDRESS { get; set; }
        public string CREATION_DATE { get; set; }

    }
}
