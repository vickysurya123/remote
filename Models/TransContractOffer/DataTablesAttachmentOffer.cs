﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransContractOffer 
{
    public class DataTablesAttachmentOffer
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int be_id { set; get; }
        public List<AUP_OFFER_ATTACHMENT> data { get; set; }
    }
}

