﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransWEList
{
    public class DataTablesTransWEList
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        // public List<AUP_TRANS_WE_WEBAPPS> data { get; set; }
        public List<AUP_TRANS_WE_LIST_WEBAPPS> data { get; set; }
    }
}