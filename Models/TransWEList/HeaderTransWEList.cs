﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TransWEList
{
    public class HeaderTransWEList
    {
        public string TRANSACTION_ID { get; set; }
        public string INSTALLATION_ID { get; set; }
    }
}
