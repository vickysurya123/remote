﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TransWEList
{
    public class DataTransWEList
    {
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string ID { get; set; }
        public string QUANTITY { get; set; }
        public string AMOUNT { get; set; }
        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public string GRAND_TOTAL { get; set; }
        public string COA_PROD { get; set; }
        public string STATUS_DINAS { get; set; }
        public string RATE { get; set; }
    }
}