﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class DashExcelParam
    {
        public string TYPE { get; set; }
        public string BUSINESS_ENTITY { get; set; }
        public string BE_ID { get; set; }
        public string end { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string STATUS { get; set; }

    }
}
