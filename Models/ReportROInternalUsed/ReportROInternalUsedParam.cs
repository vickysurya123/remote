﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportROInternalUsedParam
    {
        public string BUSINESS_ENTITY { get; set; }
        public string YEARS { get; set; }
        public string USAGE_TYPE { get; set; }
        public string ZONE { get; set; }
        public string STATUS { get; set; }
    }
}
