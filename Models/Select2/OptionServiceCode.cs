﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class OptionServiceCode
    {
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string UNIT { get; set; }
        public string CURRENCY { get; set; }
        public string PRICE { get; set; }
        public string MULTIPLY_FUNCTION { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string SERVICE_GROUP { get; set; }
    }
}