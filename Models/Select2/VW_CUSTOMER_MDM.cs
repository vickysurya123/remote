﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class VW_CUSTOMER_MDM
    {
        public string id { get; set; }
        public string COSTUMER_ID { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
    }
}