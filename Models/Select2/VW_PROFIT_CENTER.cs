﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class VW_PROFIT_CENTER
    {
        public string id { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string TERMINAL_NAME { get; set; }
    }
}