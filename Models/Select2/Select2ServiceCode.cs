﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class Select2ServiceCode
    {
        public int total_count { get; set; }
        public bool incomplete_results { get; set; }
        public IEnumerable<OptionServiceCode> items { get; set; }
    }
}