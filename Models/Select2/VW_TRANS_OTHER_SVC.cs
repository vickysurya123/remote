﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class VW_TRANS_OTHER_SVC
    {
        public string ID { get; set; }
        public string POSTING_DATE { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string SERVICES_GROUP { get; set; }
        public string COSTUMER_ID { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string TOTAL { get; set; }
        public string FI_POST_STATUS { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string BILLING_TYPE { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string COMPANY_CODE { get; set; }
        public string TAX_CODE { get; set; }
        public string NO_TAX_TOTAL { get; set; }
        public string BRANCH_ID { get; set; }
    }
}