﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.Select2
{
    public class VW_SERVICE_GROUP
    {
        public string id { get; set; }
        public string REF_CODE { get; set; }
        public string REF_DATA { get; set; }
        public string REF_DESC { get; set; }
    }
}