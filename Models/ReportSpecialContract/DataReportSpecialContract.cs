﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ReportSpecialContract
{
    public class DataReportSpecialContract
    {
        public string IJIN_PRINSIP_NO { get; set; }
        public string IJIN_PRINSIP_DATE { get; set; }
        public string LEGAL_CONTRACT_NO { get; set; }
        public string LEGAL_CONTRACT_DATE { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string ID { get; set; }
        public string STATUS { get; set; }
        public string DIRECT_CONTRACT { get; set; }
        public string TERM_IN_MONTHS { get; set; }
        public string CONTRACT_STATUS { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string COMPANY_CODE { get; set; }
        public string CURRENCY { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
        public string CUSTOMER_AR { get; set; }
        public string CODE_CONTRACT_TYPE { get; set; }

        //DATA OBJECT
        public string OBJECT_TYPE { get; set; }
        public string RO_NAME { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_ID { get; set; }

        //DATA CONDITION
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string MONTHS { get; set; }
        public string STATISTIC { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string FORMULA { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string LUAS { get; set; }
        public string TOTAL { get; set; }
        public string NJOP_PERCENT { get; set; }
        public string SALES_RULE { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string UNIT_PRICE { get; set; }
        public string KONDISI_TEKNIS_PERCENT { get; set; }

        //MEMO
        public string CONDITION { get; set; }
        public string DUE_DATE { get; set; }
        public string NET_VALUE { get; set; }
        public string MEMO { get; set; }

        //DATA SALES BASED
        public string SALES_TYPE { get; set; }
        public string NAME_OF_TERM { get; set; }
        public string CALC_FROM { get; set; }
        public string CALC_TO { get; set; }
        public string UNIT { get; set; }
        public string PERCENTAGE { get; set; }
        public string TARIF { get; set; }
        public string RR { get; set; }
        public string MINSALES { get; set; }
        public string SR { get; set; }
        public string MINPRODUCTION { get; set; }
        public string PAYMENT_TYPE { get; set; }

        //DATA REPORTING RULE
        public string REPORTING_RULE_NO { get; set; }
        public string TERM_OF_REPORTING_RULE { get; set; }

        public string QUANTITY { get; set; }

        public List<Object> Objects { get; set; }
        public List<Condition> Conditions { get; set; }
        public List<Frequency> Frequencies { get; set; }
        public List<Memo> Memos { get; set; }
    }

    public class Frequency
    {
        public int MANUAL_NO { get; set; }
        public string CONDITION { get; set; } //Untuk condition ambil id CONDITION_CODE di index 9 yg di hidden
        public string CONDITION_TYPE { get; set; }
        public string DUE_DATE { get; set; }
        public int QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string NET_VALUE { get; set; }
        public string OBJECT_ID { get; set; }
    }
    public class Condition
    {
        public string CONTRACT_NO { get; set; }
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string MONTHS { get; set; }
        public string STATISTIC { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string FORMULA { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string LUAS { get; set; }
        public string TOTAL { get; set; }
        public string NJOP_PERCENT { get; set; }
        public string SALES_RULE { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string UNIT_PRICE { get; set; }
        public string KONDISI_TEKNIS_PERCENT { get; set; }
        public string TAX_CODE { get; set; }
        public string INSTALLMENT_AMOUNT { get; set; }
        public string CONDITION_TYPE_ID { get; set; }
        public string OBJECT_ID { get; set; }
    }
    public class Memo
    {
        //MEMO
        public string CONDITION { get; set; }
        public string DUE_DATE { get; set; }
        public string NET_VALUE { get; set; }
        public string MEMO { get; set; }
        public string CONTRACT_NO { get; set; }
        public string OBJECT_CALC { get; set; }
        public string CONDITION_TYPE { get; set; }
    }
    public class Object
    {
        //DATA OBJECT
        public string CONTRACT_NO { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string RO_NAME { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_ID { get; set; }
    }


    public class DataReturnTransNumber
    {
        public string ID_TRANS { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_TRANS_CONTRACT_WEBAPPS> datas { get; set; }
    }
}