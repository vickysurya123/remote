﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ReportSpecialContract
{
    public class ExcelReportSpecialContract
    {
        public string BUSINESS_ENTITY { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_STATUS { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
    }
}
