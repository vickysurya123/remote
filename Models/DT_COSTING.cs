﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DT_COSTING
    {
        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }
        public string ID { get; set; }
    }
}