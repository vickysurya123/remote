﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_RO_OBJECT
    {
        public string RENTAL_REQUEST_NO { get; set; }
        public string RO_NUMBER { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_ID { get; set; }
        public string RO_CODE { get; set; }
        public string RO_NAME { get; set; }
        public string OBJECT_TYPE_ID { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string ACTIVE { get; set; }
        public string OBJECT_TYPE { get; set; }

        //Tambahan Untuk di Form Contract
        public string OBJECT_NAME { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
        public string ID { get; set; }
        public string CONTRACT_NO { get; set; }
        public string LUAS { get; set; }
    }
}