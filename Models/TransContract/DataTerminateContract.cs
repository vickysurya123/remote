﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransContract
{
    public class DataTerminateContract
    {
        public string SELECT_CONTRACT_STATUS { get; set; }
        public string CONTRACT_NO { get; set; }
        public string STOP_CONTRACT_DATE { get; set; }
        public string STOP_CONTRACT_BY { get; set; }
        public string MEMO_CONTRACT { get; set; }  
    }
}
