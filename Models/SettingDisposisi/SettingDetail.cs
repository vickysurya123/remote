﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.SettingDisposisi
{
    public class SettingDetail
    {
        public SettingDetail()
        {
            Service = new List<SettingDetailD>();
            DeleteD = new List<SettingDeleteDetail>();
        }
        public string HEADER_ID { get; set; }
        public List<SettingDetailD> Service { get; set; }
        public List<SettingDeleteDetail> DeleteD { get; set; }

    }
}