﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.SettingDisposisi
{
    public class SettingDisposisi
    {
        // Header Data
        public string ID { get; set; }
        public string USER_ID { get; set; }
        public string NAME { get; set; }
        public string INSTRUKSI { get; set; }
        public string MEMO { get; set; }
        public string ROLE_ID { get; set; }
        public string IS_ACTIVE { get; set; }
        public string IS_DELETE { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string USER_LOGIN { get; set; }
        public string KD_CABANG { get; set; }
        public string USER_EMAIL { get; set; }
        public int URUTAN { get; set; }

    }
}