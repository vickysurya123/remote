﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.SettingDisposisi
{
    public class SettingDetailD
    {
        public string USER_ID { get; set; }
        public string NAME { get; set; }
        public string URUTAN { get; set; }
    }
}