﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_REPORTING_RULE
    {
        public string CONTRACT_OFFER_NO { get; set; }
        public string REPORTING_RULE_NO { get; set; }
        public string SALES_TYPE { get; set; }
        public string TERM_OF_REPORTING_RULE { get; set; }
    }
}