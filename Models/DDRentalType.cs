﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDRentalType
    {
        public string ID { get; set; }
        public string REF_DATA { get; set; }
        public string REF_DESC { get; set; }
        public string DESCRIPTION { get; set; }
    }
}