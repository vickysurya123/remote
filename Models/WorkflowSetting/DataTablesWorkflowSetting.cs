﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.WorkflowSetting
{
    public class DataTablesWorkflowSetting
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_WORKFLOW_SETTING> data { get; set; }
    }

}