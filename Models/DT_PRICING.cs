﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DT_PRICING
    {
        public string ID { get; set; }
        public string P_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string INSTALLATION_ID { get; set; }
        public string PRICE_TYPE { get; set; }
        public string MAX_RANGE_USED { get; set; }
        public string TARIFF_CODE { get; set; }
        public string PRICE_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string MULTIPLY_FACT { get; set; }
        public string CURRENCY { get; set; }
    }
}