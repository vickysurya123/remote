﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDProvince
    {
        public string ID { set; get; }
        public string PROVINCE_ID { get; set; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
    }
}