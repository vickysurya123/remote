﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDRefD
    {
        public long? ID { get; set; }
        public string REF_CODE { get; set; }
        public string REF_DATA { get; set; }
        public string PROGRAM_NAME { get; set; }
        public string REF_DESC { get; set; }
        public string ATTRIB1 { get; set; }
        public string ATTRIB2 { get; set; }
        public string ATTRIB3 { get; set; }
        public string ATTRIB4 { get; set; }
        public string VAL1 { get; set; }
        public string VAL2 { get; set; }
        public string VAL3 { get; set; }
        public string VAL4 { get; set; }
        public string STATUS { get; set; }
        public string CREATION_BY { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string ATTRIB5 { get; set; }
        public string VAL5 { get; set; }
        public string ACTIVE { get; set; }
        public string VAL1TMP { get; set; }
        public string SATUAN_MDM { get; set; }
    }
}