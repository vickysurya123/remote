﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterVariousBusiness
{
    public class Select2BEID
    {
        public int total_count { get; set; }
        public bool incomplete_results { get; set; }
        public IEnumerable<OptionBE> items { get; set; }
    }
}