﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.MasterRentalObject
{
    public class DataTablesRentalObject
    {
        public string RO_NUMBER { get; set; }
        public string BE_ID { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string RO_CITY { get; set; }
        public string RO_PROVINCE { get; set; }
        public string MEMO { get; set; }
        public string BE_CODE { get; set; }
        public string BE_TERMINAL { get; set; }
    }

    //GRID BE_ID_T
    public class DROPDOWN_LIST_BE_ID_T
    {
        public string BE_ID { get; set; }
        public string BE_CODE { get; set; }
        public string BE_NAME { get; set; }
    }

    public class DataTablesRentalObjectTest
    {
        public string RO_NUMBER { get; set; }
        public string BE_ID { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string RO_CITY { get; set; }
        public string RO_PROVINCE { get; set; }
    }

    public class DataTablesMasterRentalObject
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_RENTALOBJECT_Remote> data { get; set; }
    }

    public class DataTablesFixtureFitting
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_FIXTUREFITTING_Remote> data { get; set; }
    }

    public class DataTablesFixtureFitting2
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int ro_id { set; get; }
        public List<AUP_FIXTUREFITTING2_Remote> data { get; set; }
    }

    public class DataTablesMeasurement
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_MEASUREMENT_Remote> data { get; set; }
    }

    public class DataTablesMaps
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_MAPS_Remote> data { get; set; }
    }

    public class DataTablesOccupancy
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_OCCUPANCY_Remote> data { get; set; }
    }

    public class DataReturnRONumber
    {
        
        public string RO_NUMBER { get; set; }
        public string RO_ID { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_RO_NUMBER> datas { get; set; }
    }

    public class DataDDBusinessEntity
    {
        public string BE_ID { get; set; }
        public string BE_CODE { get; set; }
        public string BE_TERMINAL { get; set; }
        public string BE_NAME { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_CITY { get; set; }
        public string BE_PROVINCE { get; set; }
        public string BE_CONTACT_PERSON { get; set; }
        public string BE_NPWP { get; set; }
        public string BE_NOPPKP { get; set; }
        public string BE_PPKP_DATE { get; set; }
    }

    public class DataDDZONA_RIP
    {
        public string ID { set; get; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
    }

    public class DDMeasurement
    {
        public string ID { set; get; }
        public string REF_DESC { get; set; }
        
    }
    public class DataDDRO_TYPE
    {
        public string ID { set; get; }
        public string REF_CODE {set; get;}
        public string REF_DATA {set; get;}
        public string REF_DESC {set; get;}
    }

    public class DataDDUSAGE_TYPE
    {
        public string ID { set; get; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
    }

    public class DataDDProvince
    {
        public string ID { set; get; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
    }

    public class getGenerateRONumber
    {
        public string RO_NUMBER { set; get; }
    }


}