﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterRentalObject
{
    public class FDDFixFit
    {
        public string ID { set; get; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
        public DDFixFit DDFixFit { set; get; }
    }
}