﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterRentalObject
{
    public class FDDPeruntukan
    {
        public string PERUNTUKAN { set; get; }
        public FDDPeruntukan DDPeruntukan { set; get; }
    }
}