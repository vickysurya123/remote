﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.MasterRentalObject
{
    public class RentalObjectExcel
    {
        public string BE_ID { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string RO_NUMBER { get; set; }
        public string ZONE_RIP { get; set; }
        public string USAGE_TYPE { get; set; }
        public string FUNCTION { get; set; }
        public string VACANCY_STATUS { get; set; }
        public string VACANCY_REASON { get; set; }
        public string STATUS { get; set; }
        public string AVAILABLE_DATE { get; set; }
    }
}
