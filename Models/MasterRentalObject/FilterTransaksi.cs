﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterRentalObject
{
    public class FilterTransaksi
    {
        public string BE_ID { get; set; }
        public string BE_NAME { set; get; }
        public Terminal Terminal { get; set; }
    }
}