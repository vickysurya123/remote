﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterRentalObject
{
    public class DataRentalObject
    {
        public string RO_NUMBER { get; set; }
        public string RO_TYPE_ID { set; get; }
        public string ZONE_RIP_ID { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string LOCATION_ID { set; get; }
        public string FUNCTION_ID { set; get; }
        public string USAGE_TYPE_ID { set; get; }
        public string BE_ID { get; set; }
        public string RO_NAME { get; set; }
        public string RO_CERTIFICATE_NUMBER { get; set; }
        public string RO_ADDRESS { get; set; }
        public string RO_POSTALCODE { get; set; }
        public string RO_CITY { get; set; }
        public string RO_PROVINCE { get; set; }
        public string MEMO { get; set; }
        public string PROFIT_CENTER { set; get; }
        public string PROFIT_CENTER_ID { set; get; }
        public string RO_CODE { set; get; }
        public string KODE_ASET { set; get; }
        public string ACTIVE { set; get; }
        public string LATITUDE { set; get; }
        public string LONGITUDE { set; get; }
        public string LINI { set; get; }
        public string PERUNTUKAN { set; get; }
        public string SERTIFIKAT_ID { set; get; }
        public string SERTIFIKAT { set; get; }
        public string KETERANGAN { set; get; }
        public string SERTIFIKAT_NAME { set; get; }
    }

    public class DataFixtureFitting
    {
        public string ID { set; get; }
        public string FIXTURE_FITTING_CODE { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
        public string POWER_CAPACITY { set; get; }
        public string RO_NUMBER { set; get; }
        public string VAL_FROM { set; get; }
        public string VAL_TO { set; get; }
    }

    public class DataMeasurement
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string MEASUREMENT_TYPE { set; get; }
        public string DESCRIPTION { set; get; }
        public string AMOUNT { set; get; }
        public string UNIT { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
   
    }

    public class DataMaps
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string LATITUDE { set; get; }
        public string LONGITUDE { set; get; }
        public string NO_URUT { set; get; }
        public string BE_ID { set; get; }
    }

    public class DataOccupancy
    {
        public string ID { set; get; }
        public string RO_NUMBER { set; get; }
        public string VACANCY_REASON { set; get; }
        public string VALID_FROM { set; get; }
        public string VALID_TO { set; get; }
    }

    public class DropdownBE
    {
        public string be_id { get; set; }
        public string be_name{ get; set; }
    }

}