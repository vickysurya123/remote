﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class VW_BUSINESS_ENTITY
    {
        public string id { get; set; }
        public string BE_ID { get; set; }
        public string BE_NAME { get; set; }
        public string BE_ADDRESS {get; set;}
        public string BE_CITY {get; set;}
    }
}