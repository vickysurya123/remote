﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DisposisiListSurat
    {
        public string CONTRACT_NO { set; get; }
        public string SUBJECT { set; get; }
        public string ISI_SURAT { set; get; }
        public string COUNT_SURAT { set; get; }
    }
}