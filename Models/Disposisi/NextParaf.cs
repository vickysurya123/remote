﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class NextParaf
    {
        public string CONTRACT_NO { set; get; }
        public string USER_NAME { set; get; }
        public string ROLE_NAME { set; get; }
        public string SURAT_ID { set; get; }
        public string STATUS { set; get; }
    }
}