﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class Disposisi
    {
        public string CONTRACT_NO { set; get; }
        public string MEMO { set; get; }
        public string SUBJECT { set; get; }
        public string SURAT_ID { set; get; }
        public string DISPOSISI_KE { set; get; }
        public string DISPOSISI_NAMA { set; get; }
        public string NO_SURAT { set; get; }
        public string INSTRUKSI { set; get; }
        public int IS_CREATE_SURAT { set; get; }
        public int FLAG { set; get; }
    };

}