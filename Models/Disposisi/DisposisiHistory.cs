﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DisposisiHistory
    {
        public string CONTRACT_NO { set; get; }
        public string MEMO { set; get; }
        public string TYPE { set; get; }
        public string TIME_CREATE { set; get; }
        public string SUBJECT { set; get; }
        public string SURAT_ID { set; get; }
        public string DISPOSISI_ID { set; get; }
        public string NAMA { set; get; }
        public string INSTRUKSI { set; get; }
        public string NAMA_KEPADA { set; get; }
    }
}