﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class ReportRentalRequestParam
    {
        public string BUSINESS_ENTITY { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string ACTIVE_STATUS { get; set; }
        public string  RENTAL_REQUEST_STATUS { get; set; }
        public string RENTAL_REQUEST_TYPE { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
    }
}
