﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ReportROThirdPartyUsed
{
    public class DataTablesReportROThirdPartyUsed
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_RO_THIRD_PARTY_USED> data { get; set; }
    }
}