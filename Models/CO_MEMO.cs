﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_MEMO
    {
        public string ID { get; set; }
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string CONDITON_TYPE_MEMO { get; set; }
        public string MEMO { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
    }
}