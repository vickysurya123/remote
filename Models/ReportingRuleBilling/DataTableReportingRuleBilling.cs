﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ReportingRuleBilling
{
    public class DataTableReportingRuleBilling
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORTING_RULE> data { get; set; }
    }
}