﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.TarifPedoman
{
    public class AttachmentTarif
    {

        public string idPedoman { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
