﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.TarifPedoman
{
    public class DataTarifPedoman
    {
        public int ID { get; set; }
        public int BRANCH_ID { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string TIPE_TARIF { get; set; }
        public string BE_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string START_ACTIVE_DATE { get; set; }
        public string END_ACTIVE_DATE { get; set; }
        public List<Tarif> tarif { get; set; }
    }

    public class ActivePedoman
    {
        public int ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string TIPE_TARIF { get; set; }
        public string INDUSTRY_TYPE { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string START_ACTIVE_DATE { get; set; }
        public string END_ACTIVE_DATE { get; set; }
        public decimal VALUE { get; set; }
    }

    public class Tarif
    {
        public int id { get; set; }
        public string kode { get; set; }
        public int batas { get; set; }
        public decimal percentage { get; set; }
        public decimal value { get; set; }
    }

    public class DataReturnTarifPedoman
    {
        public int ID { get; set; }
        public bool RESULT_STAT { get; set; }
        public string MessageInfo { get; set; }
    }

    public class TARIF_PEDOMAN_HEADER
    {
        public int ID { get; set; }
        public int BRANCH_ID { get; set; }
        public string BE_ID { get; set; }
        public string PROFIT_CENTER_ID { get; set; }
        public string TIPE_TARIF { get; set; }
        public string BE_NAME { get; set; }
        public string PROFIT_CENTER_NAME { get; set; }
        public string START_ACTIVE_DATE { get; set; }
        public string END_ACTIVE_DATE { get; set; }

        public bool INACTIVE { get; set; }
        public string INACTIVE_MEMO { get; set; }
        public string INACTIVE_AT { get; set; }
        public string INACTIVE_BY { get; set; }
    }

    public class DataNJOP
    {
        public int ID { get; set; }
        public int BRANCH_ID { get; set; }
        public string BE_ID { get; set; }
        public string BE_NAME { get; set; }
        public int VALUE { get; set; }
        public string KEYWORD { get; set; }

        public bool ACTIVE { get; set; }
        public string ACTIVE_YEAR { get; set; }
        public string INACTIVE_MEMO { get; set; }
        public string INACTIVE_AT { get; set; }
        public string INACTIVE_BY { get; set; }
    }

}