﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ListNotifikasi
{
    public class ParamFirebase
    {
        public string username { get; set; }
        public string token_firebase { get; set; }
        public string device_info { get; set; }
    }

    public class ListFirebase
    {
        public string username { get; set; }
        public string token_firebase { get; set; }
    }

    public class ParamNotification
    {
        public string username { get; set; }
        public string type { get; set; }
        public string notification { get; set; }
        public string no_contract { get; set; }
        public string pengirim { get; set; }
    }

    public class ListNotif
    {
        public string id { get; set; }
        public string username { get; set; }
        public string type { get; set; }
        public string notification { get; set; }
        public string no_contract { get; set; }
        public string pengirim { get; set; }
        public string nama_penerima { get; set; }
        public string nama_pengirim { get; set; }
        public string IS_READ { get; set; }
        public string DESKRIPSI { get; set; }
        public string judul { get; set; }
    }

    public class NotifFirebase
    {
        public string token_firebase { get; set; }
        public string notification { get; set; }
        public string type { get; set; }
        public string no_contract { get; set; }
        public string pengirim { get; set; }
    }

    public class ResultsNotif
    {
        public string status { get; set; }
        public string message { get; set; }
        public IEnumerable<ListNotif> data { get; set; }
    }
}
