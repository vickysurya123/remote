﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.TransVB;

namespace Remote.Models.ListNotifikasi
{
    public class DataListNotifikasiDetail
    {
        public string ID { get; set; }
        public string HEADER_ID { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string NOTIF_START { get; set; }
        public string CREATED_DATE { get; set; }
        public string UPDATED_DATE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public List<ListRoleRow> Role { get; set; }
        //public List<ParamRow> Param { get; set; }
        public List<ListRoleRow> delRole { get; set; }
        public IEnumerable<ListServiceDetail> Service { get; internal set; }
        //public List<ParamRow> delParam { get; set; }
    }
}