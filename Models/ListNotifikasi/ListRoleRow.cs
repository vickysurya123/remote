﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ListNotifikasi
{
    public class ListRoleRow
    {
        public string ID { get; set; }
        public string HEADER_ID { get; set; }
        public string APPROVAL_NO { get; set; }
        public string PROPERTY_ROLE { get; set; }
        public int ROLE_ID { get; set; }
        public string LIMIT_TIME { get; set; }
        public string ROLE_NAME { get; set; }
    }
}