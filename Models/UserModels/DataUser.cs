﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.UserModels
{
    public class DataUser
    {
        public int ID { get; set; }
        public string USER_LOGIN { get; set; } 
        public string USER_PASSWORD { get; set; } 
        public string USER_NAME { get; set; } 
        public string USER_EMAIL { get; set; } 
        public string USER_PHONE { get; set; } 
        public int KD_CABANG { get; set; }  
        public string KD_CABANG2 { get; set; }
        public int KD_TERMINAL { get; set; } 
        public int APP_ID { get; set; } 
        public string STATUS { get; set; } 
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string PROFIT_CENTER_ID { get; set; }

    }
}