﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.DataTablesUser
{
    public class DataTablesUser
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_DATA_USER> data { get; set; } 
    }
     
    public class DataReturnUserNumber
    {
        public string USER_NUMBER { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_USER_NUMBER> datas { get; set; }
    }

}