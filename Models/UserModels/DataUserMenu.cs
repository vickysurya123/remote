﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.UserModels
{
    public class DataUserMenu
    {
        public int xID { get; set; }
        public int USER_ID { get; set; }
        public int MENU_ID { get; set; } 
        public int MENU_PARENT_ID { get; set; } 
        public int ORDERED_BY { get; set; } 
        public int APP_ID { get; set; } 
        public int STATUS { get; set; } 
        public int ROLE_ID { get; set; }  
        public int ROLE_ID2 { get; set; }
        public DateTime CREATION_DATE { get; set; }
    }
}