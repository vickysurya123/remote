﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.UserModels
{
    public class FDDBranch
    {
        public string BRANCH_ID { set; get; }
        public string BE_NAME { set; get; }
        public string BE_ID { set; get; }
    }
}