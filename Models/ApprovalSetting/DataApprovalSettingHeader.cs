﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ApprovalSetting
{
    public class DataApprovalSettingHeader
    {
        public string ID { get; set; }
        public string BE_ID { get; set; }
        public string REGIONAL_ID { get; set; }
        public string MODUL_ID { get; set; }
        public string CREATED_DATE { get; set; }
        public string UPDATED_DATE { get; set; }
    }
}