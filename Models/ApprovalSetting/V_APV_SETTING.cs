﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ApprovalSetting
{
    public class V_APV_SETTING
    {
        public string CONTRACT_OFFER_NO { get; set; }
        public string PROPERTY_ROLE { get; set; }
        public string ROLE_NAME { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public int LUAS { get; set; }
        public int WAKTU { get; set; }
        public int APPROVAL_NO { get; set; }
        public int APV_POSITION { get; set; }
        public int BE_ID { get; set; }
        public int MODUL_ID { get; set; }
        public int ID_DETAIL { get; set; }
        public string PROPERTY_ROLE_NEXT { get; set; }
        public string PROPERTY_ROLE_MAX { get; set; }
        public int OFFER_STATUS { get; set; }
        public int COMPLETED_STATUS { get; set; }
        public DateTime CREATION_DATE { get; set; }
    }
}