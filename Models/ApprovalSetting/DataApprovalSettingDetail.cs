﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ApprovalSetting
{
    public class DataApprovalSettingDetail
    {
        public string ID { get; set; }
        public string HEADER_ID { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string NOTIF_START { get; set; }
        public string CREATED_DATE { get; set; }
        public string UPDATED_DATE { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_USAGE { get; set; }
        public List<RoleRow> Role { get; set; }
        public List<ParamRow> Param { get; set; }
        public List<RoleRow> delRole { get; set; }
        public List<ParamRow> delParam { get; set; }
    }
}