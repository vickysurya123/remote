﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ApprovalSetting
{
    public class ParamRow
    {
        public string ID { get; set; }
        public string PARAM_ID { get; set; }
        public string PARAM_NAME { get; set; }
        public string MIN { get; set; }
        public string MAX { get; set; }
        public string DETAIL_ID { get; set; }
    }
}