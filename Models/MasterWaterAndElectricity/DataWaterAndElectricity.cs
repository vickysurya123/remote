﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.MasterWaterAndElectricity
{
    public class DataWaterAndElectricity
    {
        public string SERVICES_ID { set; get; }
        public string INSTALLATION_TYPE { set; get; }
        public string DESCRIPTION { set; get; }
        public string AMOUNT { set; get; }
        public string CURRENCY { set; get; }
        public string X_FACTOR { set; get; }
        public string FALID_FROM { set; get; }
        public string FALID_TO { set; get; }
        public string ACTIVE { set; get; }
        public string PER { set; get; }
        public string UNIT { set; get; }
        public string TARIFF_CODE { set; get; }
        public string PROFIT_CENTER { set; get; }
    }

    public class DataDDProfitCenter
    {
        public string ID { set; get; }
        public string PROFIT_CENTER_ID { set; get; }
        public string TERMINAL_CODE { set; get; }
        public string PROFIT_CENTER_GROUP { set; get; }
        public string TERMINAL_NAME { set; get; }
    }

    public class DataReturnWECode
    {
        public string TARIFF_CODE { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_WATERANDELECTRICITY_Remote> datas { get; set; }
    }
}