﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.MasterWaterAndElectricity
{
    public class DataTablesMasterWaterAndElectricity 
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_WATERANDELECTRICITY_Remote> data { get; set; }
    }

    public class DataReturnWaterAndElectricity
    {
        public string TARIFF_CODE { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_WATER_AND_ELECTRICITY> datas { get; set; }
    }
}