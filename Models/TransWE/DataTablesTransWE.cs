﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransWE
{
    public class DataTablesTransWE
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
       // public List<AUP_TRANS_WE_WEBAPPS> data { get; set; }
        public List<AUP_TRANS_WE_INSTALLATION_WEBAPPS> data { get; set; }
    }

    public class DataReturnWENumber
    {
        public string WE_NUMBER { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_BE_NUMBER> datas { get; set; }
    }

    public class DataReturnMany
    {
        public string LAST_LWBP { get; set; }
        public string LAST_WBP { get; set; }
        public string LAST_KVARH { get; set; }
        public string LAST_BLOK1 { get; set; }
        public string LAST_BLOK2 { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_MANY> datas { get; set; }
    }

}