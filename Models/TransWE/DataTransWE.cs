﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;


namespace Remote.Models.TransWE
{
    public class DataTransWE
    {
        public string ID { get; set; }
        public string ID_EDIT { get; set; }
        public string INSTALLATION_TYPE { get; set; }
        public string INSTALLATION_NUMBER { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string CUSTOMER { get; set; }
        public string PERIOD { get; set; }
        public string REPORT_ON { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string PRICE { get; set; }
        public string AMOUNT { get; set; }
        public string RATE { get; set; }
        public string STATUS { get; set; }
        public string BILLING_TYPE { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string FI_POST_STATUS { get; set; }
        public string CANCEL_STATUS { get; set; }
        public string SAP_DOCUMENT_NUMBER { get; set; }
        public string ID_INSTALASI { get; set; }
        public string ID_TRANSAKSI { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string STATUS_DINAS { get; set; }

        // INI UNTUK YG LISTRIK DETAIL LWBP DKK
        public string SERVICES_TRANSACTION_ID { get; set; }
        public string PRICE_TYPE { get; set; }
        public string PRICE_CODE { get; set; }
        public string TARIFF { get; set; }
        public string USED { get; set; }
        public string MULTIPLY { get; set; }
        public string BRANCH_ID { get; set; }

        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }

        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public string GRAND_TOTAL { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string COA_PROD { get; set; }
        public string KETERANGAN { get; set; }
        public List<detailpricings> detailpricings { get; set; }
        public List<detailcostings> detailcostings { get; set; }

        // UNTUK USED DETAIL
        public string ID_TRANSACTION { get; set; }
        public string SUMBER { get; set; }
        public string PENGALI { get; set; }
        public string TGL_STAN_AWAL { get; set; }
        public string TGL_STAN_AKHIR { get; set; }
        public string STAN_AWAL { get; set; }
        public string STAN_AKHIR { get; set; }
        public string SELISIH { get; set; }
        public string KWH_TENAN { get; set; }
        public string KWH_DITAGIHKAN { get; set; }
        public string TOTAL { get; set; }
        public string LIST { get; set; }
    }

    public class detailpricings
    {
        public string SERVICES_TRANSACTION_ID { get; set; }
        public string PRICE_TYPE { get; set; }
        public string PRICE_CODE { get; set; }
        public string TARIFF { get; set; }
        public string USED { get; set; }
        public string MULTIPLY { get; set; }
        public string BRANCH_ID { get; set; }
        public string METER_FROM { get; set;  }
        public string METER_TO { get; set; }
        public string INSTALLATION_CODE { get; set; }

        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }

        public string SURCHARGE { get; set; }
        public string SUB_TOTAL { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string COA_PROD { get; set; }
        public string KETERANGAN { get; set; }
    }

    public class detailcostings
    {
        public string DESCRIPTION { get; set; }
        public string PERCENTAGE { get; set; }
        public string SERVICES_TRANSACTION_ID { get; set; }
        public string BRANCH_ID { get; set; }
    }
}