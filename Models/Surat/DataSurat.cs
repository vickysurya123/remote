﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DataSurat
    {
        public string CONTRACT_NO { set; get; }
        public string MEMO { set; get; }
        public string SUBJECT { set; get; }
        public string SURAT_ID { set; get; }
        public string DISPOSISI_KE { set; get; }
        public string KODE_KLASIFIKASI { set; get; }
        public string NO_SURAT { set; get; }
        public string PARAF { set; get; }
        public string LINK { set; get; }
        public string TTD { set; get; }
        public string ISI_SURAT { set; get; }
        public string TEMBUSAN { set; get; }
        public string TEMBUSAN_EKSTERNAL { get; set; }
        public string KEPADA { set; get; }
        public string KEPADA_EKSTERNAL { get; set; }
        public string TANGGAL { set; get; }
        public string STATUS_KIRIM { set; get; }
        public string ATAS_NAMA { set; get; }
        public string NAMA_KEPADA { set; get; }
        public string NAMA_TEMBUSAN { set; get; }
        public string NAMA_TTD { set; get; }
        public string LIST_PARAF { set; get; }
        public string DIRECTORY { set; get; }
        public string FILE_NAME { set; get; }
        public int URUTAN { set; get; }
        public int PREV_SURAT { set; get; }
        public int STATUS_APV { set; get; }
        public int USER_ID_CREATE { set; get; }
        public int BRANCH_ID { set; get; }
        public string ID { set; get; }
        public string LIST_FILE { set; get; }
    }

    public class LIST_PARAF
    {
        public string HIRARKI { set; get; }
        public string NAMA { set; get; }
        public string USER_ID { set; get; }
    }
}