﻿namespace Remote.Models
{
    public class MainMenu
    {
        public int KD_CABANG { get; set; }
        public int ID { get; set; }
        public int ROLE_ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string MENU_NAMA { get; set; }
        public string MENU_URL { get; set; }
        public string MENU_ACTIVITY { get; set; }
        public int PERMISSION_ADD { get; set; }
        public int PERMISSION_UPD { get; set; }
        public int PERMISSION_DEL { get; set; }
        public int PERMISSION_VIW { get; set; }
        public int CHILD_COUNT { get; set; }
        public int MENU_ID { get; set; }
        public int MENU_PARENT_ID { get; set; }
        public int LEVEL_ID { get; set; }
        public string MENU_ICON { get; set; }
    }
}
