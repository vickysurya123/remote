﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ReportMasterInstallation
{
    public class ReportMasterInstallationExcel
    {
        public string PROFIT_CENTER { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_SAP { get; set; }
        public string STATUS { get; set; }
    }
}
