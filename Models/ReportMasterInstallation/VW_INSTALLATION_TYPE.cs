﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class VW_INSTALLATION_TYPE
    {
        public string INSTALLATION_TYPE { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; } 
        public string CUSTOMER_SAP_AR { get; set; }
    } 
}