﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Remote.Entities;

namespace Remote.Controllers
{
    class DataTableReportMasterInstallation
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_REPORT_MASTER_INSTALLATION> data { get; set; }
    }
}
