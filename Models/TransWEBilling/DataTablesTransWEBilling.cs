﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.TransWEBilling
{
    public class DataTablesTransWEBilling
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        // public List<AUP_TRANS_WE_WEBAPPS> data { get; set; }
        public List<AUP_TRANS_WE_BILLING_WEBAPPS> data { get; set; }
    }

    public class DataReturnSAP_COCUMENT_NUMBER
    {
        public string DOCUMENT_NUMBER { get; set; }
        public string BILL_ID { get; set; }
        public string RESULT_STAT { get; set; }
        public List<AUP_RETURN_SAP_DOCUMENT_NUMBER_BILLING> datas { get; set; }
    }
}