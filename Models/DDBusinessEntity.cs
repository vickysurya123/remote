﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDBusinessEntity
    {
        public int BRANCH_ID { set; get; }
        public string BE_ID { set; get; }
        public string BE_NAME { set; get; }
    }
    internal class DDHarbourClass
    {
        public string HB_ID { set; get; }
        public string HARBOUR_CLASS { set; get; }
    }
}