﻿
using System.Collections.Generic;

namespace Remote.Controllers
{
    internal class DataTableReportBE
    {
      public int draw { get; set; }
      public int recordsTotal { get; set; }
      public int recordsFiltered { get; set; }
      public List<AUP_REPORT_BE> data { get; set; }
        
    }
}