﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DataRkapDetail
    {
        public decimal TRITAHUNINI { get; set; }
        public decimal TRITAHUNLALU { get; set; }
        public string COA_PRODUKSI { get; set; }
    }
}