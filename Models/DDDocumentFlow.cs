﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DDDocumentFlow
    {
        /** Contract Offer **/
        public string CONTRACT_OFFER_NO { get; set; } 
        public string OFFER_NAME { get; set; } 
        public string REF_CODE { get; set; }
        public string REF_NAME { get; set; }

        /** Rental Request**/
        public string RENTAL_REQUEST_NO { get; set; }
        public string REQUEST_NAME { get; set; }

        /** Rental Request Status**/
        public string STATUS { get; set; }

        /** Contract Number **/
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_NAME { get; set; }
    }
}