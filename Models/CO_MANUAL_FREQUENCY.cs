﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_MANUAL_FREQUENCY
    {
        public string ID { get; set; }
        public string CONTRACT_OFFER_NO { get; set; }
        public string MANUAL_NO { get; set; }
        public string CONDITION { get; set; }
        public string CONDITION_FREQ { get; set; }
        public string DUE_DATE { get; set; }
        public string NET_VALUE { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
    }
}