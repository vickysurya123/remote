﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ReportTransBillingProperty
{
    public class FDDBusinessEntity
    {
        public string BE_ID { set; get; }
        public string BE_NAME { set; get; }
    }
}