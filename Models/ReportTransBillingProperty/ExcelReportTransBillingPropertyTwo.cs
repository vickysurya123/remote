﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.ReportTransBillingProperty
{
    public class ExcelReportTransBillingPropertyTwo
    {
        public string PROFIT_CENTER { get; set; }
        public string BUSINESS_ENTITY { get; set; }
        public string BILLING_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string POSTING_DATE_FROM { get; set; }
        public string POSTING_DATE_TO { get; set; }
        public string CUSTOMER_ID { get; set; }
    }
}
