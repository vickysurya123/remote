﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.ReportTransBillingProperty
{
    public class FDDProfitCenter
    {
        public string PROFIT_CENTER_ID { get; set; }

        public string TERMINAL_NAME { get; set; }

        public DDProfitCenter DDProfitCenter { set; get; }
    }
}