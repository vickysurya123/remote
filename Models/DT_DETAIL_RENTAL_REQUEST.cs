﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DT_DETAIL_RENTAL_REQUEST
    {
        public string ID { get; set; }
        public string RENTAL_REQUEST_NO { get; set; }
        public string RO_NUMBER { get; set; }
        public string LAND_DIMENSION { get; set; }
        public string BUILDING_DIMENSION { get; set; }
        public string INDUSTRY { get; set; }
        public string OBJECT_ID { get; set; }
        public string OBJECT_TYPE_ID { get; set; }
        public string BRANCH_ID { get; set; }
        public string RO_NAME { get; set; }
        public string OBJECT_TYPE { get; set; }

    }
}