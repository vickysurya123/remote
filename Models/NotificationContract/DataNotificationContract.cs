﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.NotificationContract
{
    public class DataNotificationContract
    { 
        public string CONTRACT_NO { get; set; }
        public string STOP_NOTIFICATION_DATE { get; set; }
        public string STOP_NOTIFICATION_BY { get; set; }
        public string STOP_WARNING_DATE { get; set; }
        public string STOP_WARNING_BY { get; set; }
        public string LEAVE_OUT_DATE { get; set; }
        public string LEAVE_OUT_BY { get; set; }
        public string MEMO_NOTIFICATION { get; set; }
        public string MEMO_WARNING { get; set; }  
    }
}
