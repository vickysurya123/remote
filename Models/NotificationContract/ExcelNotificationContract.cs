﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.NotificationContract
{
    public class ExcelNotificationContract
    {
        public string BUSINESS_ENTITY { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string DATE_START { get; set; }
        public string DATE_END { get; set; }
    }


    public class ExcelNotificationContractWarning
    {
        public string BUSINESS_ENTITY_WARNING { get; set; }
        public string CONTRACT_NO_WARNING { get; set; }
        public string CONTRACT_TYPE_WARNING { get; set; }
        public string CUSTOMER_ID_WARNING { get; set; }
        public string DATE_START_WARNING { get; set; }
        public string DATE_END_WARNING { get; set; }
    }
}
