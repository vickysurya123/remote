﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.RentalObject
{
    public class RentalObjectAttachment
    {
        public string RO_NUMBER { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
