﻿using Remote.Entities;
using System.Collections.Generic;
 
namespace Remote.Models.RentalObject
{
    public class DataTablesRoAttachment
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int ro_number { set; get; }
        public List<AUP_RO_ATTACHMENT> data { get; set; }

    }
}