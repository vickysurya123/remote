﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models
{
    public class GetUrl
    {
        public string ApiUrl
        {
            get
            {
                return "https://localhost:5004/api/";
            }
        }
        public string UploadCloadUrl
        {
            get
            {
                return "https://clouds3.pelindo.co.id/";
            }
        }
        public string MailUrl
        {
            get
            {
                return "http://mailapi.pelindo.co.id/api/";
            }
        }
    }
}
