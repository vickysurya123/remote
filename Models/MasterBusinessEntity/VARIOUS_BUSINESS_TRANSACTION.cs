﻿using System;
namespace Remote.Models
{
    public class VARIOUS_BUSINESS_TRANSACTION
    {
        public string INSTALLATION_ADDRESS { get; set; }
        public string CUSTOMER_SAP_AR { get; set; }
        public string TAX_CODE { get; set; }
        public long? TOTAL { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string SERVICES_GROUP { get; set; }
        public string COSTUMER_NAME { get; set; }
        public string BUSINESS_ENTITY { get; set; }
        public DateTime? POSTING_DATE { get; set; }
        public string COSTUMER_MDM { get; set; }
        public long? ID { get; set; }
    }
}
