﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterBusinessEntity
{
    public class FDDProvince1
    {
        public string ID { set; get; }
        public string PROVINCE_ID { set; get; }
        public string REF_CODE { set; get; }
        public string REF_DATA { set; get; }
        public string REF_DESC { set; get; }
        public DDProvince DDProvince { set; get; }
    }
}