﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.Models.MasterBusinessEntity
{
    public class BusinessEntityAttachment
    {
        public string BE_ID { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
