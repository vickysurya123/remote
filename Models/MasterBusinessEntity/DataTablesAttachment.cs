﻿using Remote.Entities;
using System.Collections.Generic;
 
namespace Remote.Models.MasterBusinessEntity
{
    public class DataTablesAttachment
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int be_id { set; get; }
        public List<AUP_BE_ATTACHMENT> data { get; set; }

    }
}