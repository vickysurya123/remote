﻿using System;
namespace Remote.Models
{
    public class BUSINESS_ENTITY_DETAIL
    {
        public long? ID { get; set; }
        public string BE_ID { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public long? AMOUNT { get; set; }
        public string UNIT { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public DateTime? VALID_TO { get; set; }
    }
}
