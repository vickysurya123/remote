﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterBusinessEntity
{
    public class DataMeasurement
    {
        public string ID { get; set; }

        public string BE_ID { get; set; }

        public string MEASUREMENT_TYPE { get; set; }

        public string DESCRIPTION { get; set; }

        public string AMOUNT { get; set; }

        public string UNIT { get; set; }

        public string VALID_FROM { get; set; }

        public string VALID_TO { get; set; }
    }
}