﻿using System;
namespace Remote.Models
{
    public class BUSINESS_ENTITY
    {
        public string HARBOUR_CLASS { get; set; }
        public string POSTAL_CODE { get; set; }
        public long? ID { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public string BE_ID { get; set; }
        public DateTime? VALID_TO { get; set; }
        public string BE_CODE { get; set; }
        public long? BE_TERMINAL { get; set; }
        public string BE_NAME { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_CITY { get; set; }
        public string BE_PROVINCE { get; set; }
        public string BE_CONTACT_PERSON { get; set; }
        public string BE_NPWP { get; set; }
        public string BE_NOPPKP { get; set; }
        public DateTime? BE_PPKP_DATE { get; set; }
        public string CREATION_BY { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
    }
}
