﻿using System;
namespace Remote.Models
{
    public class VARIOUS_BUSINESS
    {
        public string GL_ACCOUNT { get; set; }
        public long? ID { get; set; }
        public string BE_ID { get; set; }
        public long? SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string SERVICE_GROUP { get; set; }
        public string UNIT { get; set; }
    }
}
