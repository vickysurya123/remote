﻿using Remote.Entities;
using System.Collections.Generic;

namespace Remote.Models.MasterBusinessEntity
{
    public class DataTablesBusinessEntity
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AUP_BE_WEBAPPS> data { get; set; }
    }

    public class DataReturnBENumber
    {
        public string BE_NUMBER { get; set; }
        public bool RESULT_STAT { get; set; }
        public List<AUP_RETURN_BE_NUMBER> datas { get; set; }
    }
}