﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models.MasterBusinessEntity
{
    public class FDDHarbour
    {
        public string ID { set; get; }
        public string HB_ID { set; get; }
        public string REF_DESC { set; get; }
    }
}