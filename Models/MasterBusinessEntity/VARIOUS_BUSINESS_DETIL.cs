﻿using System;
namespace Remote.Models
{
    public class VARIOUS_BUSINESS_DETIL
    {
        public long? ID { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
    }
}
