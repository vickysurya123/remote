﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.Models.ApprovedList
{
    public class DataApprovedList
    {
        //WORKFLOW LOG
        public long CONTRACT_OFFER_NO { get; set; }
        public string STATUS_CONTRACT { get; set; }
        public string USER_LOGIN { get; set; }
        public string BRANCH_ID { get; set; }
        public string CREATION_DATE { get; set; }
        public string CREATION_BY { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }	
    }
}