﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DD_ELECTRICITY_TRANS_D
    {
        public string ID { get; set; }
        public string SERVICES_TRANSACTION_ID { get; set; }
        public string PRICE_TYPE { get; set; }
        public string PRICE_CODE { get; set; }
        public string TARIFF { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string USED { get; set; }
        public string MULTIPLY { get; set; }
        public string BRANCH_ID { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string COA_PROD { get; set; }
        public string KETERANGAN { get; set; }
    }
}