﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class CO_CONDITION
    {
        public string CONTRACT_OFFER_NO { get; set; }
        public string ID { get; set; }
        public string CALC_OBJECT { get; set; }
        public string CONDITION_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string MONTHS { get; set; }
        public string STATISTIC { get; set; }
        public string UNIT_PRICE { get; set; }
        public string AMT_REF { get; set; }
        public string FREQUENCY { get; set; }
        public string START_DUE_DATE { get; set; }
        public string MANUAL_NO { get; set; }
        public string FORMULA { get; set; }
        public string MEASUREMENT_TYPE { get; set; }
        public string LUAS { get; set; }
        public string TOTAL { get; set; }
        public string NJOP_PERCENT { get; set; }
        public string KONDISI_TEKNIS_PERCENT { get; set; }
        public string SALES_RULE { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string COA_PROD { get; set; }
    }
}