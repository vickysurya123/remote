﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace Remote.Models
{
    public class UploadFilesResult
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Length { get; set; }

        public string BE_ID { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME {get; set;}
        public string BRANCH_ID { get; set; }
    }
}