﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Models
{
    public class DataDetailDashboard
    {
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_NO_NEW { get; set; }
        public string CONTRACT_NAME { get; set; }
        public string BUSINESS_PARTNER { get; set; }
        public string BUSINESS_PARTNER_NAME { get; set; }
        public string MPLG_BADAN_USAHA { get; set; }
        public string CONTRACT_END_DATE { get; set; }
        public string REF_CODE { get; set; }
        public string BRANCH_ID { get; set; }
        public string BE_NAME { get; set; }
        public string REF_DESC { get; set; }
        public string CONTRACT_START_DATE { get; set; }
        public string STATUS_NOW { get; set; }
        public string STATUS { get; set; }
        public string TOTAL_NET_VALUE { get; set; }
        public string LUAS_TANAH { get; set; }
        public string LUAS_BANGUNAN { get; set; }
    }
}