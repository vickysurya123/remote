﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MasterConfiguration;

namespace Remote.ViewModels
{
    public class Configuration
    {
        public IEnumerable<DDConfig> DDConfig { get; set; }

        public FDDConfig FDDConfig;
    }
}