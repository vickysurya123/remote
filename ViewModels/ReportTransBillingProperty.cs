﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.ReportTransBillingProperty;

namespace Remote.ViewModels
{
    public class ReportTransBillingProperty 
    {

        public FDDBusinessEntity FDDBusinessEntity; 
        public IEnumerable<DDBusinessEntity> DDBusinessEntity { get; set; }

        public FDDProfitCenter FDDProfitCenter;
        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

    }
}