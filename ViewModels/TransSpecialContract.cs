﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.TransSpecialContract;

namespace Remote.ViewModels
{
    public class TransSpecialContract
    {

        public FDDBusinessEntity FDDBusinessEntity;

        public IEnumerable<DDBusinessEntity> DDBusinessEntity { get; set; }

        public FDDContractUsage FDDContractUsage;

        public IEnumerable<DDContractUsage> DDContractUsage { get; set; }

        public FDDProfitCenter FDDProfitCenter;

        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

        public FDDContractType FDDContractType;

        public IEnumerable<DDContractType> DDContractType { get; set; }

    }
}