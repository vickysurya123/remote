﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.ViewModels
{
    public class EmailNotificationViewModel
    {
        public string NamaCabang { get; set; }
        public string AlamatCabang { get; set; }
        public string Kota { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
    }
}