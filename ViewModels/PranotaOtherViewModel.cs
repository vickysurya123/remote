﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.ViewModels
{
	public class PranotaOtherViewModel
	{
        public string COSTUMER_ID { get; set; }
        public string COSTUMER_MDM { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string TERMINAL_NAME { get; set; }
        public string INSTALLATION_ADDRESS { get; set; }
        public string BILLING_NO { get; set; }
        public string TOTAL { get; set; }
        public string BRANCH_ID { get; set; }
        public string BE_CITY { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_NAME { get; set; }
        public string ID { get; set; }
        public string Valuta { get; set; }
        public string Terbilang { get; set; }
        public string TotalTagihan { get; set; }
        public string TglPosting { get; set; }
        public int LIVE_1PELINDO { get; set; }

        public List<PRANOTA_OTHER_DETAIL> Detail { get; set; }
    }
}