﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MonitoringKronologisMaster;
using Remote.Models.MasterBusinessEntity;
using Remote.Models.MasterRentalObject;
using Remote.Models.UserModels;

namespace Remote.ViewModels
{
    public class UserViewModels
    {
        public IEnumerable<DDBranch> DDBranch { get; set; }

        public FDDBranch FDDBranch;

        public FDDRentalType FDDRentalType;

        public IEnumerable<DDRentalType> DDRentalType { get; set; }

        public FDDContractUsage FDDContractUsage;

        public IEnumerable<DDContractUsage> DDContractUsage { get; set; }

        public IEnumerable<DDBranch> DDBusinessEntity { get; set; }

        public FDDBusinessEntity FDDBusinessEntity;

        public IEnumerable<DDPosition> DDPosition { get; set; }

        public FDDPosition FDDPosition;

        public IEnumerable<DDCabang> DDCabangs { get; set; }

        public IEnumerable<DDRegional> DDRegionals { get; set; }


        public FDDCabang FDDCabang;

        public FDDRegional FDDRegional;

        public DataMonitoringKronologis DataMonitoringKronologis { get; set; }
        public List<detailKronologis> detailKronologis { get; set; }

        //public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

        //public FDDProfitCenter FDDProfitCenter;
    }
}