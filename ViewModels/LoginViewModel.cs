﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Remote.ViewModels
{
    public class LoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public Boolean remember { get; private set; }

        public LoginViewModel(){
            this.remember = true;
        }
    }
}
