﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.ViewModels
{
    public class VM_BE
    {
        public string BE_ID { set; get; }
        public string BE_CODE { set; get; }
        public string BE_NAME { set; get; }
    }
}