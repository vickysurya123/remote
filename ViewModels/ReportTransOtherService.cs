﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.ReportTransOtherService;

namespace Remote.ViewModels
{
    public class ReportTransOtherService 
    {

        public FDDBusinessEntity FDDBusinessEntity;
        public IEnumerable<DDBusinessEntity> DDBusinessEntity { get; set; }

        public FDDProfitCenter FDDProfitCenter;
        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

        public IEnumerable<DDServiceGroup> DDServiceGroup { get; set; }
        public FDDServiceGroup FDDServiceGroup { get; set; }

    }
}