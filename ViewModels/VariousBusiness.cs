﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MasterVariousBusiness;

namespace Remote.ViewModels
{
    public class VariousBusiness
    {
        public IEnumerable<DDBusinessEntity> DDBusinessEntitys { get; set; }

        public FDDBusinessEntity FDDBusinessEntity;

        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

        public FDDProfitCenter FDDProfitCenter;

        public IEnumerable<DDServiceGroup> DDServiceGroup { get; set; }

        public FDDServiceGroup FDDServiceGroup { get; set; }

        public IEnumerable<DDUnit> DDUnit { get; set; }

        public FDDUnit FDDUnit { get; set; }

    }
}