﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MasterBusinessEntity;

namespace Remote.ViewModels
{
    public class BusinessEntity
    {
        public IEnumerable<DDHarbour1> DDHarbour1 { get; set; }

        public FDDHarbour FDDHarbour;

        public IEnumerable<DDProvince> DDProvince { get; set; }

        public FDDProvince1 FDDProvince1;
    }
}