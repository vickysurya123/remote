﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Entities;

namespace Remote.ViewModels
{
    public class PranotaPropertiViewModel
    {
        public PranotaPropertiViewModel()
        {
            Detail = new List<PRANOTA_PROPERTI_DETAIL>();
            DetailMemo = new List<PRANOTA_PROPERTY_MEMO>();
            DetailMemodua = new List<PRANOTA_PROPERTY_MEMO_DUA>();
            DetailWater = new List<PRANOTA_WATER>();
            DetailElectricity = new List<PRANOTA_ELECTRICITY>();
            DetailOtherService = new List<PRANOTA_OTHER_DETAIL>();

        }
        public string BILLING_NO { get; set; }
        public string ATAS_BEBAN { get; set; }
        public string ALAMAT { get; set; }
        public string PROFIT_CENTER { get; set; }
        public string Valuta { get; set; }
        public string NamaCabang { get; set; }
        public string AlamatCabang { get; set; }
        public string TglPosting { get; set; }
        public string Kota { get; set; }
        public string Jabatan { get; set; }
        public string PathTTD { get; set; }
        public string NamaManager { get; set; }
        public string NRK { get; set; }
        public string TotalTagihan { get; set; }
        public string TarifWater { get; set; }
        public string Admin { get; set; }
        public string Surcharge { get; set; }
        public string Terbilang { get; set; }
        public string BILLING_ID { get; set; }
        public string METER_FROM { get; set; }
        public string METER_TO { get; set; }
        public string INSTALLATION_CODE { get; set; }
        public string PERIOD { get; set; }
        public string POWER_CAPACITY { get; set; }
        public string MULTIPLY_FACTOR { get; set; }
        public string BEBAN { get; set; }
        public string DAYA { get; set; }
        public string FAKTOR_KALI { get; set; }
        public string PERCEN_PPJU { get; set; }
        public string PERCEN_REDUKSI { get; set; }
        public string BE_CITY { get; set; }
        public string BRANCH_ID { get; set; }
        public int LIVE_1PELINDO { get; set; }

        public List<PRANOTA_PROPERTI_DETAIL> Detail { get; set; }
        public List<PRANOTA_PROPERTY_MEMO> DetailMemo { get; set; }
        public List<PRANOTA_PROPERTY_MEMO_DUA> DetailMemodua { get; set; }
        public List<PRANOTA_WATER> DetailWater { get; set; }
        public List<PRANOTA_ELECTRICITY> DetailElectricity { get; set; }
        public List<PRANOTA_OTHER_DETAIL> DetailOtherService { get; set; }
    }
}