﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.TransRentalRequest;

namespace Remote.ViewModels
{
    public class TransRentalRequest
    {

        public FDDBusinessEntity FDDBusinessEntity;

        public IEnumerable<DDBusinessEntity> DDBusinessEntity { get; set; }

        public FDDRentalType FDDRentalType;

        public IEnumerable<DDRentalType> DDRentalType { get; set; }

        public FDDContractUsage FDDContractUsage;

        public IEnumerable<DDContractUsage> DDContractUsage { get; set; }

        public FDDIndustry FDDIndustry;

        public IEnumerable<DDIndustry> DDIndustry { get; set; }

        public FDDProfitCenter FDDProfitCenter;

        public IEnumerable<DDProfitCenter> DDProfitCenters { get; set; }

        public IEnumerable<DDCurrency> DDCurrencys { get; set; }

        public FDDCurrency FDDCurrency;
    }
}