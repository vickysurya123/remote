﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.TransContractOffer;
using Remote.Models.MasterRentalObject;
using FDDBusinessEntity = Remote.Models.MasterRentalObject.FDDBusinessEntity;

 
namespace Remote.ViewModels
{
    public class ContractOffer
    {
        public IEnumerable<DDBusinessEntity> DDBusinessEntitys { get; set; }
        public IEnumerable<DDProfitCenter> DDProfitCenters { get; set; }
        public IEnumerable<DDContractUsage> DDContractUsages { get; set; }


        public FDDBusinessEntity FDDBusinessEntity;
        public FDDProfitCenter FDDProfitCenter;
        public FDDContractUsage FDDContractUsage;

    }
}