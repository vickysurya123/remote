﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MasterRentalObject;
//using Remote.Models.MasterVariousBusiness;
using FDDBusinessEntity = Remote.Models.MasterRentalObject.FDDBusinessEntity;

namespace Remote.ViewModels
{
    public class RentalObject
    {
        public IEnumerable<DDBusinessEntity> DDBusinessEntitys { get; set; }
        public IEnumerable<DDRoType> DDRoTypes { get; set; }
        public IEnumerable<DDZoneRip> DDZoneRips { get; set; }
        public IEnumerable<DDRoFunction> DDRoFunctions { get; set; }
        public IEnumerable<DDUsageTypeRo> DDUsageTypeRos { get; set; }
        public IEnumerable<DDLini> DDLinis { get; set; }
        public IEnumerable<DDPeruntukan> DDPeruntukans { get; set; }
        public IEnumerable<DDProvince> DDProvinces { get; set; }
        public IEnumerable<DDProfitCenter> DDProfitCenters { get; set; }
        public IEnumerable<DDRoLocation> DDRoLocations { get; set; }
        public IEnumerable<DDMeasurementType> DDMeasurementTypes { get; set; }
        public IEnumerable<DDFixFit> DDFixFits { get; set; }
        public IEnumerable<DDVacancyReason> DDVacancyReason { get; set; }




        public FDDBusinessEntity FDDBusinessEntity;
        public FDDRoType FDDRoType;
        public FDDZoneRip FDDZoneRip;
        public FDDLini FDDLini;
        public FDDPeruntukan FDDPeruntukan;
        public FDDRoFunction FDDRoFunction;
        public FDDUsageTypeRo FDDUsageTypeRo;
        public FDDProvince FDDProvince;
        public FDDProfitCenter FDDProfitCenter;
        public FDDRoLocation FDDRoLocation;
        public FDDMeasurementType FDDMeasurementType;
        public FDDVacancyReason FDDVacancyReason;
        public FDDFixFit FDDFixFit;

    }
}