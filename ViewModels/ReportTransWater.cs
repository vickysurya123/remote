﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.ReportTransWater;

namespace Remote.ViewModels
{
    public class ReportTransWater
    {

        public FDDBusinessEntity FDDBusinessEntity; 
        public IEnumerable<DDBusinessEntity> DDBusinessEntity { get; set; }

        public FDDProfitCenter FDDProfitCenter;
        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

    }
}