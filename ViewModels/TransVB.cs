﻿ ﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.TransVB;

namespace Remote.ViewModels
{
    public class TransVB
    {
        public IEnumerable<DDProfitCenter> DDProfitCenters { get; set; }

        public FDDProfitCenter FDDProfitCenter;

        public IEnumerable<DDServiceGroup> DDServiceGroup { get; set; }

        public FDDServiceGroup FDDServiceGroup { get; set; }
    }
}