﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.UserModels;

namespace Remote.ViewModels
{
    public class TarifPedoman
    {
        public IEnumerable<DDBranch> DDBranch { get; set; }

        public FDDBranch FDDBranch;

        public IEnumerable<DDProfitCenter> DDProfitCenters { get; set; }
    }
}