﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.ViewModels
{
    public class FilterRentalDetail
    {
        public string RO_NUMBER { get; set;}
        public string RO_NAME { get; set;}
        public string ID { get; set;}
        public string MEASUREMENT_TYPE { get; set;}
        public string DESCRIPTION { get; set;}
        public string AMOUNT { get; set;}
        public string UNIT { get; set;}
        public string VAL_FROM { get; set;}
        public string VAL_TO { get; set;}
        public string ACTIVE { get; set; }
    }
}