﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.MasterInstallation;

namespace Remote.ViewModels
{
    public class Installation
    {
        public IEnumerable<DDProfitCenter> DDProfitCenter { get; set; }

        public FDDProfitCenter FDDProfitCenter;

        public IEnumerable<DDTariffCode> DDTariffCode { get; set; }

        public FDDTariffCode FDDTariffCode;

        public IEnumerable<DDRentalObject> DDRentalObject { get; set; }

        public FDDRentalObject FDDRentalObject;
    }
}