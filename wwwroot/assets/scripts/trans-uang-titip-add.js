﻿var TransAddUangTitip = function () {
    var addDetail = function () {
        var detailTable = $('#table-detail').dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "paging": false,
            "lengthChange": false,
            "bSort": false
        });

        $('#btn-add-detail').on('click', function () {
            var inputDate = '<center><input type="text" class="form-control date-picker" id="date-titip" name="date-titip" value="' + todayDate() + '" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var inputValue = '<center><input type="text" class="form-control" id="value-titip" name="value-titip" /></center>';
            var action = '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a></center>';
            detailTable.fnAddData([inputDate, inputValue, 'BELUM DIPOSTING', action], true)

            $('input[name = "date-titip"]').inputmask("d/m/y", {
                placeholder: "dd/mm/yyyy"
            });

            $('input[name = "value-titip"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });
        });

        $('#table-detail').on('click', 'tr #btn-savecus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();

            var a = $('#date-titip').val();
            var b = $('#value-titip').val();
            tombol = '<center><a class="btn default btn-xs blue" id="btn-posting"><i class="fa fa-paper-plane-o"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            detailTable.fnUpdate(a, baris, 0, false);
            detailTable.fnUpdate(b, baris, 1, false);
            detailTable.fnUpdate(tombol, baris, 3, false);
        });


        $('#table-detail').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();
            detailTable.fnDeleteRow(baris);

        });


        $('#table-detail').on('click', 'tr #btn-posting', function () {
            swal({
                title: "Posting Uang Titip",
                html: '<label class="control-label">Masukkan tanggal posting: (kosongkan bila diposting pada hari ini)</label><input class="form-control" type="text" id="date-post"/>',
                showCancelButton: true,
                closeOnConfirm: false,
                onOpen: function () {
                    $('#date-post').inputmask("d/m/y", {
                        placeholder: "dd/mm/yyyy"
                    });
                },
            }, function (isConfirm) {
                if (isConfirm) {
                    var inputValue = $('#date-post').val();
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-detail').DataTable();
                    var data = table.row(baris).data();
                    tombol = '<center><a class="btn default btn-xs yellow" id="btn-undo"><i class="fa fa-undo"></i></a></center>';

                    detailTable.fnUpdate('DIPOSTING PADA ' + inputValue, baris, 2, false);
                    detailTable.fnUpdate(tombol, baris, 3, false);

                    swal("Success!", "Anda melakukan posting pada " + inputValue, "success");
                }
            })


        });
    }
    return {
        init: function () {
            addDetail();
        }
    };
}();


function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}

function join1000(somenum, usa) {
    var sep = usa ? ',' : '.';
    return parseFloat(String(somenum).split(sep).join('').replace(/,/g, '.'));
}

function todayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    return dd + '/' + mm + '/' + yyyy;
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransAddUangTitip.init();
    });
}