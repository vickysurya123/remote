﻿var DetailTransRentalRequest = function () {

    var arrCondition = [];
    var objCondition = {};

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                endDate: "Date.now()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var mdm = function () { 

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/TransRentalRequest/GetDataCustomer",
                    data: "{ MPLG_NAMA:'" + inp + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
                cekPiut("1D", ui.item.sap);
            }
        });
    }

    var cekPiut = function (doc_type, cust_code) {
        $.ajax({
            type: "POST",
            url: "/TransRentalRequest/cekPiutangSAP",
            data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var table = $('#tabel-perubahan_transrental');
    var oTable = table.dataTable({
        "lengthMenu": [
            [5, 15, 20, 50],
            [5, 15, 20, 50]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "columnDefs": [
            {
                "targets": [0, 2],
                "class": "dt-body-center",
                "visible": false
            },
            {
                "targets": [1],
                "class": "dt-body-center"
            },
            {
                "targets": [5, 6],
                "class": "dt-body-center"
            },
            {
                "targets": [8],
                "class": "dt-body-center",
                "width": "10%"
            },
        ]
    });

    $('#btn-detil-filter').click(function () {
        $('#table-filter-detail').DataTable().destroy();
        $('#table-filter-detail > tbody').html('');
        $('.sembunyi').hide();

    });


    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/TransRentalRequest/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        data.from = $('#from').val();
                        data.to = $('#to').val();
                        data.luas_tanah_from = $('#luas_tanah_from').val();
                        data.luas_tanah_to = $('#luas_tanah_to').val();
                        data.luas_bangunan_from = $('#luas_bangunan_from').val();
                        data.luas_bangunan_to = $('#luas_bangunan_to').val();
                        //console.log(data);
                    }

                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
        // Add event listener for opening and closing details

        var industry = function () {

            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDropDownIndustry",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                    }
                    $("#INDUSTRY").append(listItems);
                }
            });
        }

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();
            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);
            
            industry();
            $('#addDetailModal').modal('hide');
        });

        $('#editable_transrental').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });
        $('#editable_transrental').on('click', 'tr #btn-savecus', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data
            var xInd = $('#INDUSTRY').val();

            var param = {
                RO_NUMBER: data['RO_NUMBER'],
                OBJECT_ID: data['OBJECT_ID'],
                OBJECT_TYPE_ID: data['OBJECT_TYPE_ID'],
                OBJECT_TYPE: data['OBJECT_TYPE'],
                RO_NAME: data['RO_NAME'],
                LAND_DIMENSION: data['LUAS_BANGUNAN_RO'],
                BUILDING_DIMENSION: data['LUAS_BANGUNAN_RO'],
                INDUSTRY: xInd
            }
            //console.log(param);
            arrCondition.push(JSON.stringify(param));
            //console.log(arrCondition);
            $('#editable_transrental').dataTable().fnUpdate(xInd, baris, 7, false);
        });
    }

    var simpan = function () {
        $('#btn-update').click(function () {

            //Pengecekan save kecil
            var DTRental = $('#editable_transrental').dataTable();
            var all_row = DTRental.fnGetNodes();
            for (var i = 0; i < all_row.length; i++) {
                var cek = $(all_row[i]).find('select[name="INDUSTRY"]').val();
            }
            //console.log('cek ' + cek);

            if (cek) {
                swal('Warning', 'Please Choose Industry Type And Click Check Button Before You Save!', 'warning');
                ////console.log('warning');
            }
            else {
                ////console.log('jalankan insert');
                var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
                var BE_ID = $('#BE_ID').val();
                var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
                var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
                var CUSTOMER_ID = $('#CUSTOMER_ID').val();
                var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
                var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
                var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
                var CUSTOMER_AR = $('#CUSTOMER_AR').val();
                var OLD_CONTRACT = $('#OLD_CONTRACT').val();
                var arrObjects;
                if (RENTAL_REQUEST_TYPE && BE_ID && RENTAL_REQUEST_NAME && CUSTOMER_ID && CUSTOMER_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && CUSTOMER_AR && CONTRACT_USAGE) {
                    arrObjects = new Array();
                    var countDt = table.fnGetData();
                    //var grandtotal = 0;
                    for (var i = 0; i < countDt.length; i++) {
                        var item = table.fnGetData(i);
                        var param2 = {
                            RO_NUMBER: item[0],
                            OBJECT_ID: item[1],
                            OBJECT_TYPE_ID: item[2],
                            BUILDING_DIMENSION: item[5],
                            LAND_DIMENSION: item[6],
                            INDUSTRY: item[7]
                        };
                        arrObjects.push(param2);
                    }

                    var param = {
                        RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                        BE_ID: BE_ID,
                        RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                        CONTRACT_USAGE: CONTRACT_USAGE,
                        CUSTOMER_ID: CUSTOMER_ID,
                        CUSTOMER_NAME: CUSTOMER_NAME,
                        CONTRACT_START_DATE: CONTRACT_START_DATE,
                        CONTRACT_END_DATE: CONTRACT_END_DATE,
                        CUSTOMER_AR: CUSTOMER_AR,
                        OLD_CONTRACT: OLD_CONTRACT,
                        Objects: arrObjects
                    };

                    var cek = $("#CUSTOMER_AR").val();
                    cekPiut("1M", cek);
                    var xPiut = $("#cPiut").val();
                    //console.log(xPiut);
                    if (xPiut != "X") {
                        var document_number = "";
                        App.blockUI();
                        $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/TransRentalRequest/SaveHeader",
                            timeout: 30000,

                            success: function (data) {
                                var idHeader = data.trans_id;

                                if (data.status === 'S') {
                                    swal('Success', 'Rental Request Transaction Added Successfully with Transaction Number:' + idHeader, 'success').then(function (isConfirm) {
                                        window.location = "/TransRentalRequest";
                                    });
                                } else {
                                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                                        window.location = "/TransRentalRequest";
                                    });
                                }
                                arrObjects = [];
                                //for (var i = 0; i < count_dt.length; i++) {
                                //    var item = table.fnGetData(i);
                                //    var param2 = {
                                //        RENTAL_REQUEST_NO: id_header,
                                //        RO_NUMBER: item[0],
                                //        OBJECT_ID: item[1],
                                //        OBJECT_TYPE_ID: item[2],
                                //        BUILDING_DIMENSION: item[5],
                                //        LAND_DIMENSION: item[6],
                                //        INDUSTRY: item[7]
                                //    };

                                //    $.ajax({
                                //        contentType: "application/json",
                                //        data: JSON.stringify(param2),
                                //        method: "POST",
                                //        url: "/TransRentalRequest/SaveDetail",
                                //        timeout: 30000,
                                //        success: function (data) {
                                //            //console.log(data);
                                //            App.unblockUI();
                                //            if (data.status === 'S') {
                                //                swal('Success', 'Rental Request Transaction Added Successfully with Transaction Number:' + id_header, 'success').then(function (isConfirm) {
                                //                    window.location = "/TransRentalRequest";
                                //                 });
                                //             } else {
                                //                 swal('Failed', data.message, 'error').then(function (isConfirm) {
                                //                     window.location = "/TransRentalRequest";
                                //                 });
                                //             }
                                //        },
                                //        error: function (data) {
                                //            //console.log(data.responeText);
                                //        }

                                //    });
                                //}
                            },
                            error: function (data) {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransRentalRequest";
                                });
                                //console.log(data.responeText);
                            }
                        });
                    }

                } // END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM
                else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }

            }//end of cek save kecil         
        });
    }

    var update = function () {
        $('#btn-simpan').click(function () {
            var DTRental = $('#tabel-perubahan_transrental').dataTable();

            var ID = $('#ID').val();
            var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
            var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
            var BE_ID = $('#BE_ID').val();
            var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
            var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var CUSTOMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            var OLD_CONTRACT = $('#OLD_CONTRACT').val();
            var arrObjects;

            if (Date.parse(CONTRACT_START_DATE) > Date.parse(CONTRACT_END_DATE)) {
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                //Pengecekan save kecil
                var DTRental = $('#tabel-perubahan_transrental').dataTable();
                var all_row = DTRental.fnGetNodes();
                for (var i = 0; i < all_row.length; i++) {
                    var cek = $(all_row[i]).find('select[name="INDUSTRY"]').val();
                }
                //console.log('cek ' + cek);

                if (cek) {
                    swal('Warning', 'Please Choose Industry Type And Click Check Button Before You Save!', 'warning');
                    ////console.log('warning');
                }
                else {
                    // Jalankan Update
                    if (RENTAL_REQUEST_TYPE && BE_ID && RENTAL_REQUEST_NAME && CONTRACT_USAGE && CUSTOMER_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && CUSTOMER_ID && CUSTOMER_AR) {
                        App.blockUI({ boxed: true });
                        arrObjects = new Array();

                        var countDt = table.fnGetData();
                        //var grandtotal = 0;
                        for (var i = 0; i < countDt.length; i++) {
                            var item = table.fnGetData(i);
                            var param2 = {
                                RENTAL_REQUEST_NO: RENTAL_REQUEST_NO,
                                RO_NUMBER: item[0],
                                OBJECT_ID: item[1],
                                OBJECT_TYPE_ID: item[2],
                                OBJECT_TYPE: item[3],
                                RO_NAME: item[4],
                                LAND_DIMENSION: item[5],
                                BUILDING_DIMENSION: item[6],
                                INDUSTRY: item[7]
                            };
                            arrObjects.push(param2);
                            console.log(arrObjects);

                            //var req_detail = $.ajax({
                            //    contentType: "application/json",
                            //    data: JSON.stringify(param2),
                            //    method: "POST",
                            //    url: "/TransRentalRequest/EditDataDetail",
                            //});
                        }

                        var param = {
                            ID: ID,
                            RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                            BE_ID: BE_ID,
                            RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                            CONTRACT_USAGE: CONTRACT_USAGE,
                            CUSTOMER_NAME: CUSTOMER_NAME,
                            CONTRACT_START_DATE: CONTRACT_START_DATE,
                            CONTRACT_END_DATE: CONTRACT_END_DATE,
                            CUSTOMER_ID: CUSTOMER_ID,
                            CUSTOMER_AR: CUSTOMER_AR,
                            OLD_CONTRACT: OLD_CONTRACT,
                            RENTAL_REQUEST_NO: RENTAL_REQUEST_NO,
                            Objects: arrObjects                         
                        };

                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/TransRentalRequest/EditData",
                            timeout: 30000
                        });


                        req.done(function (data) {
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location.href = '/TransRentalRequest';
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location.href = '/TransRentalRequest';
                                });
                            }
                            //arrObjects = [];

                        });
                    } else {
                        swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    }
                }
                
            }


        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransRentalRequest";
        });
    }

    var ambil_tanggal = function () {
        $('#btn-detil').click(function () {
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();

            $('#from').val(start);
            $('#to').val(end);
        });
    }

    var editTable = function () {
        var tableEdit = $('#tabel-perubahan_transrental').DataTable();
        tableEdit.destroy();

        // Edit Data Table
        var tabler = $('#tabel-perubahan_transrental');
        var oTable = tabler.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 2],
                    "class": "dt-body-center",
                    "visible": false
                },
                {
                    "targets": [1],
                    "class": "dt-body-center"
                },
                {
                    "targets": [5, 6],
                    "class": "dt-body-center"
                },
                {
                    "targets": [8],
                    "class": "dt-body-center",
                    "width": "10%"
                },
            ]
        });

        var id_rental = $('#RENTAL_REQUEST_NO').val();

        $.ajax({
            type: "GET",
            "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rental,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data;
                arrCondition = new Array();

                for (var i = 0; i < jsonList.data.length; i++) {
                    //oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);                 
                    oTable.fnAddData([jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);

                    var param = {
                        RO_NUMBER: jsonList.data[i].RO_NUMBER,
                        OBJECT_ID: jsonList.data[i].OBJECT_ID,
                        OBJECT_TYPE_ID: jsonList.data[i].OBJECT_TYPE_ID,
                        OBJECT_TYPE: jsonList.data[i].OBJECT_TYPE,
                        RO_NAME: jsonList.data[i].RO_NAME,
                        LAND_DIMENSION: jsonList.data[i].LAND_DIMENSION,
                        BUILDING_DIMENSION: jsonList.data[i].BUILDING_DIMENSION,
                        INDUSTRY: jsonList.data[i].INDUSTRY
                    }
                    arrCondition.push(JSON.stringify(param));
                    //arrCondition.push(jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY);
                }
                ////console.log('arr condition ' +arrCondition);
            }
        });

        // BTN EDITCUS
        $('#tabel-perubahan_transrental').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-perubahan_transrental').dataTable();
            
            var nRow = $(this).parents('tr')[0];
            var editIndustry = '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>';
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';
            table.fnUpdate(editIndustry, nRow, 7, false);
            table.fnUpdate(tombol, nRow, 8, false);


            industry();

            // $("#INDUSTRY option[value='" + valPriceCode + "']").prop('selected', true);
            $("#INDUSTRY option[value='02 - Usaha Industri']").prop('selected', true);
        });

   
        // BTN SAVECUS
        $('#tabel-perubahan_transrental').on('click', 'tr #btn-savecus', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-perubahan_transrental').DataTable();
            var data = table.row(baris).data();
            var xInd = $('#INDUSTRY').val();

            var param = {
                RO_NUMBER: data[0],
                OBJECT_ID: data[1],
                OBJECT_TYPE_ID: data[2],
                OBJECT_TYPE: data[3],
                RO_NAME: data[4],
                LAND_DIMENSION: data[5],
                BUILDING_DIMENSION: data[6],
                INDUSTRY: xInd
            }
            //console.log(param);
            arrCondition.push(JSON.stringify(param));
            //console.log(arrCondition);

            $('#tabel-perubahan_transrental').dataTable().fnUpdate(xInd, baris, 7, false);
            var tombol = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            $('#tabel-perubahan_transrental').dataTable().fnUpdate(tombol, baris, 8, false);

        });

        // BTN DELCUS
        $('#tabel-perubahan_transrental').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-perubahan_transrental').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        // restoreRow(oTable, nEditing);
        // BTN CANCELCUS
         
        $('#tabel-perubahan_transrental').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();
            
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-perubahan_transrental').DataTable();
            var data = table.row(baris).data();

            var objectID = data[1];

            // Hapus Row 
            oTable.fnDeleteRow(baris);

            var json = JSON.parse('['+arrCondition+']');

            var RO_NUMBER = "";
            var OBJECT_ID = "";
            var OBJECT_TYPE_ID = "";
            var OBJECT_TYPE = "";
            var RO_NAME = "";
            var LAND_DIMENSION = "";
            var BUILDING_DIMENSION = "";
            var INDUSTRY = "";

            json.forEach(function (object) {
                if (object.OBJECT_ID == objectID) {
                    RO_NUMBER = object.RO_NUMBER;
                    OBJECT_ID = object.OBJECT_ID;
                    OBJECT_TYPE_ID = object.OBJECT_TYPE_ID;
                    OBJECT_TYPE = object.OBJECT_TYPE;
                    RO_NAME = object.RO_NAME;
                    LAND_DIMENSION = object.LAND_DIMENSION;
                    BUILDING_DIMENSION = object.BUILDING_DIMENSION;
                    INDUSTRY = object.INDUSTRY;
                }               
            });

            oTable.fnAddData([RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, OBJECT_TYPE, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);

            ////console.log(arrCondition);

            ////console.log(data[1]);
        });
    }

    // COMBOBOX INDUSTRY
    var industry = function () {
        $.ajax({
            type: "GET",
            url: "/TransRentalRequest/GetDataDropDownIndustry",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                }
                $("#INDUSTRY").append(listItems);
            }
        });
    }

    return {
        init: function () {
            //add();
            simpan();
            datePicker();
            mdm();
            filter();
            cekPiut();
            batal();
            ambil_tanggal();
            update();
            editTable();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        DetailTransRentalRequest.init();
    });
}