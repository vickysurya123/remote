﻿var DataSurat = function () {
    var DataEmployee = function () {
        $("#TTD").select2({
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/DisposisiList/GetDataEmployee",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });

        $("#PARAF").select2({
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/DisposisiList/GetDataEmployee",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        $("#TEMBUSAN").select2({
            allowClear: true,
            width: '100%',
            multiple: true,
            ajax: {
                url: "/TransContractOffer/GetDataUserEmail",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.EMAIL,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        $("#KEPADA").select2({
            allowClear: true,
            width: '100%',
            multiple: true,
            ajax: {
                url: "/TransContractOffer/GetDataUserEmail",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.EMAIL,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        $('#KEPADA option:selected').each(function () {
            alert($(this).val());
        });
        $("#STATUS_KIRIM").select2({});
    };

    var saveSurat = function () {
        $('#btn-save-surat').click(function () {
            var ATASNAMA = $('#ATASNAMA').val();
            //var MEMO = $('#MEMO').val();
            //var PEMARAF = $('#PEMARAF').val();
            var textareaValue = $('#ISI_SURAT').summernote('code');
            var kepada = '';
            $("#KEPADA").val().forEach(function (e, i) {
                kepada += (i > 0 ? ';' : '') + e;
            });
            var tembusan = '';
            $("#TEMBUSAN").val().forEach(function (e, i) {
                tembusan += (i > 0 ? ';' : '') + e;
            });
            var paraf = '';
            $("#PARAF").val().forEach(function (e, i) {
                paraf += (i > 0 ? ';' : '') + e;
            });
            if (ATASNAMA !== "") {
                App.blockUI({ boxed: true });
                var form = $("#contract-surat")[0];
                var data = new FormData(form);
                data.append('ISI_SURAT', textareaValue);
                data.append('KEPADA', kepada);
                data.append('TEMBUSAN', tembusan);
                data.append('PARAF', paraf);
                var req = $.ajax({
                    type: 'POST',
                    url: '/TransContractOffer/InsertSurat',
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: data,

                    success: function (message) {
                        alert(message);
                    },
                    error: function () {
                        alert("there was error uploading files!");
                    }
                });

                // var req = $.ajax({
                // dataType: "json",
                // contentType: "application/json",
                // data: JSON.stringify(param),
                // method: "POST",
                // url: "/TransContractOffer/InsertSurat",
                // timeout: 30000
                // });

                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            window.location.href = '/TransContractOffer';
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }

        });
    };

    function uploadImage(file) {
        var formData = new FormData();
        formData.append("files", file);
        $.ajax({
            data: formData,
            type: "POST",
            url: '/TransContractOffer/UploadFile',
            cache: false,
            contentType: false,
            processData: false,
            success: function (FileUrl) {
                $('#ISI_SURAT').summernote("insertImage", FileUrl, 'filename');
            },
            error: function (data) {
                //alert(data.responseText);
            }
        });
    }

    var summernote = function () {
        $('#ISI_SURAT').summernote({
            placeholder: '',
            tabsize: 2,
            height: 120,
            focus: true,                  // set focus to editable area after initializing summernote  
            callbacks: {
                onImageUpload: function (files) {
                    for (let i = 0; i < files.length; i++) {
                        uploadImage(files[i]);
                    }
                }
            },
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['codeview']]
            ]
        });
    }
    return {
        init: function () {
            DataEmployee();
            saveSurat();
            summernote();
        }
    };
}();

var TableDatatablesEditable = function () {
    //-------------------------------------------------BEGIN JS dt-add-trans-co.js-----------------------------------------------------
    //Declare variabel array untuk menampung condition type agar bisa digunakan di form memo
    var arrCondition = [];
    var arrObjectEdit = [];


    var arrCondType = [];
    var buah = [];
    var secondCellArray = [];
    var arrManualFrequency = [];
    var arrMemo = [];

    var objTampungSR = {};
    var arrTampungSR = [];

    var resetDatePicker = function () {
        //console.log("reset")

        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });

        $('.date-picker').each(function () {
            var date = $(this).val();
            if (date) {
                var datearray = date.split("/");
                var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                }).datepicker('setDate', new Date(newdate)); //set value
            } else {
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                });
            }
        });
    }
    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransContractOffer";
        });
    }

    var hitung_bulan = function () {

        //FUNCTION Hitung Bulan
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();

            if (d2.getDate() >= d1.getDate())
                months++

            return months <= 0 ? 0 : months;
        }

        $('#CONTRACT_END_DATE').change(function () {
            var mulai = $('#CONTRACT_START_DATE').val();
            var akhir = $('#CONTRACT_END_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

            var dateAkhir = akhir;
            var datearrayAkhir = dateAkhir.split("/");
            var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

            //console.log(newdateMulai);
            //console.log(newdateAkhir);
            var tim = monthDiff(new Date(newdateMulai), new Date(newdateAkhir));
            //console.log(tim);
            $('#TERM_IN_MONTHS').val(tim);
            coHitungPeriod();
        });

    }

    var hitung_tanggal_sampai = function () {
        function addMonths(date, count) {
            if (date && count) {
                var m, d = (date = new Date(+date)).getDate()

                date.setMonth(date.getMonth() + count, 1)
                m = date.getMonth()
                date.setDate(d)
                if (date.getMonth() !== m) date.setDate(0)
            }
            return date
        }

        $('#TERM_IN_MONTHS').change(function () {
            var tim = $('#TERM_IN_MONTHS').val();
            var mulai = $('#CONTRACT_START_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

            //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
            var f_date = new Date(newdateMulai);
            var n_tim = Number(tim);
            var end_date = addMonths(f_date, n_tim);
            var xdate = new Date(end_date);
            var f_date = xdate.toISOString().slice(0, 10);

            //format f_date menjadi dd/mm/YYYY
            var dateAkhir = f_date;
            var datearrayAkhir = dateAkhir.split("-");
            var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];
            $('#CONTRACT_END_DATE').val(newdateAkhir);
            coHitungPeriod();
            //console.log(newdateAkhir);
        });
    }

    var setProfitCenter = function (vdata) {
        var BEID = $('#BUSINESS_ENTITY_ID').val();
        $('#TERMINAL_NAME').val(vdata.data.TERMINAL_NAME);
        $('#PROFIT_CENTER').val(vdata.data.PROFIT_CENTER_ID);
        if (BEID !== "1062") {
            $('#BUSINESS_ENTITY_ID').val(vdata.data.BRANCH_ID);
            $('#PROFIT_CENTER').val(vdata.data.PROFIT_CENTER_ID);
        } else {
            $('#PROFIT_CENTER').val(12303);
        }
        $('#BUSINESS_ENTITY_ID').val(vdata.data.BRANCH_ID);
        $('#BUSINESS_ENTITY_NAME').val(vdata.data.NAMA_PROFIT_CENTER);
        //var jsonList = data
        //var listItems = "";
        //for (var i = 0; i < jsonList.data.length; i++) {
        //    listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
        //}
        //$("#INDUSTRY").append(listItems);
    }

    var filter = function () {

        $('#btn-detil-filter').click(function () {
            $('#table-filter-detail').DataTable().destroy();
            $('#table-filter-detail > tbody').html('');
            $('.sembunyi').hide();

        });

        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/TransRentalRequest/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.from = $('#from').val();
                        data.to = $('#to').val();
                        data.luas_tanah_from = $('#luas_tanah_from').val();
                        data.luas_tanah_to = $('#luas_tanah_to').val();
                        data.luas_bangunan_from = $('#luas_bangunan_from').val();
                        data.luas_bangunan_to = $('#luas_bangunan_to').val();
                        //console.log(data);
                    }
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });


        var tablet = $('#rental-object-co');
        var xoTable = tablet.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 2],
                    "class": "dt-body-center",
                    "visible": false
                },
                {
                    "targets": [1],
                    "class": "dt-body-center"
                },
                {
                    "targets": [5, 6],
                    "class": "dt-body-center"
                },
                {
                    "targets": [8],
                    "class": "dt-body-center",
                    "width": "10%"
                },
            ]
        });


        // Add event listener for opening and closing details

        var industry = function () {

            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDropDownIndustry",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                    }
                    $("#INDUSTRY").append(listItems);
                }
            });
        }

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();
            //console.log(data)
            //var xInd = $('#INDUSTRY').val();
            xoTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_TANAH_RO'], data['LUAS_BANGUNAN_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus-ob"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);
            industry();
            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataProfitCenter?id=" + data['OBJECT_ID'],
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    setProfitCenter(data)
                }
            });
            $('#addDetailModal').modal('hide');
        });

        $('#rental-object-co').on('click', 'tr #btn-delcus-ob', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();
            //console.log(data);
            $('#condition-option [value="' + 'RO ' + data[1] + '|' + data[0] + '"]').remove();
            xoTable.fnDeleteRow(baris);
        });

        $('#rental-object-co').on('click', 'tr #btn-savecus-ob', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();
            //console.log(data);
            var listItems = "<option value='" + 'RO ' + data[1] + '|' + data[0] + "'>" + 'RO ' + data[1] + ' - ' + data[4] + "</option>"
            $("#condition-option").append(listItems);


            var xInd = $(baris).find('#INDUSTRY').val();
            $('#rental-object-co').dataTable().fnUpdate(xInd, baris, 7, false);
        });


        $('#rental-object-co').on('click', 'tr #btn-editcus-ob', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').dataTable();

            var nRow = $(this).parents('tr')[0];
            var editIndustry = '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>';
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus-ob"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus-ob"><i class="fa fa-check"></i></a>';
            table.fnUpdate(editIndustry, nRow, 7, false);
            table.fnUpdate(tombol, nRow, 8, false);


            industry();

            $(baris).find("#INDUSTRY option[value='" + valPriceCode + "']").prop('selected', true);
            //$("#INDUSTRY option[value='02 - Usaha Industri']").prop('selected', true);
        });


        $('#rental-object-co').on('click', 'tr #btn-cancelcus-ob', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();

            var objectID = data[1];

            // Hapus Row 
            xoTable.fnDeleteRow(baris);

            var json = JSON.parse('[' + arrObjectEdit + ']');

            var RO_NUMBER = "";
            var OBJECT_ID = "";
            var OBJECT_TYPE_ID = "";
            var OBJECT_TYPE = "";
            var RO_NAME = "";
            var LAND_DIMENSION = "";
            var BUILDING_DIMENSION = "";
            var INDUSTRY = "";

            json.forEach(function (object) {
                if (object.OBJECT_ID == objectID) {
                    RO_NUMBER = object.RO_NUMBER;
                    OBJECT_ID = object.OBJECT_ID;
                    OBJECT_TYPE_ID = object.OBJECT_TYPE_ID;
                    OBJECT_TYPE = object.OBJECT_TYPE;
                    RO_NAME = object.RO_NAME;
                    LAND_DIMENSION = object.LAND_DIMENSION;
                    BUILDING_DIMENSION = object.BUILDING_DIMENSION;
                    INDUSTRY = object.INDUSTRY;
                }
            });

            xoTable.fnAddData([RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, OBJECT_TYPE, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus-ob"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);

            ////console.log(arrCondition);

            ////console.log(data[1]);
        });
    }

    var filterRO = function () {

        $('#btn-filter-data-ro').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail-ro')) {
                $('#table-filter-detail-ro').DataTable().destroy();
            }

            $('#table-filter-detail-ro').DataTable({
                "ajax": {
                    "url": "GetDataFilterRO",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-ro"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    var addCondition = function () {

        var tablex = $('#condition');
        var oTable = tablex.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "paging": false,
            "pageLength": 100000,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "columnDefs": [
                {
                    "targets": [20],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [20],
                    "visible": false
                }
            ]
        });

        $('#btn-add-condition').on('click', function () {

            $('#condition-option [value=""]').html(" -- Choose Condition Option -- ");
            $('#condition-option [value="Header Document"]').html("Header Document");

            // Ajax untuk ambil dan generate checkbox condition type dari parameter ref d
            $('#condition-option').change(function () {
                var condition_option = $('#condition-option').val();

                $('#cekbokList').html('');
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetCheckboxConditionType",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            var valueCombobox = jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2;

                            if (condition_option == 'Header Document') {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }
                            else {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox'  + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }

                        }
                        $("#cekbokList").append(listItems);
                    }
                });
            });

        });

        // condition add
        $('#btn-condition').on('click', function () {
            // $('#condition').dataTable();

            //raw data value calc object dan di split |
            var id_combo = $('#condition-option').val();
            var splitCalcObject = id_combo.split("|");
            var calcObject = splitCalcObject[0];
            var id_ro = splitCalcObject[1];

            if (id_ro) {
                // Ajax untuk get meas type berdasarkan id_ro yang didapatkan dari split
                $('#meas-type').html('');
                // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih

                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataMeasType/" + id_ro,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        listItems += '<option value="">--Choose--</option>';
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + jsonList.data[i].MEAS_TYPE + '|' + jsonList.data[i].ID_DETAIL + "'>" + jsonList.data[i].MEAS_TYPE + ' - ' + jsonList.data[i].MEAS_DESC + "</option>";
                        }
                        // $("#meas-type").append(listItems);
                        $('select[name="meas-type"]').html(listItems);
                    }
                });
            }

            // Matikan form untuk kondisi Header Document
            if (id_ro) {
                var measType = '<select class="form-control" id="meas-type" name="meas-type" onchange="getLuas(this)"></select>';
            }
            else {
                measType = '';
            }
            if (id_ro) {
                var luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }
            else {
                luas = '';
            }
            if (calcObject == 'Header Document') {
                var buttonPrice = '';
                var buttonCoa = '';
            }
            else {
                buttonPrice = '<a class="btn default btn-xs blue input-group-addon" id="btn-price" ><i class="fa fa-plus"></i></a>';
                buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';
            }

            //Cek selected value dari combobox calculation apakah header document atau RO
            var vCBCalculation = $("#condition-option").val();

            if (vCBCalculation == 'Header Document') {
                measType = '<input type="text" class="form-control" id="meas-type" name="meas-type" readonly>';
                luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }


            var tableDT = $('#condition  > tbody');
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();
            var tim = $('#TERM_IN_MONTHS').val();

            //date Now
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;
            //console.log(today);

            var startD = '<center><input type="text" class="form-control date-picker" name="start-date" value="' + start + '" id="start-date" onfocusout="hitungTermMonths(this)" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var endD = '<center><input type="text" class="form-control date-picker" name="end-date" value="' + end + '" id="end-date" onfocusout="hitungTermMonths(this)" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onfocusout="hitungEndDate(this)" /></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center class="input-group">' + buttonPrice + '<input type="text" class="form-control number" id="unit-price" name="unit-price" onchange="isiTotalUnitPrice(this);hitungFormula(this);"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" onchange="infoIsiManual()" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control date-picker" name="start-due-date" value="' + today + '"  id="start-due-date" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var manualNo = '<center><input type="text" class="form-control" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';
            //var measType = '<select class="form-control" id="meas-type"></select>';
            //var luas = '<input type="text" class="form-control" id="luas">';
            var total = '<center><input type="text" class="form-control number" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control decimal" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control ipercentage" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';
            //var salesRule = '<center><input type="text" class="form-control" id="sales-rule"></center>';
            var totalNetValue = '<center><input type="text" class="form-control number" id="total-net-value" name="total-net-value" readonly></center>';
            //var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
            var coa_prod = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_prod" name="coa_prod" readonly></center>';
            var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_produksi" id="coa_prod"><option value="-">-</option></select></center>';

            //Format luas dengan separator . karena
            //var fLuas = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            $('input[name="test"]:checked').each(function () {
                //raw data condition dan di split |
                var raw_cond = $(this).val();
                var split_cond = raw_cond.split("|");
                var res_condition = split_cond[0];
                var tax_code = split_cond[1];

                //console.log("RES CONDITION >> " + res_condition);
                if (res_condition == 'Z009 Administrasi' || res_condition == 'Z010 Warmeking') {
                    oTable.fnAddData([calcObject,
                        res_condition,
                        startD,
                        endD,
                        termMonths,
                        cekbokStatistic,
                        formula,
                        unitPrice,
                        amtRef,
                        frequency,
                        startDueDate,
                        manualNo,
                        measType,
                        luas,
                        total,
                        persenFromNJOP,
                        persenFromKondisiTeknis,
                        totalNetValue,
                        coa_produksi_disabled,
                        '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                        tax_code], true);
                }
                else {
                    oTable.fnAddData([calcObject,
                        res_condition,
                        startD,
                        endD,
                        termMonths,
                        cekbokStatistic,
                        formula,
                        unitPrice,
                        amtRef,
                        frequency,
                        startDueDate,
                        manualNo,
                        measType,
                        luas,
                        total,
                        persenFromNJOP,
                        persenFromKondisiTeknis,
                        totalNetValue,
                        coa_prod,
                        '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                        tax_code], true);
                }



                var arrCond = buah.push(calcObject + ' | ' + res_condition);
                //console.log(arrCond);

                var fruits = buah;

                //Masking Start Due Date
                $('input[name="start-due-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="start-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="end-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                // set datepicker condition add 
                resetDatePicker();
                $('#addCondition').modal('hide');
            });

            //$('#unit-price').mask('000.000.000.000.000', { reverse: true });
            //$('#unit-price').on('change', function () {
            //    var unitPrice = $('#unit-price').val();
            //    var mask = sep1000(unitPrice, true);
            //    $('#unit-price').val(mask);
            //});

            $('.decimal').inputmask('decimal', {
                'digits': 2,
                'digitsOptional': false,
                radixPoint: ".",
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.ipercentage').inputmask('numeric', {
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.number').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                autoGroup: true,
                rightAlign: true,
                oncleared: function () { self.Value('0'); }
            });

            getCoaProduksi();
        });

        // condition edit
        $('#condition').on('click', 'tr #btn-editcus', function () {
            //$('#condition > tbody').html('');
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#condition').dataTable();

            // Value untuk Data Table Form
            var condtype = data[1];
            var start = $('#CONTRACT_START_DATE').val(); // data[2];
            var end = $('#CONTRACT_END_DATE').val(); // data[3];
            var tim = $('#TERM_IN_MONTHS').val(); //data[4];
            var stat = data[5];
            var vFormula = data[6];
            var uPrice = data[7];
            var aRef = data[8];
            var freq = data[9];
            var dueDate = data[10];
            var manual = data[11];
            var vMeasType = data[12];
            var vLuas = data[13];
            var vTotalValue = data[14];
            var vNJOP = data[15];
            var vKondisiTeknis = data[16];
            var vNetValue = data[17];
            var coa_prod = data[18];


            //console.log(uPrice);
            //console.log(vTotalValue, vNetValue);

            var sDate = '';
            var sDueDate = '';

            if (dueDate == null) {
                sDueDate = "";
            }
            else {
                sDueDate = dueDate;
            }

            var sStat = '';
            if (stat == 0) {
                sStat = 'No';
            }
            else {
                sStat = 'Yes';
            }

            var mNO = '';
            if (manual == null) {
                mNO = "";
            }
            else {
                mNO = manual;
            }

            var sNJOP = '';
            if (vNJOP == null) {
                sNJOP = '';
            }
            else {
                sNJOP = vNJOP;
            }

            var sMeasType = '';
            if (vMeasType == null) {
                sMeasType = '';
            }
            else {
                sMeasType = vMeasType;
            }

            var sLuas = '';
            if (vLuas == null) {
                sLuas = '';
            }
            else {
                sLuas = vLuas;
            }

            var sKondisiTeknis = '';
            if (vKondisiTeknis == null) {
                sKondisiTeknis = '';
            }
            else {
                sKondisiTeknis = vKondisiTeknis;
            }


            if (condtype == 'Z009 Administrasi' || condtype == 'Z010 Warmeking') {
                var buttonPrice = '';
                var buttonCoa = '';
            }
            else {
                buttonPrice = '<a class="btn default btn-xs blue input-group-addon" id="btn-price" ><i class="fa fa-plus"></i></a>';
                buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';
            }
            //console.log("coa prod >>" + data[18]);
            //console.log("condtype >>" + condtype);



            // Data Table Form
            var startD = '<center><input type="text" class="form-control date-picker" name="start-date" value="' + start + '" id="start-date" onfocusout="hitungTermMonths(this)" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var endD = '<center><input type="text" class="form-control date-picker" name="end-date" value="' + end + '" id="end-date" onfocusout="hitungTermMonths(this)" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onfocusout="hitungEndDate(this)" /></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="' + stat + '" id="statistic" name="statistic" ' + (stat === 1 ? 'checked' : '') + ' /></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center class="input-group">' + buttonPrice + '<input type="text" class="form-control number" id="unit-price" value="' + uPrice + '" name="unit-price" onchange="isiTotalUnitPrice(this);hitungFormula(this);"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" onchange="infoIsiManual()" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control date-picker" value="' + sDueDate + '" name="start-due-date" id="start-due-date" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" /></center>';
            var manualNo = '<center><input type="text" class="form-control" value="' + mNO + '" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';

            var total = '<center><input type="text" class="form-control number" value="' + vTotalValue + '" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control decimal" value="' + sNJOP + '" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control ipercentage" value="' + sKondisiTeknis + '" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';

            var totalNetValue = '<center><input type="text" class="form-control number" value="' + vNetValue + '" id="total-net-value" name="total-net-value" readonly></center>';
            //var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
            var coa_prod = '<center class="input-group">' + buttonCoa + '<input type="text" value="' + coa_prod + '" class="form-control" id="coa_prod" name="coa_prod" readonly></center>';
            var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_prod" id="coa_prod"><option value="-" selected="selected">-</option></select></center>';
            measType = '<input type="text" class="form-control" value="' + sMeasType + '" id="meas-type" name="meas-type" readonly>';
            luas = '<input type="text" class="form-control" id="luas" value="' + sLuas + '" name="luas" readonly>';

            openTable.fnUpdate(startD, nRow, 2, false);
            openTable.fnUpdate(endD, nRow, 3, false);
            openTable.fnUpdate(termMonths, nRow, 4, false);
            openTable.fnUpdate(cekbokStatistic, nRow, 5, false);
            openTable.fnUpdate(formula, nRow, 6, false);
            openTable.fnUpdate(unitPrice, nRow, 7, false);
            openTable.fnUpdate(amtRef, nRow, 8, false);
            openTable.fnUpdate(frequency, nRow, 9, false);
            openTable.fnUpdate(startDueDate, nRow, 10, false);
            openTable.fnUpdate(manualNo, nRow, 11, false);
            openTable.fnUpdate(measType, nRow, 12, false);
            openTable.fnUpdate(luas, nRow, 13, false);
            openTable.fnUpdate(total, nRow, 14, false);
            openTable.fnUpdate(persenFromNJOP, nRow, 15, false);
            openTable.fnUpdate(persenFromKondisiTeknis, nRow, 16, false);
            openTable.fnUpdate(totalNetValue, nRow, 17, false);

            if (condtype == 'Z009 Administrasi' || condtype == 'Z010 Warmeking') {
                openTable.fnUpdate(coa_produksi_disabled, nRow, 18, false);
            } else {
                openTable.fnUpdate(coa_prod, nRow, 18, false);
            }


            openTable.fnUpdate(tombol, nRow, 19, false);

            $(baris).find("#amt-ref option[value='" + aRef + "']").prop('selected', true);
            $(baris).find("#frequency option[value='" + freq + "']").prop('selected', true);
            $(baris).find("#formula option[value='" + vFormula + "']").prop('selected', true);

            $('.decimal').inputmask('decimal', {
                'digits': 2,
                'digitsOptional': false,
                radixPoint: ".",
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.ipercentage').inputmask('numeric', {
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.number').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                autoGroup: true,
                rightAlign: true,
                oncleared: function () { self.Value('0'); }
            });

            // set datepicker condition edit 
            resetDatePicker();
            getCoaProduksi2(data[18], baris);
        });

        // save condition
        $('#condition').on('click', 'tr #btn-savecus', function () {
            //$(this).hide();

            //hitungFormula(this);
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var start_date = $(baris).find('#start-date').val();
            var end_date = $(baris).find('#end-date').val();
            var term_months = $(baris).find('#term-months').val();

            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();

            var tgl_kontrak = CONTRACT_START_DATE.replace(/\./g, '/')
            var datearray = tgl_kontrak.split("/");
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            var tgl_start_kontrak = new Date(newdate); //set value

            var tgl_condition = start_date.replace(/\./g, '/')
            var datearray_con = tgl_condition.split("/");
            var newdate_con = datearray_con[1] + '/' + datearray_con[0] + '/' + datearray_con[2];
            var tgl_start_condition = new Date(newdate_con); //set value

            var tgl_kontrak2 = CONTRACT_END_DATE.replace(/\./g, '/')
            var datearray_en = tgl_kontrak2.split("/");
            var newdate_kon_end = datearray_en[1] + '/' + datearray_en[0] + '/' + datearray_en[2];
            var tgl_end_kontrak = new Date(newdate_kon_end); //set value end

            var tgl_condition2 = end_date.replace(/\./g, '/')
            var datearray_con_end = tgl_condition2.split("/");
            var newdate_con_end = datearray_con_end[1] + '/' + datearray_con_end[0] + '/' + datearray_con_end[2];
            var tgl_end_condition = new Date(newdate_con_end); //set value end

            var unit_price = $(baris).find('#unit-price').val();
            var amt_ref = $(baris).find('#amt-ref').val();
            var frequency = $(baris).find('#frequency').val();
            var start_due_date = $(baris).find('#start-due-date').val();
            var manual_no = $(baris).find('#manual-no').val();
            var formula = $(baris).find('#formula').val();


            var meas_type = $(baris).find('#meas-type').val();
            var splitselectedMeasType = meas_type.split("|");
            var id_detail_meas_type = splitselectedMeasType[0];

            var luas = $(baris).find('#luas').val();
            var total = $(baris).find('#total').val();
            var persen_from_njop = $(baris).find('#persen-from-njop').val();
            var persen_from_kondisi_teknis = $(baris).find('#persen-from-kondisi-teknis').val();
            var coa_prod = $(baris).find('#coa_prod').val();
            //var coa_prod = $('#coa_prod').val();
            //console.log("coa  prod >> " + coa_prod);

            //var sales_rule = $('#sales-rule').val();
            var total_net_value = $(baris).find('#total-net-value').val();

            //Set Value Untuk Statistic
            var cek = $(baris).find('input[id=statistic]').is(":checked");
            var vStatistic = "";
            if (cek) {
                vStatistic = "Yes";
            }
            else {
                vStatistic = "No";
            }

            // Cek apakah manual no kosong atau tidak
            var cekManualNo = $(baris).find('input[id=manual-no]').val();

            //console.log(cekManualNo);

            var coa_prod = $(baris).find('#coa_prod').val();
            //console.log(coa_produksi);

            var condition_type = data[1];
            //console.log("CONDITON TYPE >> " + condition_type);
            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            if (cekManualNo == '') {
                swal('Info', 'Mohon isi Manual No', 'info');
            } else if (coa_prod == "") {
                swal('Info', 'Mohon Pilih COA Produksi', 'info');
            } else if (start_due_date == '') {
                swal('Info', 'Mohon isi Start Due Date', 'info');
            }
            //else if (tgl_start_condition < tgl_start_kontrak) {
            //    swal('Info', 'Tidak boleh kurang dari Contract Start Date', 'info');
            //} else if (tgl_end_condition > tgl_end_kontrak) {
            //    swal('Info', 'Tidak boleh lebih dari Contract End Date', 'info');
            //}
            else {
                // swal('Info', 'Harap Mengisi Data di Tabel Manual Frequency! ', 'info');
                $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
                $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
                $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
                $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
                $('#condition').dataTable().fnUpdate(formula, baris, 6, false);
                $('#condition').dataTable().fnUpdate(unit_price, baris, 7, false);
                $('#condition').dataTable().fnUpdate(amt_ref, baris, 8, false);
                $('#condition').dataTable().fnUpdate(frequency, baris, 9, false);
                $('#condition').dataTable().fnUpdate(start_due_date, baris, 10, false);
                $('#condition').dataTable().fnUpdate(manual_no, baris, 11, false);
                $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
                $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
                $('#condition').dataTable().fnUpdate(total, baris, 14, false);
                $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
                $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
                //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
                $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
                $('#condition').dataTable().fnUpdate(coa_prod, baris, 18, false);
                var tombolC = '<center>'; //<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                if ($('#btn-cancelcus').length) {
                    tombolC += '<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a>';
                }
                if ($('#btn-savecus').length) {
                    tombolC += '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
                }
                tombolC += '</center>';
                $('#condition').dataTable().fnUpdate(tombolC, baris, 19, false);

                //return false;
            }

            setTimeout(coHitungValue(), 1000);
        });

        // condition delete
        $('#condition').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            // ambil data by kolom
            var rData = table.row($(this).parents('tr')).data();
            var calc_object = rData[0];
            var condition_type = rData[1];
            var dataRemove = calc_object + ' | ' + condition_type;
            // panggil function untuk remove array buah dan secondCellArray:)
            removeA(buah, dataRemove);
            oTable.fnDeleteRow(baris);

            setTimeout(coHitungValue(), 1000);
        });

        // condition cancel
        $('#condition').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var calcObject = data[0];
            var conditionType = data[1];

            // Hapus Row 
            oTable.fnDeleteRow(baris);

            var json = JSON.parse('[' + arrCondition + ']');


            // Deklarasi Variabel       
            var CALC_OBJECT = "";
            var CONDITION_TYPE = "";
            var VALID_FROM = "";
            var VALID_TO = "";
            var MONTHS = "";
            var STATISTIC = "";
            var UNIT_PRICE = "";
            var AMT_REF = "";
            var FREQUENCY = "";
            var START_DUE_DATE = "";
            var MANUAL_NO = "";
            var FORMULA = "";;
            var MEASUREMENT_TYPE = "";
            var LUAS = "";
            var TOTAL = "";
            var NJOP_PERCENT = "";
            var KONDISI_TEKNIS_PERCENT = "";
            var TOTAL_NET_VALUE = "";
            var COA_PROD = "";
            var TAX_CODE = "";

            json.forEach(function (object) {
                if (object.CALC_OBJECT == calcObject && object.CONDITION_TYPE == conditionType) {
                    CALC_OBJECT = object.CALC_OBJECT;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    VALID_FROM = object.VALID_FROM;
                    VALID_TO = object.VALID_TO;
                    MONTHS = object.MONTHS;
                    STATISTIC = object.STATISTIC;
                    UNIT_PRICE = object.UNIT_PRICE;
                    AMT_REF = object.AMT_REF;
                    FREQUENCY = object.FREQUENCY;
                    START_DUE_DATE = object.START_DUE_DATE;
                    MANUAL_NO = object.MANUAL_NO;
                    FORMULA = object.FORMULA;
                    MEASUREMENT_TYPE = object.MEASUREMENT_TYPE;
                    LUAS = object.LUAS;
                    TOTAL = object.TOTAL;
                    NJOP_PERCENT = object.NJOP_PERCENT;
                    KONDISI_TEKNIS_PERCENT = object.KONDISI_TEKNIS_PERCENT;
                    TOTAL_NET_VALUE = object.TOTAL_NET_VALUE;
                    COA_PROD = object.COA_PROD;
                    TAX_CODE = object.TAX_CODE;
                }
            });

            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            oTable.fnAddData([CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, FORMULA, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, COA_PROD, tombolC, TAX_CODE]);

            //console.log(arrCondition);

            //console.log(data[1]);

            setTimeout(coHitungValue(), 1000);
        });
    }

    //Var untuk menghandle data table manual-frequency
    var manual_frequency = function () {
        var tablem = $('#manual-frequency');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 30,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "columnDefs": [
                {
                    "targets": [8],
                    "visible": false
                }
            ]
        });

        // Variabel untuk handle combobox condition
        var cond = function () {
            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //$("#memo-condition").append(listItems);
            $('select[name="condition-frequency"]').html(listItems);
        }
        var resetCondition = function () {
            $('select[name="condition-frequency"]').each(function (index, elem) {
                var baris = $(this).parents('tr')[0];
                var table = $('#manual-frequency').DataTable();
                var data = table.row(baris).data();
                var manual_no = data[0];

                var condition = function (manual) {
                    var elems = $('#condition').find('tbody tr');
                    var menu = ""
                    $(elems).each(function (index) {
                        var c_data = $('#condition').DataTable().row(index).data();
                        //console.log(c_data[0] + ' | ' + c_data[1], c_data[11], manual);
                        var m = c_data[11];
                        var v = c_data[0] + ' | ' + c_data[1];
                        //console.log(typeof manual, manual);
                        //console.log(typeof m, m);
                        if (manual == m) {
                            menu = v;
                        }
                    });
                    return menu;
                }
                var value = condition(manual_no);
                //console.log(elem);
                //console.log($(elem).find('option[value="' + value + '"]'));
                $(elem).find('option[value="' + value + '"]').attr('selected', 'selected');
                getConditionCode(elem);
            });
        }
        //Generate textbox(form) di data table manual-frequency
        $('#btn-add-manual-frequency').on('click', function () {
            var nomor = 10;

            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var xtable = $('#manual-frequency').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);


            var last = $("#manual-frequency").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            //console.log(l);
            if (l != 0) {
                nomor = (Number(last) + 10);
            }


            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" id="due-date-frequency" name="due-date-frequency" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="M2">M<sup>2</sup></option><option value="TON">TON</option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';

            oTable.fnAddData([nomor,
                condition,
                dueDateFrequency,
                netValueFrequency,
                qtyFrequency,
                unitFrequency,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                conditionCode], true);

            //set datepicker manual frequency
            resetDatePicker();
            // Panggil variable yg handle combobox condition
            cond();
            resetCondition();
            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //Masking Net Value

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});

            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }

            });
            $('input[name = "qty-frequency"]').inputmask({
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });

        });

        $('#manual-frequency').on('click', 'tr #btn-add-sama', function () {
            var table = $('#manual-frequency').DataTable();
            var data = table.row($(this).parents('tr')).data();

            var manual_no_sama = data[0];
            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control" onchange="getConditionCode(this)"></select>';
            //var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" id="due-date-frequency" name="due-date-frequency" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="M2">M<sup>2</sup></option><option value="TON">TON</option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';


            oTable.fnAddData([manual_no_sama,
                condition,
                dueDateFrequency,
                netValueFrequency,
                qtyFrequency,
                unitFrequency,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                conditionCode], true);

            //set datepicker manual frequency
            resetDatePicker();
            // Panggil variable yg handle combobox condition
            cond();
            resetCondition();
            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});

            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }

            });
        });
        var conditionCancel = '';
        var dueDateCancel = '';
        var netValueCancel = '';

        // BTN EDITCUS MANUAL FREQUENCY
        $('#manual-frequency').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var condVal = data[1];
            var dueDate = data[2];
            var netValue = data[3];
            var qty = data[4];
            var unitVal = data[5];
            var conditionCodeVal = data[8];

            conditionCancel = condVal;
            dueDateCancel = dueDate;
            netValueCancel = netValue;

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus-frequency"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#manual-frequency').dataTable();



            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" value="' + dueDate + '" id="due-date-frequency" name="due-date-frequency" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
            var netValueFrequency = '<input type="text" class="form-control" value="' + netValue + '" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" value="' + qty + '" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="M2">M<sup>2</sup></option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" value="' + conditionCodeVal + '" id="CONDITION_CODE" name="CONDITION_CODE">';

            openTable.fnUpdate(condition, nRow, 1, false);
            openTable.fnUpdate(dueDateFrequency, nRow, 2, false);
            openTable.fnUpdate(netValueFrequency, nRow, 3, false);
            openTable.fnUpdate(qtyFrequency, nRow, 4, false);
            openTable.fnUpdate(unitFrequency, nRow, 5, false);

            openTable.fnUpdate(tombol, nRow, 7, false);
            openTable.fnUpdate(conditionCode, nRow, 8, false);

            //set datepicker manual frequency
            resetDatePicker();
            // Panggil variable yg handle combobox condition
            cond();

            $(baris).find("#condition-frequency option[value='" + condVal + "']").prop('selected', true);
            $(baris).find("#unit-frequency option[value='" + unitVal + "']").prop('selected', true);

            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //Masking Net Value

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});

            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }

            });

            $('input[name = "qty-frequency"]').inputmask({
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });

        });

        $('#manual-frequency').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var manualCancel = data[0];

            //console.log('manual cancel ' + manualCancel);
            //console.log('condition cancel ' + conditionCancel);

            // Hapus Row 
            oTable.fnDeleteRow(baris);
            var json = JSON.parse('[' + arrManualFrequency + ']');


            // Deklarasi Variabel       
            var MANUAL_NO = "";
            var CONDITION_TYPE = "";
            var DUE_DATE = "";
            var NET_VALUE = "";
            var QUANTITY = "";
            var UNIT = "";
            var CONDITION = "";


            json.forEach(function (object) {
                if (object.MANUAL_NO == manualCancel && object.CONDITION_TYPE == conditionCancel && object.DUE_DATE == dueDateCancel) {
                    MANUAL_NO = object.MANUAL_NO;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    DUE_DATE = object.DUE_DATE;
                    NET_VALUE = object.NET_VALUE;
                    QUANTITY = object.QUANTITY;
                    UNIT = object.UNIT;
                    CONDITION = object.CONDITION;
                }
            });


            var tombolAddSama = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            oTable.fnAddData([MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT, tombolAddSama, tombolC, CONDITION]);
        });

        $('#manual-frequency').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#manual-frequency').on('click', 'tr #btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var a = $(baris).find('#condition-frequency').val();
            var b = $(baris).find('#due-date-frequency').val();
            var c = $(baris).find('#net-value-frequency').val();
            var d = $(baris).find('#CONDITION_CODE').val();
            var e = $(baris).find('#qty-frequency').val();
            var f = $(baris).find('#unit-frequency').val();
            console.log(b);
            console.log(e);
            console.log(f);

            //console.log(baris);
            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            if (b == '') {
                swal('Info', 'Silahkan isi Start Due Date', 'info')
                return false;
            } if (e == '') {
                swal('Info', 'Silahkan isi Qty', 'info');
                return false;
            } if (f == '') {
                swal('Info', 'Silahkan isi Unit Qty', 'info');
                return false;
            }
            else {
                console.log(111111);
                $('#manual-frequency').dataTable().fnUpdate(a, baris, 1, false);
                oTable.fnUpdate(b, baris, 2, false);
                oTable.fnUpdate(c, baris, 3, false);
                oTable.fnUpdate(e, baris, 4, false);
                oTable.fnUpdate(f, baris, 5, false);
                oTable.fnUpdate(d, baris, 8, false);
                var tombolC = '<center>';//<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                if ($('#btn-cancelcus').length) {
                    tombolC += '<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a>';
                }
                if ($('#btn-savecus-frequency').length) {
                    tombolC += '<a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a>';
                }
                tombolC += '</center>';

                oTable.fnUpdate(tombolC, baris, 7, false);
            }

            // document.getElementById('btn-savecus').disabled = true;

            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);
            return false;

        });
    }

    //Var untuk menghandle data table memo
    var memo = function () {
        var tablem = $('#table-memo');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "paging": false,
            "lengthChange": false,
            "bSort": false
        });

        $('#btn-add-memo').on('click', function () {
            $('#memo-condition').html('');
            $('#memo').val('');

            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //console.log(listItems);
            $("#memo-condition").append(listItems);

        });

        $('#btn-add-memo-md').on('click', function () {
            var raw_val = $('#memo-condition').val();
            if (raw_val == "" || raw_val == null) {
                alert("Mohon Memilih Memo Kondisi terlebih dahulu.");
                return false;
            }
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var v_split = raw_val.split("|");

            var memo_calc_object = v_split[0];
            var memo_cond_type = v_split[1];
            var memo = $('#memo').val();

            oTable.fnAddData([memo_calc_object,
                memo_cond_type,
                memo,
                '<center><a class="btn default btn-xs red" id="btn-delcus-memo"><i class="fa fa-trash"></i></a></center>'], true);
            $('#memo').html('');
            $('#addMemo').modal('hide');
        });


        var memoCancel = '';
        var conditionTypeCancel = '';
        var calObjectCancel = ''
        $('#table-memo').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var calObjectVal = data[0];
            var memoVal = data[2];
            var conditionTypeVal = data[1];

            calObjectCancel = calObjectVal;
            conditionTypeCancel = conditionTypeVal;
            memoCancel = memoVal;

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#table-memo').dataTable();
            var updateMemo = '<input type="text" id="memo" name="memo" value="' + memoCancel + '" class="form-control">';

            openTable.fnUpdate(updateMemo, nRow, 2, false);
            openTable.fnUpdate(tombol, nRow, 3, false);
        });

        $('#table-memo').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            // Hapus Row 
            oTable.fnDeleteRow(baris);
            var json = JSON.parse('[' + arrMemo + ']');

            // Deklarasi Variabel       
            var CALC_OBJECT = "";
            var CONDITION_TYPE = "";
            var MEMO = "";

            json.forEach(function (object) {
                if (object.CALC_OBJECT == calObjectCancel && object.CONDITION_TYPE == conditionTypeCancel && object.MEMO == memoCancel) {
                    CALC_OBJECT = object.CALC_OBJECT;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    MEMO = object.MEMO;
                }
            });

            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            oTable.fnAddData([CALC_OBJECT, CONDITION_TYPE, MEMO, tombolC]);
        });

        $('#table-memo').on('click', 'tr #btn-savecus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var memo = $(baris).find('#memo').val();

            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            $('#table-memo').dataTable().fnUpdate(memo, baris, 2, false);
            $('#table-memo').dataTable().fnUpdate(tombolDefault, baris, 3, false);

            return false;

        });


        $('#table-memo').on('click', 'tr #btn-delcus-memo', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#table-memo').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();
            mTable.fnDeleteRow(baris);
        });
    }

    // Var untuk menghandle sales rule
    var salesRule = function () {
        var tablem = $('#sales-based');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        var salesType = function () {
            $.ajax({
                type: "GET",
                url: "/TransContractOffer/GetDataDropDownSalesRule",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += '<option value="">--Choose--</option>';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].SALES_TYPE + "'>" + jsonList.data[i].SALES_TYPE + "</option>";
                    }
                    $('select[name="SALES_TYPE"]').html(listItems);
                }
            });
        }

        //Generate textbox(form) di data table manual-frequency
        $('#btn-add-sales-based').on('click', function () {
            var nomor = 10;

            var baris = $(this).parents('tr')[0];
            var table = $('#sales-based').DataTable();
            var xtable = $('#sales-based').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);


            var last = $("#sales-based").find("tr:last td:first").text();
            var cut = last.substring(2);
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            //console.log(l);
            if (l != 0) {
                //console.log(d);
                //console.log('d0 ' + d[0]);
                //console.log('nomor '+nomor);
                nomor = (Number(cut) + 10);
            }

            // Form input di data table Sales Based
            var salesT = '<select id="SALES_TYPE" name="SALES_TYPE" class="form-control" onchange="getUnitST(this)"></select>';
            var nameOfTerm = '<input type="text" name="NAME_OF_TERM" id="NAME_OF_TERM" class="form-control">';
            var calcFrom = '<input type="text" name="CALC_FROM" id="CALC_FROM" class="form-control">';
            var calcTo = '<input type="text" name="CALC_FROM" id="CALC_TO" class="form-control">';
            var unitST = '<input type="text" name="UNIT_ST" id="UNIT_ST" class="form-control" readonly>';
            var percentT = '<input type="text" name="PERCENT_T" id="PERCENT_T" class="form-control">';
            var tarifT = '<input type="text" name="TARIF_T" id="TARIF_T" class="form-control">';
            var rr = '<input type="text" name="RR" id="RR" class="form-control">';
            var minQtySales = '<input type="text" name="MIN_QTY_SALES" id="MIN_QTY_SALES" class="form-control">';
            var minProduction = '<input type="text" name="MIN_PRODUCTION" id="MIN_PRODUCTION" class="form-control">';
            var paymentType = '<select class="form-control" id="PAYMENT_TYPE" name="PAYMENT_TYPE"><option value="">-- Choose Payment Type --</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select>';



            oTable.fnAddData(['SR' + nomor,
                salesT,
                nameOfTerm,
                paymentType,
                calcFrom,
                calcTo,
                unitST,
                percentT,
                tarifT,
            'RR' + nomor,
                minQtySales,
                minProduction,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama-sr"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-sr"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-sr"><i class="fa fa-trash"></i></a></center>'], true);

            //Masking CALC FROM
            $('input[name="CALC_FROM"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            //Masking CALC TO
            $('input[name="CALC_TO"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            salesType();
        });

        $('#sales-based').on('click', 'tr #btn-add-sama-sr', function () {
            var table = $('#sales-based').DataTable();
            var data = table.row($(this).parents('tr')).data();

            var manual_no_sama = data[0];
            var rr_no_sama = data[9];
            // Form input di data table Sales Based
            var salesT = '<select id="SALES_TYPE" name="SALES_TYPE" class="form-control" onchange="getUnitST(this)"></select>';
            var nameOfTerm = '<input type="text" name="NAME_OF_TERM" id="NAME_OF_TERM" class="form-control">';
            var calcFrom = '<input type="text" name="CALC_FROM" id="CALC_FROM" class="form-control">';
            var calcTo = '<input type="text" name="CALC_FROM" id="CALC_TO" class="form-control">';
            var unitST = '<input type="text" name="UNIT_ST" id="UNIT_ST" class="form-control" readonly>';
            var percentT = '<input type="text" name="PERCENT_T" id="PERCENT_T" class="form-control">';
            var tarifT = '<input type="text" name="TARIF_T" id="TARIF_T" class="form-control">';
            var rr = '<input type="text" name="RR" id="RR" class="form-control">';
            var minQtySales = '<input type="text" name="MIN_QTY_SALES" id="MIN_QTY_SALES" class="form-control">';
            var minProduction = '<input type="text" name="MIN_PRODUCTION" id="MIN_PRODUCTION" class="form-control">';
            var paymentType = '<select class="form-control" id="PAYMENT_TYPE" name="PAYMENT_TYPE"><option value="">-- Choose Payment Type --</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select>';


            oTable.fnAddData([manual_no_sama,
                salesT,
                nameOfTerm,
                paymentType,
                calcFrom,
                calcTo,
                unitST,
                percentT,
                tarifT,
                rr_no_sama,
                minQtySales,
                minProduction,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama-sr"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-sr"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-sr"><i class="fa fa-trash"></i></a></center>'], true);

            //Masking CALC FROM
            $('input[name="CALC_FROM"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            //Masking CALC TO
            $('input[name="CALC_TO"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            salesType();
        });

        $('#sales-based').on('click', 'tr #btn-delcus-sr', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#sales-based').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#sales-based').on('click', 'tr #btn-savecus-sr', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#sales-based').DataTable();
            var data = table.row(baris).data();

            var valSalesType = $('#SALES_TYPE').val();
            var valNameOfTerm = $('#NAME_OF_TERM').val();
            var valCalcFrom = $('#CALC_FROM').val();
            var valCalcTo = $('#CALC_TO').val();
            var valUnit = $('#UNIT_ST').val();
            var valPercentT = $('#PERCENT_T').val();
            var valTarifT = $('#TARIF_T').val();
            var valrr = $('#RR').val();
            var valMinQtySales = $('#MIN_QTY_SALES').val();
            var valMinProduction = $('#MIN_PRODUCTION').val();
            var valPaymentType = $('#PAYMENT_TYPE').val();

            oTable.fnUpdate(valSalesType, baris, 1, false);
            oTable.fnUpdate(valNameOfTerm, baris, 2, false);
            oTable.fnUpdate(valPaymentType, baris, 3, false);
            oTable.fnUpdate(valCalcFrom, baris, 4, false);
            oTable.fnUpdate(valCalcTo, baris, 5, false);
            oTable.fnUpdate(valUnit, baris, 6, false);
            oTable.fnUpdate(valPercentT, baris, 7, false);
            oTable.fnUpdate(valTarifT, baris, 8, false);
            oTable.fnUpdate(valrr, baris, 9, false);
            oTable.fnUpdate(valMinQtySales, baris, 10, false);
            oTable.fnUpdate(valMinProduction, baris, 11, false);


            var idnya = table.row($(this).parents('tr')).data();

            var id_number = data[0];

            var isiObjSR = { "SR_NO": id_number, "SALES_TYPE": valSalesType };
            var resArraySR = arrTampungSR.push(isiObjSR);
            //console.log(isiObjSR);
            //console.log(resArraySR);

            arrTampungSR.forEach(function (entry) {
                //console.log(entry);
            });

            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus-sr');
            elem.parentNode.removeChild(elem);
            return false;

        });
    }

    // Var untuk menghandle reporting rule
    var reportingRule = function () {
        var tablerr = $('#reporting-rule');
        var oTable = tablerr.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        var salesTypeRR = function () {
            $.ajax({
                type: "GET",
                url: "/TransContractOffer/GetDataDropDownSalesRule",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += '<option value="">--Choose--</option>';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].SALES_TYPE + "'>" + jsonList.data[i].SALES_TYPE + "</option>";
                    }
                    $('select[name="SALES_TYPE_RR"]').html(listItems);
                }
            });
        }

        //Generate textbox(form) di data table reporting-rule
        $('#btn-add-reporting-rule').on('click', function () {
            var nomor = 10;

            var baris = $(this).parents('tr')[0];
            var table = $('#reporting-rule').DataTable();
            var xtable = $('#reporting-rule').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);


            var last = $("#reporting-rule").find("tr:last td:first").text();
            var cut = last.substring(2);

            var oke = 0;
            //console.log(l);
            if (l != 0) {
                nomor = (Number(cut) + 10);
            }

            // Form input di data table Reporting Rule
            var salesT = '<select id="SALES_TYPE_RR" name="SALES_TYPE_RR" class="form-control" onchange="getUnitST(this)"></select>';
            var termOfReportingRule = '<input type="text" name="TERM_OF_REPORTING_RULE" id="TERM_OF_REPORTING_RULE" class="form-control">';

            oTable.fnAddData(['RR' + nomor,
                salesT,
                termOfReportingRule,
                '<center><a class="btn default btn-xs green" id="btn-savecus-rr"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-rr"><i class="fa fa-trash"></i></a></center>'], true);
            salesTypeRR();
        });

        $('#reporting-rule').on('click', 'tr #btn-delcus-rr', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#reporting-rule').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#reporting-rule').on('click', 'tr #btn-savecus-rr', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#reporting-rule').DataTable();
            var data = table.row(baris).data();

            var valSalesTypeRR = $('#SALES_TYPE_RR').val();
            var valTermOfReportingRule = $('#TERM_OF_REPORTING_RULE').val();


            oTable.fnUpdate(valSalesTypeRR, baris, 1, false);
            oTable.fnUpdate(valTermOfReportingRule, baris, 2, false);

            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus-rr');
            elem.parentNode.removeChild(elem);
            return false;

        });
    }

    var ubahDropdown = function () {
        var oldValue = "";
        $('#CONTRACT_OFFER_TYPE').change(function () {
            var co_type = $('#CONTRACT_OFFER_TYPE').val();
            //console.log(oldValue, co_type);
            if (co_type && co_type !== '81') {
                //if (oldValue !== "") {
                //    console.log("delete");
                //}
                $('#old-contract-data').css({ 'display': 'block' });
                $('#search-contract').removeClass('disabled');
                $('#search-contract').prop('disabled');
            } else {
                if (oldValue != "" && oldValue != '81') {
                    $('#condition').dataTable().fnClearTable();
                    $('#manual-frequency').dataTable().fnClearTable();
                    $('#rental-object-co').dataTable().fnClearTable();
                    $('#table-memo').dataTable().fnClearTable();
                }
                $('.old-contract').css({ 'display': 'none' });
                $('#old-contract-data').css({ 'display': 'none' });
                $('#search-contract').addClass('disabled');
                $('#search-contract').removeProp('disabled');
            }

            $('#OLD_CONTRACT').val('');
            oldValue = co_type;
        });
    }

    var simpanRequest = function () {
        var table = $('#rental-object-co').dataTable();
        var RENTAL_REQUEST_TYPE = $('#CONTRACT_OFFER_TYPE').val();
        var BE_ID = $('#BUSINESS_ENTITY_ID').val();
        var RENTAL_REQUEST_NAME = $('#CONTRACT_OFFER_NAME').val();
        var CONTRACT_USAGE = $('#USAGE_TYPE').val();
        var CUSTOMER_ID = $('#CUSTOMER_ID').val();
        var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
        var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
        var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
        var CUSTOMER_AR = $('#CUSTOMER_AR').val();
        var OLD_CONTRACT = $('#OLD_CONTRACT').val();
        var SERTIFIKAT = $('#SERTIFIKAT').val();
        //console.log(RENTAL_REQUEST_TYPE);
        //console.log(BE_ID);
        //console.log("contract usage " + CONTRACT_USAGE);
        //console.log(CUSTOMER_ID)
        var arrObjects;
        if (!(RENTAL_REQUEST_TYPE && BE_ID && RENTAL_REQUEST_NAME &&
            CUSTOMER_ID && CUSTOMER_NAME && CONTRACT_START_DATE &&
            CONTRACT_END_DATE && CUSTOMER_AR && CONTRACT_USAGE)) {
            swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
        } else {
            arrObjects = new Array();
            var countDt = table.fnGetData();
            //var grandtotal = 0;
            for (var i = 0; i < countDt.length; i++) {
                var item = table.fnGetData(i);
                var param2 = {
                    RO_NUMBER: item[0].toString(),
                    OBJECT_ID: item[1],
                    OBJECT_TYPE_ID: item[2],
                    LAND_DIMENSION: item[5],
                    BUILDING_DIMENSION: item[6],
                    INDUSTRY: item[7]
                };
                arrObjects.push(param2);
            }

            var param = {
                RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                BE_ID: BE_ID,
                RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                CONTRACT_USAGE: CONTRACT_USAGE,
                CUSTOMER_ID: CUSTOMER_ID,
                CUSTOMER_NAME: CUSTOMER_NAME,
                CONTRACT_START_DATE: CONTRACT_START_DATE,
                CONTRACT_END_DATE: CONTRACT_END_DATE,
                CUSTOMER_AR: CUSTOMER_AR,
                OLD_CONTRACT: OLD_CONTRACT,
                SERTIFIKAT: SERTIFIKAT,
                Objects: arrObjects
            };

            //var document_number = "";
            App.blockUI({
                boxed: true
            });
            $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/TransRentalRequest/SaveHeader",
                //timeout: 30000,

                success: function (data) {
                    var idHeader = data.trans_id;
                    App.unblockUI();
                    if (data.status === 'S') {
                        //swal('Success', 'Rental Request Transaction Added Successfully with Transaction Number:' + idHeader, 'success').then(function (isConfirm) {
                        simpanKontrak(idHeader);
                        //});
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/TransContractOffer";
                        });
                    }
                    arrObjects = [];
                },
                error: function (data) {
                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                        window.location = "/TransContractOffer";
                    });
                    App.unblockUI();
                    //console.log(data.responeText);
                }
            });

        } // END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM

    }

    var addSertifikat = function () {
        $('#btn-add-sertifikat').click(function () {
            $('#viewSertifikat').modal('show');
            $('#table-sertifikat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataSertifikat/",
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "ATTACHMENT",
                        "class": "dt-body-center",
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a id="btn-save-sertifikat" class="btn btn-icon-only green" title="Save" ><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });

        $('#table-sertifikat').on('click', 'tr #btn-save-sertifikat', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-sertifikat').DataTable();
            var data = table.row(baris).data();
            //console.log(data['ATTACHMENT']);
            //document.getElementById('SERTIFIKAT').innerHTML = data['ATTACMENT'];
            $('#SERTIFIKAT_ID').val(data['ATTACHMENT_ID']);
            $('#SERTIFIKAT').val(data['ATTACHMENT']);
            //var serti_id = data['ATTACHMENT_ID'];
            //console.log(serti_id);
            //console.log(data)
            //var xInd = $('#INDUSTRY').val();
            //xoTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_TANAH_RO'], data['LUAS_BANGUNAN_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus-ob"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);
           
            $('#viewSertifikat').modal('hide');
        });
    }
    

    var simpanKontrak = function (requestNO) {
        if (!requestNO) {
            return;
        }
        var CONTRACT_OFFER_TYPE = $('#CONTRACT_OFFER_TYPE option:selected').text().substring(0, 4);
        var BUSINESS_ENTITY_NAME = $('#BUSINESS_ENTITY_ID option:selected').text();
        var BUSINESS_ENTITY_ID = $('#BUSINESS_ENTITY_ID').val();
        var CONTRACT_OFFER_NAME = $('#CONTRACT_OFFER_NAME').val();
        //var RENTAL_REQUEST_NAME = $('#CONTRACT_OFFER_NAME').val();
        var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
        var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
        var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
        var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
        var CUSTOMER_AR = $('#CUSTOMER_AR').val();
        var CUSTOMER_ID = $('#CUSTOMER_ID').val();
        //var PROFIT_CENTER = $('#PROFIT_CENTER option:selected').text();
        var PROFIT_CENTER_ID = $('#PROFIT_CENTER').val();
        var USAGE_TYPE = $('#USAGE_TYPE option:selected').text().substring(0, 3);
        var USAGE_TYPE_NAME = $('#USAGE_TYPE option:selected').text();
        var CONTRACT_CURRENCY = $('#CONTRACT_CURRENCY').val();
        var OFFER_STATUS = $('#OFFER_STATUS').val();
        var ANJUNGAN_ID = $('#acceptID').val();
        var SERTIFIKAT = $('#SERTIFIKAT').val();
        var SERTIFIKAT_ID = $('#SERTIFIKAT_ID').val();
        //var OLD_CONTRACT = $('#OLD_CONTRACT').val();
        var arrContractObjects;
        var arrContractConditions;
        var arrContractFrequencies;
        var arrContractMemos;
        //if (!ANJUNGAN_ID) {
        //    ANJUNGAN_ID = 0;
        //}
        // Declare dan getData dari dataTable OBJECT
        var DTObject = $('#rental-object-co').dataTable();
        var countDTObject = DTObject.fnGetData();
        arrContractObjects = new Array();
        for (var i = 0; i < countDTObject.length; i++) {
            var itemDTObject = DTObject.fnGetData(i);
            var paramDTObject = {
                OBJECT_ID: itemDTObject[1],
                OBJECT_TYPE: itemDTObject[3],
                RO_NAME: itemDTObject[4],
                START_DATE: CONTRACT_START_DATE,
                END_DATE: CONTRACT_END_DATE,
                //LUAS_TANAH: itemDTObject[6],
                //LUAS_BANGUNAN: itemDTObject[5],
                LUAS_TANAH: itemDTObject[5],
                LUAS_BANGUNAN: itemDTObject[6],
                INDUSTRY: itemDTObject[7]
            }
            arrContractObjects.push(paramDTObject);
        }

        //console.log("LUAS BANGUNAN : " + LUAS_BANGUNAN);
        //console.log("LUAS TANAH : " + LUAS_TANAH);
        // End Declare dan getData dari dataTable OBJECT

        // start getData dari dataTable CONDITION
        var DTCondition = $('#condition').dataTable();
        var countDTCondition = DTCondition.fnGetData();
        arrContractConditions = new Array();
        for (var i = 0; i < countDTCondition.length; i++) {
            var itemDTCondition = DTCondition.fnGetData(i);

            // hitung pembagian total net value berdasarkan frekuensi pembayaran
            var freq_pembayaran = itemDTCondition[8];
            var net_value = itemDTCondition[17];
            var formulanya = itemDTCondition[6];
            var jumlah_bulan = itemDTCondition[4];
            var pecah_bayar = 0;

            // Cek apakah frekuensi pembayaran bulanan atau tahunan
            if (freq_pembayaran == 'YEARLY') {
                var tahunan = Number(jumlah_bulan) / 12;
                pecah_bayar = Number(net_value) / Number(tahunan);
                //console.log('tahunan ' + pecah_bayar);
            }
            else if (freq_pembayaran == 'MONTHLY') {
                var bulanan = jumlah_bulan;
                pecah_bayar = Number(net_value) / Number(bulanan);
                //console.log('bulanan ' + pecah_bayar);
            }
            else {
                pecah_bayar = Number(net_value);
                //console.log('else ' + pecah_bayar);
            }

            // Cek apakah status condition yes atau no
            var status_condition = itemDTCondition[5];
            var nilai_condition = 0;
            if (status_condition == "Yes") {
                nilai_condition = 1;
            }
            else {
                nilai_condition = 0;
            }

            var rUnitPrice = itemDTCondition[7];
            var nfUnitPrice = parseFloat(rUnitPrice.split(',').join(''));

            var rTotal = itemDTCondition[14];
            var nfTotal = parseFloat(rTotal.split(',').join(''));

            var rTotalNetValue = itemDTCondition[17];
            var nfTotalNetValue = parseFloat(rTotalNetValue.split(',').join(''));

            var paramDTCondition = {
                CALC_OBJECT: itemDTCondition[0],
                CONDITION_TYPE: itemDTCondition[1],
                VALID_FROM: itemDTCondition[2],
                VALID_TO: itemDTCondition[3],
                MONTHS: itemDTCondition[4],
                STATISTIC: nilai_condition.toString(),
                FORMULA: itemDTCondition[6],
                UNIT_PRICE: nfUnitPrice.toString(),
                AMT_REF: itemDTCondition[8],
                FREQUENCY: itemDTCondition[9],
                START_DUE_DATE: itemDTCondition[10],
                MANUAL_NO: itemDTCondition[11].toString(),
                MEASUREMENT_TYPE: itemDTCondition[12],
                LUAS: itemDTCondition[13],
                TOTAL: nfTotal.toString(),
                NJOP_PERCENT: itemDTCondition[15],
                KONDISI_TEKNIS_PERCENT: itemDTCondition[16],
                TOTAL_NET_VALUE: nfTotalNetValue.toString(),
                TAX_CODE: itemDTCondition[20],
                INSTALLMENT_AMOUNT: isNaN(pecah_bayar) ? "0" : pecah_bayar.toString(),
                // Belum ditambai COA PROD
                COA_PROD: itemDTCondition[18]
            };
            arrContractConditions.push(paramDTCondition);
        }
        //END PUSH CONDITION
        //Declare dan get data dari dataTable MANUAL FREQUENCY
        var DTManualFrequency = $('#manual-frequency').dataTable();
        var countDTManualFrequency = DTManualFrequency.fnGetData();
        arrContractFrequencies = new Array();
        for (var i = 0; i < countDTManualFrequency.length; i++) {
            var itemDTManualFrequency = DTManualFrequency.fnGetData(i);

            //potong2  
            var raw_manual_freq = itemDTManualFrequency[1];
            var splitselectedCondition = raw_manual_freq.split(" | ");
            var code_lengkap = splitselectedCondition[1];
            var cut = code_lengkap.substring(0, 4);
            //console.log(cut);

            var splitChar = raw_manual_freq.split(" | ");
            var objectId = splitChar[0];

            var net_value = itemDTManualFrequency[3];
            var nfNetVal = parseFloat(net_value.split(',').join(''));
            var paramDTManualFrequency = {
                MANUAL_NO: itemDTManualFrequency[0].toString(),
                CONDITION: cut, //Untuk condition ambil id CONDITION_CODE di index 9 yg di hidden
                DUE_DATE: itemDTManualFrequency[2],
                QUANTITY: itemDTManualFrequency[4],
                UNIT: itemDTManualFrequency[5],
                NET_VALUE: nfNetVal.toString(),
                OBJECT_ID: objectId
            }
            arrContractFrequencies.push(paramDTManualFrequency);
        }
        //END PUSH FREQUENCY
        //Declare dan get data dari dataTable MEMO
        var DTMemo = $('#table-memo').dataTable();
        var countDTMemo = DTMemo.fnGetData();
        arrContractMemos = new Array();
        for (var i = 0; i < countDTMemo.length; i++) {
            var itemDTMemo = DTMemo.fnGetData(i);
            var paramDTMemo = {
                CALC_OBJECT: itemDTMemo[0],
                CONDITION_TYPE: itemDTMemo[1],
                MEMO: itemDTMemo[2]
            }
            arrContractMemos.push(paramDTMemo);
        }
        var param = {
            ANJUNGAN_ID: ANJUNGAN_ID,
            //RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
            RENTAL_REQUEST_NO: requestNO,
            CONTRACT_OFFER_TYPE: CONTRACT_OFFER_TYPE,
            BUSINESS_ENTITY_NAME: BUSINESS_ENTITY_NAME,
            BE_ID: BUSINESS_ENTITY_ID,
            CONTRACT_OFFER_NAME: CONTRACT_OFFER_NAME,
            CONTRACT_START_DATE: CONTRACT_START_DATE,
            CONTRACT_END_DATE: CONTRACT_END_DATE,
            TERM_IN_MONTHS: TERM_IN_MONTHS,
            CUSTOMER_NAME: CUSTOMER_NAME,
            CUSTOMER_AR: CUSTOMER_AR,
            CUSTOMER_ID: CUSTOMER_ID,
            PROFIT_CENTER: PROFIT_CENTER_ID,
            USAGE_TYPE: USAGE_TYPE_NAME,
            CONTRACT_USAGE: USAGE_TYPE,
            CURRENCY: CONTRACT_CURRENCY,
            SERTIFIKAT: SERTIFIKAT,
            SERTIFIKAT_ID: SERTIFIKAT_ID,
            OFFER_STATUS: OFFER_STATUS,
            Objects: arrContractObjects,
            Conditions: arrContractConditions,
            Frequencies: arrContractFrequencies,
            Memos: arrContractMemos
        };

        // Ajax Save Data Header
        App.blockUI({ boxed: true });
        $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/TransContractOffer/SaveHeader",

            success: function (data) {
                if (data.status === 'S') {
                    console.log(data.trans_id);
                    swal('Success', data.message + '\nPlease upload attachment document!' /* + '\nRental Request Transaction Added Successfully with Transaction Number:' + requestNO */, 'success').then(function (isConfirm) {
                        window.location = "/TransContractOffer/AddSurat/" + data.trans_id;
                    });
                } else {
                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                        window.location = "/TransContractOffer/AddSurat/" + data.trans_id;
                    });
                }
                App.unblockUI();
            },
            error: function (data) {
                swal('Failed', data.message, 'error').then(function (isConfirm) {
                    window.location = "/TransContractOffer";
                });
            }
        });
        arrContractObjects = [];
        arrContractConditions = [];
        arrContractFrequencies = [];
        arrContractMemos = [];

    }

    var unitPriceAuto = function () {
        //cari coa
        $('#condition').on('click', 'tr #btn-coa', function () {
            $('#coaModal').modal('show');
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetListCoa",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;
                        return data.data;
                    },
                },

                //"ajax" : function (data, callback, settings) {
                //    // make a regular ajax request using data.start and data.length
                //    $.get('/TransContractOffer/GetListCoa', {
                //        draw: data.length,
                //        start: data.start,
                //        length: data.start,
                //        search: data.search.value
                //    }, function (res) {
                //        // map your server's response to the DataTables format and pass it to
                //        // DataTables' callback
                //            //console.log(res);
                //        callback({
                //            recordsTotal: res.paging.total,
                //            recordsFiltered: res.paging.total,
                //            data: res.result
                //        });
                //    });
                //},

                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            //console.log(data,value);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                c_data = $('#condition').DataTable().row(index).node();
                //console.log(index, c_data)

                var elem = $(c_data).find('#coa_prod');
                elem.val(value);

            });
            $('#coaModal').modal('hide');
        });

        $('.input-percentage').inputmask('percentage', {
            'digits': 2,
            'digitsOptional': false,
            rightAlign: true
        });
        // click plus button in condition table
        $('#condition').on('click', 'tr #btn-price', function () {
            var be_id = $('#BUSINESS_ENTITY_ID').val();
            var profit_center = $('#PROFIT_CENTER').val();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();
            var calc_obj = data[0];
            var cond_type = data[1];
            var ro_number = calc_obj.split(" ")[1];
            var ro_data = null
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                }
            });
            if (ro_data) {
                $('#modalTarifPedoman').modal('show');
                $('#CALC_OBJ').val(calc_obj);
                $('#COND_TYPE').val(cond_type);
                var kode_industri = ro_data[7].split(" - ")[0];
                var batas = Number(ro_data[5]) + Number(ro_data[6]);
                var batas_luas = function () {
                    if (kode_industri == "09") {
                        if (batas >= 200) {
                            return 200;
                        } else if (batas >= 100) {
                            return 100;
                        }
                    }
                    return 0;
                }

                if ($.fn.DataTable.isDataTable('#table-pedoman')) {
                    $('#table-pedoman').DataTable().destroy();
                }

                $('#table-pedoman').DataTable({

                    "ajax":
                    {
                        "url": "/TarifPedoman/GetDataActivePedoman",
                        "type": "GET",
                        "data": function (data) {
                            data.be_id = be_id
                            data.kode_industri = kode_industri
                            data.batas_luas = batas_luas
                            return data
                        }
                    },
                    "columns": [
                        {
                            "data": "TIPE_TARIF",
                            "render": function (data, type, full) {
                                if (data == 'PERCENT') {
                                    var aksi = '<label class="btn default btn-xs green btn-pedoman-percent">'
                                        + '<input type="radio" name="btn-cus-pedoman" id="option-' + full.ID + '" autocomplete="off">'
                                        + '</label>'
                                } else if (data == 'TARIF') {
                                    var aksi = '<label class="btn default btn-xs green btn-pedoman-tarif">'
                                        + '<input type="radio" name="btn-cus-pedoman" id="option-' + full.ID + '" autocomplete="off">'
                                        + '</label>'
                                }
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALUE",
                            "class": "dt-body-right",
                            "render": function (data, type, full) {
                                if (full.TIPE_TARIF == 'TARIF') {
                                    var sdata = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                } else if (full.TIPE_TARIF == 'PERCENT') {
                                    var sdata = data.toString() + "%";
                                }
                                return sdata;
                            }
                        },
                        {
                            "data": "TERMINAL_NAME",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TIPE_TARIF",
                            "class": "dt-body-center"
                        },
                        {
                            "class": "dt-body-center",
                            "render": function (data, type, full) {
                                return full.START_ACTIVE_DATE + " - " + full.END_ACTIVE_DATE
                            }
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,

                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
            }
        });

        // choose pedoman percent
        $('#table-pedoman').on('click', 'tr .btn-pedoman-percent', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();

            $('.percentage').show();
            $('#NJOP_PERCENT').val(data['VALUE']);
            $('#NJOP_PERCENT_DISPLAY').val(data['VALUE']);

            var beid = $('#BUSINESS_ENTITY_ID').val();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().ajax.reload();
            } else {
                $('#table-filter-detail-njop').DataTable({
                    "ajax":
                    {
                        "url": "/TarifPedoman/GetDataActiveNJOP?be_id=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ACTIVE",
                            "render": function (data) {
                                //console.log(full);
                                var aksi = '<a class="btn default btn-xs green" id="btn-cus-njop"><i class="fa fa-check"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "KEYWORD",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALUE",
                            "class": "dt-body-right",
                            "render": function (data, type, full) {
                                var sdata = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                return "Rp " + sdata;
                            }
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,

                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
            }
        });

        // choose pedoman tarif
        $('#table-pedoman').on('click', 'tr .btn-pedoman-tarif', function () {
            $('#NJOP_PERCENT').val('');
            $('#NJOP_PERCENT_DISPLAY').val('');
            $('.percentage').hide();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().destroy();
            }

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data['VALUE'];

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();

                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val('');
                    var elem3 = $(c_data).find('#persen-from-njop');
                    elem3.val('');
                    var elem2 = $(c_data).find('#formula');
                    elem2.val("D");
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);
                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#modalTarifPedoman').modal('hide');
        });

        // choose njop
        $('#table-filter-detail-njop').on('click', 'tr #btn-cus-njop', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail-njop').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var percent = $('#NJOP_PERCENT').val();
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data.VALUE
            //console.log(value, data);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();
                    //console.log(index, c_data)
                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val(100);
                    var elem3 = $(c_data).find('#persen-from-njop');
                    elem3.val(percent);
                    var elem2 = $(c_data).find('#formula');
                    elem2.val("E1");
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);

                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#modalTarifPedoman').modal('hide');
        });
    }

    var unitPriceAuto1 = function () {
        $('.input-percentage').inputmask('percentage', {
            'digits': 2,
            'digitsOptional': false,
            rightAlign: true
        });

        // click plus button in condition table
        $('#condition').on('click', 'tr #btn-price', function () {
            var be_id = $('#BUSINESS_ENTITY_ID').val();
            var profit_center = $('#PROFIT_CENTER').val();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();
            var calc_obj = data[0];
            var cond_type = data[1];
            var ro_number = calc_obj.split(" ")[1];
            var ro_data = null
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    //console.log(index);
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                }
            });
            var param = {
                be_id: be_id,
                profit_center: profit_center
            }
            //console.log(ro_data)
            if (ro_data) {
                $.ajax({
                    contentType: "application/json",
                    data: param,
                    method: "GET",
                    url: "/TarifPedoman/GetActivePedoman",
                    success: function (data) {
                        var xdata = null;
                        var batas = Number(ro_data[5]);
                        var kode = ro_data[7].split(" - ")[0];
                        if (data.ID === 0) {
                            swal('Info', 'No data available in tarif pedoman!', 'info')
                        } else {
                            $('#btn-add-njop').prop('href', '/TarifPedoman/Njop?cabang=' + data.BRANCH_ID);
                            $('#CALC_OBJ').val(calc_obj);
                            $('#COND_TYPE').val(cond_type);
                        }

                        // untuk mencari row di table condition

                        // tidak null berarti percent dan tarif
                        if (data.tarif != null) {
                            $.each(data.tarif, function (index, tarif) {
                                //console.log(tarif, kode, batas);
                                if (tarif.kode === kode && tarif.batas <= batas) {
                                    if (xdata === null) {
                                        xdata = tarif;
                                    } else if (xdata.batas < tarif.batas) {
                                        xdata = tarif;
                                    }
                                }
                            })
                            //.then(function () {
                            //console.log(xdata, data);
                            if (data.TIPE_TARIF === 'TARIF') {
                                var elem2 = $(baris).find('#formula');
                                elem2.val("D");
                                var elem = $(baris).find('#unit-price');
                                elem.val(xdata.value);
                                setTimeout(function () {
                                    isiTotalUnitPrice(elem);
                                    //hitungFormula(elem);
                                }, 200);
                            } else if (data.TIPE_TARIF === 'PERCENT') {
                                $('.percentage').show();
                                $('#NJOP_PERCENT').val(xdata.percentage);
                                $('#NJOP_PERCENT_DISPLAY').val(xdata.percentage);
                                $('#tarifNJOP').modal('show');
                            }
                            //});
                        } else if (data.TIPE_TARIF === 'NJOP') {
                            $('.percentage').hide();
                            $('#NJOP_PERCENT').val('');
                            $('#tarifNJOP').modal('show');
                        }
                    }
                })
            }

        })

        // click filter njop button
        $('#btn-filter-data-njop').click(function () {
            var beid = $('#BUSINESS_ENTITY_ID').val();
            var percent = $('#NJOP_PERCENT').val();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().destroy();
            }

            $('#table-filter-detail-njop').DataTable({

                "ajax":
                {
                    "url": "/TarifPedoman/GetDataActiveNJOP?be_id=" + beid,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "ACTIVE",
                        "render": function (data) {
                            //console.log(full);
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-njop"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KEYWORD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALUE",
                        "class": "dt-body-right",
                        "render": function (data, type, full) {
                            var sdata = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            return "Rp " + sdata;
                        }
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        // choose njop
        $('#table-filter-detail-njop').on('click', 'tr #btn-cus-njop', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail-njop').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var percent = $('#NJOP_PERCENT').val();
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data.VALUE
            //console.log(value, data);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();
                    //console.log(index, c_data)
                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val("100");
                    var elem3 = $(c_data).find('#formula');
                    elem3.val("E1");
                    var elem2 = $(c_data).find('#persen-from-njop');
                    elem2.val(percent);
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);

                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#tarifNJOP').modal('hide');
        });
    }

    // Var untuk handle simpan data
    var simpanData = function () {
        $('#btn-update').click(function () {

            var tableco = $('#rental-object-co').dataTable();
            var table = $('#table-memo').dataTable();

            var cekP = $("#CUSTOMER_AR").val();
            cekPiut("1B", cekP);
            var xPiut = $("#cPiut").val();
            //console.log(xPiut);
            var cek = false;
            var DTRental = $('#rental-object-co').dataTable();
            var all_row = DTRental.fnGetNodes();
            for (var i = 0; i < all_row.length; i++) {
                cek = $(all_row[i]).find('select[name="INDUSTRY"]').val();
            }
            //console.log('cek ' + cek);
            // Header Data
            //var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
            //var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
            var CONTRACT_OFFER_TYPE = $('#CONTRACT_OFFER_TYPE option:selected').text().split(" ")[0];
            //var BUSINESS_ENTITY_ID = $('#BUSINESS_ENTITY_ID').val();
            var CONTRACT_OFFER_NAME = $('#CONTRACT_OFFER_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            var CUSTOMER_ID = $('#CUSTOMER_ID').val();
            //var PROFIT_CENTER = $('#PROFIT_CENTER option:selected').text();
            //var PROFIT_CENTER_ID = $('#PROFIT_CENTER').val();
            var USAGE_TYPE_ID = $('#USAGE_TYPE').val();
            var CONTRACT_CURRENCY = $('#CONTRACT_CURRENCY').val();
            var OFFER_STATUS = $('#OFFER_STATUS').val();
            var OLD_CONTRACT = $('#OLD_CONTRACT').val();
            var SERTIFIKAT = $('#SERTIFIKAT').val();

            //cek apakah pada condition terdapat kondisi biaya administrasi atau tidak
            var resultCek = 0;
            var dueDateCek = 0;
            var DTConditionCek = $('#condition').dataTable();
            var countDTConditionCek = DTConditionCek.fnGetData();
            var DTManualfCek = $('#manual-frequency').dataTable();
            var countDTManualfCek = DTManualfCek.fnGetData();
            secondCellArray = [];
            dueDate = [];

            for (var i = 0; i < countDTConditionCek.length; i++) {
                var itemDTConditionCek = DTConditionCek.fnGetData(i);
                secondCellArray.push(itemDTConditionCek[1]);
            }

            for (var i = 0; i < countDTConditionCek.length; i++) {
                var itemDTConditionCek = DTConditionCek.fnGetData(i);
                dueDate.push(itemDTConditionCek[10]);
            }

            for (var i = 0; i < countDTManualfCek.length; i++) {
                var itemDTConditionCek = DTManualfCek.fnGetData(i);
                dueDate.push(itemDTConditionCek[2]);
            }

            if (secondCellArray.indexOf("Z009 ADMINISTRASI") > -1) {
                resultCek = 1;
                //console.log('ada');
            } else {
                resultCek = 0;
                //console.log('tidak ada');
            }
            //console.log(resultCek);

            if (dueDate.indexOf("") > -1) {
                dueDateCek = 1;
                //console.log('ada');
            } else {
                dueDateCek = 0;
                //console.log('tidak ada');
            }
            //console.log(dueDateCek);

            if (!(CUSTOMER_ID && CONTRACT_OFFER_TYPE && CONTRACT_OFFER_NAME &&
                CONTRACT_START_DATE && CONTRACT_END_DATE && TERM_IN_MONTHS &&
                CUSTOMER_NAME && CUSTOMER_AR && USAGE_TYPE_ID && CONTRACT_CURRENCY && OFFER_STATUS)) {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            } else if (CONTRACT_OFFER_TYPE != 'ZC01' && OLD_CONTRACT == '') {
                swal('Warning', 'Please Choose Old Contract!', 'warning');
            } else if (cek) {
                swal('Warning', 'Please Choose Industry Type And Click Check Button Before You Save!', 'warning');
                //console.log('warning');
            } else if (dueDateCek == 1) {
                swal('Warning', 'Please fill Due Date and Start Due Date!', 'warning');
            }
            else if (xPiut == "X") {
                swal('Warning', 'Customer memiliki piutang!', 'warning');

            } else if (tableco.fnSettings().aoData.length === 0) {
                swal('Info', 'No data available in rental request object!', 'info');
            } else if (table.fnSettings().aoData.length === 0) {
                swal('Info', 'Please Fill Memo!', 'info');
            } else {
                simpanRequest();
            }
        });
    }

    var cariOldContract = function () {

        function addMonths(date, count) {
            if (date && count) {
                var m, d = (date = new Date(+date)).getDate()

                date.setMonth(date.getMonth() + count, 1)
                m = date.getMonth()
                date.setDate(d)
                if (date.getMonth() !== m) date.setDate(0)
            }
            return date
        }

        $('#btn-filter-co').click(function () {

            if ($.fn.DataTable.isDataTable('#table-old-contract')) {
                $('#table-old-contract').DataTable().destroy();
            }

            $('#table-old-contract').DataTable({

                "ajax":
                {
                    "url": "/TransContract/GetDataTransContract",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "render": function (data, type, full) {
                            //console.log(full);
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-co"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NAME"
                    },
                    {
                        "data": "BUSINESS_PARTNER_NAME"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STATUS == '1') {
                                //if (full.CONTRACT_STATUS === 'APPROVED') {
                                return '<span class="label label-sm label-success"> APPROVED </span>';
                                //}
                            }
                            else if (full.STATUS == '2') {
                                //if (full.CONTRACT_STATUS === 'TERMINATED') {
                                return '<span class="label label-sm label-danger"> TERMINATED </span>';
                                //}
                            }
                            else {
                                return '<span class="label label-sm label-danger"> INACTIVE </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        $('#btn-filter-coa').click(function () {
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetListCoa",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;

                        //response.draw = response.paging.page;
                        //response.recordsTotal = response.paging.total;
                        //response.recordsFiltered = response.paging.total;
                        //response.data = response.result;
                        console.log(data);
                        return data.data;
                    },

                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-co"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
            $('#dataTables_filter input').unbind();
            $('#dataTables_filter input').bind('keyup', function (e) {
                if (e.keyCode == 13) {
                    Table.fnFilter($(this).val());
                }
            });
        });


        $('#table-old-contract').on('click', 'tr #btn-cus-co', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-old-contract').DataTable();
            var data = table.row(baris).data();
            //console.log(data)


            //var tableDetilCondition = $('#condition').DataTable();
            //tableDetilCondition.destroy();

            //var tableDetilManual = $('#table-detil-manual').DataTable();
            //tableDetilManual.destroy();

            //var tableDetilMemo = $('#table-detil-memo').DataTable();
            //tableDetilMemo.destroy();

            $('#OLD_CONTRACT').val(data['CONTRACT_NO']);
            //$("#DETAILCONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            //$("#DETAILRENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            //$("#DETAILCONTRACT_TYPE").val(data["CONTRACT_TYPE"]);
            //$("#BUSINESS_ENTITY_ID option:contains('" + data["BE_ID"] + "')").attr('selected', 'selected');

            //var text1 = data["PROFIT_CENTER"];
            //$("#PROFIT_CENTER option").filter(function () {
            //    //may want to use $.trim in here
            //    return $(this).text() == text1;
            //}).prop('selected', true);

            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_NAME"]);

            var date = data["CONTRACT_START_DATE"].replace(/\./g, '/')
            var datearray = date.split("/");
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            $("#CONTRACT_START_DATE").val(date);
            $("#CONTRACT_START_DATE").datepicker();
            $("#CONTRACT_START_DATE").datepicker('setDate', new Date(newdate)); //set value

            var dateA = data["CONTRACT_END_DATE"].replace(/\./g, '/')
            var datearrayA = dateA.split("/");
            var newdateA = datearrayA[1] + '/' + datearrayA[0] + '/' + datearrayA[2];
            $("#CONTRACT_END_DATE").val(dateA);
            $("#CONTRACT_END_DATE").datepicker();
            $("#CONTRACT_END_DATE").datepicker('setDate', new Date(newdateA)); //set value

            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["BUSINESS_PARTNER_NAMES"]);
            $("#CUSTOMER_ID").val(data["BUSINESS_PARTNER"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            cekPiut("1B", data["CUSTOMER_AR"]);
            var xPiut = $("#cPiut").val();
            //console.log(xPiut);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#BUSINESS_ENTITY_ID").val(data["BE_ID"]);
            $("#USAGE_TYPE option:contains('" + data["CONTRACT_USAGE"] + "')").attr('selected', 'selected');
            $("#CONTRACT_CURRENCY option:contains('" + data["CURRENCY"] + "')").attr('selected', 'selected');

            $('.old-contract').css({ 'display': 'block' });
            $("#CONTRACT_PERIOD").val(data["TERM_IN_MONTHS"]);
            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"]);
            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"]);
            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
            $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
            coHitungPeriod();

            var tableEdit = $('#rental-object-co').DataTable();
            tableEdit.destroy();


            // VARIABLE UNTUK PERMOHONAN PERPANJANGAN 
            var NEW_CONTRACT_START_DATE;
            var NEW_CONTRACT_END_DATE;
            var NEW_DUE_DATE = false;

            //STORE RO NUMBER TO CHECK VACANT STATUS
            var ro_numbers = new Array();

            // GET DATA OBJECT FROM OLD CONTRACT
            var id_rental = data["RENTAL_REQUEST_NO"];

            var req_RO = $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rental,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {

                    // Edit Data Table
                    var tabler = $('#rental-object-co');
                    var xTable = tabler.dataTable({
                        "lengthMenu": [
                            [5, 15, 20, 50],
                            [5, 15, 20, 50]
                        ],
                        "pageLength": 5,
                        "language": {
                            "lengthMenu": " _MENU_ records"
                        },
                        "searching": false,
                        "lengthChange": false,
                        "columnDefs": [
                            {
                                "targets": [0, 2],
                                "class": "dt-body-center",
                                "visible": false
                            },
                            {
                                "targets": [1],
                                "class": "dt-body-center"
                            },
                            {
                                "targets": [5, 6],
                                "class": "dt-body-center"
                            },
                            {
                                "targets": [8],
                                "class": "dt-body-center",
                                "width": "10%"
                            },
                        ]
                    });

                    xTable.fnClearTable();
                    var jsonList = data;
                    arrObjectEdit = new Array();
                    // console.log(jsonList);
                    for (var i = 0; i < jsonList.data.length; i++) {
                        ro_numbers.push(jsonList.data[i].RO_NUMBER);
                        var ro = '';
                        var o_id = '';
                        //oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);                 
                        xTable.fnAddData([jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus-ob"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);

                        var param = {
                            RO_NUMBER: jsonList.data[i].RO_NUMBER,
                            OBJECT_ID: jsonList.data[i].OBJECT_ID,
                            OBJECT_TYPE_ID: jsonList.data[i].OBJECT_TYPE_ID,
                            OBJECT_TYPE: jsonList.data[i].OBJECT_TYPE,
                            RO_NAME: jsonList.data[i].RO_NAME,
                            LAND_DIMENSION: jsonList.data[i].LAND_DIMENSION,
                            BUILDING_DIMENSION: jsonList.data[i].BUILDING_DIMENSION,
                            INDUSTRY: jsonList.data[i].INDUSTRY
                        }
                        arrObjectEdit.push(JSON.stringify(param));

                        var listItems = "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].RO_NUMBER + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>"
                        $("#condition-option").append(listItems);
                        //arrObjectEdit.push(jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY);

                        // untuk contract offer PERMOHONAN PERPANJANGAN 
                        ro = jsonList.data[i].RO_NUMBER
                        o_id = jsonList.data[i].OBJECT_ID


                    }
                    ////console.log('arr condition ' +arrCondition);

                    $.ajax({
                        type: "GET",
                        url: "/TransRentalRequest/GetDataProfitCenter?id=" + o_id,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {
                            setProfitCenter(data)
                        }
                    });
                }
            });

            var offer_id = data["CONTRACT_OFFER_NO"];

            //GET DATA CONDITION FROM OLD CONTRACT
            var tableEditCondition = $('#condition').DataTable();
            tableEditCondition.destroy();

            var tablex = $('#condition');
            var oTable = tablex.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "paging": false,
                "pageLength": 100000,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": false,
                "lengthChange": false,
                "bSort": false,
                "columnDefs": [
                    {
                        "targets": [20],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [20],
                        "visible": false
                    }
                ]
            });

            //GET DATA MANUAL FREQUENCY FROM OLD CONTRACT
            var tableEditManual = $('#manual-frequency').DataTable();
            tableEditManual.destroy();

            var tablem = $('#manual-frequency');
            var mTable = tablem.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 1000,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "order": [[0, "asc"]],
                "searching": false,
                "lengthChange": false,
                "bSort": false,
                "columnDefs": [
                    {
                        "targets": [8],
                        "visible": false
                    }
                ]
            });

            //GET DATA MEMO FROM OLD CONTRACT
            var tableEditMemo = $('#table-memo').DataTable();
            tableEditMemo.destroy();

            var tablem = $('#table-memo');
            var meTable = tablem.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 20,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": false,
                "lengthChange": false,
                "bSort": false
            });

            // Ajax untuk mengambil data vacant / booked
            var req_Occupancy = function (ro, nextLoad) {
                NEW_DUE_DATE = true;
                $.ajax({
                    type: "GET",
                    "url": "/RentalObject/GetDataOccupancy/" + ro + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var d = data.data;
                        var lastdata = d[d.length - 1];
                        if (lastdata.STATUS === 'VACANT' || lastdata.STATUS === 'BOOKED') {
                            // console.log(lastdata.VALID_FROM);

                            var mulai = lastdata.VALID_FROM.replace(/\./g, "/");
                            var tim = $('#TERM_IN_MONTHS').val();

                            //Set Date End
                            var dateMulai = mulai;
                            var datearrayMulai = dateMulai.split("/");
                            var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

                            //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
                            var f_date = new Date(newdateMulai);
                            var n_tim = Number(tim);
                            var end_date = addMonths(f_date, n_tim);
                            var xdate = new Date(end_date);
                            var f_date = xdate.toISOString().slice(0, 10);

                            //format f_date menjadi dd/mm/YYYY
                            var dateAkhir = f_date;
                            var datearrayAkhir = dateAkhir.split("-");
                            var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];
                            //$('#CONTRACT_END_DATE').val(newdateAkhir);


                            var datearray = dateMulai.split("/");
                            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                            $("#CONTRACT_START_DATE").val(dateMulai);
                            NEW_CONTRACT_START_DATE = dateMulai;
                            $("#CONTRACT_START_DATE").datepicker();
                            $("#CONTRACT_START_DATE").datepicker('setDate', new Date(newdate)); //set value

                            var datearrayA = newdateAkhir.split("/");
                            var newdateA = datearrayA[1] + '/' + datearrayA[0] + '/' + datearrayA[2];
                            $("#CONTRACT_END_DATE").val(newdateAkhir);
                            NEW_CONTRACT_END_DATE = newdateAkhir;
                            $("#CONTRACT_END_DATE").datepicker();
                            $("#CONTRACT_END_DATE").datepicker('setDate', new Date(newdateA)); //set value

                        } else {
                            //console.log('NOT VACANT');
                            //console.log(lastdata)
                            swal('Warning', 'Kontrak tidak dapat diperpanjang karena Object sedang tidak tersedia.\nOBJECT_STATUS: ' + lastdata.STATUS + '\nREASON: ' + lastdata.REASON, 'error');
                        }
                        nextLoad();
                    }
                });
            }

            // Ajax untuk menampilkan data condition sebelumnya
            var req_Condition = function () {
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataDetailConditionEdit/" + offer_id,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {

                        var tombol = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                        arrCondition = new Array();

                        oTable.fnClearTable();
                        var jsonList = data
                        var listItems = "";
                        var calcObject = "";
                        var res_condition = "";
                        var statistic = "";
                        var maskUnitPrice = "";
                        var maskTotal = "";
                        var maskNetValue = "";
                        var all_value = 0;
                        $('.DETAIL_RO').html('');
                        $('.DETAIL_NJOP_PERCENT').html('');

                        for (var i = 0; i < jsonList.data.length; i++) {
                            // CHANGE TO CONTRACT DATE: PERPANJANGAN
                            if ($('#CONTRACT_OFFER_TYPE').val() === '82') {

                                if (NEW_CONTRACT_START_DATE && NEW_CONTRACT_END_DATE) {
                                    jsonList.data[i].VALID_FROM = NEW_CONTRACT_START_DATE;
                                    jsonList.data[i].VALID_TO = NEW_CONTRACT_END_DATE;
                                }

                            }
                            //console.log(NEW_DUE_DATE)
                            if (NEW_DUE_DATE) {

                                //date Now
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!
                                var yyyy = today.getFullYear();

                                if (dd < 10) {
                                    dd = '0' + dd;
                                }

                                if (mm < 10) {
                                    mm = '0' + mm;
                                }

                                today = dd + '/' + mm + '/' + yyyy;
                                //console.log("con " + today)
                                jsonList.data[i].START_DUE_DATE = today;
                            }
                            if (jsonList.data[i].STATISTIC == 0) {
                                statistic = "No";
                            }
                            else {
                                statistic = "Yes";
                            }

                            maskUnitPrice = jsonList.data[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            maskTotal = jsonList.data[i].TOTAL.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            maskNetValue = jsonList.data[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            oTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].VALID_FROM, jsonList.data[i].VALID_TO, jsonList.data[i].MONTHS, statistic, jsonList.data[i].FORMULA, maskUnitPrice, jsonList.data[i].AMT_REF, jsonList.data[i].FREQUENCY, jsonList.data[i].START_DUE_DATE, jsonList.data[i].MANUAL_NO, jsonList.data[i].MEASUREMENT_TYPE, jsonList.data[i].LUAS, maskTotal, jsonList.data[i].NJOP_PERCENT, jsonList.data[i].KONDISI_TEKNIS_PERCENT, maskNetValue, jsonList.data[i].COA_PROD, tombol, jsonList.data[i].TAX_CODE]);
                            calcObject = jsonList.data[i].CALC_OBJECT;
                            res_condition = jsonList.data[i].CONDITION_TYPE;
                            var param = {
                                CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                VALID_FROM: jsonList.data[i].VALID_FROM,
                                VALID_TO: jsonList.data[i].VALID_TO,
                                MONTHS: jsonList.data[i].MONTHS,
                                STATISTIC: statistic,
                                UNIT_PRICE: maskUnitPrice.toString(),
                                AMT_REF: jsonList.data[i].AMT_REF,
                                FREQUENCY: jsonList.data[i].FREQUENCY,
                                START_DUE_DATE: jsonList.data[i].START_DUE_DATE,
                                MANUAL_NO: jsonList.data[i].MANUAL_NO,
                                FORMULA: jsonList.data[i].FORMULA,
                                MEASUREMENT_TYPE: jsonList.data[i].MEASUREMENT_TYPE,
                                LUAS: jsonList.data[i].LUAS,
                                TOTAL: maskTotal.toString(),
                                NJOP_PERCENT: jsonList.data[i].NJOP_PERCENT,
                                KONDISI_TEKNIS_PERCENT: jsonList.data[i].KONDISI_TEKNIS_PERCENT,
                                TOTAL_NET_VALUE: maskNetValue.toString(),
                                TAX_CODE: jsonList.data[i].TAX_CODE,
                                COA_PROD: jsonList.data[i].COA_PROD
                            }
                            arrCondition.push(JSON.stringify(param));

                            var arrCond = buah.push(calcObject + ' | ' + res_condition);
                            //console.log(arrCond);

                            var fruits = buah;

                            var net_val = maskNetValue;
                            //var price = maskUnitPrice;
                            var calc_ob = jsonList.data[i].CALC_OBJECT;
                            var detail_njop = jsonList.data[i].NJOP_PERCENT;

                            if (calc_ob != 'Header Document') {
                                all_value = all_value + parseFloat(net_val.split(',').join(''));
                                var ro_number = calc_ob.split(' ')[1];
                                var elems = $('#rental-object-co').find('tbody tr');
                                $(elems).each(function (index) {
                                    var x = $(this).find('td')[0];
                                    if ($(x).html() === ro_number) {
                                        //console.log(index);
                                        ro_data = $('#rental-object-co').DataTable().row(index).data();
                                        var luas = Number(ro_data[5]) + Number(ro_data[6]);

                                        var text = $('.DETAIL_RO').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CONTRACT_CURRENCY').val() + '</td>' + '<td>|</td><td>' + detail_njop + ' %</td>' + '</tr>';
                                        $('.DETAIL_RO').html(text);
                                        var text2 = $('.DETAIL_NJOP_PERCENT').html() + '<tr><td>' + detail_njop + '</td><td>';
                                        $('.DETAIL_NJOP_PERCENT').html(text2);
                                    }
                                });

                            }
                        }

                        $('#CONTRACT_VALUE').val(all_value);
                        $('.CONTRACT_VALUE').html(sep1000(all_value, false));

                        setTimeout(coHitungValue(), 1000);
                    }
                });
            }

            // Ajax untuk menampilkan data manual frequency sebelumnya
            var req_ManualRequest = function () {
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataDetailManualFrequencyEdit/" + offer_id,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {

                        //var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';
                        var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
                        var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

                        arrManualFrequency = new Array();

                        mTable.fnClearTable();
                        var jsonList = data;
                        var res_condition = "";
                        var maskNetValueFrequency = "";
                        var maskQuantityFrequency = "";
                        for (var i = 0; i < jsonList.data.length; i++) {

                            // CHANGE TO CONTRACT DATE: PERPANJANGAN
                            //if ($('#CONTRACT_OFFER_TYPE').val() === '82') {
                            if (NEW_DUE_DATE) {
                                jsonList.data[i].DUE_DATE = "";
                            }
                            //}
                            maskNetValueFrequency = jsonList.data[i].NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            maskQuantityFrequency = jsonList.data[i].QUANTITY.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            mTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct, jsonList.data[i].CONDITION]);

                            var param = {
                                MANUAL_NO: jsonList.data[i].MANUAL_NO,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                DUE_DATE: jsonList.data[i].DUE_DATE,
                                NET_VALUE: maskNetValueFrequency,
                                QUANTITY: maskQuantityFrequency,
                                UNIT: jsonList.data[i].UNIT,
                                CONDITION: jsonList.data[i].CONDITION
                            }
                            arrManualFrequency.push(JSON.stringify(param));
                        }
                    }
                });
            }

            // Ajax untuk menampilkan data memo sebelumnya
            var req_Memo = function () {
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataDetailMemoEdit/" + offer_id,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {

                        var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
                        var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                        arrMemo = new Array();
                        meTable.fnClearTable();
                        var jsonList = data;
                        var res_condition = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            meTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].MEMO, tombolAct]);

                            var param = {
                                CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                MEMO: jsonList.data[i].MEMO
                            }
                            arrMemo.push(JSON.stringify(param));
                        }
                        //console.log(arrMemo);
                    }
                });
            }

            req_RO.done(function () {
                if ($('#CONTRACT_OFFER_TYPE').val() === '82') {
                    req_Occupancy(ro_numbers[0], function () {
                        req_Condition();
                        req_ManualRequest();
                        req_Memo();
                    });
                } else {
                    NEW_DUE_DATE = true;
                    req_Condition();
                    req_ManualRequest();
                    req_Memo();
                }
            });
            $('#oldContractModal').modal('hide');
        });

    }

    // TAMBAHAN DARI REQUEST RENTAL
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;
                $.ajax({
                    type: "POST",
                    url: "/TransContractOffer/Customer",
                    //data: "{ MPLG_NAMA:'" + inp + "'}",
                    data: { 'MPLG_NAMA': inp },
                    //contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                event.preventDefault();
                //console.log(ui.item);
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
                //$("#CUSTOMER_ADDRESS").val(ui.item.address);
                cekPiut("1B", ui.item.sap);
            }
        });
    }

    var cekPiut = function (doc_type, cust_code) {
        var param = {
            DOC_TYPE: doc_type,
            CUST_CODE: cust_code
        };

        $.ajax({
            type: "POST",
            async: false,
            url: "/TransRentalRequest/cekPiutangSAP",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var anjunganRequest = function () {
        var id = $('#acceptID').val();
        if (id) {

            $.ajax({
                type: "GET",
                url: "/MonitoringAnjungan/GetHeader/" + id,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    $('#OLD_CONTRACT').val(data['CONTRACT_NO']);
                    //$("#DETAILCONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
                    //$("#DETAILRENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
                    $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_NAME"]);

                    var date = data["CONTRACT_START_DATE"].replace(/\./g, '/')
                    var datearray = date.split("/");
                    var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                    $("#CONTRACT_START_DATE").val(date);
                    $("#CONTRACT_START_DATE").datepicker();
                    $("#CONTRACT_START_DATE").datepicker('setDate', new Date(newdate)); //set value

                    var dateA = data["CONTRACT_END_DATE"].replace(/\./g, '/')
                    var datearrayA = dateA.split("/");
                    var newdateA = datearrayA[1] + '/' + datearrayA[0] + '/' + datearrayA[2];
                    $("#CONTRACT_END_DATE").val(dateA);
                    $("#CONTRACT_END_DATE").datepicker();
                    $("#CONTRACT_END_DATE").datepicker('setDate', new Date(newdateA)); //set value
                    //console.log(data);
                    $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
                    $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
                    $("#CUSTOMER_ID").val(data["CUSTOMER_ID"]);
                    $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
                    cekPiut("1B", data["CUSTOMER_AR"]);
                    var xPiut = $("#cPiut").val();
                    //console.log(xPiut);
                    $("#CONTRACT_OFFER_TYPE option:contains('" + data["CONTRACT_TYPE"] + "')").attr('selected', 'selected');
                    //$("#BUSINESS_ENTITY_ID option:contains('" + data["BE_ID"] + "')").attr('selected', 'selected');
                    //$("#PROFIT_CENTER option:contains('" + data["PROFIT_CENTER"] + "')").attr('selected', 'selected');
                    $("#USAGE_TYPE option:contains('" + data["CONTRACT_USAGE"] + "')").attr('selected', 'selected');
                    //$("#CONTRACT_CURRENCY option:contains('" + data["CURRENCY"] + "')").attr('selected', 'selected');
                }
            });

            $.ajax({
                type: "GET",
                url: "/MonitoringAnjungan/GetDataDetail/" + id + "/?draw=1&start=0&length=10&search%5Bvalue%5D=",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {

                    // Edit Data Table
                    if ($.fn.DataTable.isDataTable('#rental-object-co')) {
                        $('#rental-object-co').DataTable().destroy();
                    }

                    var tabler = $('#rental-object-co');
                    var xTable = tabler.dataTable({
                        "lengthMenu": [
                            [5, 15, 20, 50],
                            [5, 15, 20, 50]
                        ],
                        "pageLength": 5,
                        "language": {
                            "lengthMenu": " _MENU_ records"
                        },
                        "searching": false,
                        "lengthChange": false,
                        "columnDefs": [
                            {
                                "targets": [0, 2],
                                "class": "dt-body-center",
                                "visible": false
                            },
                            {
                                "targets": [1],
                                "class": "dt-body-center"
                            },
                            {
                                "targets": [5, 6],
                                "class": "dt-body-center"
                            },
                            {
                                "targets": [8],
                                "class": "dt-body-center",
                                "width": "10%"
                            },
                        ]
                    });

                    xTable.fnClearTable();
                    var jsonList = data;
                    arrObjectEdit = new Array();
                    //console.log(jsonList);
                    var ro_id = null;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        // ro_numbers.push(jsonList.data[i].RO_NUMBER);
                        var ro = '';
                        //oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);                 
                        xTable.fnAddData([jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus-ob"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);

                        var param = {
                            RO_NUMBER: jsonList.data[i].RO_NUMBER,
                            OBJECT_ID: jsonList.data[i].OBJECT_ID,
                            OBJECT_TYPE_ID: jsonList.data[i].OBJECT_TYPE_ID,
                            OBJECT_TYPE: jsonList.data[i].OBJECT_TYPE,
                            RO_NAME: jsonList.data[i].RO_NAME,
                            LAND_DIMENSION: jsonList.data[i].LAND_DIMENSION,
                            BUILDING_DIMENSION: jsonList.data[i].BUILDING_DIMENSION,
                            INDUSTRY: jsonList.data[i].INDUSTRY
                        }
                        arrObjectEdit.push(JSON.stringify(param));

                        var listItems = "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].RO_NUMBER + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>"
                        $("#condition-option").append(listItems);
                        //arrObjectEdit.push(jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY);

                        // untuk contract offer PERMOHONAN PERPANJANGAN 
                        ro_id = jsonList.data[i].OBJECT_ID
                    }
                    //console.log(ro_id)
                    if (ro_id) {
                        $.ajax({
                            type: "GET",
                            url: "/TransRentalRequest/GetDataProfitCenter?id=" + ro_id,
                            contentType: "application/json",
                            dataType: "json",
                            success: function (data) {
                                //console.log(data);
                                setProfitCenter(data)
                            }
                        });
                    }
                    ////console.log('arr condition ' +arrCondition);

                }
            });

        }
    }

    return {
        init: function () {
            unitPriceAuto();
            batal();
            hitung_bulan();
            hitung_tanggal_sampai();
            filter();
            filterRO();
            addCondition();
            manual_frequency();
            memo();
            salesRule();
            simpanData();
            reportingRule();
            datePicker();
            mdm();
            //cekPiut();
            ubahDropdown();
            cariOldContract();
            anjunganRequest();
            addSertifikat();
        }

    };

}();


// ************** FUNCTION UNTUK HANDLE SUMMARY CONTRACT BARU DAN LAMA ********************
function coHitungArea() {
    var con = $('#rental-object-co').dataTable();
    var all_area = 0;
    for (var i = 0; i < con.fnGetData().length; i++) {
        var area = con.fnGetData(i)[1];
        var area = con.fnGetData(i)[5];
        all_area = all_area + Number(area);
        //console.log(all_area);
    }
    //console.log(all_area);
    $('.LAND_AREA_NEW').html(all_area.toLocaleString('en-US'));
}

function coHitungValue() {
    var con = $('#condition').dataTable();
    var all_value = 0;
    //console.log(con.fnGetData());
    $('.DETAIL_RO_NEW').html('')
    $('.DETAIL_NJOP_PERCENT_NEW').html('')
    $.each(con.fnGetData(), function (i, fndata) {
        var net_val = con.fnGetData(i)[17];
        //var price = con.fnGetData(i)[7];
        var calc_ob = con.fnGetData(i)[0];
        var detail_njop = con.fnGetData(i)[15];
        //console.log(net_val, calc_ob, detail_njop);
        if (calc_ob != 'Header Document') {
            all_value = all_value + parseFloat(net_val.split(',').join(''));
            var ro_number = calc_ob.split(' ')[1];
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    //console.log(index);
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                    var luas = Number(ro_data[5]) + Number(ro_data[6]);
                    var text = $('.DETAIL_RO_NEW').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CONTRACT_CURRENCY').val() + '</td>' + '<td>|</td><td>' + detail_njop + ' %</td>' + '</tr>';
                    $('.DETAIL_RO_NEW').html(text)
                    //console.log(text);
                    var text2 = $('.DETAIL_NJOP_PERCENT_NEW').html() + '<tr>' + detail_njop ? '<td>' + detail_njop + '</td>' : '' + '</tr>';
                    $('.DETAIL_NJOP_PERCENT_NEW').html(text2)
                    console.log(text2);
                }
            });
        }
    });
    //console.log(text);
    $('.CONTRACT_VALUE_NEW').html(all_value.toLocaleString('en-US'));
}

function coHitungPeriod() {
    var value = $('#TERM_IN_MONTHS').val();
    var start = $('#CONTRACT_START_DATE').val().replace(/\//g, ".");
    var end = $('#CONTRACT_END_DATE').val().replace(/\//g, ".");
    $('.CONTRACT_START_NEW').html(start);
    $('.CONTRACT_END_NEW').html(end);
    $('.CONTRACT_PERIOD_NEW').html(value);
}

// ************** FUNCTION UNTUK HANDLE SALES RULE ********************
//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getUnitST(this_cmb) {
    var selectedSalesType = $(this_cmb).val();
    var splitselectedSalesType = selectedSalesType.split(" - ");
    var id_sales_type = splitselectedSalesType[0];
    var nama_sales_type = splitselectedSalesType[1];

    //Ajax untuk get UNIT
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataUnitST/" + id_sales_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].UNIT;
            }
            $(this_cmb).parents('tr').find('input[name="UNIT_ST"]').val(listItems);
        }
    });
}
// ************** END FUNCTION UNTUK HANDLE SALES RULE ********************

//Function untuk isi condition code
function getConditionCode(this_cmb) {
    var selectedCondition = $(this_cmb).val();

    if (selectedCondition) {
        var splitselectedCondition = selectedCondition.split(" | ");
        var code_lengkap = splitselectedCondition[1];
        var cut = code_lengkap.substring(0, 4);
        //console.log(selectedCondition);
        //console.log(code_lengkap);
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val(cut);
    }
    else {
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val("");
    }
}

//Function untuk add row table manual frequency 
function addManualFreq(this_x) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var tim = data[4];
}

//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getLuas(this_cmb) {
    //console.log($(this_cmb).val());
    var selectedMeasType = $(this_cmb).val();
    var splitselectedMeasType = selectedMeasType.split("|");
    var id_detail_meas_type = splitselectedMeasType[1];

    //Ajax untuk get Luas(M2)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataLuasMeasType/" + id_detail_meas_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].LUAS;
            }
            // $("#luas").val(listItems);
            $(this_cmb).parents('tr').find('input[name="luas"]').val(listItems);


            //Mengisi field total value saat field luas terisi dengan value ajax listItems
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            //formulanya
            var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
            if (formula == 'A') {

            }
            else
                if (formula == 'U') {

                }
                else {
                    //var tim = data[4];
                    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
                    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
                    var luas = listItems;
                    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

                    nMUnitPrice = parseFloat(unitPrice.split(',').join(''));
                    var t = hitungTotal(tim, luas, nMUnitPrice, period);
                    var fT = Math.round(t).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

                    //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
                    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
                    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
                    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
                    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

                    var nfTotal = parseFloat(total.split(',').join(''));
                    var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
                    var nfPersenFromKondisiteknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));


                    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiteknis);
                    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    //console.log(fNetValue);
                    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
                }
        }
    });
}


//---------------------------HITUNG TOTAL----------------------------
//ACTION UNTUK MENGISI TOTAL VALUE SAAT CLICK COMBO AMT REF
function isiTotalAMTREF(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var period = $(this_cmb).val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);

    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        //Hitung Total Untuk Formula D
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(unit_price.split(',').join(''));
        var rTotal = hitungTotal(tim, luas, nfHarga, kode_periode);

        var fTotal = rTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fTotal);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = netVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}

function isiTotalUnitPrice(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = Math.round(unit_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        var fT = t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

        var njop;
        var kondisi_teknis;

        // update total
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        //console.log('total bulan ' + total_bulan);
        //console.log('nf harga ' + nfHarga);
        //console.log('luas ' + luas);
        //console.log('kode periode ' + kode_periode);

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //console.log('formula ' + formula);
        //console.log('njop ' + njop);
        //console.log('kondisi teknis ' + persenFromKondisiTeknis);

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}

//---------------------END OF HITUNG TOTAL----------------------------
function hitungTotal(total_bulan, luas, harga, kode_periode) {
    var total = 0;
    if (typeof harga == "string") {
        harga = harga.replace(/,/g, "");
    }
    if (kode_periode == 'YEARLY') {
        var year = Number(total_bulan) / 12;
        total = (Number(luas) * Number(harga)) * Number(year);
    }
    else if (kode_periode == 'MONTHLY') {
        total = (Number(luas) * Number(harga)) * Number(total_bulan);
    }
    else {
        total = Number(luas) * Number(harga);
    }
    return total;
}

//Action Saat Formula Dipilih
function hitungFormula(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
    var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();

    var amt_ref = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));
    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    if (formula == 'A') {
        //var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        // $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        // $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'D') {
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //Formula Lama
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);

        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
    }
}

//Action Saat Formula sudah dipilih dan mengisi persentase njop
function hitungFormulaN(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    //var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
    var nfPersenFromNjop = persen_from_njop;
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    //console.log('net value ' + net_value);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}

//Action Saat Formula sudah dipilih dan mengisi persentase kondisi teknis
function hitungFormulaT(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = persen_from_njop;
    //var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // ini masih kurang sip
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}



//Function untuk hitung net value berdasarkan formula yang dipilih
function netValue(formula, total, njop, kondisi_teknis) {
    var totalNetValue = 0;
    if (typeof total == "string") {
        total = total.replace(/,/g, "");
    }
    if (formula == 'D') {
        totalNetValue = Number(total);
    }
    else if (formula == 'E1') {
        if (njop == '' && kondisi_teknis == '') {
            totalNetValue = Number(total);
        }
        else if (njop == '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100);
        }
        else if (kondisi_teknis == '') {
            totalNetValue = Number(total) * (Number(njop) / 100);
        }
        else if (njop != '' && kondisi_teknis != '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100) * (Number(njop) / 100);
        }
        else {

        }
    }
    else if (formula == 'A') {
        totalNetValue = Number(total);
    }
    else {

    }

    return totalNetValue;
}

// Function untuk remove array value berdasarkan value
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function hitungTermMonths(this_cmb) {
    //if ($(this_cmb).not(':focus')) return;
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val();

    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

    var dateAkhir = t;
    var datearrayAkhir = dateAkhir.split("/");
    var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

    var tim = monthDiff2(new Date(newdateMulai), new Date(newdateAkhir));

    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val(tim);
    hitungFormula(this_cmb);
    //// handle perhitungannya
    //var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    //var tim2 = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    //var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    //var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    //var tx = hitungTotal(tim2, luas, unitPrice, period);
    //$(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    ////ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    //var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    //var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    //var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    //var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();
    ////console.log(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function hitungEndDate(this_cmb) {
    //if ($(this_cmb).not(':focus')) return;
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

    //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
    var f_date = new Date(newdateMulai);
    var n_tim = Number(tm);
    var end_date = addMonths2(f_date, n_tim);
    var xdate = new Date(end_date);
    var f_date = xdate.toISOString().slice(0, 10);

    //format f_date menjadi dd/mm/YYYY
    var dateAkhir = f_date;
    var datearrayAkhir = dateAkhir.split("-");
    var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];

    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val(newdateAkhir);
    hitungFormula(this_cmb);
    //// handle perhitungannya
    //var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    //var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    //var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    //var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    //var tx = hitungTotal(tim, luas, unitPrice, period);
    //$(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    ////ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    //var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    //var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    //var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    //var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    //var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function monthDiff2(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();

    if (d2.getDate() >= d1.getDate())
        months++

    return months <= 0 ? 0 : months;
}

function addMonths2(date, count) {
    if (date && count) {
        var m, d = (date = new Date(+date)).getDate()

        date.setMonth(date.getMonth() + count, 1)
        m = date.getMonth()
        date.setDate(d)
        if (date.getMonth() !== m) date.setDate(0)
    }
    return date
}


// Function untuk handle save Sales Rule (Formula U)
function saveSalesRule() {
    // Untuk get data table condition

    // Untuk ambil sales rule

    // 
}

function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}

//Function untuk isi condition code
jQuery(document).ready(function () {
    TableDatatablesEditable.init();
    $('#condition > tbody').html('');
    $('#condition-option > option').html('');
    $('#manual-frequency > tbody').html('');
    $('#table-memo > tbody').html('');
    $('#rental-object-co > tbody').html('');
    $('#sales-based > tbody').html('');
    $('#reporting-rule > tbody').html('');
    $('.sembunyi').hide();
    DataSurat.init();
});
function infoIsiManual() {
    var valFrequency = $('#frequency').val();

    if (valFrequency == 'MANUAL') {
        swal('Info', 'Mohon Mengisi \'MANUAL NO\' Untuk Pilihan Manual!', 'info');
    }
    else {

    }
}


function check() {
    var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
    var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();

    if (CONTRACT_START_DATE == "" && CONTRACT_END_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_START_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_END_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else {
        document.getElementById("btn-detil-filter").href = "#addDetailModal";
    }

    $('#from').val(CONTRACT_START_DATE);
    $('#to').val(CONTRACT_END_DATE);
}

// DARI EDIT TRANS CO

//Function untuk isi condition code
function getCoaProduksi2(coa_prod, this_cmb) {
    //console.log(this_cmb)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(this_cmb).find(".coa_prod").html('');
            $(this_cmb).find(".coa_prod").append(listItems);
            $(this_cmb).find(".coa_prod option[value='" + coa_prod + "']").prop('selected', true);
        }
    });

    //$("#coa_prod").select2({
    //    allowClear: true,
    //    width: '100%',
    //    ajax: {
    //        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
    //        dataType: "json",
    //        contentType: "application/json",
    //        data: function (params) {
    //            return {
    //                search: params.term,
    //                page: params.page,
    //            }
    //        },
    //        processResults: function (result) {
    //            return {
    //                results: $.map(result.data, function (item) {
    //                    return {
    //                        text: item.REF_DESC,
    //                        data: item,
    //                        id: item.VAL1
    //                    }
    //                })
    //            };
    //        }
    //    },
    //    escapeMarkup: function (markup) { return markup; },
    //    minimumInputLength: 3,
    //    placeholder: "-- Choose COA Production --",
    //});
    //var $newOption = $("<option selected='selected'></option>").val(coa_prod).text(coa_prod);
    //$("#coa_prod").append($newOption).trigger('change');
}

function getCoaProduksi() {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            function isEmpty(el) {
                return !$.trim(el.html())
            }
            $(".coa_prod").each(function (i, obj) {
                if (isEmpty($(obj))) {
                    $(obj).append(listItems);
                }
            });
        }
    });

    //console.log(222);
    //$("#coa_prod").select2({
    //    allowClear: true,
    //    width: '100%',
    //    ajax: {
    //        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
    //        dataType: "json",
    //        contentType: "application/json",
    //        data: function (params) {
    //            return {
    //                search: params.term,
    //                page: params.page,
    //            }
    //        },
    //        processResults: function (result) {
    //            return {
    //                results: $.map(result.data, function (item) {
    //                    return {
    //                        text: item.REF_DESC,
    //                        data: item,
    //                        id: item.VAL1
    //                    }
    //                })
    //            };
    //        }
    //    },
    //    escapeMarkup: function (markup) { return markup; },
    //    minimumInputLength: 3,
    //    placeholder: "-- Choose COA Production --",
    //});
}


