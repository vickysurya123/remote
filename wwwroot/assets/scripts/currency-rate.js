﻿var CurrencyRate = function () {
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var CBCurrency = function () {
        $("#CURRENCY_FROM").select2({
            allowClear: true,
            width: '100%',
            dropdownParent: $("#addapvsetModal"),
            ajax: {
                url: "/CurrencyRate/GetDataCurrency",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    console.log(result);
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.CODE
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
            placeholder: "Currency From"
        }),
            $("#CURRENCY_TO").select2({
                allowClear: true,
                width: '100%',
                dropdownParent: $("#addapvsetModal"),
                ajax: {
                    url: "/CurrencyRate/GetDataCurrency",
                    dataType: "json",
                    contentType: "application/json",
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page,
                        }
                    },

                    processResults: function (result) {
                        console.log(result);
                        var data = {
                            results: $.map(result.results, function (item) {
                                return {
                                    id: item.ID,
                                    text: item.CODE
                                };
                            }),
                        };
                        return data;
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 2,
                placeholder: "Currency To"
            })
    };

    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/CurrencyRate/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "CODE_X",
                    "class": "dt-body-center",
                },
                {
                    "data": "CODE_Y",
                    "class": "dt-body-center",
                },
                {
                    "data": "VALIDITY_FROM",
                    "class": "dt-body-center",
                },
                {
                    "data": "VALIDTY_TO",
                    "class": "dt-body-center",
                },
                {
                    "data": "MULTIPLY_RATE",
                    "class": "dt-body-center",
                },
                {
                    "data": "DEVIDE_RATE",
                    "class": "dt-body-center",
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-view" class="btn btn-icon-only green" title="View" ><i class="fa fa-eye"></i></a>' +
                            '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var ID = parseInt($('#param_id').val());
            var CURRENCY_FROM = parseInt($('#CURRENCY_FROM').val());
            var CURRENCY_TO = parseInt($('#CURRENCY_TO').val());
            var MULTIPLY_RATE = parseInt($('#MULTIPLY_RATE').val());
            var DEVIDE_RATE = parseInt($('#DEVIDE_RATE').val());
            var VALIDTY_TO = $('#VALIDTY_TO').val();
            var VALIDITY_FROM = $('#VALIDITY_FROM').val();
            

            if (CURRENCY_FROM !== "" && CURRENCY_TO !== "" && MULTIPLY_RATE !== "" && DEVIDE_RATE !== "" && VALIDITY_FROM !== "" && VALIDTY_TO !== "") {
                var param = {
                    ID: ID,
                    CURRENCY_FROM: CURRENCY_FROM,
                    CURRENCY_TO: CURRENCY_TO,
                    MULTIPLY_RATE: MULTIPLY_RATE,
                    DEVIDE_RATE:DEVIDE_RATE,
                    VALIDITY_FROM: VALIDITY_FROM,
                    VALIDTY_TO: VALIDTY_TO
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/CurrencyRate/Insert",
                    timeout: 30000
                });

                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    var clear = function () {
        $('input').val('');
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        $('#txt-judul').text('Edit');
        $('#addapvsetModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        $('#VALIDITY_FROM').val(aData.VALIDITY_FROM);
        $('#VALIDTY_TO').val(aData.VALIDTY_TO);
        $('#MULTIPLY_RATE').val(aData.MULTIPLY_RATE);
        $('#DEVIDE_RATE').val(aData.DEVIDE_RATE);
        $('#param_id').val(aData.ID);
        var newOption = new Option(aData.CODE_X, aData.CODE_X, false, false);
        $('#CURRENCY_FROM').append(newOption).trigger('change');
        var newOptionn = new Option(aData.CODE_Y, aData.CODE_Y, false, false);
        $('#CURRENCY_TO').append(newOptionn).trigger('change');
    });

    $('body').on('click', 'tr #btn-view', function () {
        $('#txt-judul').text('View Currency Rate');
        $('#detailModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        console.log(aData);

        $('#VALIDITY_FROMM').val(aData.VALIDITY_FROM);
        $('#VALIDTY_TOO').val(aData.VALIDTY_TO);
        $('#MULTIPLY_RATEE').val(aData.MULTIPLY_RATE);
        $('#DEVIDE_RATEE').val(aData.DEVIDE_RATE);
        $('#param_idd').val(aData.ID);
        var newOption = new Option(aData.CODE_X, aData.CODE_X, false, false);
        $('#CURRENCY_FROMM').append(newOption).trigger('change');
        var newOptionn = new Option(aData.CODE_Y, aData.CODE_Y, false, false);
        $('#CURRENCY_TOO').append(newOptionn).trigger('change');
    });

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID)
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Currency Rate " + aData.ID + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/CurrencyRate/Delete",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('#btn-bataladd').click(function () {
        clear();
        $('#CURRENCY_FROM').select2("val", "");
    });
    $('#btn-add').click(function () {
        clear();
        $('#txt-judul').text('Master Data  Currency Rate');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            datePicker();
            CBCurrency();
            //simpanEdit();
            

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        CurrencyRate.init();
    });
}