﻿var ApprovalSetting = function () {
    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/Parameter/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "PARAMETER_NAME"
                },
                {
                    "data": "PARAMETER_TABLE"
                },
                {
                    "data": "PARAMETER_COLUMN"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var ID = parseInt($('#param_id').val());
            var PARAMETER_NAME = $('#PARAMETER_NAME').val();
            var PARAMETER_TABLE = $('#PARAMETER_TABLE').val();
            var PARAMETER_COLUMN = $('#PARAMETER_COLUMN').val();
            if (PARAMETER_NAME !== "" && PARAMETER_TABLE !== "" && PARAMETER_COLUMN !== "") {
                var param = {
                    ID: ID,
                    PARAMETER_NAME: PARAMETER_NAME,
                    PARAMETER_TABLE : PARAMETER_TABLE,
                    PARAMETER_COLUMN: PARAMETER_COLUMN

                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    dataType : "json",
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Parameter/Insert",
                    timeout: 30000
                });

                console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    //var simpanEdit = function () {
    //    $('#btn-simpanadd').click(function () {
    //        var table = $('#table-apvset').DataTable();
    //        var ID = $('#param_id').val();
    //        var PARAMETER_NAME = $('#PARAMETER_NAME').val();
    //        var PARAMETER_TABLE = $('#PARAMETER_TABLE').val();
    //        var PARAMETER_COLUMN = $('#PARAMETER_COLUMN').val();
    //        if (PARAMETER_NAME !== "" && PARAMETER_TABLE !== "" && PARAMETER_COLUMN !== "") {
    //            var param = {
    //                ID: ID,
    //                PARAMETER_NAME: PARAMETER_NAME,
    //                PARAMETER_TABLE: PARAMETER_TABLE,
    //                PARAMETER_COLUMN: PARAMETER_COLUMN,

    //            };
    //            App.blockUI({ boxed: true });
    //            var req = $.ajax({
    //                contentType: "application/json",
    //                data: JSON.stringify(param),
    //                method: "POST",
    //                url: "/Parameter/Edit",
    //                timeout: 30000
    //            });
    //            req.done(function (data) {
    //                App.unblockUI();
    //                if (data.Status === 'S') {
    //                    swal('Success', data.Msg, 'success').then(function (isConfirm) {
    //                        //window.location.href = '/Role';
    //                        clear();
    //                        table.ajax.reload();
    //                    });
    //                } else {
    //                    swal('Failed', data.Msg, 'error').then(function (isConfirm) {
    //                        //window.location.href = '/Role';
    //                        clear();
    //                        table.ajax.reload();
    //                    });
    //                }
    //            });
    //        } else {
    //            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
    //        }
    //        $('#addapvsetModal').modal('hide');
    //    });
    //}

    var clear = function () {
        $('input').val('');
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        $('#txt-judul').text('Edit');
        $('#addapvsetModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        console.log(aData);
        $('#PARAMETER_NAME').val(aData.PARAMETER_NAME);
        $('#PARAMETER_TABLE').val(aData.PARAMETER_TABLE);
        $('#PARAMETER_COLUMN').val(aData.PARAMETER_COLUMN);
        $('#param_id').val(aData.ID);



    });
    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID)
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Approval Setting Parameter  " + aData.PARAMETER_NAME + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Parameter/Delete",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('#btn-bataladd').click(function () {
        clear();
    });
    $('#btn-add').click(function () {
        clear();
        $('#txt-judul').text('Add');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            //simpanEdit();

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ApprovalSetting.init();
    });
}