﻿var ReportTransOTherServices = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportContractOffer/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBOfferStatus();
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var CBOfferStatus = function () {
        $.ajax({
            type: "GET",
            url: "/ReportContractOffer/GetDataOfferStatus",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Offer Status --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].ATTRIB1 + "</option>";
                }
                $("#OFFER_STATUS").html('');
                $("#OFFER_STATUS").append(listItems);
                CBOfferType();
            }
        });
    }

    var CBOfferType = function () {
        $.ajax({
            type: "GET",
            url: "/ReportContractOffer/GetDataOfferType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Offer Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#OFFER_TYPE").html('');
                $("#OFFER_TYPE").append(listItems);
                select2ProfitCenter();
                handleBootstrapSelect();
            }
        });
    }

    var CBRentalRequestType = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportRentalRequest/GetDataRentalRequestType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].DESCRIPTION + "</option>";
                }
                $("#RENTAL_REQUEST_TYPE").html('');
                $("#RENTAL_REQUEST_TYPE").append(listItems);
            }
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-trans-os');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            var OFFER_TYPE = $('#PROFIT_CENTER').val();

            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                $('#table-report-trans-os').DataTable().destroy();
            }

            $('#table-report-trans-os').DataTable({
                "ajax": {
                    "url": "ReportContractOffer/GetDataFilternew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.customer_id = $('#CUSTOMER_ID').val();
                        data.offer_status = $('#OFFER_STATUS').val();
                        data.offer_type = $('#OFFER_TYPE').val();
                        data.rental_request_no = $('#RENTAL_REQUEST_NO').val();
                        data.contract_offer_no = $('#CONTRACT_OFFER_NO').val();
                        data.valid_from = $('#VALID_FROM').val();
                        data.valid_to = $('#VALID_TO').val();
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [

                    {
                        "data": "CONTRACT_OFFER_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_BY",
                    },
                    {
                        "data": "RENTAL_REQUEST_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OFFER_TYPE",
                    },
                    {
                        "data": "CONTRACT_OFFER_NAME"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME"
                    },
                    {
                        "data": "CUSTOMER_AR"
                    },
                    {
                        "data": "PROFIT_CENTER"
                    },
                    {
                        "data": "CONTRACT_USAGE"
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "INDUSTRY"
                    },
                    {
                        "data": "LAND_DIMENSION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BUILDING_DIMENSION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-right",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CONDITION_TYPE"
                    },
                    {
                        "data": "CONTRACT_OFFER_STATUS"
                    }
                    //},
                    //{
                    //    "render": function (data, type, full) {

                    //        return '<span class="label label-sm label-success">BILLED</span>';

                    //    },
                    //    "class": "dt-body-center"
                    //}
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });


        });
    }

    var mdm = function () {
        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportContractOffer/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {

            if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                $('#table-report-trans-os').DataTable().destroy();
            }

            $('#table-report-trans-os').DataTable({
                "dom": "Bfrtip",
                buttons: [
                    {
                        className: 'btn sbold blue',
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                buttons: [
                    {
                        className: 'btn sbold blue',
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                "ajax": {
                    "url": "/ReportTransOtherService/ShowAllTwo",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "columns": [
                    {
                        "data": "BE_NAME",
                    },
                    {
                        "data": "PROFIT_CENTER_NAME",
                    },
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POSTING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "COSTUMER_MDM_NAME",
                    },
                    {
                        "data": "CUSTOMER_SAP_AR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SERVICES_GROUP_NAME"
                    },
                    {
                        "data": "SERVICE_NAME"
                    },
                    {
                        "data": "QUANTITY"
                    },
                    {
                        "data": "UNIT"
                    },
                    {
                        "data": "PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "SAP_DOCUMENT_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "GL_ACCOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {

                            return '<span class="label label-sm label-success">BILLED</span>';

                        },
                        "class": "dt-body-center"
                    }
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            showAll();
            filter();
            mdm();
            reset();
            CBBusinessEntity();
            //CBRentalRequestType();
            //CBOfferStatus();
            //CBOfferType();
            //select2ProfitCenter();
            //CBProfitCenter();
        }
    };
}();

function CBProfitCenter(BE_ID) {
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportContractOffer/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
            }
        });
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportContractOffer/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
            }
        });
    }
}

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
    var OFFER_STATUS = $('#OFFER_STATUS').val();
    var OFFER_TYPE = $('#OFFER_TYPE').val();
    var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
    var CONTRACT_OFFER_NO = $('#CONTRACT_OFFER_NO').val();
    var VALID_FROM = $('#VALID_FROM').val();
    var VALID_TO = $('#VALID_TO').val();

    var param =  {
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        PROFIT_CENTER: PROFIT_CENTER
        , CUSTOMER_ID: CUSTOMER_ID,
        OFFER_STATUS: OFFER_STATUS
        , OFFER_TYPE: OFFER_TYPE,
        RENTAL_REQUEST_NO: RENTAL_REQUEST_NO
        , CONTRACT_OFFER_NO: CONTRACT_OFFER_NO,
        VALID_FROM: VALID_FROM
        , VALID_TO: VALID_TO
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportContractOffer/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportContractOffer/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportTransOTherServices.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-trans-os > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}