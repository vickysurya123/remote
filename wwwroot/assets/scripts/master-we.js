﻿var MasterWE = function () {

    var initTableRentalObject = function () {
        $('#table-we').DataTable({
            "ajax": {
                "url": "WaterAndElectricity/GetDataWaterAndElectricity",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "TARIFF_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "NAMA_PROFIT_CENTER",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION",
                },
                {
                    "data": "AMOUNT",
                    "class": "dt-body-center",
                },
                {
                    "data": "FALID_FROM",
                    "class": "dt-body-center"
                },
                {
                    "data": "FALID_TO",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        //Edit untuk testing pertama kali (sesuai db rental object)
                        var aksi = '<a class="btn btn-icon-only blue" id="btn-edit"><i class="fa fa-edit"></i></a>';
                        aksi += '<a class="btn btn-icon-only blue" id="btn-ubah-we"><i class="fa fa-exchange"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                        //aksi += '<a class="btn btn-icon-only red" id="btn-hapus"><i class="fa fa-close"></i></a>';

                        /*
                        if (full.KD_AKTIF === '1') {
                            aksi += '<a class="btn default btn-xs red" id="btn-status"><i class="fa fa-close"></i> Non-aktifkan</a>';
                        } else {
                            aksi += '<a class="btn default btn-xs blue" id="btn-status"><i class="fa fa-check-square"></i> Aktifkan</a>';
                        }
                        */
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    // ubah untuk testing (sesuai dengan field di database)
    var edit = function () {
        $('body').on('click', 'tr #btn-edit', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-we').DataTable();
            var data = table.row(baris).data();

            window.location = "/WaterAndElectricity/Edit/" + data["TARIFF_CODE"];
        });
    }

    // Detail water and electricity
    var detailwe = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-we').DataTable();
            var data = table.row(baris).data();

            $("#tariff_code").val(data["TARIFF_CODE"]);
            $("#installation_type").val(data["INSTALLATION_TYPE"]);
            $("#description").val(data["DESCRIPTION"]);
            $("#amount").val(data["AMOUNT"]);
            $("#currency").val(data["CURRENCY"]);
            $("#per").val(data["PER"]);
            $("#unit").val(data["UNIT"]);
            $("#x_factor").val(data["X_FACTOR"]);
            $("#valid_from").val(data["FALID_FROM"]);
            $("#valid_to").val(data["FALID_TO"]);
            $("#status").val(data["ACTIVE"]);
            $("#profit_center").val(data["PROFIT_CENTER"]);

            $("#stat").val(data["STAT"]);

        });
    }

    // Ubah sesuai dengan mockup
    var ubahwe = function () {
        $('body').on('click', 'tr #btn-ubah-we', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-we').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["SERVICES_ID"],
                        method: "get",
                        url: "WaterAndElectricity/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var deletedata = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-we').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["SERVICES_ID"],
                        method: "get",
                        url: "WaterAndElectricity/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var status = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Apakah Anda yakin?',
                text: "Status aktif tarif akan diubah.",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["RO_NUMBER"],
                        method: "get",
                        url: "MasterTarif/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Berhasil', data.message, 'success');
                        } else {
                            swal('Gagal', data.message, 'error');
                        }
                    });
                }
            });
        });
    }


    var add = function () {
        $('#btn-add-we').click(function () {
            window.location.href = '/WaterAndElectricity/AddWE';
        });
    }

    return {
        init: function () {
            initTableRentalObject();
            status();
            deletedata();
            add();
            ubahwe();
            detailwe();
            edit();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterWE.init();
    });
}