﻿var TransWaterElectricity = function () {
    //var statuswater = -1;
    //var statuselectricity = -1;
    function cekRolePostingWater() {
        $.ajax({
            type: "GET",
            url: "/SettingPosting/GetStatusWater",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var arrayRole = [];
                $.each(data, function (index, value) {
                    arrayRole.push(value[0]['Value']);
                });
                statuswater = jQuery.inArray("2-Air", arrayRole);
                console.log(statuswater);
                return statuswater;
            }
        });
    }
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var initTableWETransaction = function () {
        $('#table-transwe').DataTable({

            "ajax":
            {
                "url": "TransWEBilling/GetDataWETransBillingNew",
                "type": "GET",
            },

            "columns": [
                /*
                {
                    "render": function(data, type, full){
                        var check = "<label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'><input type='checkbox' class='checkboxes' value='1' /><span></span></label>";
                        return check;
                    },
                    "class": "dt-body-center"

                        
                //},*/
                //{
                //    "render": function () {
                //        var check = '<input type="checkbox" id="pilih-gelondongan" name="pilih-gelondongan" />';
                //        return check;
                //    },
                //    "class": "dt-body-center"
                //},
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE"

                },
                {
                    "data": "PROFIT_CENTER"

                },
                {
                    "data": "CUSTOMER_NAME"

                },
                {
                    "data": "POWER_CAPACITY"

                },
                {
                    "data": "PERIOD",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return '<span> USD </span>';
                        } else {
                            return '<span> IDR </span>';
                        }
                    },
                    "class": "dt-body-center",
                },
                {
                    "data": "AMOUNT",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return full.TOTAL_USD;
                        } else {
                            return " - ";
                        }
                    },
                    "class": "dt-body-right",
                    //render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS_DINAS === null) {
                            return '<span> - </span>';
                        } else {
                            return '<span> ' + full.STATUS_DINAS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    //"render": function (data, type, full) {
                    //    var aksi = '';
                    //    if (full.INSTALLATION_TYPE == 'L-Sambungan Listrik') {
                    //        aksi += '<a data-toggle="modal" href="#viewModalListrik" class="btn btn-icon-only green" id="btn-detail-listrik"><i class="fa fa-eye"></i></a>';

                    //    }
                    //    else {
                    //        aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                    //    }
                    //   }

                    "render": function (data, type, full) {
                        //var aksi = '';
                        var aksi = '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                        //if ((full.INSTALLATION_TYPE == 'L-Sambungan Listrik' && full.ROLE_ELECTRICITY > 0) || (full.INSTALLATION_TYPE == 'A-Sambungan Air' && full.ROLE_WATER > 0)) {
                        //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                        //}
                        console.log(full);
                        if (full.INSTALLATION_TYPE == 'L-Sambungan Listrik') {
                            //if (full.ROLE_ELECTRICITY != '0') {
                            //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            //}
                            //aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewModalListrik" class="btn btn-icon-only green" id="btn-detail-listrik" title="View Detail"><i class="fa fa-eye"></i></a>';
                            aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            //if (full.ROLE_WATER != '0') {
                            //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            //}
                            //aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail" title="View Detail"><i class="fa fa-eye"></i></a>';
                            aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';
                        }
                        return aksi;
                        },
                        "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });

    }

    var initTableWETransactionDinas = function () {
        $('#table-transwe-dinas').DataTable({

            "ajax":
            {
                "url": "TransWEBilling/GetDataWETransBillingDinasNew",
                "type": "GET",

            },

            "columns": [
                /*
                {
                    "render": function(data, type, full){
                        var check = "<label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'><input type='checkbox' class='checkboxes' value='1' /><span></span></label>";
                        return check;
                    },
                    "class": "dt-body-center"

                        
                //},*/
                //{
                //    "render": function () {
                //        var check = '<input type="checkbox" id="pilih-gelondongan" name="pilih-gelondongan" />';
                //        return check;
                //    },
                //    "class": "dt-body-center"
                //},
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE"

                },
                {
                    "data": "PROFIT_CENTER"

                },
                {
                    "data": "CUSTOMER_NAME"

                },
                {
                    "data": "POWER_CAPACITY"

                },
                {
                    "data": "PERIOD",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return '<span> USD </span>';
                        } else {
                            return '<span> IDR </span>';
                        }
                    },
                    "class": "dt-body-center",
                },
                {
                    "data": "AMOUNT",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return full.TOTAL_USD;
                        } else {
                            return " - ";
                        }
                    },
                    "class": "dt-body-right",
                    //render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS_DINAS === null) {
                            return '<span> - </span>';
                        } else {
                            return '<span> ' + full.STATUS_DINAS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing-dinas"><i class="fa fa-paper-plane-o"></i></a>';
                        //var aksi = "";

                        if (full.INSTALLATION_TYPE == 'L-Sambungan Listrik') {
                            //if (full.ROLE_ELECTRICITY != '0') {
                            //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing-dinas" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            //}
                            aksi += '<a data-toggle="modal" href="#viewModalListrik" class="btn btn-icon-only green" id="btn-detail-listrik-dinas"><i class="fa fa-eye"></i></a>';
                            aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota-dinas" title="Pranota"><i class="fa fa-print"></i></a>';

                        }
                        else {
                            //if (full.ROLE_WATER != '0') {
                            //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing-dinas" title = "Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            //}
                            aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail-dinas"><i class="fa fa-eye"></i></a>';
                            aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota-dinas" title="Pranota"><i class="fa fa-print"></i></a>';

                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });

    }

    var initDetilTransaction = function () { //pop up
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();
            var tableDetilTransaction = $('table-detil-transaction').DataTable();
            tableDetilTransaction.destroy();
            
            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            //16 items
            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "type": "GET",
                    "url": "/TransWEBilling/GetDataTransaction",
                    data: function(data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    //cache: false,

                },

                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "F_REPORT_ON",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PRICE",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SUB_TOTAL",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BILLING_TYPE",
                        "class": "dt-body-center"

                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,


                "filter": false
            });

        });

        $('body').on('click', 'tr #btn-pranota', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var billingNo = data["ID"];
            var kodeCabang = "";
                        
            //var id_special = data["ID"];
            //var contractNo = data["BILLING_CONTRACT_NO"];
            //var billingNo = data["BILLING_NO"];
            //var kodeCabang = data["BRANCH_ID"];
            var usage = data["BILLING_TYPE"];
            console.log("usage : " + usage)

            var kd = data["BRANCH_ID"];
            console.log("kd : " + kd)
            
            window.open("/Pdf/cetakPdf2?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');
            

        });

        //$('body').on('click', 'tr #btn-pranota-listrik', function () {
        //    var baris = $(this).parents('tr')[0];
        //    var table = $('#table-transwe').DataTable();
        //    var data = table.row(baris).data();

        //    var contractNo = data["ID"];
        //    var kodeCabang = "";

        //    //var id_special = data["ID"];
        //    //var contractNo = data["BILLING_CONTRACT_NO"];
        //    //var billingNo = data["BILLING_NO"];
        //    //var kodeCabang = data["BRANCH_ID"];
        //    var usage = data["BILLING_TYPE"];

        //    var param = {
        //        BE_ID: data["BE_ID"],
        //    };
        //    var req = $.ajax({
        //        contentType: "application/json",
        //        data: JSON.stringify(param),
        //        method: "POST",
        //        url: "/Pdf/GetCabang",
        //        timeout: 30000
        //    });
        //    req.done(function (data) {
        //        if (data.Status === 'S') {
        //            kodeCabang = data.Msg;

        //            window.open("/Pdf/cetakPdf2?tp=" + btoa("1E") + "&kc=" + btoa(kodeCabang) + "&cn=" + btoa(contractNo), '_blank');

        //            //window.open("/Pdf/cetakPdf?tp=" + btoa("1B") + "&bn=" + btoa(billingNo) + "&kc=" + btoa(kodeCabang), '_blank');
        //            //window.open("/Pdf/cetakPdf2?tp=" + btoa("1B") + "&kc=" + btoa(kodeCabang) + "&bn=" + btoa(billingNo), '_blank');

        //        } else {

        //        }
        //    });

        //});

    }

    var initDetilTransactionDinas = function () { //pop up
        $('body').on('click', 'tr #btn-detail-dinas', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe-dinas').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            //16 items
            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "type": "GET",
                    "url": "/TransWEBilling/GetDataTransaction",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,

                },

                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "F_REPORT_ON",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PRICE",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SUB_TOTAL",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BILLING_TYPE",
                        "class": "dt-body-center"

                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,


                "filter": false
            });

        });

        $('body').on('click', 'tr #btn-pranota-dinas', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe-dinas').DataTable();
            var data = table.row(baris).data();

            var billingNo = data["ID"];
            var kodeCabang = "";

            //var id_special = data["ID"];
            //var contractNo = data["BILLING_CONTRACT_NO"];
            //var billingNo = data["BILLING_NO"];
            //var kodeCabang = data["BRANCH_ID"];
            var usage = data["BILLING_TYPE"];
            console.log("usage : " + usage)

            window.open("/Pdf/cetakPdf2?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');


        });

    }

    var initDetilTransactionListrik = function () {
        $('body').on('click', 'tr #btn-detail-listrik', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();
            //var tableDetilTransactionListrik = $('table-detil-transaction-listrik').DataTable();
            //tableDetilTransactionListrik.destroy();
            //var tableDetilCostring = $('table-detil-transaction-listrik-costing').DataTable();
            //tableDetilCostring.destroy();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTransPricing = $('#table-detil-transaction-listrik').DataTable();
            detailTransPricing.destroy();

            var detailTransCosting = $('#table-detil-transaction-listrik-costing').DataTable();
            detailTransCosting.destroy();

            var grandTotal = data["AMOUNT"];
            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $('#GRAND_TOT').val(fGrandTotal);

            $('#table-detil-transaction-listrik').DataTable({

                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransaction",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    "data": function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "KD_TRANSAKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KODE_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "NAMA_CUSTOMER_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "DESKRIPSI_PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIl
            $('#table-detil-transaction-listrik-costing').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetail",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "USED",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TARIFF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "URL_FOTO",
                        "class": "dt-body-center",
                        "render": function (data, type, full, meta) {
                            if (data != null) {
                                return '<a href="' + data + '" target="blank"><img src="' + data + '" height="100" width="100" /></a>';
                            } else {
                                return " - ";
                            }
                        }
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });
        });
    }

    var initDetilTransactionListrikDinas = function () {
        $('body').on('click', 'tr #btn-detail-listrik-dinas', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe-dinas').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTransPricing = $('#table-detil-transaction-listrik').DataTable();
            detailTransPricing.destroy();

            var detailTransCosting = $('#table-detil-transaction-listrik-costing').DataTable();
            detailTransCosting.destroy();

            var grandTotal = data["AMOUNT"];
            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $('#GRAND_TOT').val(fGrandTotal);

            $('#table-detil-transaction-listrik').DataTable({

                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransaction",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "KD_TRANSAKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KODE_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "NAMA_CUSTOMER_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "DESKRIPSI_PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIl
            $('#table-detil-transaction-listrik-costing').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetail",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "USED",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TARIFF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "URL_FOTO",
                        "class": "dt-body-center",
                        "render": function (data, type, full, meta) {
                            if (data != null) {
                                return '<a href="' + data + '" target="blank"><img src="' + data + '" height="100" width="100" /></a>';
                            } else {
                                return " - ";
                            }
                        }
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });
        });

        //$('body').on('click', 'tr #btn-pranota-dinas', function () {
        //    var baris = $(this).parents('tr')[0];
        //    var table = $('#table-transwe-dinas').DataTable();
        //    var data = table.row(baris).data();

        //    var billingNo = data["ID"];
        //    var kodeCabang = "";

        //    //var id_special = data["ID"];
        //    //var contractNo = data["BILLING_CONTRACT_NO"];
        //    //var billingNo = data["BILLING_NO"];
        //    //var kodeCabang = data["BRANCH_ID"];
        //    var usage = data["BILLING_TYPE"];
        //    console.log("usage : " + usage)

        //    window.open("/Pdf/cetakPdf2?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');


        //});

    }

    // Aksi Button Untuk POST BILLING
    var postBilling = function () {
        $('body').on('click', 'tr #btn-post-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var POSTING_DATE = $('#POSTING_DATE').val();
            var billing_no = data["ID"];

            if (POSTING_DATE) {
                swal({
                    title: 'Warning',
                    text: "Are you sure want to post this data to Billing?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        App.blockUI({ boxed: true });
                        var param = {
                            BILL_ID: data["ID"],
                            BILL_TYPE: data["BILLING_TYPE"],
                            POSTING_DATE: POSTING_DATE
                        };
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "TransWEBilling/postToSAP"
                        });

                        req.done(function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            if (data.status === 'S') {
                                
                                //post to silapor
                                $.ajax({
                                    type: "POST",
                                    url: "/TransWEBilling/sendToSilapor",
                                    data: JSON.stringify(billing_no),
                                    contentType: "application/json; charset=utf-8",
                                    success: function (datas) {
                                        swal('Success', data.message, 'success');
                                    }
                                });
                            } else {
                                //swal('Failed', data.message, 'error');
                                $.ajax({
                                    type: "POST",
                                    url: "/TransWEBilling/GetMsgSAP",
                                    data: JSON.stringify(billing_no),
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        var tampung = "";
                                        for (var i = 0; i < data.length; i++) {
                                            var x = i % 2;

                                            //console.log(data[i]);
                                            if (x === 1) {
                                                console.log(data[i]);
                                                tampung += '<ul><li>' + data[i] + '</li></ul>';
                                            }
                                        }
                                        swal('Failed', tampung, 'error').then(function (isConfirm) {
                                            window.location = "/TransWEBilling";
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                swal('Warning', 'Please Fill The Posting Date Field!', 'warning');
            }
            
        });
    }

    // Aksi Button Untuk POST BILLING KEDINASAN
    var postBillingDinas = function () {
        $('body').on('click', 'tr #btn-post-billing-dinas', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe-dinas').DataTable();
            var data = table.row(baris).data();

            var POSTING_DATE = $('#POSTING_DATE').val();
            var billing_no = data["ID"];

            if (POSTING_DATE) {
                swal({
                    title: 'Warning',
                    text: "Are you sure want to post this data to Billing?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        App.blockUI({ boxed: true });
                        var param = {
                            BILL_ID: data["ID"],
                            BILL_TYPE: data["BILLING_TYPE"],
                            POSTING_DATE: POSTING_DATE
                        };
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "TransWEBilling/postToSAP"
                        });

                        req.done(function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            if (data.status === 'S') {

                                //post to silapor
                                $.ajax({
                                    type: "POST",
                                    url: "/TransWEBilling/sendToSilapor",
                                    data: JSON.stringify(billing_no),
                                    contentType: "application/json; charset=utf-8",
                                    success: function (datas) {
                                        swal('Success', data.message, 'success');
                                    }
                                });
                            } else {
                                //swal('Failed', data.message, 'error');
                                $.ajax({
                                    type: "POST",
                                    url: "/TransWEBilling/GetMsgSAP",
                                    data: JSON.stringify(billing_no),
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        var tampung = "";
                                        for (var i = 0; i < data.length; i++) {
                                            var x = i % 2;

                                            //console.log(data[i]);
                                            if (x === 1) {
                                                console.log(data[i]);
                                                tampung += '<ul><li>' + data[i] + '</li></ul>';
                                            }
                                        }
                                        swal('Failed', tampung, 'error').then(function (isConfirm) {
                                            window.location = "/TransWEBilling";
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                swal('Warning', 'Please Fill The Posting Date Field!', 'warning');
            }

        });
    }

    var addTransWE = function () {
        $('body').on('click', 'tr #btn-add-we-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            window.location = "/TransWE/AddTransWe/" + data["ID"];
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWE";
        });
    }

    var postingGelondongan = function () {
        $('#posting-gelondongan').on('click', function () {

            var DTGelondongan = $('#table-transwe').dataTable();
            var all_row = DTGelondongan.fnGetNodes();
            var tampung = new Array();
            for (var i = 0; i < all_row.length; i++) {

                var cek = $(all_row[i]).find("input[name='pilih-gelondongan']").is(":checked");
                if (cek) {
                    console.log($(all_row[i]).find('td:eq(1)').html());
                    //ambil lebih dari 1 data
                    var obj = new Object();
                    obj['ID'] = $(all_row[i]).find('td:eq(1)').html();

                    tampung.push(obj);
                }
            }
            console.log(JSON.stringify(tampung));
        });
    }
    
   
    

    return {
        init: function () {
            //cekRolePostingWater();
            initTableWETransaction();
            initTableWETransactionDinas();
            initDetilTransaction();
            initDetilTransactionDinas();
            initDetilTransactionListrik();
            initDetilTransactionListrikDinas();
            addTransWE();
            batal();
            postBilling();
            postBillingDinas();
            postingGelondongan();
            handleDatePickers();
            //cbHeaderGelondongan();
        }
    };
}();


function cbHeaderGelondongan() {
    //alert("test");
    var cek = $("#cbheader-gelondongan").is(":checked");
    if (cek) {
        $("input[name='pilih-gelondongan']").prop("checked",true);
    }
    else {
        $("input[name='pilih-gelondongan']").prop("checked", false);
    }
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricity.init();
    });
}

jQuery(document).ready(function () {

    //ambil tanggal sysdate js
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#POSTING_DATE').val(today);
});