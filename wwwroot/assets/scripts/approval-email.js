﻿var PendingList = function () {

    $("#CONTRACT_OFFER_NO").val(getParameterByName("xof"));
    var uriId = $('#CONTRACT_OFFER_NO').val();
    console.log(uriId)

    var initTableContractOffer = function () {
       
        
        $.ajax({
            type: "GET",
            url: "ApprovalEmail/GetDataHeder/21000002" ,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    //listItems += "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].RO_NUMBER + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>";
                    $("#CONTRACT_OFFER_NAME").val(jsonList.data[i].CONTRACT_OFFER_NAME);
                    $("#CONTRACT_OFFER_TYPE_F").val(jsonList.data[i].CONTRACT_OFFER_TYPE);
                    $("#BUSINESS_ENTITY_F").val(jsonList.data[i].BE_ID);
                    $("#CONTRACT_START_DATE").val(jsonList.data[i].CONTRACT_START_DATE);
                    $("#CONTRACT_END_DATE").val(jsonList.data[i].CONTRACT_END_DATE);
                    $("#TERM_IN_MONTHS").val(jsonList.data[i].TERM_IN_MONTHS);
                    $("#BUSINESS_PARTNER_NAME").val(jsonList.data[i].CUSTOMER_NAME);
                    $("#CUSTOMER_AR").val(jsonList.data[i].CUSTOMER_AR);
                    $("#PROFIT_CENTER_F").val(jsonList.data[i].PROFIT_CENTER);
                    $("#CONTRACT_USAGE_F").val(jsonList.data[i].CONTRACT_USAGE);
                    $("#CURRENCY").val(jsonList.data[i].CURRENCY);
                    $("#OFFER_STATUS").val(jsonList.data[i].OFFER_STATUS);

                }
                $("#condition-option").append(listItems);
            }
        });
        
    }

    var initHistoryApproval = function () {
        $('body').on('click', 'tr #btn-history', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pending-list').DataTable();
            var data = table.row(baris).data();

            var id = data["CONTRACT_OFFER_NO"];
            console.log(id);
            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/PendingList/GetDataHistoryWorkflow/" + id,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var formapprove = function () {
        $('body').on('click', 'tr #btn-form-approve', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pending-list').DataTable();
            var data = table.row(baris).data();

            window.location = "/PendingList/ApprovePage/" + data["CONTRACT_OFFER_NO"];

        });
    }

    var initTableObject = function () {

        $('#rental-object-co').DataTable({
            "ajax":
            {
                "url": "/ApprovalEmail/GetDataObject/" + uriId,
                "type": "GET",

            },

            "columns": [
                {
                    "data": "OBJECT_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "OBJECT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "OBJECT_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS_TANAH",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS_BANGUNAN",
                    "class": "dt-body-center"
                },
                {
                    "data": "INDUSTRY",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableCondition = function () {

        $('#condition').DataTable({
            "ajax": {
                "url": "/ApprovalEmail/GetDataCondition/" + uriId,
                "type": "GET",
            },
            "columns": [
                {
                    "data": "CALC_OBJECT",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "VALID_FROM",
                    "class": "dt-body-center"
                },
                {
                    "data": "VALID_TO",
                    "class": "dt-body-center"
                },
                {
                    "data": "MONTHS",
                    "class": "dt-body-center"
                },
                {
                    "data": "STATISTIC",
                    "class": "dt-body-center"
                },
                {
                    "data": "UNIT_PRICE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "AMT_REF",
                    "class": "dt-body-center"
                },
                {
                    "data": "FREQUENCY",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_DUE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "MANUAL_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "FORMULA",
                    "class": "dt-body-center"
                },
                {
                    "data": "MEASUREMENT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "NJOP_PERCENT",
                    "class": "dt-body-center"
                },
                {
                    "data": "KONDISI_TEKNIS_PERCENT",
                    "class": "dt-body-center"
                },
                {
                    "data": "SALES_RULE",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL_NET_VALUE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                }
            ],

            "ordering": false,
            "processing": false,
            "serverSide": true,


            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableManual = function () {

        $('#manual-frequency').DataTable({

            "ajax":
            {
                "url": "/ApprovalEmail/GetDataManual/" + uriId,
                "type": "GET",

            },

            "columns": [
                {
                    "data": "MANUAL_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION",
                    "class": "dt-body-center"
                },
                {
                    "data": "DUE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "NET_VALUE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "QUANTITY",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "UNIT",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableMemo = function () {

        $('#table-memo').DataTable({
            "ajax":
            {
                "url": "/ApprovalEmail/GetDataMemo/" + uriId,
                "type": "GET",

            },

            "columns": [
                {
                    "data": "CALC_OBJECT",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "MEMO",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/PendingList';
        });
    }

    var approve = function () {
        $('#btn-approve').click(function () {
            swal({
                title: 'Warning',
                text: "Are you sure want to <B>APPROVE</B> this contract ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                }
            });
        });
    }

    var reject = function () {
        $('#btn-reject').click(function () {
            swal({
                title: 'Warning',
                text: "Are you sure want to <B>REJECT</B> this contract ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                }
            });
        });
    }

    var revise = function () {
        $('#btn-revise').click(function () {
            swal({
                title: 'Warning',
                text: "Are you sure want to <B>REVISE</B> this contract ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                }
            });
        });
    }

    return {
        init: function () {
            initTableContractOffer();
            batal();
            formapprove();
            approve();
            reject();
            revise();
            initHistoryApproval();
            initTableObject();
            initTableCondition();
            initTableManual();
            initTableMemo();
        }
    };
}();

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

jQuery(document).ready(function () {
    PendingList.init();
    $('#condition > tbody').html('');
});