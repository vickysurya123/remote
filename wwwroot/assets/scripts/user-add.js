﻿var User_add = function () {

    var changeBe = function () {
        $('#BE_NAME').change(function () {
            var be_id = $('#BE_NAME').val();

            $('#PROFIT_CENTER_ID').html('');
            $('#PROFIT_CENTER_ID').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(be_id),
                    method: "POST",
                    url: "/User/GetProfitCenternew",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#PROFIT_CENTER_ID').append(temp);
                });
            }

        });
    }

    var initTable = function () {
        $('#table-config').DataTable({

            "ajax":
            {
                "url": "Configuration/GetDataConfig",
                "type": "GET",
                  
            },


            "columns": [
                {
                    "data": "REF_CODE_C"
                },
                {
                    "data": "REF_DATA",
                    "class": "dt-body-center"
                },
                {
                    "data": "REF_DESC_C"
                },
                { 
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                     "render": function (data, type, full) {
                         var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a>';
                         aksi += '<a class="btn btn-icon-only blue" id="btn-status""><i class="fa fa-exchange"></i></a>';
                         //aksi += '<a class="btn btn-icon-only red" id="btn-hapus"><i class="fa fa-close"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-config').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/Configuration/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-config').DataTable();
            var data = table.row(baris).data();
            window.location = "/Configuration/Edit/" + data["ID"];
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            console.log("AA");
            window.location = "/User";

        });
    }

    var update = function () {
        $('#btn-update').click(function () {
            var ID = $('#id').val();
            var REF_DESC = $('#ref_desc_c').val();
            var ATTRIB1 = $('#attrib1').val();
            var VAL1 = $('#val1').val();

            if (REF_DESC) {
                var param = {
                    ID: ID,
                    REF_DESC: REF_DESC,
                    ATTRIB1: ATTRIB1,
                    VAL1:VAL1
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Configuration/EditData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/Configuration';
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location.href = '/Configuration';
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }
    

    var deletedata = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-config').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete \"" + data["REF_DESC_C"] + "\"?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "Configuration/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/Configuration/AddConfig';
        });
    }

    var cbBranch = function () {
        $.ajax({
            type: "GET",
            url: "/User/GetDataBranch",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_CODE_C + "'>" + jsonList.data[i].REF_CODE_C + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#pBranch").append(listItems);
                });

            }
        });
    }
    var cbPosition = function () {
        $.ajax({
            type: "GET",
            url: "/User/GetDataPosition",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_CODE_C + "'>" + jsonList.data[i].REF_CODE_C + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#pPosition").append(listItems);
                });

            }
        });
    }

    var simpanconfig = function () {
        $('#btn-simpan').click(function () {
            var xNIK = $('#pUserLogin').val();
            var xUserName = $('#pUserName').val();
            var xBE = $('#BE_NAME').val();
            var xProfitCenter = $('#PROFIT_CENTER_ID').val();
            var xEmail = $('#pEmail').val();
            var xPhone = $('#pPhone').val();
            var xRole = $('#ROLE_NAME').val();
            console.log(xBE);

            if (xNIK && xUserName && xEmail && xPhone) {
                var param = {
                    USER_LOGIN: xNIK,
                    USER_NAME: xUserName,
                    USER_EMAIL: xEmail,
                    USER_PHONE: xPhone,
                    KD_CABANG: parseInt(xBE),
                    PROFIT_CENTER_ID : xProfitCenter,
                    ROLE_ID: xRole
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/User/AddUser1",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/User';
                        });

                    } else {
                        swal('Failed', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/User';
                        });
                        $('#addConfigModal').modal('hide');
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
                $('#addConfigModal').modal('hide');
            }
        });
    }

    var getLastID = function () {
        var REF_CODE_ID = "";
        $('#ref_code_c').change(function () {
            REF_CODE_ID = $('#ref_code_c').val();
            //console.log(REF_CODE_ID);
            // Ajax untuk get data terakhir
            $.ajax({
                type: "POST",
                url: "/Configuration/GetLastID",
                data: "{ REF_CODE:'" + REF_CODE_ID + "'}",
                //data: "{ REF_CODE:'SERVICE_GROUP'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += jsonList.data[i].REF_DATA;
                    }
                    //console.log(listItems);
                    $("#ref_data_id").html('');
                    $("#ref_data_id").val(listItems);
                }
            });

            /*
            $.ajax({ 
                        type: "POST",
                        url: "/TransVB/GetDataServiceCode",
                        data: "{ SERVICE_NAME:'" + inp + "', SERVICE_GROUP:'" + sGroup + "'}",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data, function (el) {
                                if (sGroup == 16124 || sGroup == 16115 || sGroup == 16125) {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: 2,
                                        service_name: el.service_name
                                    };
                                }
                                else {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: el.tax_code,
                                        service_name: el.service_name
                                    };
                                }
                                
                            }));
                        }
                    });
            */
        });
    }

    return {
        init: function () {
            //initTable();
            //add();
            //ubah();
            //deletedata();
            batal();
            changeBe();
            //update();
            //cbconfiguration();
            simpanconfig();
            //ubahstatus();
            //getLastID();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        User_add.init();
    });
}