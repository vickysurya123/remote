﻿var MonitoringNotification = function () {

    //----------------------------- JS UNTUK NOTIFICATION CONTRACT --------------------------

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/MonitoringNotification/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BRANCH_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY_NOTIF").html('');
                $("#BUSINESS_ENTITY_NOTIF").append(listItems);
            }
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }
    var initTable = function () {
        $('#table-list-notification').DataTable({
            "ajax": {
                "url": "/MonitoringNotification/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                    data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                    data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                    data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                    data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                    data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                    data.date_start = $('#DATE_START_NOTIF').val();
                    data.date_end = $('#DATE_END_NOTIF').val();
                },
                "dataSrc": function (json) {
                    json.data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                    json.data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                    json.data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                    json.data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                    json.data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                    json.data.date_start = $('#DATE_START_NOTIF').val();
                    json.data.date_end = $('#DATE_END_NOTIF').val();
                    //Make your callback here.
                    return json.data;
                }
            },
            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "ADDRESS",
                    "width": "1000px"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "DAYS",
                    "class": "dt-body-center"
                },
                {
                    "data": "FIRST",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print first notification" data-print="I" class="btn btn-icon-only blue" id="btn-print-i"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "SECOND",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print second notification" data-print="II" class="btn btn-icon-only blue" id="btn-print-ii"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "THIRD",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print third notification" data-print="III" class="btn btn-icon-only blue" id="btn-print-iii"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "MEMO_WARNING",
                    "class": "dt-body-center"
                },
                {
                    "data": "STOP_WARNING_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.STOP_WARNING_DATE == '-') {
                            var aksi = '<a data-toggle="modal" href="#viewConfirmContractWarning" data-toggle="tooltip" title="Stop email for this contract!" class="btn btn-icon-only blue" id="btn-form-confirm-warning"><i class="fa fa-check"></i></a>';
                        }
                        else {
                            return '<span class="label label-sm label-success"> STOPPED </span>';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },
            "filter": false
        });
    }
    var filter = function () {
        $('#btn-filter-notif').click(function () {

            if ($.fn.DataTable.isDataTable('#table-list-notification')) {
                $('#table-list-notification').DataTable().destroy();
            }

            $('#table-list-notification').DataTable({
                "ajax": {
                    "url": "/MonitoringNotification/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                        data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                        data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                        data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                        data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                        data.date_start = $('#DATE_START_NOTIF').val();
                        data.date_end = $('#DATE_END_NOTIF').val();
                    },
                    "dataSrc": function (json) {
                        json.data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                        json.data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                        json.data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                        json.data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                        json.data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                        json.data.date_start = $('#DATE_START_NOTIF').val();
                        json.data.date_end = $('#DATE_END_NOTIF').val();
                        //Make your callback here.
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FIRST",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print first notification" data-print="I" class="btn btn-icon-only blue" id="btn-print-i"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SECOND",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print second notification" data-print="II" class="btn btn-icon-only blue" id="btn-print-ii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "THIRD",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print third notification" data-print="III" class="btn btn-icon-only blue" id="btn-print-iii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_WARNING",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STOP_WARNING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STOP_WARNING_DATE == '-') {
                                var aksi = '<a data-toggle="modal" href="#viewConfirmContractWarning" data-toggle="tooltip" title="Stop email for this contract!" class="btn btn-icon-only blue" id="btn-form-confirm-warning"><i class="fa fa-check"></i></a>';
                            }
                            else {
                                return '<span class="label label-sm label-success"> STOPPED </span>';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },
                "filter": false
            });

        });
        $('#btn-filter-notif-history').click(function () {

            if ($.fn.DataTable.isDataTable('#table-list-notification')) {
                $('#table-list-notification').DataTable().destroy();
            }

            $('#table-list-notification').DataTable({
                "ajax": {
                    "url": "/MonitoringNotification/GetDataFilterHistory",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                        data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                        data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                        data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                        data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                        data.date_start = $('#DATE_START_NOTIF').val();
                        data.date_end = $('#DATE_END_NOTIF').val();
                    },
                    "dataSrc": function (json) {
                        json.data.be_id = $('#BUSINESS_ENTITY_NOTIF').val();
                        json.data.profit_center = $('#PROFIT_CENTER_NOTIF').val();
                        json.data.contract_no = $('#CONTRACT_NO_NOTIF').val();
                        json.data.badan_usaha = $('#BADAN_USAHA_NOTIF').val();
                        json.data.customer_id = $('#CUSTOMER_ID_NOTIF').val();
                        json.data.date_start = $('#DATE_START_NOTIF').val();
                        json.data.date_end = $('#DATE_END_NOTIF').val();
                        //Make your callback here.
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FIRST",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print first notification" data-print="I" class="btn btn-icon-only blue" id="btn-print-i"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SECOND",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print second notification" data-print="II" class="btn btn-icon-only blue" id="btn-print-ii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "THIRD",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print third notification" data-print="III" class="btn btn-icon-only blue" id="btn-print-iii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_WARNING",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STOP_WARNING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STOP_WARNING_DATE == '-') {
                                var aksi = '<a data-toggle="modal" href="#viewConfirmContractWarning" data-toggle="tooltip" title="Stop email for this contract!" class="btn btn-icon-only blue" id="btn-form-confirm-warning"><i class="fa fa-check"></i></a>';
                            }
                            else {
                                return '<span class="label label-sm label-success"> STOPPED </span>';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },
                "filter": false
            });

        });
    }

    var viewConfirmContract = function () {
        $('body').on('click', 'tr #btn-form-confirm-warning', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();

            $('#CONTRACT_NO_EDIT_W').val(data['CONTRACT_NO']);

        });
    }

    var btnConfirmStatus = function () {
        $('#btn-confirm-date-warning').click(function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();

            var CONTRACT_NO_EDIT = $('#CONTRACT_NO_EDIT_W').val();
            var STOP_WARNING_DATE = $('#STOP_WARNING_DATE_W').val();
            var MEMO_WARNING = $('#MEMO_WARNING_W').val();

            //console.log(STATUS_APPROVAL);
            if (STOP_WARNING_DATE) {
                var param = {
                    contract_no: CONTRACT_NO_EDIT,
                    stop_warning_date: STOP_WARNING_DATE,
                    memo_warning: MEMO_WARNING
                };
                //console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MonitoringNotification/ConfirmationDate",
                    timeout: 30000,
                    success: function (data) {
                        //console.log(data);
                        table.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#viewConfirmContractWarning').modal('hide');
                            swal('Success', 'Email Has Been Stopped', 'success');
                        } else {
                            $('#viewConfirmContractWarning').modal('hide');
                            swal('Failed', 'Something Went Wrong', 'error');
                        }
                    },
                    error: function (data) {
                        //console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                swal('Warning', 'Please Fill Confirmation Date!', 'warning');

            }
        });
    }

    var mdm = function () {
        $('#CUSTOMER_NAME').change(function () {
            if ($('#CUSTOMER_NAME_NOTIF').val() == "") {
                $("#CUSTOMER_AR_NOTIF").val("");
                $("#CUSTOMER_ID_NOTIF").val("");
            }
        })
        $('#CUSTOMER_NAME_NOTIF').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME_NOTIF').val();
                //console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/NotificationContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var changeBe = function () {
        $('#BUSINESS_ENTITY_NOTIF').change(function () {
            var be_id = $('#BUSINESS_ENTITY_NOTIF').val();

            $('#PROFIT_CENTER_NOTIF').html('');
            $('#PROFIT_CENTER_NOTIF').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/RentalObject/getProfitCenter",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#PROFIT_CENTER_NOTIF').append(temp);
                    $('#PROFIT_CENTER_NOTIF').selectpicker('refresh');
                });
            }

        });
    }

    var btnPrint = function () {
        $('#table-list-notification').on('click', 'tr #btn-print-i', function () {
            var peringatanNo = "I";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });
        $('#table-list-notification').on('click', 'tr #btn-print-ii', function () {
            var peringatanNo = "II";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });
        $('#table-list-notification').on('click', 'tr #btn-print-iii', function () {
            var peringatanNo = "III";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });

    }

    return {
        init: function () {
            //-- JS UNTUK NOTIFICATION CONTRACT
            handleDatePickers();
            CBBusinessEntity();
            viewConfirmContract();
            btnConfirmStatus();
            mdm();
            filter();
            changeBe();
            btnPrint();
            initTable();
        }
    };
}();

var NotificationContract = function () {

    //----------------------------- JS UNTUK NOTIFICATION CONTRACT --------------------------

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/NotificationContract/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBContractType();
            }
        });
    }

    var CBContractType = function () {
        $.ajax({
            type: "GET",
            url: "/NotificationContract/GetDataContractType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#CONTRACT_TYPE").html('');
                $("#CONTRACT_TYPE").append(listItems);
                CBBusinessEntityWarning();
            }
        });
    }

    var dataTableResult = function () {
        var table = $('#table-notification-contract');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20],
                [5, 15, 20]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    $('#btn-filter').click(function () {
        $('#table-notification-contract').DataTable().destroy();
        $('#table-notification-contract > tbody').html('');
        $('.sembunyi').show();

    });

    var filter = function () {
        $('#btn-filter').click(function () {
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            
            if (CUSTOMER_NAME == '') {
                $('#CUSTOMER_ID').val('');
            }

            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-notification-contract')) {
                $('#table-notification-contract').DataTable().destroy();
            }
            $('#table-notification-contract').DataTable({
                "ajax": {
                    "url": "/NotificationContract/ShowFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.contract_no = $('#CONTRACT_NO').val();
                        data.contract_type = $('#CONTRACT_TYPE').val();
                        data.customer_id = $('#CUSTOMER_ID').val();
                        data.date_start = $('#DATE_START').val();
                        data.date_end = $('#DATE_END').val();
                        //data.legal_contract_no = $('#LEGAL_CONTRACT_NO').val();

                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak-exel').show('slow');
                        $('#btn-cetak-exel-history').hide('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "EMAIL"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE_DESC"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NAME",
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "RO_ADDRESS"
                    },
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "STOP_NOTIFICATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_NOTIFICATION"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STOP_NOTIFICATION_DATE == '-') {
                                var aksi = '<a data-toggle="modal" href="#viewConfirmContract" data-toggle="tooltip" title="Stop email for this contract!" class="btn btn-icon-only blue" id="btn-form-confirm"><i class="fa fa-check"></i></a>';
                            }
                            else {
                                return '<span class="label label-sm label-success"> STOPPED </span>';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    $('#btn-filter-history').click(function () {
        $('#table-notification-contract').DataTable().destroy();
        $('#table-notification-contract > tbody').html('');
        $('.sembunyi').hide();

    });

    var filterHistory = function () {
        $('#btn-filter-history').click(function () {
            
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-notification-contract')) {
                $('#table-notification-contract').DataTable().destroy();
            }
            $('#table-notification-contract').DataTable({
                "ajax": {
                    "url": "/NotificationContract/ShowFilterHistory",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak-exel-history').show('slow');
                        $('#btn-cetak-exel').hide('slow');

                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center",
                        "visible": false
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "EMAIL"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE_DESC"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NAME",
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "RO_ADDRESS"
                    },
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "STOP_NOTIFICATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_NOTIFICATION"
                    },
                    {
                        "render": function (data, type, full) {
                        return '<span class="label label-sm label-success"> STOPPED </span>';
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var mdm = function () {
        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                //console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/NotificationContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var viewConfirmContract = function () {
        $('body').on('click', 'tr #btn-form-confirm', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-notification-contract').DataTable();
            var data = table.row(baris).data();

            $('#CONTRACT_NO_EDIT').val(data['CONTRACT_NO']);
            
        });
    }

    var btnConfirmStatus = function () {
        $('#btn-confirm-date').click(function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-notification-contract').DataTable();
            var data = table.row(baris).data();

            var CONTRACT_NO_EDIT = $('#CONTRACT_NO_EDIT').val();
            var STOP_NOTIFICATION_DATE = $('#STOP_NOTIFICATION_DATE').val();
            var MEMO_NOTIFICATION = $('#MEMO_NOTIFICATION').val();

            //console.log(STATUS_APPROVAL);
            if (STOP_NOTIFICATION_DATE) {
                var param = {
                    contract_no: CONTRACT_NO_EDIT,
                    stop_notification_date: STOP_NOTIFICATION_DATE,
                    memo_notification: MEMO_NOTIFICATION
                };
                //console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/NotificationContract/ConfirmationDate",
                    timeout: 30000,
                    success: function (data) {
                        //console.log(data);
                        table.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#viewConfirmContract').modal('hide');
                            swal('Success', 'Email Has Been Stopped', 'success');
                        } else {
                            $('#viewConfirmContract').modal('hide');
                            swal('Failed', 'Something Went Wrong', 'error');
                        }
                    },
                    error: function (data) {
                        //console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                swal('Warning', 'Please Fill Confirmation Date!', 'warning');

            }
        });
    }
    
    //-------------------------- JS UNTUK WARNING NOTIFICATION ------------------------

    var CBBusinessEntityWarning = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/NotificationContract/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY_WARNING").html('');
                $("#BUSINESS_ENTITY_WARNING").append(listItems);
                CBContractTypeWarning();
            }
        });
    }

    var CBContractTypeWarning = function () {
        $.ajax({
            type: "GET",
            url: "/NotificationContract/GetDataContractType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#CONTRACT_TYPE_WARNING").html('');
                $("#CONTRACT_TYPE_WARNING").append(listItems);
                handleBootstrapSelect();
            }
        });
    }

    var mdmWarning = function () {
        $('#CUSTOMER_NAME_WARNING').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME_WARNING').val();
                //console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/NotificationContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID_WARNING").val(ui.item.code);
            }
        });
    }

    var dataTableResultWarning = function () {
        var table1 = $('#table-warning-contract');
        var oTable1 = table1.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }
    
    $('#btn-filter-warning').click(function () {
        $('#table-warning-contract').DataTable().destroy();
        $('#table-warning-contract > tbody').html('');
        $('.sembunyi').show();

    });

    var filterWarning = function () {
        $('#btn-filter-warning').click(function () {
            var CUSTOMER_NAME_WARNING = $('#CUSTOMER_NAME_WARNING').val();

            if (CUSTOMER_NAME_WARNING == '') {
                $('#CUSTOMER_ID_WARNING').val('');
            }
            console.log('tes');

            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-warning-contract')) {
                $('#table-warning-contract').DataTable().destroy();
            }
            $('#table-warning-contract').DataTable({
                "ajax": {
                    "url": "/NotificationContract/ShowFilterWarning",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id_warning = $('#BUSINESS_ENTITY_WARNING').val();
                        data.contract_no_warning = $('#CONTRACT_NO_WARNING').val();
                        data.contract_type_warning = $('#CONTRACT_TYPE_WARNING').val();
                        data.customer_id_warning = $('#CUSTOMER_ID_WARNING').val();
                        data.date_start_warning = $('#DATE_START_WARNING').val();
                        data.date_end_warning = $('#DATE_END_WARNING').val();
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak-exel-warning').show('slow');
                        $('#btn-cetak-exel-warning-history').hide('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "EMAIL"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE_DESC"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NAME",
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "RO_ADDRESS"
                    },
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "STOP_WARNING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_WARNING"
                    },
                    {
                        "render": function (data, type, full) {

                            if (full.STOP_WARNING_DATE == '-') {

                                var aksi = '<a data-toggle="modal" href="#viewCheckOutDate" data-toggle="tooltip" title="Stop email for this contract!" class="btn btn-icon-only blue" id="btn-check"><i class="fa fa-check"></i></a>';

                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    $('#btn-filter-warning-history').click(function () {
        $('#table-warning-contract').DataTable().destroy();
        $('#table-warning-contract > tbody').html('');
        $('.sembunyi').hide();

    });

    var filterWarningHistory = function () {
        $('#btn-filter-warning-history').click(function () {
            
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-warning-contract')) {
                $('#table-warning-contract').DataTable().destroy();
            }
            $('#table-warning-contract').DataTable({
                "ajax": {
                    "url": "/NotificationContract/ShowFilterWarningHistory",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak-exel-warning-history').show('slow');
                        $('#btn-cetak-exel-warning').hide('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center",
                        "visible": false
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "EMAIL"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE_DESC"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NAME",
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "RO_ADDRESS"
                    },
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "STOP_WARNING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO_WARNING"
                    },
                    {
                        "render": function (data, type, full) {
                            return '<span class="label label-sm label-success"> STOPPED </span>';
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var viewCheckOutDate = function () {
        $('body').on('click', 'tr #btn-check', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-warning-contract').DataTable();
            var data = table.row(baris).data();

            console.log("Coba");
            console.log(data['CONTRACT_NO']);

            $('#CONTRACT_NO_CEK').val(data['CONTRACT_NO']);

        });
    }

    var btnCheckOutDate = function () {
        $('#btn-check-out-date').click(function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-warning-contract').DataTable();
            var data = table.row(baris).data();

            var CONTRACT_NO_CEK = $('#CONTRACT_NO_CEK').val();
            var STOP_WARNING_DATE = $('#STOP_WARNING_DATE').val();
            var MEMO_WARNING = $('#MEMO_WARNING').val();

            //console.log(STATUS_APPROVAL);
            if (STOP_WARNING_DATE) {
                var param = {
                    CONTRACT_NO: CONTRACT_NO_CEK,
                    STOP_WARNING_DATE: STOP_WARNING_DATE,
                    MEMO_WARNING: MEMO_WARNING
                };
                console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/NotificationContract/CheckOutDate",
                    timeout: 30000,
                    success: function (data) {
                        //console.log(data);
                        table.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#viewCheckOutDate').modal('hide');
                            swal('Success', 'Email Has Been Stopped', 'success');
                        } else {
                            $('#viewCheckOutDate').modal('hide');
                            swal('Failed', 'Something Went Wrong', 'error');
                        }
                    },
                    error: function (data) {
                        //console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                swal('Warning', 'Please Fill Confirmation Date!', 'warning');

            }
        });
    }
    
    return {
        init: function () {
            //-- JS UNTUK NOTIFICATION CONTRACT
            dataTableResult();
            handleDatePickers();
            mdm();
            CBBusinessEntity();
            //CBContractType();
            filter();
            filterHistory();
            viewConfirmContract();
            btnConfirmStatus();

            //-- JS UNTUK WARNING NOTIFICATION
            //CBBusinessEntityWarning();
            //CBContractTypeWarning();
            mdmWarning();
            dataTableResultWarning();
            filterWarning();
            filterWarningHistory();
            viewCheckOutDate();
            btnCheckOutDate();

        }
    };
}();

//--------------- EXCEL UNTUK NOTIFICATION CONTRACT -------------

var fileExcels;
function defineExcelNotification() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var CONTRACT_NO = $('#CONTRACT_NO').val();
    var CONTRACT_TYPE = $('#CONTRACT_TYPE').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
    var DATE_START = $('#DATE_START').val();
    var DATE_END = $('#DATE_END').val();

    var param = {
        BUSINESS_ENTITY,
        CONTRACT_NO,
        CONTRACT_TYPE,
        CUSTOMER_ID,
        DATE_START,
        DATE_END
    }

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/NotificationContract/defineExcelNotification",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/NotificationContract/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function defineExcelNotificationHistory() {
    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/NotificationContract/defineExcelNotificationHistory",
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

//--------------- EXCEL UNTUK WARNING CONTRACT -------------

function defineExcelWarning() {
    var BUSINESS_ENTITY_WARNING = $('#BUSINESS_ENTITY_WARNING').val();
    var CONTRACT_NO_WARNING = $('#CONTRACT_NO_WARNING').val();
    var CONTRACT_TYPE_WARNING = $('#CONTRACT_TYPE_WARNING').val();
    var CUSTOMER_ID_WARNING = $('#CUSTOMER_ID_WARNING').val();
    var DATE_START_WARNING = $('#DATE_START_WARNING').val();
    var DATE_END_WARNING = $('#DATE_END_WARNING').val();

    var param = {
        BUSINESS_ENTITY_WARNING,
        CONTRACT_NO_WARNING,
        CONTRACT_TYPE_WARNING,
        CUSTOMER_ID_WARNING,
        DATE_START_WARNING,
        DATE_END_WARNING
    }

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/NotificationContract/defineExcelWarning",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function defineExcelWarningHistory() {
    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/NotificationContract/defineExcelWarningHistory",
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" target="_blank"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>',
                type: "success",
                showConfirmButton: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

//--------------- AMBIL NAMA EXCEL -------------

function exportExcel() {
    $.ajax({
        type: "POST",
        url: "/NotificationContract/exportExcel",
        data: "{}",
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        timeout: 100000,
        success: function (response) {
            var valStrings = response.substring(2, response.length - 2);
            //console.log(valStrings);
            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

            var jsonData = "{fileName:'" + valStrings + "'}";
            setTimeout(function () {
                deleteExcel(jsonData);
            }, 2000);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

//function deleteExcel(jsonData) {
//    //console.log(jsonData);
//    $.ajax({
//        type: "POST",
//        url: "/ReportDocumentFlow/deleteExcel",
//        data: jsonData,
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        success: function (response) {
//            //console.log("Sukses");
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });

//}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        NotificationContract.init();
        MonitoringNotification.init();
    });
}

jQuery(document).ready(function () {
    $('#table-notification-contract > tbody').html('');
});

//jQuery(document).ready(function () {
//    $('#table-notification-contract > tbody').html('');
//    $('#BUSINESS_ENTITY', this).chosen();
//    $('#CONTRACT_TYPE', this).chosen();
//    $('#CONTRACT_NO', this).chosen();
//    $('#CUSTOMER_ID', this).chosen();
//});