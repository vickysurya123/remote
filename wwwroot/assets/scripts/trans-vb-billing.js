﻿var TransVariousBusiness = function () {

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
            $('.date-picker-limited').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                startDate: 1+'/'+(new Date().getMonth()+1)+'/'+(new Date().getFullYear()),
                endDate: (new Date(new Date().getFullYear(), new Date().getMonth()+1, 0).getDate())+'/'+(new Date().getMonth()+1)+'/'+(new Date().getFullYear()),
                autoclose: true
            });
        }
    }
    $('#btn-filter').click(function () {
        initTableVBTransaction();
    })
    var initTableVBTransaction = function () {
        $('#table-transvb-list').DataTable({

            "ajax":
            {
                "url": "TransVBBilling/GetDataVBTransBillingNew",
                "type": "GET",
                "data": function (data) {
                    data.select_cabang = $('#BE_NAME').val();
                }
            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "POSTING_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROF_CENTER"
                },
                {
                    "data": "COSTUMER_MDM"
                },
                {
                    "data": "KELOMPOK_PENDAPATAN"
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return '<span> USD </span>';
                        } else {
                            return '<span> IDR </span>';
                        }
                    },
                    "class": "dt-body-center",
                },
                {
                    "data": "TOTAL",
                    "class": "dt-body-center",
                     render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return full.TOTAL_USD;
                        } else {
                            return " - ";
                        }
                    },
                    "class": "dt-body-right",
                    //render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.SAP_DOCUMENT_NUMBER != null) {
                            return '<span class="label label-sm label-success"> BILLED (' + full.SAP_DOCUMENT_NUMBER + ')</span>';
                        } else {
                            return '<span class="label label-sm label-danger"> NOT BILLED YET </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.CANCEL_STATUS != null) {
                            return '<span class="badge badge-danger"> CANCELED </span>';
                        }
                        else {
                            return '';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#viewVBPTransModal" class="btn btn-icon-only green" id="btn-detail-vbp-trans"><i class="fa fa-eye"></i></a>';
                        if (full.CANCEL_STATUS != null) {
                            aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing-disabled" disabled><i class="fa fa-paper-plane-o"></i></a>';
                        }
                        //else {
                        //    if (full.ROLE_OTHER != '0') {
                        //        aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';
                        //    }
                        //    //aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';

                        //}
                        aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';
                        aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota"><i class="fa fa-print"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var idTransaksi = data["ID"];
            var installationType = data["INSTALLATION_TYPE"];
            var installationNumber = data["INSTALLATION_NUMBER"];
            console.log(installationNumber);
            console.log('id transaksi ' + idTransaksi);

            //console.log(installationType);
            if (installationType == 'A-Sambungan Air') {
                window.location = "/TransWEList/Edit/" + data["ID"];
            }
            else {
                //swal('Info', 'Edit transaksi listrik akan segera tersedia, mohon maaf atas ketidaknyamanannya saat ini.', 'info')
                var insNumber = data["INSTALLATION_NUMBER"];
                var transNumber = data["ID"];
                console.log('Trans Number ' + idTransaksi);

                var enInstNumber = Base64.encode(insNumber);
                var enTransnumber = Base64.encode(transNumber);
                console.log('inst Number ' + enInstNumber);
                console.log('trans Number ' + enTransnumber);

                window.location = "/TransWEList/EditElectricity/?xId=" + data["INSTALLATION_ID"] + '&xTd=' + data["ID"];
                //window.location.href = "/TransWEList/EditElectricity/?inumb=" + insNumber + "&tnumb=" + transNumber;
            }

        });
    }

    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail-vbp-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb-list').DataTable();
            var data = table.row(baris).data();
            console.log(data);

            $("#DETAIL_ID").val(data["ID"]);
            $("#DETAIL_PROFIT_CENTER").val(data["PROF_CENTER"]);
            $("#DETAIL_POSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAIL_SERVICE_GROUP").val(data["KELOMPOK_PENDAPATAN"]);
            $("#DETAIL_CUSTOMER_MDM").val(data["COSTUMER_MDM"]);
            $("#CUSTOMER_ID").val(data["COSTUMER_ID"]);
            $("#DETAIL_INSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);
            $("#DETAIL_CUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            var tot = data["TOTAL"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#DETAIL_TOTAL").val(tot);
            $("#DETAIL_SAP_DOCUMENT_NUMBER").val(data["SAP_DOCUMENT_NUMBER"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);

            var id_vb = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransVBBilling/GetDataTransaction/" + id_vb,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "VB_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SERVICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CM_PRICE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)

                    },
                    {
                        "data": "REMARK",
                        "class": "dt-body-center"

                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,


                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                //"language": {
                //    "url": ""
                //},

                "filter": false
            });

        });
    }

    // Aksi Button Untuk POST BILLING
    var postBilling = function () {
        $('body').on('click', 'tr #btn-post-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb-list').DataTable();
            var data = table.row(baris).data();
            var billing_no = data["ID"];

            swal({
                title: 'Warning',
                text: "Are you sure want to post this data to Billing?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    //if (POSTING_DATEC == undefined) {
                    var POSTING_DATE = $('#POSTING_DATE').val();
                        if (POSTING_DATE) {
                            App.blockUI({ boxed: true });
                            var bill_type = "ZM03";
                            var param = {
                                BILL_ID: data["ID"],
                                BILL_TYPE: bill_type,
                                POSTING_DATE: POSTING_DATE
                            };
                            var req = $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "TransVBBilling/postToSAP"
                            });
                            req.done(function (data) {

                                App.unblockUI();
                                table.ajax.reload(null, false);
                                if (data.status === 'S') {
                                    //post to silapor
                                    $.ajax({
                                        type: "POST",
                                        url: "/TransVBBilling/sendToSilapor",
                                        data: "{ BILL_ID:'" + billing_no + "'}",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (datas) {
                                            swal('Success', data.message, 'success');
                                            table.ajax.reload(null, false);
                                        }
                                    });
                                    
                                } else {
                                    $.ajax({
                                        type: "POST",
                                        url: "/TransVBBilling/GetMsgSAP",
                                        data: JSON.stringify(billing_no),
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            var tampung = "";
                                            for (var i = 0; i < data.length; i++) {
                                                var x = i % 2;

                                                if (x === 1) {
                                                    console.log(data[i]);
                                                    tampung += '<ul><li>' + data[i] + '</li></ul>';
                                                }
                                            }
                                            swal('Failed', tampung, 'error').then(function (isConfirm) {
                                                window.location = "/TransVBBilling";
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            swal('Warning', 'Please Select Posting Date!', 'warning');
                        }
                    //} else {
                    //    console.log('Cabang');
                    //    App.blockUI({ boxed: true });
                    //    var bill_type = "ZM03";
                    //    var req = $.ajax({
                    //        contentType: "application/json",
                    //        data: "{ BILL_ID:'" + data["ID"] + "' , BILL_TYPE: '" + bill_type + "'}",
                    //        method: "POST",
                    //        url: "TransVBBilling/postToSAP"
                    //    });
                    //    req.done(function (data) {

                    //        App.unblockUI();
                    //        table.ajax.reload(null, false);
                    //        if (data.status === 'S') {
                    //            swal('Success', data.message, 'success');
                    //            table.ajax.reload(null, false);
                    //        } else {
                    //            $.ajax({
                    //                type: "POST",
                    //                url: "/TransVBBilling/GetMsgSAP",
                    //                data: "{ BILL_ID:'" + billing_no + "'}",
                    //                contentType: "application/json; charset=utf-8",
                    //                success: function (data) {
                    //                    var tampung = "";
                    //                    for (var i = 0; i < data.length; i++) {
                    //                        var x = i % 2;

                    //                        if (x === 1) {
                    //                            console.log(data[i]);
                    //                            tampung += '<ul><li>' + data[i] + '</li></ul>';
                    //                        }
                    //                    }
                    //                    swal('Failed', tampung, 'error').then(function (isConfirm) {
                    //                        window.location = "/TransVBBilling";
                    //                    });
                    //                }
                    //            });
                    //        }
                    //    });
                    //}
                }
            });
        });
    }

    //Function untuk form Add (Form Sesuai Mockup)
    var simpanupdate = function () {
        $('#btn-simpan-update').click(function () {
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var ID = $('#ID_INSTALASI').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#AMOUNT').val();
            var SURCHARGE = $('#SURCHARGE').val();

            if (Number(METER_TO) < Number(METER_FROM)) {
                swal('Failed', 'Check Your Meter To Value', 'error')
            }
            else {
                if (METER_FROM && METER_TO) {
                    var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                    var nfSurcharge = parseFloat(SURCHARGE.split('.').join(''));
                    var netValue = Number(nfAmount) + Number(nfSurcharge);
                    var param = {
                        METER_FROM: METER_FROM,
                        METER_TO: METER_TO,
                        ID: ID,
                        QUANTITY: QUANTITY,
                        AMOUNT: netValue,
                        SURCHARGE: nfSurcharge,
                        SUB_TOTAL: nfAmount
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransWEList/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }

        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWEList";
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransVBList/AddTransVB';
        });
    }
    $('body').on('click', 'tr #btn-pranota', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-transvb-list').DataTable();
        var data = table.row(baris).data();
        console.log(data);
        var bill_type = data["BILLING_TYPE"];
        var bill_no = data["ID"];
        var kodeCabang = data["BRANCH_ID"];
        
        window.open("/Pdf/cetakPdf2?tp=" + btoa("1M") + "&kc=" + btoa(kodeCabang) + "&bn=" + btoa(bill_no) + "&us=" + btoa(bill_type), '_blank');
    });
    return {
        init: function () {
            initTableVBTransaction();
            initDetilTransaction();
            batal();
            ubah();
            simpanupdate();
            add();
            postBilling();
            handleDatePickers();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransVariousBusiness.init();
    });
}