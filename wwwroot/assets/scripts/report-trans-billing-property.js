﻿var ReportTransBillingProperty = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            //url: "/ReportRentalRequest/GetDataBusinessEntity",
            url: "/ReportTransBillingProperty/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBCOnditionType();
                console.log("list BE : " + listItems);
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var CBCOnditionType = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransBillingProperty/GetDataConditionType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Condition Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#CONDITION_TYPE").html('');
                $("#CONDITION_TYPE").append(listItems);
                select2ProfitCenter();
                handleBootstrapSelect();
            }
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-trans-water');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var handleDatePickers = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var mdm = function () {
        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                //console.log(inp);
                var l = inp.length;
                $.ajax({
                    type: "POST",
                    url: "/ReportTransBillingProperty/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var filter = function () {

        $('#btn-filter').click(function () {
            var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
            var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();
            //console.log(POSTING_DATE_FROM);
            //console.log(POSTING_DATE_TO);
            if (POSTING_DATE_FROM && POSTING_DATE_TO) {
                $('#row-table').css('display', 'block');
                if ($.fn.DataTable.isDataTable('#table-report-trans-property')) {
                    $('#table-report-trans-property').DataTable().destroy();
                }
                $('#table-report-trans-property').DataTable({
                    "ajax": {
                        "url": "ReportTransBillingProperty/GetDataFilterNew",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.profit_center = $('#PROFIT_CENTER').val();
                            data.be_id = $('#BUSINESS_ENTITY').val();
                            data.billing_no = $('#BILLING_NO').val();
                            data.sap_doc_no = $('#SAP_DOC_NO').val();
                            data.condition_type = $('#CONDITION_TYPE').val();
                            data.posting_date_from = $('#POSTING_DATE_FROM').val();
                            data.posting_date_to = $('#POSTING_DATE_TO').val();
                            data.customer_id = $('#CUSTOMER_ID').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak_exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "BILLING_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SAP_DOC_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "BUSINESS_ENTITY"
                        },
                        {
                            "data": "PROFIT_CENTER",
                        },
                        {
                            "data": "POSTING_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CREATION_BY"
                        },
                        {
                            "data": "BUSINESS_PARTNER_NAME"
                        },
                        {
                            "data": "CUSTOMER_AR",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "BILLING_CONTRACT_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CONDITION_TYPE_DESC"
                        },
                        {
                            "data": "GL_ACOUNT_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALID_FROM",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALID_TO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "LUAS",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "OBJECT_ID",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CURRENCY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "AMOUNT",
                            render: $.fn.dataTable.render.number('.', '.', 0),
                            "class": "dt-body-right"

                        }

                    ],
                    "columnDefs": [{
                        "width": "80px", "targets": "_all"
                    }],
                    "lengthMenu": [
                        [5, 10, 15, 20, 10000],
                        [5, 10, 15, 20, "All"]
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },

                    "filter": false,

                    "order": [
                        [10, "desc"]
                    ]
                });
            }
            else {
                swal('Warning', 'Please Select Posting Date From and Posting Date To!', 'warning');
            }
        });
    }

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {

            if ($.fn.DataTable.isDataTable('#table-report-trans-water')) {
                $('#table-report-trans-water').DataTable().destroy();
            }

            $('#table-report-trans-water').DataTable({
                "dom": "Bfrtip",
                buttons: [
                {
                    className: 'btn sbold blue',
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        }
                    }
                }
                ],
                buttons: [
                {
                    className: 'btn sbold blue',
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        }
                    }
                }
                ],
                "ajax": {
                    "url": "/ReportTransWater/ShowAllTwo",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PROFIT_CENTER_NAME"
                    },
                    {
                        "data": "CUSTOMER_NAME"
                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MIN_USED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SUB_TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "POSTING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SAP_DOCUMENT_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.CANCEL_STATUS == '1') {
                                return '<span class="label label-sm label-danger">CANCEL</span>';
                            }
                            else {
                                return '<span class="label label-sm label-success">BILLED</span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            mdm();
            showAll();
            filter();
            reset();
            CBBusinessEntity();
            //CBCOnditionType();
            select2ProfitCenter();
            //CBProfitCenter();

        }
    };
}();

function CBProfitCenter(BE_ID) {
    console.log(BE_ID);
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransBillingProperty/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list : " + listItems);
            }
        });
        
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransBillingProperty/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list d : " + listItems);
            }
        });
    }
}

var fileExcels;
function defineExcel() {
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var BILLING_NO = $('#BILLING_NO').val();
    var SAP_DOC_NO = $('#SAP_DOC_NO').val();
    var CONDITION_TYPE = $('#CONDITION_TYPE').val();
    var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
    var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();

    var param = {
        PROFIT_CENTER,
        BUSINESS_ENTITY,
        BILLING_NO,
        SAP_DOC_NO,
        CONDITION_TYPE,
        POSTING_DATE_FROM,
        POSTING_DATE_TO,
        CUSTOMER_ID,

    }

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportTransBillingProperty/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportTransBillingProperty/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

jQuery(document).ready(function () {
    $('#table-report-trans-water > tbody').html('');
    $('#table-report-trans-property').DataTable();
});

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportTransBillingProperty.init();
    });
}

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportTransBillingProperty/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}

    //var showAll = function () {
    //    $('#btn-show-all').click(function () {

    //        if ($.fn.DataTable.isDataTable('#table-report-trans-water')) {
    //            $('#table-report-trans-os').DataTable().destroy();
    //        }

    //        $('#table-report-trans-water').DataTable({
    //            "dom": "Bfrtip",
    //            buttons: [
    //            {
    //                className: 'btn sbold blue',
    //                extend: 'excel',
    //                text: '<i class="fa fa-file-excel-o"></i> Excel Export',
    //                exportOptions: {
    //                    modifier: {
    //                        search: 'applied',
    //                        order: 'applied'
    //                    }
    //                }
    //            }
    //            ],
    //            buttons: [
    //            {
    //                className: 'btn sbold blue',
    //                extend: 'excel',
    //                text: '<i class="fa fa-file-excel-o"></i> Excel Export',
    //                exportOptions: {
    //                    modifier: {
    //                        search: 'applied',
    //                        order: 'applied'
    //                    }
    //                }
    //            }
    //            ],
    //            "ajax": {
    //                "url": "/ReportTransWater/ShowAll",
    //                "type": "GET"
    //            },
    //            "columns": [
    //                {
    //                    "data": "ID",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "PROFIT_CENTER_NAME"
    //                },
    //                {
    //                    "data": "CUSTOMER_NAME"
    //                },
    //                {
    //                    "data": "INSTALLATION_CODE",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "INSTALLATION_ADDRESS",
    //                },
    //                {
    //                    "data": "PERIOD",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "METER_FROM_TO",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "QUANTITY",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "MIN_USED",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "TARIFF_CODE",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "SURCHARGE",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "SUB_TOTAL",
    //                    "class": "dt-body-center",
    //                    render: $.fn.dataTable.render.number('.', '.', 0)
    //                },
    //                {
    //                    "data": "POSTING_DATE",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "SAP_DOCUMENT_NUMBER",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "CANCEL_STATUS",
    //                    "class": "dt-body-center"
    //                }
    //            ],
    //            "columnDefs": [{
    //                "width": "80px", "targets": "_all"
    //            }],
    //            "lengthMenu": [
    //                [5, 10, 15, 20, 10000],
    //                [5, 10, 15, 20, "All"]
    //            ],
    //            "scrollX": true,
    //            "pageLength": 10,

    //            "language": {
    //                "lengthMenu": " _MENU_ records"
    //            },

    //            "filter": false,

    //            "order": [
    //                [10, "desc"]
    //            ]
    //        });
    //    });
    //}

    
