﻿//var MapsRO = function () {
//    var marker;
//    var map;
//    var PinPoint = function () {
//        var req = $.ajax({
//            contentType: "application/json",
//            data: "BRANCH_ID=" + $('#BE_NAME').val(),
//            method: "get",
//            url: "/MapsGeo/PinPoint",
//            timeout: 30000
//        });
//        req.done(function (data) {
//            App.unblockUI();
//            if (data.status === 'S') {
//                Maps(data.message.LATITUDE, data.message.LONGITUDE);
//                //PinRo();
//            } else {
//                //swal('Failed', data.message, 'error');
//            }
//        });
//    }
//    function Maps(Lat, Long) {
//        map = L.map('map').setView([Lat, Long], 16);
//        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//            //attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
//        }).addTo(map);
//        PinRo();
//    }

//    function PinRo() {
//        var req = $.ajax({
//            contentType: "application/json",
//            data: "BRANCH_ID=" + $('#BE_NAME').val() + "&STATUS=" + $('#STATUS').val() + "&ADDRESS=" + $('#ADDRESS').val(),
//            method: "get",
//            url: "/MapsGeo/PinRO",
//            timeout: 30000
//        });
//        req.done(function (data) {
//            App.unblockUI();
//            if (data.status === 'S') {
//                if (marker) { // check
//                    map.removeLayer(marker); // remove
//                }
//                var marks = data.message;
//                for (var i = 0; i < marks.length; i++) {
//                    var color = marks[i].STATUS == 'VACANT' ? "red" : marks[i].STATUS == 'BOOKED' ? "yellow" : "green";
//                    var fillColor = marks[i].STATUS == 'VACANT' ? "#ff5b5b" : marks[i].STATUS == 'BOOKED' ? "#EDC824" : "#7aff8e";
//                    var text = marks[i].STATUS == 'VACANT' ? "" : marks[i].STATUS == 'BOOKED' ? marks[i].CONTRACT_OFFER_NO : marks[i].CONTRACT_NO;
//                    marker = new L.marker([marks[i].LATITUDE, marks[i].LONGITUDE])
//                        .bindPopup("RO. NAME : " + marks[i].RO_NAME +
//                                "<br>RO. NO : " + marks[i].RO_CODE +
//                                "<br>RO. ADDRESS : " + marks[i].RO_ADDRESS +
//                                "<br>STATUS : " + marks[i].STATUS + " ( " + text + " )<br>")
//                        .addTo(map);
//                    circle = new L.circle([marks[i].LATITUDE, marks[i].LONGITUDE], {
//                        color: color,
//                        fillColor: fillColor,
//                        fillOpacity: 0.5,
//                        radius: 7
//                    }).bindPopup("RO. NAME : " + marks[i].RO_NAME +
//                                "<br>RO. NO : " + marks[i].RO_CODE +
//                                "<br>RO. ADDRESS : " + marks[i].RO_ADDRESS +
//                                "<br>STATUS : " + marks[i].STATUS + " ( " + text + " )<br>")
//                        .addTo(map);
//                }
//            } else {
//                //swal('Failed', data.message, 'error');
//            }
//        });
//    }
//    $('#btn-filter').click(function () {
//        $('#map').html('');
//        map.remove();
//        PinPoint();
//    });
//    var handleBootstrapSelect = function () {
//        $('.bs-select').selectpicker({
//            iconBase: 'fa',
//            tickIcon: 'fa-check'
//        });
//    }
//    return {
//        init: function () {
//            PinPoint();
//            handleBootstrapSelect();
//        }
//    };
//}();

var MapsGoogle = function () {
    var marker;
    var map;
    var PinPoint = function () {
        var req = $.ajax({
            contentType: "application/json",
            data: "BRANCH_ID=" + $('#BE_NAME').val(),
            method: "get",
            url: "/MapsGeo/PinPoint",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                Maps(data.message.LATITUDE, data.message.LONGITUDE);
                //PinRo();
            } else {
                //swal('Failed', data.message, 'error');
            }
        });
    }

    var map = null;
    var marker = [];
    function Maps(Lat, Long) {
        var element = document.getElementById("map");
        var mapTypeIds = [];
        for (var type in google.maps.MapTypeId) {
            mapTypeIds.push(google.maps.MapTypeId[type]);
        }
        mapTypeIds.push("satellite");

        map = new google.maps.Map(element, {
            center: new google.maps.LatLng(Lat, Long),
            zoom: 16,
            mapTypeId: "satellite",
            // disable the default User Interface
            disableDefaultUI: true,
            // add back fullscreen, streetview, zoom
            zoomControl: true,
            streetViewControl: true,
            //fullscreenControl: true,
            center: new google.maps.LatLng(Lat, Long),
            mapTypeControlOptions: {
                mapTypeIds: mapTypeIds
            }
        });

        map.mapTypes.set("satellite", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                // See above example if you need smooth wrapping at 180th meridian
                return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));

        PinRo();

    }

    function PinRo() {
        var req = $.ajax({
            contentType: "application/json",
            data: "BRANCH_ID=" + $('#BE_NAME').val() + "&STATUS=" + $('#STATUS').val() + "&ADDRESS=" + $('#ADDRESS').val(),
            method: "get",
            url: "/MapsGeo/PinRO",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                if (marker) {
                    //marker.setMap(null); // remove
                }

                var coordinates = data.message;
                var infowindow = new google.maps.InfoWindow();
                var polygonOptions;
                var polygon;
                var destinations = [];
                var destinationsCetak = [];
                var contentString = [];
                var color = [];
                var colorCetak = [];
                var fillColor = [];
                var fillColors = [];
                var text = [];
                var textCetak = [];
                var data = [];
                var dataCetak = [];

                for (var i = 0; i < coordinates.length; i++) {  
                    if (i > 0 && coordinates[i - 1].RO_NUMBER != coordinates[i].RO_NUMBER) {
                        destinationsCetak.push(destinations);
                        colorCetak.push(color);
                        fillColors.push(fillColor);
                        textCetak.push(text);
                        dataCetak.push(data);
                        var color = [];
                        var fillColor = [];
                        var destinations = [];
                        var text = [];
                        var data = [];
                        data.push({ contract: coordinates[i].CONTRACT_NO, alamat: coordinates[i].RO_ADDRESS, ro_name: coordinates[i].RO_NAME, ro_number: coordinates[i].RO_NUMBER, zone_rip: coordinates[i].ZONE_RIP, land_dimension: coordinates[i].LAND_DIMENSION, start_date : coordinates[i].START_DATE, end_date : coordinates[i].END_DATE })
                        fillColor.push(coordinates[i].STATUS == 'VACANT' ? "#ff5b5b" : coordinates[i].STATUS == 'BOOKED' ? "#EDC824" : "#7aff8e");
                        color.push(coordinates[i].STATUS == 'VACANT' ? "red" : coordinates[i].STATUS == 'BOOKED' ? "yellow" : "green");
                        destinations.push({ lat: parseFloat(coordinates[i].LATITUDE), lng: parseFloat(coordinates[i].LONGITUDE) });
                        //text.push(coordinates[i].STATUS == 'VACANT' ? "" : coordinates[i].STATUS == 'BOOKED' ? coordinates[i].CONTRACT_OFFER_NO : coordinates[i].CONTRACT_NO);
                        text.push(coordinates[i].STATUS);

                    } else {
                        fillColor.push(coordinates[i].STATUS == 'VACANT' ? "#ff5b5b" : coordinates[i].STATUS == 'BOOKED' ? "#EDC824" : "#7aff8e");
                        color.push(coordinates[i].STATUS == 'VACANT' ? "red" : coordinates[i].STATUS == 'BOOKED' ? "yellow" : "green");
                        destinations.push({ lat: parseFloat(coordinates[i].LATITUDE), lng: parseFloat(coordinates[i].LONGITUDE) });
                        //text.push(coordinates[i].STATUS == 'VACANT' ? "" : coordinates[i].STATUS == 'BOOKED' ? coordinates[i].CONTRACT_OFFER_NO : coordinates[i].CONTRACT_NO);
                        text.push(coordinates[i].STATUS);
                        data.push({ alamat: coordinates[i].RO_ADDRESS, ro_name: coordinates[i].RO_NAME, start_date: coordinates[i].START_DATE, end_date: coordinates[i].END_DATE, ro_number: coordinates[i].RO_NUMBER })
                    }

                    if (i == coordinates.length - 1) {
                        destinationsCetak.push(destinations);
                        colorCetak.push(color);
                        fillColors.push(fillColor);
                        textCetak.push(text);
                        dataCetak.push(data);
                    }
                };
                var addListenersOnPolygon = function (polygon, data, status) {
                    google.maps.event.addListener(polygon, 'click', function (event) {
                        var image = "http://i.stack.imgur.com/g672i.png";
                        var req = $.ajax({
                            contentType: "application/json",
                            data: "RO_NUMBER=" + data['ro_number'],
                            method: "get",
                            url: "/MapsGeo/GetImage",
                            timeout: 30000,
                            async: false,
                        });
                        req.done(function (data) {
                            App.unblockUI();
                            if (data.status === 'S') {
                                var gambar = data.message;
                                for (var i = 0; i < gambar.length; i++) {
                                    image = gambar[i].DIRECTORY
                                }

                            }
                        });
                        var contract = "";
                        var tgl = "";
                        if (data['contract'] != null) {
                            contract = data['contract'];
                            tgl = data['end_date']; 
                        }
                        contentString = " <div style = 'float:left; height: 130px;border-bottom: 1px solid #E7ECF1;'> <a href='#' onclick='openSlider(" + data['ro_number'] +")' > <img src='" + image + "' style ='width : 130px; height : 130px;' ></div> <div style='float:right; height: 130px; border-bottom: 1px solid #E7ECF1; padding: 10px;'></a> <b>"
                            + data['ro_name'] +
                            //"</b> <br><br>No RO : " + marks[i].RO_CODE +
                            "</b><br><br>Status : " + status + "(<a href='javascript: ' onclick='window.open(\"ContractChange/ViewContract/" + data['contract'] + " \"); ' target='_blank'> " + contract + "</a>)" +
                            "<br><br>Alamat : " + data['alamat'] +
                            "</div><div style='padding-top : 90px'> <br><br><br> Luas Lahan : " + data['land_dimension'] + " m2  <br> Jangka Waktu : "+ tgl +" <br> RIP : " + data['zone_rip'] + "</div>";

                        infowindow.setContent(contentString);
                        infowindow.setPosition(event.latLng);
                        infowindow.open(map);
                    });
                }
                for (var i = 0; i < destinationsCetak.length; i++) {
                    polygonOptions = {
                        paths: destinationsCetak[i], //'path' was not a valid option - using 'paths' array according to Google Maps API
                        strokeColor: colorCetak[i][0],
                        strokeWeight: "2",
                        fillColor: fillColors[i][0],
                        fillOpacity: 0.35
                    };
                    polygon = new google.maps.Polygon(polygonOptions);

                    //map needs to be defined somewhere outside of this loop
                    //I'll assume you already have that.
                    polygon.setMap(map);
                    addListenersOnPolygon(polygon, dataCetak[i][0], textCetak[i][0]);
                }
            }
        });
    }
    $('#btn-filter').click(function () {
        $('#map').html('');
        //map.remove();
        //marker.setMap(null);
        PinPoint();
    });

    window.openSlider = function (ro_number) {
        var req = $.ajax({
            contentType: "application/json",
            data: "RO_NUMBER=" + ro_number,
            method: "get",
            url: "/MapsGeo/GetImage",
            timeout: 30000,
            async: false,
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                var divsToRemove = document.getElementsByClassName("showSlide");
                for (var i = divsToRemove.length - 1; i >= 0; i--) {
                    divsToRemove[i].remove();
                }
                //$("#imageSlide").remove();
                var gambar = data.message;
                for (var i = 0; i < gambar.length; i++) {
                    image = gambar[i].DIRECTORY;
                    $("#image-slider").prepend("<div class='showSlide' id='imageSlide'> <img src='"+image+"' />");

                }

            }
        });
        $('#openSlider').modal('show');

        var slide_index = 1;
        displaySlides(slide_index);

        window.nextSlide = function(n) {
            displaySlides(slide_index += n);
        }

        window.currentSlide = function(n) {
            displaySlides(slide_index = n);
        }

        function displaySlides(n) {
            var i;
            var slides = document.getElementsByClassName("showSlide");
            if (n > slides.length) { slide_index = 1 }
            if (n < 1) { slide_index = slides.length }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slides[slide_index - 1].style.display = "block";
        }  
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }
    return {
        init: function () {
            PinPoint();
            handleBootstrapSelect();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        //MapsRO.init();
        MapsGoogle.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg|zip)$");
        if (!(regex.test(val))) {
            $(this).val('');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, and JPG Extension Are Allowed', 'warning');
        }
    });
});
