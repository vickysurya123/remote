﻿var BusinessEntity = function () {

    var initTable = function () {
        $('#table-be').DataTable({

            "ajax":
            {
                "url": "/BusinessEntity/GetDataBE",
                "type": "GET",
            },

            "columns": [
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_ADDRESS",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_CITY",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_PROVINCE",
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                         aksi += '<a data-toggle="modal" href="#viewBEModal" class="btn btn-icon-only green" id="btn-detil"><i class="fa fa-eye"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var addmeasurement = function () {
        $('body').on('click', 'tr #btn-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();

            window.location = "/BusinessEntity/AddMeasurement/" + data["BE_ID"];
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();

            window.location = "/BusinessEntity/Edit/" + data["BE_ID"];
        });
    }

    var detil = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();

            $("#BE_ID").val(data["BE_ID"]);
            $("#BE_NAME").val(data["BE_NAME"]);
            $("#BE_ADDRESS").val(data["BE_ADDRESS"]);
            $("#POSTAL_CODE").val(data["POSTAL_CODE"]);
            $("#BE_CITY").val(data["BE_CITY"]);
            $("#BE_PROVINCE").val(data["BE_PROVINCE"]);
            $("#VALID_FROM").val(data["VALID_FROM"]);
            $("#VALID_TO").val(data["VALID_TO"]);
            $("#REF_DESC").val(data["REF_DESC"]);
            $("#PHONE_1").val(data["PHONE_1"]);
            $("#FAX_1").val(data["FAX_1"]);
            $("#EMAIL").val(data["EMAIL"]);

            $("#HARBOUR_CLASS").val(data["HARBOUR_CLASS"]);
            $("#PHONE_1").val(data["PHONE_1"]);
            $("#FAX_1").val(data["FAX_1"]);
            $("#PHONE_2").val(data["PHONE_2"]);
            $("#FAX_2").val(data["FAX_2"]);
            $("#EMAIL").val(data["EMAIL"]);
        });
    }


    var initDetilMeasurement = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();
            var id_be = data["BE_ID"];

            $('#table-measurement').DataTable({

                "ajax":
                {
                    "url": "/BusinessEntity/GetDataMeasurement/" + id_be,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DESCRIPTION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });

    }

    var initTableAttachment = function () {
        $('body').on('click', 'tr #btn-detil', function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();
            var id_be = data["BE_ID"];

            $('#table-attachment').DataTable({

                "ajax":
                {
                    "url": "/BusinessEntity/GetDataAttachment/" + id_be,
                    "type": "GET",

                },
                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + '../../REMOTE/BusinessEntity/' + id_be + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                            return x;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }


    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/BusinessEntity/Add';
        });
    }

    var multiple_contact = function () {

    }

    return {
        init: function () {
            initTable();
            add();
            ubah();
            detil();
            addmeasurement();
            initDetilMeasurement();
            initTableAttachment();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        BusinessEntity.init();
    });
}