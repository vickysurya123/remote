﻿var SettingOtherService = function () {

    var initTableService = function () {
        $('#table-service').DataTable({
             
            "ajax":
            {
                "url": "SettingOtherService/GetDataService",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "USER_NAME",
                    "class": "dt-body-center"
                },
                //{
                //    "data": "PENDAPATAN",
                //    "class": "dt-body-center"

                //},
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                            aksi += '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }
                

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var clear = function () {
        $('input').val('');
    }

    //$('#table-service').on('click', 'tr #btn-ubah', function () {
    //    clear();
    //    var baris = $(this).parents('tr')[0];
    //    var table = $('#table-service').DataTable();
    //    var data = table.row(baris).data();
    //    var table_row = $('#table-service-detail')
    //    var a = '';
    //    var b = '';
    //    //var start_date = data["START_DATE"].split(" ");
    //    //var end_date = data["END_DATE"].split(" ");

    //    $('#txt-judul').text('Edit');
    //    $('#id_detail').val(data["ID"]);
    //    //$('#START_DATE').val(start_date[0]);
    //    //$('#END_DATE').val(end_date[0]);
    //    //$('#NOTIF_START').val(data["NOTIF_START"]);
    //    //$('#CONTRACT_TYPE').val(data["CONTRACT_TYPE"]);

    //    $('#adddetailModal').modal('show');
    //    $('#table-service-detail').DataTable().destroy();
    //    $('#table-service-detail tbody').html('');

    //    $.ajax({
    //        type: "GET",
    //        url: "/SettingOtherService/EditDataDetail?id=" + data["ID"],
    //        contentType: "application/json",
    //        dataType: "json",
    //        success: function (data) {
    //            var jsonList = data;
    //            for (var i = 0; i < jsonList.data.length; i++) {
    //                a += '<tr>' +
    //                    '<td></td>' +
    //                    '<td>' + jsonList.data[i].DESCRIPTION + '</td>' +
    //                    '<td><center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
    //                    '</tr>';
    //            }
    //            $('#table-service-detail tbody').append(a);
    //            $('#table-service-detail').DataTable({
    //                "lengthMenu": [
    //                    [5, 15, 20, -1],
    //                    [5, 15, 20, "All"]
    //                ],
    //                "pageLength": 100000,
    //                "paging": false,
    //                "language": {
    //                    "lengthMenu": " _MENU_ records"
    //                },
    //                "searching": false,
    //                "lengthChange": false,
    //                "bSort": false,
    //            });
    //            resetNumbering();
    //        }
    //    });
    //});

    $('#table-service').on('click', 'tr #btn-ubah', function () {
        clear();
        var baris = $(this).parents('tr')[0];
        var table = $('#table-service').DataTable();
        var data = table.row(baris).data();
        window.location.href = '/SettingOtherService/Config?id=' + data['ID'];
    });

    var listItems = "";

    var addServiceGroup = function () {
        //table row
        var tablem = $('#table-service-detail');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
        });

        //Generate textbox(form) di data table table-row
        $('#btn-add-detail').on('click', function () {
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var xtable = $('#table-service-detail').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-service-detail").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var serviceGroup = '<select class="form-control SERVICE_GROUP" name="SERVICE_GROUP" id="SERVICE_GROUP">' + listItems + '</select>';

            oTable.fnAddData([nomor,
                serviceGroup,
                '<center><a class="btn default btn-xs green btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>'                
            ], true);
            
        });

        $('#table-service-detail').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var data = table.row(baris).data();
            console.log(data);
            var service_Group = data[1].split(" - ")
            if (service_Group[0] != null) {
                a = "<tr><td>" + service_Group[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-service-detail tbody tr').each(function () {
                $(this).find('td').eq(0).html(index);
                index++;
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                indexparam++;
            });
        }

        $('#table-service-detail').on('click', 'tr .btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var data = table.row(baris).data();

            var a = $('#SERVICE_GROUP').val();
            var tombol = '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var service_Group = a.split("|")
            console.log(service_Group);
            oTable.fnUpdate(service_Group[3], baris, 1, false);
            oTable.fnUpdate(tombol, baris, 2, false);
            //oTable.fnUpdate(service_Group[3], baris, 3, false);


            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);

            //$(this).remove();
            return false;

        });
    }

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-service').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: aData.ID
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Approval Setting on branch \"" + aData.USER_ID + "\"?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingOtherService/DeleteHeader",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-service').DataTable();
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                        table.ajax.reload();
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/SettingOtherService/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                //var listItems = "";
                listItems += "<option value=''>-- Choose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + '|' + jsonList.data[i].PENDAPATAN + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
            }
        });

    }

    var comboUser = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/SettingOtherService/GetDataDropDownUser",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                //var listItems = "";
                listItemsUser += "<option value=''>-- Choose User --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItemsUser += "<option value='" + jsonList.data[i].ID + '|' + jsonList.data[i].USER_NAME + "'>" + jsonList.data[i].USER_NAME + "</option>";
                }
                $("#USER_NAME").html('');
                $("#USER_NAME").append(listItemsUser);

                console.log(data.ID);
            }
        });

    }

    var simpanData = function () {
        $('#btn-simpanadd-detail').click(function () {
            var table = $('#table-service').DataTable();
            var allowSave = true;
            var HEADER_ID = $('#id_detail').val();

            var DTMemo = $('#table-service-detail').dataTable();
            var countDTMemo = DTMemo.fnGetData();
            arrRole = new Array();
            for (var i = 0; i < countDTMemo.length; i++) {
                var itemDTMemo = DTMemo.fnGetData(i);
                var a = itemDTMemo[1].split(" - ");
                var paramDTMemo = {
                    COA: a[0],
                    DESCRIPTION: itemDTMemo[1],
                }
                arrRole.push(paramDTMemo);
            }
            if (countDTMemo.length > 0) {
                $('#table-service-detail tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-frequency').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }

            delRole = new Array();
            $('#delete_role tbody tr').each(function () {
                var deleteRole = {
                    COA: $(this).find("td:first").text(),
                }
                delRole.push(deleteRole);
            });
            
            if (allowSave) {
                var param = {
                    HEADER_ID: HEADER_ID,
                    Service: arrRole,
                    DeleteD: delRole,
                }
                console.log(param);
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingOtherService/InsertDetail",
                    timeout: 30000
                });
                req.done(function (data) {
                    $('#adddetailModal').modal('hide');
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                $('#adddetailModal').modal('hide');
                swal('Failed', 'Harap di cek kembali inputanya, dan pada table harap di centang semua', 'error').then(function (isConfirm) {
                    $('#adddetailModal').modal('show');
                });
            }

        });
    }

    var listItemsUser="";

    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-service').DataTable();
            var USER_NAME = $('#USER_NAME').val();
            var ID = $('#id_apv_header').val();

            var arrayUser = USER_NAME.split("|");
            var id_user = arrayUser[0];
            var user_name = arrayUser[1];

            if (id_user) {
                var param = {
                    USER_NAME: user_name,
                    //MODUL_ID: 1,
                    USER_ID: id_user,
                    ID: ID
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingOtherService/AddHeader",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    //var simpanconfig = function () {
    //    $('#btn-add').click(function () {
            
    //        var SERVICE_GROUP = $('#SERVICES_GROUP').val();
    //        var USER_NAME = $('#USER_NAME').val();
            
    //        var arrayService = SERVICE_GROUP.split("|");
    //        var val1 = arrayService[0];
    //        var val3 = arrayService[1];
    //        var val4 = arrayService[2];
    //        var pendapatan = arrayService[3];

    //        var arrayUser = USER_NAME.split("|");
    //        var id_user = arrayUser[0];
    //        var user_name = arrayUser[1];
                        
    //        if (SERVICE_GROUP && USER_NAME && arrayService && arrayUser) {
    //            var param = {
    //                USER_ID: id_user,
    //                USER_NAME: user_name,
    //                VAL1: val1,
    //                VAL3: val3,
    //                VAL4: val4,
    //                HEADER_ID: id_user,
    //                PENDAPATAN: pendapatan
    //            };

    //            //var param2 = {
    //            //    VAL1: val1,
    //            //    VAL3: val3,
    //            //    VAL4: val4,
    //            //    HEADER_ID: id_user,
    //            //    PENDAPATAN: pendapatan
    //            //}

    //            //console.log(ID);
    //            console.log("ID USER: " + id_user);
    //            console.log("USERNAME : " + user_name);
    //            console.log("VAL1 : " + val1);
    //            console.log("VAL3 : " + val3);
    //            console.log("VAL4 : " + val4);
    //            console.log("PENDAPATAN : " + pendapatan);

    //            App.blockUI({ boxed: true });
    //            var req = $.ajax({
    //                contentType: "application/json",
    //                data: JSON.stringify(param),
    //                method: "POST",
    //                url: "/SettingOtherService/AddService",
    //                timeout: 30000
    //            });

    //            req.done(function (data) {
    //                App.unblockUI();
    //                if (data.status === 'S') {
    //                    swal('Success', data.message, 'success').then(function (isConfirm) {
    //                        window.location.href = '/SettingOtherService';
    //                    });

    //                } else {
    //                    swal('Failed', data.message, 'success').then(function (isConfirm) {
    //                        window.location.href = '/SettingOtherService';
    //                    });
    //                    //$('#addConfigModal').modal('hide');
    //                }
    //            });
    //        } else {
    //            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
    //            //$('#addConfigModal').modal('hide');
    //        }
    //    });
    //}

    //var ubah = function () {
    //    $('body').on('click', 'tr #btn-ubah', function () {
    //        var baris = $(this).parents('tr')[0];
    //        var table = $('#table-transvb-list').DataTable();
    //        var data = table.row(baris).data();

    //        var idTransaksi = data["ID"];
    //        console.log('id transaksi ' + idTransaksi);

    //        window.location = "/TransVBList/Edit/" + data["ID"];
    //    });
    //}

    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail-vbp-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb-list').DataTable();
            var data = table.row(baris).data();
            console.log(data);

            $("#DETAIL_ID").val(data["ID"]);
            $("#DETAIL_PROFIT_CENTER").val(data["PROF_CENTER"]);
            $("#DETAIL_POSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAIL_SERVICE_GROUP").val(data["KELOMPOK_PENDAPATAN"]);
            $("#DETAIL_CUSTOMER_MDM").val(data["COSTUMER_MDM"]);
            $("#CUSTOMER_ID").val(data["COSTUMER_ID"]);
            $("#DETAIL_INSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);
            $("#DETAIL_CUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            var tot = data["TOTAL"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#DETAIL_TOTAL").val(tot);
            $("#DETAIL_SAP_DOCUMENT_NUMBER").val(data["SAP_DOCUMENT_NUMBER"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);

            var id_vb = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransVB/GetDataTransaction/" + id_vb,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "VB_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SERVICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CM_PRICE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)

                    },
                    {
                        "data": "REMARK",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"

                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,


                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                //"language": {
                //    "url": ""
                //},

                "filter": false
            });

        });
    }

    var initDetilTransactionListrik = function () {
        $('body').on('click', 'tr #btn-detail-listrik', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTransPricing = $('#table-detil-transaction-listrik').DataTable();
            detailTransPricing.destroy();

            var detailTransCosting = $('#table-detil-transaction-listrik-costing').DataTable();
            detailTransCosting.destroy();

            var grandTotal = data["AMOUNT"];
            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $('#GRAND_TOT').val(fGrandTotal);

            $('#table-detil-transaction-listrik').DataTable({

                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransaction",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "KD_TRANSAKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KODE_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "NAMA_CUSTOMER_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "DESKRIPSI_PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIl
            $('#table-detil-transaction-listrik-costing').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetail",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "USED",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TARIFF",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });
        });
    }


    //Function untuk form Add (Form Sesuai Mockup)
    var simpanupdate = function () {
        $('#btn-simpan-update').click(function () {
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var ID = $('#ID_INSTALASI').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#AMOUNT').val();
            var SURCHARGE = $('#SURCHARGE').val();

            if (Number(METER_TO) < Number(METER_FROM)) {
                swal('Failed', 'Check Your Meter To Value', 'error')
            }
            else {
                if (METER_FROM && METER_TO) {
                    var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                    var nfSurcharge = parseFloat(SURCHARGE.split('.').join(''));
                    var netValue = Number(nfAmount) + Number(nfSurcharge);
                    var param = {
                        METER_FROM: METER_FROM,
                        METER_TO: METER_TO,
                        ID: ID,
                        QUANTITY: QUANTITY,
                        AMOUNT: netValue,
                        SURCHARGE: nfSurcharge,
                        SUB_TOTAL: nfAmount
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransWEList/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }

        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWEList";
        });
    }

    //var add = function () {
    //    $('#btn-add').click(function () {
    //        window.location.href = '/TransVBList/AddTransVB';
    //    });
    //}

    return {
        init: function () {
            initTableService();
            initDetilTransaction();
            initDetilTransactionListrik();
            comboServiceGroup();
            comboUser();
            batal();
            //ubah();
            simpanupdate();
            //add();
            simpan();
            addServiceGroup();
            simpanData();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        SettingOtherService.init();
    });
}