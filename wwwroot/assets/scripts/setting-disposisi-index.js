﻿var SettingDisposisiIndex = function () {

    var initTableDisposisi = function () {
        $('#table-setting-disposisi').DataTable({

            "ajax":
            {
                "url": "SettingDisposisi/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAME",
                    "class": "dt-body-left"
                },
                {
                    "data": "START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "IS_ACTIVE",
                    "class": "dt-body-center",
                    "render": function (data, type, row) {
                        var x = null;
                        if (data == "1") {
                            x = '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            x = '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                        return x;
                    },
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        aksi += '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>';
                        aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }


            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-setting-disposisi').DataTable();
            var data = table.row(baris).data();
            idUser = data["ID"];
            xStatus = data["IS_ACTIVE"];
            //console.log(xStatus);
            //console.log("----------------");
            if (xStatus == 1) {
                xStatus = "0";
                //console.log("IF");
                //console.log(xStatus);
            } else {
                //console.log("ELSE");
                xStatus = "1";
                //console.log(xStatus);
            }
            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'

            }).then(function (isConfirm) {
                if (isConfirm) {

                    var param = {
                        ID: idUser,
                        IS_ACTIVE: xStatus
                    };

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/SettingDisposisi/EditStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        var table = $('#table-setting-disposisi').DataTable();
                        App.unblockUI();
                        if (data.Status === 'S') {
                            swal('Berhasil', data.Msg, 'success');
                            table.ajax.reload();
                        } else {
                            swal('Gagal', data.Msg, 'error');
                        }
                    });
                }
            });
        });
    }

    var clear = function () {
        $('input').val('');
    }

    $('#table-setting-disposisi').on('click', 'tr #btn-ubah', function () {
        clear();
        var baris = $(this).parents('tr')[0];
        var table = $('#table-setting-disposisi').DataTable();
        var data = table.row(baris).data();
        window.location.href = '/SettingDisposisi/Add?id=' + data['ID'];
    });

    var newOption = new Option(name, user_id, false, false);
    $('#USERNAME').append(newOption).trigger('change');


    var listItems = "";
    var dataEmployee = function () {
        $("#USERNAME").select2({
            allowClear: true,
            width: '100%',
            multiple: false,
            ajax: {
                url: "/SettingDisposisi/GetDataUser",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });

        $.ajax({
            type: "GET",
            url: "/SettingDisposisi/GetDataDropDownUser",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                //var listItems = "";
                listItems += "<option value=''>-- Choose User --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + '|' + jsonList.data[i].ID + '|' + jsonList.data[i].ID + '|' + jsonList.data[i].EMPLOYEE_NAME + "'>" + jsonList.data[i].EMPLOYEE_NAME + "</option>";
                }
                $("#USER_DISPOSISI").html('');
                $("#USER_DISPOSISI").append(listItems);
            }
        });
    } 

    var addUser = function () {
        //table row
        var tablem = $('#table-user-detail');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        //Generate textbox(form) di data table table-row
        $('#btn-add-detail').on('click', function () {
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-user-detail').DataTable();
            var xtable = $('#table-user-detail').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-user-detail").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var serviceGroup = '<select class="form-control SERVICE_GROUP" name="SERVICE_GROUP" id="SERVICE_GROUP">' + listItems + '</select>';
            var urutan = '<input type="number" min="1" max="5" class="form-control URUTAN" name="URUTAN" id="URUTAN" value="'+ nomor +'">';
            oTable.fnAddData([nomor, urutan,
                serviceGroup,
                '<center><a class="btn default btn-xs green btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>'
            ], true);
            $('.SERVICE_GROUP').select2();
        });

        $('#table-user-detail').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-user-detail').DataTable();
            var data = table.row(baris).data();
            var service_Group = data[2].split(" - ")
            if (service_Group[0] != null) {
                a = "<tr><td>" + service_Group[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-user-detail tbody tr').each(function () {
                if (oTable.fnGetData().length > 0) {
                    $(this).find('td').eq(0).html(index);
                    index++;
                }
                else {
                    $(this).find('td').eq(0).html("No data available in table");
                }
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                indexparam++;
            });
        }

        $('#table-user-detail').on('click', 'tr .btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-user-detail').DataTable();
            var data = table.row(baris).data();

            var a = $('#SERVICE_GROUP').val();
            var urutan = $('#URUTAN').val();
            var tombol = '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var service_Group = a.split("|")
            var dataUser = service_Group[2] + " | " + service_Group[3]
            oTable.fnUpdate(urutan , baris, 1, false);
            oTable.fnUpdate(dataUser , baris, 2, false);
            oTable.fnUpdate(tombol, baris, 3, false);
            return false;

        });
    }

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-setting-disposisi').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        console.log(aData);
        var param = {
            ID: aData.ID
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Setting Disposisi \"" + aData.NAME + "\"?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingDisposisi/DeleteHeader",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-setting-disposisi').DataTable();
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                        table.ajax.reload();
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });

    var checkbox = [];
    var updateInstruksi = [];
    updateInstruksi = instruksi.split(",");
    updateInstruksi.forEach(function (e, i) {
        if (e == "Proses Lanjut Sesuai Prosedur") {
            document.getElementById("defaultCheck1").checked = true;
            checkbox.push("Proses Lanjut Sesuai Prosedur");
        }
        if (e == "Cek dan Evaluasi") {
            document.getElementById("defaultCheck2").checked = true;
            checkbox.push("Cek dan Evaluasi");
        }
        if (e == "Saran dan Pendapat") {
            document.getElementById("defaultCheck3").checked = true;
            checkbox.push("Saran dan Pendapat");
        }
        if (e == "UMK / UDK") {
            document.getElementById("defaultCheck4").checked = true;
            checkbox.push("UMK / UDK");
        }
    });

    $("#defaultCheck1").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'Proses Lanjut Sesuai Prosedur');
            checkbox.push($('#defaultCheck1').val());
        } else {
            var res = checkbox.filter((p, i) => {
                if (p == "Proses Lanjut Sesuai Prosedur") {
                    checkbox.splice(i, 1); //remove the mached object from the original array
                    return p;
                }
            });
        }
    });

    $("#defaultCheck2").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'Cek dan Evaluasi');
            checkbox.push($('#defaultCheck2').val());
        } else {
            var res = checkbox.filter((p, i) => {
                if (p == "Cek dan Evaluasi") {
                    checkbox.splice(i, 1); //remove the mached object from the original array
                    return p;
                }
            });
        }
    });

    $("#defaultCheck3").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'Saran dan Pendapat');
            checkbox.push($('#defaultCheck3').val());
        } else {
            var res = checkbox.filter((p, i) => {
                if (p == "Saran dan Pendapat") {
                    checkbox.splice(i, 1); //remove the mached object from the original array
                    return p;
                }
            });
        }
    });

    $("#defaultCheck4").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'UMK / UDK');
            checkbox.push($('#defaultCheck4').val());
        } else {
            var res = checkbox.filter((p, i) => {
                if (p == "UMK / UDK") {
                    checkbox.splice(i, 1); //remove the mached object from the original array
                    return p;
                }
            });
        }
    });
    console.log(checkbox);
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var user_id_header = '';
    var user_header = '';

    var simpanData = function () {
        $('#btn-simpan').click(function () {
            var headerId = $('#ID').val();
            var instruksi = checkbox.toString();
            if (headerId !== "") {
                UpdateHeader(headerId);
            }
            else {
                var selections = ($("#USERNAME").select2('data'));
                selections.forEach(function (e, i) {
                    user_header += (i > 0 ? ';' : '') + e.text;
                    user_id_header += (i > 0 ? ';' : '') + e.id;
                });

                if (user_id_header !== "") {

                    var DTDetil = $('#table-user-detail').dataTable();
                    var countDTDetil = DTDetil.fnGetData().length;

                    if (countDTDetil <= 0) {
                        swal('Warning', "Harap Memilih User Terlebih Dahulu.", 'warning');
                        return false;
                    }

                    if (user_id_header) {
                        var param = {
                            NAME: user_header,
                            USER_ID: user_id_header,
                            INSTRUKSI: instruksi,
                            MEMO: $('#MEMO').val(),
                            START_DATE: $('#START_DATE').val(),
                            END_DATE: $('#END_DATE').val(),
                        };
                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/SettingDisposisi/AddHeader",
                            timeout: 30000
                        });
                        req.done(function (data) {
                            App.unblockUI();
                            if (data.Status === 'S') {
                                $('#ID').val(data.Msg);
                                SimpanDetil(data.Msg);
                            } else {
                                swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                                });
                            }
                        });
                    }
                    else {
                        swal('Warning', "Harap Memilih User Terlebih Dahulu.", 'warning');
                    }
                }
                else {
                    swal('Warning', "Harap Memilih User Terlebih Dahulu.", 'warning');
                }
            }
        });

        function UpdateHeader(HEADER_ID) {
            var instruksi = checkbox.toString();
            var selections = ($("#USERNAME").select2('data'));
            selections.forEach(function (e, i) {
                user_header += (i > 0 ? ';' : '') + e.text;
                user_id_header += (i > 0 ? ';' : '') + e.id;
            });
            var param = {
                ID: HEADER_ID,
                NAME: user_header,
                USER_ID: user_id_header,
                INSTRUKSI: instruksi,
                MEMO: $('#MEMO').val(),
                START_DATE: $('#START_DATE').val(),
                END_DATE: $('#END_DATE').val(),
            };
            App.blockUI({ boxed: true });
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/SettingDisposisi/EditHeader",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                if (data.Status === 'S') {
                    SimpanDetil(HEADER_ID);
                } else {
                    swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                    });
                }
            });
        }

        function SimpanDetil(HEADER_ID) {
            var allowSave = true;

            var DTMemo = $('#table-user-detail').dataTable();
            var countDTMemo = DTMemo.fnGetData();
            arrRole = new Array();
            for (var i = 0; i < countDTMemo.length; i++) {
                var itemDTMemo = DTMemo.fnGetData(i);
                var a = itemDTMemo[2].split(" | ");
                var b = itemDTMemo[1];

                var paramDTMemo = {
                    URUTAN: b,
                    USER_ID: a[0],
                    NAME: itemDTMemo[2],
                }
                arrRole.push(paramDTMemo);
            }
            if (countDTMemo.length > 0) {
                $('#table-user-detail tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-frequency').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }

            delRole = new Array();
            $('#delete_role tbody tr').each(function () {
                var cariId = $(this).find("td:first").text().split(" | ");
                var deleteRole = {
                    USER_ID: cariId[0],
                }
                delRole.push(deleteRole);
            });

            if (allowSave) {
                var param = {
                    HEADER_ID: HEADER_ID,
                    Service: arrRole,
                    DeleteD: delRole,
                }
                console.log(param);
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingDisposisi/InsertDetail",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            window.location.href = '/SettingDisposisi';
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                        });
                    }
                });
            } else {
                swal('Failed', 'Harap di cek kembali inputanya, dan pada table harap di centang semua', 'error').then(function (isConfirm) {
                });
            }

        }
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/SettingDisposisi";
        });
    }

    return {
        init: function () {
            dataEmployee();
            addUser();
            simpanData();
            batal();
            datePicker();
            initTableDisposisi();
            ubahstatus();
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        SettingDisposisiIndex.init();

        var headerId = $('#ID').val();

        if (headerId !== "") {
            //$('#search-employee').addClass("hidden");
            var a = '';
            $('#table-user-detail').DataTable().destroy();
            $('#table-user-detail tbody').html('');

            $.ajax({
                type: "GET",
                url: "/SettingDisposisi/EditDataDetail?id=" + headerId,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data;
                    if (jsonList.data !== null) {
                        for (var i = 0; i < jsonList.data.length; i++) {
                            a += '<tr>' +
                                '<td></td>' +
                                '<td>' + jsonList.data[i].URUTAN + '</td>' +
                                '<td>' + jsonList.data[i].NAME + '</td>' +
                                '<td><center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
                                '</tr>';
                        }
                        $('#table-user-detail tbody').append(a);
                        $('#table-user-detail').DataTable({
                            "lengthMenu": [
                                [5, 15, 20, -1],
                                [5, 15, 20, "All"]
                            ],
                            "pageLength": 100000,
                            "paging": false,
                            "language": {
                                "lengthMenu": " _MENU_ records"
                            },
                            "searching": false,
                            "lengthChange": false,
                            "bSort": false,
                        });
                        resetNumbering();
                    }
                    
                }
            });
        }
    });
}