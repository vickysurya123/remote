﻿var User = function () {
    

    var changeBe = function () {
        var be_id = $('#BE_NAME').val();
        console.log(be_id);
        var profit_center = $('#PROFIT_CENTER_EDIT').val();

        $('#PROFIT_CENTER_ID').html('');
        $('#PROFIT_CENTER_ID').html('<option value="@ViewBag.PROFIT_CENTER_ID"> -- Choose Profit Center -- </option>');
        if (be_id) {
            var param = {
                be_id: be_id
            };
            App.blockUI({ boxed: true });
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(be_id),
                method: "POST",
                url: "/User/GetProfitCenternew",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                var temp = '';
                //console.log(data);
                var jsonList = data;
                for (var i = 0; i < jsonList.data.length; i++) {
                    temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    //temp += '<option value="@ViewBag.PROFIT_CENTER_ID"> </option>'
                }
                $('#PROFIT_CENTER_ID').append(temp);
                $('#PROFIT_CENTER_ID option[value = "' + profit_center + '" ] ').attr('selected', 'selected');
            });
        }
        $('#BE_NAME').change(function () {
            var be_id = $('#BE_NAME').val();

            $('#PROFIT_CENTER_ID').html('');
            //$('#PROFIT_CENTER_ID').html('<option value="0">-- Choose Profit Center --</option>');
            $('#PROFIT_CENTER_ID').html('<option value="@ViewBag.PROFIT_CENTER_ID"> -- Choose Profit Center -- </option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(be_id),
                    method: "POST",
                    url: "/User/GetProfitCenternew",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    //console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                        //temp += '<option value="@ViewBag.PROFIT_CENTER_ID"> </option>'
                    }
                    $('#PROFIT_CENTER_ID').append(temp);
                });
            }

        });
    }

    var initTable = function () {
        $('#table-role').DataTable({
            "ajax":
            {
                "url": "User/GetDataUser?kdCabang=" + $('#BE_NAME').val(),
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center hidden"
                },
                {
                    "data": "USER_LOGIN",
                    "class": "dt-body-center",
                    "defaultContent": "-"
                },
                {
                    "data": "USER_NAME",
                    "class": "dt-body-center",
                    "defaultContent": "-"

                },
                {
                    "data": "BE_NAME",
                    "class": "dt-body-center",
                    "defaultContent": "-"

                },
                {
                    "data": "PROFIT_CENTER_ID",
                    "class": "dt-body-center",
                    "defaultContent": "-"

                },
                {
                    "data": "USER_EMAIL",
                    "class": "dt-body-center",
                    "defaultContent": "-"
                },
                {
                    "data": "USER_PHONE",
                    "class": "dt-body-center",
                    "defaultContent": "-"
                },
                {
                    "data": "ROLE_NAME",
                    "class": "dt-body-center",
                    "defaultContent": "-"
                },
                {
                    "data": "STATUS",
                    "class": "dt-body-center",
                    "render": function (data, type, row) {
                        var x = null;
                        if (data == "1") {
                            x = '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            x = '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                        return x;
                    },
                },
                /*{
                     "render": function (data, type, full) {
                         var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a>';
                         aksi += '<a class="btn btn-icon-only blue" id="btn-status""><i class="fa fa-exchange"></i></a>';
                         //aksi += '<a class="btn btn-icon-only red" id="btn-hapus"><i class="fa fa-close"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                },*/
                {
                    "data": "STATUS",
                    "class": "dt-body-center",
                    "render": function (data, type, row) {
                        var x = null;
                        if (data == "1") {
                            x = '<a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a>';
                            x += '<a class="btn btn-icon-only blue" id="btn-clones"><i class="fa fa-files-o"></i></a>';
                            x += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                        } else {
                            x = '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                        }
                        return x;
                    },
                },

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20],
                [5, 10, 15, 20]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-role').DataTable();
            var data = table.row(baris).data();
            idUser = data["ID"];
            xStatus = data["STATUS"];
            //console.log(xStatus);
            //console.log("----------------");
            if(xStatus==1){
                xStatus = "0";
                //console.log("IF");
                //console.log(xStatus);
            } else {
                //console.log("ELSE");
                xStatus = "1";
                //console.log(xStatus);
            }
            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'

            }).then(function (isConfirm) {
                if (isConfirm) {

                    var param = {
                        ID: idUser,
                        STATUS: xStatus
                    };

                    //console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/User/EditStatus1",
                        timeout: 30000
                    });


                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var idUser = "";
    var xStatus = "";
    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-role').DataTable();
            var data = table.row(baris).data();
            idUser = data["ID"];
            xStatus = data["STATUS"];
            window.location = "/User/EditUser/" + idUser;
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/User";
        });
    }

    var update = function () {
        $('#btn-update').click(function () {
            var xID = $('#txtParam').val();
            var xNIK = $('#pUserLogin').val();
            var xUserName = $('#pUserName').val();
            var xBE = $('#BE_NAME').val();
            var xProfitCenter = $('#PROFIT_CENTER_ID').val() == "@ViewBag.PROFIT_CENTER_ID" ? "" : $('#PROFIT_CENTER_ID').val();
            var xEmail = $('#pEmail').val();
            var xPhone = $('#pPhone').val();
            var xRole = $('#ROLE_NAME').val();
             

            if (xNIK && xUserName && xEmail && xPhone) {
                var param = {
                    ID: parseInt(xID),
                    USER_LOGIN: xNIK,
                    USER_NAME: xUserName,
                    USER_EMAIL: xEmail,
                    USER_PHONE: xPhone,
                    KD_CABANG: parseInt(xBE),
                    PROFIT_CENTER_ID: xProfitCenter,
                    ROLE_ID: xRole
                };
                 
                var getTemp = "";
                getTemp = tempRoleID[0];
                if (tempRoleID[0] == xRole) {
                    //console.log("ROLE SAMA DENGAN TEMP ROLE");
                    //update data
                    var x = document.getElementsByName("myCheckbox");
                    var i;
                    //console.log(x.length);
                    var xDATA = new Array(x.length);
                    var xStatus = "0";
                    for (i = 0; i < x.length; i++) {
                        if (x[i].checked) {
                            ////console.log("Checked" + x[i].value);
                            xStatus = "1";
                        } else {
                            ////console.log("UnChecked" + x[i].value);
                            xStatus = "0";
                        }
                        xDATA[i] = {
                            xID: xID,
                            MENU_ID: x[i].value,
                            STATUS: xStatus,
                            ROLE_ID: xRole
                        }
                        //console.log(xDATA[i]);

                    }
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(xDATA),
                        method: "POST",
                        url: "/User/EditStatusMenu1",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        //if (data.status === 'S') {
                        //    swal('Success', data.message, 'success').then(function (isConfirm) {
                        //        window.location.href = '/User';
                        //    });
                        //} else {
                        //    swal('Failed', data.message, 'error').then(function (isConfirm) {
                        //        window.location.href = '/User';
                        //    });
                        //}
                    });
                } else if (tempRoleID[0] != xRole) {
                    //console.log("ROLE TIDAK SAMA DENGAN TEMP ROLE");
                    //tarik semua data
                    var xDATA1 = {
                        xID: xID,
                        STATUS: xStatus,
                        ROLE_ID: tempRoleID[0],
                        ROLE_ID2: xRole
                    }

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(xDATA1),
                        method: "POST",
                        url: "/User/DeleteUpdateMenu1",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                    //    if (data.status === 'S') {
                            
                    //        swal('Success', data.message, 'success').then(function (isConfirm) {
                    //            window.location.href = '/User';
                    //        });
                    //    } else {
                    //        swal('Failed', data.message, 'error').then(function (isConfirm) {
                    //            window.location.href = '/User';
                    //        });
                    //    }
                    });

                    
                }
                
                /*var xDATA2 = {
                                xID: xID,
                                STATUS: xStatus,
                                ROLE_ID: xRole
                            }

                            App.blockUI({ boxed: true });
                            var req = $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(xDATA2),
                                method: "POST",
                                url: "/User/UpdateMenu1",
                                timeout: 30000
                            });

                            req.done(function (data) {
                                App.unblockUI();
                                if (data.status === 'S') {
                                    swal('Success', data.message, 'success').then(function (isConfirm) {
                                        window.location.href = '/User';
                                    });
                                } else {
                                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                                        window.location.href = '/User';
                                    });
                                }
                            });*/

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/User/EditUser1",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/User';
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location.href = '/User';
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }
    
    var deletedata = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-config').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete \"" + data["REF_DESC_C"] + "\"?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "Configuration/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/User/AddUser';
        });
    }

    var clones = function () {
        $('body').on('click', 'tr #btn-clones', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-role').DataTable();
            var data = table.row(baris).data();
            idUser = data["ID"];
            xStatus = data["STATUS"];
            window.location = '/User/CloneUsers/' + idUser;
        });
    }

    var cbconfiguration = function () {
        $.ajax({
            type: "GET",
            url: "/Configuration/GetDataConfiguration",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_CODE_C + "'>" + jsonList.data[i].REF_CODE_C + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#ref_code_c").append(listItems);
                });

            }
        });
    }

    var simpanconfig = function () {
        $('#btn-simpan').click(function () {
            var REF_CODE = $('#ref_code_c').val();
            var REF_DATA = $('#ref_data').val();
            var REF_DESC = $('#ref_desc_c').val();
            var ATTRIB1 = $('#attrib1').val();
            var VAL1 = $('#val1').val();


            if (REF_CODE && REF_DATA && REF_DESC) {
                var param = {
                    REF_CODE: REF_CODE,
                    REF_DATA: REF_DATA,
                    REF_DESC: REF_DESC,
                    ATTRIB1: ATTRIB1,
                    VAL1:VAL1
                };

                //console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Configuration/AddData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/Configuration';
                        });

                    } else {
                        swal('Failed', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/Configuration';
                        });
                        $('#addConfigModal').modal('hide');
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
                $('#addConfigModal').modal('hide');
            }
        });
    }

    var getLastID = function () {
        var REF_CODE_ID = "";
        $('#ref_code_c').change(function () {
            REF_CODE_ID = $('#ref_code_c').val();
            ////console.log(REF_CODE_ID);
            // Ajax untuk get data terakhir
            $.ajax({
                type: "POST",
                url: "/Configuration/GetLastID",
                data: "{ REF_CODE:'" + REF_CODE_ID + "'}",
                //data: "{ REF_CODE:'SERVICE_GROUP'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += jsonList.data[i].REF_DATA;
                    }
                    ////console.log(listItems);
                    $("#ref_data_id").html('');
                    $("#ref_data_id").val(listItems);
                }
            });

            /*
            $.ajax({ 
                        type: "POST",
                        url: "/TransVB/GetDataServiceCode",
                        data: "{ SERVICE_NAME:'" + inp + "', SERVICE_GROUP:'" + sGroup + "'}",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data, function (el) {
                                if (sGroup == 16124 || sGroup == 16115 || sGroup == 16125) {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: 2,
                                        service_name: el.service_name
                                    };
                                }
                                else {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: el.tax_code,
                                        service_name: el.service_name
                                    };
                                }
                                
                            }));
                        }
                    });
            */
        });
    }

    var roleOnChange = function () {

        $('#ROLE_NAME').on('change', function () {
            getLengthRoleMenu();
        });
    }

    var tempRoleID = [];
    tempRoleID = new Array();
    var lengthData = "";
    var getLengthRoleMenu = function () {
        var xLength = "";
        var xRole = $('#ROLE_NAME').val();
        var myNode1 = document.getElementById("class1");
        var myNode2 = document.getElementById("class2");
        
        if (xRole != null) {
            //console.log("ready!");
            //console.log(tempRoleID);
            tempRoleID.push(xRole);
            while (myNode1.firstChild) {
                myNode1.removeChild(myNode1.firstChild);
            }
            while (myNode2.firstChild) {
                myNode2.removeChild(myNode2.firstChild);
            }
            $.ajax({
                contentType: "application/json",
                data: "{ xIDRole:'" + xRole + "'}",
                method: "POST",
                url: "/User/getRoleMenu",

                success: function (data) {
                    var xdata = JSON.stringify(data);
                    var jeson = data;
                    lengthData = data;

                    var xPanjang = data.length;
                    var xMod = Number(data.length) % 2;
                    var xSelisih = "";
                    var xHasil = "";
                    var xBatas = "";
                    if (Number(xMod) > Number(0)) {
                        xSelisih = (Number(xPanjang) - Number(xMod)) / 2;
                        xHasil = Number(xSelisih) + 1;
                        xBatas = xHasil;
                        //console.log(xBatas);

                    } else {
                        xSelisih = Number(xPanjang) / 2;
                        xBatas = xSelisih;
                        //console.log(xBatas);
                    }

                    for (var i = 0; i < jeson.length; i++) {
                        var obj = jeson[i];

                        var xRoleID = "";
                        var xRoleNAME = obj.ROLE_NAME;
                        var xMenuID = obj.MENU_ID;
                        var xMenuNAMA = obj.MENU_NAMA;
                        var xMenuParentID = obj.MENU_PARENT_ID;
                        var xMenuParentNAMA = obj.MENU_PARENT_NAMA;
                        var xOrderedBY = obj.ORDERED_BY;
                        var xAppID = obj.APP_ID;
                        var xSTATUS = obj.STATUS;
                        var xValidasi = "0";
                        var xCtr = 0;

                        //declare dynamic checkbox and name
                        var xVal = xMenuID;
                        var xLabel = "<label><b>" + xMenuNAMA + "</b></label><br>";
                        var xChecked = "checked='checked'";
                        var cBox = "<input type='checkbox' id='id"+ i + "' value='" + xVal + "' name='myCheckbox' />&nbsp;&nbsp;<label>" + xMenuNAMA + "</label><br>";
                        var cBox2 = "<input type='checkbox' id='id"+ i + "' value='" + xVal + "' name='myCheckbox' />&nbsp;&nbsp;<label><b>" + xMenuNAMA + "</b></label><br>";

                        if (xMenuParentID == 0) { //jika ditemukan "ROOT/0"
                            ////console.log("PARENT: " + xMenuNAMA);

                            for (var j = 0; j < jeson.length; j++) {
                                var objct = jeson[j];
                                var cMenuParentID = objct.MENU_PARENT_ID;
                                var cMenuID = obj.MENU_ID;
                                if (cMenuID == cMenuParentID) {
                                    //punya anak
                                    ////console.log("+++++++Has Child");
                                    ////console.log(cMenuID + " = " + cMenuParentID);
                                    ////console.log("^^^^SAMA^^^^");
                                    xValidasi = "1";
                                    xCtr++;
                                    continue;
                                } else {
                                    //single
                                    ////console.log("+++++++Parent");
                                    ////console.log(cMenuID + " = " + cMenuParentID);
                                    continue;
                                }
                            }

                            if (i < 6 || i < xBatas) {
                                //apped class1
                                if (xValidasi == "0" && xCtr == 0) {
                                    $('#class1').append(cBox2);
                                } else {
                                    $('#class1').append(xLabel);
                                }
                            } else {
                                //apped class2
                                if (xValidasi == "0" && xCtr == 0) {
                                    $('#class2').append(cBox2);
                                } else {
                                    $('#class2').append(xLabel);
                                }
                            }
                            continue; //lanjutkan loop
                        } else if (xMenuParentID != 0) { //jika tidak ditemukan "ROOT/0"
                            ////console.log("---Child: " + xMenuNAMA);
                            for (var j = 0; j < jeson.length; j++) {
                                var objct = jeson[j];
                                var cMenuParentID = objct.MENU_PARENT_ID;
                                var cMenuID = obj.MENU_ID;
                                if (cMenuID == cMenuParentID) {
                                    //punya anak
                                    ////console.log("+++++++Has Child");
                                    ////console.log(cMenuID + " = " + cMenuParentID);
                                    ////console.log("^^^^SAMA^^^^");
                                    xValidasi = "1";
                                    xCtr++;
                                    continue;
                                } else {
                                    //single
                                    ////console.log("+++++++Parent");
                                    ////console.log(cMenuID + " = " + cMenuParentID);
                                    continue;
                                }
                            }
                            if (i < 6 || i < xBatas) {
                                //apped class1
                                if (xValidasi == "0" && xCtr == 0) {
                                    //$('#class1').append(cBox2);
                                    $('#class1').append(cBox);
                                } else {
                                    $('#class1').append(xLabel);
                                }
                            } else {
                                //apped class2
                                if (xValidasi == "0" && xCtr == 0) {
                                    //$('#class2').append(cBox2);
                                    $('#class2').append(cBox);
                                } else {
                                    $('#class2').append(xLabel);
                                }
                            }
                            continue; //lanjutkan loop
                        }
                    }

                    //console.log("-----------");
                    checkedUnchecked();
                    //CHECK
                        //document.getElementById("id"+i).checked = true;

                    //UNCHECK
                        //document.getElementById("id"+i).checked = false;
                    /*var xPanjang = data.length;
                    var xMod = Number(data.length) % 2;
                    var xSelisih = "";
                    var xHasil = "";
                    if(Number(xMod) > Number(0)){
                        xSelisih = (Number(xPanjang) - Number(xMod)) / 2;
                        //console.log(xSelisih);
                        xHasil = Number(xSelisih) + 1;
                        for (var i = 1; i <= xHasil; i++) {
                            $('#class1').append("<label><input type='checkbox' id='id1'" + i + "' value='' name='myCheckbox' /> </label><br>");
                        }
                        for (var i = 1; i <= xSelisih; i++) {
                            $('#class2').append("<label><input type='checkbox' id='id2'" + i + "' value='' name='myCheckbox' /> </label><br>");
                        }
    
                    } else {
                        xSelisih = Number(xPanjang) / 2;
                        //console.log(xSelisih);
                        for (var i = 0; i < xSelisih; i++) {
                            //console.log("Count:" + i);
                        }
                    }*/
                }
            });
        }
    }

    var checkedUnchecked = function () {
        var xID = $('#txtParam').val();
        var xRole = $('#ROLE_NAME').val();

        $.ajax({
            contentType: "application/json",
            data: "{ xID:'" + xID + "', xIDRole:'" + xRole + "'}",
            method: "POST",
            url: "/User/getUserMenu",

            success: function (data) {
                if(data.length>0){
                    var xdata = JSON.stringify(data);
                    var jeson = data;

                    for (i = 0; i < jeson.length; i++) {
                        var obj = jeson[i];

                        var xUserID = obj.USER_ID;
                        var xMenuID = obj.MENU_ID;
                        var xMenuParentID = obj.MENU_PARENT_ID;
                        var xOrderedBY = obj.ORDERED_BY;
                        var xAppID = obj.APP_ID;
                        var xSTATUS = obj.STATUS;
                        var xRoleID = obj.ROLE_ID;
                        var xCDate = obj.CREATION_DATE;

                        var x = document.getElementById("id" + i);
                        if (xSTATUS == "1") {
                            //$("#id" + i).prop('checked', true);
                            $("#id" + i).prop("checked", true);
                            $("#id" + i).attr("disabled", false);
                        } else if (xSTATUS == "0") {
                            $("#id" + i).attr("checked", false);
                            //$("#id" + i).prop('checked', false);
                        } else {
                            $("#id" + i).prop("checked", true);
                            $("#id" + i).attr("disabled", true);
                        }
                    }
                } else {
                    for (var i = 0; i < lengthData.length; i++) {
                        $("#id" + i).prop("checked", true);
                        $("#id" + i).prop("disabled", true);
                    }
                }
                
            }
            
        });
        
    }

    var nStatus = true; //untuk status tambah detail
    var nEditing = null;
    var nNew = false;
    var editData = function () {

        var oTable = $('#table-role').dataTable();

        var DetailLookup = function (oTable, nRow) {
            var data = oTable.fnGetData(nRow);
            $("#txtUserLogin").val(data["USER_LOGIN"]);
            $("#txtUserName").val(data["USER_NAME"]);
            $("#BE_NAME").val(data["BE_NAME"]);
            $("#BE_NAME").append("<option value='" + data["BE_NAME"] + "' selected></option>");
            $("#PROFIT_CENTER_ID").val(data["PROFIT_CENTER_ID"]);
            $("#PROFIT_CENTER_ID").append("<option value='" + data["PROFIT_CENTER_ID"] + "' selected></option>");
            $("#txtEmail").val(data["USER_EMAIL"]);
            $("#txtPhone").val(data["USER_PHONE"]);
            $("#ROLE_NAME").val(data["ROLE_NAME"]);
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<center><input id="txtUserLogin" type="text" class="form-control" ></center>';
            jqTds[1].innerHTML = '<center><input id="txtUserName" type="text" class="form-control input-xsmall" ></center>';
            jqTds[2].innerHTML = '<center>@Html.DropDownListFor(m => m.FDDBranch, new SelectList(Model.DDBranch, "BRANCH_ID", "BE_NAME"), "-- Choose Business Entity --", new { @class = "form-control", @id = "BE_NAME" })</center>';
            jqTds[3].innerHTML = '<center>@Html.DropDownListFor(m => m.FDDProfitCenter, new SelectList(Model.FDDProfitCenter, "PROFIT_CENTER_ID", "PROFIT_CENTER_ID"), "-- Choose Profit Center --", new { @class = "form-control", @id = "PROFIT_CENTER_ID" })</center>';
            jqTds[4].innerHTML = '<center><input id="txtEmail" type="text" class="form-control input-xsmall" ></center>';
            jqTds[5].innerHTML = '<center><input id="txtPhone" type="text" class="form-control" ></center>';
            jqTds[6].innerHTML = '<center>@Html.DropDownListFor(m => m.FDDPosition, new SelectList(Model.DDPosition, "ROLE_ID", "ROLE_NAME"), "-- Choose Role --", new { @class = "form-control", @id = "ROLE_NAME" })</center>';
            jqTds[8].innerHTML = '<center><a class="btn btn-icon-only green" href="#" id="btn-sUbahD"><i class="fa fa-save"></i></a>&nbsp; ' +
                                 '<a class="btn btn-icon-only red" href="#" id="btn-cUbahD"><i class="fa fa-ban"></i></a></center>';

            /*$("#txtRoleProperty").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            $("#txtRoleProperty").inputmask("numeric", {
                min: 0,
                max: 999
            });*/
        }

        function saveRow(oTable, nRow) {
            App.blockUI({ boxed: true });
            var data = oTable.fnGetData(nRow);
            var rtable = $('#table-role').DataTable();
            var jqInputs = $('input', nRow);
            var jqSelects = $('select', nRow);

            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqSelects[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqSelects[5].value, nRow, 6, false);
            oTable.fnUpdate('<center><a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a></center>', nRow, 8, false);
            oTable.fnDraw();

            if (ROLE_NAME && PROPERTY_ROLE) {
                var param = {
                    ROLE_ID: data["ROLE_ID"],
                    ROLE_NAME: jqInputs[0].value,
                    PROPERTY_ROLE: jqInputs[1].value,
                };

                //console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "Role/EditDataRole",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            rtable.ajax.reload();
                            nStatus = true;
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            rtable.ajax.reload();
                        });
                        $('#addRoleModal').modal('hide');
                    }
                });
            } else {
                //$('#addRoleModal').modal('hide');
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
        }

        $('body').on('click', 'tr #btn-ubah', function () {
            if (nStatus == true) {
                ////console.log("Ubah 1");
                ////console.log(nStatus);

                var nRow = $(this).parents('tr')[0];
                editRow(oTable, nRow);
                nStatus = false

                DetailLookup(oTable, nRow);

            } else {
                swal('Peringatan', "Anda belum selesai melakukan inputan.", 'warning');
                ////console.log("Ubah 2");
                ////console.log(nStatus);
            }
        });

        $('body').on('click', 'tr #btn-sUbahD', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            if (nStatus == true) {
                ////console.log("3");
                ////console.log(nStatus);
            } else {
                saveRow(oTable, nRow);
                ////console.log("4");
                ////console.log(nStatus);
            }
        });

        $('body').on('click', 'tr #btn-cUbahD', function (e) {
            e.preventDefault();
            var rTable = $('#table-role').DataTable();
            nStatus = true;
            //var nRow = $(this).parents('tr')[0];
            //var aData = oTable.fnGetData(nRow);
            //oTable.fnUpdate("", nRow, 2, false);

            rTable.ajax.reload();
        });
    }
    $('#filter').click(function () {
        ////console.log('bambosz  ' + $('#BE_NAME').val())
        initTable();
    });
    return {
        init: function () {
            initTable();
            add();
            getLengthRoleMenu();
            roleOnChange();
            changeBe();
            //editData();
            ubah();
            clones();
            //deletedata();
            batal();
            update();
            //cbconfiguration();
            //simpanconfig();
            ubahstatus();
            //getLastID();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        User.init();
    });
}