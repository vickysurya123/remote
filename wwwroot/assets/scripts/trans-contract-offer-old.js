﻿var TransContractOffer = function () {

    var initTableTransContractOfferCreated = function () {

        $('#table-trans-created').DataTable({

            "ajax":
            {
                "url": "TransContractOffer/GetDataTransContractOfferCreated",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE == '1') {
                            return '<span class="label label-sm label-success"> ACTIVE </span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> INACTIVE </span>';
                        }

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.OFFER_STATUS == 'Created') {
                            return '<span class="label label-sm label-primary">Created</span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> ' + full.OFFER_STATUS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE == '1' && full.OFFER_STATUS == 'Created') {
                            var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                            aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                            aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release"><i class="fa fa-external-link"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Ganarate Form" id="btn-export-ijinprinsip"><i class="fa fa-file-word-o"></i></a>';

                        }
                        else {
                            if (full.ACTIVE == '0' || full.COMPLETED_STATUS == '4') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-status" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferWorkflow = function () {
        $('#table-trans-workflow').DataTable({

            "ajax":
            {
                "url": "TransContractOffer/GetDataTransContractOfferWorkflow",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.TOP_APPROVED_LEVEL + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentWorkflowList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-workflow-list"><i class="fa fa-paperclip"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-workflow"><i class="fa fa-eye"></i></a>';

                        return aksi;

                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferRevised = function () {

        var ROLE_USER = $('#ROLE_USER').val();

        $('#table-trans-reserved-list').DataTable({

            "ajax":
            {
                "url": "TransContractOffer/GetDataTransContractOfferRevised",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        if (full.TOP_APPROVED_LEVEL <= '50') {
                            if (ROLE_USER == '6' || ROLE_USER == '8' || ROLE_USER == '10' || ROLE_USER == '4') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                                aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                                aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            else {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            return aksi;
                        }
                        else {
                            if (ROLE_USER == '6' || ROLE_USER == '8' || ROLE_USER == '10') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            else if (ROLE_USER == '7') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                                aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            else if (ROLE_USER == '12') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            else if (ROLE_USER == '11') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            else {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';

                                aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                                aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                            return aksi;
                        }
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferApproved = function () {
        $('#table-trans-approved-list').DataTable({

            "ajax":
            {
                "url": "TransContractOffer/GetDataTransContractOfferApproved",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentApprovedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-approved-list"><i class="fa fa-paperclip"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-approved"><i class="fa fa-eye"></i></a>';

                        return aksi;

                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initDetilTransContractOfferCreated = function () {
        $('body').on('click', 'tr #btn-detail-created', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSalesBased = $('#sales-based').DataTable();
            tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            //console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_tanah = full.LUAS_TANAH;
                            var fLuas_tanah = luas_tanah.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_tanah;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_bangunan = full.LUAS_BANGUNAN;
                            var fLuas_bangunan = luas_bangunan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_bangunan;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            //$('#btn-view-condition').click(function () {
            //    if ($.fn.DataTable.isDataTable('#table-detil-condition')) {
            //        $('#table-detil-condition').DataTable().destroy();
            //    }
            //});



            //$('#table-detil-condition').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "CALC_OBJECT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CONDITION_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MONTHS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATISTIC",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT_PRICE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "AMT_REF",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FREQUENCY",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "START_DUE_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MANUAL_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FORMULA",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MEASUREMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "LUAS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NJOP_PERCENT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_RULE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL_NET_VALUE",
            //            "class": "dt-body-center"
            //        },


            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Reporting Rule
            //$('#reporting-rule').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailReportingRule/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "REPORTING_RULE_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TERM_OF_REPORTING_RULE",
            //            "class": "dt-body-center"
            //        }
            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

        });
    }

    var initDetilTransContractOfferWorkflow = function () {
        $('body').on('click', 'tr #btn-detail-workflow', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSalesBased = $('#sales-based').DataTable();
            tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            //console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*"data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            //$('#btn-view-condition').click(function () {
            //    if ($.fn.DataTable.isDataTable('#table-detil-condition')) {
            //        $('#table-detil-condition').DataTable().destroy();
            //    }
            //});



            //$('#table-detil-condition').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "CALC_OBJECT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CONDITION_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MONTHS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATISTIC",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT_PRICE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "AMT_REF",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FREQUENCY",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "START_DUE_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MANUAL_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FORMULA",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MEASUREMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "LUAS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NJOP_PERCENT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_RULE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL_NET_VALUE",
            //            "class": "dt-body-center"
            //        },


            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Reporting Rule
            //$('#reporting-rule').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailReportingRule/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "REPORTING_RULE_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TERM_OF_REPORTING_RULE",
            //            "class": "dt-body-center"
            //        }
            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

        });
    }

    var initDetilTransContractOfferRevised = function () {
        $('body').on('click', 'tr #btn-detail-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSalesBased = $('#sales-based').DataTable();
            tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            //console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*
                        "data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            //$('#btn-view-condition').click(function () {
            //    if ($.fn.DataTable.isDataTable('#table-detil-condition')) {
            //        $('#table-detil-condition').DataTable().destroy();
            //    }
            //});



            //$('#table-detil-condition').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "CALC_OBJECT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CONDITION_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MONTHS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATISTIC",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT_PRICE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "AMT_REF",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FREQUENCY",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "START_DUE_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MANUAL_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FORMULA",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MEASUREMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "LUAS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NJOP_PERCENT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_RULE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL_NET_VALUE",
            //            "class": "dt-body-center"
            //        },


            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Reporting Rule
            //$('#reporting-rule').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailReportingRule/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "REPORTING_RULE_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TERM_OF_REPORTING_RULE",
            //            "class": "dt-body-center"
            //        }
            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

        });
    }

    var initDetilTransContractOfferApproved = function () {
        $('body').on('click', 'tr #btn-detail-approved', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approved-list').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSalesBased = $('#sales-based').DataTable();
            tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            //console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*"data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            //$('#btn-view-condition').click(function () {
            //    if ($.fn.DataTable.isDataTable('#table-detil-condition')) {
            //        $('#table-detil-condition').DataTable().destroy();
            //    }
            //});



            //$('#table-detil-condition').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "CALC_OBJECT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CONDITION_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MONTHS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATISTIC",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT_PRICE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "AMT_REF",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FREQUENCY",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "START_DUE_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MANUAL_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FORMULA",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MEASUREMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "LUAS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NJOP_PERCENT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_RULE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL_NET_VALUE",
            //            "class": "dt-body-center"
            //        },


            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Reporting Rule
            //$('#reporting-rule').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailReportingRule/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "REPORTING_RULE_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TERM_OF_REPORTING_RULE",
            //            "class": "dt-body-center"
            //        }
            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

        });
    }

    var releaseWorkflow = function () {
        $('body').on('click', 'tr #btn-release', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var tableWorkflow = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            //console.log(data.CONTRACT_OFFER_NO);

            swal({
                title: 'Warning',
                text: "Are you sure want <B>RELEASE</B> to <B>WORKFLOW</B> ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];

                    var param = {

                        data: CONTRACT_OFFER_NO

                    };
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransContractOffer/ReleaseToWorkflow",
                        timeout: 30000,

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            tableWorkflow.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                });
                            }
                            else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                });
                            }
                        }
                    });
                }
            });

        });
    }

    var releaseWorkflowRevised = function () {
        $('body').on('click', 'tr #btn-release-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var tableWorkflow = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want <B>RELEASE</B> to <B>WORKFLOW</B> ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];

                    var param = {

                        data: CONTRACT_OFFER_NO

                    };
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransContractOffer/ReleaseToWorkflow",
                        timeout: 30000,

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            tableWorkflow.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                });
                            }
                            else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                });
                            }
                        }
                    });
                }
            });

        });
    }

    var edit = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

            window.location = "/TransContractOffer/EditOffer/" + data["CONTRACT_OFFER_NO"];
        });
    }

    var editRevised = function () {
        $('body').on('click', 'tr #btn-ubah-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');
            console.log(data["CONTRACT_OFFER_NO"])
            window.location = "/TransContractOffer/EditOffer/" + data["CONTRACT_OFFER_NO"];
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransContractOffer/Add';
        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["CONTRACT_OFFER_NO"];
            $('#kode_offer').html(id_attachment);


            $("#ID_ATTACHMENT").val(data["CONTRACT_OFFER_NO"]);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/ContractOffer/' + id_attachment + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentWorkflowList = function () {
        $('body').on('click', 'tr #btn-attachment-workflow-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentWorkflowList = $('#table-trans-workflow').DataTable();
            tableAttachmentWorkflowList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_workflow_list').html(id_attachment);

            $("#ID_ATTACHMENT_WORKFLOWLIST").val(id_attachment);

            $('#table-attachment-workflow-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/ContractOffer/' + id_attachment + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        x += '<a class="btn default btn-xs red" id="delete-attachment-workflow-list"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentRevisedList = function () {
        $('body').on('click', 'tr #btn-attachment-reverse-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentRevisedList = $('#table-trans-reserved-list').DataTable();
            tableAttachmentRevisedList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_revised_list').html(id_attachment);

            $("#ID_ATTACHMENT_REVIESEDLIST").val(id_attachment);

            $('#table-attachment-revised-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/ContractOffer/' + id_attachment + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        x += '<a class="btn default btn-xs red" id="delete-attachment-revised-list"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentApprovedList = function () {
        $('body').on('click', 'tr #btn-attachment-approved-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentApprovedList = $('#table-trans-approved-list').DataTable();
            tableAttachmentApprovedList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approved-list').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_approved_list').html(id_attachment);

            $("#ID_ATTACHMENT_APPROVEDLIST").val(id_attachment);

            $('#table-attachment-approved-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/ContractOffer/' + id_attachment + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        x += '<a class="btn default btn-xs red" id="delete-attachment-approved-list"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }


    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadWorkflowList = function () {
        $('#fileuploadworkflowlist').click(function () {
            $("#fileuploadworkflowlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_WORKFLOWLIST').val();

            $('#fileuploadworkflowlist').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-workflow-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadRevisedList = function () {
        $('#fileuploadrevisedlist').click(function () {
            $("#fileuploadrevisedlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_REVIESEDLIST').val();

            $('#fileuploadrevisedlist').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-revised-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadApprovedList = function () {
        $('#fileuploadapprovedlist').click(function () {
            $("#fileuploadapprovedlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_APPROVEDLIST').val();
            console.log(BE_ID);

            $('#fileuploadapprovedlist').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-approved-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        var uriId = $('#ID_ATTACHMENT').val();
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentWorkflowList = function () {
        var uriId = $('#ID_ATTACHMENT_WORKFLOWLIST').val();
        $('body').on('click', 'tr #delete-attachment-workflow-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-workflow-list').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentWorkflowList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentWorkflowList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentWorkflowList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentRevisedList = function () {
        var uriId = $('#ID_ATTACHMENT_REVIESEDLIST').val();
        $('body').on('click', 'tr #delete-attachment-revised-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-revised-list').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentRevisedList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentRevisedList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentRevisedList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentApprovedList = function () {
        var uriId = $('#ID_ATTACHMENT_APPROVEDLIST').val();
        $('body').on('click', 'tr #delete-attachment-approved-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-approved-list').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentApprovedList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentApprovedList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentApprovedList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }


    var initExportdataijinprinsip = function () {
        $('body').on('click', 'tr #btn-export-ijinprinsip', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var BEID = data["BE_NAMES"];
            var CUSTOMERNAME = data["CUSTOMER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAMES = data["PROFIT_CENTER_NAME"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }


            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];

            var PROFITCENTERNAME = PROFIT_CENTER_NAMES.split('-');
            var PROFITCENTERNAMES = PROFITCENTERNAME[1];
            var PROFIT_CENTER_NAME = PROFITCENTERNAMES.replace('Cabang', '');

            console.log(BE_ID);
            console.log(CUSTOMER_NAME);
            console.log(CONTRACT_USAGE);
            console.log(PROFIT_CENTER_NAME);



            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var CONTRACT_OFFER_TYPE = data["CONTRACT_OFFER_TYPE"];
            var CONTRACT_OFFER_NAME = data["CONTRACT_OFFER_NAME"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var CURRENCY = data["CURRENCY"];
            var OFFER_STATUS = data["OFFER_STATUS"];
            var BE_CITY = data["BE_CITY"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];



            var dataJSON = "{CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) + ",CONTRACT_OFFER_TYPE:" + JSON.stringify(CONTRACT_OFFER_TYPE) + ",BE_ID:" + JSON.stringify(BE_ID) +
                           ",CONTRACT_START_DATE:" + JSON.stringify(CONTRACT_START_DATE) + ",CONTRACT_END_DATE:" + JSON.stringify(CONTRACT_END_DATE) + ",TERM_IN_MONTHS:" + JSON.stringify(TERM_IN_MONTHS) +
                           ",CUSTOMER_NAME:" + JSON.stringify(CUSTOMER_NAME) + ",PROFIT_CENTER_NAME:" + JSON.stringify(PROFIT_CENTER_NAME) + ",CONTRACT_USAGE:" + JSON.stringify(CONTRACT_USAGE) +
                           ",BE_CITY:" + JSON.stringify(BE_CITY) + ",LUAS_TANAH:" + JSON.stringify(LUAS_TANAH) + ",LUAS_BANGUNAN:" + JSON.stringify(LUAS_BANGUNAN) + ",RO_ADDRESS:" + JSON.stringify(RO_ADDRESS) + "}";
            $.ajax({
                type: "POST",
                url: "/TransContractOffer/exportWordIjinprinsip",
                data: dataJSON,
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (response) { },
                error: function (response) {
                    console.log(response);
                }
            });

        });
    }

    return {
        init: function () {
            add();
            initTableTransContractOfferCreated();
            initTableTransContractOfferWorkflow();
            initTableTransContractOfferRevised();
            initTableTransContractOfferApproved();
            initDetilTransContractOfferCreated();
            initDetilTransContractOfferWorkflow();
            initDetilTransContractOfferRevised();
            initDetilTransContractOfferApproved();
            releaseWorkflow();
            releaseWorkflowRevised();
            edit();
            editRevised();
            fupload();
            fuploadWorkflowList();
            fuploadRevisedList();
            fuploadApprovedList();
            deleteAttachment();
            deleteAttachmentWorkflowList();
            deleteAttachmentRevisedList();
            deleteAttachmentApprovedList();
            initAttachment();
            initAttachmentWorkflowList();
            initAttachmentRevisedList();
            initAttachmentApprovedList();
            initExportdataijinprinsip();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContractOffer.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg)$");
        if (!(regex.test(val))) {
            $(this).val('');
            $('#viewModalAttachment').modal('hide');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, and JPG Extension Are Allowed', 'warning');
        }
    });
});