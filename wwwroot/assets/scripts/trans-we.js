﻿var TransWaterElectricity = function () {

    var initTableWETransaction = function () {
        $('#table-transwe').DataTable({

            "ajax":
            {
                "url": "TransWE/GetDataWETransnew",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "INSTALLATION_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE",
                    "class": "dt-body-center"

                },
                {
                    "data": "PROFIT_CENTER",
                    "class": "dt-body-center"

                },
                {
                    "data": "CUSTOMER_NAME"
                },
                {
                    "data": "POWER_CAPACITY",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_ADDRESS"
                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-add-we-trans"><i class="fa fa-plus"></i></a>';
                         //aksi  += '<a data-toggle="modal" href="#viewVBPTransModal" class="btn btn-icon-only green" id="btn-detail-we-trans"><i class="fa fa-eye"></i></a>';
                         
                         //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                         //aksi += '<a data-toggle="modal" href="#viewBEModal" class="btn btn-icon-only green" id="btn-detil"><i class="fa fa-eye"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });

    }

    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail-vbp-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb').DataTable();
            var data = table.row(baris).data();

            $("#DETAIL_PROFIT_CENTER").val(data["PROF_CENTER"]);
            $("#DETAIL_POSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAIL_SERVICE_GROUP").val(data["SERVICES_GROUP"]);
            $("#DETAIL_CUSTOMER_MDM").val(data["COSTUMER_MDM"]);
            $("#DETAIL_CUSTOMER_NAME").val(data["COSTUMER_NAME"]);
            $("#DETAIL_INSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);
            $("#DETAIL_CUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            $("#DETAIL_TOTAL").val(data["TOTAL"]);

            var id_vb = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransVB/GetDataTransaction/" + id_vb,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "VB_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SERVICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CM_PRICE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TAX_AMOUNT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SUB_TOTAL",
                        "class": "dt-body-center"

                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });

        });
    }

    var addTransWE = function () {
        $('body').on('click', 'tr #btn-add-we-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var installationType = data['INSTALLATION_TYPE'];
            console.log(installationType);

            if (installationType == 'A-Sambungan Air') {
                window.location = "/TransWE/AddTransWe/" + data["ID"];
            }
            else {
                window.location = "/TransElectricity/AddTransElectricity/" + data["ID"];
            }

        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWE";
        });
    }

    return {
        init: function () {
            initTableWETransaction();
            initDetilTransaction();
            addTransWE();
            batal();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricity.init();
    });
}