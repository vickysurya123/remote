﻿var fileWords;
var DataSurat = function () {
    $('#isi_surat').summernote({
        placeholder: '',
        tabsize: 2,
        height: 320,
        focus: true,                  // set focus to editable area after initializing summernote  
        callbacks: {
            onImageUpload: function (files) {
                for (let i = 0; i < files.length; i++) {
                    uploadImage(files[i]);
                }
            }
        }
    });

    $('#isi_surat').summernote('disable');

    window.openModalViewMaps = function (RO_NUMBER) {
        $('#viewMaps').modal('show');

        var req = $.ajax({
            contentType: "application/json",
            data: "RO_NUMBER=" + RO_NUMBER,
            method: "get",
            url: "/RentalObject/PinPoint2",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                //MapsView(data.message.LATITUDE, data.message.LONGITUDE)
                var element = document.getElementById("view-maps");
                var mapTypeIds = [];
                for (var type in google.maps.MapTypeId) {
                    mapTypeIds.push(google.maps.MapTypeId[type]);
                }
                mapTypeIds.push("satellite");

                maps = new google.maps.Map(element, {
                    center: new google.maps.LatLng(data.message.LATITUDE, data.message.LONGITUDE),
                    zoom: 16,
                    mapTypeId: "satellite",
                    // disable the default User Interface
                    disableDefaultUI: true,
                    // add back fullscreen, streetview, zoom
                    zoomControl: true,
                    streetViewControl: true,
                    //fullscreenControl: true,
                    center: new google.maps.LatLng(data.message.LATITUDE, data.message.LONGITUDE),
                    mapTypeControlOptions: {
                        mapTypeIds: mapTypeIds
                    }
                });

                maps.mapTypes.set("satellite", new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        // See above example if you need smooth wrapping at 180th meridian
                        return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                    },
                    tileSize: new google.maps.Size(256, 256),
                    name: "OpenStreetMap",
                    maxZoom: 18
                }));

                var req = $.ajax({
                    contentType: "application/json",
                    data: "RO_NUMBER=" + RO_NUMBER,
                    method: "get",
                    url: "/RentalObject/GetMarker2",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {

                        var marks = data.message;
                        var flightPlanCoordinates = [];
                        for (var i = 0; i < marks.length; i++) {
                            flightPlanCoordinates.push({ lat: parseFloat(marks[i].LATITUDE), lng: parseFloat(marks[i].LONGITUDE) });
                        }
                        flightPlanCoordinates.push({ lat: parseFloat(marks[0].LATITUDE), lng: parseFloat(marks[0].LONGITUDE) });
                        var flightPath = new google.maps.Polyline({
                            path: flightPlanCoordinates,
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        flightPath.setMap(maps);
                    }
                });
            } else {
                swal('Failed', data.message, 'error');
            }
        });
    }

    return {
        init: function () {
        }
    };
}();
var TransContractOffer = function () {

    var showOldContract = function (ro, cond) {
        ////console.log(ro, cond)
        var all_value = 0;
        $('.DETAIL_RO').html('');
        for (var i = 0; i < cond.length; i++) {

            var net_val = cond[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var price = cond[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = cond[i].CALC_OBJECT;

            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_cond = calc_ob.split(' ')[1];

                for (var j = 0; j < ro.length; j++) {
                    ro_data = ro[j];
                    ////console.log(ro_data);
                    var ro_id = ro_data.OBJECT_ID;
                    if (ro_id == ro_cond) {
                        var luas = Number(ro_data.LAND_DIMENSION) + Number(ro_data.BUILDING_DIMENSION);
                        var text = $('.DETAIL_RO').html() + '<tr><td>' + ro_data.OBJECT_ID + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO').html(text);
                    }
                }
            }
        }
        $('#CONTRACT_VALUE').val(all_value);
        $('.CONTRACT_VALUE').html(sep1000(all_value, true));
    }
    
    var initTableTransApprovalException = function () {

        $('#table-trans-approval-exception-list').DataTable({

            "ajax":
            {
                "url": "/Approval/GetDataTransApprovalException",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE == '1') {
                            return '<span class="label label-sm label-success"> ACTIVE </span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> INACTIVE </span>';
                        }

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.OFFER_STATUS == 'Created') {
                            return '<span class="label label-sm label-success">Created</span>';
                        }
                        else {
                            return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                }, {
                    "render": function (data, type, full) {
                        //console.log(full.ACTIVE);
                        
                        var aksi = '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Approve" id="btn-approve"><i class="fa fa-check"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewApprovalExceptionModal" data-toggle="tooltip" title="Details" class="btn btn-icon-only green" id="btn-detail-view"><i class="fa fa-eye"></i></a>';
                         
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initDetilTransApprovalException = function () {
        $('body').on('click', 'tr #btn-detail-view', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approval-exception-list').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSurat = $('#table-detail-surat').DataTable();
            tableSurat.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#SERTIFIKAT").val(data["SERTIFIKAT"]);
            $("#CURRENCY_RATE").val(data["CURRENCY_RATE"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];

            $('#dataParaf').empty();
            $('#dataHistory').empty();
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            var nextParaf = '';
            req.done(function (data) {
                App.unblockUI();
                data.history.forEach(myFunction);
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }
                data.nextParaf.forEach(nextDataParaf);
                function nextDataParaf(item, index) {
                    nextParaf += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'></td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:70px;width:390px'><div style='height: 70px;' class='form-group'>" +
                        "<label><u>" + item.STATUS + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> "+ item.USER_NAME + " " + item.ROLE_NAME + "</b></td></tr>";
                }
                $('#dataHistory').append(isiTable);
                $('#dataParaf').append(nextParaf);
            });
            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            //var template = tpl.html().replace(/__INDEX__/g, data.dataSurat.ID);
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/'+data+'" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
            ////console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a data-toggle="modal" onClick="openModalViewMaps(' + full.OBJECT_ID + ')"  data-toggle="tooltip" title="MAP RO" id="btn-attachment">' + full.OBJECT_NAME + '</a>';
                            return aksi;
                        },
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_tanah = full.LUAS_TANAH;
                            var fLuas_tanah = luas_tanah.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_tanah;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_bangunan = full.LUAS_BANGUNAN;
                            var fLuas_bangunan = luas_bangunan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_bangunan;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });


            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            //console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            ////console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrCond = data.data;
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrRo = data.data;
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }


    var approve = function () {
        $('body').on('click', 'tr #btn-approve', function () {
            $tr = $(this).closest("tr");

            swal({
                title: 'Choose Top Approval',
                type: 'question',
                input: 'radio',
                inputOptions: {
                    '61' : 'CEO JAWA',
                    '88' : 'REGIONAL HEAD',
                },
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                inputValidator: function(value) {
                  return new Promise(function(resolve, reject) {
                    if (value) {
                      resolve();
                    } else {
                      reject('You must to choose something!');
                    }
                  });
                }
            }).then(function(result) {
                if (result) {
                    var data = $('#table-trans-approval-exception-list').DataTable().row($tr).data();
                    var param = {
                        TOP_APPROVAL: result,
                        CONTRACT_OFFER_NO: data.CONTRACT_OFFER_NO,
                    }
                    console.log(param.TOP_APPROVAL);
                    App.blockUI({ boxed: true });
                    $.ajax({
                        dataType: "json",
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/Approval/SetApprove",
                        async:true,

                        success: function (data) {
                            // console.log(data);
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal('Success', 'You have approval Contract Offer : ' + param.CONTRACT_OFFER_NO + '', 'success').then(function (isConfirm) {
                                    window.location = "/Approval";
                                });
                            }
                            else {
                                swal('Failed', 'Sorry, This approval fail.', 'error').then(function (isConfirm) {
                                });
                            }
                        },
                        error: function (request, status, error) {
                            App.unblockUI();
                            console.log(request, status, error);
                        }
                    });
                }
            })
        });
    }

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    return {
        init: function () {
            initTableTransApprovalException();
            initDetilTransApprovalException();
            approve();
            datePicker();
        }
    };
}();

function Clicks() {
    //console.log('Yess ' + fileWords);
    var jsonData = "{fileName:'" + fileWords + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}


function coHitungArea() {
    var con = $('#table-detil-object').dataTable();
    var all_area = 0;
    for (var i = 0; i < con.fnGetData().length; i++) {
        var area1 = con.fnGetData(i).BUILDING_DIMENSION;
        var area2 = con.fnGetData(i).LAND_DIMENSION;
        all_area = all_area + Number(area1) + Number(area2);
        ////console.log(all_area);
    }
    ////console.log(all_area);
    $('.LAND_AREA_NEW').html(all_area.toLocaleString('en-US'));
}

function coHitungValue() {
    var con = $('#table-detil-condition').DataTable();
    var all_value = 0;
    //var data = con.data()
    ////console.log(data);
    $('.DETAIL_RO_NEW').html('');
    for (var i = 0; i < con.data().count(); i++) {
        var row = con.row(i).data();
        ////console.log(row);
        var net_val = row.TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var price = row.UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var calc_ob = row.CALC_OBJECT;
        ////console.log(net_val, calc_ob);
        if (calc_ob != 'Header Document') {
            all_value = all_value + parseFloat(net_val.split(',').join(''));
            var ro_number = calc_ob.split(' ')[1];
            var elems = $('#table-detil-object').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() == ro_number) {
                    ////console.log(index);
                    ro_data = $('#table-detil-object').DataTable().row(index).data();
                    //console.log(ro_data);
                    var luas = Number(ro_data.LUAS_BANGUNAN) + Number(ro_data.LUAS_TANAH);
                    var text = $('.DETAIL_RO_NEW').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CURRENCY').val() + '</td></tr>';
                    $('.DETAIL_RO_NEW').html(text)
                    //console.log(text);
                }
            });
        }
    }
    ////console.log(text);
    $('.CONTRACT_VALUE_NEW').html(all_value.toLocaleString('en-US'));
}

function coHitungPeriod() {
    var value = $('#TERM_IN_MONTHS').val();
    var start = $('#CONTRACT_START_DATE').val().replace(/\//g, ".");
    var end = $('#CONTRACT_END_DATE').val().replace(/\//g, ".");
    $('.CONTRACT_START_NEW').html(start);
    $('.CONTRACT_END_NEW').html(end);
    $('.CONTRACT_PERIOD_NEW').html(value);
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContractOffer.init();
        DataSurat.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg|zip)$");
        if (!(regex.test(val))) {
            $(this).val('');
            $('#viewModalAttachment').modal('hide');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, JPG, and ZIP Extension Are Allowed', 'warning');
        }
    });
});



function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}
