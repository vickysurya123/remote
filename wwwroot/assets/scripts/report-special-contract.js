﻿var handleBootstrapSelect = function () {
    $('.bs-select').selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
}

var ReportSpecialContract = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportSpecialContract/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-special-contract');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');
            if ($.fn.DataTable.isDataTable('#table-report-special-contract')) {
                $('#table-report-special-contract').DataTable().destroy();
            }
            $('#table-report-special-contract').DataTable({
                "ajax": {
                    "url": "/ReportSpecialContract/ShowFilterNew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.business_partner = $('#BUSINESS_PARTNER').val();
                        data.contract_no = $('#CONTRACT_NO').val();
                        data.contract_status = $('#CONTRACT_STATUS').val();
                        data.valid_from = $('#VALID_FROM').val();
                        data.valid_to = $('#VALID_TO').val();
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE",
                    },
                    {
                        "data": "CREATION_BY"
                    },
                    {
                        "data": "CONTRACT_NAME"
                    },
                    {
                        "data": "CONDITION_TYPE"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BUSINESS_PARTNER_NAME"
                    },
                    {
                        "data": "CUSTOMER_AR"
                    },
                    {
                        "data": "PROFIT_CENTER"
                    },
                    {
                        "data": "CONTRACT_USAGE"
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "IJIN_PRINSIP_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "INDUSTRY"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CONTRACT_STATUS",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],
                "pageLength": 10,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },
                "filter": false
            });
        });
    }

    var mdm = function () {
        $('#BUSINESS_PARTNER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#BUSINESS_PARTNER_NAME').val();
                console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportSpecialContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#BUSINESS_PARTNER").val(ui.item.code);
            }
        });
    }

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            select2ProfitCenter();
            filter();
            mdm();
            CBBusinessEntity();
            //CBProfitCenter();

        }
    };
}();

function CBProfitCenter(BE_ID) {
    //console.log(BE_ID);
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportSpecialContract/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                handleBootstrapSelect();

            }
        });
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportSpecialContract/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                handleBootstrapSelect();

            }
        });
    }
}

var fileExcels;
function defineExcelSpecialContract() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var CONTRACT_NO = $('#CONTRACT_NO').val();
    var BUSINESS_PARTNER = $('#BUSINESS_PARTNER').val();
    var CONTRACT_STATUS = $('#CONTRACT_STATUS').val();
    var VALID_FROM = $('#VALID_FROM').val();
    var VALID_TO = $('#VALID_TO').val();

    var param = {
        BUSINESS_ENTITY,
        PROFIT_CENTER,
        CONTRACT_NO,
        BUSINESS_PARTNER,
        CONTRACT_STATUS,
        VALID_FROM,
        VALID_TO
    }

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportSpecialContract/defineExcelReportSpecialContract",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>'+
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    console.log('Yess ' + fileExcels);
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function exportExcel() {
    $.ajax({
        type: "POST",
        url: "/ReportSpecialContract/exportExcel",
        data: "{}",
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        timeout: 100000,
        success: function (response) {
            var valStrings = response.substring(2, response.length - 2);
            //console.log(valStrings);
            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

            var jsonData = "{fileName:'" + valStrings + "'}";
            setTimeout(function () {
                deleteExcel(jsonData);
            }, 2000);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function deleteExcel(jsonData) {
    //console.log(jsonData);
    $.ajax({
        type: "POST",
        url: "/ReportSpecialContract/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            //console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportSpecialContract.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-special-contract > tbody').html('');
    $('#table-report-special-contract').DataTable();
});