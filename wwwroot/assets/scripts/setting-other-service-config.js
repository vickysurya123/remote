﻿var SettingOtherServiceConfig = function () {

    var initTableService = function () {
        $('#table-service').DataTable({

            "ajax":
            {
                "url": "SettingOtherService/GetDataService",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "USER_NAME",
                    "class": "dt-body-center"
                },
                //{
                //    "data": "PENDAPATAN",
                //    "class": "dt-body-center"

                //},
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        aksi += '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }


            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var clear = function () {
        $('input').val('');
    }

    $('#table-service').on('click', 'tr #btn-ubah', function () {
        clear();
        var baris = $(this).parents('tr')[0];
        var table = $('#table-service').DataTable();
        var data = table.row(baris).data();
        var table_row = $('#table-service-detail')
        var a = '';
        var b = '';
        //var start_date = data["START_DATE"].split(" ");
        //var end_date = data["END_DATE"].split(" ");

        $('#txt-judul').text('Edit');
        $('#id_detail').val(data["ID"]);
        //$('#START_DATE').val(start_date[0]);
        //$('#END_DATE').val(end_date[0]);
        //$('#NOTIF_START').val(data["NOTIF_START"]);
        //$('#CONTRACT_TYPE').val(data["CONTRACT_TYPE"]);

        $('#adddetailModal').modal('show');
        $('#table-service-detail').DataTable().destroy();
        $('#table-service-detail tbody').html('');

        $.ajax({
            type: "GET",
            url: "/SettingOtherService/EditDataDetail?id=" + data["ID"],
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                for (var i = 0; i < jsonList.data.length; i++) {
                    a += '<tr>' +
                        '<td></td>' +
                        '<td>' + jsonList.data[i].DESCRIPTION + '</td>' +
                        '<td><center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
                        '</tr>';
                }
                $('#table-service-detail tbody').append(a);
                $('#table-service-detail').DataTable({
                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"]
                    ],
                    "pageLength": 100000,
                    "paging": false,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false,
                });
                resetNumbering();
            }
        });
    });

    $('#table-employee').on('click', 'tr #btn-employee-pick', function () {
        clear();
        var baris = $(this).parents('tr')[0];
        var table = $('#table-employee').DataTable();
        var data = table.row(baris).data();
        //console.log(data[0]);
        $('#EMPLOYEE_NAME').val(data[1]);
        $('#USER_ID').val(data[0]);
        $('#btn-bataladd').click();
    });

    var listItems = "";

    var addServiceGroup = function () {
        //table row
        var tablem = $('#table-service-detail');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        //Generate textbox(form) di data table table-row
        $('#btn-add-detail').on('click', function () {
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var xtable = $('#table-service-detail').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-service-detail").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var serviceGroup = '<select class="form-control SERVICE_GROUP" name="SERVICE_GROUP" id="SERVICE_GROUP">' + listItems + '</select>';
            var tglMulai = '<input id="TANGGAL_MULAI" name="TANGGAL_MULAI" class="form-control form-control-inline input-sm date-picker" size="16" type="text" value="" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
            var tglSelesai = '<input id="TANGGAL_SELESAI" name="TANGGAL_SELESAI" class="form-control form-control-inline input-sm date-picker" size="16" type="text" value="" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';

            oTable.fnAddData([nomor,
                serviceGroup,tglMulai,tglSelesai,
                '<center><a class="btn default btn-xs green btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>'
            ], true);
            $('.date-picker').datepicker();
$('.SERVICE_GROUP').select2();
        });

        $('#table-service-detail').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var data = table.row(baris).data();
            console.log(data);
            var service_Group = data[1].split(" - ")
            if (service_Group[0] != null) {
                a = "<tr><td>" + service_Group[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-service-detail tbody tr').each(function () {
                if (oTable.fnGetData().length > 0) {
                    $(this).find('td').eq(0).html(index);
                    index++;
                }
                else {
                    $(this).find('td').eq(0).html("No data available in table");
                }
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                indexparam++;
            });
        }

        $('#table-service-detail').on('click', 'tr .btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-service-detail').DataTable();
            var data = table.row(baris).data();

            var a = $('#SERVICE_GROUP').val();
            var tombol = '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var service_Group = a.split("|")
            console.log(service_Group);
            oTable.fnUpdate(service_Group[3], baris, 1, false);
            oTable.fnUpdate(tombol, baris, 2, false);
            //oTable.fnUpdate(service_Group[3], baris, 3, false);


            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);

            //$(this).remove();
            return false;

        });
    }

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-service').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID)
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Approval Setting on branch \"" + aData.USER_ID + "\"?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingOtherService/DeleteHeader",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-service').DataTable();
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                        table.ajax.reload();
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/SettingOtherService/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                //var listItems = "";
                listItems += "<option value=''>-- Choose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + '|' + jsonList.data[i].PENDAPATAN + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
            }
        });

    }

    var comboUser = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/SettingOtherService/GetDataDropDownUser",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                var tablemp = $('#table-employee').dataTable({
                    "lengthMenu": [
                        [5, 15, 30, -1],
                        [5, 15, 30, "All"]
                    ],
                    //"pageLength": 100000,
                    "paging": true,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": true,
                    "lengthChange": true,
                    "bSort": false,
                    "aoColumnDefs": [
                        { "bVisible": false, "aTargets": [0] }
                    ]
                });
                for (var i = 0; i < jsonList.data.length; i++) {
                    var value = jsonList.data[i].ID + '|' + jsonList.data[i].USER_NAME + " - " + jsonList.data[i].USER_LOGIN;
                    var text = jsonList.data[i].USER_NAME + " - " + jsonList.data[i].USER_LOGIN;
                    tablemp.fnAddData([value, text,
                        '<center><a class="btn default btn-xs green" id="btn-employee-pick"><i class="fa fa-check"></i></a></center>'
                    ], true);
                }

            }
        });

    }

    var simpanData = function () {

        $('#btn-simpan-conf').click(function () {
            var headerId = $('#HEADER_ID').val();

            if (headerId !== "") {
                SimpanDetil(headerId);
            }
            else {
                var employeeData = $('#USER_ID').val();
                
                if (employeeData !== "") {

                    var DTDetil = $('#table-service-detail').dataTable();
                    var countDTDetil = DTDetil.fnGetData().length;
                    
                    if (countDTDetil <= 0) {
                        swal('Warning', "Harap Memilih Service Group Terlebih Dahulu.", 'warning');
                        return false;
                    }

                    var arrayUser = employeeData.split("|");
                    var id_user = arrayUser[0];
                    var user_name = arrayUser[1];

                    if (id_user) {
                        var param = {
                            USER_NAME: user_name,
                            USER_ID: parseInt(id_user),
                        };
                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/SettingOtherService/AddHeader",
                            timeout: 30000
                        });
                        req.done(function (data) {
                            App.unblockUI();
                            if (data.Status === 'S') {
                                $('#HEADER_ID').val(data.Msg);
                                SimpanDetil(data.Msg);
                            } else {
                                swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                                });
                            }
                        });
                    }
                    else {
                        swal('Warning', "Harap Memilih User Terlebih Dahulu.", 'warning');
                    }
                }
                else {
                    swal('Warning', "Harap Memilih User Terlebih Dahulu.", 'warning');
                }
            }
        });

        function SimpanDetil(HEADER_ID) {
            var allowSave = true;

            var DTMemo = $('#table-service-detail').dataTable();
            var countDTMemo = DTMemo.fnGetData();
            arrRole = new Array();
            for (var i = 0; i < countDTMemo.length; i++) {
                var itemDTMemo = DTMemo.fnGetData(i);
                var a = itemDTMemo[1].split(" - ");
                var paramDTMemo = {
                    COA: a[0],
                    DESCRIPTION: itemDTMemo[1],
                }
                arrRole.push(paramDTMemo);
            }
            if (countDTMemo.length > 0) {
                $('#table-service-detail tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-frequency').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }

            delRole = new Array();
            $('#delete_role tbody tr').each(function () {
                var deleteRole = {
                    COA: $(this).find("td:first").text(),
                }
                delRole.push(deleteRole);
            });

            if (allowSave) {
                var param = {
                    HEADER_ID: HEADER_ID,
                    Service: arrRole,
                    DeleteD: delRole,
                }
                console.log(param);
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SettingOtherService/InsertDetail",
                    timeout: 30000
                });
                req.done(function (data) {
                    $('#adddetailModal').modal('hide');
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            window.location.href = '/SettingOtherService';
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                        });
                    }
                });
            } else {
                $('#adddetailModal').modal('hide');
                swal('Failed', 'Harap di cek kembali inputanya, dan pada table harap di centang semua', 'error').then(function (isConfirm) {
                    $('#adddetailModal').modal('show');
                });
            }

        }
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/SettingOtherService";
        });
    }

    var checkSearch = function () {
        $('#search-employee').click(function () {
            var headerId = $('#HEADER_ID').val();
            //$('#employeeModal').modal('show');
            if (headerId == "") {
                $('#employeeModal').modal('show');
            }
            else {
                swal('Warning', "User Tidak boleh diganti");
            }
        });
    }

    return {
        init: function () {
            checkSearch();
            initTableService();
            comboServiceGroup();
            comboUser();
            batal();
            addServiceGroup();
            simpanData();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        SettingOtherServiceConfig.init();
        var headerId = $('#HEADER_ID').val();

        if (headerId !== "") {
            //$('#search-employee').addClass("hidden");
            var a = '';
            $('#table-service-detail').DataTable().destroy();
            $('#table-service-detail tbody').html('');

            $.ajax({
                type: "GET",
                url: "/SettingOtherService/EditDataDetail?id=" + headerId,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        a += '<tr>' +
                            '<td></td>' +
                            '<td>' + jsonList.data[i].DESCRIPTION + '</td>' +
                            '<td>' + jsonList.data[i].WAKTU_MULAI + '</td>' +
                            '<td>' + jsonList.data[i].WAKTU_SELESAI + '</td>' +
                            '<td><center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
                            '</tr>';
                    }
                    $('#table-service-detail tbody').append(a);
                    $('#table-service-detail').DataTable({
                        "lengthMenu": [
                            [5, 15, 20, -1],
                            [5, 15, 20, "All"]
                        ],
                        "pageLength": 100000,
                        "paging": false,
                        "language": {
                            "lengthMenu": " _MENU_ records"
                        },
                        "searching": false,
                        "lengthChange": false,
                        "bSort": false,
                    });
                    resetNumbering();
                }
            });
        }
    });
}