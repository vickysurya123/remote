﻿var SertifikatHpl = function () {
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                endDate: "Date.now()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }
    var CBBusinessEntity = function () {
        $("#BE_NAME").select2({
            allowClear: true,
            width: '100%',
            dropdownParent: $("#addapvsetModal"),
            ajax: {
                url: "/SertifikatHpl/GetDataBusinessEntity",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                     var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.BE_ID,
                                text: item.BE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: "Business Entity"
        })
    };

    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/SertifikatHpl/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "BE_NAME"
                },
                {
                    "data": "NO_SERTIFIKAT",
                    "class": "dt-body-left",
                },
                {
                    "data": "SERTIFIKAT_TYPE",
                    "class": "dt-body-left",
                },
                {
                    "data": "TANGGAL",
                    "class": "dt-body-center",
                },
                {
                    "data": "LUAS_LAHAN",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0, '', ' m³')
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-view" class="btn btn-icon-only green" title="View" ><i class="fa fa-eye"></i></a>' +
                            '<a id="btn-download" class="btn btn-icon-only purple" title="Download" ><i class="fa fa-cloud-download"></i></a>' +
                            '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

        $('#FILTER_TYPE').on('change', function () {

            var beid = $('#FILTER_TYPE').val();
            console.log(beid);
            if (beid) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-apvset')) {
                    $('#table-apvset').DataTable().destroy();
                }

                $('#table-apvset').DataTable({
                    "ajax":
                    {
                        "url": "/SertifikatHpl/GetTypeSertifikat?SERTIFIKAT_TYPE=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ID",
                            "class": "dt-body-center",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            "data": "BE_NAME"
                        },
                        {
                            "data": "NO_SERTIFIKAT",
                            "class": "dt-body-left",
                        },
                        {
                            "data": "SERTIFIKAT_TYPE",
                            "class": "dt-body-left",
                        },
                        {
                            "data": "TANGGAL",
                            "class": "dt-body-center",
                        },
                        {
                            "data": "LUAS_LAHAN",
                            "class": "dt-body-right",
                            render: $.fn.dataTable.render.number('.', '.', 0, '', ' m³')
                        },
                        {
                            "render": function (data, type, full) {
                                var aksi = '<a id="btn-view" class="btn btn-icon-only green" title="View" ><i class="fa fa-eye"></i></a>' +
                                    '<a id="btn-download" class="btn btn-icon-only purple" title="Download" ><i class="fa fa-cloud-download"></i></a>' +
                                    '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                                    '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
                //$('#table-njop').DataTable().column(0).visible(false);
                //$('#table-njop').DataTable().column(1).visible(false);
            }
        });

        $('#FILTER_BE_ID').on('change', function () {

            var beid = $('#FILTER_BE_ID').val();
            console.log(beid);
            if (beid) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-apvset')) {
                    $('#table-apvset').DataTable().destroy();
                }

                $('#table-apvset').DataTable({
                    "ajax":
                    {
                        "url": "/SertifikatHpl/GetSertifikatBeid?BUSINESS_ENTITY_ID=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ID",
                            "class": "dt-body-center",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            "data": "BE_NAME"
                        },
                        {
                            "data": "NO_SERTIFIKAT",
                            "class": "dt-body-left",
                        },
                        {
                            "data": "SERTIFIKAT_TYPE",
                            "class": "dt-body-left",
                        },
                        {
                            "data": "TANGGAL",
                            "class": "dt-body-center",
                        },
                        {
                            "data": "LUAS_LAHAN",
                            "class": "dt-body-right",
                            render: $.fn.dataTable.render.number('.', '.', 0, '', ' m³')
                        },
                        {
                            "render": function (data, type, full) {
                                var aksi = '<a id="btn-view" class="btn btn-icon-only green" title="View" ><i class="fa fa-eye"></i></a>' +
                                    '<a id="btn-download" class="btn btn-icon-only purple" title="Download" ><i class="fa fa-cloud-download"></i></a>' +
                                    '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                                    '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
                //$('#table-njop').DataTable().column(0).visible(false);
                //$('#table-njop').DataTable().column(1).visible(false);
            }
        });


    }
    var simpan = function () {
        $('#btn-simpanadd').click(function (evt) {
            evt.preventDefault();
            var table = $('#table-apvset').DataTable();
            var ID = parseInt($('#param_id').val());
            var BUSINESS_ENTITY_ID = $('#BE_NAME').val();
            console.log(BUSINESS_ENTITY_ID);
            var TANGGAL = $('#TANGGAL').val();
            var LUAS_LAHAN = parseInt($('#LUAS_LAHAN').val());
            var NO_SERTIFIKAT = $('#NO_SERTIFIKAT').val();
            var SERTIFIKAT_TYPE = $('#SERTIFIKAT_TYPE').val();
            if (ID !== "") {

            } else {
                var ATTACHMENT = $('#files').val();
            }
            if (BUSINESS_ENTITY_ID !== "" && TANGGAL !== "" && ATTACHMENT !== "" && LUAS_LAHAN !== "" && NO_SERTIFIKAT !== "" && SERTIFIKAT_TYPE !== "") {
                
                App.blockUI({ boxed: true });

                var form = $("#sertifikat-hpl")[0];

                var data = new FormData(form);

                var req = $.ajax({
                    type: 'POST',
                    url: '/SertifikatHpl/insertData',
                    enctype:'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: data,

                    success: function (message) {
                        //alert(message);
                    },
                    error: function () {
                        //alert("there was error uploading files!");
                    }
                });

                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        $('#addapvsetModal').modal('hide');
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        $('#addapvsetModal').modal('hide');
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                $('#addapvsetModal').modal('hide');
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning').then(function (isConfirm) {
                    $('#addapvsetModal').modal('show');
                });
            }
        });
    }

    var clear = function () {
        var form = document.getElementById("sertifikat-hpl");
        var allElements = form.elements;
        for (var i = 0, l = allElements.length; i < l; ++i) {
            allElements[i].disabled = false;
        }
        document.getElementById('btn-simpanadd').style.visibility = '';
        document.getElementById('btn-bataladd').style.visibility = '';
        $('input').val('');
        $('#MEMO').val('');
        $('#upload_file_berita_exist').val('');
        $('a#target_berita').removeClass('d-none');
        $('a#target_berita').attr('');
        $('a#target_berita').text('');
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        $('#txt-judul').text('Edit');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        console.log(aData);
        $('#TANGGAL').val(aData.TANGGAL);
        $('#LUAS_LAHAN').val(aData.LUAS_LAHAN);
        $('#NO_SERTIFIKAT').val(aData.NO_SERTIFIKAT);
        $('#SERTIFIKAT_TYPE').val(aData.SERTIFIKAT_TYPE);
        $('#MEMO').val(aData.MEMO);
        $('#param_id').val(aData.ID);
        $('#ID').val(aData.ID);
        $('#BUSINESS_ENTITY_ID').val(aData.BUSINESS_ENTITY_ID);
        $('#PATCH').val(aData.PATCH);
        $('#ATTACHMENT').val(aData.ATTACHMENT);
        var newOption = new Option(aData.BE_NAME, aData.BUSINESS_ENTITY_ID, true, true);
        $('#BE_NAME').append(newOption).trigger('change');
        $('#upload_file_berita_exist').val(aData.ATTACHMENT);
        $('a#target_berita').removeClass('d-none');
        $('a#target_berita').attr('href', aData.PATCH.replace("~/","") + "/" + aData.ATTACHMENT );
        $('a#target_berita').text(aData.ATTACHMENT);

        var form = document.getElementById("sertifikat-hpl");
        var allElements = form.elements;
        for (var i = 0, l = allElements.length; i < l; ++i) {
            allElements[i].disabled = false;
        }
        document.getElementById('btn-simpanadd').style.visibility = '';
        document.getElementById('btn-bataladd').style.visibility = '';
        //$('#upload_file_exist').show();
        $('#addapvsetModal').modal('show');
    
    });

    $('body').on('click', 'tr #btn-view', function () {
        $('#txt-judul').text('Edit');
        $('#viewModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        console.log(aData);
        $('#TANGGALL').val(aData.TANGGAL);
        $('#LUAS_LAHANN').val(aData.LUAS_LAHAN);
        $('#NO_SERTIFIKATT').val(aData.NO_SERTIFIKAT);
        //var newOptionn = new Option(aData.SERTIFIKAT_TYPE, aData.SERTIFIKAT_TYPE, true, true);
        $('#SERTIFIKAT_TYPEE').val(aData.SERTIFIKAT_TYPE).trigger('change');
        //$('#SERTIFIKAT_TYPE').val(aData.MEMO);
        $('#MEMOO').val(aData.MEMO);
        $('#param_id').val(aData.ID);
        $('#ID').val(aData.ID);
        $('#BUSINESS_ENTITY_ID').val(aData.BUSINESS_ENTITY_ID);
        $('#PATCH').val(aData.PATCH);
        $('#ATTACHMENT').val(aData.ATTACHMENT);
        var newOption = new Option(aData.BE_NAME, aData.BE_NAME, true, true);
        $('#BE_NAMEE').append(newOption).trigger('change');
        $('#upload_file_berita_exist').val(aData.ATTACHMENT);
        $('a#target_berita').removeClass('d-none');
        $('a#target_berita').attr('href', aData.PATCH.replace("~/", ""));
        $('a#target_berita').text(aData.ATTACHMENT);
        var form = document.getElementById("sertifikat-hpl");
        var allElements = form.elements;
        for (var i = 0, l = allElements.length; i < l; ++i) {
            allElements[i].disabled = true;
        }
        document.getElementById('btn-simpanadd').style.visibility = 'hidden';
        document.getElementById('btn-bataladd').style.visibility = 'hidden';
        //$('#upload_file_exist').show();

    });

    $('body').on('click', 'tr #btn-download', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        var file_path = aData.PATCH.replace("~/", "");
        var a = document.createElement('A');
        a.href = file_path;
        a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    });

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID),
            ATTACHMENT: aData.ATTACHMENT,
            PATCH: aData.PATCH,
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Sertifikat  " + aData.ID + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/SertifikatHpl/Delete",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('#btn-bataladd').click(function () {
        clear();
        $('#BE_NAME').select2("val", "");
    });
    $('#btn-add').click(function () {
        clear();
        $('#BE_NAME').select2("val", "");
        $('#txt-judul').text('Add');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            CBBusinessEntity();
            datePicker();
            //simpanEdit();

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        SertifikatHpl.init();
    });
}