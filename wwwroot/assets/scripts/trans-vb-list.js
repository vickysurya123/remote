﻿var TransVariousBusiness = function () {

    var initTableVBTransaction = function () {
        $('#table-transvb-list').DataTable({
             
            "ajax":
            {
                "url": "TransVBList/GetDataVBTransListNew",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                    data.select_cabang = $('#BE_NAME').val();
                }

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "POSTING_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROF_CENTER"
                },
                {
                    "data": "COSTUMER_MDM"
                },
                {
                    "data": "KELOMPOK_PENDAPATAN"
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return '<span> USD </span>';
                        } else {
                            return '<span> IDR </span>';
                        }
                    },
                    "class": "dt-body-center",
                },
                {
                    "data": "TOTAL",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return full.TOTAL_USD;
                        } else {
                            return " - ";
                        }
                    },
                    "class": "dt-body-right",
                    //render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.SAP_DOCUMENT_NUMBER != null) {
                            return '<span class="label label-sm label-success"> BILLED (' + full.SAP_DOCUMENT_NUMBER + ')</span>';
                        } else {
                            return '<span class="label label-sm label-danger"> NOT BILLED YET </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.CANCEL_STATUS != null) {
                            return '<span class="badge badge-danger"> CANCELED </span>';
                        }
                        else {
                            return '';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#viewVBPTransModal" class="btn btn-icon-only green" id="btn-detail-vbp-trans"><i class="fa fa-eye"></i></a>';
                        if (full.SAP_DOCUMENT_NUMBER != null) {
                            aksi += '<a class="btn btn-icon-only blue" id="btn-ubahx" disabled><i class="fa fa-edit"></i></a>';
                        }
                        else {
                            aksi += '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota"><i class="fa fa-print"></i></a>';

                        }
                        //aksi += '<a data-toggle="modal" href="#viewVBPTransModal" class="btn btn-icon-only green" id="btn-detail-vbp-trans"><i class="fa fa-eye"></i></a>';
                        aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota"><i class="fa fa-print"></i></a>';

                        return aksi;
                     },
                     "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb-list').DataTable();
            var data = table.row(baris).data();

            var idTransaksi = data["ID"];
            console.log('id transaksi ' + idTransaksi);

            window.location = "/TransVBList/Edit/" + data["ID"];
        });
    }
    $('#btn-filter').click(function () {

        console.log('hgfhgf');
        initTableVBTransaction();
    })
    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail-vbp-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb-list').DataTable();
            var data = table.row(baris).data();
            console.log(data);

            $("#DETAIL_ID").val(data["ID"]);
            $("#DETAIL_PROFIT_CENTER").val(data["PROF_CENTER"]);
            $("#DETAIL_POSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAIL_SERVICE_GROUP").val(data["KELOMPOK_PENDAPATAN"]);
            $("#DETAIL_CUSTOMER_MDM").val(data["COSTUMER_MDM"]);
            $("#CUSTOMER_ID").val(data["COSTUMER_ID"]);
            $("#DETAIL_INSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);
            $("#DETAIL_CUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            var tot = data["TOTAL"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#DETAIL_TOTAL").val(tot);
            $("#DETAIL_SAP_DOCUMENT_NUMBER").val(data["SAP_DOCUMENT_NUMBER"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);

            var id_vb = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransVB/GetDataTransaction/" + id_vb,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "VB_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SERVICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CM_PRICE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)

                    },
                    {
                        "render": function (data, type, full) {
                            if (full.RATE > 1) {
                                return full.TOTAL_USD;
                            } else {
                                return " - ";
                            }
                        },
                        "class": "dt-body-right",
                        //render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "REMARK",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"

                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,


                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                //"language": {
                //    "url": ""
                //},

                "filter": false
            });

        });
    }

    var initDetilTransactionListrik = function () {
        $('body').on('click', 'tr #btn-detail-listrik', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTransPricing = $('#table-detil-transaction-listrik').DataTable();
            detailTransPricing.destroy();

            var detailTransCosting = $('#table-detil-transaction-listrik-costing').DataTable();
            detailTransCosting.destroy();

            var grandTotal = data["AMOUNT"];
            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $('#GRAND_TOT').val(fGrandTotal);

            $('#table-detil-transaction-listrik').DataTable({

                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransaction",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "KD_TRANSAKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KODE_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "NAMA_CUSTOMER_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "DESKRIPSI_PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIl
            $('#table-detil-transaction-listrik-costing').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetail",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "USED",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TARIFF",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "filter": false
            });
        });
    }


    //Function untuk form Add (Form Sesuai Mockup)
    var simpanupdate = function () {
        $('#btn-simpan-update').click(function () {
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var ID = $('#ID_INSTALASI').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#AMOUNT').val();
            var SURCHARGE = $('#SURCHARGE').val();

            if (Number(METER_TO) < Number(METER_FROM)) {
                swal('Failed', 'Check Your Meter To Value', 'error')
            }
            else {
                if (METER_FROM && METER_TO) {
                    var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                    var nfSurcharge = parseFloat(SURCHARGE.split('.').join(''));
                    var netValue = Number(nfAmount) + Number(nfSurcharge);
                    var param = {
                        METER_FROM: METER_FROM,
                        METER_TO: METER_TO,
                        ID: ID,
                        QUANTITY: QUANTITY,
                        AMOUNT: netValue,
                        SURCHARGE: nfSurcharge,
                        SUB_TOTAL: nfAmount
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransWEList/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }

        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWEList";
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransVBList/AddTransVB';
        });
    }

    $('body').on('click', 'tr #btn-pranota', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-transvb-list').DataTable();
        var data = table.row(baris).data();
        console.log(data);
        var bill_type = data["BILLING_TYPE"];
        var bill_no = data["ID"];
        var kodeCabang = data["BRANCH_ID"];

        window.open("/Pdf/cetakPdf2?tp=" + btoa("1M") + "&kc=" + btoa(kodeCabang) + "&bn=" + btoa(bill_no) + "&us=" + btoa(bill_type), '_blank');
    });

    return {
        init: function () {
            initTableVBTransaction();
            initDetilTransaction();
            initDetilTransactionListrik();
            batal();
            ubah();
            simpanupdate();
            add();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransVariousBusiness.init();
    });
}