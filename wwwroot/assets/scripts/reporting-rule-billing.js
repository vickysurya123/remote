﻿var ReportingRuleBilling = function () {

    var initTable = function () {
        $('#table-reporting-rule').DataTable({

            "ajax":
            {
                "url": "ReportingRuleBilling/GetDataReportingRule",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NO"
                },
                {
                    "data": "CONTRACT_NO"
                },
                {
                    "data": "CONTRACT_NO"
                },
                {
                    "data": "CONTRACT_NO"
                }
                 //{
                 //    "render": function (data, type, full) {
                 //        var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                 //        aksi += '<a data-toggle="modal" href="#viewBEModal" class="btn btn-icon-only green" id="btn-detil"><i class="fa fa-eye"></i></a>';
                 //        return aksi;
                 //    },
                 //    "class": "dt-body-center"
                 //}

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    return {
        init: function () {
            //Return var here
            initTable();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportingRuleBilling.init();
    });
}