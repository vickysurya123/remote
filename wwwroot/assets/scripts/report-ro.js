﻿var handleBootstrapSelect = function () {
    $('.bs-select').selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
}

var ReportRO = function () {

    var dataTableResult = function () {
        var table = $('#table-report-ro');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var showFilter = function () {
        $('#btn-filter').click(function () {
            var AVAILABLE_DATE = $('#AVAILABLE_DATE').val();
            if (AVAILABLE_DATE) {
                $('#row-table').css('display', 'block');
                if ($.fn.DataTable.isDataTable('#table-report-ro')) {
                    $('#table-report-ro').DataTable().destroy();
                }
                $('#table-report-ro').DataTable({
                    "ajax": {
                        "url": "/ReportRO/GetDataFilter",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.be_id = $('#BE_NAME').val();
                            data.profit_center = $('#PROFIT_CENTER').val();
                            data.ro_number = $('#RO_NUMBER').val();
                            data.zone_rip = $('#ZONE_RIP').val();
                            data.usage_type = $('#USAGE_TYPE').val();
                            data.function = $('#FUNCTION').val();
                            data.vacancy_status = $('#VACANCY_STATUS').val();
                            data.vacancy_reason = $('#VACANCY_REASON').val();
                            data.status = $('#STATUS').val();
                            data.available_date = $('#AVAILABLE_DATE').val();
                            //console.log(data);
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak-exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "BE_NAME"
                        },
                        {
                            "data": "PROFIT_CENTER"
                        },
                        {
                            "data": "RO_CODE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_NAME"
                        },
                        {
                            "data": "ZONE_RIP"
                        },
                        {
                            "data": "LAND_DIMENSION",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "BUILDING_DIMENSION",
                            "class": "dt-body-center"
                        },
                        {

                            //"data": "CONTRACT_NO",
                            "render": function (data, type, full) {
                                if (full.CONTRACT_NO === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span>' + full.CONTRACT_NO + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        //{
                        //    //"data": "CONTRACT_OFFER_NO",
                        //    "render": function (data, type, full) {
                        //        if (full.CONTRACT_OFFER_NO === null) {
                        //            return '<span> - </span>';
                        //        } else {
                        //            return '<span>' + full.CONTRACT_OFFER_NO + ' </span>';
                        //        }
                        //    },
                        //    "class": "dt-body-center"

                        //    "data": "CONTRACT_NO",
                        //    "render": function (data, type, full) {
                        //        return full.CONTRACT_NO == null ? " - " : full.CONTRACT_NO;
                        //    },
                        //},
                        {
                            "data": "CONTRACT_OFFER_NO",
                            "render": function (data, type, full) {
                                return full.CONTRACT_OFFER_NO == null ? " - " : full.CONTRACT_OFFER_NO;
                        },

                        },
                        {
                            "data": "MPLG_NAMA",
                            "render": function (data, type, full) {
                                return full.MPLG_KODE + " - " + full.MPLG_NAMA;
                            },
                            "class": "dt-body-center"
                        },
                        {

                            //"data": "MPLG_BADAN_USAHA"
                            "render": function (data, type, full) {
                                if (full.MPLG_BADAN_USAHA === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span>' + full.MPLG_BADAN_USAHA + ' </span>';
                                }
                            },
                            "class": "dt-body-center"

                            //"data": "MPLG_BADAN_USAHA",
                            //"render": function (data, type, full) {
                            //    return full.MPLG_BADAN_USAHA + " - " + full.MPLG_BADAN_USAHA;
                            //},

                        },
                        {
                            "data": "RO_CERTIFICATE_NUMBER"
                        },
                        {
                            "data": "USAGE_TYPE"
                        },
                        {
                            "data": "FUNCTION_NAME"
                        },
                        {
                            "data": "STATUS",
                            "class": "dt-body-center"
                        },
                        {
                            //"data": "REASON",
                            //"class": "dt-body-center"
                            "render": function (data, type, full) {
                                if (full.REASON === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span>' + full.REASON + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALID_FROM",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALID_TO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_ADDRESS"
                        },
                        {
                            "data": "RO_POSTALCODE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_CITY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "PROVINCE",
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.ACTIVE === '1') {
                                    return 'ACTIVE';
                                } else {
                                    return 'INACTIVE';
                                }
                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],
                    "pageLength": 10,
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },
                    "filter": false
                });
            }
            else {
                swal('Warning', 'Please Fill Available Date!', 'warning');
            }

        });
    }

    //var CBBE = function () {
    //    // Ajax untuk ambil tarif dengan code L
    //    $.ajax({
    //        type: "GET",
    //        url: "/ReportRO/GetDataRO",
    //        contentType: "application/json",
    //        dataType: "json",
    //        success: function (data) {
    //            var jsonList = data
    //            var listItems = "";
    //            listItems += "<option value=''>-- Choose Business Entity --</option>";
    //            for (var i = 0; i < jsonList.data.length; i++) {
    //                listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
    //            }
    //            $("#BUSINESS_ENTITY").html('');
    //            $("#BUSINESS_ENTITY").append(listItems);
    //            SelectCBBE();
    //            handleBootstrapSelect();
    //        }
    //    });
    //}

    //var SelectCBBE = function () {
    //    $('#BUSINESS_ENTITY').change(function () {
    //        var valBE = $(this).val();
    //        CBProfitCenter(valBE);
    //    });
    //}

    var CBZoneRip = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportRO/GetDataZoneRip",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose Zone RIP --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#ZONE_RIP").html('');
                $("#ZONE_RIP").append(listItems);
                //CBBE();
            }
        });
    }

    var CBUsageType = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportRO/GetDataUsageType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Usage Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#USAGE_TYPE").html('');
                $("#USAGE_TYPE").append(listItems);
                CBZoneRip();
            }
        });
    }

    var CBFunction = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportRO/GetDataFunction",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Function --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#FUNCTION").html('');
                $("#FUNCTION").append(listItems);
                CBUsageType();
            }
        });

    }

    var CBVacancyReason = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportRO/GetDataVacancyReason",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Vacancy Reason --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DESC + "'>" + jsonList.data[i].REF_DESCRIPTION + "</option>";
                }
                $("#VACANCY_REASON").html('');
                $("#VACANCY_REASON").append(listItems);
                CBFunction();
            }
        });

    }

    var changeBe = function () {
        $('#BE_NAME').change(function () {
            var be_id = $('#BE_NAME').val();

            $('#PROFIT_CENTER').html('');
            $('#PROFIT_CENTER').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(be_id),
                    method: "POST",
                    url: "/ReportRO/GetProfitCenternew",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#PROFIT_CENTER').append(temp);
                });
            }

        });
    }

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            showFilter();
            //CBBE();
            //SelectCBBE();
            //CBZoneRip();
            //CBUsageType();
            //CBFunction();
            CBVacancyReason();
            //CBProfitCenter();
            changeBe();
        }
    };
}();

//function CBProfitCenter(BE_ID) {   

//    if (BE_ID !== null) {
//        // Ajax untuk ambil tarif dengan code L
//        console.log(BE_ID);
//        $.ajax({
//            type: "GET",
//            url: "/ReportRO/GetDataProfitCenter",
//            //url: "/ReportRO/GetProfitCenternew",
//            contentType: "application/json",
//            dataType: "json",
//            success: function (data) {
//                var jsonList = data
//                var listItems = "";
//                listItems += "<option value=''>-- Choose Profit Center --</option>";
//                for (var i = 0; i < jsonList.data.length; i++) {
//                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
//                }
//                $("#PROFIT_CENTER").html('');
//                $("#PROFIT_CENTER").append(listItems);
//                console.log(listItems);
//            }
//        });
//    } else {
//        // Ajax untuk ambil tarif dengan code L
//        //$.ajax({
//        //    type: "GET",
//        //    url: "/ReportRO/GetDataProfitCenterD",
//        //    data: {
//        //        BE_ID: BE_ID
//        //    },
//        //    contentType: "application/json",
//        //    dataType: "json",
//        //    success: function (data) {
//        //        var jsonList = data
//        //        var listItems = "";
//        //        listItems += "<option value=''>-- Choose Profit Center --</option>";
//        //        for (var i = 0; i < jsonList.data.length; i++) {
//        //            listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
//        //        }
//        //        $("#PROFIT_CENTER").html('');
//        //        $("#PROFIT_CENTER").append(listItems);
//        //    }
//        //});
//    }
//}

var fileExcels;

function defineExcel() {

    var param = {
        BE_ID: $('#BE_NAME').val(),
        PROFIT_CENTER: $('#PROFIT_CENTER').val() == undefined ? null : $('#PROFIT_CENTER').val(),
        RO_NUMBER: $('#RO_NUMBER').val(),
        ZONE_RIP: $('#ZONE_RIP').val(),
        USAGE_TYPE: $('#USAGE_TYPE').val(),
        FUNCTION: $('#FUNCTION').val(),
        VACANCY_STATUS: $('#VACANCY_STATUS').val(),
        VACANCY_REASON: $('#VACANCY_REASON').val(),
        STATUS: $('#STATUS').val(),
        AVAILABLE_DATE: $('#AVAILABLE_DATE').val(),
    };

    console.log($('#PROFIT_CENTER').val());
    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportRO/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportRO/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportRO.init();
    });
}

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportRO/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}