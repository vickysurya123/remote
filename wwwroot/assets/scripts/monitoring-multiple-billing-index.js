﻿var MultipleBillingIndex = function () {
    var initTable = function () {
        $('#table-list-multiple-billing').DataTable({
            "ajax":
            {
                "url": "/MonitoringMultipleBilling/GetDataNew/",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "CUSTOMER_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "RO_ADDRESS",
                    "class": "dt-body-left"
                },
                {
                    "data": "CONTRACT_START_DATE",                    
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL_AMOUNT",
                    "class": "dt-body-right",
                    "render": function (data) {
                        var x = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        return x;
                    }
                },
                {
                    "data": "POSTED_AMOUNT",
                    "class": "dt-body-right",
                    "render": function (data) {
                        var x = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        return x;
                    }
                },
                {
                    "data": "UNPOSTED_AMOUNT",
                    "class": "dt-body-right",
                    "render": function (data) {
                        var x = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        return x;
                    }
                },
                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL_AMOUNT_CO",
                    "class": "dt-body-right",
                    "render": function (data) {
                        var x = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        return x;
                    }
                },
                {
                    "render": function () {
                        return '<a id="btn-detail" class="btn btn-icon-only green" data-toggle="modal" href="#detailModal" title="Detail" ><i class="fa fa-eye"></i></a>';
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,
            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, "All"]
            ],
            "pageLength": 10,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },
            "filter": true
        });
    }
    var detail = function () {

        $('#table-list-multiple-billing').on('click', 'tr #btn-detail', function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-multiple-billing').DataTable();
            var data = table.row(baris).data();
            var contract = data['CONTRACT_NO'];

            if ($.fn.DataTable.isDataTable('#detail-multiple-billing')) {
                $('#detail-multiple-billing').DataTable().destroy();
            }

            $('#detail-multiple-billing').DataTable({
                "ajax":
                {
                    "url": "/MonitoringMultipleBilling/GetDataDetail/" + contract,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POSTING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAID_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-right",
                        "render": function (data) {
                            var x = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            return x;
                        }
                    },
                    {
                        "data": "BILLING_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SAP_DOC_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS",
                        "class": "dt-body-center",
                        "render": function (data) {
                            var x = data;
                            if (data == 'PAID') {
                                x = '<span class="label label-sm label-success"> LUNAS </span>'
                            } else if (data == 'NOT PAID') {
                                x = '<span class="label label-sm label-warning"> BELUM LUNAS </span>'
                            } else if (data == 'CANCEL') {
                                x = '<span class="label label-sm label-danger"> BATAL </span>'
                            } else {
                                x = '<span class="label label-sm label-info"> '+data+' </span>'
                            }
                            return x;
                        }
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, "All"]
                ],
                "pageLength": 10,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },
                "filter": false
            });
        });
    }
    return {
        init: function () {
            initTable();
            detail();
        }
    }
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MultipleBillingIndex.init();
    });
}