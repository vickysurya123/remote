var TableDatatablesEditable = function () {

    //$('#surcharge').mask('000.000.000.000.000', { reverse: true });

    //FUNCTION SUM
    jQuery.fn.dataTable.Api.register('sum()', function () {
        return this.flatten().reduce(function (a, b) {
            if (typeof a === 'string') {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if (typeof b === 'string') {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }

            return a + b;
        }, 0);
    });

    var janganEnter = function () {
        $('html').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    }

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            //var jqTds = $('>td', nRow);
            for (var i = 0, iLen = 23; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }

        
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input class="form-control" id="mask_date1" type="text" />';
            jqTds[1].innerHTML = '<input class="form-control" id="mask_date2" type="text" />';
            //jqTds[2].innerHTML = ' <select class="js-example-basic-single"><option value="AL">Alabama</option><option value="WY">Wyoming</option></select>';
            jqTds[2].innerHTML = '<input type="text" id="service-code" name="service-code" class="form-control nama-service-code2" >';

            //jqTds[2].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control nama-service-code2"></select>';
            jqTds[3].innerHTML = '<input id="txt-service-name" type="text" class="form-control service_name" disabled >';
            jqTds[4].innerHTML = '<input type="text" id="currency" class="form-control currency" disabled>';
            jqTds[5].innerHTML = '<input id="price" type="text" class="form-control price" disabled>';
            jqTds[6].innerHTML = '<input id="multiply_factor"  type="text" class="form-control multiply_factor" disabled>';
            jqTds[7].innerHTML = '<input id="qty" type="text" class="form-control">';
            jqTds[8].innerHTML = '<input type="text" id="unit" class="form-control unit" readonly>';
            jqTds[9].innerHTML = '<input id="surcharge" type="text" class="form-control">';
            jqTds[10].innerHTML = '<input id="amount" type="text" class="form-control" readonly>';
            jqTds[11].innerHTML = '<input id="tax_code" type="text" class="form-control" disabled>';
            jqTds[12].innerHTML = '<input id="keterangan" type="text" class="form-control">';
            //jqTds[13].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';
            jqTds[13].innerHTML = '<center><a class="btn default btn-xs green edit" id="save" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red cancel" id="cancel"><i class="fa fa-trash"></i></a></center>';


            //--- AUTOCOMPLETE SERVICE CODE
            $('.nama-service-code2').autocomplete({
                source: function (request, response) {
                    var inp = $('.nama-service-code2').val();
                    var l = inp.length;
                    //console.log(l);

                    var sGroup = $('#SERVICES_GROUP').val();
                    console.log('service group '+sGroup);

                    $.ajax({ 
                        type: "POST",
                        url: "/TransVB/GetDataServiceCode",
                        data: "{ SERVICE_NAME:'" + inp + "', SERVICE_GROUP:'" + sGroup + "'}",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data, function (el) {
                                //if (sGroup == 16124 || sGroup == 16115 || sGroup == 16125) {
                                if (el.gl_account == '4080101000' || el.gl_account == '4080102000' || el.gl_account == '4090504000') {
                                    //console.log(el.gl_account);
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: 2,
                                        service_name: el.service_name,
                                        gl_account:el.gl_account
                                    };
                                }
                                else {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: el.tax_code,
                                        service_name: el.service_name
                                    };
                                }
                                
                            }));
                        }
                    });

                },
                select: function (event, ui) {
                    event.preventDefault();
                    this.value = ui.item.code;

                    //format biaya2 dengan separator 
                    var raw_price = ui.item.price;
                    //var price = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                     price = raw_price;

                    $("#txt-service-name").val(ui.item.service_name);
                    $("#unit").val(ui.item.unit);
                    $("#currency").val(ui.item.currency);
                    $("#price").val(price);
                    $("#multiply_factor").val(ui.item.multiply);
                    $("#tax_code").val(ui.item.tax_code);
                    $("#qty").val(1);
                    $("#surcharge").val(0);
                    var tax = Number(Number(ui.item.price) * 0.1);
                    var amt = Number(ui.item.price);

                    //Perhitungan Pertama
                    var sub = (Number(ui.item.price) * Number(ui.item.price)) * (Number(ui.item.multiply) / 100);
                    

                    var taxBulat = Math.round(tax);
                    var amtBulat = Math.round(amt);
                    var subBulat = Math.round(sub);
                        
                    var formatTaxBulat = taxBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatAmtBulat = amtBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatSubBulat = subBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    //$('#TOTAL').mask('000.000.000.000.000', { reverse: true });
                    //('#surcharge').mask('000.000.000.000.000', { reverse: true });

                    $("#subtotal").val(formatSubBulat);
                    $("#tax_amount").val(formatTaxBulat);
                    $("#amount").val(formatAmtBulat);
                    document.getElementById("qty").focus();
                }
            });

            // SAMPLE SELECT2
            $(".js-example-basic-single").select2();


            getServicesCode(aData[2]);
            selectingServiceCode();

            //Tax : (Amount+Sourcharge) *10%
            $('#surchargex').on('change', function (e) {
                var amount2 = $('#amount').val();
                var surcharge = $('#surcharge').val();

                //hapus formatting 
                var nfAmount = parseFloat(amount2.split('.').join(''));
                var nfSurcharge = parseFloat(surcharge.split('.').join(''));

                //isi amount
                var prc = $('#price').val();
                var multiply_factor = $('#multiply_factor').val();
                var qty = $('#qty').val();

                //hapus formatting isi amount
                var nfPrc = parseFloat(prc.split('.').join(''));
                var nfMultiplyFactor = parseFloat(multiply_factor.split('.').join(''));
                var nfQty = parseFloat(qty.split('.').join(''));

                var isiAmount = ((Number(nfPrc) * (Number(nfMultiplyFactor) / 100)) * Number(nfQty)) + Number(nfSurcharge);

                var totalAmount = ((Number(prc) * (Number(nfMultiplyFactor) / 100)) * Number(nfQty)) + Number(nfSurcharge);


                var isiAmountBulat = Math.round(totalAmount);


                var formatIsiAmountBulat = isiAmountBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                var formatSurcharge = surcharge.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                $("#amount").val(formatIsiAmountBulat);
                $("#surcharge").val(formatSurcharge);

            });

            //Sub Total : Amount + Tax 
            $('#qty').on('change', function (e) {
                var amount = $('#amount').val();
                var prc = $('#price').val();
                var qty = $('#qty').val();
                var multiply_factor = $('#multiply_factor').val();
                var surcharge = $('#surcharge').val(); // variabel ini sepertinya kemarin kehapus, mknya NaN

                var nfSurcharge = parseFloat(surcharge.split('.').join(''));
                nfPrc = parseFloat(prc.split('.').join(''));

                //var harga = Number(prc) * Number(qty) * (Number(multiply_factor) / 100);
                //var hitungraw = Number(harga);

                if (multiply_factor == 1) {
                    var htg1 = prc * qty;
                }
                else {
                    var htg1 = (prc * qty) * (multiply_factor / 100);
                }

                var rounding = Math.round(htg1);

                //var amount = (Number(nfPrc) * (Number(multiply_factor) / 100)) * (Number(qty) + nfSurcharge);
                var amountBulat = Math.round(amount);
                console.log(amountBulat);

                var formatAmountBulat = rounding.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                $('#amount').val(formatAmountBulat);
                $('#surcharge').val(nfSurcharge);
            });

            $("#mask_date1").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $("#mask_date2").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            $("#qtyx").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            $("#surcharge").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });
        }

        function saveRow(oTable, nRow) {

            var service_code = $('.nama-service-code2').val();
            var service_name = $('.service_name').val();
            var currency = $('.currency').val();
            var price = $('.price').val();
            var multiply_factor = $('.multiply_factor').val();
            var unit = $('.unit').val();

            //var namaKemasan = $('.nama-service-code2').text();

            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(service_code, nRow, 2, false);
            oTable.fnUpdate(service_name, nRow, 3, false);
            oTable.fnUpdate(currency, nRow, 4, false);
            oTable.fnUpdate(price, nRow, 5, false);
            oTable.fnUpdate(multiply_factor, nRow, 6, false);
            oTable.fnUpdate(unit, nRow, 8, false);

            //oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            //oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            //oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            //oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 7, false);
            //oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[10].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[11].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[12].value, nRow, 12, false);
            //oTable.fnUpdate(' <a class="delete" href="">Delete</a>', nRow, 13, false);
            oTable.fnUpdate('<center><a class="btn default btn-xs red delete" id="delete"><i class="fa fa-trash"></i></a></center>', nRow, 13, false)
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(kodeKemasan, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[10].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[12].value, nRow, 12, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 22, false);
            oTable.fnDraw();
        }

        var table = $('#editable_transvb');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                },
                {
                    "targets": [12],
                    "width": "30%"
                }
            ]
        });

        var tableWrapper = $("#editable_new_transvb");

        var nEditing = null;
        var nNew = false;

        $('#editable_new_transvb').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previous row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    /*
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                    */
                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            /*
            swal({
                title: "Are you sure?",
                text: "Are you sure want to delete this detail.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    swal("Deleted!", "Data Deleted Successfully.", "success");
                }
                else {
                }
            });
            */


            //Konfirmasi akan hapus data detail

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            else {
                var baris = $(this).parents('tr')[0];
                var table = $('#editable_transvb').DataTable();
                var data = table.row(baris).data();
                var subtotal = data[10];
                //console.log(data[10]);

                var nfX = parseFloat(subtotal.split('.').join(''));
                var t = $("#TOTAL").val();
                var total = Number(t) - Number(nfX);
                $("#TOTAL").val(total);
            }
            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted!");


        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            var localstore = 0;
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && document.getElementById('save')) {
                /* Editing this row and want to save it */
                var x = $("#amount").val();
                var nfX = parseFloat(x.split('.').join(''));
                var t = $("#TOTAL").val();
                var total = Number(t) + Number(nfX);
                $("#TOTAL").val(total);

                /*
                var asdf = localStorage.setItem("hitung", total);
                var retrievedObject = localStorage.getItem('hitung');
                var ser = retrievedObject + Number(x) ;
                console.log(ser);
                */

                //console.log(x);
                //console.log(t);
                //console.log(total);

                saveRow(oTable, nEditing);
                nEditing = null;
                swal('Success', 'Detail Data Updated Successfully', 'success');
                //alert("Updated!");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = false; // tambahan
            }
        });

        $('#btn-update').click(function () {
            var tvb_d = [];
            var id_header = "";
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var POSTING_DATE = $('#POSTING_DATE').val();
            var SERVICES_GROUP = $('#SERVICES_GROUP').val();
            var COSTUMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_MDM = $('#CUSTOMER_MDM').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR').val();
            var TOTAL = $('#TOTAL').val();

            // Pengecekan save kecil
            var DTAneka = $('#editable_transvb').dataTable();
            var all_row = DTAneka.fnGetNodes();
            for (var i = 0; i < all_row.length; i++) {
                var cek = $(all_row[i]).find('input[name="service-code"]').val();
            }
            console.log('cek ' + cek);
            
            if (cek) {
                swal('Warning', 'Please Save Transaction Detail!', 'warning');
            }
            else {
                //console.log('query insert');
                if (PROFIT_CENTER && POSTING_DATE && SERVICES_GROUP && COSTUMER_ID && CUSTOMER_MDM && INSTALLATION_ADDRESS && CUSTOMER_SAP_AR) {
                    var param = {
                        PROFIT_CENTER: PROFIT_CENTER,
                        POSTING_DATE: POSTING_DATE,
                        SERVICES_GROUP: SERVICES_GROUP,
                        COSTUMER_ID: COSTUMER_ID,
                        COSTUMER_MDM: CUSTOMER_MDM,
                        INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                        CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                        TOTAL: TOTAL
                    };


                    //var cek = $("#CUSTOMER_SAP_AR").val();
                    //cekPiut("1M", cek);
                    //var xPiut = $("#cPiut").val();
                    //console.log(xPiut);
                    //if (xPiut != "X") {
                    var document_number = "";
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransVB/SaveHeader"
                    });
                    req.done(function (data) {
                        id_header = data.trans_id;
                        console.log('id header ' + id_header);
                        var count_dt = table.fnGetData();
                        var grandtotal = 0;
                        var ret = null;
                        for (var i = 0; i < count_dt.length; i++) {
                            var item = table.fnGetData(i);

                            var servicePrice = item[5];
                            var nfServicePrice = parseFloat(servicePrice.split('.').join(''));

                            var amount = item[10];
                            var nfAmount = parseFloat(amount.split('.').join(''));

                            //split unit, ambil kode unit yg dibaca SAP
                            var rawUnit = item[8];
                            var splitRaw = rawUnit.split(" | ");
                            var valueUnitSAP = splitRaw[0];
                            console.log('value unit sap ' + valueUnitSAP);

                            var param2 = {
                                START_DATE: item[0],
                                END_DATE: item[1],
                                SERVICE_CODE: item[2],
                                SERVICE_NAME: item[3],
                                CURRENCY: item[4],
                                PRICE: nfServicePrice,
                                MULTIPLY_FACTOR: item[6],
                                QUANTITY: item[7],
                                UNIT: valueUnitSAP,
                                SURCHARGE: item[9],
                                AMOUNT: nfAmount,
                                TAX_CODE: item[11],
                                REMARK: item[12]
                            };

                            ret = $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param2),
                                method: "POST",
                                url: "/TransVB/SaveDetail"
                            });
                        }
                        //post sap
                        ret.done(function (data) {
                            var billing_no = id_header;
                            $.ajax({
                                contentType: "application/json",
                                method: "POST",
                                url: "/TransVB/postToSAP",
                                data: "{ BILL_ID:'" + id_header + "' , BILL_TYPE: 'ZM03'}",
                                success: function (data) {
                                    if (data.status === 'S') {
                                        swal('Success', 'Billing ID ' + id_header + ', ' + data.message, 'success').then(function (isConfirm) {
                                            window.location = "/TransVB";
                                        });
                                    } else {
                                        /*
                                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                                            window.location = "/TransVB";
                                        });
                                        */
                                        $.ajax({
                                            type: "POST",
                                            url: "/TransVB/GetMsgSAP",
                                            data: "{ BILL_ID:'" + billing_no + "'}",
                                            contentType: "application/json; charset=utf-8",
                                            success: function (data) {
                                                var tampung = "";
                                                for (var i = 0; i < data.length; i++) {
                                                    var x = i % 2;

                                                    //console.log(data[i]);
                                                    if (x === 1) {
                                                        console.log(data[i]);
                                                        tampung += '<ul><li>' + data[i] + '</li></ul>';
                                                    }
                                                }
                                                swal('Failed', tampung, 'error').then(function (isConfirm) {
                                                    window.location = "/TransVB";
                                                });
                                            }
                                        });
                                    }
                                },
                                error: function (data) {
                                    swal('Failed', 'Post to SAP Failed to Add Data.', 'error');
                                    console.log(data.responeText);
                                }

                            });
                        });
                        App.unblockUI();
                    });
                    //} end of cek piut
                }// END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM
                else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }// End of save kecil

        });

        $('#CUSTOMER_MDM').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/TransVB/Customer",
                    data: "{ MPLG_NAMA:'" + inp + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_ID").val(ui.item.code);
                $("#INSTALLATION_ADDRESS").val(ui.item.address);
                $("#CUSTOMER_SAP_AR").val(ui.item.sap);
                //cekPiut("1D", ui.item.sap);
            }
        });

    }
    //---SELECT 2 SERVICES CODE
    var getServicesCode = function (xxx) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.SERVICE_CODE + " (" + repo.SERVICE_NAME + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.SERVICE_NAME || repo.text;
        }

        $(".nama-service-code").select2({

            allowClear: true,
            width: "on",
            ajax: {
                url: "/TransVB/GetDataServiceCodeSelect2",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: item.data.SERVICE_CODE };
                callback(data);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,

            placeholder: "ketikkan service name"
        });
    }

    var selectingServiceCode = function () {
        $(".nama-service-code").on("select2:select", function (e) {
            console.log("ok");
            var service_code = e.params.data.SERVICE_CODE;
            var service_name = e.params.data.SERVICE_NAME;
            var nRow = $(this).parents('tr')[0];
            var aData = $('#editable_transvb').dataTable().fnGetData(nRow);
            $('#editable_transvb').dataTable().fnUpdate(service_code, nRow, 2, false);
            $('#editable_transvb').dataTable().fnUpdate(service_name, nRow, 3, false);

            //$('#editable_transvb').dataTable().fnUpdate(namaGudlap, nRow, 4, false);
        });
    }
    //--- END OF SELECT 2 OTHER SERVICES
    var cekPiut = function (doc_type, cust_code) {
        $.ajax({
            type: "POST",
            url: "/TransVB/cekPiutangSAP",
            data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/TransVB/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
            }
        });

    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransVB";
        });
    }


    return {

        init: function () {
            handleTable();
            batal();
            janganEnter();
            comboServiceGroup();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesEditable.init();

    //ambil tanggal sysdate js
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#POSTING_DATE').val(today);
});