﻿var TransContractBilling = function () {

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
            $('.date-picker-limited').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                startDate: 1+'/'+(new Date().getMonth()+1)+'/'+(new Date().getFullYear()),
                endDate: (new Date(new Date().getFullYear(), new Date().getMonth()+1, 0).getDate())+'/'+(new Date().getMonth()+1)+'/'+(new Date().getFullYear()),
                autoclose: true
            });
        }
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#tabel-create-billing')) {
                $('#tabel-create-billing').DataTable().destroy();
            }

            $('#tabel-create-billing').DataTable({
                "ajax": {
                    "url": "TransContractBilling/GetDataFilterBilling",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.contract = $('#CONTRACT_NO').val();
                        data.customer = $('#CUSTOMER_CODE').val();
                        data.periode = $('#PERIODE').val();
                        data.date_end = $('#DATE_END').val();
                        data.select_cabang = $('#BE_NAME').val();
                        console.log(data);
                    }

                },
                "columns": [
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "BILLING_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "INSTALLMENT_AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "render": function (data, type, full) {
                            //var aksi = '';
                            //if (full.ROLE_PROPERTY != '0') {
                            //    aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';
                            //}
                            var aksi = '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing" title="Posting"><i class="fa fa-paper-plane-o"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewTransContractBillingModal" class="btn btn-icon-only green" id="btn-detail" title="View Detail"><i class="fa fa-eye"></i></a>' +
								'<a class="btn btn-icon-only yellow" id="btn-pranota" title = "Pranota"><i class="fa fa-print"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title = "Pranota"><i class="fa fa-print"></i></a>';
                            return aksi;
							
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {
            //$('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#tabel-create-billing')) {
                $('#tabel-create-billing').DataTable().destroy();
            }

            $('#tabel-create-billing').DataTable({
                "ajax": {
                    "url": "TransContractBilling/GetDataShowAll",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "BE_NAME"
                    },
                    {
                        "data": "BILLING_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME"
                    },
                    {
                        "data": "INSTALLMENT_AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "render": function (data, type, full) {
                            var mplg = full.CUSTOMER_NAME.split(" - ");
                            var link = "https://ibs-xapi.pelindo.co.id/api//enotav2/cetakPdf?me=" + btoa("pranota") + "&ne=" + btoa(full.SAP_DOC_NO) + "&ze=" + btoa(full.BILLING_NO) + "&ck=" + btoa(full.BRANCH_ID) + "&mk=" + btoa(mplg[0]) + "&nu=" + btoa("remote") + "&mn=" + btoa(full.CONTRACT_NAME);
                            //var aksi = '';
							var aksi = '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';
                            /*if (full.ROLE_PROPERTY != '0') {
                                aksi += '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';
                            }*/
                            aksi += '<a data-toggle="modal" href="#viewTransContractBillingModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>' + 
                                '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Print Pranota" target="_blank"><i class="fa fa-print"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }
        
    // Aksi Button Untuk POST BILLING
    var postBilling = function () {
        $('body').on('click', 'tr #btn-post-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-create-billing').DataTable();
            var data = table.row(baris).data();

            var ID = data["ID"];
            var POSTING_DATE = $('#POSTING_DATE').val();
            var BILLING_CONTRACT_NO = data["BILLING_CONTRACT_NO"];
            var BE_ID = data["BE_ID"];
            var PROFIT_CENTER_ID = data["PROFIT_CENTER_ID"];
            var PROFIT_CENTER_CODE = data["PROFIT_CENTER_CODE"];
            var CUSTOMER_CODE = data["CUSTOMER_CODE"];
            var CUSTOMER_CODE_SAP = data["CUSTOMER_CODE_SAP"];
            var BILLING_DUE_DATE = data["BILLING_DUE_DATE"];
            var BILLING_PERIOD = data["BILLING_PERIOD"];
            var FREQ_NO = data["FREQ_NO"];
            var NO_REF1 = data["NO_REF1"];
            var NO_REF2 = data["NO_REF2"];
            var NO_REF3 = data["NO_REF3"];
            var CURRENCY_CODE = data["CURRENCY_CODE"];
            var BEGIN_BALANCE_AMOUNT = data["BEGIN_BALANCE_AMOUNT"];
            var INSTALLMENT_AMOUNT = data["INSTALLMENT_AMOUNT"];
            var ENDING_BALANCE_AMOUNT = data["ENDING_BALANCE_AMOUNT"];
            var BILLING_FLAG_TAX1 = data["BILLING_FLAG_TAX1"];
            var BILLING_FLAG_TAX2 = data["BILLING_FLAG_TAX2"];
            var LEGAL_CONTRACT_NO = data["LEGAL_CONTRACT_NO"];
            

            if (POSTING_DATE)
                {
                swal({
                    title: 'Warning',
                    text: "Are you sure want to post this data to Billing?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if (isConfirm) {

                            App.blockUI({ boxed: true });

                            var param = {
                                ID: ID,
                                POSTING_DATE: POSTING_DATE,
                                BILLING_CONTRACT_NO: BILLING_CONTRACT_NO.toString(),
                                BE_ID: BE_ID,
                                PROFIT_CENTER_ID: PROFIT_CENTER_ID,
                                PROFIT_CENTER_CODE: PROFIT_CENTER_CODE,
                                CUSTOMER_CODE: CUSTOMER_CODE,
                                CUSTOMER_CODE_SAP: CUSTOMER_CODE_SAP,
                                BILLING_DUE_DATE: BILLING_DUE_DATE,
                                BILLING_PERIOD: BILLING_PERIOD,
                                FREQ_NO: FREQ_NO,
                                NO_REF1: NO_REF1,
                                NO_REF2: NO_REF2,
                                NO_REF3: NO_REF3,
                                CURRENCY_CODE: CURRENCY_CODE,
                                BEGIN_BALANCE_AMOUNT: BEGIN_BALANCE_AMOUNT,
                                INSTALLMENT_AMOUNT: INSTALLMENT_AMOUNT,
                                ENDING_BALANCE_AMOUNT: ENDING_BALANCE_AMOUNT,
                                BILLING_FLAG_TAX1: BILLING_FLAG_TAX1,
                                BILLING_FLAG_TAX2: BILLING_FLAG_TAX2,
                                LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO
                            };

                            App.blockUI({ boxed: true });
                            console.log('stringify param ' + JSON.stringify(param));
                            $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "/TransContractBilling/SaveHeader",
                                timeout: 30000,

                                success: function (data) {
                                    //var id = data.ID;
                                    var billing_contract_no = data.billing_contract_no;
                                    var billing_no = data.billing_no;
                                    //console.log(data);
                                    //console.log(billing_contract_no);
                                    //console.log(billing_no);
                                    var p = {
                                        //ID: id,
                                        BILLING_CONTRACT_NO: billing_contract_no,
                                        BILL_ID: billing_no,
                                        BILL_TYPE: 'ZB01',
                                        POSTING_DATE: POSTING_DATE
                                    };

                                    //$.ajax({
                                    //    contentType: "application/json",
                                    //    data: JSON.stringify(p),
                                    //    method: "POST",
                                    //    url: "TransContractBilling/postToSAP",
                                    //    success: function (data) {
                                    //        swal('Success', 'Billing ID ' + billing_no + ',  With SAP Document Number : ' + data.document_number, 'success').then(function (isConfirm) {
                                    //        });
                                    //    },
                                    //    error: function (data) {
                                    //        swal('Failed', 'Failed to Add Data.', 'error');
                                    //        //console.log(data.responeText);
                                    //    }
                                    //});
                                    /** BYPASS SAP **/
                                    $.ajax({
                                        contentType: "application/json",
                                        data: JSON.stringify(p),
                                        method: "POST",
                                        url: "TransContractBilling/postToSAP",

                                        success: function (data) {
                                            App.unblockUI();
                                            table.ajax.reload(null, false);
                                            if (data.status === 'S') {
                                                //swal('Success', 'Billing ID ' + billing_no + ',  With SAP Document Number : ' + data.document_number, 'success').then(function (isConfirm) {
                                                //});

                                                //kirim ke silapor
                                                p = {
                                                    ID: ID,
                                                    BILLING_CONTRACT_NO: billing_contract_no,
                                                    BILL_ID: billing_no,
                                                    SAP_DOC_NO: data.document_number,
                                                    BILL_TYPE: 'ZB01',
                                                    POSTING_DATE: POSTING_DATE
                                                };
                                                $.ajax({
                                                    contentType: "application/json",
                                                    data: JSON.stringify(p),
                                                    method: "POST",
                                                    url: "TransContractBilling/sendToSilapor",
                                                    success: function (datas) {
                                                        App.unblockUI();
                                                        if (datas.status === 'S') {
                                                            swal('Success', 'Billing ID ' + billing_no + ',  With SAP Document Number : ' + data.document_number, 'success').then(function (isConfirm) {
                                                            });
                                                        } else {
                                                            swal('Failed', 'Gagal Kirim Ke Silapor', 'error');
                                                        }
                                                    }
                                                });
                                                
                                            } else {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "/TransContractBilling/GetMsgSAP",
                                                    data: JSON.stringify(billing_no),
                                                    contentType: "application/json; charset=utf-8",
                                                    success: function (data) {
                                                        var tampung = "";
                                                        for (var i = 0; i < data.length; i++) {
                                                            var x = i % 2;

                                                            console.log(data[i]);
                                                            if (x === 1) {
                                                                console.log(data[i]);
                                                                tampung += '<ul><li>' + data[i] + '</li></ul>';
                                                            }
                                                        }
                                                        swal('Failed', tampung, 'error').then(function (isConfirm) {
                                                            window.location = "/TransContractBilling";
                                                        });
                                                    }
                                                });
                                            }
                                        },
                                        error: function (data) {
                                            swal('Failed', 'Failed to Add Data.', 'error');
                                            console.log(data.responeText);
                                        }

                                    });
                                    
                                    App.unblockUI();
                                },
                                error: function (data) {
                                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    });
                                    //console.log(data.responeText);
                                }
                            });
                        }
                    }
                });
            }
            else
            {
                swal('Warning', 'Please Fill The Posting Date Field!', 'warning');
            }
        });
    }

    var initDetilTransactionContract = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-create-billing').DataTable();
            var data = table.row(baris).data();
            var tableDetilBilling = $('#table-detil-billing').DataTable();
            tableDetilBilling.destroy();

            $("#DETAILBILLING_NO").val(data["BILLING_NO"]);
            $("#DETAILCONTRACT_NO").val(data["BILLING_CONTRACT_NO"]);
            $("#DETAILLEGAL_CONTRACT_NO").val(data["LEGAL_CONTRACT_NO"]);
            $("#DETAILPROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#DETAILCONTRACT_NAME").val(data["CONTRACT_NAME"]);
            $("#DETAILCUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#DETAILCUSTOMER_CODE").val(data["CUSTOMER_CODE"]);
            $("#DETAILCUSTOMER_CODE_SAP").val(data["CUSTOMER_CODE_SAP"]);
            $("#DETAILCREATION_DATE").val(data["BILLING_DUE_DATE"]);
            $("#DETAILPOSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAILSAP_DOC_NO").val(data["SAP_DOC_NO"]);
            $("#DETAILFREQ_NO").val(data["FREQ_NO"]);
            $("#DETAILBILLING_PERIOD").val(data["BILLING_PERIOD"]);
            $("#DETAILCURRENCY_CODE").val(data["CURRENCY_CODE"]);
            $("#DETAILINSTALLMENT_AMOUNT").val(data["INSTALLMENT_AMOUNT"]);
            $("#DETAILOLD_CONTRACT_NO").val(data["OLD_CONTRACT"]);
            //INSTALLMENT_AMOUNT.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            var id_special = data["ID"];

            $('#table-detil-billing').DataTable({

                "ajax":
                {
                    "url": "/TransContractBilling/GetDataDetailBilling/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },

                "columns": [
                    {
                        "data": "CONDITION_TYPE_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_FLAG_TAX1",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CURRENCY_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_QTY",
                        "class": "dt-body-center"
                        //"data": "BILLING_UOM",
                        //"class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_UOM",
                        "class": "dt-body-center"
                        //"data": "BILLING_QTY",
                        //"class": "dt-body-center"
                    },
                    {
                        "data": "GL_ACOUNT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLMENT_AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
        $('body').on('click', 'tr #btn-pranota', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#tabel-create-billing').DataTable();
            var data = table.row(baris).data();

            var contractNo = data["BILLING_CONTRACT_NO"];
            var kodeCabang = "";

            var id_special = data["ID"];
            //var contractNo = data["BILLING_CONTRACT_NO"];
            var billingNo = data["BILLING_NO"];
            //var kodeCabang = data["BRANCH_ID"];

            var BE_ID = data["BE_ID"].toString();

            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(BE_ID),
                method: "POST",
                url: "/Pdf/GetCabang",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.Status === 'S') {
                    kodeCabang = data.Msg;
                    window.open("/Pdf/cetakPdf2?tp=" + btoa("1B") + "&kc=" + btoa(kodeCabang) + "&cn=" + btoa(contractNo), '_blank');
                } else {
                   
                }
            });
            
        });
    }

    //$('body').on('click', 'tr #btn-pranota', function () {
    //    var baris = $(this).parents('tr')[0];
    //    var table = $('#table-create-billing').DataTable();
    //    var data = table.row(baris).data();

    //    var id_special = data["ID"];
    //    var contractNo = data["BILLING_CONTRACT_NO"];
    //    var billingNo = data["BILLING_NO"];
    //    var kodeCabang = data["BRANCH_ID"];
    //    console.log(data);
    //    //window.location = "/Pdf/cetakPdf?tp=" + btoa("1B") + "&bn=" + btoa(billingNo) + "&cn=" + btoa(contractNo) + "&kc=" + btoa(kodeCabang);
    //    window.open("/Pdf/cetakPdf?tp=" + btoa("1B") + "&bn=" + btoa(billingNo) + "&kc=" + btoa(kodeCabang), '_blank');
    //});

    return {
        init: function () {
            postBilling();
            showAll();
            initDetilTransactionContract();
            filter();
            handleDatePickers();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContractBilling.init();
    });
}