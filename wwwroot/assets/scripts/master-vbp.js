﻿var MasterWE = function () {

    var initTable = function () {
        $('#table-aneka-usaha').DataTable({

            "ajax":
            {
                "url": "VariousBusinessPricing/GetDataAnekaUsaha",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER"
                },
                {
                    "data": "SERVICE_GROUP"
                },
                {
                    "data": "SERVICE_CODE"
                },
                {
                    "data": "SERVICE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         /*
                         var aksi = '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                         aksi += '<a data-toggle="modal" href="#detailModal" class="btn btn-icon-only green" id="btn-detil"><i class="fa fa-eye"></i></a>';
                         */
                         var aksi = '<a class="btn btn-icon-only red" id="btn-gear"><i class="fa fa-gear"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var gear = function () {
        $('body').on('click', 'tr #btn-gear', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();

            window.location = "/VariousBusinessPricing/gear/" + data["ID"];
        });
    }

    var initTableVBP = function () {
        var uriId = $('#UriId').val();
        $('#table-vbp').DataTable({
            "ajax": {
                "url": "/VariousBusinessPricing/GetDataVariousBusinessPricing/" + 12,
                "type": "GET",
            },
            "columns": [
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "SERVICE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER_NAME",
                },
                {
                    "data": "ACTIVE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        //Edit untuk testing pertama kali (sesuai db rental object)
                        //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                        var aksi = '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                      //  aksi += '<a class="btn btn-icon-only blue" id="btn-ubah-vbp"><i class="fa fa-exchange"></i></a>';
                        //aksi += '<a class="btn btn-icon-only red" id="btn-hapus"><i class="fa fa-close"></i></a>';

                        /*
                        if (full.KD_AKTIF === '1') {
                            aksi += '<a class="btn default btn-xs red" id="btn-status"><i class="fa fa-close"></i> Non-aktifkan</a>';
                        } else {
                            aksi += '<a class="btn default btn-xs blue" id="btn-status"><i class="fa fa-check-square"></i> Aktifkan</a>';
                        }
                        */
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    //Init table measurement untuk ditampilkan di view detail
    var initTableViewVBP = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();

            var id_vb = data["VB_ID"];

            $('#table-view-detail').DataTable({
                "ajax": {
                    "url": "/VariousBusinessPricing/GetDataVariousBusinessPricing/" + id_vb,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "REF_DATA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DESCRIPTION"
                    },
                    {
                        "data": "POWER_CAPACITY"
                    },
                    {
                        "data": "VALID_FROM"
                    },
                    {
                        "data": "VALID_TO"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    // Ubah VBP
    var ubahvbp = function () {
        $('body').on('click', 'tr #btn-ubah-vbp', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-vbp').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/VariousBusinessPricing/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    // Detail water and electricity
    var detailVBP = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-vbp').DataTable();
            var data = table.row(baris).data();

            $("#id").val(data["ID"]);
            $("#be_id").val(data["BE_ID"]);
            $("#be_name").val(data["BE_NAME"]);
            $("#profit_center_name").val(data["PROFIT_CENTER_NAME"]);
            $("#service_name").val(data["SERVICE_NAME"]);
            $("#profit_center_name").val(data["PROFIT_CENTER_NAME"]);
            $("#service_code").val(data["SERVICE_CODE"]);
            $("#valid_from").val(data["VALID_FROM"]);
            $("#valid_to").val(data["VALID_TO"]);
            $("#multiply_function").val(data["MULTIPLY_FUNCTION"]);
            $("#condition_pricing_unit").val(data["CONDITION_PRICING_UNIT"]);
            $("#condition_type").val(data["CONDITION_TYPE"]);

        });
    }

    //Function untuk Save Pricing
    var save_pricing = function () {
        $('#save-pricing').click(function () {
            //alert("asdf");
            var RO_NUMBER = $('#RO_NUMBERA').val();
            var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var CONDITION_PRICING_UNIT = $('#CONDITION_PRICING_UNIT').val();
            var SERVICE_NAME = $('#SERVICE_NAME').val();
            var CONDITION_TYPE = $('#CONDITION_TYPE').val();
            var VALID_FROM = $('#VALID_FROMA').val();
            var VALID_TO = $('#VALID_TOA').val();
            var MULTIPLY_FUNCTION = $('#MULTIPLY_FUNCTION').val();
            var CONDITION_PRICING_UNIT = $('#CONDITION_PRICING_UNIT').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (RO_NUMBER && MEASUREMENT_TYPE && VALID_FROM && AMOUNT && UNIT) {
                    var param = {
                        RO_NUMBER: RO_NUMBER,
                        MEASUREMENT_TYPE: MEASUREMENT_TYPE,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        AMOUNT: AMOUNT,
                        UNIT: UNIT
                    };

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/AddDataDetailMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addMeasurement').modal('hide');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                $('#addMeasurement').modal('hide');
                                swal('Failed', data.message, 'error');
                            });
                        }
                    });
                } else {
                    $('#addMeasurement').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    // alert("warning");
                }
            }
        });
    }

    var deletedata = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-we').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["SERVICES_ID"],
                        method: "get",
                        url: "WaterAndElectricity/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var status = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-vbp').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Apakah Anda yakin?',
                text: "Status aktif tarif akan diubah.",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["RO_NUMBER"],
                        method: "get",
                        url: "MasterTarif/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Berhasil', data.message, 'success');
                        } else {
                            swal('Gagal', data.message, 'error');
                        }
                    });
                }
            });
        });
    }


    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/VariousBusinessPricing/Add';
        });
    }

    return {
        init: function () {
            initTableVBP();
            status();
            deletedata();
            add();
            ubahvbp();
            detailVBP();
            initTable();
            gear();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterWE.init();
    });
}