﻿var TableDatatablesEditable = function () {
//-------------------------------------------------BEGIN JS trans-contract-list.js-----------------------------------------------------
    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransContract";
        });
    }

    //Data Table Condition 
    var tablec = $('#condition');
    var cTable = tablec.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });

    //Data Table Manual Frequency
    var fablem = $('#manual-frequency');
    var fTable = fablem.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });

    //Data Table Memo
    var mablem = $('#table-memo');
    var mTable = mablem.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });

    //Data Table Sales Based
    var sablem = $('#sales-based');
    var sTable = sablem.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });
    

    //Data Table Reporting Rule
    /*
    var rablem = $('#reporting-rule');
    var rTable = rablem.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });
    */

    var filter = function () {
        $('#btn-filter').click(function () {
            //$('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/TransContract/GetDataFilter/",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_OFFER_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_OFFER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TERM_IN_MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_USAGE_F",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
        // Add event listener for opening and closing details
        $('#table-filter-detail').on('click', 'tr #btn-cus', function (e) {
            App.blockUI({ boxed: true });
            e.preventDefault();
            $('#condition-option > option').html('');

            $('#rental-object-co > tbody').html('');
            $('#condition > tbody').html('');
            $('#manual-frequency > tbody').html('');
            $('#table-memo > tbody').html('');
            $('#sales-based > tbody').html('');
            //$('#reporting-rule > tbody').html('');

            var tableObjectCo = $('#rental-object-co').DataTable();
            tableObjectCo.destroy();

            //var tableConditionC = $('#condition').DataTable();
            //tableConditionC.destroy();

            //var tableManualFrequencyC = $('#manual-frequency').DataTable();
            //tableManualFrequencyC.destroy();

            //var tableMemoC = $('#table-memo').DataTable();
            //tableMemoC.destroy();

            //var tableSalesBasedC = $('#sales-based').DataTable();
            //tableSalesBasedC.destroy();

            //var tableReportingRuleC = $('#reporting-rule').DataTable();
            //tableReportingRuleC.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();

            var contract_offer_name = data['CONTRACT_OFFER_NAME'];
            var contrac_offer_no = data['CONTRACT_OFFER_NO'];
            var contract_start_date = data['CONTRACT_START_DATE'];
            var contract_end_date = data['CONTRACT_END_DATE'];
            var term_in_months = data['TERM_IN_MONTHS'];
            var business_partner_name = data['CUSTOMER_NAME'];
            var customer_ar = data['CUSTOMER_AR'];
            var currency = data['CURRENCY'];
            var profit_center_f = data['PROFIT_CENTER_F'];
            var contract_usage_f = data['CONTRACT_USAGE_F'];
            var contract_offer_type_f = data['CONTRACT_OFFER_TYPE_F'];
            var business_entity_f = data['BUSINESS_ENTITY_F'];
            var rental_request_no = data['RENTAL_REQUEST_NO'];
            var business_partner = data['CUSTOMER_ID'];
            var old_contract = data['OLD_CONTRACT'];

            $('#CONTRACT_OFFER_NAME').val(contract_offer_name);
            $('#CONTRACT_NAME').val(contract_offer_name);
            $('#CONTRACT_OFFER_NO').val(contrac_offer_no);
            $('#CONTRACT_START_DATE').val(contract_start_date);
            $('#CONTRACT_END_DATE').val(contract_end_date);
            $('#TERM_IN_MONTHS').val(term_in_months);
            $('#BUSINESS_PARTNER_NAME').val(business_partner_name);
            $('#CUSTOMER_AR').val(customer_ar);
            $('#CURRENCY').val(currency);

            $('#PROFIT_CENTER_F').val(profit_center_f);
            $('#CONTRACT_USAGE_F').val(contract_usage_f);
            $('#CONTRACT_OFFER_TYPE_F').val(contract_offer_type_f);
            $('#BUSINESS_ENTITY_F').val(business_entity_f);
            $('#RENTAL_REQUEST_NO').val(rental_request_no);
            $('#BUSINESS_PARTNER').val(business_partner);
            $('#OLD_CONTRACT').val(old_contract);

            //Data Table RO 
            var tabler = $('#rental-object-co');
            var oTable = tabler.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": false,
                "lengthChange": false,
                "bSort": false
            });

            // Ajax untuk mengisi data table rental-object-co
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataObject/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    oTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        oTable.fnAddData([jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].OBJECT_NAME, jsonList.data[i].START_DATE, jsonList.data[i].END_DATE, jsonList.data[i].LUAS_TANAH, jsonList.data[i].LUAS_BANGUNAN, jsonList.data[i].INDUSTRY]);
                    }
                }
            });
            
            
            // Ajax untuk mengisi data table condition
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataCondition/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    cTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    var coa_prod=null;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        if (jsonList.data[i].COA_PROD==null || jsonList.data[i].COA_PROD == ''){
                            coa_prod = "-";
                        }
                        else{
                            coa_prod = jsonList.data[i].COA_PROD;
                        }
                        console.log("COA PROD >> " + coa_prod);
                        cTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].VALID_FROM, jsonList.data[i].VALID_TO, jsonList.data[i].MONTHS, jsonList.data[i].STATISTIC, jsonList.data[i].UNIT_PRICE, jsonList.data[i].AMT_REF, jsonList.data[i].FREQUENCY, jsonList.data[i].START_DUE_DATE, jsonList.data[i].MANUAL_NO, jsonList.data[i].FORMULA, jsonList.data[i].MEASUREMENT_TYPE, jsonList.data[i].LUAS, jsonList.data[i].TOTAL, jsonList.data[i].NJOP_PERCENT, jsonList.data[i].KONDISI_TEKNIS_PERCENT, jsonList.data[i].SALES_RULE, jsonList.data[i].TOTAL_NET_VALUE, coa_prod]);
                    }
                }
            });

            // Ajax untuk mengisi data table manual-frequency
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataManualFrequency/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    fTable.fnClearTable();
                    var jsonListx = data
                    var listItems = "";
                    for (var i = 0; i < jsonListx.data.length; i++) {
                        fTable.fnAddData([jsonListx.data[i].MANUAL_NO, jsonListx.data[i].CONDITION_FREQ, jsonListx.data[i].DUE_DATE, jsonListx.data[i].NET_VALUE, jsonListx.data[i].QUANTITY, jsonListx.data[i].UNIT]);
                    }
                }
            });

            // Ajax untuk mengisi data table memo
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataMemo/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    mTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        mTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITON_TYPE_MEMO, jsonList.data[i].MEMO]);
                    }
                }
            });

            // Ajax untuk mengisi data table sales based
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataSalesBased/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    sTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        sTable.fnAddData([jsonList.data[i].SR, jsonList.data[i].SALES_TYPE, jsonList.data[i].NAME_OF_TERM, jsonList.data[i].PAYMENT_TYPE, jsonList.data[i].CALC_FROM, jsonList.data[i].CALC_TO, jsonList.data[i].UNIT, jsonList.data[i].PERCENTAGE, jsonList.data[i].TARIF, jsonList.data[i].RR, jsonList.data[i].MINSALES, jsonList.data[i].MINPRODUCTION]);
                    }
                }
            });

            // Ajax untuk mengisi data table reporting rule
            /*
            $.ajax({
                type: "GET",
                url: "/TransContract/GetDataReportingRule/" + contrac_offer_no,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    rTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        rTable.fnAddData([jsonList.data[i].REPORTING_RULE_NO, jsonList.data[i].SALES_TYPE, jsonList.data[i].TERM_OF_REPORTING_RULE]);
                    }
                }
            });
            */
            App.unblockUI();

            $('#addDetailModal').modal('hide');
        });
    }

    //var simpanData = function () {
    //    $('#btn-simpan').click(function () {
    //        // Var Header Data
    //        var raw_contract_type = $('#CONTRACT_OFFER_TYPE_F').val();
    //        var ct_split = raw_contract_type.split(" - ");

    //        //Hasil split untuk mendapatkan code contract type
    //        var CODE_CONTRACT_TYPE = ct_split[0];
    //        var CONTRACT_TYPE = $('#CONTRACT_OFFER_TYPE_F').val();
    //        var CONTRACT_OFFER_NAME = $('#CONTRACT_OFFER_NAME').val();
    //        var CONTRACT_OFFER_NO = $('#CONTRACT_OFFER_NO').val();
    //        var BE_ID = $('#BUSINESS_ENTITY_F').val();
    //        var CONTRACT_NAME = $('#CONTRACT_NAME').val();
    //        var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
    //        var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
    //        var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
    //        var BUSINESS_PARTNER_NAME = $('#BUSINESS_PARTNER_NAME').val();
    //        var CUSTOMER_AR = $('#CUSTOMER_AR').val();
    //        var PROFIT_CENTER = $('#PROFIT_CENTER_F').val();
    //        var CONTRACT_USAGE = $('#CONTRACT_USAGE_F').val();
    //        var IJIN_PRINSIP_NO = $('#IJIN_PRINSIP_NO').val();
    //        var IJIN_PRINSIP_DATE = $('#IJIN_PRINSIP_DATE').val();
    //        var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO').val();
    //        var LEGAL_CONTRACT_DATE = $('#LEGAL_CONTRACT_DATE').val();
    //        var CURRENCY = $('#CURRENCY').val();
    //        var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
    //        var BUSINESS_PARTNER = $('#BUSINESS_PARTNER').val();

            
    //        if (CONTRACT_NAME) {
    //            //Var param untuk insert ke DB
    //            var param = {
    //                CONTRACT_NAME: CONTRACT_NAME,
    //                CURRENCY: CURRENCY,
    //                RENTAL_REQUEST_NO:RENTAL_REQUEST_NO,
    //                CONTRACT_OFFER_NO: CONTRACT_OFFER_NO,
    //                CONTRACT_USAGE: CONTRACT_USAGE,
    //                CONTRACT_TYPE: CONTRACT_TYPE,
    //                BUSINESS_PARTNER_NAME: BUSINESS_PARTNER_NAME,
    //                CUSTOMER_AR: CUSTOMER_AR,
    //                BE_ID: BE_ID,
    //                PROFIT_CENTER: PROFIT_CENTER,
    //                IJIN_PRINSIP_NO: IJIN_PRINSIP_NO,
    //                IJIN_PRINSIP_DATE: IJIN_PRINSIP_DATE,
    //                LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO,
    //                LEGAL_CONTRACT_DATE: LEGAL_CONTRACT_DATE,
    //                CONTRACT_START_DATE: CONTRACT_START_DATE,
    //                CONTRACT_END_DATE: CONTRACT_END_DATE,
    //                TERM_IN_MONTHS: TERM_IN_MONTHS,
    //                BUSINESS_PARTNER: BUSINESS_PARTNER,
    //                CODE_CONTRACT_TYPE: CODE_CONTRACT_TYPE
    //            };
    //            //req.done(function(data) {});
    //            // Ajax Save Header
    //            // Ajax Save Data Header
    //            var ex1 = $.ajax();
    //            var ex2 = $.ajax();
    //            var ex3 = $.ajax();
    //            var ex4 = $.ajax();
    //            var ex5 = $.ajax();
    //            var ex6 = $.ajax();
    //            var ex7 = $.ajax();
    //            var id_header = "";

    //            App.blockUI({ boxed: true });
    //            var req = $.ajax({
    //                contentType: "application/json",
    //                data: JSON.stringify(param),
    //                method: "POST",
    //                url: "/TransContract/SaveHeader",

    //                success: function (data) {
    //                    id_header = data.trans_id;
    //                    // Jalankan save detail disini
    //                    // Declare dan getData dari dataTable OBJECT
    //                    req.done(function (data) {
    //                        var DTObject = $('#rental-object-co').dataTable();
    //                        var countDTObject = DTObject.fnGetData();
    //                        for (var i = 0; i < countDTObject.length; i++) {
    //                            var itemDTObject = DTObject.fnGetData(i);
    //                            var paramDTObject = {
    //                                OBJECT_ID: itemDTObject[0],
    //                                OBJECT_TYPE: itemDTObject[1],
    //                                RO_NAME: itemDTObject[2],
    //                                START_DATE: itemDTObject[3],
    //                                END_DATE: itemDTObject[4],
    //                                LUAS_TANAH: itemDTObject[5],
    //                                LUAS_BANGUNAN: itemDTObject[6],
    //                                INDUSTRY: itemDTObject[7]
    //                            }

    //                            // Ajax untuk save detail dari datatable condition
    //                            ex1 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTObject),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailObject",

    //                                success: function (data) {
    //                                    console.log('save detail object success');
    //                                }
    //                            });
    //                        }
    //                    });
    //                    // End of Save data table object

    //                    //Declare dan getData dari dataTable CONDITION
    //                    ex1.done(function (data) {
    //                        var DTCondition = $('#condition').dataTable();
    //                        var countDTCondition = DTCondition.fnGetData();
    //                        for (var i = 0; i < countDTCondition.length; i++) {
    //                            var itemDTCondition = DTCondition.fnGetData(i);

    //                            var fDueDate;

    //                            var rawDueDate = itemDTCondition[9];
    //                            //console.log(rawDueDate);
    //                            if (rawDueDate != null) {
    //                                var arrDueDate = rawDueDate.split(" ");
    //                                fDueDate = arrDueDate[0];
    //                            }
    //                            else {
    //                                fDueDate = null;
    //                            }

    //                            var paramDTCondition = {
    //                                CALC_OBJECT: itemDTCondition[0],
    //                                CONDITION_TYPE: itemDTCondition[1],
    //                                VALID_FROM: itemDTCondition[2],
    //                                VALID_TO: itemDTCondition[3],
    //                                MONTHS: itemDTCondition[4],
    //                                STATISTIC: itemDTCondition[5],
    //                                UNIT_PRICE: itemDTCondition[6],
    //                                AMT_REF: itemDTCondition[7],
    //                                FREQUENCY: itemDTCondition[8],
    //                                START_DUE_DATE: fDueDate,
    //                                MANUAL_NO: itemDTCondition[10],
    //                                FORMULA: itemDTCondition[11],
    //                                MEASUREMENT_TYPE: itemDTCondition[12],
    //                                LUAS: itemDTCondition[13],
    //                                TOTAL: itemDTCondition[14],
    //                                NJOP_PERCENT: itemDTCondition[15],
    //                                KONDISI_TEKNIS_PERCENT: itemDTCondition[16],
    //                                SALES_RULE: itemDTCondition[17],
    //                                TOTAL_NET_VALUE: itemDTCondition[18]
    //                            };
    //                            console.log('paramDTCondition '+paramDTCondition); //lihat berapa data yg terbawa sebelum save
    //                            // Ajax untuk save detail dari datatable condition
    //                            ex3 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTCondition),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailCondition",

    //                                success: function (data) {
    //                                    console.log('save detail condition success');
    //                                }
    //                            });
    //                        }
    //                    });
    //                    //End of save datatable condition

    //                    //Declare dan get data dari dataTable MANUAL FREQUENCY
    //                    ex1.done(function (data) {
    //                        var DTManualFrequency = $('#manual-frequency').dataTable();
    //                        var countDTManualFrequency = DTManualFrequency.fnGetData();
    //                        for (var i = 0; i < countDTManualFrequency.length; i++) {
    //                            var itemDTManualFrequency = DTManualFrequency.fnGetData(i);
    //                            var paramDTManualFrequency = {
    //                                MANUAL_NO: itemDTManualFrequency[0],
    //                                CONDITION: itemDTManualFrequency[1],
    //                                DUE_DATE: itemDTManualFrequency[2],
    //                                NET_VALUE: itemDTManualFrequency[3],
    //                                QUANTITY: itemDTManualFrequency[4],
    //                                UNIT: itemDTManualFrequency[5]

    //                            }

    //                            // Ajax untuk save detail dari datatable manual-frequency
    //                            ex2 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTManualFrequency),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailManualFrequency",
    //                                success: function (data) {
    //                                    console.log(data);
    //                                    console.log('save detail manual frequency success');
    //                                }
    //                            });
    //                        }
    //                    });
    //                    //End of save detail manual-frequency

    //                    //Declare dan get data dari dataTable MEMO
    //                    ex3.done(function (data) {
    //                        var DTMemo = $('#table-memo').dataTable();
    //                        var countDTMemo = DTMemo.fnGetData();
    //                        for (var i = 0; i < countDTMemo.length; i++) {
    //                            var itemDTMemo = DTMemo.fnGetData(i);
    //                            var paramDTMemo = {
    //                                CALC_OBJECT: itemDTMemo[0],
    //                                CONDITION_TYPE: itemDTMemo[1],
    //                                MEMO: itemDTMemo[2]
    //                            }
                        
    //                            // Ajax untuk save detail dari datatable manual-frequency
    //                            ex4 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTMemo),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailMemo",

    //                                success: function (data) {
    //                                    console.log('save detail memo success');
    //                                }
    //                            });
    //                        };
    //                    });

    //                    //Declare dan get data dari dataTable SALES BASED
    //                    ex4.done(function (data) {
    //                        var DTSalesBased = $('#sales-based').dataTable();
    //                        var countDTSalesBased = DTSalesBased.fnGetData();
    //                        for (var i = 0; i < countDTSalesBased.length; i++) {
    //                            var itemDTSalesBased = DTSalesBased.fnGetData(i);
    //                            var paramDTSalesBased = {
    //                                SR: itemDTSalesBased[0],
    //                                SALES_TYPE: itemDTSalesBased[1],
    //                                NAME_OF_TERM: itemDTSalesBased['2'],
    //                                PAYMENT_TYPE: itemDTSalesBased['3'],
    //                                CALC_FROM: itemDTSalesBased['4'],
    //                                CALC_TO: itemDTSalesBased['5'],
    //                                UNIT: itemDTSalesBased['6'],
    //                                PERCENTAGE: itemDTSalesBased['7'],
    //                                TARIF: itemDTSalesBased['8'],
    //                                RR: itemDTSalesBased['9'],
    //                                MINSALES: itemDTSalesBased['10'],
    //                                MINPRODUCTION: itemDTSalesBased['11']
    //                            }

    //                            // Ajax untuk save detail dari datatable manual-frequency
    //                            ex5 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTSalesBased),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailSalesBased",

    //                                success: function (data) {
    //                                    console.log('save detail sales based success');
    //                                }
    //                            });
    //                        };
    //                    });

    //                    //Declare dan get data dari dataTable REPORTING RULE
    //                    /*
    //                    ex5.done(function (data) {
    //                        var DTReportingRUle = $('#reporting-rule').dataTable();
    //                        var countDTReportingRUle = DTReportingRUle.fnGetData();
    //                        for (var i = 0; i < countDTReportingRUle.length; i++) {
    //                            var itemDTSalesBased = DTReportingRUle.fnGetData(i);
    //                            var paramDTReportingRule = {
    //                                REPORTING_RULE_NO: itemDTSalesBased[0],
    //                                SALES_TYPE: itemDTSalesBased[1],
    //                                TERM_OF_REPORTING_RULE: itemDTSalesBased[2]
    //                            }

    //                            // Ajax untuk save detail dari datatable manual-frequency
    //                            ex6 = $.ajax({
    //                                contentType: "application/json",
    //                                data: JSON.stringify(paramDTReportingRule),
    //                                method: "POST",
    //                                url: "/TransContract/SaveDetailReportingRule",

    //                                success: function (data) {
    //                                    console.log('save detail reporting rule success');
    //                                }
    //                            });
    //                        };
    //                    });
    //                    */
                      
    //                    //console.log('Result ID Header yg tersimpan' + id_header);
    //                    // Ajax untuk Generate CashFlow
    //                    ex3.done(function (data) {
    //                        $.ajax({
    //                            contentType: "application/json",
    //                            method: "POST",
    //                            url: "/TransContract/GenerateCashFlow/" + id_header,
    //                            success: function (data) {
    //                                console.log('Generate CashFlow Success');
    //                            }
    //                        });
    //                    });
                        

    //               if (data.status === 'S') {
    //                        //$.ajax({
    //                        //    contentType: "application/json",
    //                        //    method: "POST",
    //                        //    url: "/TransContract/GenerateCashFlow/" + id_header,
    //                        //    success: function (data) {
    //                        //        console.log('Generate CashFlow Success');
    //                        //    }
    //                        //});
    //                    swal('Success', data.message, 'success').then(function (isConfirm) {
    //                        window.location = "/TransContract";
    //                    });
    //                } else {
    //                    swal('Error', data.message, 'success').then(function (isConfirm) {
    //                        window.location = "/TransContract";
    //                    });
    //                }

    //                App.unblockUI();

    //                },
    //                error: function (data) {
    //                    swal('Failed', data.message, 'error').then(function (isConfirm) {
    //                        window.location = "/TransContract";
    //                    });
    //                }
    //            });
    //        }
    //        else {
    //            swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
    //        }
            
    //    });
    //}

    var simpanData = function () {
        $('#btn-simpan').click(function () {
            // Var Header Data
            var raw_contract_type = $('#CONTRACT_OFFER_TYPE_F').val();
            var ct_split = raw_contract_type.split(" - ");

            //Hasil split untuk mendapatkan code contract type
            var CODE_CONTRACT_TYPE = ct_split[0];
            var CONTRACT_TYPE = $('#CONTRACT_OFFER_TYPE_F').val();
            var CONTRACT_OFFER_NAME = $('#CONTRACT_OFFER_NAME').val();
            var CONTRACT_OFFER_NO = $('#CONTRACT_OFFER_NO').val();
            var BE_ID = $('#BUSINESS_ENTITY_F').val();
            var CONTRACT_NAME = $('#CONTRACT_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
            var BUSINESS_PARTNER_NAME = $('#BUSINESS_PARTNER_NAME').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER_F').val();
            var CONTRACT_USAGE = $('#CONTRACT_USAGE_F').val();
            var IJIN_PRINSIP_NO = $('#IJIN_PRINSIP_NO').val();
            var IJIN_PRINSIP_DATE = $('#IJIN_PRINSIP_DATE').val();
            var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO').val();
            var LEGAL_CONTRACT_DATE = $('#LEGAL_CONTRACT_DATE').val();
            var CURRENCY = $('#CURRENCY').val();
            var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
            var BUSINESS_PARTNER = $('#BUSINESS_PARTNER').val();


            if (CONTRACT_NAME) {
                //Var param untuk insert ke DB
                var param = {
                    CONTRACT_NAME: CONTRACT_NAME,
                    CURRENCY: CURRENCY,
                    RENTAL_REQUEST_NO: RENTAL_REQUEST_NO,
                    CONTRACT_OFFER_NO: CONTRACT_OFFER_NO,
                    CONTRACT_USAGE: CONTRACT_USAGE,
                    CONTRACT_TYPE: CONTRACT_TYPE,
                    BUSINESS_PARTNER_NAME: BUSINESS_PARTNER_NAME,
                    CUSTOMER_AR: CUSTOMER_AR,
                    BE_ID: BE_ID,
                    PROFIT_CENTER: PROFIT_CENTER,
                    IJIN_PRINSIP_NO: IJIN_PRINSIP_NO,
                    IJIN_PRINSIP_DATE: IJIN_PRINSIP_DATE,
                    LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO,
                    LEGAL_CONTRACT_DATE: LEGAL_CONTRACT_DATE,
                    CONTRACT_START_DATE: CONTRACT_START_DATE,
                    CONTRACT_END_DATE: CONTRACT_END_DATE,
                    TERM_IN_MONTHS: TERM_IN_MONTHS,
                    BUSINESS_PARTNER: BUSINESS_PARTNER,
                    CODE_CONTRACT_TYPE: CODE_CONTRACT_TYPE
                };
                //req.done(function(data) {});
                // Ajax Save Header
                // Ajax Save Data Header
                //var ex1 = $.ajax();
                //var ex2 = $.ajax();
                //var ex3 = $.ajax();
                //var ex4 = $.ajax();
                //var ex5 = $.ajax();
                //var ex6 = $.ajax();
                //var ex7 = $.ajax();
                var id_header = "";

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransContract/SaveHeader",

                    success: function (data) {

                        var OLD_CONTRACT = $("#OLD_CONTRACT");
                        if (OLD_CONTRACT) {

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!

                            var yyyy = today.getFullYear();
                            if (dd < 10) {
                                dd = '0' + dd;
                            }
                            if (mm < 10) {
                                mm = '0' + mm;
                            }
                            var today = dd + '/' + mm + '/' + yyyy;

                            var param = {
                                CONTRACT_NO: OLD_CONTRACT,
                                STOP_WARNING_DATE: today,
                                MEMO_WARNING: 'Sudah diperpanjang'
                            };
                            console.log(param);
                            App.blockUI({ boxed: true });

                            $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "/NotificationContract/CheckOutDate",
                                timeout: 30000,
                                success: function (data) {
                                    //console.log(data);
                                    //table.ajax.reload(null, false);

                                    if (data.status === 'S') {
                                        //$('#viewCheckOutDate').modal('hide');
                                        swal('Success', 'Old Contract Notification Has Been Stopped', 'success');
                                    } else {
                                        //$('#viewCheckOutDate').modal('hide');
                                        //swal('Failed', 'Something Went Wrong', 'error');
                                    }
                                },
                                error: function (data) {
                                    //console.log(data.responeText);
                                }

                            });
                        }

                        id_header = data.trans_id;

                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransContract";
                            });


                        } else {
                            swal('Error', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransContract";
                            });
                        }

                        App.unblockUI();

                    },
                    error: function (data) {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/TransContract";
                        });
                    }
                });
            }
            else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }

        });
    }

    return {
        init: function () {
            //return var here
            batal();
            filter();
            simpanData();
        }

    };

}();


jQuery(document).ready(function () {
    TableDatatablesEditable.init();
    $('#condition > tbody').html('');
    $('#rental-object-co > tbody').html('');
    $('#manual-frequency > tbody').html('');
});