﻿var TarifPedomanNjop = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TarifPedoman";
        });
    }
    
    // Var untuk handle simpan data
    var simpanData = function () {
        $('#btn-update').click(function () {
            var KEYWORD = $('#NAMA_JALAN').val().toUpperCase();
            var VALUE = $('#VALUE').val();
            var ACTIVE_YEAR = $('#ACTIVE_YEAR').val();
            var ID = $('#ID_EDIT').val();
            var BRANCH_ID = $('#BE_ID_ADD').val();
            $('#addModal').modal('hide');

            if (BRANCH_ID && VALUE && KEYWORD && ACTIVE_YEAR) {
                $('#BE_ID').val(BRANCH_ID);
                var param = {
                    KEYWORD: KEYWORD,
                    VALUE: parseInt(VALUE),
                    BRANCH_ID: parseInt(BRANCH_ID),
                    ACTIVE_YEAR: ACTIVE_YEAR
                }

                if (ID) {
                    param.ID = parseInt(ID)
                }

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TarifPedoman/SaveNJOP",
                    timeout: 3000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.RESULT_STAT) {

                        var beid = $('#BE_ID').val();
                        if (beid) {
                            $('#row-table').css('display', 'block');

                            if ($.fn.DataTable.isDataTable('#table-njop')) {
                                $('#table-njop').DataTable().destroy();
                            }

                            $('#table-njop').DataTable({
                                "ajax":
                                {
                                    "url": "/TarifPedoman/GetDataNJOP?be_id=" + beid,
                                    "type": "GET",
                                },
                                "columns": [
                                    {
                                        "data": "ID",
                                        "visible": false
                                    },
                                    {
                                        "data": "BRANCH_ID",
                                        "visible": false
                                    },
                                    {
                                        "data": "BE_NAME",
                                        "class": "dt-body-center"
                                    },
                                    {
                                        "data": "KEYWORD",
                                        "class": "dt-body-center"
                                    },
                                    {
                                        //"data": "VALUE",
                                        "render": function (data, type, full) {
                                            if (full.VALUE === null) {
                                                return '<span> - </span>';
                                            } else {
                                                return '<span> Rp. ' + full.VALUE + ' </span>';
                                            }
                                        },
                                        "class": "dt-body-center"
                                    },
                                    {
                                        "data": "ACTIVE",
                                        "render": function (data) {
                                            var aksi = '';
                                            if (data) {
                                                aksi = 'AKTIF';
                                            } else {
                                                aksi = 'TIDAK AKTIF';
                                            }
                                            return aksi;
                                        },
                                        "class": "dt-body-center"
                                    },
                                    {
                                        //"data": "VALUE",
                                        "render": function (data, type, full) {
                                            if (full.ACTIVE_YEAR === null) {
                                                return '<span> - </span>';
                                            } else {
                                                return '<span> ' + full.ACTIVE_YEAR + ' </span>';
                                            }
                                        },
                                        "class": "dt-body-center"
                                    },
                                    {
                                        "render": function (data, type, full) {
                                            var aksi = '<a id="btn-edit" class="btn btn-icon-only blue" data-toggle="modal" href="#addModal" title="Edit" ><i class="fa fa-edit"></i></a>';
                                            if (full.ACTIVE) {
                                                aksi += '<a id="btn-inactive" class="btn btn-icon-only red" data-toggle="modal" href="#inactiveModal" title="Inactive" ><i class="fa fa-times"></i></a>';
                                            } else {
                                                aksi += '<a id="btn-activate" class="btn btn-icon-only green" title="Activate" ><i class="fa fa-check"></i></a>';
                                            }
                                            return aksi;
                                        },
                                        "class": "dt-body-center"
                                    }
                                ],
                                "destroy": true,
                                "ordering": false,
                                "processing": true,
                                "serverSide": true,
                                "responsive": false,
                                "lengthMenu": [
                                    [5, 10, 15, 20, 50],
                                    [5, 10, 15, 20, 50]
                                ],
                                "pageLength": 10,
                                "language": {
                                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                                },

                                "filter": true
                            });
                            $('#table-njop').DataTable().column(0).visible(false);
                            $('#table-njop').DataTable().column(1).visible(false);
                        }
                        swal('Berhasil', data.MessageInfo, 'success');
                    } else {
                        swal('Gagal', data.MessageInfo, 'error');
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        })
    }
    
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                //endDate: "Date.now()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });

    }

    var addEdit = function () {

        $('#VALUE').inputmask('decimal', {
            'placeholder': '0',
            min: 0,
            rightAlign: true,
            removeMaskOnSubmit: true,
            onUnMask: function (maskedValue, unmaskedValue) {
                return Number(maskedValue)
            }
        });

        $('#btn-add').click(function () {
            $('#BE_ID_ADD').val($('#BE_ID').val());
            $('#NAMA_JALAN').val('');
            $('#VALUE').val('');
            $('#ID_EDIT').val('');
            $('#ACTIVE_YEAR').val('');
            $('#addModal .modal-title').text('Add NJOP');
        });

        $('#table-njop').on('click', 'tr #btn-edit', function () {
            var oTable = $('#table-njop').dataTable();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $('#NAMA_JALAN').val(aData.KEYWORD);
            $('#VALUE').val(aData.VALUE);
            $('#ID_EDIT').val(aData.ID);
            $('#BE_ID_ADD').val(aData.BRANCH_ID);
            $('#ACTIVE_YEAR').val(aData.ACTIVE_YEAR);
            $('#addModal .modal-title').text('Edit NJOP');
        })

    }

    var activeData = function () {
        $('#table-njop').on('click', 'tr #btn-activate', function () {
            var oTable = $('#table-njop').dataTable();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var param = {
                id: aData.ID
            };

            swal({
                title: 'Warning',
                text: "Apakah anda setuju mengaktifkan data NJOP \"" + aData.KEYWORD + "\"?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TarifPedoman/ActiveDataNJOP",// + aData.BE_ID,
                        timeout: 3000
                    });

                    req.done(function (data) {
                        var table = $('#table-njop').DataTable();
                        App.unblockUI();
                        if (data.Status === 'S') {
                            table.ajax.reload();
                            swal('Berhasil', data.Msg, 'success');
                        } else {
                            swal('Gagal', data.Msg, 'error');
                        }
                    });
                }
            });
        })
        
        $('#table-njop').on('click', 'tr #btn-inactive', function () {
            var oTable = $('#table-njop').dataTable();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $('#INACTIVE_MEMO').val();
            $('#ID_INACTIVE').val(aData.ID);
        });

        $('#btn-inactive-confirm').click(function () {
            var INACTIVE_MEMO = $('#INACTIVE_MEMO').val();
            var ID = $('#ID_INACTIVE').val();
            var param = {
                ID: parseInt(ID),
                INACTIVE_MEMO: INACTIVE_MEMO,
            }
            $('#inactiveModal').modal('hide');
            App.blockUI({ boxed: true });
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/TarifPedoman/InactiveDataNJOP",// + aData.BE_ID,
                timeout: 3000
            });
            req.done(function (data) {
                var table = $('#table-njop').DataTable();
                App.unblockUI();
                if (data.Status === 'S') {
                    table.ajax.reload();
                    swal('Berhasil', data.Msg, 'success');
                } else {
                    swal('Gagal', data.Msg, 'error');
                }
            });
        });
    }

    var initTable = function () {
        var beid = $("#param").val();
        $('#BE_ID option[value="' + beid + '"]').prop('selected', 'selected');
        $('#BE_ID').val(beid);
        if (beid) {
            $('#table-njop').DataTable({
                "ajax":
                {
                    "url": "/TarifPedoman/GetDataNJOP?be_id=" + beid,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "data": "ID",
                        "visible": false
                    },
                    {
                        "data": "BRANCH_ID",
                        "visible": false
                    },
                    {
                        "data": "BE_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KEYWORD",
                        "class": "dt-body-center"
                    },
                    {
                        //"data": "VALUE",
                        "render": function (data, type, full) {
                            if (full.VALUE === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> Rp. ' + full.VALUE + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ACTIVE",
                        "render": function (data) {
                            var aksi = '';
                            if (data) {
                                aksi = 'AKTIF';
                            } else {
                                aksi = 'TIDAK AKTIF';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        //"data": "VALUE",
                        "render": function (data, type, full) {
                            if (full.ACTIVE_YEAR === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> Rp. ' + full.ACTIVE_YEAR + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a id="btn-detail" class="btn btn-icon-only blue" data-toggle="modal" href="#detailModal" title="Edit" ><i class="fa fa-edit"></i></a>';
                            if (full.ACTIVE) {
                                aksi += '<a id="btn-inactive" class="btn btn-icon-only red" data-toggle="modal" href="#inactiveModal" title="Inactive" ><i class="fa fa-times"></i></a>';
                            } else {
                                aksi += '<a id="btn-activate" class="btn btn-icon-only green" title="Activate" ><i class="fa fa-check"></i></a>';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, "All"]
                ],
                "pageLength": 10,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        }


        $('#BE_ID').on('change', function () {

            var beid = $('#BE_ID').val();
            if (beid) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-njop')) {
                    $('#table-njop').DataTable().destroy();
                }

                $('#table-njop').DataTable({
                    "ajax":
                    {
                        "url": "/TarifPedoman/GetDataNJOP?be_id=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ID",
                            "visible": false
                        },
                        {
                            "data": "BRANCH_ID",
                            "visible": false
                        },
                        {
                            "data": "BE_NAME",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "KEYWORD",
                            "class": "dt-body-center"
                        },
                        {
                            //"data": "VALUE",
                            "render": function (data, type, full) {
                                if (full.VALUE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> Rp. ' + full.VALUE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },                        
                        {
                            "data": "ACTIVE",
                            "render": function (data) {
                                var aksi = '';
                                if (data) {
                                    aksi = 'AKTIF';
                                } else {
                                    aksi = 'TIDAK AKTIF';
                                }
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            //"data": "VALUE",
                            "render": function (data, type, full) {
                                if (full.ACTIVE_YEAR === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.ACTIVE_YEAR + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                var aksi = '<a id="btn-edit" class="btn btn-icon-only blue" data-toggle="modal" href="#addModal" title="Edit" ><i class="fa fa-edit"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                                if (full.ACTIVE) {
                                    aksi += '<a id="btn-inactive" class="btn btn-icon-only red" data-toggle="modal" href="#inactiveModal" title="Inactive" ><i class="fa fa-times"></i></a>';
                                } else {
                                    aksi += '<a id="btn-activate" class="btn btn-icon-only green" title="Activate" ><i class="fa fa-check"></i></a>';
                                }
                                return aksi;
                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
                //$('#table-njop').DataTable().column(0).visible(false);
                //$('#table-njop').DataTable().column(1).visible(false);
            }
        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-njop').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["ID"];
            $('#be_name').html(data["BE_NAME"]);

            $("#ID_ATTACHMENT").val(id_attachment);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TarifPedoman/GetDataAttachmentNjop/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                            x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var idPedoman = $('#ID_ATTACHMENT').val();
            console.log(idPedoman);
            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TarifPedoman/UploadFilesNjop',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { idPedoman: idPedoman },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();
            var fileName = data['FILE_NAME'];
            var uriId = data['BRANCH_ID'];
            console.log(data);
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TarifPedoman/DeleteDataAttachmentNjop",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    return {
        init: function () {
            batal();
            simpanData();
            datePicker();
            initTable();
            activeData();
            addEdit();
            initAttachment();
            deleteAttachment();
            fupload();
            $('#table-njop').DataTable().column(0).visible(false);
            $('#table-njop').DataTable().column(1).visible(false);
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TarifPedomanNjop.init();
    });
}

