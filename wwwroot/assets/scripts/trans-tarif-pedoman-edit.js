﻿var AddTarifPedoman = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TarifPedoman";
        });
    }

    var changeBe = function () {
        $('#BE_ID').change(function () {
            var be_id = $('#BE_ID').val();

            $('#PROFIT_CENTER').html('');
            $('#PROFIT_CENTER').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TarifPedoman/GetProfitCenter",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#PROFIT_CENTER').append(temp);
                });
            }

        });
    }

    // Var untuk handle simpan data
    var simpanData = function () {
        $('#btn-update').click(function () {
            var ID = $('#ID').val();
            var BE_ID = $('#BE_ID').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var START_ACTIVE_DATE = $('#START_ACTIVE_DATE').val();
            var END_ACTIVE_DATE = $('#END_ACTIVE_DATE').val();
            var INPUT_TYPE = $('#INPUT_TYPE').val();

            console.log(BE_ID, INPUT_TYPE, START_ACTIVE_DATE, END_ACTIVE_DATE, PROFIT_CENTER);
            if (BE_ID && INPUT_TYPE && START_ACTIVE_DATE && END_ACTIVE_DATE) {

                var tarif = null;
                if (INPUT_TYPE !== 'NJOP') {
                    tarif = new Array();
                }
                var kodes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '09_100', '09_200', '10'];
                kodes.forEach(function (kode) {
                    var id = $('#id' + kode).val();
                    var value = $('#' + kode).val();
                    var percentage = $('#' + kode + 'p').val();
                    var batas = (kode.length > 2 ? parseInt(kode.substring(3)) : 0);
                    var data = {
                        kode: kode.substring(0, 2),
                        batas: batas
                    };

                    if (id) {
                        data.id = parseInt(id)
                    }

                    if (INPUT_TYPE === 'PERCENT') {
                        data.percentage = Number(percentage);
                    }

                    if (INPUT_TYPE === 'TARIF') {
                        data.value = Number(value);
                    }

                    if (INPUT_TYPE !== 'NJOP') {
                        tarif.push(data);
                    }
                });

                var param = {
                    ID: parseInt(ID),
                    BRANCH_ID: parseInt(BE_ID),
                    PROFIT_CENTER_ID: PROFIT_CENTER,
                    TIPE_TARIF: INPUT_TYPE,
                    START_ACTIVE_DATE: START_ACTIVE_DATE,
                    END_ACTIVE_DATE: END_ACTIVE_DATE,
                    tarif: tarif
                }

                console.log(param);

                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TarifPedoman/SaveHeader",
                    timeout: 30000,
                });


                App.blockUI({ boxed: true });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/TarifPedoman';
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location.href = '/TarifPedoman';
                        });
                    }
                });

            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    // untuk menginisiasi form validation
    var initForm = function () {
        
        $("#FORM_PEDOMAN").validate();
        
        $('.input-presentase').inputmask('decimal',{
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0.00',
            max: 100,
            min: 0,
            rightAlign: true,
            removeMaskOnSubmit: true,
            onUnMask: function (maskedValue, unmaskedValue) {
                return Number(maskedValue)
            }
        });

        $('.input-tarif').inputmask('decimal', {
            'placeholder': '0',
            min: 0,
            rightAlign: true,
            removeMaskOnSubmit: true,
            onUnMask: function (maskedValue, unmaskedValue) {
                return Number(maskedValue)
            }
        });

    }


    var initEdit = function () {
        var BE_ID = $('#ID').val();
        if (BE_ID) {
            var req = $.ajax({
                contentType: "application/json",
                method: "GET",
                url: "/TarifPedoman/GetPedoman/" + BE_ID,
                // data: param,
                timeout: 3000,
            });

            req.done(function (data) {
                $('.pedomanid').val('');
                $('#INPUT_TYPE').val(data.TIPE_TARIF);
                
                if (data.TIPE_TARIF === 'PERCENT') {
                    $('.input-value').show();
                    $('.input-presentase').show();
                    $('.input-tarif').hide();
                } else if (data.TIPE_TARIF === 'TARIF') {
                    $('.input-value').show();
                    $('.input-presentase').hide();
                    $('.input-tarif').show();
                } else {
                    $('.input-value').hide();
                }

                if (data.tarif) {
                    data.tarif.forEach(function (t) {
                        var kode_batas = (t.batas != 0 ? t.kode + '_' + t.batas : t.kode);
                        $('#' + kode_batas).val(t.value);
                        $('#id' + kode_batas).val(t.id);
                        if (data.TIPE_TARIF === 'PERCENT') {
                            $('#' + kode_batas + 'p').val(t.percentage);
                        }
                    });
                }
            })
        }
    }

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                //endDate: "Date.now()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });
    }

    return {
        init: function () {
            batal();
            simpanData();
            initForm();
            initEdit();
            datePicker();
            changeBe();
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        AddTarifPedoman.init();
    });
}