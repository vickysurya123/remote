﻿var contractChange = function () {
    var arrCondition = [];

    var arrCondType = [];
    var buah = [];
    var secondCellArray = [];

    var objTampungSR = {};
    var arrTampungSR = [];

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransContractOffer"; 
        });
    }


    // Datatable Rental Object
    var tabler = $('#rental-object-co');
    var oTable = tabler.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });

    // Datatable Manual Frequency
    var tablemf = $('#manual-frequency');
    var mfTable = tablemf.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "order": [[0, "asc"]],
        "searching": false,
        "lengthChange": false,
        "bSort": false,
        "columnDefs": [
        {
            "targets": [8],
            "visible": false
        }
        ]
    });

    // Datatable Memo
    var tablemm = $('#table-memo');
    var mmTable = tablemm.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 20,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false
    });

    var filter = function () {
        $('#btn-filter').click(function () {

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/ContractChange/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PROFIT_CENTER"
                    },
                    {
                        "data": "CONTRACT_START_DATE"
                    },
                    {
                        "data": "CONTRACT_END_DATE"
                    },
                    {
                        "data": "TERM_IN_MONTHS"
                    },
                    {
                        "data": "CONTRACT_NAME"
                    },
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            $('#condition > tbody').html('');
            $('#condition-option > option').html('');
            $('#rental-object-co > tbody').html('');
            
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();

            // Isi Form Contract Data
            var CONTRACT_NO = data['CONTRACT_NO'];
            var CONTRACT_NAME = data['CONTRACT_NAME'];
            var PROFIT_CENTER = data['PROFIT_CENTER'];
            var CONTRACT_START_DATE = data['CONTRACT_START_DATE'];
            var CONTRACT_END_DATE = data['CONTRACT_END_DATE'];
            var TERM_IN_MONTHS = data['TERM_IN_MONTHS'];

            $('#CONTRACT_NO').val(CONTRACT_NO);
            $('#CONTRACT_NAME').val(CONTRACT_NAME);
            $('#PROFIT_CENTER ').val(PROFIT_CENTER);
            $('#CONTRACT_START_DATE ').val(CONTRACT_START_DATE);
            $('#CONTRACT_END_DATE ').val(CONTRACT_END_DATE);
            $('#TIM ').val(TERM_IN_MONTHS);

            // --------------------------------------------------------------------
            // Isi datatable object
            var tableObjectCo = $('#rental-object-co').DataTable();
            tableObjectCo.destroy();

            var tabler = $('#rental-object-co');
            var oTable = tabler.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": false,
                "lengthChange": false,
                "bSort": false
            });

            $.ajax({
                type: "GET",
                url: "/ContractChange/GetDataObject/" + CONTRACT_NO,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    oTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        oTable.fnAddData([jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].CONTRACT_START_DATE, jsonList.data[i].CONTRACT_END_DATE, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY]);
                    }
                }
            });

            // --------------------------------------------------------------------
            // Isi datatable condition
            var tableConditionCo = $('#condition').DataTable();
            tableConditionCo.destroy();

            var tablexc = $('#condition');
            var cTable = tablexc.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "paging": false,
                "pageLength": 100,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": false,
                "lengthChange": false,
                "bSort": false,
                "columnDefs": [
                    {
                        "targets": [19],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [19],
                        "visible": false
                    }
                ]
            });

            var tombol = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            $.ajax({
                type: "GET",
                url: "/ContractChange/GetDataDetailConditionEdit/" + CONTRACT_NO,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    arrCondition = new Array();

                    cTable.fnClearTable();
                    var jsonList = data
                    var listItems = "";
                    var calcObject = "";
                    var res_condition = "";
                    var statistic = "";
                    var maskUnitPrice = "";
                    var maskTotal = "";
                    var maskNetValue = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        if (jsonList.data[i].STATISTIC == 0) {
                            statistic = "No";
                        }
                        else {
                            statistic = "Yes";
                        }

                        maskUnitPrice = jsonList.data[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        maskTotal = jsonList.data[i].TOTAL.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        maskNetValue = jsonList.data[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        //cTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].VALID_FROM, jsonList.data[i].VALID_TO, jsonList.data[i].MONTHS, statistic, maskUnitPrice, jsonList.data[i].AMT_REF, jsonList.data[i].FREQUENCY, jsonList.data[i].START_DUE_DATE, jsonList.data[i].MANUAL_NO, jsonList.data[i].FORMULA, jsonList.data[i].MEASUREMENT_TYPE, jsonList.data[i].LUAS, maskTotal, jsonList.data[i].NJOP_PERCENT, jsonList.data[i].KONDISI_TEKNIS_PERCENT, maskNetValue, tombol, jsonList.data[i].TAX_CODE]);
                        cTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].VALID_FROM, jsonList.data[i].VALID_TO, jsonList.data[i].MONTHS, statistic, maskUnitPrice, jsonList.data[i].AMT_REF, jsonList.data[i].FREQUENCY, jsonList.data[i].START_DUE_DATE, jsonList.data[i].MANUAL_NO, jsonList.data[i].FORMULA, jsonList.data[i].MEASUREMENT_TYPE, jsonList.data[i].LUAS, maskTotal, jsonList.data[i].NJOP_PERCENT, jsonList.data[i].KONDISI_TEKNIS_PERCENT, maskNetValue, tombol, jsonList.data[i].TAX_CODE]);
                        calcObject = jsonList.data[i].CALC_OBJECT;
                        res_condition = jsonList.data[i].CONDITION_TYPE;
                        var param = {
                            CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                            CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                            VALID_FROM: jsonList.data[i].VALID_FROM,
                            VALID_TO: jsonList.data[i].VALID_TO,
                            MONTHS: jsonList.data[i].MONTHS,
                            STATISTIC: jsonList.data[i].STATISTIC,
                            UNIT_PRICE: jsonList.data[i].UNIT_PRICE,
                            AMT_REF: jsonList.data[i].AMT_REF,
                            FREQUENCY: jsonList.data[i].FREQUENCY,
                            START_DUE_DATE: jsonList.data[i].START_DUE_DATE,
                            MANUAL_NO: jsonList.data[i].MANUAL_NO,
                            FORMULA: jsonList.data[i].FORMULA,
                            MEASUREMENT_TYPE: jsonList.data[i].MEASUREMENT_TYPE,
                            LUAS: jsonList.data[i].LUAS,
                            TOTAL: jsonList.data[i].TOTAL,
                            NJOP_PERCENT: jsonList.data[i].NJOP_PERCENT,
                            KONDISI_TEKNIS_PERCENT: jsonList.data[i].KONDISI_TEKNIS_PERCENT,
                            TOTAL_NET_VALUE: jsonList.data[i].TOTAL_NET_VALUE,
                            TAX_CODE: jsonList.data[i].TAX_CODE,
                        }
                        arrCondition.push(JSON.stringify(param));

                        var arrCond = buah.push(calcObject + ' | ' + res_condition);
                        var fruits = buah;
                    }
                }
            });

            $('#btn-add-condition').on('click', function () {
                var CONTRACT_NO = $('#CONTRACT_NO').val();

                if (CONTRACT_NO == '') {
                    $('#condition-option').html('');
                    //swal('Info', 'You have to choose rental request before add a condition', 'info');
                }
                else {
                    $('#condition-option').html('');

                    // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih
                    $.ajax({
                        type: "GET",
                        url: "/ContractChange/GetDataDropDownRO/" + CONTRACT_NO,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {
                            console.log(data.data.length);
                            var jsonList = data
                            var listItems = "";
                            listItems += "<option value='Header Document'>Header Document</option>";
                            for (var i = 0; i < jsonList.data.length; i++) {
                                listItems += "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].OBJECT_ID + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>";
                            }
                            $('#condition-option').html('');

                            $("#condition-option").append(listItems);
                        }
                    });

                    // Ajax untuk ambil dan generate checkbox condition type dari parameter ref d
                    $('#cekbokList').html('');
                    $.ajax({
                        type: "GET",
                        url: "/TransContractOffer/GetCheckboxConditionType",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {
                            var jsonList = data
                            var listItems = "";
                            for (var i = 0; i < jsonList.data.length; i++) {
                                listItems += "<input type='checkbox' + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";

                            }
                            $("#cekbokList").append(listItems);
                        }
                    });
                }
            });

            // --------------------------------------------------------------------
            // Isi datatable manual-frequency
            // Edit Data Manual Frequency
            var tableManualFrequencyCo = $('#manual-frequency').DataTable();
            tableManualFrequencyCo.destroy();

            var tablemf = $('#manual-frequency');
            var mfTable = tablemf.dataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 20,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "order": [[0, "asc"]],
                "searching": false,
                "lengthChange": false,
                "bSort": false,
                "columnDefs": [
                {
                    "targets": [8],
                    "visible": false
                }
                ]
            });
           
            var arrManualFrequency = [];
            var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
            var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            // Ajax untuk menampilkan data manual frequency sebelumnya
            $.ajax({
                type: "GET",
                url: "/ContractChange/GetDataDetailManualFrequencyEdit/" + CONTRACT_NO,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    arrManualFrequency = new Array();

                    mfTable.fnClearTable();
                    var jsonList = data;
                    var res_condition = "";
                    var maskNetValueFrequency = "";
                    var maskQuantityFrequency = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        maskNetValueFrequency = jsonList.data[i].NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                        if (jsonList.data[i].QUANTITY != null)
                        {
                            maskQuantityFrequency = jsonList.data[i].QUANTITY.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }

                        mfTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct, jsonList.data[i].CONDITION]);

                        var param = {
                            MANUAL_NO: jsonList.data[i].MANUAL_NO,
                            CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                            DUE_DATE: jsonList.data[i].DUE_DATE,
                            NET_VALUE: maskNetValueFrequency,
                            QUANTITY: maskQuantityFrequency,
                            UNIT: jsonList.data[i].UNIT,
                            CONDITION: jsonList.data[i].CONDITION
                        }
                        arrManualFrequency.push(JSON.stringify(param));
                    }
                }
            });
           

            // --------------------------------------------------------------------
            // Isi datatable table-memo
            // Edit Data Memo
            
            var tableMemoCo = $('#table-memo').DataTable();
            tableMemoCo.destroy();

                var tablemm = $('#table-memo');
                var mmTable = tablemm.dataTable({
                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"]
                    ],
                    "pageLength": 20,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false
                });
                //mmTable.fnAddData(['test', 'test']);
                var arrMemo = [];

                var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
                var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                // Ajax untuk menampilkan data manual frequency sebelumnya
                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataDetailMemoEdit/" + CONTRACT_NO,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrMemo = new Array();
                        mmTable.fnClearTable();
                        var jsonList = data;
                        var res_condition = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            mmTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].MEMO, tombolAct]);

                            var param = {
                                CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                MEMO: jsonList.data[i].MEMO
                            }
                            arrMemo.push(JSON.stringify(param));
                        }
                        //console.log(arrMemo);
                    }
                });

            $('#addDetailModal').modal('hide');
        });
    }

    var addCondition = function () {
        
    }

    var masukkanCondition = function () {
        $('#btn-condition').on('click', function () {
            //raw data value calc object dan di split |
            var id_combo = $('#condition-option').val();
            var splitCalcObject = id_combo.split("|");
            var calcObject = splitCalcObject[0];
            var id_ro = splitCalcObject[1];
            console.log(id_combo);

            if (id_ro) {
                // Ajax untuk get meas type berdasarkan id_ro yang didapatkan dari split
                $('#meas-type').html('');
                // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih
                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataMeasType/" + id_ro,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        listItems += '<option value="">--Choose--</option>';
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + jsonList.data[i].MEAS_TYPE + '|' + jsonList.data[i].ID_DETAIL + "'>" + jsonList.data[i].MEAS_TYPE + ' - ' + jsonList.data[i].MEAS_DESC + "</option>";
                        }
                        // $("#meas-type").append(listItems);
                        $('select[name="meas-type"]').html(listItems);
                    }
                });
            }

            // Matikan form untuk kondisi Header Document
            if (id_ro) {
                var measType = '<select class="form-control" id="meas-type" name="meas-type" onchange="getLuas(this)"></select>';
            }
            else {
                measType = '';
            }
            if (id_ro) {
                var luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }
            else {
                luas = '';
            }

            //Cek selected value dari combobox calculation apakah header document atau RO
            var vCBCalculation = $("#condition-option").val();

            if (vCBCalculation == 'Header Document') {
                measType = '<input type="text" class="form-control" id="meas-type" name="meas-type" readonly>';
                luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }

            var tableDT = $('#condition  > tbody');
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();
            var tim = $('#TIM').val();

            var startD = '<center><input type="text" class="form-control" name="start-date" value="' + start + '" id="start-date"></center>';
            var endD = '<center><input type="text" class="form-control" name="end-date" value="' + end + '" id="end-date" onchange="hitungTermMonths(this)"></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onchange="hitungEndDate(this)"></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center><input type="text" class="form-control" id="unit-price" name="unit-price" onchange="isiTotalUnitPrice(this)"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control" name="start-due-date" id="start-due-date"></center>';
            var manualNo = '<center><input type="text" class="form-control" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';
            //var measType = '<select class="form-control" id="meas-type"></select>';
            //var luas = '<input type="text" class="form-control" id="luas">';
            var total = '<center><input type="text" class="form-control" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';
            //var salesRule = '<center><input type="text" class="form-control" id="sales-rule"></center>';
            var totalNetValue = '<center><input type="text" class="form-control" id="total-net-value" name="total-net-value" readonly></center>';


            $('input[name="test"]:checked').each(function () {
                //raw data condition dan di split |
                var raw_cond = $(this).val();
                var split_cond = raw_cond.split("|");
                var res_condition = split_cond[0];
                var tax_code = split_cond[1];

                cTable.fnAddData([calcObject,
                                  res_condition,
                                  startD,
                                  endD,
                                  termMonths,
                                  cekbokStatistic,
                                  unitPrice,
                                  amtRef,
                                  frequency,
                                  startDueDate,
                                  manualNo,
                                  formula,
                                  measType,
                                  luas,
                                  total,
                                  persenFromNJOP,
                                  persenFromKondisiTeknis,
                                  totalNetValue,
                                  '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                                  tax_code], true);
            });

            $('#addCondition').modal('hide');
        });
    }

    return {
        init: function () {
            batal();
            filter();
            addCondition();
            masukkanCondition();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        contractChange.init();
    });
}

jQuery(document).ready(function () {
    contractChange.init();
    var tablexo = $('#rental-object-co');
    var oTable = tablexo.dataTable();

    var tablexc = $('#condition');
    var cTable = tablexc.dataTable();

    var tablexmf = $('#manual-frequency');
    var mfTable = tablexmf.dataTable();

    var tablexmm = $('#table-memo');
    var mmTable = tablexmm.dataTable();

    $('#condition > tbody').html('');
    $('#condition-option > option').html('');
    $('#manual-frequency > tbody').html('');
    $('#table-memo > tbody').html('');
    $('#rental-object-co > tbody').html('');
    $('#sales-based > tbody').html('');
    $('#reporting-rule > tbody').html('');
    $('.sembunyi').hide();
});