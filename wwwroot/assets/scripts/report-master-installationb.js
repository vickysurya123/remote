﻿var ReportMasterInstallationB = function () {


    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var CBProfitCenter = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportMasterInstallationB/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                handleBootstrapSelect();
            }
        });
    }

    //var select2InstallationType = function () {
    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";
    //
    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.INSTALLATION_TYPE + "</div>" +
    //                        "<div class='select2-result-repository__description'>" + repo.INSTALLATION_CODE + "</div>" +
    //                      "</div>";
    //
    //        return markup;
    //    }
    //    function formatRepoSelection(repo) {
    //        $("#INSTALLATION_CODE").val(repo.INSTALLATION_CODE);
    //        return repo.INSTALLATION_TYPE;
    //    }
    //
    //    $("#INSTALLATION_TYPE").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportMasterInstallation/getInstallationType",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2InstallationCode = function () {
    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";
    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.INSTALLATION_CODE + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.INSTALLATION_CODE + "</div>" +
    //                      "</div>";
    //        return markup;
    //    }
    //
    //    function formatRepoSelection(repo) {
    //        $("#PROFIT_CENTER_ID").val(repo.PROFIT_CENTER_ID);
    //        return repo.TERMINAL_NAME;
    //    }
    //
    //    $("#INSTALLATION_CODE").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportMasterInstallation/getInstallationCode",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2CostumerMDM = function () {
    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";
    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.REF_DESC + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.REF_DATA + "</div>" +
    //                      "</div>";
    //        return markup;
    //    }
    //    function formatRepoSelection(repo) {
    //        $("#SERVICE_GROUP_ID").val(repo.REF_DATA);
    //        return repo.REF_DESC;
    //    }
    //    $("#COSTUMER_MDM").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportMasterInstallation/getCostumerMDM",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2CostumerSAP = function () {
    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";
    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.REF_DESC + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.REF_DATA + "</div>" +
    //                      "</div>";
    //        return markup;
    //    }
    //    function formatRepoSelection(repo) {
    //        $("#SERVICE_GROUP_ID").val(repo.REF_DATA);
    //        return repo.REF_DESC;
    //    }
    //    $("#COSTUMER_SAP").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportMasterInstallation/getCostumerSAP",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            App.blockUI({ boxed: true });
            var req = null;
            //var txtTerminal = $('#txtTerminal').val();
            var txtKegiatan = $('#txtKegiatan').val();
            var txtPBMNama = $('#select2-txtPBM-container').text();
            var txtPBM = $('#txtPBM').val();
            var txtAsalPortNama = $('#txtAsalKapalNama').val();
            var txtAsalPort = $('#txtAsalKapal').val();
            var txtTujuanPortNama = $('#txtTujuanKapalNama').val();
            var txtTujuanPort = $('#txtTujuanKapal').val();
            var txtTanggal = $('#txtTanggal').val();
            var txtAwalKapalNama = $('#select2-txtAwalKapal-container').text();
            var txtAwalKapal = $('#txtAwalKapal').val();
            var txtTransKapalNama = $('#select2-txtTransKapal-container').text();
            var txtTransKapal = $('#txtTransKapal').val();
            var xTable = $('#table-permohonan').dataTable();
            var xGetData = xTable.fnGetData();
            var xDATA_PMH = new Array(xGetData.length);
            for (i = 0; i < xGetData.length; i++) {
                var xData = xTable.fnGetData(i);

                xDATA_PMH[i] = {
                    XPPKB1_NOMOR: txtAwalKapal,
                    XKD_PBM: txtPBM,
                    XTGL_PMH_WHM: txtTanggal,
                    XKD_PELANGGAN: null,
                    XTIPE_KEGIATAN: txtKegiatan,
                    XKETERANGAN: xData[8],
                    XPORT_ORIGIN: txtAsalPort,
                    XPORT_DESTINATION: txtTujuanPort,
                    XKD_KAPAL_FROM: txtAwalKapal,
                    XKD_KAPAL_TO: txtTransKapal,
                    XKD_DOKUMEN: null,
                    XTIPE_PENUMPUKAN: xData[0],
                    XGUDLAP_ID: xData[1],
                    XKD_BARANG: xData[2],
                    XKD_SATUAN: xData[3],
                    XSIFAT_BARANG: null,
                    XJENIS_KEMASAN: xData[4],
                    XJUMLAH_BARANG: xData[5],
                    XNAMA_KAPAL_FROM: txtAwalKapalNama,
                    XNAMA_KAPAL_TO: txtTransKapalNama,
                    XNAMA_PBM: txtPBMNama,
                    XNAMA_PORT_ORIGIN: txtAsalPortNama,
                    XNAMA_PORT_DESTINATION: txtTujuanPortNama,
                    XKD_ASAL: xData[6],
                    XKD_TUJUAN: xData[7],

                }
            }
            //console.log(xDATA_PMH);
            //console.log(txtAwalKapalNama);
            req = $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(xDATA_PMH),
                method: "POST",
                url: "/PermohonanMasuk/InsertPmhBongkar",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                console.log(data);
                if (data.sts === "S") {
                    clear();
                    swal('Berhasil', "Berhasil Insert Data Permohonan [" + data.msg + "]", 'success');
                } else {
                    swal('Gagal', data.msg, 'error');
                }
            });
        });
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-instalation');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {
            var businessEntity = $('#BE_ID').val();
            var profitCenter = $('#PROFIT_CENTER').val();
            var serviceGroup = $('#SERVICE_GROUP').val();
            var status = $('#STATUS').val();

            if ($.fn.DataTable.isDataTable('#table-report-instalation')) {
                $('#table-report-instalation').DataTable().destroy();
            }

            $('#table-report-instalation').DataTable({
                "ajax": {
                    "url": "/ReportMasterInstallationB/ShowAll",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "nama_profit_center",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_SAP_AR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STATUS === '1') {
                                return 'ACTIVE';
                            } else {
                                return 'INACTIVE';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINIMUM_USED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_CODE",
                        "class": "dt-body-center"
                    }


                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var showFilter = function () {
        $('#btn-filter').click(function () {

            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            if (CUSTOMER_NAME.length <= 0) {
                $('#CUSTOMER_ID').val('');
            }


            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-instalation')) {
                $('#table-report-instalation').DataTable().destroy();
            }
            $('#table-report-instalation').DataTable({
                "ajax": {
                    "url": "/ReportMasterInstallationB/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.installation_code = $('#INSTALLATION_CODE').val();
                        data.customer_mdm = $('#CUSTOMER_ID').val();
                        data.customer_sap = $('#CUSTOMER_SAP').val();
                        data.status = $('#STATUS').val();
                        console.log(data);
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "nama_profit_center",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_SAP_AR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLATION_DATE === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLATION_DATE + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.STATUS === '1') {
                                return 'ACTIVE';
                            } else {
                                return 'INACTIVE';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.MINIMUM_USED === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.MINIMUM_USED + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_CODE",
                        "class": "dt-body-center"
                    }


                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportMasterInstallationB/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    return {
        init: function () {
            //select2InstallationType();
            //select2InstallationCode();
            //select2CostumerMDM();
            //select2CostumerSAP();
            dataTableResult();
            showAll();
            showFilter();
            simpan();
            reset();
            CBProfitCenter();
            mdm();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var param = {
        PROFIT_CENTER: $('#PROFIT_CENTER').val() == undefined ? null : $('#PROFIT_CENTER').val(),
        INSTALLATION_CODE: $('#INSTALLATION_CODE').val(),
        CUSTOMER_ID: $('#CUSTOMER_ID').val(),
        CUSTOMER_SAP: $('#CUSTOMER_SAP').val(),
        STATUS: $('#STATUS').val()
    };
    
    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "ReportMasterInstallationB/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportMasterInstallationB/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportMasterInstallationB.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-instalation > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}