﻿var ExpNotif = function () {
    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/ExpNotification/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "DAYS",
                    "class": "dt-body-center",
                    render: function (data, type, row) {
                        return data + ' Days';
                    }
                },
                {
                    "data": "START_DATE"
                },
                {
                    "data": "END_DATE"
                },
                {
                    "data": "STATUS",
                    "class": "dt-body-center",
                    render: function (data, type, row) {
                        var a = data == 1 ? '<span class="label label-sm label-info">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>';
                        return a;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var a = full.STATUS == 1 ? '<a id="btn-delete" class="btn btn-icon-only red" title="Inactive" ><i class="fa fa-arrow-down"></i></a>' : '<a id="btn-delete" class="btn btn-icon-only blue" title="Active" ><i class="fa fa-arrow-up"></i></a>';
                        return a;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var DAYS = $('#DAYS').val();
            var START_DATE = $('#START_DATE').val();
            var END_DATE = $('#END_DATE').val();
            if (DAYS && START_DATE && END_DATE) {
                var param = {
                    DAYS: parseInt(DAYS),
                    START_DATE: START_DATE,
                    END_DATE: END_DATE
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ExpNotification/Insert",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }
    var clear = function () {
        $('input').val('');
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        // console.log('asdasdasd');
        $('#txt-judul').text('Edit');
        $('#addapvsetModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        var param = {
            BE_ID: aData.BE_ID
        };
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/ApprovalSetting/SearchBranch",
            timeout: 30000
        });
        req.done(function (data) {
            $('#BE_NAME').val(data.Msg);
            $('#id_apv_header').val(aData.ID);
        });



    });
    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: aData.ID,
            STATUS: aData.STATUS,
        };
        swal({
            title: 'Warning',
            text: "Are you sure want to Inactive this Setting?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ExpNotification/Status",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('body').on('click', 'tr #btn-addparam', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-apvset').DataTable();
        var data = table.row(baris).data();

        var ID = btoa(data["ID"]);
        window.location = "/ApprovalSetting/AddParam?mimoto=" + ID;
    });
    $('#btn-bataladd').click(function () {
        clear();
    });
    $('#btn-add').click(function () {
        $('#txt-judul').text('Add');
    });
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            datePicker();
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ExpNotif.init();
    });
}