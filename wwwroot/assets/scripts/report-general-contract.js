﻿var ReportGeneralContract = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportGeneralContract/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBContractType();
                console.log("list BE : " + listItems);
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var CBContractType = function () {
        $.ajax({
            type: "GET",
            url: "/ReportGeneralContract/GetDataContractTypeTwo",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#CONTRACT_TYPE").html('');
                $("#CONTRACT_TYPE").append(listItems);
                handleBootstrapSelect();
            }
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-trans-os');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }
    
    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                $('#table-report-trans-os').DataTable().destroy();
            }

            $('#table-report-trans-os').DataTable({

                "ajax": {
                    "url": "ReportGeneralContract/ShowFilterNew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.customer_id = $('#CUSTOMER_ID').val();
                        data.contract_type = $('#CONTRACT_TYPE').val();
                        data.contract_no = $('#CONTRACT_NO').val();
                        data.contract_offer_no = $('#CONTRACT_OFFER_NO').val();
                        data.rental_request_no = $('#RENTAL_REQUEST_NO').val();
                        data.contract_status = $('#CONTRACT_STATUS').val();
                        data.valid_from = $('#VALID_FROM').val();
                        data.valid_to = $('#VALID_TO').val();
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_TYPE_DESC"
                    },
                    {
                        "data": "RENTAL_REQUEST_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_OFFER_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_BY"
                    },
                    {
                        "data": "CONTRACT_NAME"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BUSINESS_PARTNER_NAME"
                    },
                    {
                        "data": "CUSTOMER_AR"
                    },
                    {
                        "data": "PROFIT_CENTER_NAME"
                    },
                    {
                        "data": "CONTRACT_USAGE"
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "IJIN_PRINSIP_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME"
                    },
                    {
                        "data": "INDUSTRY"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CONDITION_TYPE"
                    },
                    {
                        "data": "CONTRACT_STATUS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_MIGRASI",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],
                "pageLength": 10,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "filter": false
            });
        });
    }

    var mdm = function () {
        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportGeneralContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    return {
        init: function () {
            CBBusinessEntity();
            select2ProfitCenter();
            //CBContractType();
            dataTableResult();
            handleDatePickers();
            filter();
            mdm();
        }
    };
}();

function CBProfitCenter(BE_ID) {
    console.log(BE_ID);
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportGeneralContract/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list : " + listItems);

            }
        });
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportGeneralContract/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list d : " + listItems);

            }
        });
    }
}

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
    var CONTRACT_TYPE = $('#CONTRACT_TYPE').val();
    var CONTRACT_NO = $('#CONTRACT_NO').val();
    var CONTRACT_OFFER_NO = $('#CONTRACT_OFFER_NO').val();
    var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
    var CONTRACT_STATUS = $('#CONTRACT_STATUS').val();
    var VALID_FROM = $('#VALID_FROM').val();
    var VALID_TO = $('#VALID_TO').val();

    var param = {
        BUSINESS_ENTITY,
        PROFIT_CENTER,
        CUSTOMER_ID,
        CONTRACT_TYPE,
        CONTRACT_NO,
        CONTRACT_OFFER_NO,
        RENTAL_REQUEST_NO,
        CONTRACT_STATUS,
        VALID_FROM,
        VALID_TO
    }
    var dataJSON = "{BUSINESS_ENTITY:" + JSON.stringify(BUSINESS_ENTITY) + ",PROFIT_CENTER:" + JSON.stringify(PROFIT_CENTER) +
                   ",CUSTOMER_ID:" + JSON.stringify(CUSTOMER_ID) + ",CONTRACT_TYPE:" + JSON.stringify(CONTRACT_TYPE) +
                   ",CONTRACT_NO:" + JSON.stringify(CONTRACT_NO) + ",CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) +
                   ",RENTAL_REQUEST_NO:" + JSON.stringify(RENTAL_REQUEST_NO) + ",CONTRACT_STATUS:" + JSON.stringify(CONTRACT_STATUS) +
                   ",VALID_FROM:" + JSON.stringify(VALID_FROM) + ",VALID_TO:" + JSON.stringify(VALID_TO) + "}";

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportGeneralContract/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportGeneralContract/deleteExcel",
        data: JSON.stringify(fileExcels),//jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportGeneralContract.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-trans-os > tbody').html('');
    $('#table-report-trans-os').DataTable();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportGeneralContract/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}