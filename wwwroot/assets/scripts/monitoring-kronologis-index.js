﻿var DisposisiListIndex = function () {
    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/MonitoringKronologis/Add';
        });
    }

    var cancel = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/MonitoringKronologis';
        });
    }

    var edit = function () {
        $('body').on('click', 'tr #btn-edit', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-monitorings').DataTable();
            var data = table.row(baris).data();
            window.location.href = '/MonitoringKronologis/Edit/' + data["ID"];
        });
    }

    function defineExcel_7() {
        $('body').on('click', 'tr #btn-export', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-monitorings').DataTable();
            var data = table.row(baris).data();
            //console.log(data);

            var TYPE = data["ID"];
            //console.log(TYPE);
            var JUDUL = data["JUDUL"];
            var CABANG = data["BE_NAME"];
            var REGIONAL = data["REGIONAL"];
            var TANGGAL = data["TANGGAL"];
            //console.log(JUDUL);
            /* var BUSINESS_ENTITY = $('#BE_NAME').val();
             var END_DATE = $('#AVL_DATE').val();*/

            var dataJSON = {
                ID: TYPE.toString(),
                JUDUL: JUDUL,
                CABANG: CABANG,
                REGIONAL: REGIONAL,
                TANGGAL: TANGGAL,
            };
            console.log(dataJSON);
            $.ajax({
                type: "POST",
                url: "/MonitoringKronologis/ExportExcel",
                data: JSON.stringify(dataJSON),
                contentType: "application/json; charset-utf8",
                datatype: "json",
                async: "true",
                success: function (data) {
                    fileExcels = data.data;
                    App.unblockUI();
                    swal({
                        title: "File Successfully Generated",
                        text: "[" + data.data + "]" +
                            '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                            ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                        type: "success",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                error: function (data) {
                }
            });
        });
    }


    var count = 0;
    var addDataMmonitoring = function () {
        //table row
        var tablem = $('#table-data-monitoring');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        var nEditing = null;
        var nNew = false;
        
        $('#btn-add-data').on('click', function () {
            var CABANG = $('#CABANG2').val();
            var TANGGAL = $('#TANGGAL').val();
            var REGIONAL = $('#REGIONAL2').val();
            var JUDUL = $('#JUDUL').val();


            if (CABANG && REGIONAL && JUDUL != "") {
                var baris = $(this).parents('tr')[0];
                var table = $('#table-data-monitoring').DataTable();
                var xtable = $('#table-data-monitoring').dataTable();
                var data = table.row(baris).data();
                var g = xtable.fnGetData();
                var l = g.length;
                var d = xtable.fnGetData(g - 1);
                var last = $("#table-data-monitoring").find("tr:last td:first").text();
                //console.log('last ' + last);
                //Untuk Button Add Atas
                var oke = 0;
                if (l != 0) {
                    nomor = (Number(last) + 1);
                }

                //Form input di data table table-row
                var ID = '<input type="hidden" id="param_idd" />'
                var NO_SURAT = '<input type="text" class="form-control" id="NO_SURAT" name="detailKronologis[__REINDEX__].NO_SURAT">';
                var TANGGAL_SURAT = '<input type="text" class="form-control date-picker" id="TANGGAL_SURAT" name="detailKronologis[__REINDEX__].TANGGAL_SURAT" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
                var KETERANGAN = '<textarea id="KETERANGAN" name="detailKronologis[__REINDEX__].KETERANGAN" class="form-control"></textarea>';
                // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
                var TINDAK_LANJUT = '<textarea class="form-control" id="TINDAK_LANJUT" name="detailKronologis[__REINDEX__].TINDAK_LANJUT"></textarea>';
                var KENDALA = '<textarea class="form-control" id="KENDALA" name="detailKronologis[__REINDEX__].KENDALA"></textarea>';
                var PROGRESS = '<textarea class="form-control" id="PROGRESS" name="detailKronologis[__REINDEX__].PROGRESS"></textarea>';
                var FILE_NAME = '<input type="file" accept=".pdf, .doc, .docx" class="form-control" id="FILE_NAME" name="detailKronologis[__REINDEX__].FILE">';

                oTable.fnAddData([
                    //ID,
                    NO_SURAT,
                    TANGGAL_SURAT,
                    KETERANGAN,
                    TINDAK_LANJUT,
                    KENDALA,
                    PROGRESS,
                    FILE_NAME,
                    '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>'
                ], true);
                /*$('.SERVICE_GROUP').select2();*/

                //set datepicker
                resetDatePicker();
                //Masking Due Date
                $('input[name="tanggal-surat"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });
            } else {
                swal('Info', 'Lengkapi Data Terlebih Dahulu', 'info');
            }
        });

        $('#btn-add-data-edit').on('click', function () {
                var baris = $(this).parents('tr')[0];
                var table = $('#table-detail').DataTable();
                var xtable = $('#table-detail').dataTable();
                var data = table.row(baris).data();
                var g = xtable.fnGetData();
                var l = g.length;
                var d = xtable.fnGetData(g - 1);
                var last = $("#table-detail").find("tr:last td:first").text();
                //console.log('last ' + last);
                //Untuk Button Add Atas
                var oke = 0;
                if (l != 0) {
                    nomor = (Number(last) + 1);
                }

                //Form input di data table table-row
                //var ID = '<input type="hidden" id="param_id" name="no-surat">'
                var NO_SURAT = '<input type="text" class="form-control" id="NO_SURAT" name="NO_SURAT[]">';
                var TANGGAL_SURAT = '<input type="text" class="form-control date-picker" id="TANGGAL_SURAT" name="TANGGAL_SURAT[]" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY" />';
                var KETERANGAN = '<textarea id="KETERANGAN" name="KETERANGAN[]" class="form-control"></textarea>';
                // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
                var TINDAK_LANJUT = '<textarea class="form-control" id="TINDAK_LANJUT" name="TINDAK_LANJUT[]"></textarea>';
                var KENDALA = '<textarea class="form-control" id="KENDALA" name="KENDALA[]"></textarea>';
                var PROGRESS = '<textarea class="form-control" id="PROGRESS" name="PROGRESS[]"></textarea>';
            var FILE_NAME = '<input type="file" accept=".pdf, .doc, .docx" class="form-control" id="files" name="files">';

                oTable.fnAddData([
                    //ID,
                    NO_SURAT,
                    TANGGAL_SURAT,
                    KETERANGAN,
                    TINDAK_LANJUT,
                    KENDALA,
                    PROGRESS,
                    FILE_NAME,
                    '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>'
                ], true);
                /*$('.SERVICE_GROUP').select2();*/

                //set datepicker
                resetDatePicker();
                //Masking Due Date
                $('input[name="tanggal-surat"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });
        });

        var arrMmonitorings = []
        $('#table-data-monitoring').on('click', 'tr .btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-data-monitoring').DataTable();
            var data = table.row(baris).data();

            var a = $('#NO_SURAT').val();
            var b = $('#TANGGAL_SURAT').val();
            var c = $('#KETERANGAN').val();
            var d = $('#TINDAK_LANJUT').val();
            var e = $('#KENDALA').val();
            var f = $('#PROGRESS').val();
            var g = $('#FILE_NAME').val();

            arrMmonitorings.push({
                'NO_SURAT': a,
                'TANGGAL_SURAT': b,
                'KENDALA': e,
                'TINDAK_LANJUT': d,
                'PROGRESS': f,
                'KETERANGAN': c,
                'FILE_NAME': g,
            });
            //console.log(arrMmonitorings);

            var tombol = '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            if (b == '') {
                swal('Info', 'Silahkan isi Data dengan Lengkap', 'info')
            } else {

                $('#table-data-monitoring').dataTable().fnUpdate(a, baris, 0, false);
                oTable.fnUpdate(b, baris, 1, false);
                oTable.fnUpdate(c, baris, 2, false);
                oTable.fnUpdate(d, baris, 3, false);
                oTable.fnUpdate(e, baris, 4, false);
                oTable.fnUpdate(f, baris, 5, false);
                //oTable.fnUpdate(g, baris, 6, false);

                var tombolC = '<center>';//<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                if ($('#btn-cancelcus').length) {
                    tombolC += '<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a>';
                }
                if ($('#btn-savecus-frequency').length) {
                    tombolC += '<a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a>';
                }
                tombolC += '</center>';

                oTable.fnUpdate(tombolC, baris, 7, false);
            }

        });

        var arrDetail = []
        $('#table-detail').on('click', 'tr .btn-save-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();

            var a = $('#NO_SURAT').val();
            var b = $('#TANGGAL_SURAT').val();
            var c = $('#KETERANGAN').val();
            var d = $('#TINDAK_LANJUT').val();
            var e = $('#KENDALA').val();
            var f = $('#PROGRESS').val();
            var g = $('#FILE_NAME').val();

            arrDetail.push({
                'NO_SURAT': a,
                'TANGGAL_SURAT': b,
                'KENDALA': e,
                'TINDAK_LANJUT': d,
                'PROGRESS': f,
                'KETERANGAN': c,
                'FILE_NAME': g,
            });
            //console.log(arrMmonitorings);

            var tombol = '<center><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            if (b == '') {
                swal('Info', 'Silahkan isi Data dengan Lengkap', 'info')
            } else {

                $('#table-data-monitoring').dataTable().fnUpdate(a, baris, 0, false);
                oTable.fnUpdate(b, baris, 1, false);
                oTable.fnUpdate(c, baris, 2, false);
                oTable.fnUpdate(d, baris, 3, false);
                oTable.fnUpdate(e, baris, 4, false);
                oTable.fnUpdate(f, baris, 5, false);
                //oTable.fnUpdate(g, baris, 6, false);

                var tombolC = '<center>';//<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                if ($('#btn-cancelcus').length) {
                    tombolC += '<a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a>';
                }
                if ($('#btn-savecus-frequency').length) {
                    tombolC += '<a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a>';
                }
                tombolC += '</center>';

                oTable.fnUpdate(tombolC, baris, 7, false);
            }

        });


        $('#table-data-monitoring').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-data-monitoring').DataTable();
            var data = table.row(baris).data();
            delRow($(baris));
            console.log(data);
            var service_Group = data[1].split(" - ")
            if (service_Group[0] != null) {
                a = "<tr><td>" + service_Group[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-service-detail tbody tr').each(function () {
                if (oTable.fnGetData().length > 0) {
                    $(this).find('td').eq(0).html(index);
                    index++;
                }
                else {
                    $(this).find('td').eq(0).html("No data available in table");
                }
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                indexparam++;
            });
        }
    }

    var resetDatePicker = function () {
        //console.log("reset")

        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });

        $('.date-picker').each(function () {
            var date = $(this).val();
            if (date) {
                var datearray = date.split("/");
                var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                }).datepicker('setDate', new Date(newdate)); //set value
            } else {
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                });
            }
        });
    }

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var initTable = function () {
        $('#table-monitorings').DataTable({
            "ajax":
            {
                "url": "/MonitoringKronologis/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "BE_NAME"
                },
                {
                    "data": "JUDUL",
                    "class": "dt-body-left",
                },
                {
                    "data": "TANGGAL",
                    "class": "dt-body-center",
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '';

                        if (!full.IS_APPROVE) {
                            aksi += '<a id="btn-edit" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>';
                        }
                        
                        aksi += '<a id="btn-view" class="btn btn-icon-only purple" title="View" ><i class="fa fa-eye"></i></a>';

                        if (!full.IS_APPROVE) {
                            aksi += '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        }

                        aksi += '<a id="btn-download" class="btn btn-icon-only green" title="Download" ><i class="fa fa-cloud-download"></i></a>';

                        //if (!full.IS_APPROVE) {
                        //    aksi += '<a id="btn-approve" class="btn btn-icon-only btn-default" title="Approve" data-id="'+full.ID+'"><i class="fa fa-check"></i></a>';
                        //}

                        if (!full.IS_APPROVE) {
                            aksi += '<a id="btn-export" class="btn btn-icon-only btn-default" title="Export"><i class="fa fa-file-excel-o""></i></a>';
                        }

                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

        $('#BE_ID').on('change', function () {

            var beid = $('#BE_ID').val();
            if (beid) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-monitorings')) {
                    $('#table-monitorings').DataTable().destroy();
                }

                $('#table-monitorings').DataTable({
                    "ajax":
                    {
                        "url": "/MonitoringKronologis/GetDataMonitoring?be_id=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ID",
                            "visible": false
                        },
                        {
                            "data": "BE_NAME",
                        },
                        {
                            "data": "JUDUL",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TANGGAL",
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                var aksi = '<a id="btn-edit" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                                    '<a id="btn-view" class="btn btn-icon-only purple" title="View" ><i class="fa fa-eye"></i></a>' +
                                    '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>' +
                                    '<a id="btn-download" class="btn btn-icon-only green" title="Download" ><i class="fa fa-cloud-download"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
                //$('#table-njop').DataTable().column(0).visible(false);
                //$('#table-njop').DataTable().column(1).visible(false);
            }
        });
    }

    var initTablDetailMenonitoring = function () {
        var table = $('#table-detail');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 1000,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "order": [[0, "asc"]],
            "searchable": false,
            "lengthChange": false,
            "bSort": false,
            "columnDefs": [
                {
                    "visible": false
                }
            ]
        });
        var DetailData = [];
        var Id = $('#ID').val();
        var tombolAct = '<center><a class="btn default btn-xs green btn-editdata" name="btn-editdata" id="btn-editdata"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-detail"><i class="fa fa-trash"></i></a></center>';
        /*$('#table-detail').DataTable({*/
        //$.ajax({
        //    "url": "/MonitoringKronologis/GetDataDetailEdit/" + Id,
        //    "type": "GET",
        //    contentType: "application/json",
        //    dataType: "json",
        //    success: function (data) {
        //        DetailData = new Array();

        //        oTable.fnClearTable();
        //        var jsonList = data;
        //        //var res_condition = "";
        //        /*var maskNetValueDetail = "";
        //        var maskQuantityDetail = "";*/
        //        for (var i = 0; i < jsonList.data.length; i++) {
        //            /*maskNetValueDetail = jsonList.data[i].NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //            maskQuantityDetail = jsonList.data[i].QUANTITY.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/

        //            oTable.fnAddData([jsonList.data[i].NO_SURAT, jsonList.data[i].TANGGAL_SURAT, jsonList.data[i].KETERANGAN,
        //                jsonList.data[i].TINDAK_LANJUT, jsonList.data[i].KENDALA, jsonList.data[i].PROGRESS, jsonList.data[i].DIRECTORY, tombolAct]);
        //            //console.log(oTable.fnAddData);
        //            var param = {
        //                NO_SURAT: jsonList.data[i].NO_SURAT,
        //                TANGGAL_SURAT: jsonList.data[i].TANGGAL_SURAT,
        //                KETERANGAN: jsonList.data[i].KETERANGAN,
        //                TINDAK_LANJUT: jsonList.data[i].TINDAK_LANJUT,
        //                KENDALA: jsonList.data[i].KENDALA,
        //                PROGRESS: jsonList.data[i].PROGRESS,
        //                FILE_NAME: jsonList.data[i].DIRECTORY
        //            }
        //            DetailData.push(JSON.stringify(param));
        //            //console.log(DetailData);
        //        }
        //    }
        //});

        /*});*/

        $('#table-detail').on('click', 'tr #btn-editdata', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();
            //console.log(data);

            var no_surat = data[0];
            var tanggal_surat = data[1];
            var keterangan = data[2];
            var kendala = data[3];
            var tindak_lanjut = data[4];
            var progress = data[5];
            var file_name = data[6];

            /*nosuratCancel = no_surat;
            keteranganCancel = keterangan;
            dueDateCancel = tanggal_surat;*/

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a>';

            // Open Data Table
            var openTable = $('#table-detail').dataTable();
            //Form input di data table manual-frequency
           /* var no = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';*/
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var NOMOR_SURAT = '<input type="text" class="form-control" value="' + no_surat + '" id="NO_SURAT" name="NO_SURAT">';
            var TANGGAL_SURAT = '<input type="text" class="form-control" value="' + tanggal_surat + '" id="TANGGAL_SURAT" name="TANGGAL_SURAT">';
            var KETERANGAN = '<textarea class="form-control" value="' + keterangan + '" id="KETERANGAN" name="KETERANGAN"></textarea>';
            var TINDAK_LANJUT = '<textarea class="form-control" value="' + tindak_lanjut + '" id="TINDAK_LANJUT" name="TINDAK_LANJUT"></textarea>';
            var KENDALA = '<textarea class="form-control" value="' + kendala + '" id="KENDALA" name="KENDALA"></textarea>';
            var PROGRESS = '<textarea class="form-control sembunyi" value="' + progress + '" id="PROGRESS" name="PROGRESS"></textarea>';
            var FILE_NAME = '<input type="file" class="form-control sembunyi" value="' + file_name + '" id="files" name="files">';

            openTable.fnUpdate(NOMOR_SURAT, nRow, 0, false);
            openTable.fnUpdate(TANGGAL_SURAT, nRow, 1, false);
            openTable.fnUpdate(KETERANGAN, nRow, 2, false);
            openTable.fnUpdate(TINDAK_LANJUT, nRow, 3, false);
            openTable.fnUpdate(KENDALA, nRow, 4, false);
            openTable.fnUpdate(PROGRESS, nRow, 5, false);
            openTable.fnUpdate(FILE_NAME, nRow, 6, false);
            openTable.fnUpdate(tombol, nRow, 7, false);

            resetDatePicker();
            //Tanggal Surat
            $('input[name="tanggal-surat"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

        });

        var nosuratCancel = '';
        var keteranganCancel = '';
        var dueDateCancel = '';

        $('#table-detail').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();

           /* var nosuratCancel = data[0];
            console.log(nosuratCancel);*/

            //console.log('manual cancel ' + manualCancel);
            //console.log('condition cancel ' + conditionCancel);

            // Hapus Row 
            oTable.fnDeleteRow(baris);
            var json = JSON.parse('[' + DetailData + ']');


            // Deklarasi Variabel       
            var NO_SURAT = "";
            var TANGGAL_SURAT = "";
            var KETERANGAN = "";
            var TINDAK_LANJUT = "";
            var PROGRESS = "";
            var FILE_NAME = "";
            var KENDALA = "";


            json.forEach(function (object) {
                if (object.NO_SURAT == nosuratCancel && object.KETERANGAN == keteranganCancel && object.TANGGAL_SURAT == dueDateCancel) {
                    NO_SURAT = object.NO_SURAT;
                    TANGGAL_SURAT = object.TANGGAL_SURAT;
                    KETERANGAN = object.KETERANGAN;
                    TINDAK_LANJUT = object.TINDAK_LANJUT;
                    KENDALA = object.KENDALA;
                    PROGRESS = object.PROGRESS;
                    FILE_NAME = object.FILE_NAME;
                }
            });
            console.log(keteranganCancel);
            console.log(dueDateCancel);
            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-detail"><i class="fa fa-trash"></i></a></center>';

            oTable.fnAddData([NO_SURAT, TANGGAL_SURAT, KETERANGAN, TINDAK_LANJUT, KENDALA, PROGRESS, FILE_NAME, tombolC]);
        });

        //$('#table-detail').on('click', 'tr #btn-delcus-detail', function () {
        //    var baris = $(this).parents('tr')[0];
        //    var table = $('#table-detail').DataTable();
        //    var data = table.row(baris).data();
        //    oTable.fnDeleteRow(baris);
        //});
    }

    var ViewMonitoringKronologis = function () {
        $('body').on('click', 'tr #btn-view', function () {
            $('#txt-judul').text('Kronologis');
            $('#detailModal').modal('show');
            var baris = $(this).parents('tr')[0];
            var table = $('#table-monitorings').DataTable();
            var data = table.row(baris).data();
            console.log(data);
            /*var Table = $('#table-monitorings').dataTable();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);*/

            var id = data["ID"];
            $('#ID').val(data.ID);
            $('#CABANG').val(data.BE_NAME);
            $('#REGIONAL').val(data.REGIONAL);
            $('#JUDUL').val(data.JUDUL);
            $('#TANGGAL').val(data.TANGGAL);
            //$('#param_idd').val(data.ID);
            /*},
            {*/
            //console.log(id);
            $('#table-data-monitoring').DataTable({
                "ajax":
                {
                    "url": "/MonitoringKronologis/GetDataDetail/" + id,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "TANGGAL_SURAT",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "KETERANGAN",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "TINDAK_LANJUT",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "KENDALA",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "PROGRESS",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DIRECTORY",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="' + data + '">' + (row.FILE_NAME == null ? "-" : row.FILE_NAME) + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        })
    }

    function delRow($tr) {
        var id = $tr.find('#ID').val();
        console.log(id);
        if (id !== '') {
            $('<input>').attr({
                type: 'hidden',
                name: 'delete_line[]',
                value: id
            }).appendTo('#delete-line');
        }
        console.log($tr);
        //$tr.remove();
    }

    //$('#table-detail').on('click', 'tr #btn-delcus-detail', function () {
    //    console.log($tr);
    //    delRow($(this).closest('tr'));
    //});

    $('.del-upload').on('click', function () {
        var $td = $(this).closest('td');
        $td.empty();
        var tpl = $('#tmpl-upload').html();
        $td.append(tpl);
        $td.closest('tr').find('#FILE_DELETE').val('deleted');
    });

    $('#table-monitorings').on('click', '#btn-approve', function () {
        var btn = this;
        swal({
            title: "Approve",
            text: "Apakah anda yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then(function (result) {
            if (result) {
                var id = $(btn).data('id');
                App.blockUI({ boxed: true });
                $.ajax({
                    url: '/MonitoringKronologis/Approve',
                    method: "POST",
                    data: {
                        id: id
                    },
                    success: function (response) {
                        if (response.STATUS && response.STATUS == 'S') {
                            $('#table-monitorings').DataTable().ajax.reload()
                        } else {
                            if (response.message) {
                                swal('', response.message, 'error');
                            } else {
                                swal('', 'Terjadi kesalahan teknis.', 'error');
                            }
                        }
                    },
                    error: function (response) {
                        if (response.responseJSON && response.responseJSON.message)
                            swal('', response.responseJSON.message, 'error');
                    },
                    complete: function () {
                        App.unblockUI();
                    }
                });
            }
        });

    });

    $('#MonitoringKronologis').on('submit',function (e) {
        e.preventDefault();
        $('#table-data-monitoring tbody tr').each(function (i, e) {
            $(this).find(':input').each(function (j, f) {
                if (typeof $(f).attr('name') != 'undefined') {
                    $(f).attr('name', $(f).attr('name').replace(/__REINDEX__/g, i));
                }
            });
        });

        var data = new FormData($('#MonitoringKronologis')[0]);

        var urlSave = 'save';
        if ($('#ID').val() != '') {
            urlSave = 'saveEdit/' + $('#ID').val();
        }

        App.blockUI({ boxed: true });
        $.ajax({
            url: '/MonitoringKronologis/' + urlSave,
            method: "POST",
            enctype: 'multipart/form-data',
            data: data,
            processData: false,
            contentType: false,
            success: function (response) {
                if (response.STATUS && response.STATUS == 'S') {
                    window.location.href = "/MonitoringKronologis";
                } else {
                    if (response.message) {
                        swal('', response.message, 'error');
                    } else {
                        swal('', 'Terjadi kesalahan teknis.', 'error');
                    }
                }
            },
            error: function (response) {
                if (response.responseJSON && response.responseJSON.message)
                    swal('', response.responseJSON.message, 'error');
            },
            complete: function () {
                App.unblockUI();
            }
        });

    });


    /* $('body').on('click', 'tr #btn-edit', function () {
             var baris = $(this).parents('tr')[0];
             var table = $('#table-monitoring').DataTable();
             var data = table.row(baris).data();
 
             //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');
 
             window.location = "/MonitoringKronologis/AddDisposisi/" + data["ID"];
     });*/

    /* $('body').on('click', 'tr #btn-add', function () {
         var baris = $(this).parents('tr')[0];
         var table = $('#table-disposisi').DataTable();
         var data = table.row(baris).data();
 
         //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');
 
         window.location = "/MonitoringKronologis/AddSuratDisposisi/" + data["ID"];
     });*/

   /* var i = 0;
    $('#FILE_NAME').click(function () {

        *//*i++
        var a = `<div class="row" id="FILE_NAME` + i + `" style="margin:10px 0px">
                            <div class="col-md-10">
                                <input class="form-control" type="file" accept=".pdf" id="FILE_NAME" name="FILE_NAME" />
                            </div>
                        </div>
                        `;
        $('#FILE_NAME').append(a);*//*
    })*/
    /*function hapus_attc(id) {
        $('#attc' + id).remove();
    }*/

    $('body').on('click', 'tr #btn-delete', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-monitorings').DataTable();
        var data = table.row(baris).data();

        var id = data["ID"];
        console.log(param);
        swal({
            title: 'Warning',
            text: "Are you sure want to delete Kronologis  " + id + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MonitoringKronologis/Delete/" + id,
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-monitorings').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });

    $('#btn-update-baru').click(function () {
            var ID = parseInt($('#param_id').val());
            var CABANG = $('#CABANG').val();
            var REGIONAL = $('#REGIONAL').val();
            var TANGGAL = $('#TANGGAL').val();
            var JUDUL = $('#JUDUL').val();


        if (CABANG !== "" && REGIONAL !== "" && TANGGAL !== "" && JUDUL !== "") {
                var param = {
                    ID: ID,
                    BE_ID: CABANG,
                    REGIONAL: REGIONAL,
                    TANGGAL: TANGGAL,
                    JUDUL: JUDUL
                };
            console.log(param);

                App.blockUI({ boxed: true });

            var req = $.ajax({
                    url: "/MonitoringKronologis/InsertHeader",
                    method: "POST",
                    data: JSON.stringify(param),
                    contentType: "application/json",
                    dataType: "json",
                });
                console.log(data);
                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            window.location.href = '/MonitoringKronologis';
                            /*clear();
                            table.ajax.reload();*/
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            window.location.href = '/MonitoringKronologis';
                            /*clear();
                            table.ajax.reload();*/
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            //$('#addapvsetModal').modal('hide');
    })

    
    $('#btn-updatex').click(function () {//disable dlu
        var table = $('#table-data-monitoring').DataTable();
        var ID = parseInt($('#param_id').val());
        var BE_ID = $('#CABANG2').val();
        var REGIONAL = $('#REGIONAL2').val();
        var TANGGAL = $('#TANGGAL').val();
        var JUDUL = $('#JUDUL').val();
        var arrSurat = [];
        var ID_DETAIL = parseInt($('#param_idd').val());
        var NO_SURAT = $('#NO_SURAT').val();
        var TANGGAL_SURAT = $('#TANGGAL_SURAT').val();
        var KENDALA = $('#KENDALA').val();
        var PROGRESS = $('#PROGRESS').val();
        var TINDAK_LANJUT = $('#TINDAK_LANJUT').val();
        var KETERANGAN = $('#KETERANGAN').val();
        var ATTACHMENT = $('#FILE_NAME').val();

            /* data.append('detailKronologis', arrMonitoringKronologis);
 
             App.blockUI({ boxed: true });
             var form = $("#MonitoringKronologis")[0];*/

            /*var data = new FormData(form);
            var list = [];
            var rows = $("#table-data-monitoring tbody tr");
            console.log(rows);
            rows.each(function (index, row) {
                list.push({
                    'NO_SURAT': $(row).find("#NO_SURAT").val(),
                    'TANGGAL_SURAT': $(row).find("#TANGGAL_SURAT").val(),
                    'KENDALA': $(row).find("#KENDALA").val(),
                    'TINDAK_LANJUT': $(row).find("#TINDAK_LANJUT").val(),
                    'PROGRESS': $(row).find("#PROGRESS").val(),
                    'KETERANGAN': $(row).find("#KETERANGAN").val(),
                    'FILE_NAME': $(row).find("#FILE_NAME").val(),
                });
                console.log(row);
            });
            data.append('LIST_DATA', JSON.stringify(list));*/
            /*if (detailKronologis !== "") {
                App.blockUI({ boxed: true });
                var form = $("#MonitoringKronologis")[0];
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: "/MonitoringKronologis/SaveHeader",
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: data,
                    *//*success: function () {
                        reqMonitoring();
                    }*//*
                });
                var reqMonitoring = function () {
                    $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/MonitoringKronologis/AddDetailMonitoring",
                        timeout: 30000,
                        type: 'POST',
                        processData: false,
                       *//* data: data,
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        type: "POST",
                        url: "/MonitoringKronologis/SaveHeader",
                        enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        data: JSON.stringify(param),*//*

                        success: function (data) {
                            if (data.Status === 'S') {
                                //console.log(data.trans_id);
                                swal('Success', 'Data Added Succesfully', 'success').then(function (isConfirm) {
                                    window.location = "/MonitoringKronologis";
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/MonitoringKronologis";
                                });
                            }
                            App.unblockUI();
                        },
                        error: function (data) {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/MonitoringKronologis";
                            });
                        }
                    });
                    arrMonitoringKronologis = [];
                }

            } else {*/
            App.blockUI({ boxed: true });
            
            //console.log(data);
                var form = $("#MonitoringKronologis")[0];
                /*var data = {
                            'JUDUL': JUDUL,
                            'TANGGAL': TANGGAL,
                            'BE_ID': BE_ID,
                            'REGIONAL': REGIONAL,
                            'detailKronologis': []
                        };

                        data.detailKronologis = [];
                        //console.log(count);
                        for (var index = 0; index < count; index++) {
                            data.detailKronologis.push({
                                'NO_SURAT': $('.surat' + index).val(),
                                'TANGGAL_SURAT': $('.tgl-surat' + index).val(),
                                'KETERANGAN': $('.keterangan' + index).val(),
                                'TINDAK_LANJUT': $('.tindak-lanjut' + index).val(),
                                'PROGRESS': $('.progress' + index).val(),
                                'KENDALA': $('.kendala' + index).val(),
                            });
                        }*/
                
                var data_form = new FormData(form);
                //data_form.append("files", file);
                /*for (var i = 0; i < region.length; i++) {
                    formData.append("Regions[" + i + "].Id", region[i])
                }*/
                //data_form.append("detailKronologis", JSON.stringify(data.detailKronologis));

                $.ajax({
                    type: 'POST',
                    url: "/MonitoringKronologis/SaveHeader",
                    cache: false,
                    /*enctype: 'multipart/form-data',*/
                    contentType: false,
                    processData: false,
                    data: data_form,
                    /*success: function () {
                       reqDetail();
                    }
                });


                    var reqDetail = function () {
                        
                    $.ajax({
                        type: 'POST',
                        //url: "/MonitoringKronologis/SaveHeaderMonitoring",
                        url: "/MonitoringKronologis/SaveDetail",
                        *//*enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,*//*
                        data: data,*/

                        success: function (data) {
                            if (data.Status === 'S') {
                                //console.log(data.trans_id);
                                swal('Success', 'Data Added Succesfully', 'success').then(function (isConfirm) {
                                    window.location = "/MonitoringKronologis";
                                });
                            } else {
                                //swal('Failed', data.message, 'error').then(function (isConfirm) {
                                //    window.location = "/MonitoringKronologis";
                                //});
                            }
                            App.unblockUI();
                        },
                        error: function (data) {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/MonitoringKronologis";
                            });
                        }
                    });
                
            
            //App.blockUI({ boxed: true });
    });

    $('#btn-update-update').click(function () {
        var table = $('#table-detail').DataTable();
        var ID = parseInt($('#ID').val());
        var BE_ID = $('#FDDCabang').val();
        var REGIONAL = $('#FDDRegional').val();
        var TANGGAL = $('#TANGGAL').val();
        var JUDUL = $('#JUDUL').val();
        var arrSurat = [];
        var NO_SURAT = $('#NO_SURAT').val();
        var TANGGAL_SURAT = $('#TANGGAL_SURAT').val();
        var KENDALA = $('#KENDALA').val();
        var PROGRESS = $('#PROGRESS').val();
        var TINDAK_LANJUT = $('#TINDAK_LANJUT').val();
        var KETERANGAN = $('#KETERANGAN').val();
        var ATTACHMENT = $('#FILE_NAME').val();
        //('additionaltitlename');
        //var myElements = document.getElementsByName("form-control-NO-SURAT");
        // var NO_SURAT2 = $('input[name^=NO_SURAT]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // var TANGGAL_SURAT2 = $('input[name^=TANGGAL_SURAT]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // var KETERANGAN2 = $('input[name^=KETERANGAN]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // var KENDALA2 = $('input[name^=KENDALA]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // var PROGRESS2 = $('input[name^=PROGRESS]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // var TINDAK_LANJUT2 = $('input[name^=TINDAK_LANJUT]').map(function (idx, elem) {
        //     return $(elem).val();
        // }).get();

        // arrSurat.push({
        //     'NO_SURAT2': NO_SURAT2,
        //     'TANGGAL_SURAT2': TANGGAL_SURAT2,
        //     'KENDALA2': KENDALA2,
        //     'TINDAK_LANJUT2': PROGRESS2,
        //     'PROGRESS2': TINDAK_LANJUT2,
        //     'KETERANGAN2': KETERANGAN2,
        // });
        // console.log(arrSurat);
        // var detailKronologis = arrSurat;
        /* var arrMonitoringKronologis;
 
         var DTManualfCek = $('#table-data-monitoring').dataTable();
         var countDTManualfCek = DTManualfCek.fnGetData();
         arrMonitoringKronologis = new Array();
         for (var i = 0; i < countDTManualfCek.length; i++) {
             var itemDTMonitoringCek = DTManualfCek.fnGetData(i);
             //dueDate.push(itemDTConditionCek[2]);
 
             var paramDetailMonitoring = {
                 NO_SURAT: $('#NO_SURAT').val()[i],
                 TANGGAL_SURAT: itemDTMonitoringCek[1],
                 KETERANGAN: itemDTMonitoringCek[2],
                 TINDAK_LANJUT: itemDTMonitoringCek[3],
                 KENDALA: itemDTMonitoringCek[4],
                 PROGRESS: itemDTMonitoringCek[5],
                 FILE_NAME: itemDTMonitoringCek[6],
                 //FILE_NAME: itemDTMonitoringCek[7],
                 //OBJECT_ID: objectId
             }
             arrMonitoringKronologis.push(paramDetailMonitoring);*/
        /*var filex = [];

        filex.push(paramDetailMonitoring.FILE_NAME);*/
        /*console.log(filex);*/
        //}
        //console.log(itemDTMonitoringCek);

        /* if (ID !== "") {
 
         } else {
             var ATTACHMENT = $('#FILE_NAME').val();*/
        /*}*/
        if (JUDUL !== "" && TANGGAL !== "" && REGIONAL !== "") {
            App.blockUI({ boxed: true });
            var form = $("#edit-monitoring")[0];
            var data = new FormData(form);

            $.ajax({
                type: 'POST',
                url: "/MonitoringKronologis/UpdateHeader",
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                data: data,

                success: function (data) {
                    if (data.Status === 'S') {
                        //console.log(data.trans_id);
                        swal('Success', 'Data Update Succesfully', 'success').then(function (isConfirm) {
                            window.location = "/MonitoringKronologis";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/MonitoringKronologis";
                        });
                    }
                    App.unblockUI();
                },
                error: function (data) {
                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                        window.location = "/MonitoringKronologis";
                    });
                }
            });
            // }
            //App.blockUI({ boxed: true });
        } else {
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    });

    $('body').on('click', 'tr #btn-download', function () {
        $('#txt-judul').text('Download File Monitoring Kronologis');
        $('#viewDownload').modal('show');
        var baris = $(this).parents('tr')[0];
        var table = $('#table-monitorings').DataTable();
        var data = table.row(baris).data();
        var id = data["ID"];
        $('#table-download').DataTable({
            "ajax":
            {
                "url": "/MonitoringKronologis/GetDataDownload/" + id,
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "DIRECTORY",
                    "class": "dt-body-center",
                    "render": function (data, type, row, meta) {
                        if (type === 'display') {
                            data = '<a href="' + data + '">' + row.FILE_NAME + '</a>';
                        }

                        return data;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-downloadFILE" class="btn btn-icon-only green" title="Download" ><i class="fa fa-cloud-download"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center",
                },
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
        /*var oTable = $('#table-data-monitoring').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        var file_path = aData.DIRECTORY.replace("~/", "") + "/" + aData.FILE_NAME;
        var a = document.createElement('A');
        a.href = file_path;
        a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);*/
    });

    $('body').on('click', 'tr #btn-downloadFILE', function () {
        var oTable = $('#table-download').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
        var file_path = aData.DIRECTORY.replace("~/", "");
        console.log(file_path);
        var a = document.createElement('A');
        a.href = file_path;
        a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    });

    return {
        init: function () {
            initTable();
            add();
            cancel();
            edit();
            addDataMmonitoring();
            datePicker();
            defineExcel_7();
            resetDatePicker();
            ViewMonitoringKronologis();
            initTablDetailMenonitoring();
            //reqDetail();
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        DisposisiListIndex.init();
        $('#table-detail > tbody').html('');
    });
}