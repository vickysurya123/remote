﻿var fileWords;
var TransContract = function () {

    var handleDatePickers = function () {
        //var xx = $("#TANGGAL_END").val();
        ////console.log(xx);

        //var xdate = new Date();
        //xdate.setDate(xdate.getDate());
        
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                //startDate: xdate,
                //endDate: xdate,
                autoclose: true
            });
        }
    }

    var initDetilTransactionContract = function () {
        $('body').on('click', 'tr #btn-detail-trans-contract', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data(); 

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            $("#DETAILCONTRACT_NO").val(data["CONTRACT_NO"]);
            $("#DETAILCONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#DETAILRENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#DETAILCONTRACT_TYPE").val(data["CONTRACT_TYPE"]);
            $("#DETAILBE_ID").val(data["BE_ID"]);
            $("#DETAILCONTRACT_NAME").val(data["CONTRACT_NAME"]);
            $("#DETAILCONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#DETAILCONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#DETAILTERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#DETAILBUSINESS_PARTNER_NAME").val(data["BUSINESS_PARTNER_NAME"]);
            $("#DETAILBUSINESS_PARTNER").val(data["BUSINESS_PARTNER"]);
            $("#DETAILCUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#DETAILPROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#DETAILCONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#DETAILIJIN_PRINSIP_NO").val(data["IJIN_PRINSIP_NO"]);
            $("#DETAILIJIN_PRINSIP_DATE").val(data["IJIN_PRINSIP_DATE"]);
            $("#DETAILLEGAL_CONTRACT_NO").val(data["LEGAL_CONTRACT_NO"]);
            $("#DETAILLEGAL_CONTRACT_DATE").val(data["LEGAL_CONTRACT_DATE"]);
            $("#DETAILCURRENCY").val(data["CURRENCY"]);
            $("#DETAILMONTHS").val(data["TERM_IN_MONTHS"]);
            var start = data["CONTRACT_START_DATE"];
            var end = data["CONTRACT_END_DATE"];
            var term = data["TERM_IN_MONTHS"];
            var cur = data["CURRENCY"];
            var id_special = data["CONTRACT_NO"];
            $('#table-detil-object').DataTable({

                "ajax":
                {
                    "url": "/TransContract/GetDataDetailObject/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-condition').DataTable({

                "ajax":
                {
                    "url": "/TransContract/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "initComplete": function () {
                    setTimeout(coHitungValue(), 1000);
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_1",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //View detil manual
            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContract/GetDataDetailManualy/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                     {
                         "data": "MANUAL_NO",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "CONDITION",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "DUE_DATE",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "NET_VALUE",
                         "class": "dt-body-center",
                         render: $.fn.dataTable.render.number('.', '.', 0)
                     },
                     {
                         "data": "QUANTITY",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "UNIT",
                         "class": "dt-body-center"
                     }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detil memo
            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContract/GetDataDetailMemo/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },

                "columns": [
                    {
                        "data": "OBJECT_CALC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //get data Kontrak
            var id_co = "";
            var id_rr = "";
            var old_data = $.ajax({
                type: "GET",
                "url": "/TransContract/GetOldData/" + data["CONTRACT_OFFER_NO"],
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (data.Status == "S") {
                        data = data.Message;
                        id_co = data['CONTRACT_OFFER_OLD'];
                        id_rr = data['RENTAL_REQUEST_OLD'];
                        id_c = data['CONTRACT_OLD'];
                        ////console.log(id_co, id_rr, id_c);
                        $('.old-contract').css({ 'display': 'none' });
                        if (id_co != "" && id_co !=null ) {
                            $('.old-contract').css({ 'display': 'block' });
                            $(".CONTRACT_START_NEW").html(start);
                            $(".CONTRACT_END_NEW").html(end);
                            $(".CONTRACT_PERIOD_NEW").html(term);
                            $(".CURRENCY").html('&nbsp;' + cur);
                            $("#old_contract").text(id_c);
                            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
                            var arrRo;
                            var arrCond;
                            var req_Offer = $.ajax({
                                type: "GET",
                                "url": "/TransContractOffer/GetContractOffer/" + id_co,
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    if (data.Status == "S") {
                                        data = data.Message;
                                        //////console.log(data);

                                        $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                                        $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                                        $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                                    } else {
                                        //////console.log(data.Message);
                                    }
                                }
                            });
                            var req_Cond = $.ajax({
                                type: "GET",
                                "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    arrCond = data.data;
                                }
                            });
                            var req_Ro = $.ajax({
                                type: "GET",
                                "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    arrRo = data.data;
                                }
                            });
                            req_Ro.done(function () {
                                req_Offer.done();
                                req_Cond.done(function () {
                                    showOldContract(arrRo, arrCond);
                                });
                            });
                        }
                    } else {
                    }
                }
            });
           
        });
    }

    var initTableTransContract = function () {
        $('#table-trans-contract').DataTable({

            "ajax":
            {
                "url": "TransContract/GetDataTransContract",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },

            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NAME"
                },
                {
                    "data": "BUSINESS_PARTNER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS == '1') {
                            //if (full.CONTRACT_STATUS === 'APPROVED') {
                                return '<span class="label label-sm label-success"> APPROVED </span>';
                            //}
                        }
                        else if (full.STATUS == '2') {
                            //if (full.CONTRACT_STATUS === 'TERMINATED') {
                                return '<span class="label label-sm label-danger"> TERMINATED </span>';
                            //}
                        }
                        else {
                            return '<span class="label label-sm label-danger"> INACTIVE </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         if (full.STATUS == '1') {
                             //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                             var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                             aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Terminate Contract" id="btn-status"><i class="fa fa-times"></i></a>';// data-toggle="modal" href="#modalTerminated" 
                             aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah-contract"><i class="fa fa-edit"></i></a>';
                             aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Generate Contract Form" id="btn-export-contract"><i class="fa fa-file-word-o"></i></a>';
                             aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Generate BA Form" id="btn-export-ba"><i class="fa fa-file-word-o"></i></a>';
                             aksi += '<a data-toggle="modal" href="#viewTransSpecialContractModal" class="btn btn-icon-only green" data-toggle="tooltip" title="Detail Contract" id="btn-detail-trans-contract"><i class="fa fa-eye"></i></a>';
                         }
                         else {
                             //var aksi = '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                             var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                             aksi += '<button class="btn btn-icon-only red" id="btn-status" disabled><i class="fa fa-times"></i></button>';
                             aksi += '<button class="btn btn-icon-only blue" id="btn-ubah-contract" disabled><i class="fa fa-edit"></i></button>';
                             aksi += '<button class="btn btn-icon-only grey" id="btn-export-contract" disabled><i class="fa fa-file-word-o"></i></button>';
                             aksi += '<button class="btn btn-icon-only yellow" id="btn-export-ba" disabled><i class="fa fa-file-word-o"></i></button>';
                             aksi += '<a data-toggle="modal" href="#viewTransSpecialContractModal" class="btn btn-icon-only green" id="btn-detail-trans-contract"><i class="fa fa-eye"></i></a>';
                         }
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/TransContract/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }
    
    var ChangeConfirmation = function () {
        $("#SELECT_CONTRACT_STATUS").change(function () {
            var kgt = $('#SELECT_CONTRACT_STATUS').val();
            if (kgt.includes("TERMINATED") === true) {
                //$(".Inactive").hide();
                //$(".Terminate").show();
                $("#nameButton").remove();
                $("#btn-confirm-terminate").append('<span id="nameButton"></span>');
                $("#nameButton").append('Terminate');
            } else {
                $("#nameButton").remove();
                $("#btn-confirm-terminate").append('<span id="nameButton"></span>');
                $("#nameButton").append('Inactive');
                //$(".Inactive").show();
                //$(".Terminate").hide();
            }
        });
    }
    
    var terminatedContract = function () {
        $('body').on('click', 'tr #btn-status', function () {
            clear();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + data['CONTRACT_NO'],
                method: "get",
                url: "/TransContract/CheckBilling",
                timeout: 30000
            });

            req.done(function (data) {
                App.unblockUI();
                table.ajax.reload(null, false);
                if (data.status === 'S') {
                    $("#modalTerminated").modal();

                    
                } else {
                    swal("Failed To Terminated", "Masih ada Billing yang belum dibatalkan!", "error")
                }
            });

            //$('#CONTRACT_NO_EDIT').val(data['CONTRACT_NO']);
            //$('#TANGGAL_END').val(moment(data['CONTRACT_END_DATE']).format("DD/MM/YYYY"));

            //$("#nameButton").remove();
            //$("#btn-confirm-terminate").append('<span id="nameButton"></span>');
            //$("#nameButton").append('Terminate');
            //handleDatePickers();

        });
    }

    var btnConfirmTerminated = function () {
        $('#btn-confirm-terminate').click(function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            var SELECT_CONTRACT_STATUS = $('#SELECT_CONTRACT_STATUS').val();
            //var CONTRACT_NO_EDIT = $('#CONTRACT_NO_EDIT').val();
            var CONTRACT_NO_EDIT = data['CONTRACT_NO'];
            var STOP_CONTRACT_DATE = $('#STOP_CONTRACT_DATE').val();
            var MEMO_CONTRACT = $('#MEMO_CONTRACT').val();

            console.log(CONTRACT_NO_EDIT, data['CONTRACT_NO']);
            if (STOP_CONTRACT_DATE && MEMO_CONTRACT) {
                var param = {
                    SELECT_CONTRACT_STATUS: SELECT_CONTRACT_STATUS,
                    CONTRACT_NO: CONTRACT_NO_EDIT.toString(),
                    STOP_CONTRACT_DATE: STOP_CONTRACT_DATE,
                    MEMO_CONTRACT: MEMO_CONTRACT
                };
                ////console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransContract/TerminatedContract",
                    timeout: 30000,
                    success: function (data) {
                        ////console.log(data);
                        table.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#modalTerminated').modal('hide');
                            swal('Success', 'Contract Has Been Terminated', 'success');
                            clear();

                        } else {
                            $('#modalTerminated').modal('hide');
                            swal('Failed', 'Something Went Wrong', 'error');
                            clear();

                        }
                    },
                    error: function (data) {
                        ////console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                $('#modalTerminated').modal('hide');
                swal('Warning', 'Please Fill Date and Memo!', 'warning');
                clear();
            }
        });
    }

    var clear = function () {
        $("#STOP_CONTRACT_DATE").val('');
        $("#MEMO_CONTRACT").val('');
        $('#SELECT_CONTRACT_STATUS').prop('selectedIndex', 0).trigger('change');
    }

    //var fupload = function () {
    //    $('#fileupload').click(function () {
    //        $("#fileupload").css('display', 'block'); //enable upload

    //        var BE_ID = $('#ID_ATTACHMENT').val();

    //        $('#fileupload').fileupload({
    //            beforeSend: function (e, data) {
    //                ////console.log(data.loaded);
    //                var progress = parseInt(0);
    //                $('.progress .progress-bar').css('width', progress + '%');
    //                ////console.log('data loaded upload ' + data.loaded);
    //                setTimeout(progress, 10000);
    //            },
    //            dataType: 'json',
    //            type: 'POST',
    //            url: '/TransContractOffer/UploadFiles',
    //            formData: {
    //                BE_ID: BE_ID
    //            },
    //            autoUpload: true,
    //            done: function (e, data) {
    //                $('.file_name').html(data.result.name);
    //                $('.file_type').html(data.result.type);
    //                $('.file_size').html(data.result.size);
    //                $('#table-attachment').dataTable().api().ajax.reload();
    //            }
    //        }).on('fileuploadprogressall', function (e, data) {
    //            var progress = parseInt(data.loaded / data.total * 100, 10);
    //            ////console.log('data loaded upload ' + data.loaded);
    //            $('.progress .progress-bar').css('width', progress + '%');
    //        });

    //    });
    //}

    var add = function () {
        $('#btn-add').click(function () {

            window.location.href = '/TransContract/AddTransContract';
        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["CONTRACT_NO"];

            $("#ID_ATTACHMENT").val(data["CONTRACT_NO"]);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TransContract/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();
            console.log(BE_ID);
            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    console.log(data, e);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContract/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { BE_ID: BE_ID },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var uriId = $('#ID_ATTACHMENT').val();
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContract/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var initExportdatacontract = function () {
        $('body').on('click', 'tr #btn-export-contract', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var BEID = data["BE_NAME"];
            var CUSTOMERNAME = data["BUSINESS_PARTNER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAME = data["TERMINAL_NAME"];
            var RO_CERTIFICATE_NUMBER = data["RO_CERTIFICATE_NUMBER"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var CONTRACT_TYPE = data["CONTRACT_TYPE"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }

            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];                       

            var CONTRACT_NO = data["CONTRACT_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_NAME = data["CONTRACT_NAME"];
            var BUSINESS_PARTNER_NAME = data["BUSINESS_PARTNER_NAME"];
            var BUSINESS_PARTNER = data["BUSINESS_PARTNER"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var PROFIT_CENTER = data["PROFIT_CENTER"];
            var IJIN_PRINSIP_NO = data["IJIN_PRINSIP_NO"];
            var IJIN_PRINSIP_DATE = data["IJIN_PRINSIP_DATE"];
            var LEGAL_CONTRACT_NO = data["LEGAL_CONTRACT_NO"];
            var LEGAL_CONTRACT_DATE = data["LEGAL_CONTRACT_DATE"];
            var CURRENCY = data["CURRENCY"];
            var REGIONAL_NAME = data["REGIONAL_NAME"];
            var VALID_FROM = data["VALID_FROM"];
            var VALID_TO = data["VALID_TO"];
            var TOTAL_NET_VALUE = data["TOTAL_NET_VALUE"];
            var USER_NAME = data["USER_NAME"];
            var NJOP_PERCENT = data["NJOP_PERCENT"];


            if (NJOP_PERCENT == undefined) {
                NJOP_PERCENT = '0';
            }

            var dataJSON = "{CONTRACT_NO:" + JSON.stringify(CONTRACT_NO) + "}";
            $.ajax({
                type: "POST",
                url: "/TransContract/exportWordContractCondition",
                data: dataJSON,
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI, PENGALIHAN, ADMINISTRASI, WARMERKING;
                    //var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI, PENGALIHAN, ADMINISTRASI, WARMERKING;
                    var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI, PENGALIHAN, ADMINISTRASI, WARMERKING, NJOP_DARATAN, NJOP_BANGUNAN, NJOP_NORMALISASI, NJOP_PENGALIHAN, NJOP_ADMINISTRASI, NJOP_WARMERKING, TOTAL_SEWA_BANGUNAN, TOTAL_SEWA_DARATAN;

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            SEWA_DARATAN = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            SEWA_BANGUNAN = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NORMALISASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            PENGALIHAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            ADMINISTRASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            WARMERKING = response.data[i].TOTAL_NET_VALUE;
                        } 
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            NJOP_DARATAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            NJOP_BANGUNAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NJOP_NORMALISASI = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            NJOP_PENGALIHAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            NJOP_ADMINISTRASI = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            NJOP_WARMERKING = response.data[i].NJOP_PERCENT;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            TOTAL_SEWA_DARATAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            TOTAL_SEWA_BANGUNAN = response.data[i].TOTAL_NET_VALUE;
                        }
                    }

                    //var VALID_FROM = data["VALID_FROM"];
                    //var VALID_TO = response.data["VALID_TO"];
                    //var TOTAL_NET_VALUE = response.data["TOTAL_NET_VALUE"];

                    //jumlah_sewa = parseInt(LUAS) * (parseInt(NJOP_PERCENT) * parseInt(SEWA)) * parseInt(TERM_IN_MONTHS);
                    //var jumlah_sewa = parseInt(SEWA_BANGUNAN);


                    console.log("sewa : " + jumlah_sewa + ", ppn : " + ppn + ", seluruh: " + jumlah_seluruh);

                    if (SEWA_DARATAN == undefined) {
                        SEWA_DARATAN = '0';
                    }

                    if (SEWA_BANGUNAN == undefined) {
                        SEWA_BANGUNAN = '0';
                    }

                    if (NORMALISASI == undefined) {
                        //NORMALISASI = '....................';
                        NORMALISASI = '0';
                    }

                    if (PENGALIHAN == undefined) {
                        //PENGALIHAN = '....................';
                        PENGALIHAN = '0';
                    }

                    if (ADMINISTRASI == undefined) {
                        //ADMINISTRASI = '....................';
                        ADMINISTRASI = '0';
                    }

                    if (WARMERKING == undefined) {
                        //WARMERKING = '...............';
                        WARMERKING = '0';
                    }

                    if (NJOP_DARATAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_DARATAN = '0';
                    }
                    if (NJOP_BANGUNAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_BANGUNAN = '0';
                    }
                    if (NJOP_ADMINISTRASI == undefined) {
                        //WARMERKING = '...............';
                        NJOP_ADMINISTRASI = '0';
                    }
                    if (NJOP_NORMALISASI == undefined) {
                        //WARMERKING = '...............';
                        NJOP_NORMALISASI = '0';
                    }
                    if (NJOP_WARMERKING == undefined) {
                        //WARMERKING = '...............';
                        NJOP_WARMERKING = '0';
                    }
                    if (NJOP_PENGALIHAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_PENGALIHAN = '0';
                    }
                    if (TOTAL_SEWA_DARATAN == undefined) {
                        //WARMERKING = '...............';
                        TOTAL_SEWA_DARATAN = '0';
                    }
                    if (TOTAL_SEWA_BANGUNAN == undefined) {
                        //WARMERKING = '...............';
                        TOTAL_SEWA_BANGUNAN = '0';
                    }
                                     

                    var dataJSONS = "{BE_ID:" + JSON.stringify(BE_ID) + ",CUSTOMER_NAME:" + JSON.stringify(CUSTOMER_NAME) + ",RO_CERTIFICATE_NUMBER:" + JSON.stringify(RO_CERTIFICATE_NUMBER) +
                           ",LUAS_TANAH:" + JSON.stringify(LUAS_TANAH) + ",LUAS_BANGUNAN:" + JSON.stringify(LUAS_BANGUNAN) + ",RO_ADDRESS:" + JSON.stringify(RO_ADDRESS) +
                           ",CONTRACT_USAGE:" + JSON.stringify(CONTRACT_USAGE) + ",TERM_IN_MONTHS:" + JSON.stringify(TERM_IN_MONTHS) + ",CONTRACT_START_DATE:" + JSON.stringify(CONTRACT_START_DATE) +
                           ",CONTRACT_END_DATE:" + JSON.stringify(CONTRACT_END_DATE) + ",CONTRACT_TYPE:" + JSON.stringify(CONTRACT_TYPE) + ",REGIONAL_NAME:" + JSON.stringify(REGIONAL_NAME) + 
                        ",VALID_FROM:" + JSON.stringify(VALID_FROM) + ",VALID_TO:" + JSON.stringify(VALID_TO) + ",BUSINESS_PARTNER_NAME:" + JSON.stringify(BUSINESS_PARTNER_NAME) + 
                        ",SEWA_DARATAN:" + JSON.stringify(SEWA_DARATAN) + ",SEWA_BANGUNAN:" + JSON.stringify(SEWA_BANGUNAN) + ",NORMALISASI:" + JSON.stringify(NORMALISASI) + ",PENGALIHAN:" + JSON.stringify(PENGALIHAN) +
                        ",ADMINISTRASI:" + JSON.stringify(ADMINISTRASI) + ",WARMERKING:" + JSON.stringify(WARMERKING) + ",NJOP_DARATAN: " + JSON.stringify(NJOP_DARATAN) + ", NJOP_BANGUNAN: " + JSON.stringify(NJOP_BANGUNAN) +
                            ",TOTAL_SEWA_DARATAN:" + JSON.stringify(TOTAL_SEWA_DARATAN) + ",TOTAL_SEWA_BANGUNAN:" + JSON.stringify(TOTAL_SEWA_BANGUNAN) + ",NJOP_NORMALISASI:" + JSON.stringify(NJOP_NORMALISASI) + "}"; 

                    if (SEWA_BANGUNAN == "0") {
                        var SEWA = SEWA_DARATAN;
                    }
                    else if (SEWA_DARATAN == "0") {
                        SEWA = SEWA_BANGUNAN;
                    }
                    else {
                        SEWA = SEWA_BANGUNAN;
                    }


                    if (LUAS_TANAH == "0") {
                        var LUAS = LUAS_BANGUNAN;
                    }
                    else if (LUAS_BANGUNAN == "0") {
                        LUAS = LUAS_TANAH;
                    }
                    else {
                        LUAS = LUAS_TANAH;
                    }

                    var Years = parseFloat(TERM_IN_MONTHS) / 12;

                    var jumlah_sewa = parseFloat(LUAS) * (0.05 * parseFloat(SEWA)) * Years;
                    var ppn = 0.1 * jumlah_sewa;

                    var jumlah_seluruh = jumlah_sewa + ppn + parseFloat(ADMINISTRASI) + parseFloat(WARMERKING);



                    console.log("sewa : " + jumlah_sewa + ", ppn : " + ppn + ", seluruh: " + jumlah_seluruh);

                    console.log(dataJSONS);

                    App.blockUI({ boxed: true });
                    $.ajax({
                        type: "POST",
                        url: "/TransContract/exportWordContract",
                        data: dataJSONS,
                        contentType: "application/json; charset-utf8",
                        datatype: "jsondata",
                        async: "true",
                        success: function (data) {
                            fileWords = data.data;
                            App.unblockUI();
                            swal({
                                title: "File Successfully Generated",
                                text: "[" + data.data + "]" +
                                    '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                                type: "success",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        error: function (response) {
                            //console.log(response);
                        }
                    });
                },
                error: function (response) {
                    //console.log(response);
                }
            });

            //var dataJSON = "{BE_ID:" + JSON.stringify(BE_ID) + ",CUSTOMER_NAME:" + JSON.stringify(CUSTOMER_NAME) + ",RO_CERTIFICATE_NUMBER:" + JSON.stringify(RO_CERTIFICATE_NUMBER) +
            //               ",LUAS_TANAH:" + JSON.stringify(LUAS_TANAH) + ",LUAS_BANGUNAN:" + JSON.stringify(LUAS_BANGUNAN) + ",RO_ADDRESS:" + JSON.stringify(RO_ADDRESS) +
            //               ",CONTRACT_USAGE:" + JSON.stringify(CONTRACT_USAGE) + ",TERM_IN_MONTHS:" + JSON.stringify(TERM_IN_MONTHS) + ",CONTRACT_START_DATE:" + JSON.stringify(CONTRACT_START_DATE) +
            //               ",CONTRACT_END_DATE:" + JSON.stringify(CONTRACT_END_DATE) + ",CONTRACT_TYPE:" + JSON.stringify(CONTRACT_TYPE) + ",REGIONAL_NAME:" + JSON.stringify(REGIONAL_NAME) + 
            //               ",VALID_FROM:" + JSON.stringify(VALID_FROM) + ",VALID_TO:" + JSON.stringify(VALID_TO) + "}";

            //console.log(dataJSON)

            //App.blockUI({ boxed: true });
            //$.ajax({
            //    type: "POST", 
            //    url: "/TransContract/exportWordContract",
            //    data: dataJSON,
            //    contentType: "application/json; charset-utf8",
            //    datatype: "jsondata",
            //    async: "true",
            //    success: function (data) {
            //        fileWords = data.data;
            //        App.unblockUI();
            //        swal({
            //            title: "File Successfully Generated",
            //            text: "[" + data.data + "]" +
            //                  '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
            //                  ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
            //            type: "success",
            //            showConfirmButton: false,
            //            allowOutsideClick: false
            //        });
            //    },
            //    error: function (response) {
            //        //console.log(response);
            //    }
            //});
        });

        $('body').on('click', 'tr #btn-export-ba', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();

            var BEID = data["BE_NAME"];
            var CUSTOMERNAME = data["BUSINESS_PARTNER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAME = data["TERMINAL_NAME"];
            var RO_CERTIFICATE_NUMBER = data["RO_CERTIFICATE_NUMBER"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var CONTRACT_TYPE = data["CONTRACT_TYPE"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }

            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];

            //console.log(BE_ID);
            //console.log(CUSTOMER_NAME);
            //console.log(CONTRACT_USAGE);
            //console.log(PROFIT_CENTER_NAME);
            //console.log(RO_ADDRESS);

            var CONTRACT_NO = data["CONTRACT_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_NAME = data["CONTRACT_NAME"];
            var BUSINESS_PARTNER_NAME = data["BUSINESS_PARTNER_NAME"];
            var BUSINESS_PARTNER = data["BUSINESS_PARTNER"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var PROFIT_CENTER = data["PROFIT_CENTER"];
            var IJIN_PRINSIP_NO = data["IJIN_PRINSIP_NO"];
            var IJIN_PRINSIP_DATE = data["IJIN_PRINSIP_DATE"];
            var LEGAL_CONTRACT_NO = data["LEGAL_CONTRACT_NO"];
            var LEGAL_CONTRACT_DATE = data["LEGAL_CONTRACT_DATE"];
            var CURRENCY = data["CURRENCY"];

            var dataJSON = "{BE_ID:" + JSON.stringify(BE_ID) + ",CUSTOMER_NAME:" + JSON.stringify(CUSTOMER_NAME) + ",RO_CERTIFICATE_NUMBER:" + JSON.stringify(RO_CERTIFICATE_NUMBER) +
                           ",LUAS_TANAH:" + JSON.stringify(LUAS_TANAH) + ",LUAS_BANGUNAN:" + JSON.stringify(LUAS_BANGUNAN) + ",RO_ADDRESS:" + JSON.stringify(RO_ADDRESS) +
                           ",CONTRACT_USAGE:" + JSON.stringify(CONTRACT_USAGE) + ",TERM_IN_MONTHS:" + JSON.stringify(TERM_IN_MONTHS) + ",CONTRACT_START_DATE:" + JSON.stringify(CONTRACT_START_DATE) +
                           ",CONTRACT_END_DATE:" + JSON.stringify(CONTRACT_END_DATE) + ",CONTRACT_TYPE:" + JSON.stringify(CONTRACT_TYPE) + "}";

            App.blockUI({ boxed: true });
            $.ajax({
                type: "POST",
                url: "/TransContract/exportWordBA",
                data: dataJSON,
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (data) {
                    fileWords = data.data;
                    App.unblockUI();
                    swal({
                        title: "File Successfully Generated",
                        text: "[" + data.data + "]" +
                              '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                              ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                        type: "success",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                error: function (response) {
                    //console.log(response);
                }
            });
        });
    }

    var editContract = function () {
        $('body').on('click', 'tr #btn-ubah-contract', function () {
           var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-contract').DataTable();
            var data = table.row(baris).data();
            window.location = "/ContractChange/EditContract/" + data["CONTRACT_NO"];
            console.log('ALHAMDULILLAH');
          
        });
    }
    var showOldContract = function (ro, cond) {
        //////console.log(ro, cond)
        var all_value = 0;
        $('.DETAIL_RO').html('');
        for (var i = 0; i < cond.length; i++) {

            var net_val = cond[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var price = cond[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = cond[i].CALC_OBJECT;

            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_cond = calc_ob.split(' ')[1];

                for (var j = 0; j < ro.length; j++) {
                    ro_data = ro[j];
                    //////console.log(ro_data);
                    var ro_id = ro_data.OBJECT_ID;
                    if (ro_id == ro_cond) {
                        var luas = Number(ro_data.LAND_DIMENSION) + Number(ro_data.BUILDING_DIMENSION);
                        var text = $('.DETAIL_RO').html() + '<tr><td>' + ro_data.OBJECT_ID + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#DETAILCURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO').html(text);
                    }
                }
            }
        }
        $('#CONTRACT_VALUE').val(all_value);
        $('.CONTRACT_VALUE').html(sep1000(all_value, true));
    }
    function coHitungValue() {
        var con = $('#table-detil-condition').DataTable();
        var all_value = 0;
        //var data = con.data()
        //////console.log(data);
        $('.DETAIL_RO_NEW').html('');
        for (var i = 0; i < con.data().count() ; i++) {
            var row = con.row(i).data();
            //////console.log(row);
            var net_val = row.TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var price = row.UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = row.CALC_OBJECT;
            //////console.log(net_val, calc_ob);
            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_number = calc_ob.split(' ')[1];
                var elems = $('#table-detil-object').find('tbody tr');
                $(elems).each(function (index) {
                    var x = $(this).find('td')[0];
                    if ($(x).html() == ro_number) {
                        //////console.log(index);
                        ro_data = $('#table-detil-object').DataTable().row(index).data();
                        ////console.log(ro_data);
                        var luas = Number(ro_data.LUAS_BANGUNAN) + Number(ro_data.LUAS_TANAH);
                        var text = $('.DETAIL_RO_NEW').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#DETAILCURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO_NEW').html(text)
                        ////console.log(text);
                    }
                });
            }
        }
        //////console.log(text);
        $('.CONTRACT_VALUE_NEW').html(all_value.toLocaleString('en-US'));
    }
    return {
        init: function () {
            handleDatePickers();
            add();
            initTableTransContract();
            initDetilTransactionContract();
            //ubahstatus();
            ChangeConfirmation();
            terminatedContract();
            btnConfirmTerminated();
            initAttachment();
            fupload();
            deleteAttachment();
            initExportdatacontract();
            editContract();
        }
    };
}();

function Clicks() {
    //console.log('Yess ' + fileWords);
    var jsonData = "{fileName:'" + fileWords + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/TransContractOffer/deleteWord",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            //console.log("Sukses");
        },
        error: function (response) {
            //console.log(response);
        }
    });

}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContract.init();
    });
}
function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}