﻿var TransContractBillingList = function () {

    var initTableContractBillingList = function () {
        $('#table-list-contract-billing').DataTable({

            "ajax":
            {
                "url": "TransContractBillingList/GetDataTransContractBillingList",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "BILLING_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_NAME",
                },
                {
                    "data": "BILLING_CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NAME",
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "BILLING_PERIOD",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLMENT_AMOUNT",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.SAP_DOC_NO != null) {
                            if (full.CANCEL_STATUS != null) {
                                return full.SAP_DOC_NO + '<br><span class="badge badge-danger"> CANCEL </span>';
                            }
                            else {
                                return full.SAP_DOC_NO;
                            }
                        } else {
                            if (full.CANCEL_STATUS != null) {
                                return '<span class="badge badge-danger"> CANCEL </span>';
                            }
                            else {
                                return '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';

                            }
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var mplg = full.CUSTOMER_NAME.split(" - ");
                        var link = "https://ibs-xapi.pelindo.co.id/api//enotav2/cetakPdf?me=" + btoa("pranota") + "&ne=" + btoa(full.SAP_DOC_NO) + "&ze=" + btoa(full.BILLING_NO) + "&ck=" + btoa(full.BRANCH_ID) + "&mk=" + btoa(mplg[0]) + "&nu=" + btoa("remote") + "&mn=" + btoa(full.CONTRACT_NAME);
                        //var link = "https://ibs-xapi.pelindo.co.id/api//enotav2/cetakPdf?me=" + atob("cHJhbm90YQ==") + "&ne=" + atob("NzIwMDAwMDEyNA==") + "&ze=" + atob("NDEwMDAzNzE=") + "&ck=" + atob("OQ==") + "&mk=" + atob("MDAwMTg=") + "&nu=" + atob("MDkwMDAxODAx") + "&mn=" + atob("SU5ET05FU0lBIFBPV0VSIFVCUFMgUFQu");
                        //var link = "https://ibs-xapi.pelindo.co.id/api//enotav2/cetakPdf?me=" + "pranota" + "&ne=" +full.SAP_DOC_NO + "&ze=" + full.BILLING_NO + "&ck=" + full.BRANCH_ID + "&mk=" + mplg[0] + "&nu=" + mplg[0] + "&mn=" + full.CONTRACT_NAME;
                        var aksi = '<a data-toggle="modal" href="#viewTransContractBillingListModal" class="btn btn-icon-only green" id="btn-detail-billing"><i class="fa fa-eye"></i></a>'+
                            '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Print Pranota" target="_blank"><i class="fa fa-print"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var initDetilTransactionContractBilling = function () {
        $('body').on('click', 'tr #btn-detail-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-contract-billing').DataTable();
            var data = table.row(baris).data();

            var tableDetilBilling = $('#table-detil-billing').DataTable();
            tableDetilBilling.destroy();

            $("#DETAILBILLING_NO").val(data["BILLING_NO"]);
            $("#DETAILCONTRACT_NO").val(data["BILLING_CONTRACT_NO"]);
            $("#DETAILLEGAL_CONTRACT_NO").val(data["LEGAL_CONTRACT_NO"]);
            $("#DETAILPROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#DETAILCONTRACT_NAME").val(data["CONTRACT_NAME"]);
            $("#DETAILCUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#DETAILCUSTOMER_CODE").val(data["CUSTOMER_CODE"]);
            $("#DETAILCUSTOMER_CODE_SAP").val(data["CUSTOMER_CODE_SAP"]);
            $("#DETAILCREATION_DATE").val(data["CREATION_DATE"]);
            $("#DETAILPOSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAILSAP_DOC_NO").val(data["SAP_DOC_NO"]);
            $("#DETAILFREQ_NO").val(data["FREQ_NO"]);
            $("#DETAILBILLING_PERIOD").val(data["BILLING_PERIOD"]);
            $("#DETAILCURRENCY_CODE").val(data["CURRENCY_CODE"]);

            ambilInstallmentAmount = data["INSTALLMENT_AMOUNT"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            //INSTALLMENT_AMOUNT.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#DETAILINSTALLMENT_AMOUNT").val(ambilInstallmentAmount);


            var id_special = data["BILLING_NO"];

            $('#table-detil-billing').DataTable({

                "ajax":
                {
                    "url": "/TransContractBillingList/GetDataDetailBillingList/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "REF_DESC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_FLAG_TAX1",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "CURRENCY_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_QTY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_UOM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "GL_ACOUNT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLMENT_AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    // Aksi Button Untuk POST BILLING
    var postBilling = function () {
        $('body').on('click', 'tr #btn-post-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-contract-billing').DataTable();
            var data = table.row(baris).data();

            var billing_contract_no = data["BILLING_CONTRACT_NO"];
            var billing_no = data["BILLING_NO"];

            console.log(billing_no);
            console.log(billing_contract_no);
            //console.log(BILL_TYPE);

            var p = {
                BILLING_CONTRACT_NO: billing_contract_no,
                BILL_ID: billing_no,
                BILL_TYPE: 'ZB01'
            };

            swal({
                title: 'Warning',
                text: "Are you sure want to post this data to Billing?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(p),
                        method: "POST",
                        url: "TransContractBilling/postToSAP",

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', 'Billing ID ' + billing_no + ',  With SAP Document Number : ' + data.document_number, 'success').then(function (isConfirm) {
                                });
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "/TransContractBilling/GetMsgSAP",
                                    data: "{ BILL_ID:'" + billing_no + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        var tampung = "";
                                        for (var i = 0; i < data.length; i++) {
                                            var x = i % 2;

                                            //console.log(data[i]);
                                            if (x === 1) {
                                                console.log(data[i]);
                                                tampung += '<ul><li>' + data[i] + '</li></ul>';
                                            }
                                        }
                                        swal('Failed', tampung, 'error').then(function (isConfirm) {
                                            //window.location = "/TransContractBilling";
                                        });
                                    }
                                });
                            }
                        },
                        error: function (data) {
                            swal('Failed', 'Failed to Add Data.', 'error');
                            //console.log(data.responeText);
                        }

                    });
                }
            });
        });
    }
    $('body').on('click', 'tr #btn-pranota', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-list-contract-billing').DataTable();
        var data = table.row(baris).data();

        var id_special = data["ID"];
        var contractNo = data["BILLING_CONTRACT_NO"];
        var billingNo = data["BILLING_NO"]; 
        var kodeCabang = data["BRANCH_ID"];
        console.log(data);
        //window.location = "/Pdf/cetakPdf?tp=" + btoa("1B") + "&bn=" + btoa(billingNo) + "&cn=" + btoa(contractNo) + "&kc=" + btoa(kodeCabang);
        window.open("/Pdf/cetakPdf?tp=" + btoa("1B") + "&bn=" + btoa(billingNo) + "&kc=" + btoa(kodeCabang), '_blank');
    });

    return {
        init: function () {
            initTableContractBillingList();
            initDetilTransactionContractBilling();
            postBilling();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContractBillingList.init();
    });
}