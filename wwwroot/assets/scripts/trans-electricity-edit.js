﻿$("#METER_TO").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

$("#METER_FROM").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

// Define Tagihan
var totalTagihan = 0;
var fTotalTagihan = 0;
var biayaBeban = 0;
var biayaAdmin = 0;
var tagMin = 0;
var capacityVA = 0;

// Define Multiply Factor
var multiplyFactor = 0;

// GET DATA PRICING
var tarifLWBP = 0;
var tarifKVARH = 0;
var tarifWBP = 0;
var tarifBLOK1 = 0;
var tarifBLOK2 = 0;

var usedLWBP = 0;
var usedKVARH = 0;
var usedWBP = 0;
var usedBLOK1 = 0;
var usedBLOK2 = 0;

var maxRangeUsedBLOk1 = 0
var maxRangeUsedBLOK2 = 0

// GET DATA COSTING
var persenPPJU = 0;
var persenREDUKSI = 0;

// Define Kode cabang dan min KWH
var kodeCabang = 0;
var minKWH = 0;
var kodeprofitcenter = 0;

var TransWaterElectricityAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWEList";
        });
    }

    var ambilHeader = function () {
        var trans_id = getParameterByName("xTd");
        var inst_id = getParameterByName("xId");

        var param = {
            TRANSACTION_ID: trans_id,
            INSTALLATION_ID: inst_id
        };

        $.ajax({
            type: "POST",
            url: "/TransWEList/getHeaderElectricityEdit",
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            success: function (data) {
                var jsonList = data
                var listItems1 = "";
                var disableTextBoxBLOK2 = '';
                var INSTALLATION_TYPE = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    INSTALLATION_TYPE = jsonList.data[i].INSTALLATION_TYPE;
                    STATUS_DINAS = jsonList.data[i].STATUS_DINAS;
                    INSTALLATION_CODE = jsonList.data[i].INSTALLATION_CODE;
                    PROFIT_CENTER = jsonList.data[i].PROFIT_CENTER;
                    CUSTOMER_MDM = jsonList.data[i].CUSTOMER_NAME;
                    CUSTOMER_CODE = jsonList.data[i].CUSTOMER;
                    MINIMUM_USED = jsonList.data[i].MINIMUM_USED;
                    CAPACITY_VA = jsonList.data[i].POWER_CAPACITY;
                    MIN_PAYMENT = jsonList.data[i].MINIMUM_PAYMENT;
                    BIAYA_BEBAN = jsonList.data[i].BIAYA_BEBAN;
                    BIAYA_ADMIN = jsonList.data[i].BIAYA_ADMIN;
                    INSTALLATION_ADDRESS = jsonList.data[i].INSTALLATION_ADDRESS;
                    UPDATE_MULTIPLY_FACTOR = jsonList.data[i].MULTIPLY_FACT;
                    AMOUNT = jsonList.data[i].AMOUNT;
                    BRANCH_ID = jsonList.data[i].BRANCH_ID;
                    RATE = jsonList.data[i].RATE;

                }
                $('#INSTALLATION_TYPE').val(INSTALLATION_TYPE);
                $('#INSTALLATION_CODE').val(INSTALLATION_CODE);
                $('#STATUS_DINAS').val(STATUS_DINAS);
                $('#PROFIT_CENTER').val(PROFIT_CENTER);
                $('#CUSTOMER_MDM').val(CUSTOMER_MDM);
                $('#CUSTOMER_CODE').val(CUSTOMER_CODE);
                $('#CAPACITY_VA').val(CAPACITY_VA);
                $('#MIN_USED').val(MINIMUM_USED);
                $('#MIN_PAYMENT').val(MIN_PAYMENT);
                $('#BIAYA_BEBAN').val(BIAYA_BEBAN);
                $('#BIAYA_ADMIN').val(BIAYA_ADMIN);
                $('#INSTALLATION_ADDRESS').val(INSTALLATION_ADDRESS);
                $('#UPDATE_MULTIPLY_FACTOR').val(UPDATE_MULTIPLY_FACTOR);
                $('#KODE_CABANG').val(BRANCH_ID);

                $('#TOTAL_PAYMENT').val(AMOUNT);
                $('#TOTAL_PRICE').val(AMOUNT);
                if (RATE > 1) {
                    $('#TOTAL_USD').val(AMOUNT / RATE);
                    $('#RATE').val(RATE);
                } else {
                    $('#TOTAL_USD').val(AMOUNT / RATE);
                    $('#RATE').val(RATE);
                    $('#TOTAL_USD').hide();
                    $('#RATE').hide();
                    $('#USD').hide();
                }
                $('#TOTAL_PAYMENT').mask('000.000.000.000.000', { reverse: true });
                $('#TOTAL_PRICE').mask('000.000.000.000.000', { reverse: true });
                $('#TOTAL_USD').mask('000.000.000.000.000', { reverse: true });

            }
        });
    }

    var generateFormPricing = function () {
        var idInstalasi = getParameterByName("xId");
        var idTransaksi = getParameterByName("xTd");

        var valMeterFromLWBP = 0;
        var valMeterFromWBP = 0;
        var valMeterFromKVARH = 0;
        var valMeterFromBLOK1 = 0;
        var valMeterFromBlok2 = 0;


        var valMeterToLWBP = 0;
        var valMeterToWBP = 0;
        var valMeterToKVARH = 0;
        var valMeterToBLOK1 = 0;
        var valMterToBlok2 = 0;

        var idLWBP = 0;
        var idWBP = 0;
        var idKVARH = 0;
        var idBLOK1 = 0;
        var idBLOK2 = 0;

        var usedLWBP = 0;
        var usedWBP = 0;
        var usedKVARH = 0;
        var usedBLOK1 = 0;
        var usedBlok2 = 0;

        var ketLWBP = 0;
        var ketWBP = 0;
        var ketKVARH = 0;
        var ketBLOK1 = 0;
        var ketBlok2 = 0;

        // Jalankan ajax untuk ambil nilai transaksi bulan sebelumnya
        // Ambil nilai LWBP Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransWEList/getLastLWBPFrom",
            contentType: "application/json",
            dataType: "json",
            data: { id_transaksi: idTransaksi },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                var disableTextBoxBLOK2 = '';

                getCoaProduksi();
                for (var i = 0; i < jsonList.data.length; i++) {
                    var METER_FROM_LWBP = jsonList.data[i].METER_FROM;
                    var METER_TO_LWBP = jsonList.data[i].METER_TO;
                    var USED_LWBP = jsonList.data[i].USED;
                    var ID_LWBP = jsonList.data[i].ID;
                    var KET_LWBP = jsonList.data[i].KETERANGAN;
                }
                valMeterFromLWBP = METER_FROM_LWBP;
                valMeterToLWBP = METER_TO_LWBP;
                usedLWBP = USED_LWBP;
                idLWBP = ID_LWBP;
                ketLWBP = KET_LWBP;
            }
        });

        // Ambil nilai WBP Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransWEList/getLastWBPFrom",
            contentType: "application/json",
            dataType: "json",
            data: { id_transaksi: idTransaksi },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems1 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    var METER_FROM_WBP = jsonList.data[i].METER_FROM;
                    var METER_TO_WBP = jsonList.data[i].METER_TO;
                    var USED_WBP = jsonList.data[i].USED;
                    var ID_WBP = jsonList.data[i].ID;
                    var KET_WBP = jsonList.data[i].KETERANGAN;
                }
                valMeterFromWBP = METER_FROM_WBP;
                valMeterToWBP = METER_TO_WBP;
                usedWBP = USED_WBP;
                idWBP = ID_WBP;
                ketWBP = KET_WBP;
            }
        });


        // Ambil nilai KVARH Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransWEList/getLastKVARHFrom",
            contentType: "application/json",
            dataType: "json",
            data: { id_transaksi: idTransaksi },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems2 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    var METER_FROM_KVARH = jsonList.data[i].METER_FROM;
                    var METER_TO_KVARH = jsonList.data[i].METER_TO;
                    var USED_KVARH = jsonList.data[i].USED;
                    var ID_KVARH = jsonList.data[i].ID;
                    var KET_KVARH = jsonList.data[i].KETERANGAN;
                }
                valMeterFromKVARH = METER_FROM_KVARH;
                valMeterToKVARH = METER_TO_KVARH;
                usedKVARH = USED_KVARH;
                idKVARH = ID_KVARH;
                ketKVARH = KET_KVARH;
            }
        });


        // Ambil nilai BLOK1 Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransWEList/getLastBLOKSATUFrom",
            contentType: "application/json",
            dataType: "json",
            data: { id_transaksi: idTransaksi },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems2 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    var METER_FROM_BLOK1 = jsonList.data[i].METER_FROM;
                    var METER_TO_BLOK1 = jsonList.data[i].METER_TO;
                    var USED_BLOK1 = jsonList.data[i].USED;
                    var ID_BLOK1 = jsonList.data[i].ID;
                    var KET_BLOK1 = jsonList.data[i].KETERANGAN;
                }
                valMeterFromBLOK1 = METER_FROM_BLOK1;
                valMeterToBLOK1 = METER_TO_BLOK1;
                usedBLOK1 = USED_BLOK1;
                idBLOK1 = ID_BLOK1;
                ketBLOK1 = KET_BLOK1;
            }
        });

        // Ambil nilai BLOK2 Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransWEList/getLastBLOKDUAFrom",
            contentType: "application/json",
            dataType: "json",
            data: { id_transaksi: idTransaksi },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems2 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    var METER_FROM_BLOK2 = jsonList.data[i].METER_FROM;
                    var METER_TO_BLOK2 = jsonList.data[i].METER_TO;
                    var USED_BLOK2 = jsonList.data[i].USED;
                    var ID_BLOK2 = jsonList.data[i].ID;
                    var ket_blok2 = jsonList.data[i].KETERANGAN;
                }
                valMeterFromBLOK2 = METER_FROM_BLOK2;
                valMeterToBLOK2 = METER_TO_BLOK2;
                usedBLOK2 = USED_BLOK2;
                idBLOK2 = ID_BLOK2;
                ketblok2 = ket_blok2;
            }
        });
        // Akhir dari ajax untuk ambil nilai transaksi bulan sebelumnya


        var tablePricing = $('#table-pricing').DataTable();


        tablePricing.destroy();

        //Data Table Pricing 
        var tabler = $('#table-pricing');
        var oTable = tabler.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "bPaginate": false
        });

        //Cek transaksi berdasarkan id instalasi yg dipilih
        //ajax untuk cek apakah ada transaksi untuk id transaksi yg dipilih

        var buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';

        var meterFrom = '<input type="text" id="METER_FROM" name="METER_FROM" onchange="hitungUSEDFROM(this)" class="form-control">';
        var meterTo = '<input type="text" id="METER_TO" name="METER_TO" onchange="hitungUSEDTO(this)" class="form-control">';
        var used = '<input type="text" id="USED" name="USED" class="form-control" readonly>';

        var rOnlymeterFrom = '<input type="text" id="METER_FROM" name="METER_FROM" onchange="hitungUSEDFROM(this)" class="form-control" readonly>';
        var rOnlymeterTo = '<input type="text" id="METER_TO" name="METER_TO" onchange="hitungUSEDTO(this)" class="form-control" readonly>';
        var rOnlyused = '<input type="text" id="USED" name="USED" class="form-control" readonly>';
        var rOnlyIdTransaksi = '<input type="text" id="ID_TRANS" name="ID_TRANS" class="form-control" readonly>';
        //var coa_produksi = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
        var coa_produksi = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_produksi" name="coa_produksi" readonly></center>';
        var keterangan = '<input type="text" id="keterangan" name="keterangan" class="form-control" style="width:200px">';

        // Masking
        $("input[name=METER_FROM]").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });

        $("input[name=METER_TO]").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });

        //cari coa
        $('#table-pricing').on('click', 'tr #btn-coa', function () {
            console.log(111);
            $('#coaModal').modal('show');
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransElectricity/GetListCoaElectric",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.draw;
                        data.recordsTotal = response.recordsTotal;
                        data.recordsFiltered = response.recordsFiltered;
                        data.data = response.data;
                        return data.data;
                    },
                    cache: false,
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            console.log(value);

            var elems = $('#table-pricing').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                c_data = $('#table-pricing').DataTable().row(index).node();
                console.log(index, c_data)

                var elem = $(c_data).find('#coa_produksi');
                elem.val(value);

            });
            $('#coaModal').modal('hide');
        });

        $.ajax({
            type: "GET",
            url: "/TransElectricity/GetDataPricing/" + idInstalasi,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                var disableTextBoxBLOK2 = '';

                getCoaProduksi();

                for (var i = 0; i < jsonList.data.length; i++) {
                    mFactor = '<input type="text" id="MULTIPLY_FACT" value="' + jsonList.data[i].MULTIPLY_FACT + '" name="M_FACTOR" class="form-control" readonly>';
                    
                    if (jsonList.data[i].PRICE_TYPE == 'BLOK2') {
                        oTable.fnAddData([jsonList.data[i].PRICE_TYPE, jsonList.data[i].PRICE_CODE, jsonList.data[i].AMOUNT, jsonList.data[i].CURRENCY, rOnlymeterFrom, rOnlymeterTo, rOnlyused, mFactor, jsonList.data[i].MAX_RANGE_USED, rOnlyIdTransaksi, coa_produksi, keterangan]);
                    }
                    else {
                        //oTable.fnAddData([jsonList.data[i].PRICE_TYPE, jsonList.data[i].PRICE_CODE, jsonList.data[i].AMOUNT, meterFrom, meterTo, used, mFactor, jsonList.data[i].MAX_RANGE_USED, rOnlyIdTransaksi, coa_produksi, keterangan]);
                        oTable.fnAddData([jsonList.data[i].PRICE_TYPE, jsonList.data[i].PRICE_CODE, jsonList.data[i].AMOUNT, jsonList.data[i].CURRENCY, meterFrom, meterTo, used, mFactor, jsonList.data[i].MAX_RANGE_USED, rOnlyIdTransaksi ,coa_produksi, keterangan]);
                    }
                }

                //LWBP
                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (valMeterFromLWBP == 'null') {
                        var assignNolLWBP = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolLWBP);
                        console.log('kvarh 0 ' + assignNolLWBP);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromLWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (valMeterToLWBP == 'null') {
                        var assignNolLWBP = 0;
                        $(this).find("td:eq(5) input[type='text']").val(assignNolLWBP);
                        console.log('lwbp nol meter to ' + assignNolLWBP);
                    }
                    else {
                        $(this).find("td:eq(5) input[type='text']").val(valMeterToLWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (usedLWBP == 'null') {
                        var assignNolLWBP = 0;
                        $(this).find("td:eq(6) input[type='text']").val(assignNolLWBP);
                        console.log('lwbp nol meter to ' + assignNolLWBP);
                    }
                    else {
                        $(this).find("td:eq(6) input[type='text']").val(usedLWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (idLWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(9) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(9) input[type='text']").val(idLWBP);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (ketLWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(11) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(11) input[type='text']").val(ketLWBP);
                    }
                });

                //KVARH
                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (valMeterFromKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromKVARH);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (valMeterToKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(5) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(5) input[type='text']").val(valMeterToKVARH);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (usedKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(6) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(6) input[type='text']").val(usedKVARH);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (idKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(9) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(9) input[type='text']").val(idKVARH);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (ketKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(11) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(11) input[type='text']").val(ketKVARH);
                    }
                });


                //WBP FROM
                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (valMeterFromWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolWBP);
                        console.log('wbp 0 ' + assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (valMeterToWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(5) input[type='text']").val(assignNolWBP);
                        console.log('wbp 0 ' + assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(5) input[type='text']").val(valMeterToWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (usedWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(6) input[type='text']").val(assignNolWBP);
                        console.log('wbp 0 ' + assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(6) input[type='text']").val(usedWBP);
                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (idWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(9) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(9) input[type='text']").val(idWBP);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (ketWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(11) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(11) input[type='text']").val(ketWBP);
                    }
                });

                //BLOK1
                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (valMeterFromBLOK1 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromBLOK1);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (valMeterToBLOK1 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(5) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(5) input[type='text']").val(valMeterToBLOK1);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (usedBLOK1 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(6) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(6) input[type='text']").val(usedBLOK1);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (idBLOK1 == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(9) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(9) input[type='text']").val(idBLOK1);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (ketBLOK1 == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(11) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(11) input[type='text']").val(ketBLOK1);
                    }
                });

                //BLOK2
                $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
                    if (valMeterFromBLOK2 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromBLOK2);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
                    if (valMeterToBLOK2 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(5) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(5) input[type='text']").val(valMeterToBLOK2);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
                    if (usedBLOK2 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(6) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(6) input[type='text']").val(usedBLOK2);

                    }
                });
                $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
                    if (idBLOK2 == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(9) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(9) input[type='text']").val(idBLOK2);
                    }
                });

                $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
                    if (ketBlok2 == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(11) input[type='text']").val(assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(11) input[type='text']").val(ketBlok2);
                    }
                });
            }
        });
    }

    generateFormCosting = function () {
        var idInstalasix = getParameterByName("xId");

        var tableCosting = $('#table-costing').DataTable();
        tableCosting.destroy();

        //Data Table Costing 
        var tablec = $('#table-costing');
        var cTable = tablec.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "bPaginate": false
        });

        $.ajax({
            type: "GET",
            url: "/TransElectricity/GetDataCosting/" + idInstalasix,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                cTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    cTable.fnAddData([jsonList.data[i].DESCRIPTION, jsonList.data[i].PERCENTAGE]);
                }
            }
        });
    }

    var simpanUpdate = function () {
        $('#btn-simpan-update').click(function () {
            var ID_TRANSAKSI = $('#TRANSACTION_ID').val();

            var ID_INSTALASI = $('#INSTALLATION_CODE').val();
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            var CUSTOMER = $('#CUSTOMER_MDM').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var PERIOD = $('#PERIOD').val();
            var STATUS_DINAS = $('#STATUS_DINAS').val();
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var PRICE = $('#PRICE').val();
            var MULTIPLY_FACTOR = $('#UPDATE_MULTIPLY_FACTOR').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#TOTAL_PAYMENT').val();
            var RATE = $('#RATE').val();
            var UNIT = 'KWH';
            var INSTALLATION_CODE = $('#INSTALLATION_CODE').val();

            var INSTALLATION_NUMBER = $('#INSTALLATION_NUMBER').val();

            var raw_profit_center = $('#PROFIT_CENTER').val();
            var pecahRawPC = raw_profit_center.split(" - ");
            var PROFIT_CENTER = pecahRawPC[0];

            // PENGECEKAN VALID_FROM VALID_TO DAN AMOUNT
            var uWBP = 0;
            var uLWBP = 0;
            var uKVARH = 0;
            var uBlok1 = 0;

            //USED LWBP
            $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                uLWBP = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                uWBP = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                uKVARH = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                uBlok1 = $(this).find("td:eq(5) input[type='text']").val();
            });

            var mFac = $('#UPDATE_MULTIPLY_FACTOR').val();

            var kaliWBP = Number(uLWBP) * Number(mFac);
            var kaliLWBP = Number(uLWBP) * Number(mFac);
            var kaliKVARH = Number(uKVARH) * Number(mFac);
            var totalK = Number(kaliWBP) + Number(kaliLWBP) + Number(kaliKVARH);


            // variable nilaiAmount masuk ke QUANTITY
            var nilaiAMOUNT = 0;
            if (uBlok1 == 0) {
                nilaiAMOUNT = totalK;
            }
            else {
                nilaiAMOUNT = uBlok1;
            }
            console.log('Nilai AMOUNT ' + nilaiAMOUNT);


            if (AMOUNT) {
                var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                var param = {
                    MULTIPLY_FACTOR: MULTIPLY_FACTOR,
                    ID_TRANSAKSI: ID_TRANSAKSI,
                    ID_INSTALASI: ID_INSTALASI,
                    AMOUNT: nfAmount.toString(),
                    QUANTITY: nilaiAMOUNT.toString(),
                    PERIOD: PERIOD,
                    RATE: RATE,
                    STATUS_DINAS: STATUS_DINAS
                };
                console.log(param);
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransElectricity/UpdateDataHeader"
                });

                req.done(function (data) {
                    var kode_transaksi = data.be_number;
                    //BEGIN LOOPING SAVE DATA DETAIL

                    arrpricing = new Array();
                    arrcoaprodkosong = new Array();

                    // Datatable Pricing
                    var DTPricing = $('#table-pricing').dataTable();
                    var countDTPricing = DTPricing.fnGetNodes();

                    var DTGelondongan = $('#table-pricing').dataTable();
                    var all_row = DTGelondongan.fnGetNodes();

                    var paramDTPricing = {};
                    for (var i = 0; i < countDTPricing.length; i++) {
                        var itemDTPricing = DTPricing.fnGetData(i);
                        var all_row = DTGelondongan.fnGetNodes();
                        paramDTPricing = {
                            ID_TRANSAKSI: ID_TRANSAKSI,
                            PRICE_TYPE: itemDTPricing[0],
                            PRICE_CODE: itemDTPricing[1],
                            TARIFF: itemDTPricing[2],
                            METER_FROM: $(all_row[i]).find('input[name="METER_FROM"]').val(),
                            METER_TO: $(all_row[i]).find('input[name="METER_TO"]').val(),
                            USED: $(all_row[i]).find('input[name="USED"]').val(),
                            MULTIPLY: $(all_row[i]).find('input[name="M_FACTOR"]').val(),
                            INSTALLATION_CODE: INSTALLATION_CODE,
                            ID_EDIT: $(all_row[i]).find('input[name="ID_TRANS"]').val(),
                            //COA_PROD: $(all_row[i]).find('select[name="coa_prod"]').val(),
                            COA_PROD: $(all_row[i]).find('input[name="coa_produksi"]').val(),
                            KETERANGAN: $(all_row[i]).find('input[name="keterangan"]').val()
                        }
                        arrpricing.push(paramDTPricing);
                        //console.log(paramDTPricing);


                        // Ajax untuk save detail dari datatable manual-frequency
                        //$.ajax({
                        //    contentType: "application/json",
                        //    data: JSON.stringify(paramDTPricing),
                        //    method: "POST",
                        //    url: "/TransElectricity/UpdateDetailPricing",
                        //    success: function (data) {
                        //        console.log('save detail pricing success');
                        //    }
                        //});
                    }
                    // Save Pricing

                    //END OF LOOPING SAVE DATA
                    for (var i = 0; i < arrpricing.length; i++) {

                        console.log("COA ARRAY : " + arrpricing[i].COA_PROD);
                        if (arrpricing[i].COA_PROD == null || arrpricing[i].COA_PROD == '') {
                            arrcoaprodkosong.push("Y");
                        } else {
                            arrcoaprodkosong.push("T");
                        }
                    }

                    var found = arrcoaprodkosong.find(function (element) {
                        return element === "Y";
                    });

                    if (typeof (found) !== "undefined" && found !== null) {
                        swal('Warning', 'Mohon lengkapi Entrian', 'warning');
                        console.log("COA PRODUKSI ADA YANG KOSONG");
                    } else {
                        console.log("COA PRODUKSI SUDAH TERISI");
                        for (var i = 0 ; i < arrpricing.length ; i++) {
                            $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(arrpricing[i]),
                                method: "POST",
                                url: "/TransElectricity/UpdateDetailPricing",
                                success: function (data) {
                                    console.log('save detail pricing success');
                                }
                            });
                        }
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransWEList";
                            });
                        }
                    }

                    App.unblockUI();
                    //if (data.status === 'S') {
                    //    swal('Success', data.message, 'success').then(function (isConfirm) {
                    //          window.location = "/TransWEList"; 
                    //      });
                    //  } else {
                    //    swal('Failed', data.message, 'error').then(function (isConfirm) {
                    //          window.location = "/TransWEList";
                    //      });
                    //  }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var customer = function () {
        setTimeout(function(){
            var cekCustomerId = $('#CUSTOMER_CODE').val();
            if (cekCustomerId == '0210012143') {
                $('#USED').after("<a data-toggle='modal' href='#viewModalListrik' class='btn btn-icon-only green' id='btn-detail-listrik'><i class='fa fa-pencil'></i></a>");
                
            }
        }, 3000)
    }

    var editDetail = function(){
        var $line = $('.detail-penggunaan');
        $('#btn-add').on('click', function() {
            $line.after('<div class="portlet-body detail-penggunaan">'+
                '<div class="row">'+
                    '<div class="col-md-4">'+
                        '<h4>Sumber</h4>'+
                        '<select class="form-control sumber" id="sumber">'+
                            '<option value="">--Pilih Sumber--</option>'+
                            '<option value="PLN">PLN</option>'+
                            '<option value="PLTMG">PLTMG</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>Price Type</h4>'+
                        '<select class="form-control type" id="type">'+
                            '<option value="">--Pilih Price Type--</option>'+
                            '<option value="LWBP">LWBP</option>'+
                            '<option value="WBP">WBP</option>'+
                            '<option value="KVARH">KVARH</option>'+
                            '<option value="BLOK1">BLOK1</option>'+
                            '<option value="BLOK2">BLOK2</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>Faktor Pengali</h4>'+
                        '<input type="number" id="pengali" class="form-control pengali"/>'+
                    '</div>'+
                '</div>'+
                '<br>'+
                '<div class="row">'+
                    '<div class="col-md-3">'+
                        '<h4>Tgl Stan Awal</h4>'+
                        '<input type="date" id="tglStanAwal" class="form-control tglStanAwal"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Tgl Stan Akhir</h4>'+
                        '<input type="date" id="tglStanAkhir" class="form-control tglStanAkhir"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Stan Awal</h4>'+
                        '<input type="number" id="stanAwal" class="form-control stanAwal"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Stan Akhir</h4>'+
                        '<input type="number" id="stanAkhir" class="form-control stanAkhir"/>'+
                    '</div>'+
                '</div>'+
                '<br>'+
                '<div class="row">'+
                    '<div class="col-md-4">'+
                        '<h4>Selisih</h4>'+
                        '<input type="text" id="selisih" class="form-control selisih" readonly/>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>KWH Tenan</h4>'+
                        '<input type="number" id="kwhTenan" class="form-control kwhTenan"/>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>KWH Ditagihkan</h4>'+
                        '<input type="text" id="kwhDitagihkan" class="form-control kwhDitagihkan" readonly/>'+
                    '</div>'+
                '</div>'+
                '<hr>'+
                '<br>'+
            '</div>');
        })

    }

    $(document).on('change','.pengali', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });

    $(document).on('change','.stanAwal', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });

    $(document).on('change','.stanAkhir', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });
    
    $(document).on('change','.kwhTenan', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();
        
        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        console.log(ditagihkan);
        
        if (tenan != '') {
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }else{
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        }

    });

    $(document).on('change','.kwhDitagihkan', function (e) {
        var total = 0;
        $('.kwhDitagihkan').each(function() {
            total += parseFloat($(this).val());
        })
        $('.total').val(total);
    });
    
    $('#btn-save').click(function(){
        var id = $('#idTrans').val();
        var sumber = $('.sumber').val();
        var type = $('.type').val();
        var pengali = $('.pengali').val();
        var tgl_stan_awal = $('.tglStanAwal').val();
        var tgl_stan_akhir = $('.tglStanAkhir').val();
        var stan_awal = $('.stanAwal').val();
        var stan_akhir = $('.stanAkhir').val();
        var selisih = $('.selisih').val();
        var kwh_tenan = $('.kwhTenan').val();
        var kwh_ditagihkan = $('.kwhDitagihkan').val();
        var total = $('.total').val();
        
        $('#viewModalListrik').modal('hide');
        swal({
            title: 'Warning',
            text: "Are you sure want to save Detail Used ?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {

                var param = {
                    ID : id,
                    SUMBER : sumber,
                    TYPE : type,
                    PENGALI : pengali,
                    TGL_STAN_AWAL : tgl_stan_awal,
                    TGL_STAN_AKHIR : tgl_stan_akhir,
                    STAN_AWAL : stan_awal,
                    STAN_AKHIR : stan_akhir,
                    SELISIH : selisih,
                    KWH_TENAN : kwh_tenan,
                    KWH_DITAGIHKAN : kwh_ditagihkan,
                    TOTAL : total
                };
               
            }
        });

    })

    return {
        init: function () {
            batal();
            //hitung();
            simpanUpdate();
            generateFormPricing();
            generateFormCosting();
            ambilHeader();
            customer();
            editDetail();
        }
    };
}();

function hitungUSEDTO(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#table-pricing').DataTable();
    var data = table.row(baris).data();

    var meter_from = $(this_cmb).parents('tr').find('input[name="METER_FROM"]').val();
    var meter_to = $(this_cmb).val();
    var nilai_used = Number(meter_to) - Number(meter_from);
    var used = $(this_cmb).parents('tr').find('input[name="USED"]').val(nilai_used);
}

function hitungUSEDFROM(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#table-pricing').DataTable();
    var data = table.row(baris).data();

    var meter_from = $(this_cmb).val();
    var meter_to = $(this_cmb).parents('tr').find('input[name="METER_TO"]').val();
    var nilai_used = Number(meter_to) - Number(meter_from);
    var used = $(this_cmb).parents('tr').find('input[name="USED"]').val(nilai_used);
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricityAdd.init();

        $.extend($.expr[":"], {
            containsExact: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 return function (elem) {
                     return $.trim(elem.innerHTML.toLowerCase()) === text.toLowerCase();
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 return $.trim(elem.innerHTML.toLowerCase()) === match[3].toLowerCase();
             },

            containsExactCase: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 return function (elem) {
                     return $.trim(elem.innerHTML) === text;
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 return $.trim(elem.innerHTML) === match[3];
             },

            containsRegex: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 var reg = /^\/((?:\\\/|[^\/]) )\/([mig]{0,3})$/.exec(text);
                 return function (elem) {
                     return RegExp(reg[1], reg[2]).test($.trim(elem.innerHTML));
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 var reg = /^\/((?:\\\/|[^\/]) )\/([mig]{0,3})$/.exec(match[3]);
                 return RegExp(reg[1], reg[2]).test($.trim(elem.innerHTML));
             }

        });
        // get value from url parameter
        $("#INSTALLATION_ID").val(getParameterByName("xId"));
        $("#TRANSACTION_ID").val(getParameterByName("xTd"));

    });
}


function gantiMultiply(this_cmb) {
    var nilaiMultiply = $(this_cmb).val();
    $('input[name="M_FACTOR"]').val(nilaiMultiply);

}


function hitungFormula() {

    // GET DATA PRICING ---------------------------------------------------
    $('#TOTAL_PRICE').val('');
    $('#TOTAL_PAYMENT').val('');

    biayaBeban = $('#BIAYA_BEBAN').val();
    biayaAdmin = $('#BIAYA_ADMIN').val();
    tagMin = $('#MIN_PAYMENT').val();
    capacityVA = $('#CAPACITY_VA').val();

    multiplyFactor = $('#UPDATE_MULTIPLY_FACTOR').val();

    //LWBP
    $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
        //alert($(this).find("td:last-child").html());
        //alert($(this).find('td:eq(2)').html());
        tarifLWBP = $(this).find('td:eq(2)').html();
        usedLWBP = $(this).find("td:eq(6) input[type='text']").val();

        var raw_multiplyFactor = $(this).find("td:eq(7) input[type='text']").val();
        $('#MULTIPLY_FACTOR').val(raw_multiplyFactor);
        /*
        multiplyFactor = $(this).find('td:eq(6)').html();
        $('#MULTIPLY_FACTOR').val(multiplyFactor);
        */

    });

    //KVARH
    $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
        tarifKVARH = $(this).find('td:eq(2)').html();
        usedKVARH = $(this).find("td:eq(6) input[type='text']").val();
    });


    //WBP
    $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
        tarifWBP = $(this).find('td:eq(2)').html();
        usedWBP = $(this).find("td:eq(6) input[type='text']").val();
    });

    //BLOK1
    $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
        // tarifBLOK1 = $(this).find('td:eq(2)').html();
        tarifBLOK1 = $(this).find('td:eq(2)').html();
        usedBLOK1 = $(this).find("td:eq(6) input[type='text']").val();
        maxRangeUsedBLOk1 = $(this).find('td:eq(8)').html();
        //console.log('max range used blok1 ' + maxRangeUsedBLOk1);
    });

    //BLOK2
    $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
        tarifBLOK2 = $(this).find('td:eq(2)').html();
        usedBLOK2 = $(this).find("td:eq(6) input[type='text']").val();
        maxRangeUsedBLOK2 = $(this).find('td:eq(8)').html();
    });


    // GET DATA COSTING -----------------------------------------------------
    // Persen PPJU
    $("#table-costing tbody tr:has(td:containsExact('PPJU'))").each(function () {
        persenPPJU = $(this).find('td:eq(1)').html();
    });

    // Persen REDUKSI
    $("#table-costing tbody tr:has(td:containsExact('REDUKSI'))").each(function () {
        persenREDUKSI = $(this).find('td:eq(1)').html();
    });

    // Perhitungan Sesuai Dengan Kode Cabang
    kodeCabang = $('#KODE_CABANG').val();
    minKWH = $('#MIN_USED').val();
    var daya = Number(capacityVA);

    totalTagihan = 0;

    // 1. Perhitungan Untuk Kode Cabang TANJUNG PERAK
    if (kodeCabang == 2 || kodeCabang == 86) { //2
        // Kondisi Pertama
        var cekPertama = (Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor));
        var cekKedua = ((Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor))) * 0.62;

        console.log('usedlwbp : ' + usedLWBP);
        console.log('multiplyfactor : ' + multiplyFactor);
        console.log('usedWBP : ' + usedWBP);
        console.log('minimal kwh : ' + minKWH);
        console.log('persen Reduksi : ' + persenREDUKSI);
        console.log('usedkvarh : ' + usedKVARH);

        console.log('CEK PERTAMA ' + cekPertama);
        console.log('CEK KEDUA ' + cekKedua);
        // Untuk pengecekan kondisi 
        var kvarhKaliMfact = Number(usedKVARH) * Number(multiplyFactor);
        console.log('kvarhKaliMfact = ' + kvarhKaliMfact);

        //Kondisi Pertama Untuk Cabang Perak
        if (Number(cekPertama) <= Number(minKWH)) {
            //Tagihan = Minkwh * (tarifLWBP + tarifWBP) + Minkwh * (tarifLWBP + tarifWBP) * PPJU / 100 - (tagihan * reduksi / 100)
            // var totalTagihana = Number(minKWH) * (Number(tarifLWBP) + Number(tarifWBP)); rumus lama
            var totalTagihana = Number(minKWH) * Number(tarifLWBP); //rumus baru edited deden 8 januari 2019
            var totalTagihana1 = Number(totalTagihana) + (Number(totalTagihana) * Number(persenPPJU) / 100);
            totalTagihan = Math.round(Number(totalTagihana1) - (Number(totalTagihana1) * Number(persenREDUKSI) / 100));

            console.log('Persen PPJU : ' + persenPPJU);
            console.log('total tagihana : ' + minKWH + ' * ' + '( ' + tarifLWBP + ' + ' + +tarifWBP + ' ) ' + ' = ' + totalTagihana);
            console.log('total tagihana1 : ' + totalTagihana1);
            console.log('rumus perak pertama ' + totalTagihan);

        }
        if (Number(kvarhKaliMfact) < Number(cekKedua) && Number(cekPertama) > Number(minKWH)) {
            var totalTagihan1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + (Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP));
            var totalTagihan2 = Number(totalTagihan1) + (Number(totalTagihan1) * Number(persenPPJU) / 100);
            totalTagihan = Math.round(Number(totalTagihan2) - (Number(totalTagihan2) * Number(persenREDUKSI) / 100));

            console.log('rumus perak kedua ' + totalTagihan);


        }
        if (Number(kvarhKaliMfact) > Number(cekKedua) && Number(cekPertama) > Number(minKWH)) {
            var x = ((Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor))) * 0.62;
            console.log('x =  ' + x);
            var y = (Number(usedKVARH) * Number(multiplyFactor));
            console.log('Y =  ' + y);
            var x1 = Number(y) - Number(x);
            console.log('X1 =  ' + x1);
            var hitung1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + (Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP));
            //tagihan= tagihan + (((qty(kVARH)*faktor kali) - ((qty(LWBP)*faktorkali) + (qty(WBP)*faktor kali))*62%)) * tarifkVARH)
            console.log('Hitung1 =  ' + hitung1);
            var hitung2 = Number(x1) * Number(tarifKVARH);
            console.log('Hitung2 =  ' + hitung2);
            //tagihan= tagihan + (tagihan*ppj/100) - (tagihan*reduksi/100)
            var hitung3 = Number(hitung1) + Number(hitung2);
            console.log('Hitung3 =  ' + hitung3);
            var hitung4 = Number(hitung3) + (Number(hitung3) * Number(persenPPJU) / 100);
            console.log('Hitung4 =  ' + hitung4);

            totalTagihan = Math.round(Number(hitung4) - (Number(hitung4) * Number(persenREDUKSI) / 100));


            //console.log('used lwbp+wbp ' + x);
            //console.log('used kvarh' + y);
            //console.log('used kvarh-lwbp+wbp' + x1);
            //console.log('hitung1 ' + hitung1);
            //console.log('hitung2 ' + hitung2);
            //console.log('hitung3 ' + hitung3);
            //console.log('hitung4 ' + hitung4);

            console.log('rumus perak ketiga ' + totalTagihan);
        }
    }

    // Khusus BENOA yg sebelumnya rame2 skrg dipisah
    if (kodeCabang == 12 || kodeCabang == 24) {
        var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
        var t2 = Number(t1) + Number(biayaBeban);
        var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
        var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
        totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));
    }

    // Khusus SEMARANG yg sebelumnya rame2 skrg dipisah
    if (kodeCabang == 9 || kodeprofitcenter == 10201) {
        console.log('RUMUS SEMARANG JALAN');
        var kondisiSemarang = Number(usedLWBP) * Number(multiplyFactor);
        var minPaymentSemarang = Number(tagMin);
        var powerCapacitySemarang = Number(capacityVA);
        var t1 = (Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(tarifWBP) * Number(multiplyFactor));

        if (t1 <= minPaymentSemarang && powerCapacitySemarang > 900) {
            console.log('SEMARANG 1');
            var t2 = Number(minPaymentSemarang) + Number(biayaBeban); //+ Number(biayaAdmin)
            var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
            totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
        }
        else if (t1 > minPaymentSemarang && powerCapacitySemarang > 900) {
            console.log('SEMARANG 2');
            var t2 = Number(t1) + Number(biayaBeban); //+ Number(biayaAdmin)
            var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
            totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
        }
        else if (powerCapacitySemarang == 450) {
            console.log('SEMARANG 3');
            var pemakaianBlok1 = usedBLOK1;
            var maxRangeBlok1 = maxRangeUsedBLOk1;
            console.log('pemakaian blok ' + pemakaianBlok1);
            console.log('max range blok ' + maxRangeUsedBLOk1);

            if (pemakaianBlok1 <= maxRangeBlok1) {
                console.log('SEMARANG 4');
                var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                var kali1 = Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                totalTagihan = Math.round(Number(kali2));

                console.log('hitung blok 1 ' + hitungBlok1);
                console.log('tambah biaya beban ' + tambahBiayaBeban);
                console.log('kali 1 ' + kali1);
                console.log('kali 2 ' + kali2);
                console.log('total tagihan ' + totalTagihan);

                console.log('rumus 2 blok1 semarang');
            } else {
                console.log('SEMARANG 5');
                var hitungBlok1 = Number(maxRangeBlok1) * Number(tarifBLOK1);
                var hitungBlok2 = (Number(usedBLOK1) - Number(maxRangeBlok1)) * Number(tarifBLOK2);

                var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                var kali1 = Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                totalTagihan = Math.round(Number(kali2));

                console.log('hitung blok 2.1 Semarang ' + hitungBlok1);
                console.log('hitung blok 2.2 semarang ' + hitungBlok2);
                console.log('sum blok ' + sumBlok);
                console.log('tambah biaya beban ' + tambahBiayaBeban);
                console.log('kali 1 ' + kali1);
                console.log('kali 2' + kali2);
                console.log('total tagihan blok semarang ' + totalTagihan);
                console.log('rumus 2 blok2 semarang');
            }
        }
        else if (powerCapacitySemarang == 900) {
            console.log('SEMARANG 6');
            var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
            var t2 = Number(t1) + Number(biayaBeban);
            var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
            var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
            totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));

            console.log('pemakaian ' + t1);
            console.log('pemakaian + biaya beban ' + t2);
            console.log('sudah tambah ppj ' + t3);
        }
        else {

        }
        /*
        if (kondisiSemarang <= minKWH) {
            var t1 = Number(minKWH) * Number(tarifLWBP);
            var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
            var t3 = Number(t2) + Number(biayaBeban);
            var t4 = Number(t3) + Number(biayaAdmin) + Number(tagMin);

            totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));
            console.log('sermarang 1');
        }
        else if (kondisiSemarang > minKWH) {
            var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
            var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
            var t3 = Number(t2) + Number(biayaBeban);
            var t4 = Number(t3) + Number(biayaAdmin) + Number(tagMin);

            totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));
            console.log('sermarang 2');

            console.log('rumus 2 blok2');
        }
        else {

        }
        */
    }

    // Bima
    if (kodeCabang == 17) {
        var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
        var t2 = (1.25 * Number(t1)) + Number(biayaBeban);
        var t3 = Number(t2) + (Number(t1) * Number(persenPPJU) / 100);
        totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
    }

    // 2. CABANG RAME2 //kodeCabang == 12 ||
    if (kodeCabang == 18 || kodeCabang == 3 || kodeCabang == 25 || kodeCabang == 31 || kodeCabang == 29 || kodeCabang == 7 || kodeCabang == 11) {
        console.log('RUMUS TG INTAN JALAN');
        var kondisiRame = Number(usedLWBP) * Number(multiplyFactor);
        if (kondisiRame <= minKWH) {
            /*
            RUMUS
            tagihan=minkwh*tarifLWBP + beban
            tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
            tagihan = tagihan - (tagihan*reduksi/100)
            */
            console.log('INTAN 1');
            var t1 = Number(minKWH) * Number(tarifLWBP);
            var t2 = Number(t1) + Number(biayaBeban);
            var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
            var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
            totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));

        }
        else if (kondisiRame > minKWH) {
            /*
            tagihan=qty(LWBP)*faktorkali*tarifLWBP + beban
            tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
            tagihan = tagihan - (tagihan*reduksi/100)
            */
            console.log('INTAN 2');
            var t1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + Number(biayaBeban);
            console.log('T1 : ' + t1);
            console.log('BIAYA BEBAN : ' + biayaBeban);
            var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
            console.log('T2 : ' + t2);
            var t3 = Number(t2) + Number(tagMin); //Number(biayaAdmin) +
            console.log('T3 : ' + t3);
            totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
          
        }
        else if (kodeCabang == 7 && daya < 1300) {
            // Skema Grading untuk tanjung wangi 2
            //cek pemakain kwh apakah lebih besar dari maxrangeusedblok1, jika tidak maka langsung hitung tagihan
            //kalau lebih besar maka tagihan dikenakan tarif blok1 dan blok2 sesuai meternya
            // Skema Grading
            console.log('INTAN 3');
            var pemakaianBlok1 = usedBLOK1;
            var maxRangeBlok1 = maxRangeUsedBLOk1;

            if (Number(pemakaianBlok1) <= Number(maxRangeBlok1)) {
                var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                var kali1 = 1.25 * Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                totalTagihan = Math.round(Number(kali2));

                console.log('rumus 2 blok1');
            } else {
                var hitungBlok1 = Number(maxRangeUsedBLOk1) * Number(tarifBLOK1);
                var hitungBlok2 = (Number(usedBLOK1) - Number(maxRangeUsedBLOK1)) * Number(tarifBLOK2);

                var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                var kali1 = 1.25 * Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                totalTagihan = Math.round(Number(kali2));

                console.log('rumus 2 blok2');
            }
        }
        else {
            // do nothing
        }
    }


    // 3. CABANG MAUMERE
    if (kodeCabang == 21) {
        var kondisiMaumere = Number(usedLWBP) * Number(multiplyFactor);
        //console.log('k mau ' +kondisiMaumere);
        if (kondisiMaumere == 0) {
            //tagihan=1.25*(1.1*beban)
            var totTag = 1.25 * (1.1 * Number(biayaBeban));
            totalTagihan = Math.round(totTag);
            console.log('biaya beban mau' + biayaBeban);
        }
        else if (kondisiMaumere != 0 && (kondisiMaumere <= minKWH)) {
            /*
            if QTY LWBP USED*Multiply Factor != 0 and QTY LWBP USED*Multiply Factor < MIN USED
            tagihan=(1,25*minkwh* tarifLWBP) + (minkwh* tarifLWBP* PPJU/100) + beban
            */
            var t1 = 1.25 * Number(minKWH) * Number(tarifLWBP);
            var t2 = Number(minKWH) * Number(tarifLWBP) * ((persenPPJU) / 100);
            totalTagihan = Math.round(Number(t1) + Number(t2) + Number(biayaBeban));

        }
        else if (kondisiMaumere > minKWH) {
            /*
            if QTY LWBP USED*Multiply Factor > MIN USED
            tagihan=(1,25*qty(LWBP)*faktorkali* tarifLWBP) + (qty(LWBP)*faktorkali * tarifLWBP*PPJU/100) + beban
            */
            var t1 = 1.25 * Number(usedLWBP) * Number(tarifLWBP);
            var t2 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) * (Number(persenPPJU) / 100);
            totalTagihan = Math.round(Number(t1) + Number(t2) + Number(biayaBeban));

        }
        else {
            console.log('else cabang maumere');
        }
    }

    // 4. CABANG LEMBAR 
    var lembarSatuCond = Number(usedLWBP) * Number(multiplyFactor);
    var lembarDuaCond = (Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor));

    if (kodeCabang == 15) { //lembar itu 15 bukan 24
        if (daya >= 1300 && daya <= 200000) {   //1300 <= daya <= 200000
            // if QTY LWBP USED*Multiply Factor &lt; MIN USED
            if (Number(lembarSatuCond) <= Number(minKWH)) {
                // Lembar 1.1
                // tagihan=(1.25*(minkwh*tarifLWBP+beban)) + ((minkwh*tarifLWBP+beban)*PPJU/100)
                var t1 = 1.25 * ((Number(minKWH) * Number(tarifLWBP)) + Number(biayaBeban));
                var t2 = (Number(minKWH) * Number(tarifLWBP) + Number(biayaBeban)) * (Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(t1) + Number(t2));

            }
            else if (Number(lembarSatuCond) > Number(minKWH)) {
                // Lembar 1.2
                // tagihan=(1.25*(qty(LWBP)*faktorkali*tarifLWBP+beban)) + ((qty(LWBP)*faktorkali*tarifLWBP+beban)*PPJU/100)
                var t1 = 1.25 * (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP) + Number(biayaBeban));
                var t2 = ((Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + Number(biayaBeban)) * (Number(persenPPJU) / 100);

                totalTagihan = Math.round(Number(t1) + Number(t2));

            } else {
                // Gak ngapa2in
            }
        }
        else if (daya > 200000) {
            if (Number(lembarDuaCond) <= Number(minKWH)) {
                var t1 = Number(minKWH) * Number(tarifWBP);
                var t2 = (Number(minKWH) * Number(tarifWBP)) * Number(persenPPJU) / 100;
                var t3 = Number(t1) + Number(t2);
                totalTagihan = Math.round(Number(t3) + (Number(t3) * 0.25));

            }
            else if (Number(lembarDuaCond) > Number(minKWH)) {
                var t1 = Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP);
                var t2 = Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP);
                //var t3 = Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP);
                //var t4 = Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP)
                var t3 = Number(t1) + Number(t2); //+ Number(t3) + Number(t4);
                var t4 = Number(t3) * Number(persenPPJU) / 100;
                var t5 = (Number(t3) + Number(t4)) * 0.25;
                var t6 = Number(t3) + Number(t4) + Number(t5);

                console.log('t1 ' + t1);
                console.log('t2 ' + t2);
                console.log('t3' + t3);
                console.log('t4 ' + t4);

                totalTagihan = Math.round(Number(t6));

            }
            else {
                // do nothing
            }

        }
        else if (daya < 1300) {
            // Skema Grading
            var pemakaianBlok1 = usedBLOK1;
            var maxRangeBlok1 = maxRangeUsedBLOk1;
            console.log('max used blok ' + maxRangeBlok1);
            console.log('used blok 1 ' + pemakaianBlok1);

            if (Number(pemakaianBlok1) <= Number(maxRangeBlok1)) {
                console.log('PERHITUNGAN USED KONDISI PERTAMA');
                var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                var kali1 = 1.25 * Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));

                console.log('hitungb1 ' + hitungBlok1);
                console.log('tambahbeban ' + tambahBiayaBeban);
                console.log('kali1 ' + kali1);
                console.log('ppju ' + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100)));

                totalTagihan = Math.round(Number(kali2));
            }
            else if (Number(pemakaianBlok1) > Number(maxRangeUsedBLOk1)) {
                var x = maxRangeUsedBLOk1;
                var hitungBlok1 = Number(maxRangeUsedBLOk1) * Number(tarifBLOK1);
                var hitungBlok2 = (Number(usedBLOK1) - Number(x)) * Number(tarifBLOK2);

                var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                var kali1 = 1.25 * Number(tambahBiayaBeban);
                var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                totalTagihan = Math.round(Number(kali2));

                console.log('sum blok ' + sumBlok);
                console.log('tambah biaya beban ' + tambahBiayaBeban);
                console.log('kali1 ' + kali1);
                console.log('kali 2 ' + kali2);
                console.log('total tagihan ' + totalTagihan);

                console.log('perhitungan blok 2')
            }
            else {

            }
        }
        else {
            // do nothing
        }
    }



    // CABANG TANJUNG WANGI SAMA SEPERTI RAME2 DG SKEMA GRADINGNYA
    /*
    if (kodeCabang == 7) {
        // Skema Grading
        var hitungBlok1 = Number(maxRangeUsedBLOk1) * Number(tarifBLOK1);
        var hitungBlok2 = (Number(usedBLOK2) - Number(maxRangeUsedBLOK2)) * Number(tarifBLOK2);

        var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
        var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
        var kali1 = 1.25 * Number(tambahBiayaBeban);
        var kali2 = Number(tambahBiayaBeban);
        totalTagihan = Number(kali1) + Number(kali2);
    }
    */

    //Format Total Price
    //fTotalTagihan = totalTagihan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    //$('#TOTAL_PRICE').val(fTotalTagihan);
    //$('#TOTAL_PAYMENT').val(fTotalTagihan);

    //cari rate
    var rate = 1;
    $("#table-pricing tbody").each(function () {
        currency = $(this).find('td:eq(3)').html();
        if (currency == 'USD') {
            $.ajax({
                type: "GET",
                url: "/TransContractOffer/GetDataCurrencyRate",
                contentType: "application/json",
                dataType: "json",
                data: {},
                success: function (data) {
                    rate = data.Data;
                    totalUsd = totalTagihan;
                    totalTagihan = totalTagihan * rate;
                    fTotalTagihan = totalTagihan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    fTotalTagihanUsd = totalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    console.log("ths " + totalTagihan + " - " + fTotalTagihan);
                    $('#TOTAL_PRICE').val(fTotalTagihan);
                    $('#TOTAL_PAYMENT').val(fTotalTagihan);
                    $('#TOTAL_USD').val(fTotalTagihanUsd);
                    $('#RATE').val(rate);
                }
            });
        } else {
            totalUsd = totalTagihan;
            fTotalTagihan = totalTagihan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            fTotalTagihanUsd = totalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            console.log("ths " + totalTagihan + " - " + fTotalTagihan);
            $('#TOTAL_PRICE').val(fTotalTagihan);
            $('#TOTAL_PAYMENT').val(fTotalTagihan);
            $('#TOTAL_USD').val(fTotalTagihanUsd);
            $('#TOTAL_USD').hide();
            $('#USD').hide();
            $('#RATE').val(1);
        }
    });

}

// Function untuk ambil data dari uri
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getCoaProduksi() {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiElectricity",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- ChoOse COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(".coa_prod").html('');
            $(".coa_prod").append(listItems);
            //$(".coa_prod option[value='" + coaprod + "']").prop('selected', true);
            //$(".coa_prod").select2(coaprod, coaprod);
            //console.log("COA PROD COMBO: " + coaprod);
        }
    });
}