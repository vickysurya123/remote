﻿var Zrem007 = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            //url: "/ReportRentalRequest/GetDataBusinessEntity",
            url: "/ReportTransBillingProperty/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                //CBCOnditionType();
                console.log("list BE : " + listItems);
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var handleDatePickers = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            //$('#row-table').css('display', 'block');
            //if ($.fn.DataTable.isDataTable('#table-report-zrem007')) {
            //    $('#table-report-zrem007').DataTable().destroy();
            //}
            $.blockUI({ boxed: true }); //$.unblockUI();
            dtTable.clear().draw();

            var dateFro = $('#POSTING_DATE_FROM').val() ? $('#POSTING_DATE_FROM').val().split("/") : "";
            var dateFrox = $('#POSTING_DATE_FROM').val() ? ""+dateFro[2] + dateFro[1] + dateFro[0] : "";
            var dateTo = $('#POSTING_DATE_TO').val() ? $('#POSTING_DATE_TO').val().split("/") : "";
            var dateTox = $('#POSTING_DATE_TO').val() ? "" + dateTo[2] + dateTo[1] + dateTo[0] : "";

            var isSemester = $("#semestercheck").prop("checked") ? $("#semester").val() : "";
            var isTriwulan = $("#triwulancheck").prop("checked") ? $("#triwulan").val() : "";
            //if ($('#PROFIT_CENTER').val()) {
                if (($('#POSTING_DATE_FROM').val() && $('#POSTING_DATE_TO').val()) || isSemester || isTriwulan) {
                    $.ajax({
                        type: "GET",
                        url: "/ReportZrem007/GetDataZrem007",
                        contentType: "application/json",
                        async: true,
                        data: {
                            ProfitCenter: $('#PROFIT_CENTER').val(),
                            Contract: $('#CONTRACT_NO').val(),
                            Semester: isSemester,
                            TahunSemester: $('#select-semester-tahun').val(),
                            Triwulan: isTriwulan,
                            TahunTriwulan: $('#select-periode-tahun').val(),
                            PostingFrom: dateFrox,
                            PostingTo: dateTox
                        },
                        dataType: "json",
                        success: function (data) {
                            //console.log(data)
                            $.unblockUI();
                            if (data.Status == "S") {
                                dtTable.rows.add(data.Message).draw();
                            } else {
                                alert(data.Message)
                            }
                        },
                        error: function (e) {
                            $.unblockUI();
                        }
                    });
                } else {
                    $.unblockUI();
                    alert("Mohon melengkapi filter tanggal")
                }
            //} else {
            //    alert("Mohon melengkapi filter profit center")
            //}

        });
    }

    var tableData = $('#table-report-zrem007');
    var dtTable = tableData.DataTable({
        info: true,
        ordering: true,
        paging: true,
        searching: true,
        //pagingType: "simple",
        columns: [
            { data: 'CONTRACT' },
            { data: 'RO' },
            { data: 'CONDTYPE' },
            { data: 'ITEM_TRX' },
            { data: 'KD_CATEGORY' },
            { data: 'BULAN' },
            { data: 'BILLING_NO' },
            { data: 'MASA_SEWA' },
            { data: 'PERIOD' },
            { data: 'CONTRACT_START' },
            { data: 'CONTRACT_END' },
            { data: 'LEGAL_CONTRACT' },
            { data: 'PRCTR' },
            { data: 'CUSTOMER' },
            { data: 'NAME1' },
            { data: 'GL_REVENUE' },
            { data: 'QUANTITY' },
            { data: 'BASE_UOM' },
            { data: 'CURRENCY' },
            {
                data: 'VALUE_CONTRACT',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'VALUE_PER_MONTH',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'AR_D',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PANJANG_D',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDEK_D',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDAPATAN_D',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PANJANG_C',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDEK_C',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDAPATAN_C',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PANJANG_SALDO',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDEK_SALDO',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'SISA_PENDEK_SALDO',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            {
                data: 'PENDAPATAN_SALDO',
                render: $.fn.dataTable.render.number('.', '.', 0, '')
            },
            { data: 'GJAHR' },
            { data: 'BUZEI' },
            { data: 'KTEXT' },
            { data: 'POSTING_DATE' },
            { data: 'BELNR' },
            { data: 'BUKRS' },
            { data: 'KETERANGAN' },
            { data: 'POST_STATUS' },
            { data: 'CANCEL_STATUS' },
            { data: 'TERMINATE_STATUS' },
            { data: 'CREATED_BY' },
            { data: 'CREATED_DATE' },
            { data: 'LAST_CHANGED_BY' },
            { data: 'LAST_CHANGED_DATE' },
        ],
        language: {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian-Alternative.json"
        }
    });

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    //new export excel zrem
    var exportExcel = function () {
        $('#btn-cetak_excel').click(function () {
            var dataTablerows = $('#table-report-zrem007').DataTable().rows().data();
            if (dataTablerows.length > 0) {
                var dataExcel = new Array();
                dataExcel.push({
                    CONTRACT: 'Contract',
                    RO: 'RO',
                    CONDTYPE: 'Condition Type',
                    ITEM_TRX: 'Item Trx',
                    KD_CATEGORY: 'Kode Cat.',
                    BULAN: 'Accr. Bln ke',
                    BILLING_NO: 'Billing No',
                    MASA_SEWA: 'Masa Sewa',
                    PERIOD: 'Accr.Period',
                    CONTRACT_START: 'Contract Start',
                    CONTRACT_END: 'Contract End',
                    LEGAL_CONTRACT: 'Legal Contract',
                    PRCTR: 'Profit Center',
                    CUSTOMER: 'Customer',
                    NAME1: 'Name 1',
                    GL_REVENUE: 'GL Rev.',
                    QUANTITY: 'Qty',
                    BASE_UOM: 'UoM',
                    CURRENCY: 'Currency',
                    VALUE_CONTRACT: 'Value Contract',
                    VALUE_PER_MONTH: 'Value Per Month',
                    AR_D: 'AR(D)',
                    PANJANG_D: 'Panjang(D)',
                    PENDEK_D: 'Pendek(D)',
                    PENDAPATAN_D: 'Pendapatan(D)',
                    PANJANG_C: 'Panjang(C)',
                    PENDEK_C: 'Pendek(C)',
                    PENDAPATAN_C: 'Pendapatan(C)',
                    PANJANG_SALDO: 'Tot.Panjang',
                    PENDEK_SALDO: 'Tot.Pendek',
                    SISA_PENDEK_SALDO: 'Sisa Pendek Saldo',
                    PENDAPATAN_SALDO: 'Tot.Pendapatan',
                    GJAHR: 'Fiscal Year',
                    BUZEI: 'Line item',
                    KTEXT: 'Profit Center Text',
                    POSTING_DATE: 'Posting Date',
                    BELNR: 'Document Number',
                    BUKRS: 'Company Code',
                    KETERANGAN: 'Keterangan',
                    POST_STATUS: 'Post Status',
                    CANCEL_STATUS: 'Cancel Status',
                    TERMINATE_STATUS: 'Terminate Status',
                    CREATED_BY: 'Created By',
                    CREATED_DATE: 'Created Date',
                    LAST_CHANGED_BY: 'Last Chg By',
                    LAST_CHANGED_DATE: 'Last Chg Date',
                });
                $.each(dataTablerows, function (e, v) {
                    dataExcel.push({
                        CONTRACT: v.CONTRACT,
                        RO: v.RO,
                        CONDTYPE: v.CONDTYPE,
                        ITEM_TRX: v.ITEM_TRX,
                        KD_CATEGORY: v.KD_CATEGORY,
                        BULAN: v.BULAN,
                        BILLING_NO: v.BILLING_NO,
                        MASA_SEWA: v.MASA_SEWA,
                        PERIOD: v.PERIOD,
                        CONTRACT_START: v.CONTRACT_START,
                        CONTRACT_END: v.CONTRACT_END,
                        LEGAL_CONTRACT: v.LEGAL_CONTRACT,
                        PRCTR: v.PRCTR,
                        CUSTOMER: v.CUSTOMER,
                        NAME1: v.NAME1,
                        GL_REVENUE: v.GL_REVENUE,
                        QUANTITY: v.QUANTITY,
                        BASE_UOM: v.BASE_UOM,
                        CURRENCY: v.CURRENCY,
                        VALUE_CONTRACT: v.VALUE_CONTRACT,
                        VALUE_PER_MONTH: v.VALUE_PER_MONTH,
                        AR_D: v.AR_D,
                        PANJANG_D: v.PANJANG_D,
                        PENDEK_D: v.PENDEK_D,
                        PENDAPATAN_D: v.PENDAPATAN_D,
                        PANJANG_C: v.PANJANG_C,
                        PENDEK_C: v.PENDEK_C,
                        PENDAPATAN_C: v.PENDAPATAN_C,
                        PANJANG_SALDO: v.PANJANG_SALDO,
                        PENDEK_SALDO: v.PENDEK_SALDO,
                        SISA_PENDEK_SALDO: v.SISA_PENDEK_SALDO,
                        PENDAPATAN_SALDO: v.PENDAPATAN_SALDO,
                        GJAHR: v.GJAHR,
                        BUZEI: v.BUZEI,
                        KTEXT: v.KTEXT,
                        POSTING_DATE: v.POSTING_DATE,
                        BELNR: v.BELNR,
                        BUKRS: v.BUKRS,
                        KETERANGAN: v.KETERANGAN,
                        POST_STATUS: v.POST_STATUS,
                        CANCEL_STATUS: v.CANCEL_STATUS,
                        TERMINATE_STATUS: v.TERMINATE_STATUS,
                        CREATED_BY: v.CREATED_BY,
                        CREATED_DATE: v.CREATED_DATE,
                        LAST_CHANGED_BY: v.LAST_CHANGED_BY,
                        LAST_CHANGED_DATE: v.LAST_CHANGED_DATE,
                    });
                });
                var ws = XLSX.utils.json_to_sheet(dataExcel, {
                    //header: [
                    //    'NO_CTR', 'TRANSACT_DATE', 'NM_KAPAL', 'VOYAGE_NO', 'NM_AGEN', 'SIZE_CTR', 'TIPE_CTR',
                    //    'STATUS_CTR', 'NM_YBLOK', 'YSLOT', 'YROW', 'YTIER', 'NM_SERVIS', 'KETERANGAN'
                    //],
                    skipHeader: true
                });
                var wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, 'Report');
                XLSX.writeFile(wb, 'Report.xlsx', { cellDates: true, cellStyles: true });
            }
            else {
                swal({
                    title: 'Warning',
                    text: 'Tidak Ada Data untuk di Export',
                    confirmButtonColor: '#FF7043',
                    type: 'warning'
                });
            }
        });
    }

    return {
        init: function () {
            handleDatePickers();
            filter();
            reset();
            CBBusinessEntity();
            //CBCOnditionType();
            select2ProfitCenter();
            //CBProfitCenter();
            exportExcel();
        }
    };
}();

function CBProfitCenter(BE_ID) {
    console.log(BE_ID);
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransBillingProperty/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list : " + listItems);
            }
        });
        
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransBillingProperty/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
                console.log("list d : " + listItems);
            }
        });
    }
}

var fileExcels;
function defineExcel() {
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var BILLING_NO = $('#BILLING_NO').val();
    var SAP_DOC_NO = $('#SAP_DOC_NO').val();
    var CONDITION_TYPE = $('#CONDITION_TYPE').val();
    var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
    var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();

    var param = {
        PROFIT_CENTER,
        BUSINESS_ENTITY,
        BILLING_NO,
        SAP_DOC_NO,
        CONDITION_TYPE,
        POSTING_DATE_FROM,
        POSTING_DATE_TO,
        CUSTOMER_ID,

    }

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportTransBillingProperty/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportTransBillingProperty/deleteExcel",
        data: JSON.stringify(fileExcels),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

jQuery(document).ready(function () {
    //$('#table-report-trans-water > tbody').html('');
    $('#table-report-zrem007').DataTable();
});

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Zrem007.init();
    });
}

    
