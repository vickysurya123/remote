﻿var PricingAnekaUsaha = function () { 

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/VariousBusinessPricing/Add';
        });
    }
    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/VariousBusinessPricing";
        });
    } 
     
    return {
        init: function () {
            add();
            batal();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        PricingAnekaUsaha.init();
    });
}