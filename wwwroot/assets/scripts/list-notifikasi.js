﻿var ListNotifikasi = function () {
    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/ListNotifikasi/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                {   
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "BE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-view" class="btn btn-icon-only blue" title="Detail" ><i class="fa fa-eye"></i></a>';
                        var branch = $('#branch_id').html();
                        if (branch == full.BRANCH_ID) {
                            aksi +=
                                '<a id="btn-ubah" class="btn btn-icon-only red" title="Edit" ><i class="fa fa-edit"></i></a>' +
                                '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    $('#btn-add-role').click(function () {
        $('#txt-judul').text('Add');
        clear();
    });

    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var BE_ID = $('#BE_NAME').val();
            var ID = $('#id_apv_header').val();
            if (BE_ID) {
                var param = {
                    BE_ID: BE_ID,
                    MODUL_ID: "1",
                    ID: ID
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ListNotifikasi/InsertHeader",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    $('#table-apvset').on('click', 'tr #btn-view', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-apvset').DataTable();
        var data = table.row(baris).data();

        //var start_date = data["START_DATE"].split(" ");
        //var end_date = data["END_DATE"].split(" ");

        //$('.START_DATE').val(start_date[0]);
        //$('.END_DATE').val(end_date[0]);
        //$('.NOTIF_START').val(data["NOTIF_START"]);
        //$('.CONTRACT_TYPE').val(data["CONTRACT_TYPE"]);

        $('#detailRole').modal('show');
        $('#table-detail-row').DataTable({
            "ajax":
            {
                "url": "/ListNotifikasi/ViewDetail/" + data["ID"],
                "type": "GET",

            },

            "columns": [
                {
                    "data": "APPROVAL_NO",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = full.ROLE_NAME;
                        return aksi;
                    },
                    "class": "dt-body-center"
                },

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });

    });

    $('#table-apvset').on('click', 'tr #btn-ubah', function () {
        clear();
        var baris = $(this).parents('tr')[0];
        var table = $('#table-apvset').DataTable();
        var data = table.row(baris).data();
        var table_row = $('#table-row ')
        var a = '';
        var b = '';
        //var start_date = data["START_DATE"].split(" ");
        //var end_date = data["END_DATE"].split(" ");

        $('#txt-judul').text('Edit');
        $('#id_detail').val(data["ID"]);
        //$('#START_DATE').val(start_date[0]);
        //$('#END_DATE').val(end_date[0]);
        //$('#NOTIF_START').val(data["NOTIF_START"]);
        //$('#CONTRACT_TYPE').val(data["CONTRACT_TYPE"]);

        $('#adddetailModal').modal('show');
        $('#table-row').DataTable().destroy();
        $('#table-row tbody').html('');
        $.ajax({
            type: "GET",
            url: "/ListNotifikasi/EditDataDetail?id=" + data["ID"],
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                for (var i = 0; i < jsonList.data.length; i++) {
                    a += '<tr>' +
                        '<td>' + jsonList.data[i].ID + '</td>' +
                        '<td>' + jsonList.data[i].APPROVAL_NO + '</td>' +
                        '<td>' + jsonList.data[i].ROLE_NAME + '</td>' +
                        '<td><center><a class="btn default btn-xs blue" id="btn-update-row"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
                        '<td>' + jsonList.data[i].ROLE_ID + '</td>' +
                        '</tr>';
                }
                $('#table-row tbody').append(a);
                $('#table-row').DataTable({
                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"]
                    ],
                    "pageLength": 100000,
                    "paging": false,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false,
                    "aoColumnDefs": [
                        { "bVisible": false, "aTargets": [0, 4] }
                    ]
                });
                
            }
        });

        
    });
        
    var listItems = "";

    var addRow = function () {
        //table row
        var tablem = $('#table-row');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "aoColumnDefs": [
                { "bVisible": false, "aTargets": [0, 4] }
            ]
        });
                
        //Generate textbox(form) di data table table-row
        $('#btn-add-row').on('click', function () {
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var xtable = $('#table-row').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-row").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var roleId = '<select class="form-control role_id" name="role_id" id="role_id">' + listItems + '</select>';
            var waktu = '<input type="text" class="form-control" id="limit-time" name="limit-time">';

            oTable.fnAddData([null, nomor,
                roleId,
                '<center><a class="btn default btn-xs green btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                null
            ], true);

            $('input[name="limit-time"]').inputmask("hh:mm", {
                "placeholder": "HH:MM",
                insertMode: false,
                showMaskOnHover: false,
            });
        });

        $('#table-row').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var data = table.row(baris).data();
            console.log(data[0]);
            if (data[0] != null) {
                a = "<tr><td>" + data[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-row tbody tr').each(function () {
                $(this).find('td').eq(0).html(index);
                index++;
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                indexparam++;
            });
        }

        $('#table-row').on('click', 'tr .btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var data = table.row(baris).data();

            var a = $('#role_id').val();
            var b = $('#limit-time').val();
            var tombol = '<center><a class="btn default btn-xs blue" id="btn-update-row"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var role_id = a.split("-")
            oTable.fnUpdate(role_id[1], baris, 2, false);
            oTable.fnUpdate(tombol, baris, 3, false);
            oTable.fnUpdate(role_id[0], baris, 4, false);


            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);

            //$(this).remove();
            return false;

        });        
    }

    $('#table-row').on('click', 'tr #btn-update-row', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-row').DataTable();
        var adata = table.row(baris).data();
        var role = adata[4] + "-" + adata[2];
        var listitemsEdit = '';
        $.ajax({
            type: "GET",
            url: "/ListNotifikasi/GetDataDropDownRole",
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                var jsonList = data;
                console.log(role);
                listitemsEdit += '<select class="form-control role_id" name="role_id" id="role_id">';
                for (var i = 0; i < jsonList.data.length; i++) {
                    var selected = (role == jsonList.data[i].ROLE_ID + "-" + jsonList.data[i].ROLE_NAME ? "selected" : "");
                    listitemsEdit += "<option value='" + jsonList.data[i].ROLE_ID + "-" + jsonList.data[i].ROLE_NAME + "' " + selected + ">" + jsonList.data[i].ROLE_NAME + "</option>";
                }
                listitemsEdit += '</select>';
            }
        });

        var tombol = '<center><a class="btn default btn-xs green btn-savecus-frequency" name="btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
        var openTable = $('#table-row').dataTable();
        openTable.fnUpdate(listitemsEdit, baris, 2, false);
        openTable.fnUpdate(tombol, baris, 3, false);
    })

    var Role = function () {
        $.ajax({
            type: "GET",
            url: "/ListNotifikasi/GetDataDropDownRole",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ROLE_ID + "-" + jsonList.data[i].ROLE_NAME + "'>" + jsonList.data[i].ROLE_NAME + "</option>";
                }
            }
        });
    }

    var simpanData = function () {
        $('#btn-simpanadd-detail').click(function () {
            var table = $('#table-apvset').DataTable();
            var allowSave = true;
            //var START_DATE = $('#START_DATE').val();
            //var END_DATE = $('#END_DATE').val();
            //var NOTIF_START = $('#NOTIF_START').val();
            //var HEADER_ID = $('#id_header').val();
            var HEADER_ID = $('#id_detail').val();
            
            //var CONTRACT_TYPE = $('#CONTRACT_TYPE').val();
            //if (START_DATE && END_DATE && CONTRACT_TYPE) {

            //} else {
            //    allowSave = false;
            //}
            //$('#table-param tbody tr').each(function () {
            //    kolom = $(this).find('#btn-savecus-param').length;
            //    if (kolom == 1) {
            //        allowSave = false;
            //    }
            //});
            //array role
            var DTMemo = $('#table-row').dataTable();
            var countDTMemo = DTMemo.fnGetData();
            arrRole = new Array();
            for (var i = 0; i < countDTMemo.length; i++) {
                var itemDTMemo = DTMemo.fnGetData(i);
                var paramDTMemo = {
                    HEADER_ID: HEADER_ID,
                    APPROVAL_NO: itemDTMemo[1].toString(),
                    ROLE_ID: parseInt(itemDTMemo[4]),
                    ROLE_NAME: itemDTMemo[2],
                    ID: itemDTMemo[0],
                }
                arrRole.push(paramDTMemo);
            }
            if (countDTMemo.length > 0) {
                $('#table-row tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-frequency').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }         
            
            delRole = new Array();
            $('#delete_role tbody tr').each(function () {
                var deleteRole = {
                    ID: $(this).find("td:first").text(),
                }
                delRole.push(deleteRole);
            });
            
            //allowSave = false;
            //console.log(param);
            if (allowSave) {
                var param = {
                    //START_DATE: START_DATE,
                    //END_DATE: END_DATE,
                    //NOTIF_START: NOTIF_START,
                    //CONTRACT_TYPE: CONTRACT_TYPE,
                    ID: HEADER_ID.toString(),
                    ROLE: arrRole,
                    delRole: delRole,
                }
                console.log(param);
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ListNotifikasi/InsertDetail",
                    timeout: 30000
                });
                req.done(function (data) {
                    $('#adddetailModal').modal('hide');
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                $('#adddetailModal').modal('hide');
                swal('Failed', 'Harap di cek kembali inputanya, dan pada table harap di centang semua', 'error').then(function (isConfirm) {
                    $('#adddetailModal').modal('show');
                });
            }

        });
    }

    var clear = function () {
        $('input').val('');
    }

    //$('body').on('click', 'tr #btn-ubah', function () {
    //    console.log('asdasdasd');
    //    $('#txt-judul').text('Edit');
    //    $('#addapvsetModal').modal('show');
    //    var oTable = $('#table-apvset').dataTable();
    //    var nRow = $(this).parents('tr')[0];
    //    var aData = oTable.fnGetData(nRow);
    //    var param = {
    //        BE_ID: aData.BE_ID
    //    };
    //    var req = $.ajax({
    //        contentType: "application/json",
    //        data: JSON.stringify(param),
    //        method: "POST",
    //        url: "/ApprovalSetting/SearchBranch",
    //        timeout: 30000
    //    });
    //    req.done(function (data) {
    //        $('#BE_NAME').val(data.Msg);
    //        $('#id_apv_header').val(aData.ID);
    //    });



    //});
        
    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: aData.ID.toString()
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Approval Setting on branch \"" + aData.BE_NAME + "\"?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ListNotifikasi/DeleteHeader",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    
    $('#btn-bataladd').click(function () {
        clear();
    });

    $('#btn-add').click(function () {
        $('#txt-judul').text('Add');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            Role();
            addRow();
            simpanData();

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ListNotifikasi.init();
    });
}