﻿$("#METER_TO").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

$("#METER_FROM").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

//for function
  // Define Tagihan
    var totalTagihan = 0;
    var fTotalTagihan = 0;
    var biayaBeban = 0;
    var biayaAdmin = 0;
    var tagMin = 0;
    var capacityVA = 0;

    // Define Multiply Factor
    var multiplyFactor = 0;

    // GET DATA PRICING
    var tarifLWBP = 0;
    var tarifKVARH = 0;
    var tarifWBP = 0;
    var tarifBLOK1 = 0;
    var tarifBLOK2 = 0;

    var usedLWBP = 0;
    var usedKVARH = 0;
    var usedWBP = 0;
    var usedBLOK1 = 0;

    var usedBLOK2 = 0;
    var mFacLWBP = 0;
    var mFacWBP = 0;
    var mFacKVARH = 0;

    var maxRangeUsedBLOk1 = 0
    var maxRangeUsedBLOK2 = 0

    // GET DATA COSTING
    var persenPPJU = 0;
    var persenREDUKSI = 0;

    // Define Kode cabang dan min KWH
var kodeCabang = 0;
var minKWH = 0;

var kodeprofitcenter = 0;


var TransWaterElectricityAdd = function () {
    // Define Tagihan
    var totalTagihan = 0;
    var fTotalTagihan = 0;
    var biayaBeban = 0;
    var biayaAdmin = 0;
    var tagMin = 0;
    var capacityVA = 0;

    // Define Multiply Factor
    var multiplyFactor = 0;

    // GET DATA PRICING
    var tarifLWBP = 0;
    var tarifKVARH = 0;
    var tarifWBP = 0;
    var tarifBLOK1 = 0;
    var tarifBLOK2 = 0;

    var usedLWBP = 0;
    var usedKVARH = 0;
    var usedWBP = 0;
    var usedBLOK1 = 0;
    var usedBLOK2 = 0;

    var mFacLWBP = 0;
    var mFacWBP = 0;
    var mFacKVARH = 0;

    var maxRangeUsedBLOk1 = 0
    var maxRangeUsedBLOK2 = 0

    // GET DATA COSTING
    var persenPPJU = 0;
    var persenREDUKSI = 0;

    // Define Kode cabang dan min KWH
    var kodeCabang = 0;
    var minKWH = 0;

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWE";
        });
    }

    generateFormPricing = function () {
        var idInstalasi = $('#ID_INSTALASI').val();
        //console.log('instalasi '+idInstalasi);

        var installationCode = $('#INSTALLATION_CODE').val();
        var valMeterFromLWBP = 0;
        var valMeterFromWBP = 0;
        var valMeterFromKVARH = 0;
        var valMeterFromBLOK1 = 0;

        // Jalankan ajax untuk ambil nilai transaksi bulan sebelumnya
        // Ambil nilai WBP Sebelumnya
        
        $.ajax({
            type: "GET",
            url: "/TransElectricity/getLastWBP",
            contentType: "application/json",
            dataType: "json",
            data: { id_instalasi: installationCode },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems1 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems1 += jsonList.data[i].METER_TO;
                }
                valMeterFromWBP = listItems1;
                console.log('wbp ' + valMeterFromWBP);
            }
        });
        

        // Ambil nilai LWBP Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransElectricity/getLastLWBP",
            contentType: "application/json",
            dataType: "json",
            data: { id_instalasi: installationCode },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += jsonList.data[i].METER_TO;
                }
                valMeterFromLWBP = listItems;
                console.log('lwbp '+valMeterFromLWBP);
            }
        });
        
        

        // Ambil nilai KVARH Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransElectricity/getLastKVARH",
            contentType: "application/json",
            dataType: "json",
            data: { id_instalasi: installationCode },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems2 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems2 += jsonList.data[i].METER_TO;
                }
                valMeterFromKVARH = listItems2;
                console.log('kvarh '+valMeterFromKVARH);

            }
        });

        // Ambil nilai BLOK1 Sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransElectricity/getLastBLOKSATU",
            contentType: "application/json",
            dataType: "json",
            data: { id_instalasi: installationCode },
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems2 = "";
                var disableTextBoxBLOK2 = '';

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems2 += jsonList.data[i].METER_TO;
                }
                valMeterFromBLOK1 = listItems2;
                console.log('blok1 ' + valMeterFromBLOK1);

            }
        });
        // Akhir dari ajax untuk ambil nilai transaksi bulan sebelumnya

        
        var tablePricing = $('#table-pricing').DataTable();
        tablePricing.destroy();

        //Data Table Pricing 
        var tabler = $('#table-pricing');
        var oTable = tabler.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "bPaginate": false
        });
        var buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';

        //Cek transaksi berdasarkan id instalasi yg dipilih
        //ajax untuk cek apakah ada transaksi untuk id transaksi yg dipilih
        var meterFrom = '<input type="text" id="METER_FROM" name="METER_FROM" onchange="hitungUSEDFROM(this)" class="form-control">';
        var meterTo = '<input type="text" id="METER_TO" name="METER_TO" onchange="hitungUSEDTO(this)" class="form-control">';
        var used = '<input type="text" id="USED" name="USED" class="form-control" readonly>';
        var currency = '<input type="text" id="CURRENCY" name="CURRENCY" class="form-control" readonly>';
        var mFactor = '';

        var rOnlymeterFrom = '<input type="text" id="METER_FROM" name="METER_FROM" onchange="hitungUSEDFROM(this)" class="form-control" readonly>';
        var rOnlymeterTo = '<input type="text" id="METER_TO" name="METER_TO" onchange="hitungUSEDTO(this)" class="form-control" readonly>';
        var rOnlyused = '<input type="text" id="USED" name="USED" class="form-control" readonly>';
        var coa_produksi = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
        //var coa_produksi = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_prod" name="coa_prod" readonly></center>';
        var keterangan = '<input type="text" id="keterangan" name="keterangan" class="form-control" style="width:200px">';
        //var coa_produksi = 'assdf'
        

        // Masking
        $("input[name=METER_FROM]").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });

        $("input[name=METER_TO]").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });

        $("div.dataTables_filter input").keyup( function (e) {
            if (e.keyCode == 13) {
                oTable.fnFilter( this.value );
            }
        } );

        //cari coa
        $('#table-pricing').on('click', 'tr #btn-coa', function () {
            $('#coaModal').modal('show');
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransElectricity/GetListCoaElectric",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;
                        return data.data;
                    },
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            console.log(value);

            var elems = $('#table-pricing').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                c_data = $('#table-pricing').DataTable().row(index).node();
                console.log(index, c_data)

                var elem = $(c_data).find('#coa_prod');
                elem.val(value);

            });
            $('#coaModal').modal('hide');
        });

        $.ajax({
            type: "GET",
            url: "/TransElectricity/GetDataPricing/" + idInstalasi,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data;
                var listItems = "";
                var disableTextBoxBLOK2 = '';
                var isiNilaiMultiplyFactor = 0;
                getCoaProduksi();

                for (var i = 0; i < jsonList.data.length; i++) {
                    console.log(jsonList.data);
                    mFactor = '<input type="text" id="MULTIPLY_FACT" value="'+jsonList.data[i].MULTIPLY_FACT+'" name="M_FACTOR" class="form-control" readonly>';
                    if (jsonList.data[i].PRICE_TYPE == 'BLOK2') {
                        oTable.fnAddData([jsonList.data[i].PRICE_TYPE, jsonList.data[i].PRICE_CODE, jsonList.data[i].AMOUNT, jsonList.data[i].CURRENCY, rOnlymeterFrom, rOnlymeterTo, rOnlyused, mFactor, jsonList.data[i].MAX_RANGE_USED, coa_produksi, keterangan]);
                    }
                    else {
                        oTable.fnAddData([jsonList.data[i].PRICE_TYPE, jsonList.data[i].PRICE_CODE, jsonList.data[i].AMOUNT, jsonList.data[i].CURRENCY, meterFrom, meterTo, used, mFactor, jsonList.data[i].MAX_RANGE_USED, coa_produksi, keterangan]);
                    }
                    isiNilaiMultiplyFactor = jsonList.data[i].MULTIPLY_FACT;
                }

                $('#UPDATE_MULTIPLY_FACTOR').val(isiNilaiMultiplyFactor);

                /*
                var countTablePrc = oTable.fnGetData();
                console.log(countTablePrc);

                for (var i = 0; i < countTablePrc.length; i++) {
                    var itemDTPricing = oTable.fnGetData(i);
                    console.log(itemDTPricing[1]);
                }
                */

                //LWBP
                
                $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                    if (valMeterFromLWBP == 'null') {
                        var assignNolLWBP = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolLWBP);
                        console.log('kvarh 0 ' + assignNolLWBP);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromLWBP);
                    }
                });
                

                //WBP
                $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                    if (valMeterFromKVARH == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolKVARH);
                        console.log('kvarh 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromKVARH);
                    }
                });


                //WBP
                $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                    if (valMeterFromWBP == 'null') {
                        var assignNolWBP = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolWBP);
                        console.log('wbp 0 ' + assignNolWBP);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromWBP);
                    }
                });

                //WBP
                $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                    if (valMeterFromBLOK1 == 'null') {
                        var assignNolKVARH = 0;
                        $(this).find("td:eq(4) input[type='text']").val(assignNolKVARH);
                        console.log('blok 1 0 ' + assignNolKVARH);
                    }
                    else {
                        $(this).find("td:eq(4) input[type='text']").val(valMeterFromBLOK1);

                    }
                });

            }
        });
    }

    generateFormCosting = function () {
        var idInstalasix = $('#ID_INSTALASI').val();

        var tableCosting = $('#table-costing').DataTable();
        tableCosting.destroy();

        //Data Table Pricing 
        var tablec = $('#table-costing');
        var cTable = tablec.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "bPaginate": false
        });

        $.ajax({
            type: "GET",
            url: "/TransElectricity/GetDataCosting/" + idInstalasix,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                cTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    cTable.fnAddData([jsonList.data[i].DESCRIPTION, jsonList.data[i].PERCENTAGE]);
                }
            }
        });
    }
    
    var perhitunganPricing = function () {
        $('#iClick').on('click', function () {

        });
    }

    /*
    var hitung = function () {
        $('#METER_TO').on('change', function (e) {
            var prc = $('#PRICE').val();
            var m_factor = $('#MULTIPLY_FACTOR').val();

            var to = $('#METER_TO').val();
            var from = $('#METER_FROM').val();
            var kwh = Number(to) - Number(from);

            var total = Number(prc) * Number(m_factor) * Number(kwh);
            var totalBulat = Math.round(total);

            var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#AMOUNT").val(fTotalBulat);
            $("#KWH").val(kwh);
        });
    }
    */

    var simpan = function () {
        $('#btn-simpan').click(function () {

            var cekP = $('#CUSTOMER_MDM').val();
            cekPiut("1M", cekP);
            var xPiut = $("#cPiut").val();

            console.log("xPiut : " + xPiut);

            var ID_INSTALASI = $('#ID_INSTALASI').val();
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            //var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var CUSTOMER = $('#CUSTOMER_MDM').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var PERIOD = $('#PERIOD').val();
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var PRICE = $('#PRICE').val();
            var MULTIPLY_FACTOR = $('#MULTIPLY_FACTOR').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#TOTAL_PAYMENT').val();
            var RATE = $('#RATE').val();
            var UNIT = 'KWH';
            var INSTALLATION_CODE = $('#INSTALLATION_CODE').val();
            var COA_PROD = $('#coa_prod');

            var INSTALLATION_NUMBER = $('#INSTALLATION_NUMBER').val();

            var raw_profit_center = $('#PROFIT_CENTER').val();
            var STATUS_DINAS = $('#STATUS_DINAS').val();
            
            var pecahRawPC = raw_profit_center.split(" - ");
            var PROFIT_CENTER = pecahRawPC[0];


            // PENGECEKAN VALID_FROM VALID_TO DAN AMOUNT
            var uWBP = 0;
            var uLWBP = 0;
            var uKVARH = 0;
            var uBlok1 = 0;

            // Array Untuk Menampung Detail Pricing dan Costing
            arrDetailPricing = new Array();
            arrDetailCosting = new Array();
            arrcoprodkosong = new Array();
            arrperiod = new Array();


            //USED LWBP
            $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
                uLWBP = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
                uWBP = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
                uKVARH = $(this).find("td:eq(5) input[type='text']").val();
            });

            $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
                uBlok1 = $(this).find("td:eq(5) input[type='text']").val();
            });

            var mFac = MULTIPLY_FACTOR;

            var kaliWBP = Number(uLWBP) * Number(mFac);
            var kaliLWBP = Number(uLWBP) * Number(mFac);
            var kaliKVARH = Number(uKVARH) * Number(mFac);
            var totalK = Number(kaliWBP) + Number(kaliLWBP) + Number(kaliKVARH);


            // variable nilaiAmount masuk ke QUANTITY
            var nilaiAMOUNT = 0;
            if (uBlok1 == 0) {
                nilaiAMOUNT = totalK;
            }
            else {
                nilaiAMOUNT = uBlok1;
            }
            console.log('Nilai AMOUNT ' + nilaiAMOUNT);

            //if (PERIOD == "" || PERIOD == null) {
            //    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            //    console.log("PERIOD KOSONG");
            //}
            //else {
            //    console.log("PERIOD ADA");
            //}

            if (AMOUNT) {

                // DATA DETAIL PRICING
                var dtTransEL = $('#table-pricing').dataTable();
                var countDTObject = dtTransEL.fnGetData();
                var all_row = dtTransEL.fnGetNodes();

                for (var i = 0; i < countDTObject.length; i++) {
                    var itemDTObject = dtTransEL.fnGetData(i);

                    for (var i = 0; i < all_row.length; i++) {
                        var meter_from = $(all_row[i]).find("td:eq(4) input[type='text']").val();
                        var meter_to = $(all_row[i]).find("td:eq(5) input[type='text']").val();
                        var used = $(all_row[i]).find("td:eq(6) input[type='text']").val();
                        var satu = $(all_row[i]).find('td:eq(0)').html();
                        var dua = $(all_row[i]).find('td:eq(1)').html();
                        var tiga = $(all_row[i]).find('td:eq(2)').html();
                        var mFactor = $(all_row[i]).find("td:eq(7) input[type='text']").val();
                        var coprod = $(all_row[i]).find("td:eq(9) select[name = 'coa_prod']").val();
                        //var coprod = $(all_row[i]).find("td:eq(9) input[type = 'text']").val();
                        var keterangan = $(all_row[i]).find("td:eq(10) input[type='text']").val();
                        //console.log(mFactor,coprod,keterangan)
                        var paramDTObject = {
                            PRICE_TYPE: satu,
                            PRICE_CODE: dua,
                            TARIFF: tiga,
                            METER_FROM: meter_from,
                            METER_TO: meter_to,
                            USED: used,
                            MULTIPLY: mFactor,
                            INSTALLATION_CODE: INSTALLATION_CODE,
                            COA_PROD: coprod,
                            KETERANGAN: keterangan,
                        };
                        arrDetailPricing.push(paramDTObject);
                    }
                }
              
                // DATA DETAIL COSTING
              /*  var DTCosting = $('#table-costing').dataTable();
                var countDTCosting = DTCosting.fnGetData();
                for (var i = 0; i < countDTCosting.length; i++) {
                    var itemDTCosting = DTCosting.fnGetData(i);
                    var paramDTCosting = {
                        DESCRIPTION: itemDTCosting[0],
                        PERCENTAGE: itemDTCosting[1]
                    }
                    arrDetailCosting.push(itemDTCosting);
                }*/

                var DTCosting = $('#table-costing').dataTable();
                var countDTCosting = DTCosting.fnGetData();
                var all_row2 = DTCosting.fnGetNodes();

                for (var i = 0; i < countDTCosting.length; i++) {
                    var itemDTObject = DTCosting.fnGetData(i);

                    for (var i = 0; i < all_row2.length; i++) {
                        var satuu = $(all_row2[i]).find('td:eq(0)').html();
                        var duaa = $(all_row2[i]).find('td:eq(1)').html();
                        var paramDTObject = {
                            DESCRIPTION: satuu,
                            PERCENTAGE: duaa,
                        };
                        arrDetailCosting.push(paramDTObject);
                    }
                }





                var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                var param = {
                    ID_INSTALASI: ID_INSTALASI,
                    INSTALLATION_TYPE: INSTALLATION_TYPE,
                    PROFIT_CENTER: PROFIT_CENTER,
                    CUSTOMER: CUSTOMER,
                    CUSTOMER_NAME:CUSTOMER_NAME,
                    PERIOD: PERIOD,
                    MULTIPLY_FACTOR: MULTIPLY_FACTOR,
                    AMOUNT: nfAmount.toString(),
                    UNIT: UNIT,
                    RATE: RATE,
                    INSTALLATION_NUMBER: INSTALLATION_NUMBER,
                    INSTALLATION_CODE: INSTALLATION_CODE,
                    QUANTITY: nilaiAMOUNT.toString(),
                    detailpricings: arrDetailPricing,
                    detailcostings: arrDetailCosting,
                    STATUS_DINAS: STATUS_DINAS
                };


        
                for (var i = 0; i < arrDetailPricing.length; i++) {
                    coprodkosong = "";
                    console.log("COA ARRAY : " + arrDetailPricing[i].COA_PROD);
                    if (arrDetailPricing[i].COA_PROD == null || arrDetailPricing[i].COA_PROD == '') {
                        arrcoprodkosong.push("Y");
                    } else {
                        arrcoprodkosong.push("T");
                        
                    }
                }                
                
                var found = arrcoprodkosong.find(function (element) {
                    return element === "Y";
                });

                if (typeof (found) !== "undefined" && found !== null) {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
                else if (PERIOD == '' || PERIOD == null) {
                    swal('Warning', 'Please Fill Period', 'warning');
                }
                else if (STATUS_DINAS == '' || STATUS_DINAS == null) {
                    swal('Warning', 'Please Fill Status Dinas', 'warning');
                }
                else if (xPiut == "X") {
                    swal('Warning', 'Customer memiliki piutang!', 'warning');

                }
                else {
                    App.blockUI({ boxed: true });
                    console.log(param);
               var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransElectricity/AddData"
                });

                //var reqq = $.ajax({
                //    type: "POST",
                //    url: "/TransRentalRequest/cekPiutangSAP",
                //    data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
                //    contentType: "application/json; charset=utf-8",
                     
                //});

                //    reqq.done(function (data1)){
                        
                //        if (data1.E_RELEASE_STATUS == "X") {
                //            $("#cpiut").val(data1.E_RELEASE_STATUS);
                //            swal('warning', data1.e_description, 'error');

                //        }
                //        else {
                //            $("#cpiut").val("1");
                //        }
                //    }    

                req.done(function (data) {
                    var kode_transaksi = data.be_number;
                    //END OF LOOPING SAVE DATA
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/TransWE";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location = "/TransWE";
                        });
                    }
                });

            }
                } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
       
    }

    var cekPiut = function (doc_type, cust_code) {
        var param = {
            DOC_TYPE: doc_type,
            CUST_CODE: cust_code
        };

        $.ajax({
            type: "POST",
            url: "/TransRentalRequest/cekPiutangSAP",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var mdm = function () {
        var cekMdm = $('#CUSTOMER_MDM').val();
        cekPiut("1M", cekMdm);

        var xPiut = $("#cPiut").val();

        console.log("cpiut : " + xPiut);
        console.log("mdm : " + cekMdm);
        
    }

    var customer = function () {
        setTimeout(function(){
            var cekCustomerId = $('#CUSTOMER_CODE').val();
            if (cekCustomerId == '0210012143') {
                $('#USED').after("<a data-toggle='modal' href='#viewModalListrik' class='btn btn-icon-only green' id='btn-detail-listrik'><i class='fa fa-pencil'></i></a>");
                
            }
        }, 3000)
    }

    var x = 0;
    var addDetail = function(){
        var $line = $('.detail-penggunaan');
        $('#btn-add').on('click', function() {
            $line.after('<div class="portlet-body detail-penggunaan">'+
                '<div class="row">'+
                    '<div class="col-md-4">'+
                        '<h4>Sumber</h4>'+
                        '<select class="form-control sumber" id="sumber" name="detail["'+ x++ +'"].sumber">'+
                            '<option value="">--Pilih Sumber--</option>'+
                            '<option value="PLN">PLN</option>'+
                            '<option value="PLTMG">PLTMG</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>Price Type</h4>'+
                        '<select class="form-control type" id="type" name="detail["'+ x++ +'"].type">'+
                            '<option value="">--Pilih Price Type--</option>'+
                            '<option value="LWBP">LWBP</option>'+
                            '<option value="WBP">WBP</option>'+
                            '<option value="KVARH">KVARH</option>'+
                            '<option value="BLOK1">BLOK1</option>'+
                            '<option value="BLOK2">BLOK2</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>Faktor Pengali</h4>'+
                        '<input type="number" id="pengali" name="detail["'+ x++ +'"].pengali" class="form-control pengali"/>'+
                    '</div>'+
                '</div>'+
                '<br>'+
                '<div class="row">'+
                    '<div class="col-md-3">'+
                        '<h4>Tgl Stan Awal</h4>'+
                        '<input type="date" id="tglStanAwal" name="detail["'+ x++ +'"].tglStanAwal" class="form-control tglStanAwal"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Tgl Stan Akhir</h4>'+
                        '<input type="date" id="tglStanAkhir" name="detail["'+ x++ +'"].tglStanAkhir" class="form-control tglStanAkhir"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Stan Awal</h4>'+
                        '<input type="number" id="stanAwal" name="detail["'+ x++ +'"].stanAwal" class="form-control stanAwal"/>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<h4>Stan Akhir</h4>'+
                        '<input type="number" id="stanAkhir" name="detail["'+ x++ +'"].stanAkhir" class="form-control stanAkhir"/>'+
                    '</div>'+
                '</div>'+
                '<br>'+
                '<div class="row">'+
                    '<div class="col-md-4">'+
                        '<h4>Selisih</h4>'+
                        '<input type="text" id="selisih" name="detail["'+ x++ +'"].selisih" class="form-control selisih" readonly/>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>KWH Tenan</h4>'+
                        '<input type="number" id="kwhTenan" name="detail["'+ x++ +'"].kwhTenan" class="form-control kwhTenan"/>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<h4>KWH Ditagihkan</h4>'+
                        '<input type="text" id="kwhDitagihkan" name="detail["'+ x++ +'"].kwhDitagihkan" class="form-control kwhDitagihkan" readonly/>'+
                    '</div>'+
                '</div>'+
                '<hr>'+
                '<br>'+
            '</div>');
        })

    }

    $(document).on('change','.pengali', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });

    $(document).on('change','.stanAwal', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });

    $(document).on('change','.stanAkhir', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();

        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        
        if (awal != '' && akhir != '' && fpengali != '') {
            parent.find(".selisih").val(selisih);
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        } else if (tenan != ''){
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }
    });
    
    $(document).on('change','.kwhTenan', function (e) {
        var parent = $(this).parents('.detail-penggunaan')
        var awal = parent.find('.stanAwal').val();
        var akhir = parent.find('.stanAkhir').val();
        var fpengali = parent.find('.pengali').val();
        var tenan = parent.find('.kwhTenan').val();
        
        var selisih = (parseFloat(akhir) - parseFloat(awal)).toFixed(2) * fpengali;
        var ditagihkan = (selisih - tenan).toFixed(2);
        console.log(ditagihkan);
        
        if (tenan != '') {
            parent.find(".kwhDitagihkan").val(ditagihkan).trigger('change');
        }else{
            parent.find(".kwhDitagihkan").val(selisih).trigger('change');
        }

    });

    $(document).on('change','.kwhDitagihkan', function (e) {
        var total = 0;
        $('.kwhDitagihkan').each(function() {
            total += parseFloat($(this).val());
        })
        $('.total').val(total);
    });
    
    $('#btn-save').click(function(){

        //var datastring = $('#used').serialize();
        // var datastring = new FormData($('#used')[0]);
        //console.log(datastring);
        var arrlist = [];
        $(".detail-penggunaan").each(function () {
            arrlist.push({
                'SUMBER': $(this).find('#sumber').val(),
                'PRICE_TYPE': $(this).find('#type').val(),
                'PENGALI': $(this).find('#pengali').val(),
                'TGL_STAN_AWAL': $(this).find('#tglStanAwal').val(),
                'TGL_STAN_AKHIR': $(this).find('#tglStanAkhir').val(),
                'STAN_AWAL': $(this).find('#stanAwal').val(),
                'STAN_AKHIR': $(this).find('#stanAkhir').val(),
                'SELISIH': $(this).find('#selisih').val(),
                'KWH_TENAN': $(this).find('#kwhTenan').val(),
                'KWH_DITAGIHKAN': $(this).find('#kwhDitagihkan').val(),
            });
        });
        var formData = new FormData;
        formData.append('ID_TRANSACTION', $('#idTrans').val());
        formData.append('LIST', JSON.stringify(arrlist));
        formData.append('TOTAL', $('#total').val())
        
        $('#viewModalListrik').modal('hide');
        swal({
            title: 'Warning',
            text: "Are you sure want to save Detail Used ?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {

                App.blockUI({ boxed: true });
                $.ajax({
                    processData: false,
                    contentType: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    type: "post",
                    url: "/TransElectricity/AddDataUsed",
                    data: formData,
                    success: function (data) {
                        var kode_transaksi = data.be_number;
                        //END OF LOOPING SAVE DATA
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#USED').val($('#total').val());
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                swal.close();
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                //window.location = "/TransWE";
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr, status);
                    }
                });
            }
        });

    })

    // Validasi Period
    var validasiPeriod = function () {
        $('#PERIOD').change(function () {
            var period_terpilih = $('#PERIOD').val();

            var idInstalasi = $('#ID_INSTALASI').val();
            var installationCode = $('#INSTALLATION_CODE').val();

            $.ajax({
                type: "GET",
                url: "/TransElectricity/getLastPeriod",
                contentType: "application/json",
                dataType: "json",
                data: { id_instalasi: idInstalasi, periode: period_terpilih },
                success: function (data) {
                    var jsonList = data
                    var listItems1 = "";

                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems1 += jsonList.data[i].PERIOD;
                    }

                    if (listItems1 != '') {
                        swal('Info', 'Sudah Ada Transaksi Untuk Periode ' + period_terpilih + '!', 'info');
                    }
                }
            });
        })
    }

    return {
        init: function () {
            batal();
            mdm();
            //hitung();
            simpan();
            generateFormPricing();
            generateFormCosting();
            perhitunganPricing();
            validasiPeriod();
            //cekPiut();
            customer();
            addDetail();
        }
    };
}();

function hitungUSEDTO(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#table-pricing').DataTable();
    var data = table.row(baris).data();

    var meter_from = $(this_cmb).parents('tr').find('input[name="METER_FROM"]').val();
    var meter_to = $(this_cmb).val();
    var nilai_used = Number(meter_to) - Number(meter_from);
    var used = $(this_cmb).parents('tr').find('input[name="USED"]').val(nilai_used);
}

function hitungUSEDFROM(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#table-pricing').DataTable();
    var data = table.row(baris).data();

    var meter_from = $(this_cmb).val();
    var meter_to = $(this_cmb).parents('tr').find('input[name="METER_TO"]').val();
    var nilai_used = Number(meter_to) - Number(meter_from);
    var used = $(this_cmb).parents('tr').find('input[name="USED"]').val(nilai_used);
}

function gantiMultiply(this_cmb) {
    var nilaiMultiply = $(this_cmb).val();
    $('input[name="M_FACTOR"]').val(nilaiMultiply);

}

function hitungFormula() {

    // GET DATA PRICING ---------------------------------------------------
    $('#TOTAL_PRICE').val('');
    $('#TOTAL_PAYMENT').val('');

    var biayaBeban = $('#BIAYA_BEBAN').val();
    var biayaAdmin = $('#BIAYA_ADMIN').val();
    var tagMin = $('#MIN_PAYMENT').val();
    var capacityVA = $('#CAPACITY_VA').val();

    var multiplyFactor = $('#UPDATE_MULTIPLY_FACTOR').val();

    //LWBP
    $("#table-pricing tbody tr:has(td:containsExact('LWBP'))").each(function () {
        //alert($(this).find("td:last-child").html());
        //alert($(this).find('td:eq(2)').html());
        tarifLWBP = $(this).find('td:eq(2)').html();
        usedLWBP = $(this).find("td:eq(6) input[type='text']").val();

        var raw_multiplyFactor = $(this).find("td:eq(7) input[type='text']").val();
        $('#MULTIPLY_FACTOR').val(raw_multiplyFactor);
        /*
        multiplyFactor = $(this).find('td:eq(6)').html();
        $('#MULTIPLY_FACTOR').val(multiplyFactor);
        */

    });

    //KVARH
    $("#table-pricing tbody tr:has(td:containsExact('KVARH'))").each(function () {
        tarifKVARH = $(this).find('td:eq(2)').html();
        usedKVARH = $(this).find("td:eq(6) input[type='text']").val();
    });


    //WBP
    $("#table-pricing tbody tr:has(td:containsExact('WBP'))").each(function () {
        tarifWBP = $(this).find('td:eq(2)').html();
        usedWBP = $(this).find("td:eq(6) input[type='text']").val();
    });

    //BLOK1
    $("#table-pricing tbody tr:has(td:containsExact('BLOK1'))").each(function () {
        // tarifBLOK1 = $(this).find('td:eq(2)').html();
        tarifBLOK1 = $(this).find('td:eq(2)').html();
        usedBLOK1 = $(this).find("td:eq(6) input[type='text']").val();
        maxRangeUsedBLOk1 = $(this).find('td:eq(8)').html();
        //console.log('max range used blok1 ' + usedBLOK1 + "- " + maxRangeUsedBLOk1);
    });

    //BLOK2
    $("#table-pricing tbody tr:has(td:containsExact('BLOK2'))").each(function () {
        tarifBLOK2 = $(this).find('td:eq(2)').html();
        usedBLOK2 = $(this).find("td:eq(6) input[type='text']").val();
        maxRangeUsedBLOK2 = $(this).find('td:eq(8)').html();
    });


    // GET DATA COSTING -----------------------------------------------------
    // Persen PPJU
    $("#table-costing tbody tr:has(td:containsExact('PPJU'))").each(function () {
        persenPPJU = $(this).find('td:eq(1)').html();
    });

    // Persen REDUKSI
    $("#table-costing tbody tr:has(td:containsExact('REDUKSI'))").each(function () {
        persenREDUKSI = $(this).find('td:eq(1)').html();
    });

    // Perhitungan Sesuai Dengan Kode Cabang
    kodeCabang = $('#KODE_CABANG').val();
    kodeprofitcenter = $('#PROFIT_CENTER').val();
    kodeprofitcenter2 = kodeprofitcenter.substring(0, 5);
    minKWH = $('#MIN_USED').val();
    var daya = Number(capacityVA);

    totalTagihan = 0;
 
    // 1. Perhitungan Untuk Kode Cabang TANJUNG PERAK atau LEGI
    if (kodeCabang == 2 || kodeprofitcenter2 == 10301) { //2
            // Kondisi Pertama
            var cekPertama = (Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor));
            var cekKedua = ((Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor))) * 0.62;
                
            console.log('CEK PERTAMA ' + cekPertama);
            console.log('CEK KEDUA ' + cekKedua);
            // Untuk pengecekan kondisi 
            var kvarhKaliMfact = Number(usedKVARH) * Number(multiplyFactor);

            //Kondisi Pertama Untuk Cabang Perak
            if (Number(cekPertama) <= Number(minKWH)) {
                //Tagihan = Minkwh * (tarifLWBP + tarifWBP) + Minkwh * (tarifLWBP + tarifWBP) * PPJU / 100 - (tagihan * reduksi / 100)
                //var totalTagihana = Number(minKWH) * (Number(tarifLWBP) + Number(tarifWBP));
                var totalTagihana = Number(minKWH) * Number(tarifLWBP);
                var totalTagihana1 = Number(totalTagihana) + (Number(totalTagihana) * Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(totalTagihana1) - (Number(totalTagihana1) * Number(persenREDUKSI) / 100));
                    
                console.log(totalTagihana);
                console.log('rumus perak pertama ' + totalTagihan);

            }
            if (Number(kvarhKaliMfact) < Number(cekKedua) && Number(cekPertama) > Number(minKWH)) {
                var totalTagihan1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + (Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP));
                var totalTagihan2 = Number(totalTagihan1) + (Number(totalTagihan1) * Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(totalTagihan2) - (Number(totalTagihan2) * Number(persenREDUKSI) / 100));
                   
                console.log('rumus perak kedua ' + totalTagihan);

            }
            if (Number(kvarhKaliMfact) > Number(cekKedua) && Number(cekPertama) > Number(minKWH)) {
                var x = ((Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor))) * 0.62;
                var y = (Number(usedKVARH) * Number(multiplyFactor));
                var x1 = Number(y) - Number(x);
                var hitung1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + (Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP));
                //tagihan= tagihan + (((qty(kVARH)*faktor kali) - ((qty(LWBP)*faktorkali) + (qty(WBP)*faktor kali))*62%)) * tarifkVARH)
                var hitung2 = Number(x1) * Number(tarifKVARH);
                //tagihan= tagihan + (tagihan*ppj/100) - (tagihan*reduksi/100)
                var hitung3 = Number(hitung1) + Number(hitung2);
                var hitung4 = Number(hitung3) + (Number(hitung3) * Number(persenPPJU) / 100);

                totalTagihan = Math.round(Number(hitung4) - (Number(hitung4) * Number(persenREDUKSI) / 100));

                    
                //console.log('used lwbp+wbp ' + x);
                //console.log('used kvarh' + y);
                //console.log('used kvarh-lwbp+wbp' + x1);
                //console.log('hitung1 ' + hitung1);
                //console.log('hitung2 ' + hitung2);
                //console.log('hitung3 ' + hitung3);
                //console.log('hitung4 ' + hitung4);

                //console.log('rumus perak ketiga ' + totalTagihan);
        }
    }

        // Khusus BENOA yg sebelumnya rame2 skrg dipisah
    if (kodeCabang == 12 || kodeCabang == 24) {
        
                var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
                var t2 = Number(t1) + Number(biayaBeban);
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));         
        }
     
        // Khusus SEMARANG yg sebelumnya rame2 skrg dipisah
    if (kodeCabang == 9 || kodeprofitcenter2 == 10201) {
            var kondisiSemarang = Number(usedLWBP) * Number(multiplyFactor);
            var minPaymentSemarang = Number(tagMin);
            var powerCapacitySemarang = Number(capacityVA);
            var t1 = (Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(tarifWBP) * Number(multiplyFactor));

            if (t1 <= minPaymentSemarang && powerCapacitySemarang > 900) {
                var t2 = Number(minPaymentSemarang) + Number(biayaBeban); //+ Number(biayaAdmin)
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
            }
            else if (t1 > minPaymentSemarang && powerCapacitySemarang > 900) {
                var t2 = Number(t1) + Number(biayaBeban); //+ Number(biayaAdmin)
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
            }
            else if (powerCapacitySemarang == 450){
                var pemakaianBlok1 = usedBLOK1;
                var maxRangeBlok1 = maxRangeUsedBLOk1;
                console.log('pemakaian blok ' + pemakaianBlok1);
                console.log('max range blok ' + maxRangeUsedBLOk1);

                if (Number(pemakaianBlok1) <= Number(maxRangeBlok1)) {
                    var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                    var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                    var kali1 = Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                    totalTagihan = Math.round(Number(kali2));

                    //console.log('hitung blok 1 ' + hitungBlok1);
                    //console.log('tambah biaya beban ' + tambahBiayaBeban);
                    //console.log('kali 1 ' + kali1);
                    //console.log('kali 2 ' + kali2);
                    //console.log('total tagihan ' + totalTagihan);

                    console.log('rumus 2 blok1 semarang');
                } else {
                    var hitungBlok1 = Number(maxRangeBlok1) * Number(tarifBLOK1);
                    var hitungBlok2 = (Number(usedBLOK1) - Number(maxRangeBlok1)) * Number(tarifBLOK2);

                    var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                    var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                    var kali1 = Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                    totalTagihan = Math.round(Number(kali2));

                    //var ht1 = Number(usedBLOK1) * Number(tarifBLOK1) + Number(biayaBeban);
                    //var ht2 = (Number(usedBLOK1) * Number(tarifBLOK1) + Number(biayaBeban)) * (Number(persenPPJU) / 100);
                    //totalTagihan = Math.round(Number(ht1) + Number(ht2));
                    //console.log('ht 1 ' + ht1);

                    //console.log('hitung blok 2.1 Semarang ' + hitungBlok1);
                    //console.log('hitung blok 2.2 semarang ' + hitungBlok2);
                    //console.log('sum blok ' + sumBlok);
                    //console.log('tambah biaya beban ' + tambahBiayaBeban);
                    //console.log('kali 1 ' + kali1);
                    //console.log('kali 2' + kali2);
                    //console.log('total tagihan blok semarang ' + totalTagihan);
                    console.log('rumus 2 blok2 semarang');
                }
            }
            else if (Number(powerCapacitySemarang) == 900) {
                var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
                var t2 = Number(t1) + Number(biayaBeban);
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));

                console.log('pemakaian ' + t1);
                console.log('pemakaian + biaya beban ' + t2);
                console.log('sudah tambah ppj ' + t3);
            }
            else {

            }
            /*
            if (kondisiSemarang <= minKWH) {
                var t1 = Number(minKWH) * Number(tarifLWBP);
                var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
                var t3 = Number(t2) + Number(biayaBeban);
                var t4 = Number(t3) + Number(biayaAdmin) + Number(tagMin);

                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));
                console.log('sermarang 1');
            }
            else if (kondisiSemarang > minKWH) {
                var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
                var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
                var t3 = Number(t2) + Number(biayaBeban);
                var t4 = Number(t3) + Number(biayaAdmin) + Number(tagMin);

                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));
                console.log('sermarang 2');

                console.log('rumus 2 blok2');
            }
            else {

            }
            */
        }

        // Bima
    if (kodeCabang == 17) {
        console.log('Mulai ngecek 1');
        var t1 = Number(usedLWBP) * Number(tarifLWBP) * Number(multiplyFactor);
        var t2 = (1.25 * Number(t1)) + Number(biayaBeban);
        var t3 = Number(t2) + (Number(t1) * Number(persenPPJU) / 100);

        totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
       
        }

        // 2. CABANG RAME2 //kodeCabang == 12 ||
        if (kodeCabang == 18 || kodeCabang == 3 || kodeCabang == 25 || kodeCabang == 31 || kodeCabang == 29 || kodeCabang == 11) {
            var kondisiRame = Number(usedLWBP) * Number(multiplyFactor);
            if (kondisiRame <= minKWH) {
                /*
                RUMUS
                tagihan=minkwh*tarifLWBP + beban
                tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
                tagihan = tagihan - (tagihan*reduksi/100)
                */

                //var t1 = Number(minKWH) * Number(tarifLWBP); (pas commit dinyalakan yg ini)
                var t1 = Number(minKWH) * Number(parseFloat(tarifLWBP.replace(",", "."))); //ini ditutup
                var t2 = Number(t1) + Number(biayaBeban);
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));

                console.log('rame2 satu');
                
            }
            else if (kondisiRame > minKWH) {
                /*
                tagihan=qty(LWBP)*faktorkali*tarifLWBP + beban
                tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
                tagihan = tagihan - (tagihan*reduksi/100)
                */
                var t1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + Number(biayaBeban);
                var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
                var t3 = Number(t2) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
                console.log('perhitungan pertama rame2 rumus 2 ' + t1);
                console.log('perhitungan pertama rame2 rumus 2 total tagihan ' + totalTagihan);

                console.log('t1 rame2 dua ' + t1);
                console.log('t2 rame2 dua ' + t2);
                console.log('t3 rame2 dua ' + t3);

                console.log('rumus 2 rame2');
            }
            else {
                // do nothing
            }
        }

        var maxRUsedBlok1 = maxRangeUsedBLOk1;
        // Tanjung Wangi
        if (kodeCabang == 7) {
            var kondisiRame = Number(usedLWBP) * Number(multiplyFactor);
            if (kondisiRame <= minKWH && daya>900) {
                /*
                RUMUS
                tagihan=minkwh*tarifLWBP + beban
                tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
                tagihan = tagihan - (tagihan*reduksi/100)
                */

                var t1 = Number(minKWH) * Number(tarifLWBP);
                var t2 = Number(t1) + Number(biayaBeban);
                var t3 = Number(t2) + (Number(t2) * Number(persenPPJU) / 100);
                var t4 = Number(t3) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t4) - (Number(t4) * Number(persenREDUKSI) / 100));

                console.log('rame2 satu');
            }
            else if (kondisiRame > minKWH && daya>900) {
                /*
                tagihan=qty(LWBP)*faktorkali*tarifLWBP + beban
                tagihan= tagihan + (tagihan*ppj/100) + biayaadmin + tagMin
                tagihan = tagihan - (tagihan*reduksi/100)
                */
                var t1 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + Number(biayaBeban);
                var t2 = Number(t1) + (Number(t1) * Number(persenPPJU) / 100);
                var t3 = Number(t2) + Number(tagMin); //Number(biayaAdmin) +
                totalTagihan = Math.round(Number(t3) - (Number(t3) * Number(persenREDUKSI) / 100));
                console.log('perhitungan pertama rame2 rumus 2 ' + t1);
                console.log('perhitungan pertama rame2 rumus 2 total tagihan ' + totalTagihan);

                console.log('t1 rame2 dua ' + t1);
                console.log('t2 rame2 dua ' + t2);
                console.log('t3 rame2 dua ' + t3);

                console.log('rumus 2 rame2');
            }
            else if (kodeCabang == 7 && daya < 1300) {
                // Skema Grading untuk tanjung wangi 2
                //cek pemakain kwh apakah lebih besar dari maxrangeusedblok1, jika tidak maka langsung hitung tagihan
                //kalau lebih besar maka tagihan dikenakan tarif blok1 dan blok2 sesuai meternya
                // Skema Grading
                var pemakaianBlok1 = usedBLOK1;
                var maxRangeBlok1 = maxRangeUsedBLOk1;

                if (Number(pemakaianBlok1) <= Number(maxRangeBlok1)) {
                    var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                    var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                    var kali1 = 1.25 * Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                    totalTagihan = Math.round(Number(kali2));

                    console.log('rumus 2 blok1');
                } else {
                    var hitungBlok1 = Number(maxRangeBlok1) * Number(tarifBLOK1);
                    var hitungBlok2 = (Number(usedBLOK1) - Number(maxRangeBlok1)) * Number(tarifBLOK2);

                    var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                    var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                    var kali1 = 1.25 * Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                    totalTagihan = Math.round(Number(kali2));

                    console.log('rumus 2 blok2');
                }
            }
            else {
                // do nothing
            }
        }


        // 3. CABANG MAUMERE
        if (kodeCabang == 21) {
            var kondisiMaumere = Number(usedLWBP) * Number(multiplyFactor);
            //console.log('k mau ' +kondisiMaumere);
            if (kondisiMaumere == 0) {
                //tagihan=1.25*(1.1*beban)
                var totTag = 1.25 * (1.1 * Number(biayaBeban));
                totalTagihan = Math.round(totTag);
                console.log('biaya beban mau' + biayaBeban);
            }
            else if (kondisiMaumere != 0 && (kondisiMaumere <= minKWH)) {
                /*
                if QTY LWBP USED*Multiply Factor != 0 and QTY LWBP USED*Multiply Factor < MIN USED
                tagihan=(1,25*minkwh* tarifLWBP) + (minkwh* tarifLWBP* PPJU/100) + beban
                */
                var t1 = 1.25 * Number(minKWH) * Number(tarifLWBP);
                var t2 = Number(minKWH) * Number(tarifLWBP) * ((persenPPJU) / 100);
                totalTagihan = Math.round(Number(t1) + Number(t2) + Number(biayaBeban));

            }
            else if (kondisiMaumere > minKWH){ 
                /*
                if QTY LWBP USED*Multiply Factor > MIN USED
                tagihan=(1,25*qty(LWBP)*faktorkali* tarifLWBP) + (qty(LWBP)*faktorkali * tarifLWBP*PPJU/100) + beban
                */
                var t1 = 1.25 * Number(usedLWBP) * Number(tarifLWBP);
                var t2 = (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) * (Number(persenPPJU) / 100);
                totalTagihan = Math.round(Number(t1) + Number(t2) + Number(biayaBeban));
              
            }
            else {
                console.log('else cabang maumere');
            }
        }

        // 4. CABANG LEMBAR 
        var lembarSatuCond = Number(usedLWBP) * Number(multiplyFactor);
        var lembarDuaCond = (Number(usedLWBP) * Number(multiplyFactor)) + (Number(usedWBP) * Number(multiplyFactor));

        console.log('daya ' + daya);
        if (kodeCabang == 15) { //lembar itu 15 bukan 24
            if (daya >= 1300 && daya <= 200000) {   //1300 <= daya <= 200000
                // if QTY LWBP USED*Multiply Factor &lt; MIN USED
                console.log('daya' + daya);
                if (Number(lembarSatuCond) <= Number(minKWH)) {
                    console.log('usedlwbp ' + usedLWBP);
                    console.log('AA1');
                    // Lembar 1.1
                    // tagihan=(1.25*(minkwh*tarifLWBP+beban)) + ((minkwh*tarifLWBP+beban)*PPJU/100)
                 //   var t1 = 1.25 * ((Number(minKWH) * Number(tarifLWBP)) + Number(biayaBeban));
                    var t1 = 1.25 * ((Number(minKWH) * Number(tarifLWBP)) + Number(biayaBeban));
                    console.log('t1 = ' + t1)
                    var t2 = (Number(minKWH) * Number(tarifLWBP) + Number(biayaBeban)) * (Number(persenPPJU) / 100);
                    console.log('t2 = ' + t2)
                    totalTagihan = Math.round(Number(t1) + Number(t2));

                    console.log('minkwh ' + minKWH);
                    console.log('tariflwbp ' + tarifLWBP);
                    console.log('biaya beban ' + biayaBeban);

                  console.log('TOTAL AA1 = ' + totalTagihan);
                }
                else if(Number(lembarSatuCond) > Number(minKWH)){
                    // Lembar 1.2
                    // tagihan=(1.25*(qty(LWBP)*faktorkali*tarifLWBP+beban)) + ((qty(LWBP)*faktorkali*tarifLWBP+beban)*PPJU/100)
                    var t1 = 1.25 * (Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP) + Number(biayaBeban));
                    var t2 = ((Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP)) + Number(biayaBeban)) * (Number(persenPPJU)/100);

                    totalTagihan = Math.round(Number(t1) + Number(t2));
                    console.log('AA2');

                }else{
                    // Gak ngapa2in
                    console.log('AA3');
                }
            }
            else if (daya > 200000){
                if(Number(lembarDuaCond) <= Number(minKWH)){
                    var t1 = Number(minKWH) * Number(tarifWBP);
                    var t2 = (Number(minKWH) * Number(tarifWBP)) * Number(persenPPJU) / 100;
                    var t3 = Number(t1) + Number(t2);
                    totalTagihan = Math.round(Number(t3) + (Number(t3) * 0.25));
                    console.log('AA4');
                }
                else if(Number(lembarDuaCond) > Number(minKWH)){
                    var t1 = Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP);
                    var t2 = Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP);
                    //var t3 = Number(usedLWBP) * Number(multiplyFactor) * Number(tarifLWBP);
                    //var t4 = Number(usedWBP) * Number(multiplyFactor) * Number(tarifWBP)
                    var t3 = Number(t1) + Number(t2); //+ Number(t3) + Number(t4);
                    var t4 = Number(t3) * Number(persenPPJU) / 100;
                    var t5 = (Number(t3) + Number(t4)) * 0.25;
                    var t6 = Number(t3) + Number(t4) + Number(t5);
                    console.log('AA5');

                    console.log('t1 ' + t1);
                    console.log('t2 ' + t2);
                    console.log('t3' + t3);
                    console.log('t4 ' + t4);
                        
                    totalTagihan = Math.round(Number(t6));

                }
                else{
                    // do nothing
                    console.log('AA6');
                }

            }
            else if (daya < 1300) {
                // Skema Grading
                var pemakaianBlok1 = usedBLOK1;
                var maxRangeBlok1 = maxRangeUsedBLOk1;
                console.log('max used blok ' + maxRangeBlok1);
                console.log('used blok 1 ' + pemakaianBlok1);
                console.log('AA7');
                if (Number(pemakaianBlok1) <= Number(maxRangeBlok1)) {
                    console.log('PERHITUNGAN USED KONDISI PERTAMA');
                    var hitungBlok1 = Number(usedBLOK1) * Number(tarifBLOK1);
                    var tambahBiayaBeban = Number(hitungBlok1) + Number(biayaBeban);
                    var kali1 = 1.25 * Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));

                    console.log('hitungb1 ' + hitungBlok1);
                    console.log('tambahbeban ' + tambahBiayaBeban);
                    console.log('kali1 ' + kali1);
                    console.log('ppju ' + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100)));
                   
                    totalTagihan = Math.round(Number(kali2));
                }
                else if (Number(pemakaianBlok1) > Number(maxRangeUsedBLOk1)) {
                    console.log('AA8');
                    var x = maxRangeUsedBLOk1;
                    var hitungBlok1 = Number(maxRangeUsedBLOk1) * Number(tarifBLOK1);
                    var hitungBlok2 = (Number(usedBLOK1) - Number(x)) * Number(tarifBLOK2);

                    var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
                    var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
                    var kali1 = 1.25 * Number(tambahBiayaBeban);
                    var kali2 = Number(kali1) + (Number(tambahBiayaBeban) * (Number(persenPPJU) / 100));
                    totalTagihan = Math.round(Number(kali2));

                }
                else {
                    
                }              
            }
            else{
                // do nothing
            }
        }


        // CABANG TANJUNG WANGI SAMA SEPERTI RAME2 DG SKEMA GRADINGNYA
        /*
        if (kodeCabang == 7) {
            // Skema Grading
            var hitungBlok1 = Number(maxRangeUsedBLOk1) * Number(tarifBLOK1);
            var hitungBlok2 = (Number(usedBLOK2) - Number(maxRangeUsedBLOK2)) * Number(tarifBLOK2);

            var sumBlok = Number(hitungBlok1) + Number(hitungBlok2);
            var tambahBiayaBeban = Number(sumBlok) + Number(biayaBeban);
            var kali1 = 1.25 * Number(tambahBiayaBeban);
            var kali2 = Number(tambahBiayaBeban);
            totalTagihan = Number(kali1) + Number(kali2);
        }
        */

        //Format Total Price
    //cari rate
    var rate = 1;
    $("#table-pricing tbody").each(function () {
        currency = $(this).find('td:eq(3)').html();
        if (currency == 'USD') {
            $.ajax({
                type: "GET",
                url: "/TransContractOffer/GetDataCurrencyRate",
                contentType: "application/json",
                dataType: "json",
                data: {},
                success: function (data) {
                    rate = data.Data;
                    totalUsd = totalTagihan;
                    totalTagihan = totalTagihan * rate;
                    fTotalTagihan = totalTagihan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    fTotalTagihanUsd = totalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    console.log("ths " + totalTagihan + " - " + fTotalTagihan);
                    $('#TOTAL_PRICE').val(fTotalTagihan);
                    $('#TOTAL_PAYMENT').val(fTotalTagihan);
                    $('#TOTAL_USD').val(fTotalTagihanUsd);
                    $('#RATE').val(rate);
                }
            });
        } else {
            totalUsd = totalTagihan;
            fTotalTagihan = totalTagihan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            fTotalTagihanUsd = totalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            console.log("ths " + totalTagihan + " - " + fTotalTagihan);
            $('#TOTAL_PRICE').val(fTotalTagihan);
            $('#TOTAL_PAYMENT').val(fTotalTagihan);
            $('#TOTAL_USD').val(fTotalTagihanUsd);
            $('#TOTAL_USD').hide();
            $('#USD').hide();
            $('#RATE').val(1);
        }
    });
    

    
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricityAdd.init();

        $.extend($.expr[":"], {
            containsExact: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 return function (elem) {
                     return $.trim(elem.innerHTML.toLowerCase()) === text.toLowerCase();
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 return $.trim(elem.innerHTML.toLowerCase()) === match[3].toLowerCase();
             },

            containsExactCase: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 return function (elem) {
                     return $.trim(elem.innerHTML) === text;
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 return $.trim(elem.innerHTML) === match[3];
             },

            containsRegex: $.expr.createPseudo ?
             $.expr.createPseudo(function (text) {
                 var reg = /^\/((?:\\\/|[^\/]) )\/([mig]{0,3})$/.exec(text);
                 return function (elem) {
                     return RegExp(reg[1], reg[2]).test($.trim(elem.innerHTML));
                 };
             }) :
             // support: jQuery <1.8
             function (elem, i, match) {
                 var reg = /^\/((?:\\\/|[^\/]) )\/([mig]{0,3})$/.exec(match[3]);
                 return RegExp(reg[1], reg[2]).test($.trim(elem.innerHTML));
             }

        });
    });
}

function getCoaProduksi() {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiElectricity",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- ChoOse COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(".coa_prod").html('');
            $(".coa_prod").append(listItems);
        }
    });
}
