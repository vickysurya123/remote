﻿//--- AUTOCOMPLETE SERVICE CODE
$('.REQUEST_NAME').autocomplete({
    source: function (request, response) {
        var inp = $('.REQUEST_NAME').val();
        var l = inp.length;
        console.log(l);

        $.ajax({
            type: "POST",
            url: "/TransContractOffer/GetDataRequestName",
            data: "{ REQUEST_NAME:'" + inp + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                response($.map(data, function (el) {
                    return {
                        label: el.label,
                        value: "",
                        code: el.code
                    };
                }));
            }
        });

    },
    select: function (event, ui) {
        event.preventDefault();
        this.value = ui.item.code;
        $("#RENTAL_REQUEST_NAME").val(ui.item.label);
        $("#RENTAL_REQUEST_NO").val(ui.item.code);
    }
});

var TableDatatablesEditable = function () {

    //FUNCTION SUM
    jQuery.fn.dataTable.Api.register('sum()', function () {
        return this.flatten().reduce(function (a, b) {
            if (typeof a === 'string') {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if (typeof b === 'string') {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }

            return a + b;
        }, 0);
    });

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            //var jqTds = $('>td', nRow);
            for (var i = 0, iLen = 23; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }


        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" id="ro_number" class="form-control ro_number">';
            jqTds[1].innerHTML = '<input type="text" id="ro_name" class="form-control ro_name">';
            jqTds[2].innerHTML = '<input type="text" id="land_dimension" class="form-control land_dimension">';
            jqTds[3].innerHTML = '<input type="text" id="building_dimension" class="form-control building_dimension">';
            jqTds[4].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';

            //--- AUTOCOMPLETE SERVICE CODE
            //$('.nama-service-code2').autocomplete({
            //    source: function (request, response) {
            //        var inp = $('.nama-service-code2').val();
            //        var l = inp.length;
            //        console.log(l);

            //        $.ajax({
            //            type: "POST",
            //            url: "/TransVB/GetDataServiceCode",
            //            data: "{ SERVICE_NAME:'" + inp + "'}",
            //            contentType: "application/json; charset=utf-8",
            //            success: function (data) {
            //                response($.map(data, function (el) {
            //                    return {
            //                        label: el.label,
            //                        value: "",
            //                        unit: el.unit,
            //                        code: el.code,
            //                        currency: el.currency,
            //                        price: el.price,
            //                        multiply: el.multiply,
            //                        tax_code: el.tax_code
            //                    };
            //                }));
            //            }
            //        });

            //    },
            //    select: function (event, ui) {
            //        event.preventDefault();
            //        this.value = ui.item.code;
            //        $("#txt-service-name").val(ui.item.label);
            //        $("#unit").val(ui.item.unit);
            //        $("#currency").val(ui.item.currency);
            //        $("#price").val(ui.item.price);
            //        $("#multiply_factor").val(ui.item.multiply);
            //        $("#tax_code").val(ui.item.tax_code);
            //        $("#qty").val(1);
            //        $("#surcharge").val(0);
            //        var tax = Number(Number(ui.item.price) * 0.1).toFixed(2);
            //        var amt = Number(ui.item.price);
            //        var sub = ((Number(ui.item.price) * (Number(ui.item.multiply) / 100)) + (Number(ui.item.price) * 0.1)).toFixed(2);
            //        $("#subtotal").val(sub);
            //        $("#tax_amount").val(tax);
            //        $("#amount").val(amt);
            //        document.getElementById("qty").focus();
            //    }
            //});
                        
        }

        function saveRow(oTable, nRow) {

            var ro_number = $('.ro_number').val();
            var ro_name = $('.ro_name').val();
            var land_dimension = $('.land_dimension').val();
            var building_dimension = $('.building_dimension').val();

            //var namaKemasan = $('.nama-service-code2').text();

            var jqInputs = $('input', nRow);
            oTable.fnUpdate(ro_number, nRow, 0, false);
            oTable.fnUpdate(ro_name, nRow, 1, false);
            oTable.fnUpdate(land_dimension, nRow, 2, false);
            oTable.fnUpdate(building_dimension, nRow, 3, false);
            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 4, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            oTable.fnDraw();
        }

        var table = $('#editable_transrental');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [0, 1, 2, 3, 5, 6, 7, 8, 10, 17, 18, 19, 20, 21],
                    "visible": false,
                },
                */
                {
                    "targets": [0],
                    "width": "15%"
                },
                {
                    "targets": [1],
                    "width": "40%"
                },
                {
                    "targets": [2,3],
                    "width": "20%"
                }
            ]
        });

        var tableWrapper = $("#editable_new_transrental");

        var nEditing = null;
        var nNew = false;

        $('#editable_new_transrental').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previous row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    /*
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                    */
                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            /*
            swal({
                title: "Are you sure?",
                text: "Are you sure want to delete this detail.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    swal("Deleted!", "Data Deleted Successfully.", "success");
                }
                else {
                }
            });
            */


            //Konfirmasi akan hapus data detail

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted!");


        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            var localstore = 0;
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                var x = $("#subtotal").val();
                var t = $("#TOTAL").val();
                var total = Number(t) + Number(x);
                $("#TOTAL").val(total);

                /*
                var asdf = localStorage.setItem("hitung", total);
                var retrievedObject = localStorage.getItem('hitung');
                var ser = retrievedObject + Number(x) ;
                console.log(ser);
                */

                //console.log(x);
                //console.log(t);
                //console.log(total);

                saveRow(oTable, nEditing);
                nEditing = null;
                swal('Success', 'Detail Data Updated Successfully', 'success');
                //alert("Updated!");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = false; // tambahan
            }
        });

        $('#btn-update').click(function () {

            var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
            var BE_ID = $('#BE_ID').val();
            var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
            var CUSTOMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            var OLD_CONTRACT = $('#OLD_CONTRACT').val();
            var STATUS = $('#STATUS').val();


            var param = {
                RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                BE_ID: BE_ID,
                RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                CUSTOMER_ID: CUSTOMER_ID,
                CUSTOMER_NAME: CUSTOMER_NAME,
                CONTRACT_START_DATE: CONTRACT_START_DATE,
                CONTRACT_END_DATE: CONTRACT_END_DATE,
                CUSTOMER_AR: CUSTOMER_AR,
                OLD_CONTRACT: OLD_CONTRACT,
                STATUS: STATUS
            };

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransRentalRequest/SaveHeader",
                    timeout: 30000,

                    success: function (data) {
                        var count_dt = table.fnGetData();
                        var grandtotal = 0;

                        for (var i = 0; i < count_dt.length; i++) {
                            var item = table.fnGetData(i);
                            var param2 = {
                                RO_NUMBER: item[0],
                                LAND_DIMENSION: item[1],
                                BUILDING_DIMENSION: item[2]
                            };


                            $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param2),
                                method: "POST",
                                url: "/TransRentalRequest/SaveDetail",
                                timeout: 30000,
                                success: function (data) {
                                    console.log(data)
                                },
                                error: function (data) {
                                    console.log(data.responeText);
                                }

                            });
                        }
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransRentalRequest";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransRentalRequest";
                            });
                        }
                    },
                    error: function (data) {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/TransRentalRequest";
                        });
                        console.log(data.responeText);
                    }
                });
            });

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/TransRentalRequest/GetDataCustomer",
                    data: "{ MPLG_NAMA:'" + inp + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                code: el.code,
                                label: el.label,
                                sap: el.sap
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });


    }
    //---SELECT 2 SERVICES CODE
    var getServicesCode = function (xxx) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.SERVICE_CODE + " (" + repo.SERVICE_NAME + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.SERVICE_NAME || repo.text;
        }

        $(".nama-service-code").select2({

            allowClear: true,
            width: "on",
            ajax: {
                url: "/TransVB/GetDataServiceCodeSelect2",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: item.data.SERVICE_CODE };
                callback(data);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,

            placeholder: "ketikkan service name"
        });
    }

    var selectingServiceCode = function () {
        $(".nama-service-code").on("select2:select", function (e) {
            console.log("ok");
            var service_code = e.params.data.SERVICE_CODE;
            var service_name = e.params.data.SERVICE_NAME;
            var nRow = $(this).parents('tr')[0];
            var aData = $('#editable_transvb').dataTable().fnGetData(nRow);
            $('#editable_transvb').dataTable().fnUpdate(service_code, nRow, 2, false);
            $('#editable_transvb').dataTable().fnUpdate(service_name, nRow, 3, false);

            //$('#editable_transvb').dataTable().fnUpdate(namaGudlap, nRow, 4, false);
        });
    }
    //--- END OF SELECT 2 OTHER SERVICES

    var cekPiut = function (doc_type, cust_code) {
        $.ajax({
            type: "POST",
            url: "/TransRentalRequest/cekPiutangSAP",
            data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransRentalRequest";
        });
    } 

    return {

        init: function () {
            handleTable();
            batal();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesEditable.init();
});