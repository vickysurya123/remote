﻿var Role = function () {

    var initTable = function () {
        $('#table-role').DataTable({
            "ajax":
            {
                "url": "Role/GetDataRole",
                "type": "GET",

            },
            "columns": [
                {
                    "data": "ROLE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "ROLE_NAME"
                },
                {
                    "data": "PROPERTY_ROLE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

        $("#PROPERTY_ROLE").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });

        $("#PROPERTY_ROLE").inputmask("numeric", {
            min: 0,
            max: 999
        });
    }
    
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var rTable = $('#table-role').DataTable();
            var ROLE_NAME = $('#ROLE_NAME').val();
            var PROPERTY_ROLE = $('#PROPERTY_ROLE').val();

            if (ROLE_NAME && PROPERTY_ROLE) {
                var param = {
                    ROLE_NAME: ROLE_NAME,
                    PROPERTY_ROLE: PROPERTY_ROLE
                };

                console.log(param);

                $('#addRoleModal').modal('hide');
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "Role/InstDataRole",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            rTable.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            rTable.ajax.reload();
                        });
                    }
                });
            } else {
                $('#addRoleModal').modal('hide');
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
        });
    }

    var nStatus = true; //untuk status tambah detail
    var nEditing = null;
    var nNew = false;
    var editData = function () {

        var oTable = $('#table-role').dataTable();

        var DetailLookup = function (oTable, nRow) {
            var data = oTable.fnGetData(nRow);
            $("#txtRoleName").val(data["ROLE_NAME"]);
            $("#txtRoleProperty").val(data["PROPERTY_ROLE"]);
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[1].innerHTML = '<center><input id="txtRoleName" type="text" class="form-control" ></center>';
            jqTds[2].innerHTML = '<center><input id="txtRoleProperty" type="text" class="form-control input-xsmall" ></center>';
            jqTds[3].innerHTML = '<center><a class="btn btn-icon-only green" href="#" id="btn-sUbahD"><i class="fa fa-save"></i></a>&nbsp; ' +
                                 '<a class="btn btn-icon-only red" href="#" id="btn-cUbahD"><i class="fa fa-ban"></i></a></center>';
            
            $("#txtRoleProperty").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            $("#txtRoleProperty").inputmask("numeric", {
                min: 0,
                max: 999
            });
        }

        function saveRow(oTable, nRow) {
            App.blockUI({ boxed: true });
            var data = oTable.fnGetData(nRow);
            var rtable = $('#table-role').DataTable();
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate('<center><a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a></center>', nRow, 3, false);
            oTable.fnDraw();
            
            if (ROLE_NAME && PROPERTY_ROLE) {
                var param = {
                    ROLE_ID: data["ROLE_ID"],
                    ROLE_NAME: jqInputs[0].value,
                    PROPERTY_ROLE: jqInputs[1].value,
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "Role/EditDataRole",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            rtable.ajax.reload();
                            nStatus = true;
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            rtable.ajax.reload();
                        });
                        $('#addRoleModal').modal('hide');
                    }
                });
            } else {
                //$('#addRoleModal').modal('hide');
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
        }

        $('body').on('click', 'tr #btn-ubah', function () {
            if (nStatus == true) {
                //console.log("Ubah 1");
                //console.log(nStatus);

                var nRow = $(this).parents('tr')[0];
                editRow(oTable, nRow);
                nStatus = false

                DetailLookup(oTable, nRow);

            } else {
                swal('Peringatan', "Anda belum selesai melakukan inputan.", 'warning');
                //console.log("Ubah 2");
                //console.log(nStatus);
            }
        });

        $('body').on('click', 'tr #btn-sUbahD', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            if (nStatus == true) {
                //console.log("3");
                //console.log(nStatus);
            } else {
                saveRow(oTable, nRow);
                //console.log("4");
                //console.log(nStatus);
            }
        });

        $('body').on('click', 'tr #btn-cUbahD', function (e) {
            e.preventDefault();
            var rTable = $('#table-role').DataTable();
            nStatus = true;
            //var nRow = $(this).parents('tr')[0];
            //var aData = oTable.fnGetData(nRow);
            //oTable.fnUpdate("", nRow, 2, false);

            rTable.ajax.reload();
        });
    }
    
    var clear = function () {
        $('input').val('');
    }

    var tombolCancel = function () {
        $('#btn-bataladd').click(function () {
            clear();
        });
    }

    return {
        init: function () {
            initTable();
            simpan();
            editData();
            tombolCancel();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Role.init();
    });
}