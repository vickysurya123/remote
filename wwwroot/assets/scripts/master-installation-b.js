﻿$('#BIAYA_ADMIN').mask('000.000.000.000.000', { reverse: true });
$('#MIN_PAYMENT').mask('000.000.000.000.000', { reverse: true });

var Installation = function () {

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/InstallationB/AddInstallationB';
        });
    }

    var addPricing = function () {
        $('body').on('click', 'tr #btn-price', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();
            window.location.href = '/InstallationB/AddInstallationPricingB/'+data["ID"];
        });
    }


    var mdm = function () {
        $('#CUSTOMER_MDM').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/InstallationB/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_IDX").val(ui.item.code);
                $("#CUSTOMER_SAP_AR").val(ui.item.sap);
                $("#INSTALLATION_ADDRESS").val(ui.item.address);

            }
        });
    }

    var mdmEdit = function () {
        $('#CUSTOMER_MDM_EDIT').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM_EDIT').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/InstallationB/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_IDX_EDIT").val(ui.item.code);
                $("#CUSTOMER_SAP_AR_EDIT").val(ui.item.sap);
                $("#INSTALLATION_ADDRESS_EDIT").val(ui.item.address);

            }
        });
    }

    var kembali = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/InstallationB';
        });
    }

    var ok = function () {
        $('#btn-ok').click(function () {
            var uriId = $('#UriId').val();
            console.log(uriId);
            var PROFIT_CENTER = $('#PROFIT_CENTER_EDIT').val();
            var CUSTOMER_NAME = $('#CUSTOMER_MDM_EDIT').val();
            var CUSTOMER_ID = $('#CUSTOMER_IDX_EDIT').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR_EDIT').val();
            var INSTALLATION_DATE = $('#INSTALLATION_DATE_EDIT').val();
            var POWER_CAPACITY = $('#POWER_CAPACITY_EDIT').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS_EDIT').val();
            var TARIFF_CODE = $('#TARIFF_CODE_EDIT').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR_EDIT').val();
            var RO_CODE = $('#RO_NUMBER_EDIT').val();
            var MINIMUM_PAYMENT = $('#MIN_PAYMENT_EDIT').val();
            var MINIMUM_AMOUNT = $('#MINIMUM_AMOUNT_EDIT').val();
            var MULTIPLY_FACT = $('#MULTIPLY_FACTOR_EDIT').val();
            var MINIMUM_USED = $('#MIN_USED_EDIT').val();
            var BIAYA_ADMIN = $('#BIAYA_ADMIN_EDIT').val();
            var SERIAL_NUMBER = $('#SERIAL_NUMBER_EDIT').val();

            if (CUSTOMER_ID && INSTALLATION_ADDRESS && POWER_CAPACITY && MULTIPLY_FACT) {
                var param = {
                    ID : uriId,
                    PROFIT_CENTER: PROFIT_CENTER,
                    CUSTOMER_ID: CUSTOMER_ID,
                    CUSTOMER_NAME: CUSTOMER_NAME,
                    CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                    INSTALLATION_DATE: INSTALLATION_DATE,
                    POWER_CAPACITY: POWER_CAPACITY,
                    INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                    TARIFF_CODE: TARIFF_CODE,
                    MINIMUM_AMOUNT: MINIMUM_AMOUNT,
                    RO_CODE: RO_CODE,
                    MINIMUM_USED: MINIMUM_USED,
                    MINIMUM_PAYMENT: MINIMUM_PAYMENT,
                    MULTIPLY_FACT: MULTIPLY_FACT,
                    BIAYA_ADMIN: BIAYA_ADMIN,
                    SERIAL_NUMBER: SERIAL_NUMBER,
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/UpdateDataHeader",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/InstallationB";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/InstallationB";
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var initTableInstallation = function () {
        $('#table-installation').DataTable({
            "ajax": {
                "url": "InstallationB/GetDataInstallationB",
                "type": "GET",
            },
            "columns": [
                {
                    "data": "INSTALLATION_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "SERIAL_NUMBER",
                },
                {
                    "data": "INSTALLATION_TYPE",
                },
                {
                    "data": "PROFIT_CENTER",
                },
                {
                    "data": "RO_CODE",
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "TARIFF_CODE",
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                        var aksi = '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                        aksi += '<a class="btn btn-icon-only green" id="btn-price"><i class="fa fa-gear"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var CUSTOMER_ID = $('#CUSTOMER_IDX').val();
            var CUSTOMER_NAME = $('#CUSTOMER_MDM').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR').val();
            var INSTALLATION_DATE = $('#INSTALLATION_DATE').val();
            var POWER_CAPACITY = $('#POWER_CAPACITY').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS').val();
            var TARIFF_CODE = $('#TARIFF_CODE').val();
            var MINIMUM_AMOUNT = $('#MINIMUM_AMOUNT').val();
            var RO_NUMBER = $('#RO_NUMBER').val();
            var MIN_USED = $('#MIN_USED').val();
            var MIN_PAYMENT = $('#MIN_PAYMENT').val();
            var MULTIPLY_FACTOR = $('#MULTIPLY_FACTOR').val();
            var BIAYA_BEBAN = $('#BIAYA_BEBAN').val();
            var BIAYA_ADMIN = $('#BIAYA_ADMIN').val();
            var nfBiayaAdmin = parseFloat(BIAYA_ADMIN.split('.').join(''));
            var nfMinPayment = parseFloat(MIN_PAYMENT.split('.').join(''));
            var SERIAL_NUMBER = $('#SERIAL_NUMBER').val();

            if (INSTALLATION_TYPE && CUSTOMER_ID && INSTALLATION_ADDRESS && POWER_CAPACITY && MULTIPLY_FACTOR && MIN_PAYMENT && BIAYA_ADMIN) {
                var param = {
                    INSTALLATION_TYPE: INSTALLATION_TYPE,
                    PROFIT_CENTER: PROFIT_CENTER,
                    CUSTOMER_ID: CUSTOMER_ID,
                    CUSTOMER_NAME: CUSTOMER_NAME,
                    CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                    INSTALLATION_DATE: INSTALLATION_DATE,
                    POWER_CAPACITY: POWER_CAPACITY,
                    INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                    TARIFF_CODE: TARIFF_CODE,
                    MINIMUM_AMOUNT: MINIMUM_AMOUNT,
                    RO_NUMBER: RO_NUMBER,
                    MINIMUM_USED: MIN_USED,
                    MINIMUM_PAYMENT: nfMinPayment.toString(),
                    MULTIPLY_FACT: MULTIPLY_FACTOR,
                    BIAYA_BEBAN: BIAYA_BEBAN,
                    BIAYA_ADMIN: nfBiayaAdmin.toString(),
                    SERIAL_NUMBER: SERIAL_NUMBER
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/AddData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/InstallationB";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/InstallationB";
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();
            window.location = "/InstallationB/EditInstallation/" + data["INSTALLATION_NUMBER"];
        });
    }

    var detail = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();

            var idx = data["ID"];
            console.log(idx);

            $("#DETILINSTALLATION_CODE").val(data["INSTALLATION_CODE"]);
            $("#SERIAL_NUMBER").val(data["SERIAL_NUMBER"]);
            $("#DETILINSTALLATION_TYPE").val(data["INSTALLATION_TYPE"]);
            $("#DETILBE_ID").val(data["PROFIT_CENTER"]);
            $("#DETILCUSTOMER_MDM").val(data["CUSTOMER_NAME"]);
            $("#DETILCUSTOMER_ID").val(data["CUSTOMER_ID"]);
            $("#DETILCUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            $("#DETILINSTALLATION_DATE").val(data["INSTALLATION_DATE"]);
            $("#DETILPOWER_CAPACITY").val(data["POWER_CAPACITY"]);
            $("#DETILINSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETILTARIFF_CODE").val(data["TARIFF_CODE"]);
            $("#DETILTAX_CODE").val(data["TAX_CODE"]);
            $("#DETILMINIMUM_AMOUNT").val(data["MINIMUM_AMOUNT"]);
            $("#DETILCURRENCY").val(data["CURRENCY"]);
            $("#DETILRO_NUMBER").val(data["RO_CODE"]);
            $("#MINIMUM_PAYMENT").val(data["MINIMUM_PAYMENT"]);
            $("#MULTIPLY_FACT").val(data["MULTIPLY_FACT"]);
            $("#BIAYA_BEBAN").val(data["BIAYA_BEBAN"]);
            $("#MINIMUM_USED").val(data["MINIMUM_USED"]);

            var tablePricingx = $('#table-view-pricing').DataTable();
            tablePricingx.destroy();

            var tableCostingx = $('#table-view-costing').DataTable();
            tableCostingx.destroy();

            //Data table pricing
            $('#table-view-pricing').DataTable({
                "ajax":
                {
                    "url": "/InstallationB/getDataPricing/" + idx,
                    "type": "GET",

                },
                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PRICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MAX_RANGE_USED",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Data table costing
            $('#table-view-costing').DataTable({
                "ajax":
                {
                    "url": "/InstallationB/getDataCosting/" + idx,
                    "type": "GET",

                },
                "columns": [
                    {
                        "data": "DESCRIPTION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var status = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["INSTALLATION_NUMBER"],
                        method: "get",
                        url: "InstallationB/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#table-installation').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var lookupCustomer = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('customer'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        remote: {
            url: '/Installation/GetCustomer?query=%QUERY',
            ajax: {
                dataType: 'jsonp'
            },
            wildcard: '%QUERY'
        }
    });

    lookupCustomer.initialize();

    if (App.isRTL()) {
        $('#CUSTOMER_ID').attr("dir", "rtl");
    }
    $('#CUSTOMER_ID').typeahead(null, {
        name: 'dalookup_customer',
        displayKey: 'MPLG_NAMA',
        source: lookupCustomer.ttAdapter(),
        hint: (App.isRTL() ? false : true),
        templates: {
            suggestion: Handlebars.compile([
              '<div class="media">',

                    '<div class="media-body">',
                        '<h4 class="media-heading">{{MPLG_KODE}}</h4>',
                        '<p>{{MPLG_KODE}}-{{MPLG_NAMA}}</p>',
                    '</div>',
              '</div>',
            ].join(''))
        }
    });

    /*
    var changeInstalasi = function () {
        $('#INSTALLATION_TYPE').on('change', function () {
            var instalasi = $('#INSTALLATION_TYPE').val();
            //console.log(instalasi);
            if (instalasi == 'L-Sambungan Listrik') {
                // ajax sambungan listrik

                // Ajax untuk ambil tarif dengan code L
                $.ajax({
                    type: "GET",
                    url: "/InstallationB/GetDataDropDownTarifL",
                    contentType: "application/json",
                    dataType: "json", 
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" +  jsonList.data[i].TARIFF_CODE + "'>" +  jsonList.data[i].AMOUNT + "</option>";
                        }
                        $("#TARIFF_CODE").html('');
                        $("#TARIFF_CODE").append(listItems);
                    }
                });

            }
            else {
                // ajax sambungan air
                // Ajax untuk ambil tarif dengan code L
                $.ajax({
                    type: "GET",
                    url: "/InstallationB/GetDataDropDownTarifA",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                        }
                        $("#TARIFF_CODE").html('');
                        $("#TARIFF_CODE").append(listItems);
                    }
                });
            }
        });
    }
    */

    var pCode = function () {
        $('#PRICE_TYPE').change(function () {
            var PROFIT_CENTER = $('#PROFIT_CENTER_EDIT').val();
            var p = {
                PROFIT_CENTER: PROFIT_CENTER
            }
            $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(PROFIT_CENTER),
                method: "POST",
                url: "/InstallationB/GetDataDropDownTarifL",
                timeout: 30000,
                //type: "GET",
                //url: "/InstallationB/GetDataDropDownTarifL",
                //contentType: "application/json",
                //dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                    }
                    $("#PRICE_CODE").html('');
                    $("#PRICE_CODE").append(listItems);
                }
            });
        });
       
    }

    var simpanPricing = function () {
        $('#btn-simpan-pricing').click(function () {
            var INSTALLATION_ID = $('#INSTALLATION_ID').val();
            var PRICE_TYPE = $('#PRICE_TYPE').val();
            var PRICE_CODE = $('#PRICE_CODE').val();
            var MAX_RANGE_USED = $('#MAX_RANGE_USED').val();
            var INSTALLATION_CODE = $('#INSTALLATION_CODE').val();

            // $('#addMeasurementModal').modal('hide');
            if (PRICE_TYPE && PRICE_CODE) {
                var param = {
                    INSTALLATION_ID:INSTALLATION_ID,
                    PRICE_TYPE: PRICE_TYPE,
                    PRICE_CODE: PRICE_CODE,
                    MAX_RANGE_USED: MAX_RANGE_USED,
                    INSTALLATION_CODE: INSTALLATION_CODE
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/AddPricing"
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success');
                        $('#addPricing').modal('hide');
                        $('#table-pricing').dataTable().api().ajax.reload();
                    } else {
                        swal('Failed', data.message, 'error');
                        $('#addPricing').modal('hide');
                    }
                });

            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                $('#addpricing').modal('hide');
            }

        });
    }

    var updatePricing = function () {
        $('#btn-simpan-edit-pricing').click(function () {
            var ID_EDIT = $('#ID_EDIT').val();
            var PRICE_TYPE = $('#PRICE_TYPE_EDIT').val();
            var PRICE_CODE = $('#PRICE_CODE_EDIT').val();
            var MAX_RANGE_USED = $('#MAX_RANGE_USED_EDIT').val();

            // $('#addMeasurementModal').modal('hide');
            if (PRICE_TYPE && PRICE_CODE) {
                var param = {
                    ID: ID_EDIT,
                    PRICE_TYPE: PRICE_TYPE,
                    PRICE_CODE: PRICE_CODE,
                    MAX_RANGE_USED: MAX_RANGE_USED
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/editPricing"
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success');
                        $('#editPricing').modal('hide');
                        $('#table-pricing').dataTable().api().ajax.reload();
                    } else {
                        swal('Failed', data.message, 'error');
                        $('#addPricing').modal('hide');
                    }
                });

            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                $('#editPricing').modal('hide');
            }

        });
    }

    var initTablePricing = function () {
        var uriId = $('#UriId').val();
        $('#table-pricing').DataTable({
            "ajax":
            {
                "url": "/InstallationB/getDataPricing/" + uriId,
                "type": "GET",

            },
            "columns": [
                {
                    "data": "PRICE_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "PRICE_CODE",
                    "class": "dt-body-center"

                },
                {
                    "data": "MAX_RANGE_USED",
                    "class": "dt-body-center"

                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a data-toggle="modal" href="#editPricing" class="btn default btn-xs green" id="btn-ubah-pricing"><i class="fa fa-edit"></i> </a>';
                         aksi += '<a class="btn default btn-xs red" id="btn-delete-pricing"><i class="fa fa-close"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var btnAddPricing = function () {
        $('#addPricing').on('click', function () {
            //$.clearInput();
        });
    }

    var btnAddCosting = function () {
        $('#addCosting').on('click', function () {
            $.clearInput();
        });
    }

    var btnEditPricing = function () {

        $('body').on('click', 'tr #btn-ubah-pricing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pricing').DataTable();
            var data = table.row(baris).data();
            
            var priceCode = data["PRICE_CODE"];
            var splitCode = priceCode.split(" - ");
            var valPriceCode = splitCode[0];
            //console.log(valPriceCode);

            $("#MAX_RANGE_USED_EDIT").val(data["MAX_RANGE_USED"]);
            $("#ID_EDIT").val(data["ID"]);
            var PROFIT_CENTER = $('#PROFIT_CENTER_EDIT').val();

            $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(PROFIT_CENTER),
                method: "POST",
                url: "/InstallationB/GetDataDropDownTarifL",
                timeout: 30000,
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                    }
                    $("#PRICE_CODE_EDIT").html('');
                    $("#PRICE_CODE_EDIT").append(listItems);

                    $("#PRICE_CODE_EDIT option[value='"+valPriceCode+"']").prop('selected', true);
                    //$find("<%=PRICE_CODE_EDIT.ClientID%>").val(stripHTML1(valPriceCode));  
                }
            });

            var priceType = data["PRICE_TYPE"];
            $("#PRICE_TYPE_EDIT option[value='" + priceType + "']").prop('selected', true);

        });
    }

    var deletePricing = function () {
        $('body').on('click', 'tr #btn-delete-pricing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pricing').DataTable();
            var data = table.row(baris).data();

            console.log(data["ID"]);

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        //data: "id=" + data["ID"],
                        method: "post",
                        url: "/InstallationB/deletePricing/"+data["ID"],
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#table-pricing').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var simpanCosting = function () {
        $('#btn-simpan-costing').click(function () {
            var SERVICES_INSTALLATION_ID = $('#INST_ID').val();
            var DESCRIPTION = $('#DESCRIPTION').val();
            var PERCENTAGE = $('#PERCENT').val();
            var INSTALLATION_CODE = $('#INSTALLATION_CODE').val();


            // $('#addMeasurementModal').modal('hide');
            if (SERVICES_INSTALLATION_ID && DESCRIPTION) {
                var param = {
                    SERVICES_INSTALLATION_ID: SERVICES_INSTALLATION_ID,
                    DESCRIPTION: DESCRIPTION,
                    PERCENTAGE: PERCENTAGE,
                    INSTALLATION_CODE: INSTALLATION_CODE
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/AddCosting"
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success');
                        $('#addCosting').modal('hide');
                        $('#table-costing').dataTable().api().ajax.reload();
                    } else {
                        swal('Failed', data.message, 'error');
                        $('#addCosting').modal('hide');
                    }
                });

            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                $('#addpricing').modal('hide');
            }

        });
    }

    var initTableCosting = function () {
        var uriId = $('#UriId').val();
        $('#table-costing').DataTable({
            "ajax":
            {
                "url": "/InstallationB/getDataCosting/" + uriId,
                "type": "GET",

            },
            "columns": [
                {
                    "data": "DESCRIPTION",
                    "class": "dt-body-center"
                },
                {
                    "data": "PERCENTAGE",
                    "class": "dt-body-center"

                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a data-toggle="modal" href="#editCosting" class="btn default btn-xs green" id="btn-ubah-costing"><i class="fa fa-edit"></i> </a>';
                         aksi += '<a class="btn default btn-xs red" id="btn-delete-costing"><i class="fa fa-close"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var btnEditCosting = function () {
        $('body').on('click', 'tr #btn-ubah-costing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-costing').DataTable();
            var data = table.row(baris).data();

            var description = data["DESCRIPTION"];
            var percent = data["PERCENTAGE"];
            var idedit = data["ID"];

            console.log(percent);

            $("#INST_ID_EDIT").val(idedit);
            $("#PERCENT_EDIT").val(percent);
            $("#DESCRIPTION_EDIT option[value='" + description + "']").prop('selected', true);
        });
    }

    var updateCosting = function () {
        $('#btn-simpan-edit-costing').click(function () {
            var ID_EDIT = $('#INST_ID_EDIT').val();
            var DESCRIPTION_EDIT = $('#DESCRIPTION_EDIT').val();
            var PERCENT_EDIT = $('#PERCENT_EDIT').val();

            // $('#addMeasurementModal').modal('hide');
            if (ID_EDIT && DESCRIPTION_EDIT) {
                var param = {
                    ID: ID_EDIT,
                    DESCRIPTION: DESCRIPTION_EDIT,
                    PERCENTAGE: PERCENT_EDIT
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/InstallationB/editCosting"
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success');
                        $('#editCosting').modal('hide');
                        $('#table-costing').dataTable().api().ajax.reload();
                    } else {
                        swal('Failed', data.message, 'error');
                        $('#addCosting').modal('hide');
                    }
                });

            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                $('#editPricing').modal('hide');
            }

        });
    }

    var deleteCosting = function () {
        $('body').on('click', 'tr #btn-delete-costing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-costing').DataTable();
            var data = table.row(baris).data();

            console.log(data["ID"]);

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        //data: "id=" + data["ID"],
                        method: "post",
                        url: "/InstallationB/deleteCosting/" + data["ID"],
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#table-costing').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var editProfitCenter = function () {
        var kdProfitC = $('#PROFIT_CENTER').val();
        $.ajax({
            type: "GET",
            url: "/InstallationB/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].TERMINAL_NAME + "</option>";
                }
                $("#PROFIT_CENTER_EDIT").html('');
                $("#PROFIT_CENTER_EDIT").append(listItems);

                $("#PROFIT_CENTER_EDIT option[value='" + kdProfitC + "']").prop('selected', true);
            }
        });
    }

    var editAssignmentRO = function () {
        var asssignmentRo = $('#RO_NUMBER').val();
        $.ajax({
            type: "GET",
            url: "/InstallationB/GetDataRental",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Assignment RO --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].RO_CODE + "'>" + jsonList.data[i].RO_NAME + "</option>";
                }
                $("#RO_NUMBER_EDIT").html('');
                $("#RO_NUMBER_EDIT").append(listItems);

                $("#RO_NUMBER_EDIT option[value='" + asssignmentRo + "']").prop('selected', true);
            }
        });
    }

    var tCode = function () {
        $('#PROFIT_CENTER').on('change', function () {
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var PC = $('#PROFIT_CENTER').val();
        
            var p = {
                PROFIT_CENTER: PROFIT_CENTER
            }

            console.log('profit centernya : ' + PC);

            $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(PROFIT_CENTER),
                method: "POST",
                url: "/InstallationB/GetDataDropDownTarifL",
                timeout: 30000,
                //type: "GET",
                //url: "/InstallationB/GetDataDropDownTarifL",
                //contentType: "application/json",
                //dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += '<option value="">-- Choose Biaya Beban --</option>';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                    }
                    $("#TARIFF_CODE").html('');
                    $("#TARIFF_CODE").append(listItems);
                }
            });
        });
    }

    var tCodeEdit = function () {
        $('#PROFIT_CENTER_EDIT').on('change', function () {
            var PROFIT_CENTER = $('#PROFIT_CENTER_EDIT').val();
            var p = {
                PROFIT_CENTER: PROFIT_CENTER
            }

            console.log('profit centernya : ' + PROFIT_CENTER);
            var kodeTarif = $('#TARIFF_CODE').val();
            $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(PROFIT_CENTER),
                method: "POST",
                url: "/InstallationB/GetDataDropDownTarifL",
                timeout: 30000,
                //type: "GET",
                //url: "/InstallationB/GetDataDropDownTarifL",
                //contentType: "application/json",
                //dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += '<option value="">-- Choose Biaya Beban --</option>';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                    }
                    $("#TARIFF_CODE_EDIT").html('');
                    $("#TARIFF_CODE_EDIT").append(listItems);
                    $("#TARIFF_CODE_EDIT option[value='" + kodeTarif + "']").prop('selected', true);
                }
            });
        });
        
    }

    return {
        init: function () {
            add();
            kembali();
            initTableInstallation();
            detail();
            status();
            simpan();
            ubah();
            mdm();
            //changeInstalasi();
            addPricing();
            tCode();
            pCode();
            ok();
            simpanPricing();
            initTablePricing();
            btnAddPricing();
            btnEditPricing();
            updatePricing();
            deletePricing();
            simpanCosting();
            initTableCosting();
            btnEditCosting();
            updateCosting();
            btnAddCosting();
            deleteCosting();
            editProfitCenter();
            editAssignmentRO();
            tCode();
            tCodeEdit();
            mdmEdit();
            gethehe();
        }
    };
}();

function HitungMinUsed() {
    var capacityVA = $('#POWER_CAPACITY').val();
    var hitungMinUsed = 40 * Number(capacityVA) / 1000;
    $('#MIN_USED').val(hitungMinUsed);
}

function HitungMinUsedEdit() {
    var capacityVA = $('#POWER_CAPACITY_EDIT').val();
    var hitungMinUsed = 40 * Number(capacityVA) / 1000;
    $('#MIN_USED_EDIT').val(hitungMinUsed);
}

function gethehe() {
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var prof = $('#PROFIT_CENTER').val();
    //var PROFIT_CENTER = $('#PROFIT_CENTER_EDIT').val();
    //var prof = $('#PROFIT_CENTER_EDIT').val();
    var p = {
        PROFIT_CENTER: prof
    }

    console.log('profit centernya : ' + prof);
    var kodeTarif = $('#TARIFF_CODE').val();
    console.log(kodeTarif);
    $.ajax({
        contentType: "application/json",
        traditional: true,
        data: JSON.stringify(prof),
        method: "POST",
        url: "/InstallationB/GetDataDropDownTarifL",
        timeout: 30000,
        //type: "GET",
        //url: "/InstallationB/GetDataDropDownTarifL",
        //contentType: "application/json",
        //dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += '<option value="">-- Choose Biaya Beban --</option>';
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
            }
            $("#TARIFF_CODE_EDIT").html('');
            $("#TARIFF_CODE_EDIT").append(listItems);
            $("#TARIFF_CODE_EDIT option[value='" + kodeTarif + "']").prop('selected', true);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Installation.init();
    });
}