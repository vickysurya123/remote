﻿var TransSpecialContract = function () {

    var initDetilTransactionSpecial = function () {
        $('body').on('click', 'tr #btn-detail-trans-special', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-special-contract').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            $("#DETAILCONTRACT_NO").val(data["CONTRACT_NO"]);
            $("#DETAILCONTRACT_TYPE").val(data["CONTRACT_TYPE"]);
            $("#DETAILBE_ID").val(data["BE_ID"]);
            $("#DETAILCONTRACT_NAME").val(data["CONTRACT_NAME"]);
            $("#DETAILCONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#DETAILCONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#DETAILTERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#DETAILBUSINESS_PARTNER_NAME").val(data["BUSINESS_PARTNER_NAME"]);
            $("#DETAILBUSINESS_PARTNER").val(data["BUSINESS_PARTNER"]);
            $("#DETAILCUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#DETAILPROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#DETAILCONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#DETAILIJIN_PRINSIP_NO").val(data["IJIN_PRINSIP_NO"]);
            $("#DETAILIJIN_PRINSIP_DATE").val(data["IJIN_PRINSIP_DATE"]);
            $("#DETAILLEGAL_CONTRACT_NO").val(data["LEGAL_CONTRACT_NO"]);
            $("#DETAILLEGAL_CONTRACT_DATE").val(data["LEGAL_CONTRACT_DATE"]);
            $("#DETAILCURRENCY").val(data["CURRENCY"]);
            $("#DETAILMONTHS").val(data["TERM_IN_MONTHS"]);

            var id_special = data["CONTRACT_NO"];

            $('#table-detil-object').DataTable({

                "ajax":
                {
                    "url": "/TransSpecialContract/GetDataDetailObject/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-condition').DataTable({

                "ajax":
                {
                    "url": "/TransSpecialContract/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_1",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    },


                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //View detil manual
            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransSpecialContract/GetDataDetailManualy/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }

                },
                "columns": [
                     {
                         "data": "MANUAL_NO",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "CONDITION",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "DUE_DATE",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "NET_VALUE",
                         "class": "dt-body-center",
                         render: $.fn.dataTable.render.number('.', '.', 0)
                     },
                     {
                         "data": "QUANTITY",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "UNIT",
                         "class": "dt-body-center"
                     }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detil memo
            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransSpecialContract/GetDataDetailMemo/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }

                },

                "columns": [
                    {
                        "data": "OBJECT_CALC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var initTableTransSpecialContract = function () {
        $('#table-trans-special-contract').DataTable({

            "ajax":
            {
                "url": "TransSpecialContract/GetDataTransSpecialContract",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },

            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NAME"
                },
                {
                    "data": "BUSINESS_PARTNER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS == '1') {
                            if (full.CONTRACT_STATUS === 'APPROVED') {
                                return '<span class="label label-sm label-success"> APPROVED </span>';
                            }
                        }
                        else {
                            if (full.CONTRACT_STATUS === 'TERMINATED') {
                                return '<span class="label label-sm label-danger"> TERMINATED </span>';
                            }
                            else {
                                return '<span class="label label-sm label-danger"> INACTIVE </span>';
                            }
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         console.log("full statusnya = " + full.STATUS);
                         if (full.STATUS == '1') {
                             //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                             var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                             aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Inactive Contract" id="btn-status"><i class="fa fa-exchange"></i></a>';
                             aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah-contract"><i class="fa fa-edit"></i></a>';
                             aksi += '<a data-toggle="modal" href="#viewTransSpecialContractModal" class="btn btn-icon-only green" id="btn-detail-trans-special" data-toggle="tooltip" title="Detail Contract"><i class="fa fa-eye"></i></a>';
                         }
                         else {
                             //var aksi = '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                             var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                             aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Inactive Contract" id="btn-status" disabled><i class="fa fa-exchange"></i></button>';
                             aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah-contract" disabled><i class="fa fa-edit"></i></button>';
                             aksi += '<a data-toggle="modal" href="#viewTransSpecialContractModal" class="btn btn-icon-only green" id="btn-detail-trans-special" data-toggle="tooltip" title="Detail Contract"><i class="fa fa-eye"></i></a>';
                         }
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var editContract = function () {
       $('body').on('click', 'tr #btn-ubah-contract', function () {
           var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-special-contract').DataTable();
            var data = table.row(baris).data();
            window.location = "/ContractChange/EditContract/" + data["CONTRACT_NO"];
         
        });
        
    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-special-contract').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/TransSpecialContract/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransSpecialContract/AddTransSpecialContract';
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransSpecialContract/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { BE_ID: BE_ID },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-special-contract').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["CONTRACT_NO"];

            $("#ID_ATTACHMENT").val(data["CONTRACT_NO"]);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TransSpecialContract/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        var x = '<a href ="' + '../../REMOTE/SpecialContract/' + id_attachment + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var deleteAttachment = function () {
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var uriId = $('#ID_ATTACHMENT').val();
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransSpecialContract/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    return {
        init: function () {
            add();
            initTableTransSpecialContract();
            initDetilTransactionSpecial();
            ubahstatus();
            fupload();
            initAttachment();
            deleteAttachment();
            editContract();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransSpecialContract.init();
    });
}