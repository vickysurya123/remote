﻿var ApprovalSettingDetail = function () {
    var initTable = function () {
        var id_header = $('#id_header').val();
        $('#table-detail').DataTable({
            "ajax":
            {
                "url": "/ApprovalSetting/GetDataDetail?id=" + id_header,
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        var v = row.START_DATE + " - " + row.END_DATE;
                        return v;
                    }
                },
                {
                    "data": "TOP_APPROVAL",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_USAGE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-view" class="btn btn-icon-only green" title="Detail" ><i class="fa fa-eye"></i></a>' +
                            '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

        $('#table-detail').on('click', 'tr #btn-view', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();

            var start_date = data["START_DATE"].split(" ");
            var end_date = data["END_DATE"].split(" ");

            $('.START_DATE').val(start_date[0]);
            $('.END_DATE').val(end_date[0]);
            $('.NOTIF_START').val(data["NOTIF_START"]);
            $('.CONTRACT_TYPE').val(data["CONTRACT_TYPE"]);
            $('#detailModal').modal('show');
            $('#table-detail-row').DataTable({
                "ajax":
                {
                    "url": "/ApprovalSetting/ViewDetail/" + data["ID"],
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "APPROVAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = full.ROLE_NAME;
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
            $('#table-detail-param').DataTable({
                "ajax":
                {
                    "url": "/ApprovalSetting/ViewDetailParam/" + data["ID"],
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = full.PARAM_NAME;
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MIN",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "MAX",
                        "class": "dt-body-center",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });

        $('#table-detail').on('click', 'tr #btn-delete', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();
            var param = {
                ID: data["ID"].toString()
            };

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this detail Approval Setting?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/ApprovalSetting/DeleteDetail",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload();
                        if (data.Status === 'S') {
                            swal('Berhasil', data.Msg, 'success');
                        } else {
                            swal('Gagal', data.Msg, 'error');
                        }
                    });
                }
            });
        });

        $('#btn-back').click(function () {
            window.location = "/ApprovalSetting/index";
        });
        $('#btn-add').click(function () {
            $('#txt-judul').text('Add');
            clear();
        });
        $('#table-detail').on('click', 'tr #btn-ubah', function () {
            clear();
            var baris = $(this).parents('tr')[0];
            var table = $('#table-detail').DataTable();
            var data = table.row(baris).data();
            var table_row = $('#table-row ')
            var a = '';
            var b = '';
            var start_date = data["START_DATE"].split(" ");
            var end_date = data["END_DATE"].split(" ");

            $('#txt-judul').text('Edit');
            $('#id_detail').val(data["ID"]);
            $('#START_DATE').val(start_date[0]);
            $('#START_DATE').datepicker('setDate', start_date[0]);
            $('#END_DATE').val(end_date[0]);
            $('#END_DATE').datepicker('setDate', end_date[0]);
            $('#CONTRACT_TYPE option').filter(function () { return $(this).html() == data["CONTRACT_TYPE"]; }).attr('selected', 'selected');
            $('#CONTRACT_USAGE option').filter(function () { return $(this).html() == data["CONTRACT_USAGE"]; }).attr('selected', 'selected');

            $('#table-row tbody').html('');
            $('#table-row').DataTable().destroy();
            $('#table-param tbody').html('');
            $('#table-param').DataTable().destroy();
            $.ajax({
                type: "GET",
                url: "/ApprovalSetting/EditDataDetail?id=" + data["ID"],
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        a += '<tr>' +
                            '<td>' + jsonList.data[i].ID + '</td>' +
                            '<td>' + jsonList.data[i].APPROVAL_NO + '</td>' +
                            '<td>' + jsonList.data[i].ROLE_NAME + '</td>' +
                            '<td><center><a class="btn default btn-xs blue" id="btn-update-row"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center></td>' +
                            '<td>' + jsonList.data[i].PROPERTY_ROLE + '</td>' +
                            '</tr>';
                    }
                    $('#table-row tbody').append(a);
                    $('#table-row').DataTable({
                        "lengthMenu": [
                            [5, 15, 20, -1],
                            [5, 15, 20, "All"]
                        ],
                        "pageLength": 100000,
                        "paging": false,
                        "language": {
                            "lengthMenu": " _MENU_ records"
                        },
                        "searching": false,
                        "lengthChange": false,
                        "bSort": false,
                        "aoColumnDefs": [
                            { "bVisible": false, "aTargets": [0, 4] }
                        ]
                    });
                    for (var i = 0; i < jsonList.param.length; i++) {
                        b += '<tr>' +
                            '<td>' + jsonList.param[i].ID + '</td>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + jsonList.param[i].PARAM_NAME + '</td>' +
                            '<td>' + jsonList.param[i].MIN + '</td>' +
                            '<td>' + jsonList.param[i].MAX + '</td>' +
                            '<td><center><a class="btn default btn-xs blue" id="btn-update-param"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-param"><i class="fa fa-trash"></i></a></center></td>' +
                            '<td>' + jsonList.param[i].PARAM_ID + '</td>' +
                            '</tr>';
                    }
                    $('#table-param tbody').append(b);
                    $('#table-param').DataTable({
                        "lengthMenu": [
                            [5, 15, 20, -1],
                            [5, 15, 20, "All"]
                        ],
                        "pageLength": 100000,
                        "paging": false,
                        "language": {
                            "lengthMenu": " _MENU_ records"
                        },
                        "searching": false,
                        "lengthChange": false,
                        "bSort": false,
                        "aoColumnDefs": [
                            { "bVisible": false, "aTargets": [0, 6] }
                        ],

                    });
                }
            });

            $('#adddetailModal').modal('show');
        });
    }

    // Variabel untuk handle combobox role
    var listItems = "";

    var Role = function () {
        $.ajax({
            type: "GET",
            url: "/ApprovalSetting/GetDataDropDownRole",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROPERTY_ROLE + "-" + jsonList.data[i].ROLE_NAME + "'>" + jsonList.data[i].ROLE_NAME + "</option>";
                }
            }
        });
    }
    var listParam = "";
    var Param = function () {
        $.ajax({
            type: "GET",
            url: "/ApprovalSetting/GetDataDropDownParam",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                for (var i = 0; i < jsonList.data.length; i++) {
                    listParam += "<option value='" + jsonList.data[i].ID + "-" + jsonList.data[i].PARAMETER_NAME + "'>" + jsonList.data[i].PARAMETER_NAME + "</option>";
                }
            }
        });

        $('#BATAS_BAWAH_LUAS, #BATAS_ATAS_LUAS,#BATAS_BAWAH_WAKTU, #BATAS_ATAS_WAKTU').inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });
    }

    //Var untuk menghandle data table table-row
    var addRow = function () {
        //table row
        var tablem = $('#table-row');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "aoColumnDefs": [
                { "bVisible": false, "aTargets": [0, 4] }
            ]
        });

        //Generate textbox(form) di data table table-row
        $('#btn-add-row').on('click', function () {
            resetNumbering();
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var xtable = $('#table-row').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-row").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var roleId = '<select class="form-control role_id" name="role_id" id="role_id">' + listItems + '</select>';
            var waktu = '<input type="text" class="form-control" id="limit-time" name="limit-time">';

            oTable.fnAddData([null, nomor,
                roleId,
                '<center><a class="btn default btn-xs green btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                null
            ], true);

            $('input[name="limit-time"]').inputmask("hh:mm", {
                "placeholder": "HH:MM",
                insertMode: false,
                showMaskOnHover: false,
            });
        });

        $('#table-row').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var data = table.row(baris).data();
            console.log(data[0]);
            if (data[0] != null) {
                a = "<tr><td>" + data[0] + "</td></tr>";
                $('#delete_role tbody').append(a);
            }
            oTable.fnDeleteRow(baris);
            resetNumbering();
        });

        resetNumbering = function () {
            var index = 1;
            $('#table-row tbody tr').each(function () {
                $(this).find('td').eq(0).html(index);
                //oTable.fnUpdate(index, index - 1, 1, false);
                index++;
            });
            var indexparam = 1;
            $('#table-param tbody tr').each(function () {
                $(this).find('td').eq(0).html(indexparam);
                //oTableparam.fnUpdate(indexparam, indexparam - 1, 1, false);
                indexparam++;
            });
        }

        $('#table-row').on('click', 'tr .btn-savecus-frequency', function () {
            resetNumbering();
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var data = table.row(baris).data();

            var a = $(baris).find('#role_id').val();
            var b = $(baris).find('#limit-time').val();
            var tombol = '<center><a class="btn default btn-xs blue" id="btn-update-row"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var role_id = a.split("-")
            oTable.fnUpdate(role_id[1], baris, 2, false);
            oTable.fnUpdate(tombol, baris, 3, false);
            oTable.fnUpdate(role_id[0], baris, 4, false);


            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);

            //$(this).remove();
            return false;

        });

        //tableparam
        var tparam = $('#table-param');
        var oTableparam = tparam.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "aoColumnDefs": [
                { "bVisible": false, "aTargets": [0, 6] }
            ]
        });
        $('#btn-add-param').on('click', function () {
            resetNumbering();
            var nomor = 1;
            var baris = $(this).parents('tr')[0];
            var table = $('#table-param').DataTable();
            var xtable = $('#table-param').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);
            var last = $("#table-param").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            if (l != 0) {
                nomor = (Number(last) + 1);
            }

            //Form input di data table table-row
            var paramId = '<select class="form-control role_id" name="param_id" id="param_id">' + listParam + '</select>';
            var Min = '<input type="text" class="form-control numeric" id="min" name="min">';
            var Max = '<input type="text" class="form-control numeric" id="max" name="max">';
            var id_param_row = '<input type="text" class="form-control numeric" id="id_param_row" name="id_param_row" value="null">';
            oTableparam.fnAddData([null, nomor,
                paramId,
                Min,
                Max,
                '<center><a class="btn default btn-xs green btn-savecus-param" id="btn-savecus-param"><i class="fa fa-check"></i></a><a class="btn default btn-xs red " id="btn-delcus-param"><i class="fa fa-trash"></i></a></center>',
                null
            ], true);
            $('.numeric').inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });
            //resetNumbering();
        });
        $('#table-param').on('click', 'tr #btn-delcus-param', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-param').DataTable();
            var data = table.row(baris).data();
            var a;
            console.log(data[0]);
            if (data[0] != null) {
                a = "<tr><td>" + data[0] + "</td></tr>";
                $('#delete_param tbody').append(a);
            }
            oTableparam.fnDeleteRow(baris);
            resetNumbering();
        });
        $('#table-param').on('click', 'tr .btn-savecus-param', function () {
            resetNumbering();
            var baris = $(this).parents('tr')[0];
            var table = $('#table-param').DataTable();
            var data = table.row(baris).data();

            var a = $(baris).find('#param_id').val();
            var b = $(baris).find('#min').val();
            var c = $(baris).find('#max').val();
            var tombol = '<center><a class="btn default btn-xs blue" id="btn-update-param"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-param"><i class="fa fa-trash"></i></a></center>';
            var param_id = a.split("-");
            //oTableparam.fnUpdate(d, baris, 1, false);
            oTableparam.fnUpdate(param_id[1], baris, 2, false);
            oTableparam.fnUpdate(b, baris, 3, false);
            oTableparam.fnUpdate(c, baris, 4, false);
            oTableparam.fnUpdate(tombol, baris, 5, false);
            oTableparam.fnUpdate(param_id[0], baris, 6, false);


            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);

            //$(this).remove();
            return false;

        });
        $('#table-param').on('click', 'tr #btn-update-param', function () {
            resetNumbering();
            var baris = $(this).parents('tr')[0];
            var table = $('#table-param').DataTable();
            var adata = table.row(baris).data();
            var param = adata[6] + "-" + adata[2];
            var listparamsEdit = '';
            $.ajax({
                type: "GET",
                url: "/ApprovalSetting/GetDataDropDownParam",
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data) {
                    var jsonList = data;
                    listparamsEdit += '<select class="form-control param_id" name="param_id" id="param_id">';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        var selected = (param == jsonList.data[i].ID + "-" + jsonList.data[i].PARAMETER_NAME ? "selected" : "");
                        listparamsEdit += "<option value='" + jsonList.data[i].ID + "-" + jsonList.data[i].PARAMETER_NAME + "' " + selected + ">" + jsonList.data[i].PARAMETER_NAME + "</option>";
                    }
                    listparamsEdit += '</select>';
                }
            });

            var tombol = '<center><a class="btn default btn-xs green btn-savecus-param" name="btn-savecus-frequency" id="btn-savecus-param"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-param"><i class="fa fa-trash"></i></a></center>';
            var min = '<input type="text" class="form-control numeric" id="min" name="min" value="' + adata[3] + '">';
            var max = '<input type="text" class="form-control numeric" id="max" name="max" value="' + adata[4] + '">';
            var openTable = $('#table-param').dataTable();
            openTable.fnUpdate(listparamsEdit, baris, 2, false);
            openTable.fnUpdate(min, baris, 3, false);
            openTable.fnUpdate(max, baris, 4, false);
            openTable.fnUpdate(tombol, baris, 5, false);
            $('.numeric').inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });
        });

        // edit row
        $('#table-row').on('click', 'tr #btn-update-row', function () {
            resetNumbering();
            var baris = $(this).parents('tr')[0];
            var table = $('#table-row').DataTable();
            var adata = table.row(baris).data();
            var role = adata[4] + "-" + adata[2];
            var listitemsEdit = '';
            $.ajax({
                type: "GET",
                url: "/ApprovalSetting/GetDataDropDownRole",
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data) {
                    var jsonList = data;
                    console.log(role);
                    listitemsEdit += '<select class="form-control role_id" name="role_id" id="role_id">';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        var selected = (role == jsonList.data[i].PROPERTY_ROLE + "-" + jsonList.data[i].ROLE_NAME ? "selected" : "");
                        listitemsEdit += "<option value='" + jsonList.data[i].PROPERTY_ROLE + "-" + jsonList.data[i].ROLE_NAME + "' " + selected + ">" + jsonList.data[i].ROLE_NAME + "</option>";
                    }
                    listitemsEdit += '</select>';
                }
            });

            var tombol = '<center><a class="btn default btn-xs green btn-savecus-frequency" name="btn-savecus-frequency" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
            var openTable = $('#table-row').dataTable();
            openTable.fnUpdate(listitemsEdit, baris, 2, false);
            openTable.fnUpdate(tombol, baris, 3, false);
        })

    }
    var clear = function () {
        $('input').not("#id_header").val('');
        $('select option').filter(function () { return $(this).html() == ""; }).attr('selected', 'selected');
        //$('select').val('');
        var oSettings = $('#table-row').dataTable().fnSettings();
        var iTotalRecords = oSettings.fnRecordsTotal();
        for (i = 0; i <= iTotalRecords; i++) {
            $('#table-row').dataTable().fnDeleteRow(0, null, true);
        }

        oSettings = $('#table-param').dataTable().fnSettings();
        iTotalRecords = oSettings.fnRecordsTotal();
        for (i = 0; i <= iTotalRecords; i++) {
            $('#table-param').dataTable().fnDeleteRow(0, null, true);
        }
    }
    var simpanData = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-detail').DataTable();
            var allowSave = true;
            var START_DATE = $('#START_DATE').val();
            var END_DATE = $('#END_DATE').val();
            var HEADER_ID = $('#id_header').val();
            var DETAIL_ID = $('#id_detail').val();
            var CONTRACT_TYPE = $('#CONTRACT_TYPE').val();
            var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
            if (START_DATE && END_DATE && CONTRACT_TYPE) {

            } else {
                allowSave = false;
            }
            $('#table-param tbody tr').each(function () {
                kolom = $(this).find('#btn-savecus-param').length;
                if (kolom == 1) {
                    allowSave = false;
                }
            });
            //array role
            var DTMemo = $('#table-row').dataTable();
            var countDTMemo = DTMemo.fnGetData();
            arrRole = new Array();
            //console.log(countDTMemo);
            for (var i = 0; i < countDTMemo.length; i++) {
                var itemDTMemo = DTMemo.fnGetData(i);
                var paramDTMemo = {
                    APPROVAL_NO: itemDTMemo[1].toString(),
                    PROPERTY_ROLE: itemDTMemo[4],
                    ROLE_NAME: itemDTMemo[2],
                    ID: itemDTMemo[0],
                }
                arrRole.push(paramDTMemo);
            }
            if (countDTMemo.length > 0) {
                $('#table-row tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-frequency').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }

            //array param
            var DTParam = $('#table-param').dataTable();
            var countDTParam = DTParam.fnGetData();
            arrParam = new Array();
            for (var i = 0; i < countDTParam.length; i++) {
                var itemDTParam = DTParam.fnGetData(i);
                var paramDTMemo = {
                    PARAM_ID: itemDTParam[6],
                    PARAM_NAME: itemDTParam[2],
                    MIN: itemDTParam[3],
                    MAX: itemDTParam[4],
                    ID: itemDTParam[0],
                }
                arrParam.push(paramDTMemo);
            }
            if (countDTParam.length > 0) {
                $('#table-param tbody tr').each(function () {
                    var kolom = $(this).find('#btn-savecus-param').length;
                    if (kolom == 1) {
                        allowSave = false;
                    }
                });
            } else {
                allowSave = false;
            }
            var deleteParam = $('#delete_param tbody');
            delParam = new Array();
            $('#delete_param tbody tr').each(function () {
                var deleteParam = {
                    ID: $(this).find("td:first").text(),
                }
                delParam.push(deleteParam);
            });
            delRole = new Array();
            $('#delete_role tbody tr').each(function () {
                var deleteRole = {
                    ID: $(this).find("td:first").text(),
                }
                delRole.push(deleteRole);
            });
            //var param = {
            //    START_DATE: START_DATE,
            //    END_DATE: END_DATE,
            //    HEADER_ID: HEADER_ID,
            //    CONTRACT_TYPE: CONTRACT_TYPE,
            //    CONTRACT_USAGE: CONTRACT_USAGE,
            //    ID: DETAIL_ID,
            //    ROLE: arrRole,
            //    PARAM: arrParam,
            //    delParam: delParam,
            //    delRole: delRole,
            //}
            //allowSave = false;
            //console.log(param);
            if (allowSave) {
                var param = {
                    START_DATE: START_DATE,
                    END_DATE: END_DATE,
                    HEADER_ID: HEADER_ID,
                    CONTRACT_TYPE: CONTRACT_TYPE,
                    CONTRACT_USAGE: CONTRACT_USAGE,
                    ID: DETAIL_ID,
                    ROLE: arrRole,
                    PARAM: arrParam,
                    delParam: delParam,
                    delRole: delRole,
                }
                App.blockUI({ boxed: true });
                $('#adddetailModal').modal('hide');
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ApprovalSetting/InsertDetail",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                $('#adddetailModal').modal('hide');
                swal('Failed', 'Harap di cek kembali inputanya, dan pada table harap di centang semua', 'error').then(function (isConfirm) {
                    $('#adddetailModal').modal('show');
                });
            }

        });

        $('#btn-bataladd').click(function () {
            clear();
        });
    }


    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    return {
        init: function () {
            initTable();
            addRow();
            Role();
            Param();
            simpanData();
            datePicker();
        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ApprovalSettingDetail.init();
    });
}