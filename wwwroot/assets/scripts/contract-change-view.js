﻿var buttonDisabled = function () {
    $('#btn-editcus').prop('disabled', true);

    return {
        init: function () {
        }

    };
}();
var TableDatatablesEditable = function () {
    var initTablehistoryPerubahanContract = function () {
        var CONTRACT_NO = $('#CONTRACT_NO').val();

        $('#table-history-contract').DataTable({
            "ajax": {
                "url": "/ContractChange/GetDataHistoryContract/" + CONTRACT_NO,
                "type": "GET",
            },
            "columns": [
                {
                    "data": "CHANGE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CHANGE_BY",
                    "class": "dt-body-center"
                },
                //{
                //    "render": function () {
                //     var aksi = '<a data-toggle="modal" href="#viewHistoryPerubahanModal" class="btn btn-icon-only green" id="btn-detil-history"><i class="fa fa-eye"></i></a>';

                //     return aksi;
                //    },
                //    "class": "dt-body-center"
                //}
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initDetilTransactionContract = function () {
        $('body').on('click', 'tr #btn-detil-history', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-history-contract').DataTable();
            var data = table.row(baris).data();
            id_perubahan = data['ID'];

            var tableDetilObject = $('#table-detil-object-history').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var contract_no = $('#CONTRACT_NO').val();

            $('#table-detil-object-history').DataTable({
                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailObject" ,
                    "type": "GET",
                    data: {
                        id: contract_no
                    }
                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
            
            $('#table-detil-condition').DataTable({

                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailCondition",
                    "type": "GET",
                    data: {
                        contract_no: contract_no,
                        id_perubahan: id_perubahan
                    }
                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_1",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },


                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 100,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            
            //View detil manual
            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailManualy",
                    "type": "GET",
                    data: {
                        contract_no: contract_no,
                        id_perubahan: id_perubahan
                    }
                },
                "columns": [
                     {
                         "data": "MANUAL_NO",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "CONDITION",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "DUE_DATE",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "NET_VALUE",
                         "class": "dt-body-center",
                         render: $.fn.dataTable.render.number('.', '.', 0)
                     },
                     {
                         "data": "QUANTITY",
                         "class": "dt-body-center"
                     },
                     {
                         "data": "UNIT",
                         "class": "dt-body-center"
                     }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            
            //Detil memo
            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailMemo",
                    "type": "GET",
                    data: {
                        contract_no: contract_no,
                        id_perubahan: id_perubahan
                    }
                },

                "columns": [
                    {
                        "data": "OBJECT_CALC",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
            

        });
    }

    //-------------------------------------------------BEGIN JS dt-add-trans-co.js-----------------------------------------------------
    //Declare variabel array untuk menampung condition type agar bisa digunakan di form memo
    var arrCondition = [];

    var arrCondType = [];
    var buah = [];
    var secondCellArray = []; 

    var objTampungSR = {};
    var arrTampungSR = [];

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransContract";
        });
    }

    var hitung_bulan = function () {

        //FUNCTION Hitung Bulan
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();

            if (d2.getDate() >= d1.getDate())
                months++

            return months <= 0 ? 0 : months;
        }

        $('#CONTRACT_END_DATE').change(function () {
            var mulai = $('#CONTRACT_START_DATE').val();
            var akhir = $('#CONTRACT_END_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

            var dateAkhir = akhir;
            var datearrayAkhir = dateAkhir.split("/");
            var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

            //console.log(newdateMulai);
            //console.log(newdateAkhir);
            var tim = monthDiff(new Date(newdateMulai), new Date(newdateAkhir));
            $('#TERM_IN_MONTHS').val(tim);
        });

    }

    var hitung_tanggal_sampai = function () {
        function addMonths(date, count) {
            if (date && count) {
                var m, d = (date = new Date(+date)).getDate()

                date.setMonth(date.getMonth() + count, 1)
                m = date.getMonth()
                date.setDate(d)
                if (date.getMonth() !== m) date.setDate(0)
            }
            return date
        }

        $('#TERM_IN_MONTHS').change(function () {
            var tim = $('#TERM_IN_MONTHS').val();
            var mulai = $('#CONTRACT_START_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

            //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
            var f_date = new Date(newdateMulai);
            var n_tim = Number(tim);
            var end_date = addMonths(f_date, n_tim);
            var xdate = new Date(end_date);
            var f_date = xdate.toISOString().slice(0, 10);

            //format f_date menjadi dd/mm/YYYY
            var dateAkhir = f_date;
            var datearrayAkhir = dateAkhir.split("-");
            var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];
            $('#CONTRACT_END_DATE').val(newdateAkhir);

            //console.log(newdateAkhir);
        });
    }

    var filter = function () {
        $('#btn-filter').click(function () {

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/ContractChange/GetDataFilter",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PROFIT_CENTER"
                    },
                    {
                        "data": "CONTRACT_START_DATE"
                    },
                    {
                        "data": "CONTRACT_END_DATE"
                    },
                    {
                        "data": "TERM_IN_MONTHS"
                    },
                    {
                        "data": "CONTRACT_NAME"
                    },
                    {
                        "render": function () {
                            //var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            //return aksi;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
        // Add event listener for opening and closing details

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            $('#condition > tbody').html('');
            $('#condition-option > option').html('');
            $('#rental-object-co > tbody').html('');

            var tableObjectCo = $('#rental-object-co').DataTable();
            tableObjectCo.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();

            var CONTRACT_NO = data['CONTRACT_NO'];
            var CONTRACT_NAME = data['CONTRACT_NAME'];
            var PROFIT_CENTER = data['PROFIT_CENTER'];
            var CONTRACT_START_DATE = data['CONTRACT_START_DATE'];
            var CONTRACT_END_DATE = data['CONTRACT_END_DATE'];
            var TERM_IN_MONTHS = data['TERM_IN_MONTHS'];

            $('#CONTRACT_NO').val(CONTRACT_NO);
            $('#CONTRACT_NAME').val(CONTRACT_NAME);
            $('#PROFIT_CENTER ').val(PROFIT_CENTER);
            $('#CONTRACT_START_DATE ').val(CONTRACT_START_DATE);
            $('#CONTRACT_END_DATE ').val(CONTRACT_END_DATE);
            $('#TERM_IN_MONTHS').val(TERM_IN_MONTHS);

            $('#addDetailModal').modal('hide');
        });
    }

    var addCondition = function () {
        /*
        $('#btn-add-condition').on('click', function () {
            var rental_request_no = $('#CONTRACT_NO').val();

            if (rental_request_no == '') {
                $('#condition-option').html('');
                //swal('Info', 'You have to choose rental request before add a condition', 'info');
            }
            else {
                $('#condition-option').html('');
                // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih
                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataDropDownRO/" + rental_request_no,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        listItems += "<option value='Header Document'>Header Document</option>";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].OBJECT_ID + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>";
                            //listItems += '<option value="test">test</option>';
                        }
                        $("#condition-option").append(listItems);
                    }
                });
            }
        });
        */


        $('#condition').on('click', 'tr #btn-editcusx', function () {
            $('#condition > tbody').html('');
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data;

            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input class="form-control" id="mask_date1" type="text" />';

            }

            //table.fnUpdate(xInd, baris, 18, false);
            document.getElementById('btn-savecus').disabled = true;

        });

        $('#condition').on('click', 'tr #btn-savecusx', function () {
            //$(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var start_date = $('#start-date').val();
            var end_date = $('#end-date').val();
            var term_months = $('#term-months').val();

            var unit_price = $('#unit-price').val();
            var amt_ref = $('#amt-ref').val();
            var frequency = $('#frequency').val();
            var start_due_date = $('#start-due-date').val();
            var manual_no = $('#manual-no').val();
            var formula = $('#formula').val();

            var meas_type = $('#meas-type').val();
            var splitselectedMeasType = meas_type.split("|");
            var id_detail_meas_type = splitselectedMeasType[0];

            var luas = $('#luas').val();
            var total = $('#total').val();
            var persen_from_njop = $('#persen-from-njop').val();
            var persen_from_kondisi_teknis = $('#persen-from-kondisi-teknis').val();

            //var sales_rule = $('#sales-rule').val();
            var total_net_value = $('#total-net-value').val();

            //Set Value Untuk Statistic
            var cek = $('input[id=statistic]').is(":checked");
            var vStatistic = "";
            if (cek) {
                vStatistic = "Yes";
            }
            else {
                vStatistic = "No";
            }

            $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
            $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
            $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
            $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
            $('#condition').dataTable().fnUpdate(unit_price, baris, 6, false);
            $('#condition').dataTable().fnUpdate(amt_ref, baris, 7, false);
            $('#condition').dataTable().fnUpdate(frequency, baris, 8, false);
            $('#condition').dataTable().fnUpdate(start_due_date, baris, 9, false);
            $('#condition').dataTable().fnUpdate(manual_no, baris, 10, false);
            $('#condition').dataTable().fnUpdate(formula, baris, 11, false);
            $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
            $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
            $('#condition').dataTable().fnUpdate(total, baris, 14, false);
            $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
            $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
            //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
            $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus');
            elem.parentNode.removeChild(elem);
            return false;

        });
    }

    //Cek apakah NOMOR_CONTRACT terisi atau tidak jika terisi maka masukkan data2 ke datatable
    /*
    var $myText = $("#CONTRACT_NO");

    $myText.data("value", $myText.val());
    var getDataOBject = "";

    setInterval(function () {
        var data = $myText.data("value"),
            val = $myText.val();

        if (data !== val) {
            $myText.data("value", val);
    */
            //Jalankan variabel2 untuk edit
            //----------------------------- CONTRACT OBJECT EDIT-------------------------------
                var CONTRACT_NO = $('#CONTRACT_NO').val();

                var tabler = $('#rental-object-co');
                var jTable = tabler.dataTable({
                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"]
                    ],
                    "pageLength": 5,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false
                });

                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataObject/" + CONTRACT_NO,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        jTable.fnClearTable();
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            jTable.fnAddData([jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].CONTRACT_START_DATE, jsonList.data[i].CONTRACT_END_DATE, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY]);
                        }
                    }
                });
            //-----------------------------END OF CONTRACT OBJECT EDIT-------------------------------

            //--------------------------------- CONTRACT CONDITION ----------------------------------
                var tablex = $('#condition');
                var xTable = tablex.dataTable({
                    "order": [[7, "asc"]],
                    "lengthMenu": [
                        [5, 15, 20, 50],
                        [5, 15, 20, 50]
                    ],
                    "pageLength": 100,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false,
                    "order": [[0, "asc"]],
                    "columnDefs": [
                        {
                            "targets": [20],
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": [20],
                            "visible": false
                        }
                    ]
                });
                
                var tombol = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataDetailConditionEdit/" + CONTRACT_NO,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrCondition = new Array();

                        xTable.fnClearTable();
                        var jsonList = data
                        var listItems = "";
                        var calcObject = "";
                        var res_condition = "";
                        var statistic = "";
                        var maskUnitPrice = "";
                        var maskTotal = "";
                        var maskNetValue = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            if (jsonList.data[i].STATISTIC == 0) {
                                statistic = "No";
                            }
                            else {
                                statistic = "Yes";
                            }

                            maskUnitPrice = jsonList.data[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            maskTotal = jsonList.data[i].TOTAL.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            maskNetValue = jsonList.data[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            xTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].VALID_FROM, jsonList.data[i].VALID_TO, jsonList.data[i].MONTHS, statistic, maskUnitPrice, jsonList.data[i].AMT_REF, jsonList.data[i].FREQUENCY, jsonList.data[i].START_DUE_DATE, jsonList.data[i].MANUAL_NO, jsonList.data[i].FORMULA, jsonList.data[i].MEASUREMENT_TYPE, jsonList.data[i].LUAS, maskTotal, jsonList.data[i].NJOP_PERCENT, jsonList.data[i].KONDISI_TEKNIS_PERCENT, maskNetValue,jsonList.data[i].COA_PROD, tombol, jsonList.data[i].TAX_CODE]);
                            calcObject = jsonList.data[i].CALC_OBJECT;
                            res_condition = jsonList.data[i].CONDITION_TYPE;
                            var param = {
                                CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                VALID_FROM: jsonList.data[i].VALID_FROM,
                                VALID_TO: jsonList.data[i].VALID_TO,
                                MONTHS: jsonList.data[i].MONTHS,
                                STATISTIC: jsonList.data[i].STATISTIC,
                                UNIT_PRICE: jsonList.data[i].UNIT_PRICE,
                                AMT_REF: jsonList.data[i].AMT_REF,
                                FREQUENCY: jsonList.data[i].FREQUENCY,
                                START_DUE_DATE: jsonList.data[i].START_DUE_DATE,
                                MANUAL_NO: jsonList.data[i].MANUAL_NO,
                                FORMULA: jsonList.data[i].FORMULA,
                                MEASUREMENT_TYPE: jsonList.data[i].MEASUREMENT_TYPE,
                                LUAS: jsonList.data[i].LUAS,
                                TOTAL: jsonList.data[i].TOTAL,
                                NJOP_PERCENT: jsonList.data[i].NJOP_PERCENT,
                                KONDISI_TEKNIS_PERCENT: jsonList.data[i].KONDISI_TEKNIS_PERCENT,
                                TOTAL_NET_VALUE: jsonList.data[i].TOTAL_NET_VALUE,
                                COA_PROD: jsonList.data[i].COA_PROD,
                                TAX_CODE: jsonList.data[i].TAX_CODE,
                            }
                            arrCondition.push(JSON.stringify(param));

                            var arrCond = buah.push(calcObject + ' | ' + res_condition);
                            //console.log(arrCond);

                            var fruits = buah;
                        }
                    }
                });

                //$('#btn-condition').on('click', function () {
                //    // $('#condition').dataTable();

                //    //raw data value calc object dan di split |
                //    var id_combo = $('#condition-option').val();
                //    var splitCalcObject = id_combo.split("|");
                //    var calcObject = splitCalcObject[0];
                //    var id_ro = splitCalcObject[1];
                //    var measType = '';
                //    if (id_ro) {
                //        measType = '<select class="form-control" id="meas-type" name="meas-type" onchange="getLuas(this)"></select>';
                //        //console.log(measType);
                //    }
                //    if (id_ro) {
                //        // Ajax untuk get meas type berdasarkan id_ro yang didapatkan dari split
                //        $('#meas-type').html('');
                //        // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih
                //        $.ajax({
                //            type: "GET",
                //            url: "/ContractChange/GetDataMeasType/" + id_ro,
                //            contentType: "application/json",
                //            dataType: "json",
                //            success: function (data) {
                //                var jsonList = data
                //                var listItems = "";

                //                listItems += '<option value="">--Choose--</option>';
                //                for (var i = 0; i < jsonList.data.length; i++) {
                //                    console.log("jsonnya:  " + jsonList.data[i].MEAS_TYPE);
                //                    listItems += "<option value='" + jsonList.data[i].MEAS_TYPE + '|' + jsonList.data[i].ID_DETAIL + "'>" + jsonList.data[i].MEAS_TYPE + ' - ' + jsonList.data[i].MEAS_DESC + "</option>";
                                    
                //                }
                //                console.log("listitemnya:  " + listItems);

                //                $("#meas-type").append(listItems);
                //                //$('select[name="meas-type"]').html(listItems);
                //            }
                //        });


                //    }

                //    // Matikan form untuk kondisi Header Document
                    
                //    if (id_ro) {
                //        var luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
                //    }
                //    else {
                //        luas = '';
                //    }

                //    //Cek selected value dari combobox calculation apakah header document atau RO
                //    var vCBCalculation = $("#condition-option").val();

                //    if (vCBCalculation == 'Header Document') {
                //        measType = '<input type="text" class="form-control" id="meas-type" name="meas-type" readonly>';
                //        luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';

                //    }

                //    getCoaProduksi();

                //    var tableDT = $('#condition  > tbody');
                //    var start = $('#CONTRACT_START_DATE').val();
                //    var end = $('#CONTRACT_END_DATE').val();
                //    var tim = $('#TERM_IN_MONTHS').val();

                //    var startD = '<center><input type="text" class="form-control" name="start-date" value="' + start + '" id="start-date"></center>';
                //    var endD = '<center><input type="text" class="form-control" name="end-date" value="' + end + '" id="end-date" onchange="hitungTermMonths(this)"></center>';
                //    var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onchange="hitungEndDate(this)"></center>';

                //    var kosong = '';
                //    var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
                //    var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
                //    var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
                //    var unitPrice = '<center><input type="text" class="form-control" id="unit-price" name="unit-price" onchange="isiTotalUnitPrice(this)"></center>';
                //    var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
                //    var frequency = '<center><select class="form-control" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
                //    var startDueDate = '<center><input type="text" class="form-control" name="start-due-date" id="start-due-date"></center>';
                //    var manualNo = '<center><input type="text" class="form-control" id="manual-no"></center>';
                //    var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';
                //    //var measType = '<select class="form-control" id="meas-type"></select>';
                //    //var luas = '<input type="text" class="form-control" id="luas">';
                //    var total = '<center><input type="text" class="form-control" id="total" name="total" readonly></center>';
                //    var persenFromNJOP = '<center><input type="text" class="form-control" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
                //    var persenFromKondisiTeknis = '<center><input type="text" class="form-control" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';
                //    //var salesRule = '<center><input type="text" class="form-control" id="sales-rule"></center>';
                //    var totalNetValue = '<center><input type="text" class="form-control" id="total-net-value" name="total-net-value" readonly></center>';
                //    var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
                //    var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_prod" id="coa_prod"><option value="-">-</option></select></center>';

                //    //Format luas dengan separator . karena
                //    //var fLuas = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                //    $('input[name="test"]:checked').each(function () {
                //        //raw data condition dan di split |
                //        var raw_cond = $(this).val();
                //        var split_cond = raw_cond.split("|");
                //        var res_condition = split_cond[0];
                //        var tax_code = split_cond[1];

                //        if (res_condition == 'Z009 Administrasi' || res_condition == 'Z010 Warmeking') {
                //            xTable.fnAddData([calcObject,
                //                              res_condition,
                //                              startD,
                //                              endD,
                //                              termMonths,
                //                              cekbokStatistic,
                //                              unitPrice,
                //                              amtRef,
                //                              frequency,
                //                              startDueDate,
                //                              manualNo,
                //                              formula,
                //                              measType,
                //                              luas,
                //                              total,
                //                              persenFromNJOP,
                //                              persenFromKondisiTeknis,
                //                              totalNetValue,
                //                              coa_produksi_disabled,
                //                              '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                //                              tax_code], true);
                //        } else {
                //            xTable.fnAddData([calcObject,
                //                              res_condition,
                //                              startD,
                //                              endD,
                //                              termMonths,
                //                              cekbokStatistic,
                //                              unitPrice,
                //                              amtRef,
                //                              frequency,
                //                              startDueDate,
                //                              manualNo,
                //                              formula,
                //                              measType,
                //                              luas,
                //                              total,
                //                              persenFromNJOP,
                //                              persenFromKondisiTeknis,
                //                              totalNetValue,
                //                              coa_prod,
                //                              '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                //                              tax_code], true);
                //        }

                //        var arrCond = buah.push(calcObject + ' | ' + res_condition);
                //        //console.log(arrCond);

                //        var fruits = buah;

                //        //Masking Start Due Date
                //        $('input[name="start-due-date"]').inputmask("d/m/y", {
                //            "placeholder": "dd/mm/yyyy"
                //        });

                //        $('input[name="start-date"]').inputmask("d/m/y", {
                //            "placeholder": "dd/mm/yyyy"
                //        });

                //        $('input[name="end-date"]').inputmask("d/m/y", {
                //            "placeholder": "dd/mm/yyyy"
                //        });

                //        $('#addCondition').modal('hide');
                //    });

                //    // MASKING2
                //    $('#unit-price').on('change', function () {
                //        var unitPrice = $('#unit-price').val();
                //        var mask = sep1000(unitPrice, true);
                //        $('#unit-price').val(mask);
                //    });
                //});

            // BTN EDITCUS
                //$('#condition').on('click', 'tr #btn-editcus', function () {
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#condition').DataTable();
                //    var data = table.row(baris).data();

                //    var nRow = $(this).parents('tr')[0];
                //    var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

                //    // Open Data Table
                //    var openTable = $('#condition').dataTable();

                //    // Value untuk Data Table Form
                //    var condtype = data[1];
                //    var start = data[2];
                //    var end = data[3];
                //    var tim = data[4];
                //    var stat = data[5];
                //    var uPrice = data[6];
                //    var aRef = data[7];
                //    var freq = data[8];
                //    var dueDate = data[9];
                //    var manual = data[10];
                //    var vFormula = data[11];
                //    var vMeasType = data[12];
                //    var vLuas = data[13];
                //    var vTotalValue = data[14];
                //    var vNJOP = data[15];
                //    var vKondisiTeknis = data[16];
                //    var vNetValue = data[17];
                //    var coa_prod = data[18];
                //    console.log(uPrice);

                //    var sDate = '';
                //    var sDueDate = '';

                //    if (dueDate == null) {
                //        sDueDate = "";
                //    }
                //    else {
                //        sDueDate = dueDate;
                //    }

                //    var mNO = '';
                //    if (manual == null) {
                //        mNO = "";
                //    }
                //    else {
                //        mNO = manual;
                //    }

                //    var sNJOP = '';
                //    if (vNJOP == null) {
                //        sNJOP = '';
                //    }
                //    else {
                //        sNJOP = vNJOP;
                //    }

                //    var sMeasType = '';
                //    if (vMeasType == null) {
                //        sMeasType = '';
                //    }
                //    else {
                //        sMeasType = vMeasType;
                //    }

                //    var sLuas = '';
                //    if (vLuas == null) {
                //        sLuas = '';
                //    }
                //    else {
                //        sLuas = vLuas;
                //    }

                //    var sKondisiTeknis = '';
                //    if (vKondisiTeknis == null) {
                //        sKondisiTeknis = '';
                //    }
                //    else {
                //        sKondisiTeknis = vKondisiTeknis;
                //    }

                //    getCoaProduksi(data[18]);

                //    // Data Table Form
                //    var startD = '<center><input type="text" class="form-control" name="start-date" value="' + start + '" id="start-date"></center>';
                //    var endD = '<center><input type="text" class="form-control" name="end-date" value="' + end + '" id="end-date" onchange="hitungTermMonths(this)"></center>';
                //    var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onchange="hitungEndDate(this)"></center>';

                //    var kosong = '';
                //    var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
                //    var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
                //    var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
                //    var unitPrice = '<center><input type="text" class="form-control" id="unit-price" value="' + uPrice + '" name="unit-price" onchange="isiTotalUnitPrice(this)"></center>';
                //    var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
                //    var frequency = '<center><select class="form-control" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
                //    var startDueDate = '<center><input type="text" class="form-control" value="' + sDueDate + '" name="start-due-date" id="start-due-date"></center>';
                //    var manualNo = '<center><input type="text" class="form-control" value="' + mNO + '" id="manual-no"></center>';
                //    var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';

                //    var total = '<center><input type="text" class="form-control" value="' + vTotalValue + '" id="total" name="total" readonly></center>';
                //    var persenFromNJOP = '<center><input type="text" class="form-control" value="' + sNJOP + '" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
                //    var persenFromKondisiTeknis = '<center><input type="text" class="form-control" value="' + sKondisiTeknis + '" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';

                //    var totalNetValue = '<center><input type="text" class="form-control" value="' + vNetValue + '" id="total-net-value" name="total-net-value" readonly></center>';
                //    var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
                //    var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_produksi" id="coa_prod"><option value="-" selected="selected">-</option></select></center>';
                //    measType = '<input type="text" class="form-control" value="' + sMeasType + '" id="meas-type" name="meas-type" readonly>';
                //    luas = '<input type="text" class="form-control" id="luas" value="' + sLuas + '" name="luas" readonly>';

                //    openTable.fnUpdate(startD, nRow, 2, false);
                //    openTable.fnUpdate(endD, nRow, 3, false);
                //    openTable.fnUpdate(termMonths, nRow, 4, false);
                //    openTable.fnUpdate(cekbokStatistic, nRow, 5, false);
                //    openTable.fnUpdate(unitPrice, nRow, 6, false);
                //    openTable.fnUpdate(amtRef, nRow, 7, false);
                //    openTable.fnUpdate(frequency, nRow, 8, false);
                //    openTable.fnUpdate(startDueDate, nRow, 9, false);
                //    openTable.fnUpdate(manualNo, nRow, 10, false);
                //    openTable.fnUpdate(formula, nRow, 11, false);
                //    openTable.fnUpdate(measType, nRow, 12, false);
                //    openTable.fnUpdate(luas, nRow, 13, false);
                //    openTable.fnUpdate(total, nRow, 14, false);
                //    openTable.fnUpdate(persenFromNJOP, nRow, 15, false);
                //    openTable.fnUpdate(persenFromKondisiTeknis, nRow, 16, false);
                //    openTable.fnUpdate(totalNetValue, nRow, 17, false);
                //    if (condtype == 'Z009 Administrasi' || condtype == 'Z010 Warmeking') {
                //        openTable.fnUpdate(coa_produksi_disabled, nRow, 18, false);
                //    } else {
                //        openTable.fnUpdate(coa_prod, nRow, 18, false);
                //    }
                //    openTable.fnUpdate(tombol, nRow, 19, false);

                //    $("#amt-ref option[value='" + aRef + "']").prop('selected', true);
                //    $("#frequency option[value='" + freq + "']").prop('selected', true);
                //    $("#formula option[value='" + vFormula + "']").prop('selected', true);

                //    $('#unit-price').on('change', function () {
                //        var unitPrice = $('#unit-price').val();
                //        var mask = sep1000(unitPrice, true);
                //        $('#unit-price').val(mask);
                //    });

                //});


                //$('#condition').on('click', 'tr #btn-cancelcus', function (e) {
                //    e.preventDefault();

                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#condition').DataTable();
                //    var data = table.row(baris).data();

                //    var calcObject = data[0];
                //    var conditionType = data[1];

                //    // Hapus Row 
                //    xTable.fnDeleteRow(baris);

                //    var json = JSON.parse('[' + arrCondition + ']');


                //    // Deklarasi Variabel       
                //    var CALC_OBJECT = "";
                //    var CONDITION_TYPE = "";
                //    var VALID_FROM = "";
                //    var VALID_TO = "";
                //    var MONTHS = "";
                //    var STATISTIC = "";
                //    var UNIT_PRICE = "";
                //    var AMT_REF = "";
                //    var FREQUENCY = "";
                //    var START_DUE_DATE = "";
                //    var MANUAL_NO = "";
                //    var FORMULA = "";;
                //    var MEASUREMENT_TYPE = "";
                //    var LUAS = "";
                //    var TOTAL = "";
                //    var NJOP_PERCENT = "";
                //    var KONDISI_TEKNIS_PERCENT = "";
                //    var TOTAL_NET_VALUE = "";
                //    var TAX_CODE = "";

                //    json.forEach(function (object) {
                //        if (object.CALC_OBJECT == calcObject && object.CONDITION_TYPE == conditionType) {
                //            CALC_OBJECT = object.CALC_OBJECT;
                //            CONDITION_TYPE = object.CONDITION_TYPE;
                //            VALID_FROM = object.VALID_FROM;
                //            VALID_TO = object.VALID_TO;
                //            MONTHS = object.MONTHS;
                //            STATISTIC = object.STATISTIC;
                //            UNIT_PRICE = object.UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                //            AMT_REF = object.AMT_REF;
                //            FREQUENCY = object.FREQUENCY;
                //            START_DUE_DATE = object.START_DUE_DATE;
                //            MANUAL_NO = object.MANUAL_NO;
                //            FORMULA = object.FORMULA;
                //            MEASUREMENT_TYPE = object.MEASUREMENT_TYPE;
                //            LUAS = object.LUAS;
                //            TOTAL = object.TOTAL.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                //            NJOP_PERCENT = object.NJOP_PERCENT;
                //            KONDISI_TEKNIS_PERCENT = object.KONDISI_TEKNIS_PERCENT;
                //            TOTAL_NET_VALUE = object.TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
                //            TAX_CODE = object.TAX_CODE;
                //        }
                //    });

                //    var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                //    xTable.fnAddData([CALC_OBJECT, CONDITION_TYPE, VALID_FROM, VALID_TO, MONTHS, STATISTIC, UNIT_PRICE, AMT_REF, FREQUENCY, START_DUE_DATE, MANUAL_NO, FORMULA, MEASUREMENT_TYPE, LUAS, TOTAL, NJOP_PERCENT, KONDISI_TEKNIS_PERCENT, TOTAL_NET_VALUE, tombolC, TAX_CODE]);

                //});

                //$('#condition').on('click', 'tr #btn-savecus', function () {
                //    //$(this).hide();
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#condition').DataTable();
                //    var data = table.row(baris).data();

                //    var start_date = $('#start-date').val();
                //    var end_date = $('#end-date').val();
                //    var term_months = $('#term-months').val();

                //    var unit_price = $('#unit-price').val();
                //    var amt_ref = $('#amt-ref').val();
                //    var frequency = $('#frequency').val();
                //    var start_due_date = $('#start-due-date').val();
                //    var manual_no = $('#manual-no').val();
                //    var formula = $('#formula').val();

                //    var meas_type = $('#meas-type').val();
                //    var splitselectedMeasType = meas_type.split("|");
                //    var id_detail_meas_type = splitselectedMeasType[0];

                //    var luas = $('#luas').val();
                //    var total = $('#total').val();
                //    var persen_from_njop = $('#persen-from-njop').val();
                //    var persen_from_kondisi_teknis = $('#persen-from-kondisi-teknis').val();
                //    var coa_prod = $('#coa_prod').val();
                //    //var sales_rule = $('#sales-rule').val();
                //    var total_net_value = $('#total-net-value').val();

                //    //Set Value Untuk Statistic
                //    var cek = $('input[id=statistic]').is(":checked");
                //    var vStatistic = "";
                //    if (cek) {
                //        vStatistic = "Yes";
                //    }
                //    else {
                //        vStatistic = "No";
                //    }

                //    var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                //    if (coa_prod == "") {
                //        swal('Info', 'Mohon Pilih COA Produksi', 'info');
                //    } else {
                //        $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
                //        $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
                //        $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
                //        $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
                //        $('#condition').dataTable().fnUpdate(unit_price, baris, 6, false);
                //        $('#condition').dataTable().fnUpdate(amt_ref, baris, 7, false);
                //        $('#condition').dataTable().fnUpdate(frequency, baris, 8, false);
                //        $('#condition').dataTable().fnUpdate(start_due_date, baris, 9, false);
                //        $('#condition').dataTable().fnUpdate(manual_no, baris, 10, false);
                //        $('#condition').dataTable().fnUpdate(formula, baris, 11, false);
                //        $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
                //        $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
                //        $('#condition').dataTable().fnUpdate(total, baris, 14, false);
                //        $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
                //        $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
                //        //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
                //        $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
                //        $('#condition').dataTable().fnUpdate(coa_prod, baris, 18, false);
                //        $('#condition').dataTable().fnUpdate(tombolDefault, baris, 19, false);

                //        return false;
                //    }
                //});

                //$('#condition').on('click', 'tr #btn-delcus', function () {
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#condition').DataTable();
                //    var data = table.row(baris).data();

                //    // ambil data by kolom
                //    var rData = table.row($(this).parents('tr')).data();
                //    var calc_object = rData[0];
                //    var condition_type = rData[1];
                //    var dataRemove = calc_object + ' | ' + condition_type;
                //    // panggil function untuk remove array buah dan secondCellArray:)
                //    removeA(buah, dataRemove);
                //    xTable.fnDeleteRow(baris);
                //});

            //----------------------------- END OF CONTRACT CONDITION -------------------------------

            //----------------------------------- MANUAL FREQUENCY ----------------------------------
                var tablem = $('#manual-frequency');
                var oTable = tablem.dataTable({
                    "lengthMenu": [
                        [5, 15, 20, 50],
                        [5, 15, 20, 50]
                    ],
                    "pageLength": 45,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "order": [[0, "asc"]],
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false,
                    "columnDefs": [
                    {
                        "targets": [8],
                        "visible": false
                    }
                    ]
                });

                var arrManualFrequency = [];

                //var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';
                var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
                var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
                //var tombolAdd2 = '';
                //var tombolAct2 = '<center><span class="label label-sm label-success"> LUNAS </span></center>';
;
                // Ajax untuk menampilkan data manual frequency sebelumnya
                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataDetailManualFrequencyEdit/" + CONTRACT_NO,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrManualFrequency = new Array();

                        oTable.fnClearTable();
                        var jsonList = data;
                        var res_condition = "";
                        var maskNetValueFrequency = "";
                        var maskQuantityFrequency = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            maskNetValueFrequency = jsonList.data[i].NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            if (jsonList.data[i].QUANTITY != null) {
                                maskQuantityFrequency = jsonList.data[i].QUANTITY.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }

                            if (jsonList.data[i].TGL_LUNAS == null) {
                                oTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct, jsonList.data[i].CONDITION]);

                            }
                            else {
                                oTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct2, jsonList.data[i].CONDITION]);

                            }

                            //oTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct, jsonList.data[i].CONDITION]);

                            var param = {
                                MANUAL_NO: jsonList.data[i].MANUAL_NO,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                DUE_DATE: jsonList.data[i].DUE_DATE,
                                NET_VALUE: maskNetValueFrequency,
                                QUANTITY: maskQuantityFrequency,
                                UNIT: jsonList.data[i].UNIT,
                                CONDITION: jsonList.data[i].CONDITION
                            }
                            arrManualFrequency.push(JSON.stringify(param));
                        }
                    }
                });

            // Variabel untuk handle combobox condition
                var cond = function () {
                    var fruits = buah;
                    var listItems = "";
                    listItems += "<option value=''>--Choose--</option>";
                    for (var i = 0; i < fruits.length; i++) {
                        listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
                    }
                    //$("#memo-condition").append(listItems);
                    $('select[name="condition-frequency"]').html(listItems);
                }

            //Generate textbox(form) di data table manual-frequency
                //$('#btn-add-manual-frequency').on('click', function () {
                //    var nomor = 10;

                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#manual-frequency').DataTable();
                //    var xtable = $('#manual-frequency').dataTable();
                //    var data = table.row(baris).data();
                //    var g = xtable.fnGetData();
                //    var l = g.length;
                //    var d = xtable.fnGetData(g - 1);


                //    var last = $("#manual-frequency").find("tr:last td:first").text();
                //    //console.log('last ' + last);
                //    //Untuk Button Add Atas
                //    var oke = 0;
                //    console.log(l);
                //    if (l != 0) {
                //        nomor = (Number(last) + 10);
                //    }


                //    //Form input di data table manual-frequency
                //    var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
                //    var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
                //    // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
                //    var dueDateFrequency = '<input type="text" class="form-control" id="due-date-frequency" name="due-date-frequency">';
                //    var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
                //    var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
                //    var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
                //    var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';

                //    oTable.fnAddData([nomor,
                //                      condition,
                //                      dueDateFrequency,
                //                      netValueFrequency,
                //                      qtyFrequency,
                //                      unitFrequency,
                //                      '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                //                      '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                //                      conditionCode], true);
                //    //Masking Due Date
                //    $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                //        "placeholder": "dd/mm/yyyy"
                //    });

                //    //Masking Net Value

                //    //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
                //    $('input[name = "net-value-frequency"]').on('change', function () {
                //        var netValueFrequency = $('input[name = "net-value-frequency"]').val();
                //        var mask = sep1000(netValueFrequency, true);
                //        $('input[name = "net-value-frequency"]').val(mask);
                //    });

                //    $('input[name = "qty-frequency"]').inputmask({
                //        mask: "9",
                //        repeat: 10,
                //        greedy: !1
                //    });

                //    // Panggil variable yg handle combobox condition
                //    cond();
                //});

                //$('#manual-frequency').on('click', 'tr #btn-add-sama', function () {
                //    var table = $('#manual-frequency').DataTable();
                //    var data = table.row($(this).parents('tr')).data();

                //    var manual_no_sama = data[0];
                //    //Form input di data table manual-frequency
                //    var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
                //    var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control" onchange="getConditionCode(this)"></select>';
                //    //var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
                //    var dueDateFrequency = '<input type="text" class="form-control" id="due-date-frequency" name="due-date-frequency">';
                //    var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
                //    var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
                //    var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
                //    var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';


                //    oTable.fnAddData([manual_no_sama,
                //                      condition,
                //                      dueDateFrequency,
                //                      netValueFrequency,
                //                      qtyFrequency,
                //                      unitFrequency,
                //                      '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                //                      '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                //                      conditionCode], true);

                //    //Masking Due Date
                //    $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                //        "placeholder": "dd/mm/yyyy"
                //    });

                //    //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
                //    $('input[name = "net-value-frequency"]').on('change', function () {
                //        var netValueFrequency = $('input[name = "net-value-frequency"]').val();
                //        var mask = sep1000(netValueFrequency, true);
                //        $('input[name = "net-value-frequency"]').val(mask);
                //    });

                //    // Panggil variable yg handle combobox condition
                //    cond();
                //});

                var conditionCancel = '';
                var dueDateCancel = '';
                var netValueCancel = '';

            // BTN EDITCUS MANUAL FREQUENCY
                //$('#manual-frequency').on('click', 'tr #btn-editcus', function () {
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#manual-frequency').DataTable();
                //    var data = table.row(baris).data();

                //    var condVal = data[1];
                //    var dueDate = data[2];
                //    var netValue = data[3];
                //    var qty = data[4];
                //    var unitVal = data[5];
                //    var conditionCodeVal = data[8];

                //    conditionCancel = condVal;
                //    dueDateCancel = dueDate;
                //    netValueCancel = netValue;

                //    var nRow = $(this).parents('tr')[0];
                //    var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus-frequency"><i class="fa fa-check"></i></a>';

                //    // Open Data Table
                //    var openTable = $('#manual-frequency').dataTable();



                //    //Form input di data table manual-frequency
                //    var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
                //    var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
                //    // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
                //    var dueDateFrequency = '<input type="text" class="form-control" value="' + dueDate + '" id="due-date-frequency" name="due-date-frequency">';
                //    var netValueFrequency = '<input type="text" class="form-control" value="' + netValue + '" id="net-value-frequency" name="net-value-frequency">';
                //    var qtyFrequency = '<input type="text" class="form-control" value="' + qty + '" id="qty-frequency" name="qty-frequency">';
                //    var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
                //    var conditionCode = '<input type="text" class="form-control sembunyi" value="' + conditionCodeVal + '" id="CONDITION_CODE" name="CONDITION_CODE">';

                //    openTable.fnUpdate(condition, nRow, 1, false);
                //    openTable.fnUpdate(dueDateFrequency, nRow, 2, false);
                //    openTable.fnUpdate(netValueFrequency, nRow, 3, false);
                //    openTable.fnUpdate(qtyFrequency, nRow, 4, false);
                //    openTable.fnUpdate(unitFrequency, nRow, 5, false);

                //    openTable.fnUpdate(tombol, nRow, 7, false);
                //    openTable.fnUpdate(conditionCode, nRow, 8, false);
                //    cond();


                //    $("#condition-frequency option[value='" + condVal + "']").prop('selected', true);
                //    $("#unit-frequency option[value='" + unitVal + "']").prop('selected', true);

                //    //Masking Due Date
                //    $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                //        "placeholder": "dd/mm/yyyy"
                //    });

                //    //Masking Net Value

                //    //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
                //    $('input[name = "net-value-frequency"]').on('change', function () {
                //        var netValueFrequency = $('input[name = "net-value-frequency"]').val();
                //        var mask = sep1000(netValueFrequency, true);
                //        $('input[name = "net-value-frequency"]').val(mask);
                //    });

                //    $('input[name = "qty-frequency"]').inputmask({
                //        mask: "9",
                //        repeat: 10,
                //        greedy: !1
                //    });

                //    // Panggil variable yg handle combobox condition


                //});

                //$('#manual-frequency').on('click', 'tr #btn-delcus-frequency', function () {
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#manual-frequency').DataTable();
                //    var data = table.row(baris).data();
                //    oTable.fnDeleteRow(baris);
                //});

                //$('#manual-frequency').on('click', 'tr #btn-delcus', function () {
                //    var baris = $(this).parents('tr')[0];
                //    var table = $('#manual-frequency').DataTable();
                //    var data = table.row(baris).data();
                //    oTable.fnDeleteRow(baris);
                //});

            //    $('#manual-frequency').on('click', 'tr #btn-cancelcus', function (e) {
            //        e.preventDefault();

            //        var baris = $(this).parents('tr')[0];
            //        var table = $('#manual-frequency').DataTable();
            //        var data = table.row(baris).data();

            //        var manualCancel = data[0];

            //        console.log('manual cancel ' + manualCancel);
            //        console.log('condition cancel ' + conditionCancel);

            //        // Hapus Row 
            //        oTable.fnDeleteRow(baris);
            //        var json = JSON.parse('[' + arrManualFrequency + ']');


            //        // Deklarasi Variabel       
            //        var MANUAL_NO = "";
            //        var CONDITION_TYPE = "";
            //        var DUE_DATE = "";
            //        var NET_VALUE = "";
            //        var QUANTITY = "";
            //        var UNIT = "";
            //        var CONDITION = "";


            //        json.forEach(function (object) {
            //            if (object.MANUAL_NO == manualCancel && object.CONDITION_TYPE == conditionCancel && object.DUE_DATE == dueDateCancel) {
            //                MANUAL_NO = object.MANUAL_NO;
            //                CONDITION_TYPE = object.CONDITION_TYPE;
            //                DUE_DATE = object.DUE_DATE;
            //                NET_VALUE = object.NET_VALUE;
            //                QUANTITY = object.QUANTITY;
            //                UNIT = object.UNIT;
            //                CONDITION = object.CONDITION;
            //            }
            //        });


            //        var tombolAddSama = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
            //        var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            //        oTable.fnAddData([MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT, tombolAddSama, tombolC, CONDITION]);
            //    });

            //    $('#manual-frequency').on('click', 'tr #btn-delcus-frequency', function () {
            //        var baris = $(this).parents('tr')[0];
            //        var table = $('#manual-frequency').DataTable();
            //        var data = table.row(baris).data();
            //        oTable.fnDeleteRow(baris);
            //    });

            //    $('#manual-frequency').on('click', 'tr #btn-savecus-frequency', function () {
            //        var baris = $(this).parents('tr')[0];
            //        var table = $('#manual-frequency').DataTable();
            //        var data = table.row(baris).data();

            //        var a = $('#condition-frequency').val();
            //        var b = $('#due-date-frequency').val();
            //        var c = $('#net-value-frequency').val();
            //        var d = $('#CONDITION_CODE').val();
            //        var e = $('#qty-frequency').val();
            //        var f = $('#unit-frequency').val();
            //        console.log(f);

            //        //console.log(baris);
            //        var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            //        $('#manual-frequency').dataTable().fnUpdate(a, baris, 1, false);
            //        oTable.fnUpdate(b, baris, 2, false);
            //        oTable.fnUpdate(c, baris, 3, false);
            //        oTable.fnUpdate(e, baris, 4, false);
            //        oTable.fnUpdate(f, baris, 5, false);
            //        oTable.fnUpdate(d, baris, 8, false);
            //        oTable.fnUpdate(tombolDefault, baris, 7, false);
            //        return false;
            //    });
            ////------------------------------- END OF MANUAL FREQUENCY -------------------------------
            //---------------------------------------- MEMO -----------------------------------------
                var tablem = $('#table-memo');
                var mTable = tablem.dataTable({
                    "lengthMenu": [
                        [5, 15, 20, 50],
                        [5, 15, 20, 50]
                    ],
                    "pageLength": 20,
                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "searching": false,
                    "lengthChange": false,
                    "bSort": false
                });

            // Ajax untuk get data memo sebelumnya
                var arrMemo = [];

                //var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
                //var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            // Ajax untuk menampilkan data manual frequency sebelumnya
                $.ajax({
                    type: "GET",
                    url: "/ContractChange/GetDataDetailMemoEdit/" + CONTRACT_NO,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrMemo = new Array();
                        mTable.fnClearTable();
                        var jsonList = data;
                        var res_condition = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            mTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].MEMO, tombolAct]);

                            var param = {
                                CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                                CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                                MEMO: jsonList.data[i].MEMO
                            }
                            arrMemo.push(JSON.stringify(param));
                        }
                        //console.log(arrMemo);
                    }
                });

                $('#btn-add-memo').on('click', function () {
                    $('#memo-condition').html('');
                    $('#memo').val('');

                    var fruits = buah;
                    var listItems = "";
                    listItems += "<option value=''>--Choose--</option>";
                    for (var i = 0; i < fruits.length; i++) {
                        listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
                    }
                    //console.log(listItems);
                    $("#memo-condition").append(listItems);

                });

                $('#btn-add-memo-md').on('click', function () {
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();

                    var raw_val = $('#memo-condition').val();
                    var v_split = raw_val.split("|");

                    var memo_calc_object = v_split[0];
                    var memo_cond_type = v_split[1];
                    var memo = $('#memo').val();

                    mTable.fnAddData([memo_calc_object,
                                      memo_cond_type,
                                      memo,
                                     '<center><a class="btn default btn-xs red" id="btn-delcus-memo"><i class="fa fa-trash"></i></a></center>'], true);
                    $('#memo').html('');
                    $('#addMemo').modal('hide');
                });

                var memoCancel = '';
                var conditionTypeCancel = '';
                var calObjectCancel = ''
                $('#table-memo').on('click', 'tr #btn-editcus', function () {
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();

                    var calObjectVal = data[0];
                    var memoVal = data[2];
                    var conditionTypeVal = data[1];

                    calObjectCancel = calObjectVal;
                    conditionTypeCancel = conditionTypeVal;
                    memoCancel = memoVal;

                    var nRow = $(this).parents('tr')[0];
                    var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

                    // Open Data Table
                    var openTable = $('#table-memo').dataTable();
                    var updateMemo = '<input type="text" id="memo" name="memo" value="' + memoCancel + '" class="form-control">';

                    openTable.fnUpdate(updateMemo, nRow, 2, false);
                    openTable.fnUpdate(tombol, nRow, 3, false);
                });

                $('#table-memo').on('click', 'tr #btn-cancelcus', function (e) {
                    e.preventDefault();

                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();

                    // Hapus Row 
                    mTable.fnDeleteRow(baris);
                    var json = JSON.parse('[' + arrMemo + ']');

                    // Deklarasi Variabel       
                    var CALC_OBJECT = "";
                    var CONDITION_TYPE = "";
                    var MEMO = "";

                    json.forEach(function (object) {
                        if (object.CALC_OBJECT == calObjectCancel && object.CONDITION_TYPE == conditionTypeCancel && object.MEMO == memoCancel) {
                            CALC_OBJECT = object.CALC_OBJECT;
                            CONDITION_TYPE = object.CONDITION_TYPE;
                            MEMO = object.MEMO;
                        }
                    });

                    var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

                    mTable.fnAddData([CALC_OBJECT, CONDITION_TYPE, MEMO, tombolC]);
                });

                $('#table-memo').on('click', 'tr #btn-savecus', function () {
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();

                    var memo = $('#memo').val();

                    var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
                    $('#table-memo').dataTable().fnUpdate(memo, baris, 2, false);
                    $('#table-memo').dataTable().fnUpdate(tombolDefault, baris, 3, false);

                    return false;

                });

                $('#table-memo').on('click', 'tr #btn-delcus-memo', function () {
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();
                    mTable.fnDeleteRow(baris);
                });

                $('#table-memo').on('click', 'tr #btn-delcus', function () {
                    var baris = $(this).parents('tr')[0];
                    var table = $('#table-memo').DataTable();
                    var data = table.row(baris).data();
                    mTable.fnDeleteRow(baris);
                });
            //------------------------------------- END OF MEMO -------------------------------------
    /*
        }
    }, 100);
    */



    var editDataCondition = function () {
        $('#btn-add-condition').on('click', function () {
            var CONTRACT_NO = $('#CONTRACT_NO').val();

            //Isi combobox condition-option
            $('#condition-option').html('');
            $.ajax({
                type: "GET",
                url: "/ContractChange/GetDataDropDownRO/" + CONTRACT_NO,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += "<option value=''>-- Choose Condition Option --</option>";
                    listItems += "<option value='Header Document'>Header Document</option>";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].OBJECT_ID + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>";
                    }
                    $("#condition-option").append(listItems);
                }
            });

            // Ajax untuk ambil dan generate checkbox condition type dari parameter ref d
            $('#condition-option').change(function () {
                var condition_option = $('#condition-option').val();

                $('#cekbokList').html('');
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetCheckboxConditionType",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            var valueCombobox = jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2;

                            if (condition_option == 'Header Document') {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }
                            else {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox'  + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }

                        }
                        $("#cekbokList").append(listItems);
                    }
                });
            });
        });

        /*
        ('#condition').on('click', 'tr #btn-editcusx', function () {
            $('#condition > tbody').html('');
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data;

            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input class="form-control" id="mask_date1" type="text" />';

            }

            //table.fnUpdate(xInd, baris, 18, false);
            document.getElementById('btn-savecus').disabled = true;

        });
        */

        $('#condition').on('click', 'tr #btn-savecusx', function () {
            //$(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var start_date = $('#start-date').val();
            var end_date = $('#end-date').val();
            var term_months = $('#term-months').val();

            var unit_price = $('#unit-price').val();
            var amt_ref = $('#amt-ref').val();
            var frequency = $('#frequency').val();
            var start_due_date = $('#start-due-date').val();
            var manual_no = $('#manual-no').val();
            var formula = $('#formula').val();

            var meas_type = $('#meas-type').val();
            var splitselectedMeasType = meas_type.split("|");
            var id_detail_meas_type = splitselectedMeasType[0];

            var luas = $('#luas').val();
            var total = $('#total').val();
            var persen_from_njop = $('#persen-from-njop').val();
            var persen_from_kondisi_teknis = $('#persen-from-kondisi-teknis').val();

            //var sales_rule = $('#sales-rule').val();
            var total_net_value = $('#total-net-value').val();

            //Set Value Untuk Statistic
            var cek = $('input[id=statistic]').is(":checked");
            var vStatistic = "";
            if (cek) {
                vStatistic = "Yes";
            }
            else {
                vStatistic = "No";
            }

            $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
            $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
            $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
            $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
            $('#condition').dataTable().fnUpdate(unit_price, baris, 6, false);
            $('#condition').dataTable().fnUpdate(amt_ref, baris, 7, false);
            $('#condition').dataTable().fnUpdate(frequency, baris, 8, false);
            $('#condition').dataTable().fnUpdate(start_due_date, baris, 9, false);
            $('#condition').dataTable().fnUpdate(manual_no, baris, 10, false);
            $('#condition').dataTable().fnUpdate(formula, baris, 11, false);
            $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
            $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
            $('#condition').dataTable().fnUpdate(total, baris, 14, false);
            $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
            $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
            //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
            $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus');
            elem.parentNode.removeChild(elem);
            return false;

        });
    }

    // Edit Data Manual Frequency
    var editDataManualFrequency = function () {

        
    }

    //Var untuk menghandle data table edit memo
    var editDataMemo = function () {
        
    }

    // Var untuk handle update data
    var simpanUpdateData = function () {
        $('#btn-update').click(function () {
            // Header Data
            var CONTRACT_NO = $('#CONTRACT_NO').val();
            var IJIN_PRINSIP_NO = $('#IJIN_PRINSIP_NO').val();
            var IJIN_PRINSIP_DATE = $('#IJIN_PRINSIP_DATE').val();
            var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO').val();
            var LEGAL_CONTRACT_DATE = $('#LEGAL_CONTRACT_DATE').val();
            
            var arrContractConditions;
            var arrContractFrequencies;
            var arrContractMemos;

            //cek apakah pada condition terdapat kondisi biaya administrasi atau tidak
            var resultCek = 0;
            var DTConditionCek = $('#condition').dataTable();
            var countDTConditionCek = DTConditionCek.fnGetData();
            secondCellArray = [];
            for (var i = 0; i < countDTConditionCek.length; i++) {
                var itemDTConditionCek = DTConditionCek.fnGetData(i);
                secondCellArray.push(itemDTConditionCek[1]);
            }

            if (secondCellArray.indexOf("Z009 ADMINISTRASI") > -1) {
                resultCek = 1;
                console.log('ada');
            } else {
                resultCek = 0;
                console.log('tidak ada');
            }
            console.log(resultCek);


            if (CONTRACT_NO) {
                // start getData dari dataTable CONDITION
                var DTCondition = $('#condition').dataTable();
                var countDTCondition = DTCondition.fnGetData();
                arrContractConditions = new Array();
                for (var i = 0; i < countDTCondition.length; i++) {
                    var itemDTCondition = DTCondition.fnGetData(i);

                    // hitung pembagian total net value berdasarkan frekuensi pembayaran
                    var freq_pembayaran = itemDTCondition[8];
                    var net_value = itemDTCondition[17];
                    var formulanya = itemDTCondition[11];
                    var jumlah_bulan = itemDTCondition[4];
                    var pecah_bayar = 0;

                    // Cek apakah frekuensi pembayaran bulanan atau tahunan
                    if (freq_pembayaran == 'YEARLY') {
                        var tahunan = Number(jumlah_bulan) / 12;
                        pecah_bayar = Number(net_value) / Number(tahunan);
                        console.log('tahunan ' + pecah_bayar);
                    }
                    else if (freq_pembayaran == 'MONTHLY') {
                        var bulanan = jumlah_bulan;
                        pecah_bayar = Number(net_value) / Number(bulanan);
                        console.log('bulanan ' + pecah_bayar);
                    }
                    else {
                        pecah_bayar = Number(net_value);
                        console.log('else ' + pecah_bayar);
                    }

                    // Cek apakah status condition yes atau no
                    var status_condition = itemDTCondition[5];
                    var nilai_condition = 0;
                    if (status_condition == "Yes") {
                        nilai_condition = 1;
                    }
                    else {
                        nilai_condition = 0;
                    }

                    var rUnitPrice = itemDTCondition[6];
                    var nfUnitPrice = parseFloat(rUnitPrice.split(',').join(''));
                    //var nfUnitPrice = rUnitPrice;

                    var rTotal = itemDTCondition[14];
                    var nfTotal = parseFloat(rTotal.split(',').join(''));
                    //var nfTotal = rTotal;

                    var rTotalNetValue = itemDTCondition[17];
                    var coa_prod = itemDTCondition[18];
                    var nfTotalNetValue = parseFloat(rTotalNetValue.split(',').join(''));
                    //var nfTotalNetValue = rTotalNetValue;

                    //split calc object untuk menghapus tulisan RO dan disimpan di table OBJECT_ID
                    var objek = itemDTCondition[0];
                    var splitObject = objek.split(" ");
                    var tulisanRO = splitObject[0];
                    var kodeObjek = splitObject[1];
                    var OBJECT_ID = '';
                    if (tulisanRO == 'RO'){
                        OBJECT_ID = kodeObjek;
                    }
                    else {
                        OBJECT_ID = objek;
                    }

                    var paramDTCondition = {
                        CALC_OBJECT: itemDTCondition[0],
                        CONDITION_TYPE: itemDTCondition[1],
                        VALID_FROM: itemDTCondition[2],
                        VALID_TO: itemDTCondition[3],
                        MONTHS: itemDTCondition[4],
                        STATISTIC: nilai_condition,
                        UNIT_PRICE: nfUnitPrice,
                        AMT_REF: itemDTCondition[7],
                        FREQUENCY: itemDTCondition[8],
                        START_DUE_DATE: itemDTCondition[9],
                        MANUAL_NO: itemDTCondition[10],
                        FORMULA: itemDTCondition[11],
                        MEASUREMENT_TYPE: itemDTCondition[12],
                        LUAS: itemDTCondition[13],
                        TOTAL: nfTotal,
                        NJOP_PERCENT: itemDTCondition[15],
                        KONDISI_TEKNIS_PERCENT: itemDTCondition[16],
                        TOTAL_NET_VALUE: nfTotalNetValue,
                        COA_PROD: itemDTCondition[18],
                        TAX_CODE: itemDTCondition[20],
                        INSTALLMENT_AMOUNT: pecah_bayar,
                        OBJECT_ID: OBJECT_ID
                    };
                    arrContractConditions.push(paramDTCondition);
                }
                //END PUSH CONDITION
                //Declare dan get data dari dataTable MANUAL FREQUENCY
                var DTManualFrequency = $('#manual-frequency').dataTable();
                var countDTManualFrequency = DTManualFrequency.fnGetData();
                arrContractFrequencies = new Array();
                for (var i = 0; i < countDTManualFrequency.length; i++) {
                    var itemDTManualFrequency = DTManualFrequency.fnGetData(i);

                    //potong2  
                    var raw_manual_freq = itemDTManualFrequency[1];
                    var splitselectedCondition = raw_manual_freq.split(" | ");
                    var code_lengkap = splitselectedCondition[1];
                    var cut = code_lengkap.substring(0, 4);
                    console.log(cut);

                    var splitChar = raw_manual_freq.split(" | ");
                    var objectId = splitChar[0];

                    var net_value = itemDTManualFrequency[3];
                    var nfNetVal = parseFloat(net_value.split(',').join(''));
                    var paramDTManualFrequency = {
                        MANUAL_NO: itemDTManualFrequency[0],
                        CONDITION_TYPE: cut, //Untuk condition ambil id CONDITION_CODE di index 9 yg di hidden
                        DUE_DATE: itemDTManualFrequency[2],
                        QUANTITY: itemDTManualFrequency[4],
                        UNIT: itemDTManualFrequency[5],
                        NET_VALUE: nfNetVal,
                        OBJECT_ID: objectId
                    }
                    arrContractFrequencies.push(paramDTManualFrequency);
                }
                //END PUSH FREQUENCY
                //Declare dan get data dari dataTable MEMO
                var DTMemo = $('#table-memo').dataTable();
                var countDTMemo = DTMemo.fnGetData();
                arrContractMemos = new Array();
                for (var i = 0; i < countDTMemo.length; i++) {
                    var itemDTMemo = DTMemo.fnGetData(i);
                    var paramDTMemo = {
                        OBJECT_CALC: itemDTMemo[0],
                        CONDITION_TYPE: itemDTMemo[1],
                        MEMO: itemDTMemo[2]
                    }
                    arrContractMemos.push(paramDTMemo);
                }
                var param = {
                    CONTRACT_NO: CONTRACT_NO,
                    IJIN_PRINSIP_NO: IJIN_PRINSIP_NO,
                    IJIN_PRINSIP_DATE: IJIN_PRINSIP_DATE,
                    LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO,
                    LEGAL_CONTRACT_DATE: LEGAL_CONTRACT_DATE,
                    Conditions: arrContractConditions,
                    Frequencies: arrContractFrequencies,
                    Memos: arrContractMemos
                };
                console.log(param);
                // Ajax Save Data Header
                App.blockUI({ boxed: true });
                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/ContractChange/UpdateHeader",

                  success: function (data) {
                        if (data.status === 'S') {
                            console.log('data messagenya sukses : ' + data.message);
                          swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransContract";
                            });
                        } else {
                           swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransContract";
                            });
                        }
                        App.unblockUI();
                    },
                    error: function (data) {
                        console.log('data messagenya error : ' + data.status);
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/TransContract";

                        });
                    }
                });
                
                arrContractObjects = [];
                arrContractConditions = [];
                arrContractFrequencies = [];
                arrContractMemos = [];
            }
            else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });


    }

    var initDetailHistoryBilling = function () {
        $('#btn-history-posted-billing').click(function () {
            var CONTRACT_NO = $('#CONTRACT_NO').val();

            // Get Data History Billing
            var tableDetailBilling = $('#table-history-posted-billing').DataTable();
            tableDetailBilling.destroy();

            $('#table-history-posted-billing').DataTable({
                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailPostedBilling/" + CONTRACT_NO,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "BILLING_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SAP_DOC_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<center><a data-toggle="modal" href="#viewModalDetailPostedBilling" class="btn btn-icon-only purple" data-toggle="tooltip" title="Detail Posted Billing" id="btn-detail-posted-billing"><i class="fa fa-eye"></i></a></center>';
                            return aksi;
                        }
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
   }
   
    var initDetailHistoryBillingD = function () {
        $('#table-history-posted-billing').on('click', 'tr #btn-detail-posted-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-history-posted-billing').DataTable();
            var data = table.row(baris).data();

            var BILLING_ID = data['BILLING_ID'];
            console.log(BILLING_ID);

            // Get Data History Billing
            var tableDetailBillingD = $('#table-history-posted-billing-d').DataTable();
            tableDetailBillingD.destroy();

            $('#table-history-posted-billing-d').DataTable({
                "ajax":
                {
                    "url": "/ContractChange/GetDataDetailPostedBillingD/" + BILLING_ID,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BILLING_PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLMENT_AMOUNT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });       
    }

    return {
        init: function () {
            batal();
            hitung_bulan();
            hitung_tanggal_sampai();
            filter();
            simpanUpdateData();
            editDataCondition();
            editDataManualFrequency();
            editDataMemo();
            addCondition();
            initTablehistoryPerubahanContract();
            initDetilTransactionContract();
            initDetailHistoryBilling();
            initDetailHistoryBillingD();
        }

    };

}();

// ************** FUNCTION UNTUK HANDLE SALES RULE ********************
//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getUnitST(this_cmb) {
    var selectedSalesType = $(this_cmb).val();
    var splitselectedSalesType = selectedSalesType.split(" - ");
    var id_sales_type = splitselectedSalesType[0];
    var nama_sales_type = splitselectedSalesType[1];

    //Ajax untuk get UNIT
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataUnitST/" + id_sales_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].UNIT;
            }
            $(this_cmb).parents('tr').find('input[name="UNIT_ST"]').val(listItems);
        }
    });
}
// ************** END FUNCTION UNTUK HANDLE SALES RULE ********************

//Function untuk isi condition code
function getConditionCode(this_cmb) {
    var selectedCondition = $(this_cmb).val();

    if (selectedCondition) {
        var splitselectedCondition = selectedCondition.split(" | ");
        var code_lengkap = splitselectedCondition[1];
        var cut = code_lengkap.substring(0, 4);
        console.log(selectedCondition);
        console.log(code_lengkap);
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val(cut);
    }
    else {
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val("");
    }
}

//Function untuk add row table manual frequency 
function addManualFreq(this_x) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var tim = data[4];
}

//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getLuas(this_cmb) {
    console.log("test test " + $(this_cmb).val());
    var selectedMeasType = $(this_cmb).val();
    var splitselectedMeasType = selectedMeasType.split("|");
    var id_detail_meas_type = splitselectedMeasType[1];

    //Ajax untuk get Luas(M2)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataLuasMeasType/" + id_detail_meas_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].LUAS;
            }
            // $("#luas").val(listItems);
            $(this_cmb).parents('tr').find('input[name="luas"]').val(listItems);


            //Mengisi field total value saat field luas terisi dengan value ajax listItems
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            //formulanya
            var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
            if (formula == 'A') {

            }
            else if (formula == 'U') {

            }
            else {
                //var tim = data[4];
                var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
                var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
                var luas = listItems;
                var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

                nMUnitPrice = parseFloat(unitPrice.split(',').join(''));
                var t = hitungTotal(tim, luas, nMUnitPrice, period);
                var fT = Math.round(t).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

                //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
                var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
                var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
                var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
                var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

                var nfTotal = parseFloat(total.split(',').join(''));
                var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
                var nfPersenFromKondisiteknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));


                var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiteknis);
                var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                console.log(fNetValue);
                $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
            }
        }
    });
}


//---------------------------HITUNG TOTAL----------------------------
//ACTION UNTUK MENGISI TOTAL VALUE SAAT CLICK COMBO AMT REF
function isiTotalAMTREF(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var period = $(this_cmb).val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);

    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        //Hitung Total Untuk Formula D
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(unit_price.split(',').join(''));
        var rTotal = hitungTotal(tim, luas, nfHarga, kode_periode);

        var fTotal = rTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fTotal);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = netVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}

function isiTotalUnitPrice(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = Math.round(unit_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        var fT = t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

        var njop;
        var kondisi_teknis;

        // update total
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        console.log('total bulan ' + total_bulan);
        console.log('nf harga ' + nfHarga);
        console.log('luas ' + luas);
        console.log('kode periode ' + kode_periode);

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        console.log('formula ' + formula);
        console.log('njop ' + njop);
        console.log('kondisi teknis ' + persenFromKondisiTeknis);

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}
//---------------------END OF HITUNG TOTAL----------------------------

function hitungTotal(total_bulan, luas, harga, kode_periode) {
    var total = 0;
    if (kode_periode == 'YEARLY') {
        var year = Number(total_bulan) / 12;
        total = (Number(luas) * Number(harga)) * Number(year);
    }
    else if (kode_periode == 'MONTHLY') {
        total = (Number(luas) * Number(harga)) * Number(total_bulan);
    }
    else {
        total = Number(luas) * Number(harga);
    }
    return total;
}

//Action Saat Formula Dipilih
function hitungFormula(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
    var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();

    var amt_ref = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split('.').join(''));
    var nfLuas = parseFloat(luas.split('.').join(''));
    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    if (formula == 'A') {
        //var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        // $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        // $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'D') {
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //Formula Lama
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);

        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
    }
}

//Action Saat Formula sudah dipilih dan mengisi persentase njop
function hitungFormulaN(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    //var nfPersenFromNjop = parseFloat(persen_from_njop.split('.').join(''));
    var nfPersenFromNjop = persen_from_njop;
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    console.log('net value ' + net_value);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}

//Action Saat Formula sudah dipilih dan mengisi persentase kondisi teknis
function hitungFormulaT(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = persen_from_njop;
    //var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split('.').join(''));
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // ini masih kurang sip
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}



//Function untuk hitung net value berdasarkan formula yang dipilih
function netValue(formula, total, njop, kondisi_teknis) {
    var totalNetValue = 0;
    if (formula == 'D') {
        totalNetValue = Number(total);
    }
    else if (formula == 'E1') {
        if (njop == '' && kondisi_teknis == '') {
            totalNetValue = Number(total);
        }
        else if (njop == '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100);
        }
        else if (kondisi_teknis == '') {
            totalNetValue = Number(total) * (Number(njop) / 100);
        }
        else if (njop != '' && kondisi_teknis != '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100) * (Number(njop) / 100);
        }
        else {

        }
    }
    else if (formula == 'A') {
        totalNetValue = Number(total);
    }
    else {

    }

    return totalNetValue;
}

// Function untuk remove array value berdasarkan value
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function hitungTermMonths(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val();

    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

    var dateAkhir = t;
    var datearrayAkhir = dateAkhir.split("/");
    var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

    var tim = monthDiff2(new Date(newdateMulai), new Date(newdateAkhir));

    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val(tim);

    // handle perhitungannya
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var tim2 = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tx = hitungTotal(tim2, luas, unitPrice, period);
    $(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function hitungEndDate(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

    //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
    var f_date = new Date(newdateMulai);
    var n_tim = Number(tm);
    var end_date = addMonths2(f_date, n_tim);
    var xdate = new Date(end_date);
    var f_date = xdate.toISOString().slice(0, 10);

    //format f_date menjadi dd/mm/YYYY
    var dateAkhir = f_date;
    var datearrayAkhir = dateAkhir.split("-");
    var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];

    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val(newdateAkhir);

    // handle perhitungannya
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tx = hitungTotal(tim, luas, unitPrice, period);
    $(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function monthDiff2(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();

    if (d2.getDate() >= d1.getDate())
        months++

    return months <= 0 ? 0 : months;
}

function addMonths2(date, count) {
    if (date && count) {
        var m, d = (date = new Date(+date)).getDate()

        date.setMonth(date.getMonth() + count, 1)
        m = date.getMonth()
        date.setDate(d)
        if (date.getMonth() !== m) date.setDate(0)
    }
    return date
}


// Function untuk handle save Sales Rule (Formula U)
function saveSalesRule() {
    // Untuk get data table condition

    // Untuk ambil sales rule

    // 
}

function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
       , sep = usa ? ',' : '.'
       , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
           , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}

function getCoaProduksi(coa_prod) {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksi",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(".coa_prod").html('');
            $(".coa_prod").append(listItems);
            $(".coa_prod option[value='" + coa_prod + "']").prop('selected', true);
        }
    });
}

jQuery(document).ready(function () {
    TableDatatablesEditable.init();
    buttonDisabled.init();
    $('#condition > tbody').html('');
    $('#condition-option > option').html('');
    $('#manual-frequency > tbody').html('');
    $('#table-memo > tbody').html('');
    $('#rental-object-co > tbody').html('');
    $('#sales-based > tbody').html('');
    $('#reporting-rule > tbody').html('');
    $('.sembunyi').hide();
    
    var currency = $('#CURRENCY').val();
    $("#CONTRACT_CURRENCY option[value='" + currency + "']").prop('selected', true);
});