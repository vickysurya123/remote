﻿var DetailRO = function () {
    var id = $('#RO_NUMBER').val();
    var initTableRentalObject = function () {
       
        $('#table-occupancy').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataOccupancy/" + id,
                "type": "GET",
            },
            "columns": [
            {
                "data": "F_VALID_FROM",
                "class": "dt-body-center"
            },
            {
                "data": "F_VALID_TO",
                "class": "dt-body-center"
            },
            {
                "data": "REASON",
                "class": "dt-body-center"
            },
            {
                "data": "CONTRACT_NO",
                "class": "dt-body-center"
            },
            {
                "data": "STATUS",
                "class": "dt-body-center"
            }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
        
        
    }
    var initTablFixtureFitting = function () {
        $('#table-fixture-fitting').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataFixtureFitting2/" + id,
                "type": "GET",
            },
            "columns": [
                {
                    "data": "REF_DATA",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION"
                },
                {
                    "data": "POWER_CAPACITY"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }
    var initTableMeasurement = function () {
        $('#table-measurement').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataMeasurement/" + id,
                "type": "GET",
            },
            "columns": [
                {
                    "data": "REF_DATA",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION"
                },
                {
                    "data": "AMOUNT"
                },
                {
                    "data": "UNIT"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }
    var DetailStatus = function () {
        var param = {
            RO_CODE: $('#RO_CODE').val()
        };
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/RentalObject/GetROStatus",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                if (data.type == 'OCCUPIED') {
                    $('.detail').removeClass('hidden');
                    $('#detail_contract').removeClass('hidden');
                    $('#detail_contract_offer').addClass('hidden');
                    $('#c_no').val(data.message.CONTRACT_NO);
                    $('#c_name').val(data.message.CONTRACT_NAME);
                    $('#c_start_date').val(data.message.CONTRACT_START_DATE);
                    $('#c_end_date').val(data.message.CONTRACT_END_DATE);
                    $('#c_customer').val(data.message.BUSINESS_PARTNER + " - " + data.message.BUSINESS_PARTNER_NAME);
                    $('#c_customer_ar').val(data.message.CUSTOMER_AR);
                } else if (data.type == 'BOOKED') {
                    $('.detail').removeClass('hidden');
                    $('#detail_contract_offer').removeClass('hidden');
                    $('#detail_contract').addClass('hidden');
                    $('#co_no').val(data.message.CONTRACT_NO);
                    $('#co_name').val(data.message.CONTRACT_NAME);
                    $('#co_start_date').val(data.message.CONTRACT_START_DATE);
                    $('#co_end_date').val(data.message.CONTRACT_END_DATE);
                    $('#co_customer').val(data.message.BUSINESS_PARTNER + " - " + data.message.BUSINESS_PARTNER_NAME);
                    $('#co_customer_ar').val(data.message.CUSTOMER_AR);
                } else {
                    $('.detail').addClass('hidden');
                }
            } else {

            }
        });
    }
    return {
        init: function () {
            initTableRentalObject();
            initTablFixtureFitting();
            initTableMeasurement();
            DetailStatus();
        }
    };
}();

jQuery(document).ready(function () {
    DetailRO.init();
});