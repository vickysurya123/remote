﻿var reportingRule = function () {


    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/ReportingRule/Add';
        });
    }

    return {
        init: function () {
            //return function here
            add();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        reportingRule.init();
    });
}