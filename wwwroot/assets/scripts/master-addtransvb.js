﻿//AUTO COMPLETE AGENT
$("#CUSTOMER_MDM").change(function () {
    alert("");
});
$("#CUSTOMER_MDM").autocomplete({
    source: function (request, response) {
        var inp = $('#CUSTOMER_MDM').val();
        var l = inp.length;
        console.log(l);
        if (l > 2) {
            $.ajax({
                type: "POST",
                url: "/TransVB/Customer",
                data: "{ MPLG_NAMA:'" + inp + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);
                    response($.map(data, function (el) {
                        return {
                            label: el.label,
                            value: el.value,
                            address: el.address,
                            sap: el.sap
                        };
                    }));
                }
            });
        }
    },
    select: function (event, ui) {
        event.preventDefault();
        this.value = ui.item.label;
        $("#CUSTOMER_NAME").val(ui.item.name);
        $("#INSTALLATION_ADDRESS").val(ui.item.address);
        $("#CUSTOMER_SAP_AR").val(ui.item.sap);
    }
});