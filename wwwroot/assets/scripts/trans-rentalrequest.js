﻿var TransRentalRequest = function () {

    var RoleUser = $('#ROLE_USER').val();

    var initTableTransRental = function () {
        $('#table-rental-created').DataTable({

            "ajax":
            {
                "url": "TransRentalRequest/GetDataTransRental",
                "type": "GET",

            },  

            "columns": [
                {
                    "data": "RENTAL_REQUEST_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "RENTAL_REQUEST_TYPE"

                },
                {
                    "data": "RENTAL_REQUEST_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                       
                            if (full.STATUS == 'CREATED') {
                                return '<span class="label label-sm label-primary"> CREATED </span>';
                            }
                            else if (full.STATUS == 'APPROVED') {
                                return '<span class="label label-sm label-success"> APPROVED </span>';
                            }
                            else {
                                return '<span class="label label-sm label-danger"> REJECTED </span>';
                            }
                        
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {

                         if (full.ACTIVE == '1') {
                             return '<span class="label label-sm label-primary"> ACTIVE </span>';
                         }
                         else {
                             return '<span class="label label-sm label-danger"> INACTIVE </span>';
                         }

                     },
                     "class": "dt-body-center"
                 },
                 {
                     "render": function (data, type, full) {
                         if (RoleUser == '12') {
                             if (full.ACTIVE == '1' && full.CONTRACT_OFFER_NUMBER == null && full.STATUS == 'CREATED') {
                                 //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                 var aksi = '<a data-toggle="modal" href="#viewEditStatus" data-toggle="tooltip" title="Approve Rental Request" class="btn btn-icon-only blue" id="btn-update-status" data-id="' + full.RENTAL_REQUEST_NO + '"><i class="fa fa-check"></i></a>';
                                 // aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" data-toggle="tooltip" title="Rental Request Details" class="btn btn-icon-only green" id="btn-detail-rental-created"><i class="fa fa-eye"></i></a>';

                             }
                             else {
                                 //var aksi = '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                 var aksi = '<button data-toggle="tooltip" title="Approve Rental Request" data-toggle="modal" href="#viewEditStatus" class="btn btn-icon-only blue" id="btn-update-status" disabled><i class="fa fa-check"></i></button>';
                                 // aksi += '<button class="btn btn-icon-only blue" id="btn-status" disabled><i class="fa fa-exchange"></i></button>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" data-toggle="tooltip" title="Rental Request Details" class="btn btn-icon-only green" id="btn-detail-rental-created"><i class="fa fa-eye"></i></a>';
                             }
                         }
                         else {
                             if (full.ACTIVE == '1' && full.CONTRACT_OFFER_NUMBER == null && full.STATUS == 'CREATED') {
                                 var aksi = '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Rental Request" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                  aksi += '<a data-toggle="modal" href="#viewEditStatusStaff" class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive Rental Request" id="btn-update-status" data-id="' + full.RENTAL_REQUEST_NO + '"><i class="fa fa-close"></i></a>';
                                 // aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                                  aksi += '<a data-toggle="modal" href="#viewTransRentalModal" class="btn btn-icon-only green" data-toggle="tooltip" title="Rental Request Details" id="btn-detail-rental-created"><i class="fa fa-eye"></i></a>';

                             }
                             else {
                                 var aksi = '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                 aksi += '<button data-toggle="modal" href="#viewEditStatusStaff" class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive Rental Request" id="btn-update-status" disabled><i class="fa fa-close"></i></button>';
                                 // aksi += '<button class="btn btn-icon-only blue" id="btn-status" disabled><i class="fa fa-exchange"></i></button>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" class="btn btn-icon-only green" data-toggle="tooltip" title="Rental Request Details" id="btn-detail-rental-created"><i class="fa fa-eye"></i></a>';
                             }
                         }
                         
                         return aksi;
                         
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var initTableTransRentalApproved = function () {
        $('#table-rental-approved').DataTable({

            "ajax":
            {
                "url": "TransRentalRequest/GetDataTransRentalApproved",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "RENTAL_REQUEST_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "RENTAL_REQUEST_TYPE"

                },
                {
                    "data": "RENTAL_REQUEST_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        if (full.STATUS == 'CREATED') {
                            return '<span class="label label-sm label-primary"> CREATED </span>';
                        }
                        else if (full.STATUS == 'APPROVED') {
                            return '<span class="label label-sm label-success"> APPROVED </span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> REJECTED </span>';
                        }

                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {

                         if (full.ACTIVE == '1') {
                             return '<span class="label label-sm label-primary"> ACTIVE </span>';
                         }
                         else {
                             return '<span class="label label-sm label-danger"> INACTIVE </span>';
                         }

                     },
                     "class": "dt-body-center"
                 },
                 {
                     "render": function (data, type, full) {
                         if (RoleUser == '12') {
                             if (full.ACTIVE == '1' && full.STATUS == 'APPROVED' && full.CONTRACT_OFFER_NUMBER == null) {
                                 //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                 var aksi = '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive Rental Request" id="btn-status"><i class="fa fa-times"></i></a>';
                                 // aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" data-toggle="tooltip" title="Rental Request Details" class="btn btn-icon-only green" id="btn-detail-rental-approved"><i class="fa fa-eye"></i></a>';

                             }
                             else {
                                 //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                 var aksi = '<button data-toggle="tooltip" title="Inactive Rental Request" class="btn btn-icon-only red" id="btn-status" disabled><i class="fa fa-times"></i></button>';
                                 // aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" data-toggle="tooltip" title="Rental Request Details" class="btn btn-icon-only green" id="btn-detail-rental-approved"><i class="fa fa-eye"></i></a>';
                             }
                         }
                         else {
                             if (full.ACTIVE == '1' && full.CONTRACT_OFFER_NUMBER == null && full.STATUS == 'CREATED') {
                                 //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                 var aksi = '<a data-toggle="modal" href="#viewEditStatusStaff" class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive Rental Request" id="btn-update-status" data-id="' + full.RENTAL_REQUEST_NO + '"><i class="fa fa-close"></i></a>';
                                 // aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" class="btn btn-icon-only green" data-toggle="tooltip" title="Rental Request DetailS" id="btn-detail-rental-approved"><i class="fa fa-eye"></i></a>';

                             }
                             else {
                                 //var aksi = '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                                 var aksi = '<button data-toggle="modal" href="#viewEditStatusStaff" class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive Rental Request" id="btn-update-status" disabled><i class="fa fa-close"></i></button>';
                                 // aksi += '<button class="btn btn-icon-only blue" id="btn-status" disabled><i class="fa fa-exchange"></i></button>';
                                 aksi += '<a data-toggle="modal" href="#viewTransRentalModal" class="btn btn-icon-only green" data-toggle="tooltip" title="Rental Request DetailS" id="btn-detail-rental-approved"><i class="fa fa-eye"></i></a>';
                             }
                         }

                         return aksi;

                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var initDetilTransactionRental = function () {
        $('body').on('click', 'tr #btn-detail-rental-created', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-rental-created').DataTable();
            var data = table.row(baris).data();

            $("#DETAILRENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#DETAILRENTAL_REQUEST_TYPE").val(data["RENTAL_REQUEST_TYPE"]);
            $("#DETAILBE_ID").val(data["BE_ID"]);
            $("#DETAILRENTAL_REQUEST_NAME").val(data["RENTAL_REQUEST_NAME"]);
            $("#DETAILCONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#DETAILCONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#DETAILCUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#DETAILCUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#DETAILOLD_CONTRACT").val(data["OLD_CONTRACT"]);
            $("#DETAILSTATUS").val(data["STATUS"]);
            $("#DETAILUSAGE_TYPE").val(data["CONTRACT_USAGE"]);

            var id_rental = data["RENTAL_REQUEST_NO"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransRentalRequest/GetDataDetailRental/" + id_rental,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LAND_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BUILDING_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var initDetilTransactionRentalApproved = function () {
        $('body').on('click', 'tr #btn-detail-rental-approved', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-rental-approved').DataTable();
            var data = table.row(baris).data();

            $("#DETAILRENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#DETAILRENTAL_REQUEST_TYPE").val(data["RENTAL_REQUEST_TYPE"]);
            $("#DETAILBE_ID").val(data["BE_ID"]);
            $("#DETAILRENTAL_REQUEST_NAME").val(data["RENTAL_REQUEST_NAME"]);
            $("#DETAILCONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#DETAILCONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#DETAILCUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#DETAILCUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#DETAILOLD_CONTRACT").val(data["OLD_CONTRACT"]);
            $("#DETAILSTATUS").val(data["STATUS"]);
            $("#DETAILUSAGE_TYPE").val(data["CONTRACT_USAGE"]);

            var id_rental = data["RENTAL_REQUEST_NO"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransRentalRequest/GetDataDetailRental/" + id_rental,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LAND_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BUILDING_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }
    
    var edit = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-rental-created').DataTable();
            var data = table.row(baris).data();

            window.location = "/TransRentalRequest/EditTransRentalRequest/" + data["ID"];
        });
    }

    var showModal = function () {
        $('body').on('click', 'tr #btn-update-status', function () {
            var nilai = $(this).data('id');
            $('#RENTAL_REQUEST_NO').val(nilai);
            //console.log(nilai);
        });
    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-rental-approved').DataTable();
            var data = table.row(baris).data();
            xid = data["ID"];
            xno = data["RENTAL_REQUEST_NO"];
            
            swal({
                title: 'Warning',
                text: "Are you sure want to INACTIVE this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    console.log(xid);
                    console.log(xno);
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        //data: "id=" + data["ID"],
                        data: "{ xid:'" + xid + "', xno:'" + xno + "'}",
                        method: "POST",
                        url: "/TransRentalRequest/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            initTableTransRental();
                            initTableTransRentalApproved();
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var btnUpdateS = function () {
        $('#btn-update-status-modal').click(function () {
            console.log('update status modal');
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-rental-request').DataTable();
            var data = table.row(baris).data();

            var STATUS_APPROVAL = $('#STATUS_APPROVAL').val();
            var rental_request_no = $('#RENTAL_REQUEST_NO').val();
            console.log(STATUS_APPROVAL);
            if (STATUS_APPROVAL) {
                var param = {
                    RENTAL_REQUEST_NO: rental_request_no
                };
                console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransRentalRequest/changeStatus",
                    timeout: 30000,
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#viewEditStatusStaff').modal('hide');
                            initTableTransRental();
                            initTableTransRentalApproved();
                            swal('Success', 'Status Updated', 'success');
                        } else {
                            $('#viewEditStatusStaff').modal('hide');
                            initTableTransRental();
                            initTableTransRentalApproved();
                            swal('Failed', 'Something Went Wrong', 'error');
                        }
                    },
                    error: function (data) {
                        console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');

            }
        });
    }

    var btnEditStatus = function () {
        $('#btn-update-status-f').click(function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-rental-created').DataTable();
            var table1 = $('#table-rental-approved').DataTable();
            var data = table.row(baris).data();

            var STATUS_APPROVAL = $('#STATUS_APPROVAL').val();
            var rental_request_no = $('#RENTAL_REQUEST_NO').val();
            //console.log(STATUS_APPROVAL);
            if (STATUS_APPROVAL) {
                var param = {
                    RENTAL_REQUEST_NO: rental_request_no,
                    STATUS: STATUS_APPROVAL
                };
                //console.log(param);
                App.blockUI({ boxed: true });

                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/TransRentalRequest/StatusApproval",
                    timeout: 30000,
                    success: function (data) {
                        //console.log(data);
                        table.ajax.reload(null, false);
                        table1.ajax.reload(null, false);

                        if (data.status === 'S') {
                            $('#viewEditStatus').modal('hide');
                            initTableTransRental();
                            initTableTransRentalApproved();
                            swal('Success', 'Status Updated', 'success');
                        } else {
                            $('#viewEditStatus').modal('hide');
                            initTableTransRental();
                            initTableTransRentalApproved();
                            swal('Failed', 'Something Went Wrong', 'error');
                        }
                    },
                    error: function (data) {
                        //console.log(data.responeText);
                    }

                });
                App.unblockUI();
            }
            else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');

            }
        });
    }
    
    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransRentalRequest/AddTransRentalRequest';
        });
    }

    return {
        init: function () {
            add();
            initTableTransRental();
            initDetilTransactionRental();
            ubahstatus();
            edit();
            btnEditStatus();
            showModal();
            btnUpdateS();
            initTableTransRentalApproved();
            initDetilTransactionRentalApproved();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransRentalRequest.init();
    });
}