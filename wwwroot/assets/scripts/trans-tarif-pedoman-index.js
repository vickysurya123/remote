﻿var TarifPedomanIndex = function () {

    var initTable = function () {
        $('#table-pedoman').DataTable({
            "ajax":
            {
                "url": "/TarifPedoman/GetData",
                "type": "GET",
            },
            "columns": [
                {
                    "data": "ID",
                    "visible": false
                },
                {
                    "data": "BRANCH_ID",
                    "visible": false
                },
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "TIPE_TARIF",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_ACTIVE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "END_ACTIVE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-detail" class="btn btn-icon-only green" data-toggle="modal" href="#detailModal" title="Detail" ><i class="fa fa-eye"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                        if (full.TIPE_TARIF === 'TARIF') {
                            // aksi += '<a id="btn-njop" class="btn btn-icon-only yellow disabled" href="#" ><i class="fa fa-money"></i></a>';
                        } else {
                            aksi += '<a id="btn-njop" class="btn btn-icon-only yellow" href="/TarifPedoman/Njop?cabang=' + full.BRANCH_ID + '" title="NJOP" ><i class="fa fa-money"></i></a>';
                        }
                        var kd_cabang = $('#BRANCH_ID').val();
                        if (kd_cabang == 0 || kd_cabang == full.BRANCH_ID) {
                            aksi += '<a id="btn-ubah" class="btn btn-icon-only blue" href="/TarifPedoman/Edit/' + full.ID + '" title="Edit" ><i class="fa fa-edit"></i></a>';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,
            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, "All"]
            ],
            "pageLength": 10,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },
            "filter": true
        });
    }

    var initTableHistory = function () {
        $('#table-pedoman-history').DataTable({
            "ajax":
            {
                "url": "/TarifPedoman/GetDataHistory",
                "type": "GET",
            },
            "columns": [
                {
                    "data": "ID",
                    "visible": false
                },
                {
                    "data": "BRANCH_ID",
                    "visible": false
                },
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "TIPE_TARIF",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_ACTIVE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "END_ACTIVE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-detail" class="btn btn-icon-only green" data-toggle="modal" href="#detailModal" title="Detail" ><i class="fa fa-eye"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                        if (full.TIPE_TARIF === 'TARIF') {
                            // aksi += '<a id="btn-njop" class="btn btn-icon-only yellow disabled" href="#" ><i class="fa fa-money"></i></a>';
                        } else {
                            aksi += '<a id="btn-njop" class="btn btn-icon-only yellow" href="/TarifPedoman/Njop?cabang=' + full.BRANCH_ID + '" title="NJOP" ><i class="fa fa-money"></i></a>';
                        } 
                        var kd_cabang = $('#BRANCH_ID').val();
                        if (kd_cabang == 0 || kd_cabang == full.BRANCH_ID) {
                            aksi += '<a id="btn-ubah" class="btn btn-icon-only blue" href="/TarifPedoman/Edit/' + full.ID + '" title="Edit" ><i class="fa fa-edit"></i></a>';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,
            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, "All"]
            ],
            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var detailData = function () {

        $('#table-pedoman').on('click', 'tr #btn-detail', function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();
            var ID = data.ID
            $('#BE_NAME').val(data.BE_NAME)
            $('#BE_ID').val(data.BE_ID)
            $('#INPUT_TYPE').val(data.TIPE_TARIF);
            $('#PROFIT_CENTER_ID').val(data.PROFIT_CENTER_ID);
            $('#PROFIT_CENTER_NAME').val(data.PROFIT_CENTER_NAME);
            $('#START_ACTIVE_DATE').val(data.START_ACTIVE_DATE);
            $('#END_ACTIVE_DATE').val(data.END_ACTIVE_DATE);
            if (ID) {
                initEdit(ID);
            }
            
        });


        $('#table-pedoman-history').on('click', 'tr #btn-detail', function () {

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman-history').DataTable();
            var data = table.row(baris).data();
            var ID = data.ID
            $('#BE_NAME').val(data.BE_NAME)
            $('#BE_ID').val(data.BE_ID)
            $('#INPUT_TYPE').val(data.TIPE_TARIF);
            $('#PROFIT_CENTER_ID').val(data.PROFIT_CENTER_ID);
            $('#PROFIT_CENTER_NAME').val(data.PROFIT_CENTER_NAME);
            $('#START_ACTIVE_DATE').val(data.START_ACTIVE_DATE);
            $('#END_ACTIVE_DATE').val(data.END_ACTIVE_DATE);
            if (ID) {
                initEdit(ID);
            }

        });

        var initEdit = function (ID) {
            var req = $.ajax({
                contentType: "application/json",
                method: "GET",
                url: "/TarifPedoman/GetPedoman/" + ID,
                // data: param,
                timeout: 3000,
            });

            req.done(function (data) {
                //console.log(data)
                $('.pedomanid').val('');
                
                if (data.TIPE_TARIF === 'PERCENT') {
                    $('.input-value').show();
                    $('.input-presentase').show();
                    $('.input-tarif').hide();
                    $('#tabnjop').show();
                } else if (data.TIPE_TARIF === 'TARIF') {
                    $('.input-value').show();
                    $('.input-presentase').hide();
                    $('.input-tarif').show();
                    $('#tabnjop').hide();
                } else {
                    $('.input-value').hide();
                    $('#tabnjop').show();
                }

                if (data.TIPE_TARIF !== 'TARIF') {

                    if ($.fn.DataTable.isDataTable('#table-njop')) {
                        $('#table-njop').DataTable().destroy();
                    }

                    $('#table-njop').DataTable({

                        "ajax":
                        {
                            "url": "/TarifPedoman/GetDataActiveNJOP?be_id=" + data.BE_ID,
                            "type": "GET",
                        },
                        "columns": [
                            {
                                "data": "KEYWORD",
                                "class": "dt-body-center"
                            },
                            {
                                "data": "VALUE",
                                "class": "dt-body-right",
                                "render": function (data, type, full) {
                                    return convertToRupiah(data);
                                }
                            }
                        ],
                        "destroy": true,
                        "ordering": false,
                        "processing": true,
                        "serverSide": true,

                        "lengthMenu": [
                            [5, 10, 15, 20, -1],
                            [5, 10, 15, 20, "All"]
                        ],

                        "pageLength": 10,

                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                        },

                        "filter": true
                    });
                }

                if (data.tarif) {
                    data.tarif.forEach(function (t) {
                        var kode_batas = (t.batas != 0 ? t.kode + '_' + t.batas : t.kode);
                        $('#id' + kode_batas).val(t.id);
                        if (data.TIPE_TARIF === 'PERCENT') {
                            $('#' + kode_batas + 'p').val(t.percentage);
                        } else if (data.TIPE_TARIF === 'TARIF') {
                            $('#' + kode_batas).val(t.value);
                        }
                        
                    });
                }
            })
        }
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["ID"];
            $('#be_name').html(data["BE_NAME"]);

            $("#ID_ATTACHMENT").val(id_attachment);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TarifPedoman/GetDataAttachmentPedoman/" + id_attachment,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                            x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var idPedoman = $('#ID_ATTACHMENT').val();

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TarifPedoman/UploadFilesPedoman',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { idPedoman: idPedoman },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();
            var fileName = data['FILE_NAME'];
            var uriId = data['FILE_NAME'];
            console.log(data);
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: data['TARIF_LAHAN_ID']
                        },
                        method: "get",
                        url: "/TarifPedoman/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }
    return {
        init: function () {
            initTable();
            detailData();
            initTableHistory();
            initAttachment();
            deleteAttachment();
            fupload();
        }
    }
}();

function convertToRupiah(angka) {
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
    return 'Rp ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TarifPedomanIndex.init();
    });
}