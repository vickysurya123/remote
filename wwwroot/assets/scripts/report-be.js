﻿var ReportBE = function () {


    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    /*
    var select2BEID = function () {

        function formatRepo(repo) {
            if (repo.loading) return "Processing...";

            var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.BE_NAME + "</div>" +
                            "<div class='select2-result-repository__description'>KODE : " + repo.BE_ID + "</div>" +
                          "</div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            $("#BE_ID").val(repo.BE_ID);
            return repo.BE_MANE;
        }

        $("#BUSINESS_ENTITY").select2({
            width: "off",
            ajax: {
                url: "/ReportBE/getBEID",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    }

    var select2HARBOURCLASS = function () {

        function formatRepo(repo) {
            if (repo.loading) return "Processing...";

            var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.HARBOUR_CLASS + "</div>" +
                          "</div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            $("#HARBOUR_CLASS").val(repo.HARBOUR_CLASS);
            return repo.HARBOUR_CLASS;
        }

        $("#HARBOUR_CLASS").select2({
            width: "off",
            ajax: {
                url: "/ReportBE/getHARBOURClASS",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    } */

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            App.blockUI({ boxed: true });
            var req = null;
            //var txtTerminal = $('#txtTerminal').val();
            var txtKegiatan = $('#txtKegiatan').val();
            var txtPBMNama = $('#select2-txtPBM-container').text();
            var txtPBM = $('#txtPBM').val();
            var txtAsalPortNama = $('#txtAsalKapalNama').val();
            var txtAsalPort = $('#txtAsalKapal').val();
            var txtTujuanPortNama = $('#txtTujuanKapalNama').val();
            var txtTujuanPort = $('#txtTujuanKapal').val();
            var txtTanggal = $('#txtTanggal').val();
            var txtAwalKapalNama = $('#select2-txtAwalKapal-container').text();
            var txtAwalKapal = $('#txtAwalKapal').val();
            var txtTransKapalNama = $('#select2-txtTransKapal-container').text();
            var txtTransKapal = $('#txtTransKapal').val();
            var xTable = $('#table-permohonan').dataTable();
            var xGetData = xTable.fnGetData();
            var xDATA_PMH = new Array(xGetData.length);
            for (i = 0; i < xGetData.length; i++) {
                var xData = xTable.fnGetData(i);

                xDATA_PMH[i] = {
                    XPPKB1_NOMOR: txtAwalKapal,
                    XKD_PBM: txtPBM,
                    XTGL_PMH_WHM: txtTanggal,
                    XKD_PELANGGAN: null,
                    XTIPE_KEGIATAN: txtKegiatan,
                    XKETERANGAN: xData[8],
                    XPORT_ORIGIN: txtAsalPort,
                    XPORT_DESTINATION: txtTujuanPort,
                    XKD_KAPAL_FROM: txtAwalKapal,
                    XKD_KAPAL_TO: txtTransKapal,
                    XKD_DOKUMEN: null,
                    XTIPE_PENUMPUKAN: xData[0],
                    XGUDLAP_ID: xData[1],
                    XKD_BARANG: xData[2],
                    XKD_SATUAN: xData[3],
                    XSIFAT_BARANG: null,
                    XJENIS_KEMASAN: xData[4],
                    XJUMLAH_BARANG: xData[5],
                    XNAMA_KAPAL_FROM: txtAwalKapalNama,
                    XNAMA_KAPAL_TO: txtTransKapalNama,
                    XNAMA_PBM: txtPBMNama,
                    XNAMA_PORT_ORIGIN: txtAsalPortNama,
                    XNAMA_PORT_DESTINATION: txtTujuanPortNama,
                    XKD_ASAL: xData[6],
                    XKD_TUJUAN: xData[7],

                }
            }
            //console.log(xDATA_PMH);
            //console.log(txtAwalKapalNama);
            req = $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(xDATA_PMH),
                method: "POST",
                url: "/PermohonanMasuk/InsertPmhBongkar",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                console.log(data);
                if (data.sts === "S") {
                    clear();
                    swal('Berhasil', "Berhasil Insert Data Permohonan [" + data.msg + "]", 'success');
                } else {
                    swal('Gagal', data.msg, 'error');
                }
            });
        });
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-BE');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " MENU records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var CBBE = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportBE/GetDataBE",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose BUSINESS ENTITY --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBHARBOUR();
            }
        });

    }
    var CBHARBOUR = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportBE/GetDataHarbourClass",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose HARBOUR CLASS --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].HB_ID + "'>" + jsonList.data[i].HARBOUR_CLASS + "</option>";
                }
                $("#HARBOUR_CLASS").html('');
                $("#HARBOUR_CLASS").append(listItems);
                handleBootstrapSelect();
            }
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {
            var businessEntity = $('#BE_ID').val();
            var profitCenter = $('#PROFIT_CENTER').val();
            var serviceGroup = $('#SERVICE_GROUP').val();
            var status = $('#STATUS').val();

            if ($.fn.DataTable.isDataTable('#table-report-BE')) {
                $('#table-report-BE').DataTable().destroy();
            }

            $('#table-report-BE').DataTable({
                "dom": "Bfrtip",
                buttons: [
                {
                    className: 'btn sbold blue',
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        }
                    }
                }
                ],
                buttons: [
                {
                    className: 'btn sbold blue',
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        }
                    }
                }
                ],
                "ajax": {
                    "url": "/ReportBE/ShowAllTwo",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "BE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POSTAL_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_CITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_PROVINCE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "HARBOUR_CLASS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FAX",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "EMAIL",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VAL_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VAL_TO",
                        "class": "dt-body-center"
                    }


                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " MENU records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    var showFilter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-BE')) {
                $('#table-report-BE').DataTable().destroy();
            }
            $('#table-report-BE').dataTable({
                "ajax": {
                    "url": "/ReportBE/GetDataFilterNew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be = $('#BUSINESS_ENTITY').val();
                        data.harbour_class = $('#HARBOUR_CLASS').val();
                        console.log(data);
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "BE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POSTAL_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_CITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_PROVINCE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "HARBOUR_CLASS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PHONE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FAX",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "EMAIL",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VAL_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VAL_TO",
                        "class": "dt-body-center"
                    }
                ],
                "columnDefs": [{
                    "defaultContent": "0",
                    "targets": "_all"
                },
                { "width": "30px", "targets": "0" },
                { "width": "30px", "targets": "1" },
                { "width": "300px", "targets": "2" },
                { "width": "50px", "targets": "3" },
                { "width": "50px", "targets": "4" },
                { "width": "50px", "targets": "5" },
                { "width": "50px", "targets": "6" },
                { "width": "50px", "targets": "7" },
                { "width": "50px", "targets": "8" },
                { "width": "50px", "targets": "9" },
                { "width": "50px", "targets": "10" },
                { "width": "50px", "targets": "11" }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "serverSide": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " MENU records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    return {
        init: function () {
            //select2BEID();
            //select2HARBOURCLASS();
            dataTableResult();
            CBBE();
            //CBHARBOUR();
            showAll();
            showFilter();
            simpan();
            reset();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var HARBOUR_CLASS = $('#HARBOUR_CLASS').val();

    var dataJSON = "{BUSINESS_ENTITY:" + JSON.stringify(BUSINESS_ENTITY) + ",HARBOUR_CLASS:" + JSON.stringify(HARBOUR_CLASS) + "}";
    
    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        //url: "/ReportBE/defineExcel",
        url: "/ReportBE/defineExcelNew",
        data: dataJSON,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileNames:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportBE/deleteExcel",
        data: JSON.stringify(fileExcels),//jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportBE.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-BE > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}