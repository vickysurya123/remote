﻿var ReportTransOTherServices = function () {

    var dataTableResult = function () {
        var table = $('#table-report-trans-os');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var CBProfitCenter = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransElectricity/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].TERMINAL_NAME + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
            }
        });
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
            var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();

            if (POSTING_DATE_FROM && POSTING_DATE_TO) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                    $('#table-report-trans-os').DataTable().destroy();
                }
                $('#table-report-trans-os').DataTable({
                    "ajax": {
                        "url": "ReportTransElectricity/ShowFilterNew",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.profit_center = $('#PROFIT_CENTER').val();
                            data.billing_no = $('#BILLING_NO').val();
                            data.customer_id = $('#CUSTOMER_ID').val();
                            data.power_capacity = $('#POWER_CAPACITY').val();
                            data.period = $('#PERIOD').val();
                            data.posting_date_from = $('#POSTING_DATE_FROM').val();
                            data.posting_date_to = $('#POSTING_DATE_TO').val();
                            data.sap_document_number = $('#SAP_DOCUMENT_NUMBER').val();
                            data.condition_used = $('#CONDITION_USE').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak_exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "ID",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "STATUS_DINAS",
                        },
                        {
                            "data": "PROFIT_CENTER",
                        },
                        {
                            "data": "CUSTOMER_NAME"
                        },
                        {
                            "data": "INSTALLATION_CODE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "INSTALLATION_ADDRESS"
                        },
                        {
                            "data": "POWER_CAPACITY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "MINIMUM_USED",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "PERIOD",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "METER_FROM_TO_BLOK",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "USED_BLOK",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "METER_FROM_TO_LWBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "USED_LWBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "METER_FROM_TO_WBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "USED_WBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "METER_FROM_TO_KVARH",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "USED_KVARH",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "MULTIPLY_FACT",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TARIFF_BLOK",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TARIFF_BLOK2",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TARIFF_LWBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TARIFF_WBP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TARIFF_KVARH",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "EXPENSE_TARIFF",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "PPJU",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "REDUKSI",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "GRAND_TOTAL",
                            "class": "dt-body-right",
                            render: $.fn.dataTable.render.number('.', '.', 0)
                        },
                        {
                            "data": "POSTING_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SAP_DOCUMENT_NUMBER",
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {

                                return '<span class="label label-sm label-success">BILLED</span>';

                            },
                            "class": "dt-body-center"
                        },
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],
                    "pageLength": 10,
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },
                    "filter": false
                });
            }
            else {
                swal('Warning', 'Please Select Posting Date From and Posting Date To!', 'warning');
            }
        });
    }

    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportTransElectricity/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_NAME").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }
    //render: $.fn.dataTable.render.number('.', '.', 0)

    var showAll = function () {
        $('#btn-show-all').click(function () {

            if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                $('#table-report-trans-os').DataTable().destroy();
            }

            $('#table-report-trans-os').DataTable({
                "dom": "Bfrtip",
                buttons: [
                    {
                        className: 'btn sbold blue',
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                "ajax": {
                    "url": "/ReportTransElectricity/ShowAllTwo",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PROFIT_CENTER",
                    },
                    {
                        "data": "CUSTOMER_MDM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME"
                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINIMUM_USED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM_TO_BLOK",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "USED_BLOK",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM_TO_LWBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "USED_LWBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM_TO_WBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "USED_WBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM_TO_KVARH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "USED_KVARH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FACT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_BLOK",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_BLOK2",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_LWBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_WBP",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIFF_KVARH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "EXPENSE_TARIFF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "GRAND_TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "POSTING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SAP_DOCUMENT_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {

                            return '<span class="label label-sm label-success">BILLED</span>';

                        },
                        "class": "dt-body-center"
                    },
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    //var select2BusinessEntity = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.BE_NAME + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.BE_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#BE_ID").val(repo.BE_ID);
    //        return repo.BE_NAME;
    //    }

    //    $("#BUSINESS_ENTITY").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getBusinessEntity",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2ProfitCenter = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.TERMINAL_NAME + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.PROFIT_CENTER_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#PROFIT_CENTER_ID").val(repo.PROFIT_CENTER_ID);
    //        return repo.TERMINAL_NAME;
    //    }

    //    $("#PROFIT_CENTER").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getProfitCenter",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2ServiceGroup = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.REF_DESC + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.REF_DATA + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#SERVICE_GROUP_ID").val(repo.REF_DATA);
    //        return repo.REF_DESC;
    //    }

    //    $("#SERVICE_GROUP").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getServiceGroup",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2CustomerMDM = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.COSTUMER_MDM + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.COSTUMER_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#CUSTOMER_ID").val(repo.COSTUMER_ID);
    //        return repo.COSTUMER_MDM;
    //    }

    //    $("#CUSTOMER_MDM").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getCustomerMDM",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            showAll();
            filter();
            mdm();
            reset();
            CBProfitCenter();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var BILLING_NO = $('#BILLING_NO').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
    var POWER_CAPACITY = $('#POWER_CAPACITY').val();
    var PERIOD = $('#PERIOD').val();
    var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
    var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();
    var SAP_DOCUMENT_NUMBER = $('#SAP_DOCUMENT_NUMBER').val();
    var CONDITION_USED = $('#CONDITION_USE').val();

    var dataJSON = {
        PROFIT_CENTER: PROFIT_CENTER, BILLING_NO: BILLING_NO
        , CUSTOMER_ID: CUSTOMER_ID, POWER_CAPACITY: POWER_CAPACITY
        , PERIOD: PERIOD, POSTING_DATE_FROM: POSTING_DATE_FROM
        , POSTING_DATE_TO: POSTING_DATE_TO, SAP_DOCUMENT_NUMBER: SAP_DOCUMENT_NUMBER,
        CONDITION_USED: CONDITION_USED
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "ReportTransElectricity/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(jsonData));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportTransElectricity/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportTransOTherServices.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-trans-os > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportTransElectricity/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}