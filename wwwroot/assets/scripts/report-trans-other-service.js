﻿var ReportTransOTherServices = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransOtherService/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                comboServiceGroup();
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-trans-os');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    /*
    var filter = function () {
        $('#btn-filter').click(function () {
            var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
            var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();

            sGroup = $('#SERVICES_GROUP').val();
            var arraySGroup = sGroup.split("|");
            var valSGroup = arraySGroup[0];


            if (POSTING_DATE_FROM && POSTING_DATE_TO) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                    $('#table-report-trans-os').DataTable().destroy();
                }

                $('#table-report-trans-os').DataTable({
                    "ajax": {
                        "url": "/ReportTransOtherService/ShowFilterTwo",
                        "type": "GET",
                        "data": function (data) {
                            data.be_id = $('#BUSINESS_ENTITY').val();
                            data.profit_center = $('#PROFIT_CENTER').val();
                            data.billing_no = $('#BILLING_NO').val();
                            data.sap_no = $('#SAP_NO').val();
                            data.customer_id = $('#CUSTOMER_ID').val();
                            data.service_group = valSGroup;
                            data.posting_date_from = $('#POSTING_DATE_FROM').val();
                            data.posting_date_to = $('#POSTING_DATE_TO').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak_exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "BE_NAME",
                        },
                        {
                            "data": "PROFIT_CENTER_NAME",
                        },
                        {
                            "data": "ID",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "POSTING_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "COSTUMER_MDM_NAME",
                        },
                        {
                            "data": "CUSTOMER_SAP_AR",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SERVICES_GROUP_NAME"
                        },
                        {
                            "data": "SERVICE_NAME"
                        },
                        {
                            "data": "QUANTITY"
                        },
                        {
                            "data": "UNIT"
                        },
                        {
                            "render": function (data, type, full) {

                                return full.PRICE;

                            }
                            //"class": "dt-body-center"
                            //"data": "PRICE",
                            //"class": "dt-body-center",
                            //render: $.fn.dataTable.render.number('.', '.', 0)
                        },
                        {
                            "data": "CURRENCY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "MULTIPLY_FACTOR",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SURCHARGE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "AMOUNT",
                            "class": "dt-body-center",
                            render: $.fn.dataTable.render.number('.', '.', 0)
                        },
                        {
                            "data": "SAP_DOCUMENT_NUMBER",
                            "class": "dt-body-center"
                        },
                        //{
                        //    "data": "GL_ACCOUNT",
                        //    "class": "dt-body-center"
                        //},
                        {
                            "render": function (data, type, full) {

                                return '<span class="label label-sm label-success">BILLED</span>';

                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "columnDefs": [{
                        "width": "80px", "targets": "_all"
                    }],
                    "lengthMenu": [
                        [5, 10, 15, 20, 10000],
                        [5, 10, 15, 20, "All"]
                    ],
                    "scrollX": true,
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },

                    "filter": false,

                    "order": [
                        [10, "desc"]
                    ]
                });
            }
            else {
                swal('Warning', 'Please Select Posting Date From and Posting Date To!', 'warning');
            }

        });
    }
    */

    var filter = function () {
        $('#btn-filter').click(function () {

            var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
            var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();
            var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();

            sGroup = $('#SERVICES_GROUP').val();
            var arraySGroup = sGroup.split("|");
            var valSGroup = arraySGroup[0];

            if (POSTING_DATE_FROM && POSTING_DATE_TO && BUSINESS_ENTITY) {
                $('#row-table').css('display', 'block');

                if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                    $('#table-report-trans-os').DataTable().destroy();
                }

                $('#table-report-trans-os').DataTable({
                    "ajax": {
                        "url": "/ReportTransOtherService/GetDataFilterTwoNew",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.be_id = $('#BUSINESS_ENTITY').val();
                            data.profit_center = $('#PROFIT_CENTER').val();
                            data.billing_no = $('#BILLING_NO').val();
                            data.sap_no = $('#SAP_NO').val();
                            data.customer_id = $('#CUSTOMER_ID').val();
                            data.service_group = valSGroup;
                            data.posting_date_from = $('#POSTING_DATE_FROM').val();
                            data.posting_date_to = $('#POSTING_DATE_TO').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak_exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "BE_NAME",
                        },
                        {
                            "data": "PROFIT_CENTER_NAME",
                        },
                        {
                            "data": "ID",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "POSTING_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "COSTUMER_MDM_NAME",
                        },
                        {
                            "data": "CUSTOMER_SAP_AR",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SERVICES_GROUP_NAME"
                        },
                        {
                            "data": "SERVICE_NAME"
                        },
                        {
                            "data": "QUANTITY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "UNIT"
                        },
                        {
                            "render": function (data, type, full) {

                                return full.PRICE;

                            }
                            //"class": "dt-body-center"
                            //"data": "PRICE",
                            //"class": "dt-body-center",
                            //render: $.fn.dataTable.render.number('.', '.', 0)
                        },
                        {
                            "data": "CURRENCY",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "MULTIPLY_FACTOR",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "SURCHARGE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "AMOUNT",
                            "class": "dt-body-right",
                            render: $.fn.dataTable.render.number('.', '.', 0)
                        },
                        {
                            "data": "SAP_DOCUMENT_NUMBER",
                            "class": "dt-body-center"
                        },
                        //{
                        //    "data": "GL_ACCOUNT",
                        //    "class": "dt-body-center"
                        //},
                        {
                            "render": function (data, type, full) {

                                return '<span class="label label-sm label-success">BILLED</span>';

                            },
                            "class": "dt-body-center"
                        }
                    ],
                    "columnDefs": [{
                        "width": "80px", "targets": "_all"
                    }],
                    "lengthMenu": [
                        [5, 10, 15, 20, 10000],
                        [5, 10, 15, 20, "All"]
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },

                    "filter": false,

                    "order": [
                        [10, "desc"]
                    ]
                });
            }
            else {
                swal('Warning', 'Please Select Posting Date From and Posting Date To!', 'warning');
            }



        });
    }

    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/ReportTransOtherService/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {

            if ($.fn.DataTable.isDataTable('#table-report-trans-os')) {
                $('#table-report-trans-os').DataTable().destroy();
            }

            $('#table-report-trans-os').DataTable({
                "dom": "Bfrtip",
                buttons: [
                    {
                        className: 'btn sbold blue',
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                buttons: [
                    {
                        className: 'btn sbold blue',
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                "ajax": {
                    "url": "/ReportTransOtherService/ShowAllTwo",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "columns": [
                    {
                        "data": "BE_NAME",
                    },
                    {
                        "data": "PROFIT_CENTER_NAME",
                    },
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "POSTING_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "COSTUMER_MDM_NAME",
                    },
                    {
                        "data": "CUSTOMER_SAP_AR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SERVICES_GROUP_NAME"
                    },
                    {
                        "data": "SERVICE_NAME"
                    },
                    {
                        "data": "QUANTITY"
                    },
                    {
                        "data": "UNIT"
                    },
                    {
                        "data": "PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "SAP_DOCUMENT_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "GL_ACCOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {

                            return '<span class="label label-sm label-success">BILLED</span>';

                        },
                        "class": "dt-body-center"
                    }
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    //var select2BusinessEntity = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.BE_NAME + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.BE_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#BE_ID").val(repo.BE_ID);
    //        return repo.BE_NAME;
    //    }

    //    $("#BUSINESS_ENTITY").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getBusinessEntity",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2ProfitCenter = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.TERMINAL_NAME + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.PROFIT_CENTER_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#PROFIT_CENTER_ID").val(repo.PROFIT_CENTER_ID);
    //        return repo.TERMINAL_NAME;
    //    }

    //    $("#PROFIT_CENTER").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getProfitCenter",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2ServiceGroup = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.REF_DESC + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.REF_DATA + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#SERVICE_GROUP_ID").val(repo.REF_DATA);
    //        return repo.REF_DESC;
    //    }

    //    $("#SERVICE_GROUP").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getServiceGroup",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    //var select2CustomerMDM = function () {

    //    function formatRepo(repo) {
    //        if (repo.loading) return "Processing...";

    //        var markup = "<div class='select2-result-repository clearfix'>" +
    //                        "<div class='select2-result-repository__title'>" + repo.COSTUMER_MDM + "</div>" +
    //                        "<div class='select2-result-repository__description'>KODE : " + repo.COSTUMER_ID + "</div>" +
    //                      "</div>";

    //        return markup;
    //    }

    //    function formatRepoSelection(repo) {
    //        $("#CUSTOMER_ID").val(repo.COSTUMER_ID);
    //        return repo.COSTUMER_MDM;
    //    }

    //    $("#CUSTOMER_MDM").select2({
    //        width: "off",
    //        ajax: {
    //            url: "/ReportTransOtherService/getCustomerMDM",
    //            dataType: 'json',
    //            delay: 250,
    //            data: function (params) {
    //                return {
    //                    p: params.term,
    //                    page: params.page
    //                };
    //            },
    //            processResults: function (data, page) {
    //                return {
    //                    results: data.items
    //                };
    //            },
    //            cache: true
    //        },
    //        escapeMarkup: function (markup) { return markup; },
    //        minimumInputLength: 1,
    //        templateResult: formatRepo,
    //        templateSelection: formatRepoSelection
    //    });
    //}

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/TransVBList/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
                //$("#SERVICES_GROUP").trigger("chosen:updated");
                select2ProfitCenter();
                handleBootstrapSelect();
            }
        });
        //$('#SERVICES_GROUP').chosen({ width: "100%" });
    }

    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            showAll();
            filter();
            mdm();
            reset();
            CBBusinessEntity();
            //select2ProfitCenter();
            //CBProfitCenter();
            //comboServiceGroup();
        }
    };
}();

function CBProfitCenter(BE_ID) {
    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransOtherService/GetDataProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
            }
        });
    } else {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportTransOtherService/GetDataProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].NAMA_PROFIT_CENTER + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);
            }
        });
    }
}

var fileExcels;
function defineExcel() {
    sGroup = $('#SERVICES_GROUP').val();
    var arraySGroup = sGroup.split("|");
    var valSGroup = arraySGroup[0];

    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var BILLING_NO = $('#BILLING_NO').val();
    var SAP_NO = $('#SAP_NO').val();
    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
    var SERVICE_GROUP_ID = valSGroup;
    var POSTING_DATE_FROM = $('#POSTING_DATE_FROM').val();
    var POSTING_DATE_TO = $('#POSTING_DATE_TO').val();

    var dataJSON = {
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        PROFIT_CENTER: PROFIT_CENTER,
        BILLING_NO: BILLING_NO,
        SAP_NO: SAP_NO,
        CUSTOMER_ID: CUSTOMER_ID,
        SERVICE_GROUP_ID: valSGroup,
        POSTING_DATE_FROM: POSTING_DATE_FROM,
        POSTING_DATE_TO: POSTING_DATE_TO
    };

    console.log(dataJSON);

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "ReportTransOtherService/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(jsonData));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportTransOtherService/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportTransOTherServices.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-trans-os > tbody').html('');
    //$('#SERVICES_GROUP', this).chosen();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportTransOtherService/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}