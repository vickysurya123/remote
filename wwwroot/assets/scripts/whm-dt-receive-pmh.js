var TableDatatablesEditable = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
           var aData = oTable.fnGetData(nRow);
            //var jqTds = $('>td', nRow);
            for (var i = 0, iLen = 23; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control nama-gudlap"></select>';
            jqTds[1].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control nama-barang"></select>';
            jqTds[2].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control select-kemasan"><option value="XXXXX">General Cargo</option></select>';
            jqTds[3].innerHTML = '<input type="number" class="form-control" value="' + aData[12] + '">';
            jqTds[4].innerHTML = '<input type="number" class="form-control" value="' + aData[13] + '">';
            jqTds[5].innerHTML = '<input type="number" class="form-control" value="' + aData[14] + '">';
            jqTds[6].innerHTML = '<input type="number" class="form-control" value="' + aData[15] + '">';
            jqTds[7].innerHTML = '<input type="number" class="form-control" value="' + aData[16] + '">';
            jqTds[8].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';
            $(".select-kemasan").select2();
            getGudLap(aData[4]);
            selectingGudLap();
            getBarang(aData[9]);
            selectingBarang();
        }
        
        function saveRow(oTable, nRow) {
            
            var kodeGudang = $('.select-gudang').val();
            var namaGudang = $('.select-gudang').text();

            var kodeBarang = $('.select-barang').val();
            var namaBarang = $('.select-barang').text();

            var kodeKemasan = $('.select-kemasan').val();
            var namaKemasan = $('.select-kemasan').text();

            var jqInputs = $('input', nRow);
            oTable.fnUpdate(namaGudang, nRow, 4, false);
            oTable.fnUpdate(namaBarang, nRow, 9, false);
            oTable.fnUpdate(namaKemasan, nRow, 11, false);
            oTable.fnUpdate(jqInputs[0].value, nRow, 12, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 13, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 14, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 15, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 16, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a> <a class="delete" href="">Delete</a>', nRow, 22, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 12, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 13, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 14, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 15, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 16, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 22, false);
            oTable.fnDraw();
        }

        var table = $('#dt-receive-pmh');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1, 2, 3, 5, 6, 7, 8, 10, 17, 18, 19, 20, 21],
                    "visible": false,
                },
                {
                    "targets": [4, 9],
                    "width"  : "15%"
                }
            ]
        });

        var tableWrapper = $("#dt-receive-pmh_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#dt-receive-pmh_new').click(function (e) {
            e.preventDefault();
            
            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = false; // tambahan
            }
        });
        
    }

    var getGudLap = function (xxx) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.nama_gudlap + " (" + repo.id + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.nama_gudlap || repo.text;
        }

        $(".nama-gudlap").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/Receiving/GetGudLap",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: xxx };
                callback(data);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 5,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "ketikkan nama atau kode gudang"
        });
    }

    var selectingGudLap = function () {
        $(".nama-gudlap").on("select2:select", function (e) {
            var kodeGudLap = e.params.data.id;
            var namaGudlap = e.params.data.nama_gudlap;
            var nRow = $(this).parents('tr')[0];
            var aData = $('#dt-receive-pmh').dataTable().fnGetData(nRow);
            $('#dt-receive-pmh').dataTable().fnUpdate(kodeGudLap, nRow, 3, false);
            $('#dt-receive-pmh').dataTable().fnUpdate(namaGudlap, nRow, 4, false);
        });
    }

    var getBarang = function (xxx) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.nama_barang + " (" + repo.id + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.nama_barang || repo.text;
        }

        $(".nama-barang").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/Receiving/GetBarang",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: xxx };
                callback(data);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "ketikkan nama barang"
        });
    }

    var selectingBarang = function () {
        $(".nama-barang").on("select2:select", function (e) {
            var kodeBarang = e.params.data.id;
            var namaBarang = e.params.data.nama_barang;
            var nRow = $(this).parents('tr')[0];
            $('#dt-receive-pmh').dataTable().fnUpdate(kodeBarang, nRow, 8, false);
            $('#dt-receive-pmh').dataTable().fnUpdate(namaBarang, nRow, 9, false);
            $('#dt-receive-pmh').dataTable().fnDraw();
            var aData = $('#dt-receive-pmh').dataTable().fnGetData(nRow);
        });
    }

    return {

        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable.init();
});