﻿var DataSurat = function () {
    $('#isi_surat').summernote({
        placeholder: '',
        tabsize: 2,
        height: 420,
        focus: true,                  // set focus to editable area after initializing summernote  

    });

    $('#isi_surat').summernote('disable');
    return {
        init: function () {
        }
    };
}();
var PendingList = function () {
    var initTableContractOffer = function () {
        $('#table-pending-list').DataTable({

            "ajax":
            {
                "url": "PendingList/GetDataHeaderContractOffer",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE",
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {
                        if (full.OFFER_STATUS == '10') {
                            return '<span class="label label-sm label-primary">10 - Created</span>';
                        }
                        else {
                            return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.TOP_APPROVED_LEVEL + ' </span>';
                        
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE == '1') {
                            //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                            var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                            if (full.APV_DISPOSISI == 1) {
                                aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Approve Contract Offer" id="btn-form-approve"><i class="fa fa-check"></i></a>';
                            }
                            aksi += '<a data-toggle="modal" href="#viewHistoryApproval" class="btn btn-icon-only green" data-toggle="tooltip" title="View Detail Contract Offer" id="btn-history"><i class="fa fa-eye"></i></a>';

                        }
                        else {
                            var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                            aksi += '<button class="btn btn-icon-only blue" id="btn-ubah" disabled><i class="fa fa-edit"></i></button>';
                            aksi += '<button class="btn btn-icon-only blue" id="btn-form-approve" disabled><i class="fa fa-check"></i></button>';
                            aksi += '<a data-toggle="modal" href="#viewHistoryApproval" class="btn btn-icon-only green" id="btn-history"><i class="fa fa-eye"></i></a>';
                        }
                        return aksi;

                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    //var initHistoryApproval = function () {
    //    $('body').on('click', 'tr #btn-history', function () {
    //        var baris = $(this).parents('tr')[0];
    //        var table = $('#table-pending-list').DataTable();
    //        var data = table.row(baris).data();

    //        var id = data["CONTRACT_OFFER_NO"];
    //        $("#PROFIT_CENTER_NAME").val(data["PROFIT_CENTER_NAME"]);
    //        $("#BE_ID").val(data["BE_ID"]);
    //        $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
    //        $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
    //        $("#BE_ID").val(data["BE_ID"]);
    //        $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
    //        $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
    //        $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
    //        $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
    //        $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
    //        $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
    //        $("#TOP_APPROVED_LEVEL").val(data["TOP_APPROVED_LEVEL"]);
    //        $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

    //        console.log(id);
    //        $('#table-history').DataTable({

    //            "ajax":
    //            {
    //                "url": "/PendingList/GetDataHistoryWorkflow/" + id,
    //                "type": "GET",

    //            },

    //            "columns": [
    //                {
    //                    "data": "USER_LOGIN",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "CREATION_DATE",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "NOTES",
    //                    "class": "dt-body-center"
    //                },
    //                {
    //                    "data": "STATUS_CONTRACT",
    //                    "class": "dt-body-center"
    //                },
    //            ],
    //            "destroy": true,
    //            "ordering": false,
    //            "processing": true,
    //            "serverSide": true,

    //            "lengthMenu": [
    //                [5, 10, 15, 20, -1],
    //                [5, 10, 15, 20, "All"]
    //            ],

    //            "pageLength": 10,

    //            "language": {
    //                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
    //            },

    //            "filter": false
    //        });

    //    });
    //}

    var initHistoryApproval = function () {
        $('body').on('click', 'tr #btn-history', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pending-list').DataTable();
            var data = table.row(baris).data();
            //var baris = $(this).parents('tr')[0];            

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory2 = $('#table-history2').DataTable();
            tableHistory2.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#PROFIT_CENTER_NAME").val(data["PROFIT_CENTER_NAME"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#TOP_APPROVED_LEVEL").val(data["TOP_APPROVED_LEVEL"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            $("#isi_surat").summernote('code', "");
            $('#dataHistory').empty();
            //console.log(id_special);
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            req.done(function (data) {
                App.unblockUI();
                var isiSurat = "";
                /*if (data.dataSurat != null) {
                    isiSurat = data.dataSurat.ISI_SURAT;
                    $('.upload-iframe').empty();
                    var tpl = $('#pdf-upload-iframe');

                    var template = tpl.html().replace(/__INDEX__/g, data.dataSurat.ID);
                    $('.upload-iframe').append(template);
                }*/
                //console.log(data);
                $("#isi_surat").summernote('code', isiSurat);
                data.history.forEach(myFunction);
                //var tableHistory = $('#idHistory > tbody');
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }
                $('#dataHistory').append(isiTable);
            });

            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/' + data + '" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //console.log(id_special);
            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/PendingList/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });


            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/PendingList/GetDataDetailObject/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_tanah = full.LUAS_TANAH;
                            var fLuas_tanah = luas_tanah.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_tanah;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_bangunan = full.LUAS_BANGUNAN;
                            var fLuas_bangunan = luas_bangunan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_bangunan;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/PendingList/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            //$('#btn-view-condition').click(function () {
            //    if ($.fn.DataTable.isDataTable('#table-detil-condition')) {
            //        $('#table-detil-condition').DataTable().destroy();
            //    }
            //});



            //$('#table-detil-condition').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "CALC_OBJECT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CONDITION_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "VALID_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MONTHS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATISTIC",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT_PRICE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "AMT_REF",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FREQUENCY",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "START_DUE_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MANUAL_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "FORMULA",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MEASUREMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "LUAS",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NJOP_PERCENT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_RULE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TOTAL_NET_VALUE",
            //            "class": "dt-body-center"
            //        },


            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/PendingList/GetDataDetailManualy/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/PendingList/GetDataDetailMemo/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //$('#table-history2').DataTable({

            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "USER_LOGIN",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CREATION_DATE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NOTES",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "STATUS_CONTRACT_DESC",
            //            "class": "dt-body-center"
            //        },

            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/PendingList/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Reporting Rule
            //$('#reporting-rule').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailReportingRule/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "REPORTING_RULE_NO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TERM_OF_REPORTING_RULE",
            //            "class": "dt-body-center"
            //        }
            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

        });
    }
    
    var formapprove = function () {
        $('body').on('click', 'tr #btn-form-approve', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pending-list').DataTable();
            var data = table.row(baris).data();

            window.location = "/PendingList/ApprovePage/" + data["CONTRACT_OFFER_NO"];
            
        });
    }

    var initTableObject = function () {

        var uriId = $('#UriId').val();

        $('#rental-object-co').DataTable({
            "ajax":
            {
                "url": "/PendingList/GetDataObject/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },

            },

            "columns": [
                {
                    "data": "OBJECT_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "OBJECT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "OBJECT_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS_TANAH",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS_BANGUNAN",
                    "class": "dt-body-center"
                },
                {
                    "data": "INDUSTRY",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableCondition = function () {

        var uriId = $('#UriId').val();

        $('#condition').DataTable({
            "ajax": {
                "url": "/PendingList/GetDataCondition/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },
            },
            "columns": [
                {
                    "data": "CALC_OBJECT",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "VALID_FROM",
                    "class": "dt-body-center"
                },
                {
                    "data": "VALID_TO",
                    "class": "dt-body-center"
                },
                {
                    "data": "MONTHS",
                    "class": "dt-body-center"
                },
                {
                    "data": "STATISTIC",
                    "class": "dt-body-center"
                },
                {
                    "data": "UNIT_PRICE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "AMT_REF",
                    "class": "dt-body-center"
                },
                {
                    "data": "FREQUENCY",
                    "class": "dt-body-center"
                },
                {
                    "data": "START_DUE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "MANUAL_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "FORMULA",
                    "class": "dt-body-center"
                },
                {
                    "data": "MEASUREMENT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "LUAS",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "NJOP_PERCENT",
                    "class": "dt-body-center"
                },
                {
                    "data": "KONDISI_TEKNIS_PERCENT",
                    "class": "dt-body-center"
                },
                {
                    "data": "SALES_RULE",
                    "class": "dt-body-center"
                },
                {
                    "data": "TOTAL_NET_VALUE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                }
            ],

            "ordering": false,
            "processing": false,
            "serverSide": true,


            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableManual = function () {

        var uriId = $('#UriId').val();

        $('#manual-frequency').DataTable({

            "ajax":
            {
                "url": "/TransContractOffer/GetDataDetailManualy/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },

            },

            "columns": [
                {
                    "data": "MANUAL_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION",
                    "class": "dt-body-center"
                },
                {
                    "data": "DUE_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "NET_VALUE",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "QUANTITY",
                    "class": "dt-body-center",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "UNIT",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableMemo = function () {

        var uriId = $('#UriId').val();

        $('#table-memo').DataTable({
            "ajax":
            {
                "url": "/TransContractOffer/GetDataDetailMemo/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },

            },

            "columns": [
                {
                    "data": "CALC_OBJECT",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONDITION_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "MEMO",
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/PendingList';
        });
    }

    //var setApproval = function (contractOfferNo, approvalLevel, approvalNotes, approvalType) {
    //    var param = {
    //        CONTRACT_OFFER_NO: contractOfferNo,
    //        LEVEL_NO: approvalLevel,
    //        APPROVAL_TYPE: approvalType,
    //        APPROVAL_NOTE: approvalNotes
    //    }
    //    $.ajax({
    //        contentType: "application/json",
    //        data: JSON.stringify(param),
    //        method: "POST",
    //        url: "/PendingList/SetApproveWorkflow",
    //        timeout: 30000,
    //        success: function (data) {
    //            if (data.status == "S") {
    //                return 'S';
    //            } else {
    //                return 'E';
    //            }
    //        },
    //        error: function (data) {
    //           return 'E';
    //        }
    //    });
    //    //return "E";
    //};

    var approve = function () {
        $('#btn-approve').click(function () {
            swal({
                title: 'Warning',
                text: "Are you sure want to <B>APPROVE</B> this contract offer?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var contractOfferNo = $('#CONTRACT_OFFER_NO').val();
                    var approvalLevel = $('#property_role').val();
                    var approvalType = "2";
                    var approvalNotes = $('#NOTES').val();
                    var disposisiParaf = $('#DISPOSISI_PARAF').val();

                    var param = {
                        CONTRACT_OFFER_NO: contractOfferNo,
                        LEVEL_NO: approvalLevel,
                        APPROVAL_TYPE: approvalType,
                        APPROVAL_NOTE: approvalNotes,
                        DISPOSISI_PARAF: disposisiParaf,
                    }

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        dataType: "json",
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/PendingList/SetApproveWorkflownew",
                        //url: "/PendingList/SetApproveWorkflow",
                        //timeout: 30000,
                        async:true,

                        success: function (data) {
                            console.log(data);
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal('Success', 'You have approved Contract Offer : ' + contractOfferNo + '', 'success').then(function (isConfirm) {
                                    window.location = "/PendingList";
                                });
                            }
                            else {
                                swal('Failed', 'Sorry, This approval fail.', 'error').then(function (isConfirm) {
                                });
                            }
                        },
                        error: function (request, status, error) {
                            App.unblockUI();
                            console.log(request, status, error);
                        }
                    });
                }
            });
        });
    }

    var reject = function () {
        $('#btn-reject').click(function () {
            var approvalNotes = $('#NOTES').val();
            if (approvalNotes) {
                swal({
                    title: 'Warning',
                    text: "Are you sure want to <B>REJECT</B> this contract offer ?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var contractOfferNo = $('#CONTRACT_OFFER_NO').val();
                        var approvalLevel = $('#property_role').val();
                        var approvalType = "4";
                        var approvalNotes = $('#NOTES').val();

                        var param = {
                            CONTRACT_OFFER_NO: contractOfferNo,
                            LEVEL_NO: approvalLevel,
                            APPROVAL_TYPE: approvalType,
                            APPROVAL_NOTE: approvalNotes
                        }

                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            //url: "/PendingList/SetApproveWorkflow",
                            url: "/PendingList/SetApproveWorkflownew",
                            timeout: 30000,

                            success: function (data) {
                                App.unblockUI();
                                if (data.status === 'S') {
                                    swal('Success', 'You have rejected Contract Offer : ' + contractOfferNo + '', 'success').then(function (isConfirm) {
                                        window.location = "/PendingList";
                                    });
                                }
                                else {
                                    swal('Failed', 'Sorry, This approval fail.', 'error').then(function (isConfirm) {
                                    });
                                }
                            }
                        });
                    }
                });
            }
            else {
                swal('Info', 'Please Fill Notes!', 'info');
            }
        });
    }

    var revise = function () {
        $('#btn-revise').click(function () {
            var approvalNotes = $('#NOTES').val();
            if (approvalNotes) {
                swal({
                    title: 'Warning',
                    text: "Are you sure want to <B>REVISE</B> this contract offer ?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var contractOfferNo = $('#CONTRACT_OFFER_NO').val();
                        var approvalLevel = $('#property_role').val();
                        var approvalType = "3";
                        var approvalNotes = $('#NOTES').val();
                        var disposisiParaf = $('#DISPOSISI_PARAF').val();

                        var param = {
                            CONTRACT_OFFER_NO: contractOfferNo,
                            LEVEL_NO: approvalLevel,
                            APPROVAL_TYPE: approvalType,
                            APPROVAL_NOTE: approvalNotes,
                            DISPOSISI_PARAF: disposisiParaf,
                        }

                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            //url: "/PendingList/SetApproveWorkflow",
                            url: "/PendingList/SetApproveWorkflownew",
                            timeout: 30000,

                            success: function (data) {
                                App.unblockUI();
                                if (data.status === 'S') {
                                    swal('Success', 'You have revised Contract Offer : ' + contractOfferNo + '', 'success').then(function (isConfirm) {
                                        window.location = "/PendingList";
                                    });
                                }
                                else {
                                    swal('Failed', 'Sorry, This approval fail.', 'error').then(function (isConfirm) {
                                    });
                                }
                            }
                        });
                    }
                });
            }
            else {
                swal('Info', 'Please Fill Notes!', 'info');
            }
        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pending-list').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["CONTRACT_OFFER_NO"];
            $('#kode_offer').html(id_attachment);

            $("#ID_ATTACHMENT").val(data["CONTRACT_OFFER_NO"]);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/PendingList/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },

                },

                "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();
            //console.log(BE_ID);

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {

                    console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/PendingList/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { BE_ID: BE_ID },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();
            var uriId = $('#ID_ATTACHMENT').val();

            var fileName = data['FILE_NAME'];
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/PendingList/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }
    
    return {
        init: function () {
            initTableContractOffer();
            batal();
            formapprove();
            approve();
            reject();
            revise();
            initHistoryApproval();
            initTableObject();
            initTableCondition();
            initTableManual();
            initTableMemo();
            initAttachment();
            fupload();
            deleteAttachment();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        PendingList.init();
        DataSurat.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg|zip)$");
        if (!(regex.test(val))) {
            $(this).val('');
            $('#viewModalAttachment').modal('hide');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, JPG, and ZIP Extension Are Allowed', 'warning');
        }
    });
});