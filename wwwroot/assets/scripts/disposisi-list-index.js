﻿var DataSurat = function () {
    $('#isi_surat').summernote({
        placeholder: '',
        tabsize: 2,
        height: 120,
        focus: true,                  // set focus to editable area after initializing summernote  
        
    });

    $('#isi_surat').summernote('disable');
    return {
        init: function () {
        }
    };
}();
var DisposisiListIndex = function () {
    var initTable = function () {
        $('#table-disposisi').DataTable({
            "ajax":
            {
                "url": "/DisposisiList/GetDataDisposisi",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS_APV == 1) {
                            return full.NO_SURAT;
                        } else {
                            return '-';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "SUBJECT",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center",
                },
                {
                    "data": "OFFER_TYPE",
                    "class": "dt-body-left",
                },
                {
                    "data": "CUSTOMER_NAME",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center",
                },
                //{
                //    "data": "IS_CREATE_SURAT",
                //    "class": "dt-body-right",
                //},
                {
                    "render": function (data, type, full) {
                        //console.log(full.EDIT);
                        var aksi = '';
                        if (full.IS_CREATE_SURAT == 1 && full.EDIT == 1 && full.IS_ACTION == 0 ) {
                            aksi += '<a id="btn-surat" class="btn btn-icon-only purple" title="Create Surat" ><i class="fa fa-envelope-o"></i></a>';
                        } else if (full.IS_ACTION_DISPOSISI == 0 && full.IS_CREATE_SURAT == 0) {
                            aksi += '<a id="btn-disposisi" class="btn btn-icon-only red" title="Disposisi" ><i class="fa fa-share-square"></i></a>';
                        } else if (full.STATUS_KIRIM == "Draft" && full.ROLE_ID == 0 && full.IS_CREATE_SURAT == 1 && full.EDIT == 0) {
                            aksi += '<a id="btn-edit-surat" class="btn btn-icon-only purple" title="Edit Surat" ><i class="fa fa-envelope-o"></i></a>';
                        }
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="View" class="btn btn-icon-only green" id="btn-detail-disposisi"><i class="fa fa-eye"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }



    $('body').on('click', 'tr #btn-disposisi', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-disposisi').DataTable();
        var data = table.row(baris).data();

        //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

        window.location = "/DisposisiList/AddDisposisi/" + data["encodedId"];
    });

    $('body').on('click', 'tr #btn-surat', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-disposisi').DataTable();
        var data = table.row(baris).data();

        //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

        window.location = "/DisposisiList/AddSuratDisposisi/" + data["encodedId"];
    });

    $('body').on('click', 'tr #btn-edit-surat', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-disposisi').DataTable();
        var data = table.row(baris).data();

        //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

        window.location = "/DisposisiList/EditSuratDisposisi/" + data["encodedId"];
    });

    return {
        init: function () {
            initTable();

        }
    };
}();

var ParafListIndex = function () {
    var initTable = function () {
        $('#table-paraf').DataTable({
            "ajax":
            {
                "url": "/DisposisiList/GetDataParaf",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS_APV == 1) {
                            return full.NO_SURAT;
                        } else {
                            return '-';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "SUBJECT",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center",
                },
                {
                    "data": "OFFER_TYPE",
                    "class": "dt-body-left",
                },
                {
                    "data": "CUSTOMER_NAME",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center",
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center",
                },
                //{
                //    "data": "IS_CREATE_SURAT",
                //    "class": "dt-body-right",
                //},
                {
                    "render": function (data, type, full) {
                        var aksi = '';
                        if (full.IS_ACTION == 0 && full.URUTAN == full.PARAF_LEVEL && full.DISPOSISI_PARAF == 'paraf') {
                            aksi += '<a id="btn-paraf" class="btn btn-icon-only red" title="Paraf" ><i class="fa fa-check-square"></i></a>';
                        } else if (full.DISPOSISI_MAX > 0 && full.URUTAN == full.APV_LEVEL && full.IS_ACTION == 0 && full.STATUS_APV == 0) {
                            aksi += '<a id="btn-paraf-disposisi" class="btn btn-icon-only red" title="Paraf" ><i class="fa fa-check-square"></i></a>';
                        }
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="View" class="btn btn-icon-only green" id="btn-detail-paraf"><i class="fa fa-eye"></i></a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    $('body').on('click', 'tr #btn-paraf', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-paraf').DataTable();
        var data = table.row(baris).data();

        window.location = "/DisposisiList/Paraf/" + data["encodedId"];
    });

    $('body').on('click', 'tr #btn-paraf-disposisi', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-paraf').DataTable();
        var data = table.row(baris).data();

        window.location = "/DisposisiList/ParafDisposisi/" + data["encodedId"];
    });

    return {
        init: function () {
            initTable();

        }
    };
}();
var TransContractOffer = function () {

    var showOldContract = function (ro, cond) {
        ////console.log(ro, cond)
        var all_value = 0;
        $('.DETAIL_RO').html('');
        for (var i = 0; i < cond.length; i++) {

            var net_val = cond[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var price = cond[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = cond[i].CALC_OBJECT;

            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_cond = calc_ob.split(' ')[1];

                for (var j = 0; j < ro.length; j++) {
                    ro_data = ro[j];
                    ////console.log(ro_data);
                    var ro_id = ro_data.OBJECT_ID;
                    if (ro_id == ro_cond) {
                        var luas = Number(ro_data.LAND_DIMENSION) + Number(ro_data.BUILDING_DIMENSION);
                        var text = $('.DETAIL_RO').html() + '<tr><td>' + ro_data.OBJECT_ID + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO').html(text);
                    }
                }
            }
        }
        $('#CONTRACT_VALUE').val(all_value);
        $('.CONTRACT_VALUE').html(sep1000(all_value, true));
    }
    window.xx;
    var initDetilTransContractOfferParaf = function () {

        $('body').on('click', 'tr #btn-detail-paraf', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-paraf').DataTable();
            window.xx = table;
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            var id_special = data["CONTRACT_NO"];
            var req = $.ajax({
                contentType: "application/json",
                data: "CONTRACT_NO=" + id_special,
                method: "get",
                url: "/DisposisiList/GetContract",
                timeout: 30000,
                async: false,
            });
            req.done(function (data) {
                App.unblockUI();
                //if (data.status === 'S') {
                    $("#CONTRACT_OFFER_NO").val(data.CONTRACT_OFFER_NO);
                    $("#CONTRACT_OFFER_TYPE").val(data.CONTRACT_OFFER_TYPE);
                    $("#BE_ID").val(data.BE_ID);
                    $("#CONTRACT_OFFER_NAME").val(data.CONTRACT_OFFER_NAME);
                    $("#CONTRACT_START_DATE").val(data.CONTRACT_START_DATE);
                    $("#CONTRACT_END_DATE").val(data.CONTRACT_END_DATE);
                    $("#TERM_IN_MONTHS").val(data.TERM_IN_MONTHS);
                    $("#CUSTOMER_NAME").val(data.CUSTOMER_NAME);
                    $("#CUSTOMER_AR").val(data.CUSTOMER_AR);
                    $("#PROFIT_CENTER").val(data.PROFIT_CENTER_NAME);
                    $("#CONTRACT_USAGE").val(data.CONTRACT_USAGE);
                    $("#CURRENCY").val(data.CURRENCY);
                    $("#CURRENCY_RATE").val(data.CURRENCY_RATE);
                    $("#OFFER_STATUS").val(data.OFFER_STATUS);
                //}
            });

            var id_surat = data["ID"];

            $('.upload-iframe').empty();
            var tpl = $('#tmpl-upload-iframe');
            var template = tpl.html().replace(/__INDEX__/g, id_surat);
            $('.upload-iframe').append(template);

            $("#isi_surat").summernote('code', "");
            $('#dataHistory').empty();
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_surat,
                method: "get",
                url: "/DisposisiList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            req.done(function (data) {
                App.unblockUI();
                $("#isi_surat").summernote('code', data.dataSurat.ISI_SURAT);
                data.history.forEach(myFunction);
                //var tableHistory = $('#idHistory > tbody');
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }
                $('#dataHistory').append(isiTable);
            });

            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_tanah = full.LUAS_TANAH;
                            var fLuas_tanah = luas_tanah.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_tanah;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_bangunan = full.LUAS_BANGUNAN;
                            var fLuas_bangunan = luas_bangunan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_bangunan;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });


            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            //console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            ////console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrCond = data.data;
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrRo = data.data;
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    var initDetilTransContractOfferDisposisi = function () {
        $('body').on('click', 'tr #btn-detail-disposisi', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-disposisi').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSalesBased = $('#sales-based').DataTable();
            tableSalesBased.destroy();

            var id_special = data["CONTRACT_NO"];
            var req = $.ajax({
                contentType: "application/json",
                data: "CONTRACT_NO=" + id_special,
                method: "get",
                url: "/DisposisiList/GetContract",
                timeout: 30000,
                async: false,
            });
            req.done(function (data) {
                App.unblockUI();
                //if (data.status === 'S') {
                $("#CONTRACT_OFFER_NO").val(data.CONTRACT_OFFER_NO);
                $("#CONTRACT_OFFER_TYPE").val(data.CONTRACT_OFFER_TYPE);
                $("#BE_ID").val(data.BE_ID);
                $("#CONTRACT_OFFER_NAME").val(data.CONTRACT_OFFER_NAME);
                $("#CONTRACT_START_DATE").val(data.CONTRACT_START_DATE);
                $("#CONTRACT_END_DATE").val(data.CONTRACT_END_DATE);
                $("#TERM_IN_MONTHS").val(data.TERM_IN_MONTHS);
                $("#CUSTOMER_NAME").val(data.CUSTOMER_NAME);
                $("#CUSTOMER_AR").val(data.CUSTOMER_AR);
                $("#PROFIT_CENTER").val(data.PROFIT_CENTER_NAME);
                $("#CONTRACT_USAGE").val(data.CONTRACT_USAGE);
                $("#CURRENCY").val(data.CURRENCY);
                $("#CURRENCY_RATE").val(data.CURRENCY_RATE);
                $("#OFFER_STATUS").val(data.OFFER_STATUS);
                //}
            });
            var id_surat = data["ID"];

            $('.upload-iframe').empty();
            var tpl = $('#tmpl-upload-iframe');
            var template = tpl.html().replace(/__INDEX__/g, id_surat);
            $('.upload-iframe').append(template);

            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_surat,
                method: "get",
                url: "/DisposisiList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            req.done(function (data) {
                App.unblockUI();
                $("#isi_surat").summernote('code', data.dataSurat.ISI_SURAT);
                data.history.forEach(myFunction);
                //var tableHistory = $('#idHistory > tbody');
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>"; 
                        if (item.TYPE == "Disposisi") {
                            isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                        }
                        isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }
                $('#dataHistory').append(isiTable);
            });

            ////console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*"data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }

                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });


            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            //console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrCond = data.data;
                        ////console.log(arrCond);
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrRo = data.data;
                        ////console.log(arrRo);
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    return {
        init: function () {
            ;
            initDetilTransContractOfferParaf();
            initDetilTransContractOfferDisposisi();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        DisposisiListIndex.init();
        ParafListIndex.init();
        DataSurat.init();
        TransContractOffer.init();
    });
}