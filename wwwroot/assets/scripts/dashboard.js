﻿var d1;
var PieSelected;
var PieROSelected;
var StatROSelected;
var SertifikatSelected;
var Bar4Selected;
var CabangSelected;
var zone_rip = '-';
var term_in_months = '-';
var contract_no = '-';
var kode_aset = '-';
var Dashboard = function () {
    var Chart1 = function () {
        var param = {
            BE_ID: $('#BE_NAME').val(),
            start: $('#START_DATE').val(),
            end: $('#END_DATE').val()
        };
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetData",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.Status === 'S') {
                Pie1(data.Data1, data.Data2, data.Data3);
                Pie2(data.Data4, data.Data5, data.Data6);
            } else {

            }
        });

    }
    var Pie1 = function (data1, data2, data3) {
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart1", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.data = [{
            "jenis": "Kontrak Aktif",
            "jumlah": data1,
            "lain": 555,
        }, {
            "jenis": "Kontrak yang akan Habis",
            "jumlah": data2,
            "lain": 111,
        }, {
            "jenis": "Kontrak Habis",
            "jumlah": data3,
            "lain": 333,
        }];

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "jumlah";
        series.dataFields.category = "jenis";
        series.colors.list = [
            am4core.color("#00e676"),
            am4core.color("#ffea00"),
            am4core.color("#ff1744"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        //series.labels.template.radius = am4core.percent(20);
        //series.labels.template.fill = am4core.color("black");

        //series.labels.template.disabled = true;
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var type = ev.target.dataItem.properties.category;
            handleClick(type);
        });

    }
    var Pie2 = function (data4, data5, data6) {
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart2", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = [{
            "jenis": "Kontrak Diperpanjang",
            "jumlah": data4,
        }, {
            "jenis": "Kontrak Tidak Diperpanjang",
            "jumlah": data5,
        }, {
            "jenis": "Kontrak Baru",
            "jumlah": data6,
        }];

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "jumlah";
        series.dataFields.category = "jenis";
        series.dataFields.balloonText = 
        series.colors.list = [
            am4core.color("#00e676"),
            am4core.color("#ff1744"),
            am4core.color("#2979ff"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        //series.labels.template.text = "{jumlah}";
        //series.labels.template.radius = am4core.percent(80);
        //series.labels.template.fill = am4core.color("black");

        //series.labels.template.disabled = true;
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var type = ev.target.dataItem.properties.category;
            handleClick(type);
        });
    }
    function handleClick(type) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel').show('slow');
        $('#text_tool').text(type);

        $('#table_data').DataTable().destroy();
        $('#table_data tbody').html('');
        var temp = '';
        var jenis = 0;
        if (type == 'Kontrak Aktif') {
            jenis = 1
        } else if (type == 'Kontrak yang akan Habis') {
            jenis = 2
        } else if (type == 'Kontrak Habis') {
            jenis = 3
        } else if (type == 'Kontrak Diperpanjang') {
            jenis = 4
        } else if (type == 'Kontrak Tidak Diperpanjang') {
            jenis = 5
        } else if (type == 'Kontrak Baru') {
            jenis = 6
        }

        PieSelected = jenis;

        var param = {
            jenis: jenis.toString(),
            BE_ID: $('#BE_NAME').val(),
            start: $('#START_DATE').val(),
            end: $('#END_DATE').val()
        };
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetail",
            timeout: 30000
        });
        req.done(function (data) {
            var jsonList = data;
            for (var i = 0; i < jsonList.Positionlist.length; i++) {
                //console.log(jsonList.Positionlist[i].CONTRACT_NO_NEW);
                if (jsonList.Positionlist[i].CONTRACT_NO_NEW == jsonList.Positionlist[i].CONTRACT_NO) {
                    temp += '<tr>' +
                        '<td>' + jsonList.Positionlist[i].BE_NAME + '</td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_NO + '</td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_START_DATE + '</td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_END_DATE + '</td>' +
                        '<td>' + jsonList.Positionlist[i].BUSINESS_PARTNER + ' - ' + jsonList.Positionlist[i].BUSINESS_PARTNER_NAME + '</td>' +
                        '<td>' + jsonList.Positionlist[i].MPLG_BADAN_USAHA + '</td>' +
                        '<td style="float:right">' + jsonList.Positionlist[i].TOTAL_NET_VALUE + '</td>' +
                        '<td> Luas Tanah :' + jsonList.Positionlist[i].LUAS_TANAH + ' <br>Luas Bangunan : ' + jsonList.Positionlist[i].LUAS_BANGUNAN + ' </td>' +
                        '<td>' + jsonList.Positionlist[i].STATUS + '</td>'
                    '</tr>';
                } else {
                    temp += '<tr>' +
                        '<td>' + jsonList.Positionlist[i].BE_NAME + '</td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_NO + ' <span style="font-size:9px">(' + jsonList.Positionlist[i].CONTRACT_NO_NEW + ')</p></td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_START_DATE + '</td>' +
                        '<td>' + jsonList.Positionlist[i].CONTRACT_END_DATE + '</td>' +
                        '<td>' + jsonList.Positionlist[i].BUSINESS_PARTNER + ' - ' + jsonList.Positionlist[i].BUSINESS_PARTNER_NAME + '</td>' +
                        '<td>' + jsonList.Positionlist[i].MPLG_BADAN_USAHA + '</td>' +
                        '<td style="float:right">' + jsonList.Positionlist[i].TOTAL_NET_VALUE + '</td>' +
                        '<td> Luas Tanah :' + jsonList.Positionlist[i].LUAS_TANAH + ' <br>Luas Bangunan : ' + jsonList.Positionlist[i].LUAS_BANGUNAN + ' </td>' +
                        '<td>' + jsonList.Positionlist[i].STATUS + '</td>'
                    '</tr>';
                }

            }
            $('#table_data tbody').append(temp);
            $('#table_data').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data').removeClass('hidden');
            $('#text_tool').removeClass('hidden');
            $('#text_tool').text(type)
            App.unblockUI();
        });
    }
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var datePicker2 = function () {
        if (jQuery().datepicker) {
            $('.date-pickerr').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "yyyy-mm-dd"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    $('#btn-clear').click(function () {
        $('.date-picker').val('');
    });
    $('#btn-filter').click(function () {
        App.blockUI({ boxed: true });
        $('#view_data').addClass('hidden');
        $('#chart1').html('');
        $('#chart2').html('');
        Chart1();
    });
    var BarRkap = function () {
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart3", am4charts.XYChart);

        // Add data
        var contohChar = "";
        chart.data = [{
            "cabang": "Cabang Perak",
            "income": 23.5,
            "expenses": 18.1
        }, {
            "cabang": "Cabang Banjarmasin",
            "income": 26.2,
            "expenses": 22.8
        }, {
            "cabang": "Cabang Benoa",
            "income": 30.1,
            "expenses": 23.9
        }, {
            "cabang": "Cabang Bima",
            "income": 29.5,
            "expenses": 25.1
        }, {
            "cabang": "Cabang Celukan Bawang",
            "income": 24.6,
            "expenses": 25
        }];

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "cabang";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = false;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.opposite = false;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "cabang";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueY}";
            //valueLabel.label.horizontalCenter = "left";
            valueLabel.label.verticalCenter = "left";
            valueLabel.label.dy = -20;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            categoryLabel.label.text = "{name}";
            categoryLabel.label.verticalCenter = "left";
            //categoryLabel.label.horizontalCenter = "left";
            categoryLabel.label.dy = 10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
        }

        createSeries("income", "RKAP");
        createSeries("expenses", "Realisasi");

    }
    var Chart4 = function () {
        var data = {
            BRANCH_ID: $('#BE_NAME_4').val(),
            start: $('#POSTING_DATE_START').val(),
            end: $('#POSTING_DATE_END').val(),
        };
        console.log(data);
        var req = $.ajax({
            //contentType: "application/json",
            dataType: "json",
            data: data,
            type: "POST",
            url: "/Home/GetPiutang",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.Status === 'S') {
                ChartPiutang(data.Data);
            } else {

            }
        });

    }
    var ChartPiutang = function (data) {
        am4core.useTheme(am4themes_animated);
        var chart = am4core.create("chart4", am4charts.XYChart);
        // Add data
        var contohChar = "";
        chart.data = data;
        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "BE_NAME";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = false;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = 280;
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.opposite = false;
        valueAxis.max = 1000;
        // Create series
        function createSeries(field, text, name, color) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "BE_NAME";
            series.dataFields.categoryY = "BRANCH_ID";
            series.dataFields.categoryZ = text;
            series.columns.template.fill = am4core.color(color);
            series.columns.template.stroke = am4core.color(color);
            series.name = name;
            series.text = "PROPERTY_TEXT";
            series.columns.template.tooltipText = "{name}: [bold]{categoryZ}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;
            series.columns.template.events.on("hit", function (ev) {
                var cabang = ev.target.dataItem.categoryY;
                var cabang_name = ev.target.dataItem.categoryX;
                var jenis = name;
                PiutangTable(cabang, jenis, cabang_name);
            });
            //series.rotation = 45;

        }

        createSeries("PROPERTY", "PROPERTY_TEXT", "Properti", "#CFF27E");
        createSeries("WATER_ELECTRICITY", "WATER_ELECTRICITY_TEXT", "Water and Electricity", "#E5B25D");
        createSeries("OTHER_SERVICE", "OTHER_SERVICE_TEXT", "Other Service", "#523A34");

    }
    function PiutangTable(cabang, type, cabang_name) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel_4').show('slow');
        $('#text_tool_4').text(type);
        $('#table_data_4').DataTable().destroy();
        $('#table_data_4 tbody').html('');
        var jenis = 0;
        var code_ = "";
        var temp = "";
        if (type == 'Properti') {
            jenis = 1;
            code_ = "NO KONTRAK";
        } else if (type == 'Water and Electricity') {
            jenis = 2;
            code_ = "NO INSTALASI";
        } else if (type == 'Other Service') {
            jenis = 3;
            code_ = "KELOMPOK PENDAPATAN";
        }
        var param = {
            jenis: jenis.toString(),
            BE_ID: cabang,
            end: $('#POSTING_DATE').val()
        };
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailPiutang",
            timeout: 30000
        });
        req.done(function (data) {
            //console.log(data);
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                temp += '<tr>' +
                    '<td>' + jsonList.Data[i].BE_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].JENIS_TAGIHAN + '</td>' +
                    '<td>' + jsonList.Data[i].BILLING_NO + '</td>' +
                    '<td>' + jsonList.Data[i].CODE + '</td>' +
                    '<td>' + jsonList.Data[i].CUSTOMER_CODE + '</td>' +
                    '<td>' + jsonList.Data[i].CUSTOMER_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].PSTNG_DATE + '</td>' +
                    '<td>' + jsonList.Data[i].AMOUNT + '</td>' +
                    '</tr>';
            }
            $('#table_data_4 tbody').append(temp);
            $('#table_data_4').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_4').removeClass('hidden');
            $('#text_tool_4').removeClass('hidden');
            $('#code_').text(code_);
            $('#text_tool_4').text(cabang_name + " - " + type);
            CabangSelected = cabang;
            Bar4Selected = jenis;
            App.unblockUI();
        });
    }
    $('#btn-filter_4').click(function () {
        $('#view_data_4').addClass('hidden');
        $('#chart4').html('');
        Chart4();
    });
    function sekarang() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;
        $('#AVL_DATE').val(today);

    }
    var Chart5 = function () {
        var param = {
            BE_ID: $('#BE_NAME_5').val(),
            end: $('#AVL_DATE').val()
        };
        console.log(param);
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetRO",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.Status === 'S') {
                Pie5(data.Data);
            } else {

            }
        });

    }
    var Pie5 = function (data) {
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart5", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        d1 = marker;
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.data = data;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "JUMLAH";
        series.dataFields.category = "STATUS";
        series.colors.list = [
            am4core.color("#2979ff"),
            am4core.color("#00e676"),
            am4core.color("#ff1744"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        //series.labels.template.text = "{jumlah}";
        //series.labels.template.radius = am4core.percent(20);
        //series.labels.template.fill = am4core.color("black");

        //series.labels.template.disabled = true;
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var type = ev.target.dataItem.properties.category;
            $('#view_data_5').addClass('hidden');
            $('#text_tool_5').addClass('hidden');
            Chart6(type);
        });

    }
    $('#btn-filter_5').click(function () {
        $('#view_data_5').addClass('hidden');
        $('#chart5').html('');
        $('#chart6').html('');
        Chart5();
    });
    $('#btn-clear_5').click(function () {
        $('#chart6').html('');
        App.blockUI({ boxed: true });
        sekarang();
        Chart5();
    });
    var Chart6 = function (type) {
        App.blockUI({ boxed: true });
        if (type != 'VACANT') {
            var param = {
                jenis: type,
                BE_ID: $('#BE_NAME_5').val(),
                end: $('#AVL_DATE').val()
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Home/GetRODetail",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                if (data.Status === 'S') {
                    Pie6(data.Data, type);
                    ROTable('', type);
                } else {

                }
            });
        } else {
            $('#chart6').html('');
            ROTable('', type);
        }
    }
    var Pie6 = function (data, type) {
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart6", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        d1 = marker;
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.data = data;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "JUMLAH";
        series.dataFields.category = "MPLG_BADAN_USAHA";
        series.colors.list = [
            am4core.color("#3E3A39"),
            am4core.color("#EA5504"),
            am4core.color("#2A9D8F"),
            am4core.color("#E9C46A"),
            am4core.color("#F4A261"),
            am4core.color("#E0FBFC"),
            am4core.color("#C2DFE3"),
            am4core.color("#9DB4C0"),
            am4core.color("#596475"),
            am4core.color("#253237"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        //series.labels.template.text = "{jumlah}";
        //series.labels.template.radius = am4core.percent(20);
        //series.labels.template.fill = am4core.color("black");

        //series.labels.template.disabled = true;
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var badan_usaha = ev.target.dataItem.properties.category;
            ROTable(badan_usaha, type);
        });

    }

    //bersertifikat//

    var Chart7 = function () {
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE').val()
        };
        //console.log(param);
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetPenggunaanLahan",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.Status === 'S') {
                Pie7(data.dataPusatBersertifikat, data.CustomerBersertifikat, data.IddleBersertifikatkontrak,
                    data.IddleBersertifikatnokontrak, data.LahanPusatBersertifikat, data.LahanCustomerBersertifikat,
                    data.LahanIddleBersertifikatKontrak, data.LahanIddleBersertifikatNoKontrak);
                Pie8(data.dataPusat, data.Customer, data.Iddlekontrak, data.Iddlenokontrak, data.LahanPusat, data.LahanCustomer,
                     data.LahanIddleKontrak, data.LahanIddlenoKontrak);
                Pie9(data.dataPusatBersertifikat, data.CustomerBersertifikat, data.IddleBersertifikatkontrak,
                    data.IddleBersertifikatnokontrak, data.LahanPusatBersertifikat, data.LahanCustomerBersertifikat,
                    data.LahanIddleBersertifikatKontrak, data.LahanIddleBersertifikatNoKontrak,
                    data.dataPusat, data.Customer, data.Iddlekontrak, data.Iddlenokontrak, data.LahanPusat, data.LahanCustomer,
                    data.LahanIddleKontrak, data.LahanIddlenoKontrak);
            } else {

            }
        });

    }


    //bersertifikat//
    var Pie7 = function (datapusatbersertifikat, customerbersertifikat, iddlebersertifikatkontrak,
        iddlebersertifikatnokontrak, lahanpusatbersertifikat, lahancustomerbersertifikat,
        lahaniddlebersertifikatkontrak, lahaniddlebersertifikatnokontrak) {
        //console.log(lahanpusatbersertifikat);
        //am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart7", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        d1 = marker;
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        chart.data = [{
            "jenis": "Digunakan oleh Pelindo III",
            "jumlah": datapusatbersertifikat,
            "total_lahan": lahanpusatbersertifikat + 'm²',
        }, {
            "jenis": "Pengguna Jasa",
            "jumlah": customerbersertifikat,
                "total_lahan": lahancustomerbersertifikat + 'm²',
        }, {
            "jenis": "Idle",
            "jumlah": iddlebersertifikatkontrak,
                "total_lahan": lahaniddlebersertifikatkontrak + 'm²',
        }, {
            "jenis": "Dalam Proses",
            "jumlah": iddlebersertifikatnokontrak,
                "total_lahan": lahaniddlebersertifikatnokontrak + 'm²',
        }];

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "total_lahan";
        series.dataFields.category = "jenis";
        //series.dataFields.total_lahan = "total_lahan";
        series.colors.list = [
            am4core.color("#277DA1"),
            am4core.color("#90BE6D"),
            am4core.color("#F94144"),
            am4core.color("#FFD700"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {

            var type = ev.target.dataItem.properties.category;
            var category;
            //console.log(type);
            if (type == 'Idle') {
                category = 'IddleBersertifikatkontrak'
            } else if (type == 'Dalam Proses') {
                category = 'IddleBersertifikatnokontrak'
            } else if (type == 'Pengguna Jasa') {
                category = 'CustomerBersertifikat'
            } else {
                category = 'KantorPusatBersertifikat'
            }
            //console.log(category);

            if (category == 'CustomerBersertifikat') {
                TblSertifikatCust2(category);
            } else {
                TblSertifikatPel_Iddle2(category);
            }
        });
    }

    var Pie8 = function (datapusat, customer, iddlekontrak, iddlenokontrak, lahanpusat, lahancustomer, lahaniddlekontrak,
                         lahaniddlenokontrak) {
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart8", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        d1 = marker;
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        chart.data = [{
            "jenis": "Digunakan oleh Pelindo III",
            "jumlah": datapusat,
            "total_lahan": lahanpusat + 'm²',
        }, {
            "jenis": "Pengguna Jasa",
            "jumlah": customer,
            "total_lahan" : lahancustomer + 'm²',
        }, {
            "jenis": "Idle",
            "jumlah": iddlekontrak,
            "total_lahan" : lahaniddlekontrak+ 'm²', 
        }, {
            "jenis": "Dalam Proses",
            "jumlah": iddlenokontrak,
            "total_lahan" : lahaniddlenokontrak+ 'm²', 
        }];

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "total_lahan";
        series.dataFields.category = "jenis";

        // Disable pulling out
        //series.slices.template.states.getKey("active").properties.shiftRadius = 0;

        // Set up slices
        /*series.slices.template.tooltipText = "";
        series.slices.template.alwaysShowTooltip = true;*/

        // Add slice click event
        /*var currentSlice;
        series.slices.template.events.on("hit", function (ev) {
            if (currentSlice) {
                currentSlice.tooltip.hide();
            }
            currentSlice = ev.target.dataItem.properties.category;
            console.log(currentSlice);
            currentSlice.tooltipText = "{category}:  ({value.value})";
            currentSlice.showTooltip();
        });*/

        // Set up page click event to close open tooltip
        /*am4core.getInteraction().body.events.on("hit", function (ev) {
            if (currentSlice) {
                currentSlice.tooltip.hide();
            }
        });*/

       /* series.dataFields.total_lahan = "total_lahan";*/
        series.colors.list = [
            am4core.color("#277DA1"),
            am4core.color("#90BE6D"),
            am4core.color("#F94144"),
            am4core.color("#FFD700"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var type = ev.target.dataItem.properties.category;
            var category;
            //console.log(type);
            if (type == 'Idle') {
                category = 'Iddlekontrak'
            } else if (type == 'Dalam Proses') {
                category = 'Iddlenokontrak'
            }
            else if (type == 'Pengguna Jasa') {
                category = 'Customer'
            }else {
                category = 'KantorPusat'
            }
            //console.log(category);

            if (category == 'Customer') {
                TblSertifikatCust(category);
                //$('#view_data_7').hide();
            } else {
                TblSertifikatPel_Iddle(category);
                //$('#view_data_6').hide();
            }
               
        });
         

    }

    var Pie9 = function (datapusat, customer, iddlekontrak, iddlenokontrak, lahanpusat, lahancustomer, lahaniddlekontrak,
        lahaniddlenokontrak, datapusatbersertifikat, customerbersertifikat, iddlebersertifikatkontrak,
        iddlebersertifikatnokontrak, lahanpusatbersertifikat, lahancustomerbersertifikat,
        lahaniddlebersertifikatkontrak, lahaniddlebersertifikatnokontrak) {
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart9", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        var marker = chart.legend.markers.template.children.getIndex(0);
        d1 = marker;
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();
        chart.data = [{
            "jenis": "Digunakan oleh Pelindo III",
            "jumlah": datapusat + datapusatbersertifikat,
            "total_lahan": lahanpusat + lahanpusatbersertifikat + 'm²',
        }, {
                "jenis": "Pengguna Jasa",
                "jumlah": customer + customerbersertifikat,
                "total_lahan": lahancustomer + lahancustomerbersertifikat + 'm²',
        }, {
                "jenis": "Idle",
                "jumlah": iddlekontrak + iddlebersertifikatkontrak,
                "total_lahan": lahaniddlekontrak + lahaniddlebersertifikatkontrak + 'm²',
        }, {
                "jenis": "Dalam Proses",
                "jumlah": iddlenokontrak + iddlebersertifikatnokontrak,
                "total_lahan": lahaniddlenokontrak + lahaniddlebersertifikatnokontrak + 'm²',
        }];

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "total_lahan";
        series.dataFields.category = "jenis";
        series.colors.list = [
            am4core.color("#277DA1"),
            am4core.color("#90BE6D"),
            am4core.color("#F94144"),
            am4core.color("#FFD700"),
        ];
        ////text inside slice
        series.alignLabels = false;
        series.labels.template.text = "";
        series.ticks.template.disabled = true;

        series.slices.template.events.on("hit", function (ev) {
            var type = ev.target.dataItem.properties.category;
            var category;
            //console.log(type);
            if (type == 'Idle') {
                category = 'Iddley'
            } else if (type == 'Dalam Proses') {
                category = 'Iddlex'
            }
            else if (type == 'Pengguna Jasa') {
                category = 'CustBersertifikatTidak'
            } else {
                category = 'PstBersertifikatTidak'
            }
            //console.log(category);

            if (category == 'CustBersertifikatTidak') {
                TblSertifikatCust3(category);
            } else {
                TblSertifikatPel_Iddle3(category);
            }
        });

    }


    $('#btn-filter-lahan').click(function () {
        App.blockUI({ boxed: true });
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        grafikLahan();
    });
    $('#btn-clear_lahan').click(function () {
        App.blockUI({ boxed: true });
        $('#chart7').html('');
        $('#chart8').html('');
        $('#chart9').html('');
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#AVL_DATE_LAHAN').val('');
        $('#BE_NAME_LAHAN').val('');
        Chart7();
    });

    function grafikLahan() {
        var data = {
            BRANCH_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val()
        };
        //console.log(param);
        var req = $.ajax({
            url: "/Home/GetDetailLahan",
            method: "POST",
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json"
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.Status === 'S') {
                Pie7(data.dataPusatBersertifikat, data.CustomerBersertifikat, data.IddleBersertifikatkontrak,
                    data.IddleBersertifikatnokontrak, data.LahanPusatBersertifikat, data.LahanCustomerBersertifikat,
                    data.LahanIddleBersertifikatKontrak, data.LahanIddleBersertifikatNoKontrak);
                Pie8(data.dataPusat, data.Customer, data.Iddlekontrak, data.Iddlenokontrak, data.LahanPusat, data.LahanCustomer,
                    data.LahanIddleKontrak, data.LahanIddlenoKontrak);
                Pie9(data.dataPusatBersertifikat, data.CustomerBersertifikat, data.IddleBersertifikatkontrak,
                    data.IddleBersertifikatnokontrak, data.LahanPusatBersertifikat, data.LahanCustomerBersertifikat,
                    data.LahanIddleBersertifikatKontrak, data.LahanIddleBersertifikatNoKontrak,
                    data.dataPusat, data.Customer, data.Iddlekontrak, data.Iddlenokontrak, data.LahanPusat, data.LahanCustomer,
                    data.LahanIddleKontrak, data.LahanIddlenoKontrak);
            } else {
            }
        });
    }

    function ROTable(badan_usaha, type) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel_5').show('slow');
        $('#text_tool_5').text(type);
        $('#table_data_5').DataTable().destroy();
        $('#table_data_5 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            jenis: type,
            jenis2: badan_usaha,
            BE_ID: $('#BE_NAME_5').val(),
            end: $('#AVL_DATE').val()
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailRO",
            timeout: 30000
        });
        req.done(function (data) {
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                temp += '<tr>' +
                    '<td>' + jsonList.Data[i].BE_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].PROFIT_CENTER + '</td>' +
                    '<td>' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + jsonList.Data[i].RO_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].ZONE_RIP + '</td>' +
                    '<td>' + jsonList.Data[i].LAND_DIMENSION + '</td>' +
                    '<td>' + jsonList.Data[i].BUILDING_DIMENSION + '</td>' +
                    '<td>' + (jsonList.Data[i].CONTRACT_NO == null ? "-" : jsonList.Data[i].CONTRACT_NO) + '</td>' +
                    '<td>' + (jsonList.Data[i].CONTRACT_OFFER_NO == null ? "-" : jsonList.Data[i].CONTRACT_OFFER_NO) + '</td>' +
                    '<td>' + (jsonList.Data[i].MPLG_KODE == null && jsonList.Data[i].MPLG_NAMA == null ? " - " : jsonList.Data[i].MPLG_KODE + " - " + jsonList.Data[i].MPLG_NAMA) + '</td>' +
                    '<td>' + (jsonList.Data[i].MPLG_BADAN_USAHA == null ? " - " : jsonList.Data[i].MPLG_BADAN_USAHA) + '</td>' +
                    '<td>' + jsonList.Data[i].RO_CERTIFICATE_NUMBER + '</td>' +
                    '<td>' + (jsonList.Data[i].SERTIFIKAT_TYPE == null ? " - " : jsonList.Data[i].SERTIFIKAT_TYPE) + '</td>' +
                    '<td>' + jsonList.Data[i].USAGE_TYPE + '</td>' +
                    '<td>' + jsonList.Data[i].FUNCTION_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].STATUS + '</td>' +
                    '<td>' + (jsonList.Data[i].REASON == null ? "-" : jsonList.Data[i].REASON) + '</td>' +
                    '<td>' + jsonList.Data[i].VALID_FROM + '</td>' +
                    '<td>' + jsonList.Data[i].VALID_TO + '</td>' +
                    '<td>' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td>' + jsonList.Data[i].RO_POSTALCODE + '</td>' +
                    '<td>' + jsonList.Data[i].RO_CITY + '</td>' +
                    '<td>' + jsonList.Data[i].PROVINCE + '</td>' +
                    '</tr>';
            }
            $('#table_data_5 tbody').append(temp);
            $('#table_data_5').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_5').removeClass('hidden');
            $('#text_tool_5').removeClass('hidden');
            var text = (badan_usaha == "" || badan_usaha == null ? "Status : " + type : "Badan Usaha : " + badan_usaha + ", Status : " + type);
            $('#text_tool_5').text(text);
            PieROSelected = badan_usaha;
            StatROSelected = type;
            App.unblockUI();
        });
    }


    //belum bersertifikat//
    function TblSertifikatCust(category) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel_6').show('slow');
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#text_tool_6').text('Pengguna Jasa Belum Bersertifikat');
        $('#table_data_6').DataTable().destroy();
        $('#table_data_6 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                
                temp += '<tr>' +
                    '<td class="dt-body-center">' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + (jsonList.Data[i].BUSINESS_PARTNER_NAME == null ? " - " : jsonList.Data[i].BUSINESS_PARTNER_NAME) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + 'm²' + '</td>' +
                    '<td class="dt-body-center">' + '-' + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].ZONE_RIP == null ? " - " : jsonList.Data[i].ZONE_RIP) + '</td>' +
                    '<td class="dt-body-center">' + '<a href="/ContractChange/ViewContract/' + jsonList.Data[i].CONTRACT_NO + '"target="_blank">' + (jsonList.Data[i].CONTRACT_NO == null ? " - " : jsonList.Data[i].CONTRACT_NO) + '</a>' + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].TERM_IN_MONTHS == null ? " - " : jsonList.Data[i].TERM_IN_MONTHS) + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].CONTRACT_START_DATE == null ? " - " : jsonList.Data[i].CONTRACT_START_DATE) + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].CONTRACT_END_DATE == null ? " - " : jsonList.Data[i].CONTRACT_END_DATE) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].TERM + '</td>' +
                    '</tr>';
            }

            $('#table_data_6 tbody').append(temp);
            $('#table_data_6').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            var a = $('#view_data_6').removeClass('hidden');
            if (a) {
                $('#view_data_7').hide();
                $('#view_data_7').addClass('hidden');
                $('#view_data_6').show();
            }
            $('#text_tool_6').removeClass('hidden');
           // var text = (badan_usaha == "" || badan_usaha == null ? "Status : " + type : "Badan Usaha : " + badan_usaha + ", Status : " + type);
            //$('#text_tool_5').text(text);
            //PieROSelected = badan_usaha;
            SertifikatSelected = category;
            App.unblockUI();
        });
    }

    //belum bersertifikat//
    function TblSertifikatPel_Iddle(category) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel_7').show('slow');
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#table_data_7').DataTable().destroy();
        $('#table_data_7 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                temp += '<tr>' +
                    '<td>' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + jsonList.Data[i].RO_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + 'm²' +'</td>' +
                    '<td class="dt-body-center">' + '-' + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>'+ 
                '</tr>';
            }
            $('#table_data_7 tbody').append(temp);
            $('#table_data_7').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            var a = $('#view_data_7').removeClass('hidden');
            if (a) {
                $('#view_data_6').hide();
                $('#view_data_6').addClass('hidden');
                $('#view_data_7').show();
            }
            SertifikatSelected = category;
            if (category == "KantorPusat") {
                category = "Digunakan Oleh Pelindo III Belum Bersertifikat"
            } else if (category == "Iddlekontrak") {
                category = " Iddle dengan Kontrak (Belum Bersertifikat)"
            } else {
                category = "Iddle tidak dengan Kontrak (Belum Bersertifikat)"
            }
            $('#text_tool_7').text(category);
            
            App.unblockUI();
        });
    }


    //bersertifikat//
    function TblSertifikatCust2(category) {
        App.blockUI({ boxed: true });
        $('#btn-cetak_exel_6').show('slow');
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#text_tool_6').text('Pengguna Jasa Bersertifikat');
        $('#table_data_6').DataTable().destroy();
        $('#table_data_6 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat2",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                temp += '<tr>' +
                    '<td class="dt-body-center">' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + (jsonList.Data[i].BUSINESS_PARTNER_NAME == null ? " - " : jsonList.Data[i].BUSINESS_PARTNER_NAME) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + 'm²' + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CERTIFICATE_NUMBER + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].ZONE_RIP + '</td>' +
                    '<td class="dt-body-center">' + '<a href="/ContractChange/ViewContract/' + jsonList.Data[i].CONTRACT_NO + '"target="_blank">' + (jsonList.Data[i].CONTRACT_NO == null ? " - " : jsonList.Data[i].CONTRACT_NO) + '</a>' + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].TERM_IN_MONTHS == null ? " - " : jsonList.Data[i].TERM_IN_MONTHS) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].CONTRACT_START_DATE + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].CONTRACT_END_DATE + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].TERM + '</td>' +
                '</tr>';
            }
            $('#table_data_6 tbody').append(temp);
            $('#table_data_6').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_6').removeClass('hidden');
            $('#view_data_6').show();
            $('#view_data_7').hide();
            $('#view_data_7').addClass('hidden');
            $('#text_tool_6').removeClass('hidden');
            // var text = (badan_usaha == "" || badan_usaha == null ? "Status : " + type : "Badan Usaha : " + badan_usaha + ", Status : " + type);
            //$('#text_tool_5').text(text);
            //PieROSelected = badan_usaha;
            SertifikatSelected = category;
            App.unblockUI();
        });
    }

    //bersertifikat//
    function TblSertifikatPel_Iddle2(category) {
        App.blockUI({ boxed: true });
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        var v_text = "";
        console.log(category);
        $('#table_data_7').DataTable().destroy();
        $('#table_data_7 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat2",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                temp += '<tr>' +
                    '<td class="dt-body-center">' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + jsonList.Data[i].RO_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + 'm²' + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CERTIFICATE_NUMBER + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>' +
                '</tr>';
            }
            $('#table_data_7 tbody').append(temp);
            $('#table_data_7').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_7').removeClass('hidden');
            $('#view_data_7').show();
            $('#view_data_6').hide();
            $('#view_data_6').addClass('hidden');
            $('#btn-cetak_exel_7').show('slow');
            SertifikatSelected = category;
            if (category == "KantorPusatBersertifikat") {
                category = "Digunakan Oleh Pelindo III Bersertifikat"
            } else if (category == "IddleBersertifikatkontrak") {
                category = " Iddle"
            } else if (category == "IddleBersertifikatnokontrak") {
                category = " Dalam Proses(Bersertifikat)"
            }
            $('#text_tool_7').text(category);
            App.unblockUI();
        });
    }


    //belum bersertikat dan bersertifikat//
    function TblSertifikatCust3(category) {
        App.blockUI({ boxed: true });
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#btn-cetak_exel_6').show('slow');
        $('#text_tool_6').text('Pengguna Jasa Bersertifikat dan Belum Bersertifikat ');
        $('#table_data_6').DataTable().destroy();
        $('#table_data_6 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat3",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                temp += '<tr>' +
                    '<td class="dt-body-center">' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + (jsonList.Data[i].BUSINESS_PARTNER_NAME == null ? " - " : jsonList.Data[i].BUSINESS_PARTNER_NAME) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_ADDRESS + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + 'm²' + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CERTIFICATE_NUMBER + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].ZONE_RIP + '</td>' +
                    '<td class="dt-body-center">' + '<a href="/ContractChange/ViewContract/' + jsonList.Data[i].CONTRACT_NO + '"target="_blank">' + (jsonList.Data[i].CONTRACT_NO == null ? " - " : jsonList.Data[i].CONTRACT_NO) + '</a>' + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].TERM_IN_MONTHS == null ? " - " : jsonList.Data[i].TERM_IN_MONTHS) + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].CONTRACT_START_DATE == null ? " - " : jsonList.Data[i].CONTRACT_START_DATE) + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].CONTRACT_END_DATE == null ? " - " : jsonList.Data[i].CONTRACT_END_DATE) + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].TERM + '</td>'+
                '</tr>';
            }
            $('#table_data_6 tbody').append(temp);
            $('#table_data_6').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_6').removeClass('hidden');
            $('#view_data_6').show();
            $('#view_data_7').hide();
            $('#view_data_7').addClass('hidden');
            $('#text_tool_6').removeClass('hidden');
            // var text = (badan_usaha == "" || badan_usaha == null ? "Status : " + type : "Badan Usaha : " + badan_usaha + ", Status : " + type);
            //$('#text_tool_5').text(text);
            //PieROSelected = badan_usaha;
            SertifikatSelected = category;
            App.unblockUI();
        });
    }


    //belum bersertikat dan bersertifikat//
    function TblSertifikatPel_Iddle3(category) {
        App.blockUI({ boxed: true });
        $('#view_data_6').hide();
        $('#view_data_6').addClass('hidden');
        $('#view_data_7').hide();
        $('#view_data_7').addClass('hidden');
        $('#table_data_7').DataTable().destroy();
        $('#table_data_7 tbody').html('');
        var jenis = 0;
        var temp = "";
        var param = {
            BE_ID: $('#BE_NAME_LAHAN').val(),
            end: $('#AVL_DATE_LAHAN').val(),
            jenis: category
        };
        //console.log(param)
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailSertifikat3",
            timeout: 30000
        });
        req.done(function (data) {
            var no = 1;
            var no_data = no++;
            var jsonList = data;
            for (var i = 0; i < jsonList.Data.length; i++) {
                no_data = i + 1;
                temp += '<tr>' +
                    '<td class="dt-body-center">' + no_data + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CODE + '</td>' +
                    '<td>' + jsonList.Data[i].RO_NAME + '</td>' +
                    '<td>' + jsonList.Data[i].RO_ADDRESS + 'm²' + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].LAND_DIMENSION + '</td>' +
                    '<td class="dt-body-center">' + jsonList.Data[i].RO_CERTIFICATE_NUMBER + '</td>' +
                    '<td class="dt-body-center">' + (jsonList.Data[i].KODE_ASET == null ? " - " : jsonList.Data[i].KODE_ASET) + '</td>' +
                '</tr>';
            }
            $('#table_data_7 tbody').append(temp);
            $('#table_data_7').DataTable({
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"]
                ],
                "pageLength": 5,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "searching": true,
                "lengthChange": false,
                "bSort": false,
            });
            $('#view_data_7').removeClass('hidden');
            $('#view_data_7').show();
            $('#view_data_6').hide();
            $('#view_data_6').addClass('hidden');
            $('#text_tool_7').removeClass('hidden');
            $('#btn-cetak_exel_7').show('slow');
            SertifikatSelected = category;
            if (category == "PstBersertifikatTidak") {
                category = "Digunakan Oleh Pelindo III Bersertifikat dan Belum Bersertifikat"
            } else if (category == "Iddley") {
                category = "Iddle (Bersertifikat dan Tidak Bersertifikat)"
            } else {
                category = "Dalam Proses (Bersertifikat dan Tidak Bersertifikat)"
            }
            $('#text_tool_7').text(category);
            
            App.unblockUI();
        });
    }


    $('#btn-filter_rkap').click(function () {
        App.blockUI();
        tableRkap();
    });
    $('#btn-clear_rkap').click(function () {

        Chart5();
    });

    function tableRkap() {

        if ($("#BE_NAME_RKAP").val() == "") {
            alert("Cabang Harus Diisi");
            App.unblockUI();
            return false;
        }


        var param = {
            tahun: $('#select-periode-tahun').val(),
            triwulan: $('#triwulan').val(),
            kdCabang: $('#BE_NAME_RKAP').val(),
        };

        var table_rkap = $('#table_data_rkap').DataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
        });


        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/Home/GetDetailRkap",
            timeout: 30000
        });
        req.done(function (data) {
            var jsonList = data;
            //console.log(data);
            var temp = '';
            var i = 0;
            if (jsonList.Positionlist.length > 0) {
                temp += '<tr>' +
                    '<td></td>' +
                    '<td><b>PENGUSAHAAN PROPERTI</b></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '</tr>';
                temp += '<tr>' +
                    '<td>1</td>' +
                    '<td><b>Pengusahaan Lahan<b/><br>Bangunan</td>' +
                    '<td>M2</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>' + jsonList.Positionlist[0].TRITAHUNINI + '</td>' +
                    '<td>' + jsonList.Positionlist[0].TRITAHUNLALU + '</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '</tr>';
                temp += '<tr>' +
                    '<td>2</td>' +
                    '<td>Pengusahaan Pengairan</td>' +
                    '<td>M2</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>' + jsonList.Positionlist[1].TRITAHUNINI + '</td>' +
                    '<td>' + jsonList.Positionlist[1].TRITAHUNLALU + '</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '</tr>';
                temp += '<tr>' +
                    '<td>3</td>' +
                    '<td>Pengusahaan Bangunan</td>' +
                    '<td>M2</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>' + jsonList.Positionlist[2].TRITAHUNINI + '</td>' +
                    '<td>' + jsonList.Positionlist[2].TRITAHUNLALU + '</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '</tr>';
                temp += '<tr>' +
                    '<td>4</td>' +
                    '<td>Konsolidasi dan Distribusi Barang</td>' +
                    '<td>M2</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>' + jsonList.Positionlist[3].TRITAHUNINI + '</td>' +
                    '<td>' + jsonList.Positionlist[3].TRITAHUNLALU + '</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '<td>0</td>' +
                    '</tr>';
            }
            $('#table_data_rkap tbody').empty().append(temp);

            App.unblockUI();
        });

    }
    return {
        init: function () {
            sekarang();
            Chart1();
            Chart4();
            Chart5();
            Chart7();
            datePicker();
            datePicker2();
            BarRkap();
            $('#table_data').DataTable();

        }
    };
}();

function defineExcel() {
    var TYPE = PieSelected;
    var BUSINESS_ENTITY = $('#BE_NAME').val();
    var START_DATE = $('#START_DATE').val() == undefined ? null : $('#START_DATE').val();
    var END_DATE = $('#END_DATE').val() == undefined ? null : $('#END_DATE').val();

    var dataJSON = {
        TYPE: TYPE.toString(),
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        START_DATE: START_DATE,
        END_DATE: END_DATE
    };
    $.ajax({
        type: "POST",
        url: "/Home/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}
function defineExcel_4() {
    var TYPE = Bar4Selected;
    var BUSINESS_ENTITY = CabangSelected;
    var END_DATE = $('#POSTING_DATE').val();

    var dataJSON = {
        TYPE: TYPE.toString(),
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        END_DATE: END_DATE
    };

    $.ajax({
        type: "POST",
        url: "/Home/defineExcel_4",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}
function defineExcel_5() {
    var STATUS = StatROSelected;
    var TYPE = PieROSelected;
    var BUSINESS_ENTITY = $('#BE_NAME').val();
    var END_DATE = $('#AVL_DATE').val();

    var dataJSON = {
        TYPE: TYPE.toString(),
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        END_DATE: END_DATE
        , STATUS: STATUS
    };
    $.ajax({
        type: "POST",
        url: "/Home/defineExcel_5",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}

function defineExcel_6() {
    var TYPE = SertifikatSelected;
   /* var BUSINESS_ENTITY = $('#BE_NAME').val();
    var END_DATE = $('#AVL_DATE').val();*/

    var dataJSON = {
        TYPE: TYPE.toString(),
        BE_ID: $('#BE_NAME_LAHAN').val(),
        end: $('#AVL_DATE_LAHAN').val()
    };
    $.ajax({
        type: "POST",
        url: "/Home/defineExcel_6",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}

function defineExcel_7() {
    var TYPE = SertifikatSelected;
    /* var BUSINESS_ENTITY = $('#BE_NAME').val();
     var END_DATE = $('#AVL_DATE').val();*/

    var dataJSON = {
        TYPE: TYPE.toString(),
        BE_ID: $('#BE_NAME_LAHAN').val(),
        end: $('#AVL_DATE_LAHAN').val()
    };
    $.ajax({
        type: "POST",
        url: "/Home/defineExcel_7",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Dashboard.init();
    });
}