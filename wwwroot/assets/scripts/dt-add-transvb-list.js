﻿var TableDatatablesEditable = function () {
    //$('#surcharge').mask('000.000.000.000.000', { reverse: true });

    //FUNCTION SUM
    jQuery.fn.dataTable.Api.register('sum()', function () {
        return this.flatten().reduce(function (a, b) {
            if (typeof a === 'string') {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if (typeof b === 'string') {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }

            return a + b;
        }, 0);
    });

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var resetDatePicker = function () {
        //console.log("reset")

        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });

        $('.date-picker').each(function () {
            var date = $(this).val();
            if (date) {
                var datearray = date.split("/");
                var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                }).datepicker('setDate', new Date(newdate)); //set value
            } else {
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                });
            }
        });
    }

    var janganEnter = function () {
        $('html').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    }

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            //var jqTds = $('>td', nRow);
            for (var i = 0, iLen = 23; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }


        function editRow(oTable, nRow) {
            var buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';

            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input class="form-control date-picker" id="mask_date1" type="text" />';
            jqTds[1].innerHTML = '<input class="form-control date-picker" id="mask_date2" type="text" />';
            //jqTds[2].innerHTML = ' <select class="js-example-basic-single"><option value="AL">Alabama</option><option value="WY">Wyoming</option></select>';
            jqTds[2].innerHTML = '<input type="text" id="service-code" name="service-code" class="form-control nama-service-code2" >';

            //jqTds[2].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control nama-service-code2"></select>';
            jqTds[3].innerHTML = '<input id="txt-service-name" type="text" class="form-control service_name" disabled >';
            jqTds[4].innerHTML = '<input type="text" id="currency" class="form-control currency" disabled>';
            jqTds[5].innerHTML = '<input id="price" type="text" class="form-control price" disabled>';
            jqTds[6].innerHTML = '<input id="multiply_factor"  type="text" class="form-control multiply_factor" disabled>';
            jqTds[7].innerHTML = '<input id="qty" type="text" class="form-control">';
            jqTds[8].innerHTML = '<input type="text" id="unit" class="form-control unit" readonly>';
            jqTds[9].innerHTML = '<input id="surcharge" type="text" class="form-control">';
            jqTds[10].innerHTML = '<input id="amount" type="text" class="form-control" readonly>';
            jqTds[11].innerHTML = '<input id="amount_usd" type="text" class="form-control" readonly>';
            jqTds[12].innerHTML = '<input id="tax_code" type="text" class="form-control" disabled>';
            jqTds[13].innerHTML = '<input id="pph_code" type="text" class="form-control" disabled>';
            jqTds[14].innerHTML = '<input id="keterangan" type="text" class="form-control">';
            //jqTds[13].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';
            //jqTds[15].innerHTML = '<center><select class="form-control coa_produksi" name="coa_produksi" id="coa_produksi"></select></center>';
            jqTds[15].innerHTML = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_produksi" name="coa_produksi" readonly></center>';
            jqTds[16].innerHTML = '<center><a class="btn default btn-xs green edit" id="save" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red cancel" id="cancel"><i class="fa fa-trash"></i></a></center>';

            //var startDate = $('#mask_date1').val();

            //var date = data["startDate"].replace(/\./g, '/')
            //var datearray = date.split("/");
            //var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            //$("#mask_date1").val(date);
            //$("#mask_date1").datepicker();
            //$("#mask_date1").datepicker('setDate', new Date(newdate));

            //--- AUTOCOMPLETE SERVICE CODE
            $('.nama-service-code2').autocomplete({
                source: function (request, response) {
                    var inp = $('.nama-service-code2').val();
                    var l = inp.length;

                    var sGroup = $('#SERVICES_GROUP').val();

                    var arraySGroup = sGroup.split("|");
                    var valSGroup = arraySGroup[0];
                    var valTaxSGroup = arraySGroup[1];
                    var valPPHSGroup = arraySGroup[2];

                    //console.log('val sGroup ' + valSGroup);
                    //console.log('val valTaxSGroup ' + valTaxSGroup);
                    var param = {
                        SERVICE_NAME: inp,
                        SERVICE_GROUP: valSGroup,
                    };

                    $.ajax({
                        type: "POST",
                        url: "/TransVBList/GetDataServiceCode",
                        data: JSON.stringify(param),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            console.log(data);
                            response($.map(data, function (el) {
                                return {
                                    label: el.label,
                                    value: "",
                                    unit: el.unit,
                                    code: el.code,
                                    currency: el.currency,
                                    price: el.price,
                                    multiply: el.multiply,
                                    tax_code: el.tax_code,
                                    //tax_code: valTaxSGroup,
                                    pph_code: valPPHSGroup,
                                    service_name: el.service_name,
                                    gl_account: el.gl_account
                                };
                            }));
                        }
                    });

                },
                select: function (event, ui) {
                    event.preventDefault();
                    this.value = ui.item.code;
                    console.log(ui.item)
                    //format biaya2 dengan separator 
                    var raw_price = ui.item.price;
                    //var price = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    price = raw_price;

                    $("#txt-service-name").val(ui.item.service_name);
                    $("#unit").val(ui.item.unit);
                    $("#currency").val(ui.item.currency);
                    $("#price").val(price);
                    $("#multiply_factor").val(ui.item.multiply);
                    $("#tax_code").val(ui.item.tax_code);
                    $("#pph_code").val(ui.item.pph_code);
                    $("#qty").val(1);
                    $("#surcharge").val(0);
                    var tax = Number(Number(ui.item.price) * 0.1);
                    var amt = Number(ui.item.price);

                    //Perhitungan Pertama
                    var sub = (Number(ui.item.price) * Number(ui.item.price)) * (Number(ui.item.multiply) / 100);


                    var taxBulat = Math.round(tax);
                    var amtBulat = Math.round(amt);
                    var subBulat = Math.round(sub);

                    var formatTaxBulat = taxBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatAmtBulat = amtBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatSubBulat = subBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    //$('#TOTAL').mask('000.000.000.000.000', { reverse: true });
                    //('#surcharge').mask('000.000.000.000.000', { reverse: true });

                    $("#subtotal").val(formatSubBulat);
                    $("#tax_amount").val(formatTaxBulat);
                    if (ui.item.currency == "USD") {
                        $.ajax({
                            type: "GET",
                            url: "/TransContractOffer/GetDataCurrencyRate",
                            contentType: "application/json",
                            dataType: "json",
                            data: {},
                            success: function (data) {
                                rate = data.Data;
                                grandTotalUsd = amtBulat * rate;
                                var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                console.log(amtBulat);
                                $("#amount").val(fGrandTotalUsd);
                                $("#amount_usd").val(formatAmtBulat);
                                $('#RATE').val(rate);
                            }
                        });
                    } else {
                        $("#amount").val(formatAmtBulat);
                        $("#amount_usd").val(formatAmtBulat);
                        $('#RATE').val(1);
                    }
                    document.getElementById("qty").focus();
                }
            });

            //cari coa
            $('#editable_transvb').on('click', 'tr #btn-coa', function () {
                $('#coaModal').modal('show');
                if ($.fn.DataTable.isDataTable('#table-coa')) {
                    $('#table-coa').DataTable().destroy();
                }

                var svc = $('#SERVICES_GROUP').val();
                var paramCoa = svc.substring(0, 3);
                $('#table-coa').DataTable({

                    "ajax":
                    {
                        "url": "/TransVBList/GetListCoa?param=" + paramCoa,
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                        },
                        "dataSrc": function (response) {
                            var data = {};
                            data.draw = response.paging.page;
                            data.recordsTotal = response.paging.total;
                            data.recordsFiltered = response.paging.total;
                            data.data = response.result;
                            return data.data;
                        },
                    },
                    "columns": [
                        {
                            "render": function () {
                                var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center",
                        },
                        {
                            "data": "COA_PROD",
                            "class": "dt-body-center",
                        },
                        {
                            "data": "DESKRIPSI",
                        },
                        {
                            "data": "GL_TEXT",
                        },

                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,

                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"]
                    ],

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
            });

            //choose coa
            $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
                var baris = $(this).parents('tr')[0];
                var table = $('#table-coa').DataTable();
                var data = table.row(baris).data();
                var c_data = null;
                var value = data['COA_PROD'];
                //console.log(data,value);

                var elems = $('#editable_transvb').find('tbody tr');
                $(elems).each(function (index) {
                    var x = $(this).find('td')[0];
                    var y = $(this).find('td')[1];
                    c_data = $('#editable_transvb').DataTable().row(index).node();
                    //console.log(index, c_data)

                    var elem = $(c_data).find('#coa_produksi');
                    elem.val(value);

                });
                $('#coaModal').modal('hide');
            });

            // SAMPLE SELECT2
            $(".js-example-basic-single").select2();


            getServicesCode(aData[2]);
            selectingServiceCode();

            //Tax : (Amount+Sourcharge) *10%
            $('#surchargex').on('change', function (e) {
                var amount2 = $('#amount').val();
                var surcharge = $('#surcharge').val();

                //hapus formatting 
                var nfAmount = parseFloat(amount2.split('.').join(''));
                var nfSurcharge = parseFloat(surcharge.split('.').join(''));

                //isi amount
                var prc = $('#price').val();
                var multiply_factor = $('#multiply_factor').val();
                var qty = $('#qty').val();

                //hapus formatting isi amount
                var nfPrc = parseFloat(prc.split('.').join(''));
                var nfMultiplyFactor = parseFloat(multiply_factor.split('.').join(''));
                var nfQty = parseFloat(qty.split('.').join(''));

                var isiAmount = ((Number(nfPrc) * (Number(nfMultiplyFactor) / 100)) * Number(nfQty)) + Number(nfSurcharge);

                var totalAmount = ((Number(prc) * (Number(nfMultiplyFactor) / 100)) * Number(nfQty)) + Number(nfSurcharge);

                var rate = $('#RATE').val();
                var isiAmountBulat = Math.round(totalAmount * rate);
                var isiAmountBulatUsd = Math.round(totalAmount);

                var formatIsiAmountBulat = isiAmountBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                var formatIsiAmountBulatUsd = isiAmountBulatUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                var formatSurcharge = surcharge.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                $("#amount").val(formatIsiAmountBulat);
                $("#amount_usd").val(formatIsiAmountBulatUsd);
                $("#surcharge").val(formatSurcharge);

            });

            //Sub Total : Amount + Tax 
            $('#qty').on('blur', function (e) {
                var amount = $('#amount').val();
                var prc = $('#price').val();
                var qty = $('#qty').val();
                var multiply_factor = $('#multiply_factor').val();
                var surcharge = $('#surcharge').val(); // variabel ini sepertinya kemarin kehapus, mknya NaN

                var nfSurcharge = parseFloat(surcharge.split('.').join(''));
                var rate = $('#RATE').val();
                //Amount : (Price*Multiply Factor/100)*Quantity
                //Remove formatting sebelum dihitung
                nfPrc = parseFloat(prc.split('.').join(''));

                var harga = Number(prc) * Number(qty) * (Number(multiply_factor) / 100);
                //var hitungraw = Number(harga) + Number(nfSurcharge);
                var hitungraw = Number(harga);
                console.log('total no format ' + harga);
                console.log('hitung raw ' + hitungraw);
                var rounding = Math.round(hitungraw * rate);
                var roundingUsd = Math.round(hitungraw);
                console.log('hitung rounding ' + prc + qty + multiply_factor);
                console.log('rate ' + rate);




                /*
                var hitung1 = Number(qty) * Number(nfPrc) * (Number(multiply_factor) / 100);
                var amount = Number(hitung1) + Number(nfSurcharge);
                console.log('hitung 1' + hitung1);
                console.log('hitung 2' + amount);
                */


                //var amount = (Number(nfPrc) * (Number(multiply_factor) / 100)) * (Number(qty) + nfSurcharge);
                var amountBulat = Math.round(amount);

                var formatAmountBulat = rounding.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                var formatAmountBulatUsd = roundingUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                $('#amount').val(formatAmountBulat);
                $('#amount_usd').val(formatAmountBulatUsd);
                $('#surcharge').val(nfSurcharge);
            });

            $("#mask_date1").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $("#mask_date2").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            $("#qtyx").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            $("#surcharge").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            resetDatePicker();
        }

        function saveRow(oTable, nRow) {
            console.log(111);
            var service_code = $('.nama-service-code2').val();
            var service_name = $('.service_name').val();
            var currency = $('.currency').val();
            var price = $('.price').val();
            var multiply_factor = $('.multiply_factor').val();
            var unit = $('.unit').val();
            var coa_prod = $('#coa_produksi').val();
            //var namaKemasan = $('.nama-service-code2').text();

            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(service_code, nRow, 2, false);
            oTable.fnUpdate(service_name, nRow, 3, false);
            oTable.fnUpdate(currency, nRow, 4, false);
            oTable.fnUpdate(price, nRow, 5, false);
            oTable.fnUpdate(multiply_factor, nRow, 6, false);
            oTable.fnUpdate(unit, nRow, 8, false);

            //oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            //oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            //oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            //oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 7, false);
            //oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[10].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[11].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[12].value, nRow, 12, false);
            oTable.fnUpdate(jqInputs[13].value, nRow, 13, false);
            oTable.fnUpdate(jqInputs[14].value, nRow, 14, false);
            //oTable.fnUpdate(' <a class="delete" href="">Delete</a>', nRow, 13, false);
            oTable.fnUpdate(coa_prod, nRow, 15, false);
            oTable.fnUpdate('<center><a class="btn default btn-xs red delete" id="delete"><i class="fa fa-trash"></i></a></center>', nRow, 16, false)
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(kodeKemasan, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[11].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[12].value, nRow, 12, false);
            oTable.fnUpdate(jqInputs[13].value, nRow, 13, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 23, false);
            oTable.fnDraw();
        }

        var table = $('#editable_transvb');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 100,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1],
                    "width": "20%"
                },
                {
                    "targets": [12],
                    "width": "30%"
                }
            ]
        });

        var tableWrapper = $("#editable_new_transvb");

        var nEditing = null;
        var nNew = false;

        $('#editable_new_transvb').click(function (e) {

            //var cekPiutang = $('#CUSTOMER_SAP_AR').val();
            //cekPiut("1M", cekPiutang);

            if ($('#SERVICES_GROUP').val() != "") {
                e.preventDefault();
                getCoaProduksi();
                if (nNew && nEditing) {
                    if (confirm("Previous row not saved. Do you want to save it ?")) {
                        saveRow(oTable, nEditing); // save
                        /*
                        $(nEditing).find("td:first").html("Untitled");
                        nEditing = null;
                        nNew = false;
                        */
                    } else {
                        oTable.fnDeleteRow(nEditing); // cancel
                        nEditing = null;
                        nNew = false;
                        return;
                    }
                }

                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = true;
            } else {
                swal('Error', 'Pilih Service Group Terlebih dahulu', 'error');
            }
            
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            /*
            swal({
                title: "Are you sure?",
                text: "Are you sure want to delete this detail.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    swal("Deleted!", "Data Deleted Successfully.", "success");
                }
                else {
                }
            });
            */


            //Konfirmasi akan hapus data detail

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            else {
                var baris = $(this).parents('tr')[0];
                var table = $('#editable_transvb').DataTable();
                var data = table.row(baris).data();
                var subtotal = data[10];
                //console.log(data[10]);

                var nfX = parseFloat(subtotal.split('.').join(''));
                var t = $("#TOTAL").val();
                var total = Number(t) - Number(nfX);
                $("#TOTAL").val(total);
            }
            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted!");


        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            var localstore = 0;
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && document.getElementById('save')) {
                /* Editing this row and want to save it */
                var x = $("#amount").val();
                var nfX = parseFloat(x.split('.').join(''));
                var t = $("#TOTAL").val();
                var total = Number(t) + Number(nfX);
                $("#TOTAL").val(total);

                /*
                var asdf = localStorage.setItem("hitung", total);
                var retrievedObject = localStorage.getItem('hitung');
                var ser = retrievedObject + Number(x) ;
                console.log(ser);
                */

                //console.log(x);
                //console.log(t);
                //console.log(total);
                var mask_date1 = $('#mask_date1').val();
                var mask_date2 = $('#mask_date2').val();
                var servicecode = $('#service-code').val();
                var coa_prod2 = $('#coa_produksi').val();
                var qtys = $('#qty').val();

                var startDate = mask_date1.replace(/\./g, '/')
                var startArray = startDate.split("/");
                var newdate_start = startArray[1] + '/' + startArray[0] + '/' + startArray[2];
                var tgl_start = new Date(newdate_start);

                var endDate = mask_date2.replace(/\./g, '/')
                var endArray = endDate.split("/");
                var newdate_end = endArray[1] + '/' + endArray[0] + '/' + endArray[2];
                var tgl_end = new Date(newdate_end);

                var sGroup2 = $('#SERVICES_GROUP').val();

                var arraySGroup2 = sGroup2.split("|");
                var valSGroup2 = arraySGroup2[0];

                console.log("nilai service group " + valSGroup2);
                if (valSGroup2 == '4919900000' || valSGroup2 == '4911100000' || valSGroup2 == '4090715000' || valSGroup2 == '4911200000' || valSGroup2 == '4910800000' || valSGroup2 == '4910500000') {
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    swal('Success', 'Detail Data Updated Successfully', 'success');
                    //////
                } else {
                    if (mask_date1 && mask_date2 && servicecode && qtys) {
                        if (coa_prod2 == '' || coa_prod2 == null) {
                            console.log("COA KOSONG");
                            swal('Warning', 'Data is not completed', 'warning');
                        } else if (tgl_end < tgl_start) {
                            console.log("date error");
                            swal('Warning', 'End Date harus lebih besar dari Start Date', 'warning');
                        } else {
                            console.log("COA ADA : " + coa_prod2);
                            saveRow(oTable, nEditing);
                            nEditing = null;
                            swal('Success', 'Detail Data Updated Successfully', 'success');
                        }
                    } 
                    else {
                        swal('Warning', 'Data is not completed', 'warning');
                    }
                }
                //alert("Updated!");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = false; // tambahan
            }
        });

        $('#btn-update').click(function () {
            var tvb_d = [];
            var id_header = "";            

            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var POSTING_DATE = $('#POSTING_DATE').val();
            var SERVICES_GROUP = $('#SERVICES_GROUP').val();
            var COSTUMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_MDM = $('#CUSTOMER_MDM').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR').val();
            var TOTAL = $('#TOTAL').val();
            var RATE = $('#RATE').val();
            var CREATED_BY = $('#CREATED_BY').val();
            var BRANCH_ID = $('#BRANCH_ID').val();

            var arraySGroup = SERVICES_GROUP.split("|");
            var valSGroup = arraySGroup[0];
            var valTaxSGroup = arraySGroup[1];
            var valPPHSGroup = arraySGroup[2];

            var cekP = $("#CUSTOMER_SAP_AR").val();
            cekPiut("1M", cekP);
            var xPiut = $("#cPiut").val();

            console.log("xPiut : " + xPiut);

            // Pengecekan save kecil
            var DTAneka = $('#editable_transvb').dataTable();
            var all_row = DTAneka.fnGetNodes();
            for (var i = 0; i < all_row.length; i++) {
                var cek = $(all_row[i]).find('input[name="service-code"]').val();
            }
            console.log('cek ' + cek);

            if (cek) {
                swal('Warning', 'Please Save Transaction Detail!', 'warning');
            }             
            
            else {
                //console.log('query insert');
                if (PROFIT_CENTER && POSTING_DATE && valSGroup && COSTUMER_ID && CUSTOMER_MDM && INSTALLATION_ADDRESS && CUSTOMER_SAP_AR) {
                    // Declare dan getData dari dataTable Detail
                    var DTDetail = $('#editable_transvb').dataTable();
                    var countDTDetail = DTDetail.fnGetData();
                    arrDetail = new Array();

                    for (var i = 0; i < countDTDetail.length; i++) {
                        var item = DTDetail.fnGetData(i);
                        var servicePrice = item[5];
                        var nfServicePrice = parseFloat(servicePrice.split('.').join(''));

                        var amount = item[10];
                        var nfAmount = parseFloat(amount.split('.').join(''));

                        //split unit, ambil kode unti yg dibaca SAP
                        var rawUnit = item[8];
                        var splitRaw = rawUnit.split(" | ");
                        var valueUnitSAP = splitRaw[0];

                        var paramDTDetail = {
                            START_DATE: item[0],
                            END_DATE: item[1],
                            SERVICE_CODE: item[2],
                            SERVICE_NAME: item[3],
                            CURRENCY: item[4],
                            PRICE: servicePrice,
                            MULTIPLY_FACTOR: item[6],
                            QUANTITY: item[7],
                            UNIT: valueUnitSAP,
                            SURCHARGE: item[9],
                            AMOUNT: nfAmount.toString(),
                            TAX_CODE: item[12],
                            PPH_CODE: item[13],
                            REMARK: item[14],
                            COA_PROD: item[15]
                        }
                        arrDetail.push(paramDTDetail);
                        valTaxSGroup = item[12];
                    }

                    var param = {
                        PROFIT_CENTER: PROFIT_CENTER,
                        POSTING_DATE: POSTING_DATE,
                        SERVICES_GROUP: valSGroup,
                        TAX_CODE: valTaxSGroup,
                        PPH_CODE: valPPHSGroup,
                        COSTUMER_ID: COSTUMER_ID,
                        COSTUMER_MDM: CUSTOMER_MDM,
                        INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                        CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                        TOTAL: TOTAL,
                        RATE: RATE,
                        CREATED_BY: CREATED_BY,
                        KD_CABANG: BRANCH_ID,
                        details: arrDetail,
                        RAW_JSON_DATA: JSON.stringify(arrDetail)
                    };
                    console.log(arrDetail);

                    App.blockUI({ boxed: true });
                    if (arrDetail.length <= 0) {
                        //swal('Warning', 'Detail Transaksi Tidak Ada, Mohon Cek Lagi. Jika Masih Muncul Pesan Ini Silahkan Clear Cache dan Ulangi Transaksi!', 'warning');
                        swal('Warning', 'Detail Transaksi Tidak Ada, Mohon Cek Lagi. Jika Masih Muncul Pesan Ini Silahkan Clear Cache dan Ulangi Transaksi!', 'warning').then(function (isConfirm) {
                            window.location = "/TransVBList";
                        });
                    }
                    else if (xPiut == "X") {
                        swal('Warning', 'Customer memiliki piutang!', 'warning');
                    }
                    else {
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/TransVBList/SaveHeader"
                        });

                        req.done(function (data) {
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location = "/TransVBList";
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransVBList";
                                });
                            }
                        });
                    }
                }// END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM
                
                else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }// End of save kecil

        });

        $('#CUSTOMER_MDM').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/TransVBList/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_ID").val(ui.item.code);
                $("#INSTALLATION_ADDRESS").val(ui.item.address);
                $("#CUSTOMER_SAP_AR").val(ui.item.sap);
                //cekPiut("1D", ui.item.sap);
                cekPiut("1M", ui.item.sap);

            }
        });

    }

    //var cekPiut = function (doc_type, cust_code) {
    //    $.ajax({
    //        type: "POST",
    //        url: "/TransRentalRequest/cekPiutangSAP",
    //        data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
    //        contentType: "application/json; charset=utf-8",
    //        success: function (data) {
    //            if (data.E_RELEASE_STATUS == "X") {
    //                $("#cPiut").val(data.E_RELEASE_STATUS);
    //                swal('Warning', data.E_DESCRIPTION, 'error');
    //                //return data.E_RELEASE_STATUS;
    //            }
    //            else {
    //                $("#cPiut").val("1");
    //            }
    //        }
    //    });
    //}

    //---SELECT 2 SERVICES CODE
    var getServicesCode = function (xxx) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.SERVICE_CODE + " (" + repo.SERVICE_NAME + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.SERVICE_NAME || repo.text;
        }

        $(".nama-service-code").select2({

            allowClear: true,
            width: "on",
            ajax: {
                url: "/TransVB/GetDataServiceCodeSelect2",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: item.data.SERVICE_CODE };
                callback(data);
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,

            placeholder: "ketikkan service name"
        });
    }       

    var selectingServiceCode = function () {
        $(".nama-service-code").on("select2:select", function (e) {
            console.log("ok");
            var service_code = e.params.data.SERVICE_CODE;
            var service_name = e.params.data.SERVICE_NAME;
            var nRow = $(this).parents('tr')[0];
            var aData = $('#editable_transvb').dataTable().fnGetData(nRow);
            $('#editable_transvb').dataTable().fnUpdate(service_code, nRow, 2, false);
            $('#editable_transvb').dataTable().fnUpdate(service_name, nRow, 3, false);

            //$('#editable_transvb').dataTable().fnUpdate(namaGudlap, nRow, 4, false);
        });
    }
    //--- END OF SELECT 2 OTHER SERVICES
    var cekPiut = function (doc_type, cust_code) {
        var param = {
            DOC_TYPE: doc_type,
            CUST_CODE: cust_code
        };

        $.ajax({
            type: "POST",
            async: false,
            url: "/TransVB/cekPiutangSAP",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/TransVBList/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data;
                var listItems = "";
                listItems += "<option value=''>-- Chose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
            }
        });

    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransVBList";
        });
    }
    $('#SERVICES_GROUP').change(function () {
        var svc = $('#SERVICES_GROUP').val();
        paramCoa = svc.substring(0, 3);
    });

    return {

        init: function () {
            handleTable();
            batal();
            janganEnter();
            comboServiceGroup();
            datePicker();
            cekPiut();
        }

    };

}();

//function getCoaProduksi() {
//    $.ajax({
//        type: "GET",
//        url: "/TransContractOffer/GetDataDropDownCoaProduksi",
//        contentType: "application/json",
//        dataType: "json",
//        success: function (data) {
//            var jsonList = data
//            var listItems = "";
//            listItems += "<option value=''>-- Choose COA Produksi --</option>";
//            for (var i = 0; i < jsonList.data.length; i++) {
//                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
//            }
//            $(".coa_produksi").html('');
//            $(".coa_produksi").append(listItems);
//        }
//    });
//}
function getCoaProduksi() {
    var svc = $('#SERVICES_GROUP').val();
    var paramCoa = svc.substring(0, 3);
    $.ajax({
        type: "GET",
        url: "/TransVBList/GetDataDropDownCoaProduksi?param=" + paramCoa,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(".coa_produksi").html('');
            $(".coa_produksi").append(listItems);
        }
    });
}

jQuery(document).ready(function () {
    TableDatatablesEditable.init();

    //ambil tanggal sysdate js
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#POSTING_DATE').val(today);
});