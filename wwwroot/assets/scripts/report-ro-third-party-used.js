﻿var ReportROThirdPartyUsed = function () {

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var CBBE = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportROThirdPartyUsed/GetDataBENew",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBYears();
                //$("#BUSINESS_ENTITY").trigger("chosen:updated");
            }
        });
        //$('#BUSINESS_ENTITY').chosen({ width: "100%" });
    }

    var CBYears = function () {
        // Ajax untuk ambil ContractOfferNumber
        var listItems = "";
        listItems += "<option value=''>-- Choose Years --</option>";

        for (i = new Date().getFullYear(); i > 1900; i--) {
            listItems += "<option value='" + i + "'>" + i + "</option>";
        }
        $("#YEARS").html('');
        $("#YEARS").append(listItems);
        CBDataUsageType();
        //$("#YEARS").trigger("chosen:updated");
        //$('#YEARS').chosen({ width: "100%" });
    }

    var CBDataUsageType = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportROThirdPartyUsed/GetDataUsageType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Usage Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#USAGE_TYPE").html('');
                $("#USAGE_TYPE").append(listItems);
                CBDataZone();
                //$("#USAGE_TYPE").trigger("chosen:updated");
            }
        });
        //$('#USAGE_TYPE').chosen({ width: "100%" });
    }

    var CBDataZone = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportROThirdPartyUsed/GetDataZONE",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Zone --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#ZONE").html('');
                $("#ZONE").append(listItems);
                CBDataContractUsage();
                //$("#ZONE").trigger("chosen:updated");
            }
        });
        //$('#ZONE').chosen({ width: "100%" });
    }

    var CBDataContractUsage = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportROThirdPartyUsed/GetDataContractUsage",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Usage --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#CONTRACT_USAGE").html('');
                $("#CONTRACT_USAGE").append(listItems);
                handleBootstrapSelect();
                //$("#CONTRACT_USAGE").trigger("chosen:updated");
            }
        });
        //$('#CONTRACT_USAGE').chosen({ width: "100%" });
    }

    var dataTableResult = function () {
        var table = $('#table-report-ro-third-party-used');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var showFilter = function () {
        $('#btn-filter-third-party').click(function () {
            console.log($('#BUSINESS_ENTITY').val());

            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-ro-third-party-used')) {
                $('#table-report-ro-third-party-used').DataTable().destroy();
            }
            $('#table-report-ro-third-party-used').DataTable({
                "ajax": {
                    "url": "/ReportROThirdPartyUsed/GetFilterTwoNew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.val_years = $('#YEARS').val();
                        data.usage_type = $('#USAGE_TYPE').val();
                        data.val_zone = $('#ZONE').val();
                        data.val_contract_usage = $('#CONTRACT_USAGE').val();

                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "BE_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "USAGE_TYPES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KELOMPOK_USAHA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_ADDRESS",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.LUAS === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.LUAS + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_USAGES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "ZONA_RIPS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_CERTIFICATE_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.LEGAL_CONTRACT_NO === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.LEGAL_CONTRACT_NO + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TERM_IN_MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z003 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z003.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z003 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z003.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z005 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z005.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z005 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z005.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z007 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z007.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z007 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z007.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z008 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z008.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z008 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z008.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z013 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z013.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z013 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z013.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.JENIS_TARIF_Z014 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.JENIS_TARIF_Z014.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT_Z014 === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT_Z014.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.INSTALLMENT_AMOUNT === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.INSTALLMENT_AMOUNT.toString()
                                    .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        "."
                                    ); + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    return {
        init: function () {
            dataTableResult();
            CBBE();
            //CBYears();
            //CBDataZone();
            //CBDataUsageType();
            //CBDataContractUsage();
            showFilter();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var YEARS = $('#YEARS').val();
    var USAGE_TYPE = $('#USAGE_TYPE').val();
    var ZONE = $('#ZONE').val();
    var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();

    var dataJSON = {
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        YEARS: YEARS,
        USAGE_TYPE: USAGE_TYPE,
        ZONE: ZONE,
        CONTRACT_USAGE: CONTRACT_USAGE
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportROThirdPartyUsed/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });

            //console.log("Sukses");
            //setTimeout(function () {
            //    exportExcel();
            //}, 2000);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportROThirdPartyUsed/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

jQuery(document).ready(function () {
    ReportROThirdPartyUsed.init();
});

jQuery(document).ready(function () {
    $('#table-report-ro-internal-used > tbody').html('');
    //$('#BUSINESS_ENTITY', this).chosen();
    //$('#YEARS', this).chosen();
    //$('#USAGE_TYPE', this).chosen();
    //$('#ZONE', this).chosen();
    //$('#CONTRACT_USAGE', this).chosen();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportROThirdPartyUsed/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}