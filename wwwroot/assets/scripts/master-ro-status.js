﻿var MasterRO = function () {


    var initTableRentalObject = function () {
        $('#table-ro-rentalobject').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataRentalObject",
                "type": "GET",
            },
            "columns": [
                {
                    "data": "RO_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "BE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "RO_NAME",
                    "class": "dt-body-center"
                },
                {
                    "data": "ZONE_RIP",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var str = "";
                        
                        if (full.OCCUPANCY === 'VACANT') {
                            str += '<span class="label label-sm label-success"> ' + full.OCCUPANCY + ' </span>';
                        } else if (full.OCCUPANCY === 'OCCUPIED') {
                            str += '<span class="label label-sm label-danger"> ' + full.OCCUPANCY + ' </span>';
                        } else if (full.OCCUPANCY === 'BOOKED') {
                            str += '<span class="label label-sm label-info"> ' + full.OCCUPANCY + ' </span>';
                        } else {
                            str += '<span class="label label-sm label-info"> ' + full.OCCUPANCY + ' </span>';
                        }
                        return str;
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.OCCUPANCY == 'VACANT' || full.OCCUPANCY == 'BOOKED') {
                            return '';
                        } else {
                            return data;
                        }
                    },
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center",
                },
                {
                    "render": function (data, type, full) {
                        if (full.OCCUPANCY == 'VACANT') {
                            return '';
                        } else {
                            return data;
                        }
                    },
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center",
                },
                {
                    "render": function (data, type, full) {
                        var aksi = "";
                        //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah-ro" title="Ubah"><i class="fa fa-edit"></i></a>';
                        //aksi += '<a class="btn btn-icon-only blue" id="btn-ubah-status-ro" title="Ubah Status"><i class="fa fa-exchange"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewROModal" class="btn btn-icon-only green" id="btn-detil" title="View"><i class="fa fa-eye"></i></a>';
                        //aksi += '<a class="btn btn-icon-only green" id="btn-print-qr" title="Print QR Code"><i class="fa fa-print "></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "RO_NUMBER",
                    "class": "dt-body-center",
                    //"visible": false
                },
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true,
            "aoColumnDefs": [
                { "bVisible": false, "aTargets": [9] }
            ]
        });
    }

    // Ubah status rental object
    var ubahStatusRO = function () {
        $('body').on('click', 'tr #btn-ubah-status-ro', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();
            console.log('RO CODE ' + data["RO_CODE"]);

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["RO_CODE"],
                        method: "get",
                        url: "/RentalObject/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    //Init table measurement untuk ditampilkan di view detail
    var initTableMeasurement = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            var tableDetilMeasurement = $('#table-measurement').DataTable();
            tableDetilMeasurement.destroy();

            var number_ro = data["RO_NUMBER"];
            $('#table-measurement').DataTable({
                "ajax": {
                    "url": "/RentalObject/GetDataMeasurement/" + number_ro,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "REF_DATA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DESCRIPTION"
                    },
                    {
                        "data": "AMOUNT"
                    },
                    {
                        "data": "UNIT"
                    },
                    {
                        "data": "VALID_FROM"
                    },
                    {
                        "data": "VALID_TO"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    //Init table measurement untuk ditampilkan di view detail
    var initTableFixtureFitting = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            var tableDetilFixture = $('#table-fixture-fitting').DataTable();
            tableDetilFixture.destroy();

            var number_ro = data["RO_NUMBER"];
            $('#table-fixture-fitting').DataTable({
                "ajax": {
                    "url": "/RentalObject/GetDataFixtureFitting2/" + number_ro,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "REF_DATA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DESCRIPTION"
                    },
                    {
                        "data": "POWER_CAPACITY"
                    },
                    {
                        "data": "VALID_FROM"
                    },
                    {
                        "data": "VALID_TO"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    //Init table measurement untuk ditampilkan di view detail
    var initTableOccupancy = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            var tableDetilOccupancy = $('#table-occupancy').DataTable();
            tableDetilOccupancy.destroy();

            var number_ro = data["RO_NUMBER"];
            $('#table-occupancy').DataTable({
                "ajax": {
                    "url": "/RentalObject/GetDataOccupancy/" + number_ro,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "F_VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "F_VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "REASON",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS",
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }
    //print qr code
    var printQrCode = function () {
        $('body').on('click', 'tr #btn-print-qr', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();
            window.open('/pdf/cetakQr?id=' + btoa(data["RO_CODE"]), '_blank');

        });
    }

    // ubah untuk testing (sesuai dengan field di database)
    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            //window.location = "/RentalObject/Edit/" + data["RO_NUMBER"];
        });
    }

    // Ubah sesuai dengan mockup
    var ubahro = function () {
        $('body').on('click', 'tr #btn-ubah-ro', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            window.location = "/RentalObject/Edit/" + data["RO_NUMBER"];
        });
    }

    // Detail modal rental object
    var detil = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            $("#BE_ID").val(data["BE_ID"]);
            $("#RO_NUMBER").val(data["RO_NUMBER"]);
            $("#RO_CODE").val(data["RO_CODE"]);
            $("#RO_TYPE").val(data["RO_TYPE"]);
            $("#RO_NAME").val(data["RO_NAME"]);
            $("#RO_CERTIFICATE_NUMBER").val(data["RO_CERTIFICATE_NUMBER"]);
            $("#RO_ADDRESS").val(data["RO_ADDRESS"]);
            $("#RO_POSTALCODE").val(data["RO_POSTALCODE"]);
            $("#RO_CITY").val(data["RO_CITY"]);
            $("#MEMO").val(data["MEMO"]);
            $("#VALID_FROM").val(data["VALID_FROM"]);
            $("#VALID_TO").val(data["VALID_TO"]);
            $("#BE_NAME").val(data["BE_NAME"]);
            $("#USAGE_TYPE").val(data["USAGE_TYPE"]);
            $("#PROVINCE").val(data["PROVINCE"]);
            $("#ZONE_RIP").val(data["ZONE_RIP"]);
            $("#FUNCTION_ID").val(data["FUNCTION_ID"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#LOCATION_ID").val(data["LOCATION_ID"]);
        });
    }

    // Form Detail (Fixture Fitting)
    var fixturefitting = function () {
        $('body').on('click', 'tr #btn-fixfit', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();
            var uriId = $('#UriId').val();
            window.location = "/RentalObject/EditDetailRO/" + data["RO_NUMBER"];
        });
    }

    var deletedata = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete Rental Object \"" + data["RO_NAME"] + "\"?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["RO_NUMBER"],
                        method: "get",
                        url: "/RentalObject/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Berhasil', data.message, 'success');
                        } else {
                            swal('Gagal', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var status = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-ro-rentalobject').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Apakah Anda yakin?',
                text: "Status aktif tarif akan diubah.",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["RO_NUMBER"],
                        method: "get",
                        url: "MasterTarif/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Berhasil', data.message, 'success');
                        } else {
                            swal('Gagal', data.message, 'error');
                        }
                    });
                }
            });
        });
    }


    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/RentalObject/Add';
        });
    }

    var addTest = function () {
        $('#btn-add-test').click(function () {
            window.location.href = '/RentalObject/AddTest';
        });
    }

    var addTest = function () {
        $('#btn-add-rentalobject').click(function () {
            window.location.href = '/RentalObject/AddRentalObject';
        });
    }

    var changeBe = function () {
        $('#BE_NAME').change(function () {
            var be_id = $('#BE_NAME').val();

            $('#profit-center').html('');
            $('#profit-center').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/RentalObject/getProfitCenter",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#profit-center').append(temp);
                });
            }

        });
    }
    var Filter = function () {
        $('#filter').click(function () {
            var be = $('#BE_NAME').val();
            var pc = $('#profit-center').val();
            var oc = $('#occupancy').val();
            //console.log(be, typeof be, pc, typeof pc);
            if (be != "" && be != null && typeof be !== 'undefined') {
                var table = $('#table-ro-rentalobject').DataTable();
                table.destroy();
                $('#table-ro-rentalobject tbody').html('');

                $('#table-ro-rentalobject').DataTable({
                    "ajax": {
                        "url": "/RentalObject/GetDataRentalObjectfilter?be=" + be + "&pc=" + pc + "&occupancy=" + oc,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "RO_CODE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "BE_NAME",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_NAME"
                        },
                        {
                            "data": "ZONE_RIP",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "PROFIT_CENTER",
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                str = "";
                                //if (full.ACTIVE == '1') {
                                //    str += '<span class="label label-sm label-success"> ACTIVE </span>';
                                //}
                                //else {
                                //    str += '<span class="label label-sm label-danger"> INACTIVE </span>';
                                //}

                                if (full.OCCUPANCY === 'VACANT') {
                                    str += '<span class="label label-sm label-success"> ' + full.OCCUPANCY + ' </span>';
                                } else if (full.OCCUPANCY === 'OCCUPIED') {
                                    str += '<span class="label label-sm label-danger"> ' + full.OCCUPANCY + ' </span>';
                                } else if (full.OCCUPANCY === 'BOOKED') {
                                   str += '<span class="label label-sm label-info"> ' + full.OCCUPANCY + ' </span>';
                                } else {
                                    str += '<span class="label label-sm label-info"> ' + full.OCCUPANCY + ' </span>';
                                }
                                return str;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.OCCUPANCY == 'VACANT' || full.OCCUPANCY == 'BOOKED') {
                                    return '';
                                } else {
                                    return data;
                                }
                            },
                            "data": "CONTRACT_NO",
                            "class": "dt-body-center",
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.OCCUPANCY == 'VACANT') {
                                    return '';
                                } else {
                                    return data;
                                }
                            },
                            "data": "CONTRACT_OFFER_NO",
                            "class": "dt-body-center",
                        },
                        {
                            "render": function (data, type, full) {
                                var aksi = "";
                                //var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah-ro" title="Ubah"><i class="fa fa-edit"></i></a>';
                                //aksi += '<a class="btn btn-icon-only blue" id="btn-ubah-status-ro" title="Ubah Status"><i class="fa fa-exchange"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewROModal" class="btn btn-icon-only green" id="btn-detil" title="View"><i class="fa fa-eye"></i></a>';
                                //aksi += '<a class="btn btn-icon-only green" id="btn-print-qr" title="Print QR Code"><i class="fa fa-print "></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_NUMBER",
                            "class": "dt-body-center",
                            //"visible": false
                        },
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,

                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true,
                    "aoColumnDefs": [
                        { "bVisible": false, "aTargets": [9] }
                    ]
                });
            } else {
                swal("Bussiness Entity Empty", "Bussiness Entity Cant be Empty", "error");
            }


        });
        $('#print-qr').click(function () {
            var be = $('#BE_NAME').val();
            var pc = $('#profit-center').val();
            if (be) {
                window.open('/pdf/cetakQr?kodeCabang=' + btoa(be) + '&kodeProfit=' + btoa(pc), '_blank');
            } else {
                swal("Bussiness Entity Empty", "Bussiness Entity Cant be Empty", "error");
            }

        });
    }

    return {
        init: function () {
            initTableRentalObject();
            status();
            deletedata();
            add();
            addTest();
            ubahro();
            fixturefitting();
            detil();
            initTableMeasurement();
            initTableFixtureFitting();
            initTableOccupancy();
            ubahStatusRO();
            changeBe();
            Filter();
            //printQrCode();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterRO.init();
    });
}