﻿var currency = function () {
    /*var resetDatePicker = function () {
        //console.log("reset")
        if (jQuery().datepicker) {
            $('.date-picker').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            $('.date-picker').each(function () {
                var date = $(this).val();
                if (date) {
                    var datearray = date.split("/");
                    var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                    $(this).datepicker({
                        format: "dd/mm/yyyy"
                    }).datepicker('setDate', new Date(newdate)); //set value
                } else {
                    $(this).datepicker({
                        format: "dd/mm/yyyy"
                    });
                }
            });
        }
    }*/
    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }
   
    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/Currency/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "CODE"
                },
                {
                    "data": "SYMBOL",
                    "class": "dt-body-center",
                },
                {
                    "data": "VALIDITY_FROM",
                    "class": "dt-body-right",
                },
                {
                    "data": "VALIDTY_TO",
                    "class": "dt-body-right",
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var ID = parseInt($('#param_id').val());
            var CODE = $('#CODE').val();
            var SYMBOL = $('#SYMBOL').val();
            var VALIDTY_TO = $('#VALIDTY_TO').val();
            var VALIDITY_FROM = $('#VALIDITY_FROM').val();
            

            if (CODE !== "" && SYMBOL !== "" && VALIDITY_FROM !== "" && VALIDTY_TO !== "") {
                var param = {
                    ID: ID,
                    CODE: CODE,
                    SYMBOL: SYMBOL,
                    VALIDITY_FROM: VALIDITY_FROM,
                    VALIDTY_TO: VALIDTY_TO
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    dataType : "json",
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Currency/Insert",
                    timeout: 30000
                });

                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    var clear = function () {
        $('input').val('');
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        $('#txt-judul').text('Edit');
        $('#addapvsetModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        $('#CODE').val(aData.CODE);
        $('#SYMBOL').val(aData.SYMBOL);
        $('#param_id').val(aData.ID);
        $('#VALIDITY_FROM').val(aData.VALIDITY_FROM);
        $('#VALIDTY_TO').val(aData.VALIDTY_TO); 
    });

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID)
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Currency  " + aData.ID + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Currency/Delete",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('#btn-bataladd').click(function () {
        clear();
    });
    $('#btn-add').click(function () {
        clear();
        $('#txt-judul').text('Add');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            datePicker();
            //resetDatePicker
            //simpanEdit();

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        currency.init();
    });
}