﻿var MasterDataLahan = function () {
    var CBBusinessEntity = function () {
        $("#BE_NAME").select2({
            allowClear: true,
            width: '100%',
            dropdownParent: $("#addapvsetModal"),
            ajax: {
                url: "/MasterDataLahan/GetDataBusinessEntity",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    console.log(result);
                     var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.BE_ID,
                                text: item.BE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: "Business Entity"
        })
    };
    var initTable = function () {
        $('#table-apvset').DataTable({
            "ajax":
            {
                "url": "/MasterDataLahan/GetData",
                "type": "GET",
                //"data": function (data) {
                //    delete data.columns;
                //}

            },
            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "BE_NAME"
                },
                {
                    "data": "TAHUN",
                    "class": "dt-body-center",
                },
                {
                    "data": "LUAS_LAHAN",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0, '', ' m³')
                },
                {
                    "data": "IS_ACTIVE",
                    "class": "dt-body-center",
                    render: function (data, type, full) {

                        let status = "NON ACTIVE";

                        if (full.IS_ACTIVE == "1") {
                            status = "ACTIVE";
                        }

                        return status;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" title="Edit" ><i class="fa fa-edit"></i></a>' +
                            '<a id="btn-delete" class="btn btn-icon-only red" title="Delete" ><i class="fa fa-times"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }
    var simpan = function () {
        $('#btn-simpanadd').click(function () {
            var table = $('#table-apvset').DataTable();
            var ID = parseInt($('#param_id').val());
            var BUSINESS_ENTITY_ID = parseInt($('#BE_NAME').val());
            var TAHUN = parseInt($('#TAHUN').val());
            var LUAS_LAHAN = parseInt($('#LUAS_LAHAN').val());
            var IS_ACTIVE = $('#is_active').is(':checked') ? "1" : "0";

            if (BUSINESS_ENTITY_ID !== "" && TAHUN !== "" && LUAS_LAHAN !== "") {
                var param = {
                    ID: ID,
                    BUSINESS_ENTITY_ID: BUSINESS_ENTITY_ID,
                    TAHUN: TAHUN,
                    LUAS_LAHAN: LUAS_LAHAN,
                    IS_ACTIVE: IS_ACTIVE
                };

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    dataType : "json",
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MasterDataLahan/Insert",
                    timeout: 30000
                });

                //console.log('IDnya : ', ID);

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
            $('#addapvsetModal').modal('hide');
        });
    }

    var clear = function () {
        $('input').val('');
        $("#is_active").prop("checked", false);
    }
    $('body').on('click', 'tr #btn-ubah', function () {
        $('#txt-judul').text('Edit');
        $('#addapvsetModal').modal('show');
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        $('#TAHUN').val(aData.TAHUN);
        $('#LUAS_LAHAN').val(aData.LUAS_LAHAN);
        $('#param_id').val(aData.ID);
        var newOption = new Option(aData.BE_NAME, aData.BE_NAME, false, false);
        $('#BE_NAME').append(newOption).trigger('change');

        if (aData.IS_ACTIVE == '1') {
            $("#is_active").click();
        }          
           
    });

    $('body').on('click', 'tr #btn-delete', function () {
        var oTable = $('#table-apvset').dataTable();
        var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);

        var param = {
            ID: parseInt(aData.ID)
        };

        swal({
            title: 'Warning',
            text: "Are you sure want to delete Master Data Lahan  " + aData.BUSINESS_ENTITY_ID + "?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function (isConfirm) {
            if (isConfirm) {
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MasterDataLahan/Delete",
                    timeout: 30000
                });

                req.done(function (data) {
                    var table = $('#table-apvset').DataTable();
                    App.unblockUI();
                    table.ajax.reload();
                    if (data.Status === 'S') {
                        swal('Berhasil', data.Msg, 'success');
                    } else {
                        swal('Gagal', data.Msg, 'error');
                    }
                });
            }
        });

    });
    $('#btn-bataladd').click(function () {
        clear();
        $('#BE_NAME').select2("val", "");
    });
    $('#btn-add').click(function () {
        clear();
        $('#BE_NAME').select2("val", "");
        $('#txt-judul').text('Add');
    });
    return {
        init: function () {
            initTable();
            simpan();
            clear();
            CBBusinessEntity();
            //simpanEdit();

        }
    };
}();
if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterDataLahan.init();
    });
}