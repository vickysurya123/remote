﻿var Configuration = function () {

    var initTable = function () {
        $('#table-workflow-setting').DataTable({
            "ajax":
            {
                "url": "WorkflowSetting/GetDataWorkflowSetting",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },

            "columns": [
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "OPERATOR_PUSAT_EMAIL",
                    "class": "dt-body-center"
                },
                {
                    "data": "OPERATOR_PUSAT_PHONE",
                    "class": "dt-body-center"
                },
                {
                    "data": "OPERATOR_CABANG_EMAIL",
                    "class": "dt-body-center"
                },
                {
                    "data": "OPERATOR_CABANG_PHONE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn btn-icon-only blue" ><i class="fa fa-edit"></i></a>';
                        aksi += '<a data-toggle="modal" href="#ViewWorkflowSetting" class="btn btn-icon-only green" id="btn-view""><i class="fa fa-eye"></i></a>';
                        //aksi += '<a class="btn btn-icon-only red" id="btn-hapus"><i class="fa fa-close"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var detil = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-be').DataTable();
            var data = table.row(baris).data();

            $("#BE_ID").val(data["BE_ID"]);
            $("#BE_NAME").val(data["BE_NAME"]);
            $("#BE_ADDRESS").val(data["BE_ADDRESS"]);
            $("#POSTAL_CODE").val(data["POSTAL_CODE"]);
            $("#BE_CITY").val(data["BE_CITY"]);
            $("#BE_PROVINCE").val(data["BE_PROVINCE"]);
            $("#VALID_FROM").val(data["VALID_FROM"]);
            $("#VALID_TO").val(data["VALID_TO"]);
            $("#REF_DESC").val(data["REF_DESC"]);
            $("#PHONE_1").val(data["PHONE_1"]);
            $("#FAX_1").val(data["FAX_1"]);
            $("#EMAIL").val(data["EMAIL"]);

            $("#HARBOUR_CLASS").val(data["HARBOUR_CLASS"]);
            $("#PHONE_1").val(data["PHONE_1"]);
            $("#FAX_1").val(data["FAX_1"]);
            $("#PHONE_2").val(data["PHONE_2"]);
            $("#FAX_2").val(data["FAX_2"]);
            $("#EMAIL").val(data["EMAIL"]);
        });
    }

    var ddBE = function () {
        $.ajax({
            type: "GET",
            url: "/WorkflowSetting/getDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += '<option value="">-- Choose Business Entity --</option>';
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BE_WORKFLOW").html('');
                $("#BE_WORKFLOW").append(listItems);
            }
        });
    }

    

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/WorkflowSetting/AddConfig';
        });
    }

    var kembali = function () {
        $('#btn-add').click(function () {
            window.location.href = '/WorkflowSetting/';
        });
    }

    return {
        init: function () {
            initTable();
            add();
            ddBE();
            detil();
            kembali();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Configuration.init();
    });
}