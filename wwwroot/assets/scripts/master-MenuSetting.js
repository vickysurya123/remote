﻿var MenuSetting = function () {

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var nestableHandle = function () {
        /*
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                    output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            }
            else {
                output.val('JSON browser support required for this demo.');
            }
        };

        var json = [
            {
                "id": 1,
                "content": "First item",
                "classes": ["dd-nochildren"]
            },
            {
                "id": 2,
                "content": "Second item",
                "children": [
                    {
                        "id": 3,
                        "content": "Item 3"
                    },
                    {
                        "id": 4,
                        "content": "Item 4"
                    },
                    {
                        "id": 5,
                        "content": "Item 5",
                        "value": "Item 5 value",
                        "foo": "Bar",
                        "children": [
                            {
                                "id": 6,
                                "content": "Item 6"
                            },
                            {
                                "id": 7,
                                "content": "Item 7"
                            },
                            {
                                "id": 8,
                                "content": "Item 8"
                            }
                        ]
                    }
                ]
            },
            {
                "id": 9,
                "content": "Item 9"
            },
            {
                "id": 10,
                "content": "Item 10",
                "children": [
                    {
                        "id": 11,
                        "content": "Item 11",
                        "children": [
                            {
                                "id": 12,
                                "content": "Item 12"
                            }
                        ]
                    }
                ]
            }
        ];

        // activate Nestable list
        $('#nestable').nestable({
            group: 1,
            json: json,
            contentCallback: function (item) {
                var content = item.content || '' ? item.content : item.id;
                content += ' <i>(id = ' + item.id + ')</i>';

                return content;
            }
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target),
                    action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
        */
    }

    var initTable = function () {
        $('#table-ListMenu').DataTable({
            "ajax":
            {
                "url": "/MenuSetting/GetDataMenu",
                "type": "GET",
            },
            "columns": [
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-add" class="btn default btn-xs green" ><i class="fa fa-plus"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "MENU_NAMA"
                },
                {
                    "data": "MENU_URL"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a id="btn-ubah" class="btn default btn-xs blue" data-toggle="modal" href="#editListMenuModal"><i class="fa fa-edit"></i></a> &nbsp; ' +
                                   '<a id="btn-hapus" class="btn default btn-xs red" ><i class="fa fa-trash-o"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "bLengthChange": false,

            "pageLength": 5,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            var rTable = $('#table-ListMenu').DataTable();
            var MENU_NAMA = $('#MenuName').val();
            var MENU_URL = $('#MenuUrl').val();
            var MENU_ALIAS = $('#MenuAlias').val();
            var MENU_ICON = $('#MenuIcon').val();

            if (MENU_NAMA && MENU_URL && MENU_ALIAS && MENU_ICON) {
                var param = {
                    MENU_NAMA: MENU_NAMA,
                    MENU_URL: MENU_URL,
                    MENU_ALIAS: MENU_ALIAS,
                    MENU_ICON: MENU_ICON
                };

                //console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MenuSetting/InstDataMenu",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            rTable.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            rTable.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
        });
    }

    var klikubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var oTable = $('#table-ListMenu').dataTable();
            var nRow = $(this).parents('tr')[0];
            var data = oTable.fnGetData(nRow);

            //console.log(data["MENU_ID"]);
            $("#MenuIdEdit").val(data["MENU_ID"]);
            $("#MenuNameEdit").val(data["MENU_NAMA"]);
            $("#MenuAliasEdit").val(data["MENU_ALIAS"]);
            $("#MenuUrlEdit").val(data["MENU_URL"]);
            $("#MenuIconEdit").val(data["MENU_ICON"]);
        });
    }

    var ubahsimpan = function () {
        $('#btn-updEdit').click(function () {

            var MENU_ID = $('#MenuIdEdit').val();
            var MENU_NAMA = $('#MenuNameEdit').val();
            var MENU_URL = $('#MenuUrlEdit').val();
            var MENU_ALIAS = $('#MenuAliasEdit').val();
            var MENU_ICON = $('#MenuIconEdit').val();

            var rTable = $('#table-ListMenu').DataTable();
            if (MENU_NAMA && MENU_URL && MENU_ALIAS && MENU_ICON) {
                var param = {
                    MENU_ID: MENU_ID,
                    MENU_NAMA: MENU_NAMA,
                    MENU_URL: MENU_URL,
                    MENU_ALIAS: MENU_ALIAS,
                    MENU_ICON: MENU_ICON
                };

                //console.log(param);

                $('#editListMenuModal').modal('hide');
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/MenuSetting/EditDataMenu",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            rTable.ajax.reload();
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            //clear();
                            //rTable.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }
        });
    }

    var hapus = function () {
        $('body').on('click', 'tr #btn-hapus', function () {
            var oTable = $('#table-ListMenu').dataTable();
            var nRow = $(this).parents('tr')[0];
            var data = oTable.fnGetData(nRow);

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var MENU_ID = data["MENU_ID"];

                    var rTable = $('#table-ListMenu').DataTable();
                    if (MENU_ID) {
                        var param = {
                            MENU_ID: MENU_ID
                        };

                        //console.log(param);

                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/MenuSetting/DelDataMenu",
                            timeout: 30000
                        });

                        req.done(function (data) {
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal("Deleted!", data.message, "success").then(function (isConfirm) {
                                    //window.location.href = '/Role';
                                    clear();
                                    rTable.ajax.reload();
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    //window.location.href = '/Role';
                                    //clear();
                                    //rTable.ajax.reload();
                                });
                            }
                        });
                    } else {
                        swal('Peringatan', 'Tidak Dapat Menghapus Menu', 'warning');
                    }
                }
            });
        });
    }

    var bsSelectRoleName = function () {
        $.ajax({
            type: "GET",
            url: "/MenuSetting/getDataDropDownRoleName",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += '<option value="0">-- Choose Role Name --</option>';
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ROLE_ID + "'>" + jsonList.data[i].ROLE_NAME + "</option>";
                }
                $("#roleName").html('');
                $("#roleName").append(listItems);
                handleBootstrapSelect();
            }
        });
    }

    var clear = function () {
        $('input').val('');
    }

    var tombolClear = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var RoleMenuChange = function () {
        $("#roleName").change(function () {
            getnested();
        });
    }

    var getnested = function () {
        var rolename = $('#roleName').val();

        $('#nestable').nestable('destroy');

        // Ajax untuk panggil menu sesuai dengan role menu yg dipilih
        $.ajax({
            type: "GET",
            url: "/MenuSetting/getDataNestable",
            contentType: "application/json",
            data: {
                rolename: rolename
            },
            dataType: "json",
            success: function (data) {
                //console.log(data);
                var jsonList = data;
                var listItems = "";
                var id;
                var menuNama;
                var kelas;
                var json = [];
                var nilai = "";
                var x = "";

                var updateOutput = function (e) {
                    var list = e.length ? e : $(e.target),
                            output = list.data('output');

                    if (window.JSON) {
                        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                        allData = new Array();
                        var x = $('#nestable-output').val();
                        var dX = JSON.parse(x);
                        //console.log(dX);
                        recursiveIteration(dX);
                    }
                    else {
                        output.val('JSON browser support required.');
                    }
                };

                //for (var i = 0; i < jsonList.data.length; i++) {
                //    //id = JSON.stringify(jsonList.data[i].ORDERED_BY);
                //    id = jsonList.data[i].ORDERED_BY;
                //    menuNama = jsonList.data[i].MENU_NAMA;

                //    x = JSON.stringify({ "id": id, "content": menuNama });
                //    json.push(x);
                //}
                //nilai = '[' + json + ']';
                //var data = '[' + json + ']';

                //*Get JSON structure List*//
                var param = "";
                arr = new Array;
                var result = "";
                var resultChild = "";
                var hasilJSON = "";
                var hasilJSONsub = "";

                var getNestedChildren = function (arry, parent) {
                    //console.log(arry);
                    //console.log("--Parent--"+parent);
                    var out = new Array;

                    for (var j in arry) {
                        if (arry[j].parent == parent) {
                            var children = getNestedChildren(arry, arry[j].id)

                            if (children.length) {
                                arry[j].children = children
                            }
                            out.push(arry[j]);

                        }
                    }
                    resultChild = out;
                    hasilJSONsub = JSON.stringify(resultChild);
                    //console.log("getNestedChildren " + hasilJSONsub);
                    return out
                }
                
                for (var i = 0; i < jsonList.data.length; i++) {
                    var obj = jsonList.data[i];

                    var xRoleID = obj.ROLE_ID;
                    var xRoleNAME = obj.ROLE_NAME;
                    var xAppID = obj.APP_ID;
                    var xMenuID = obj.MENU_ID;
                    var xMenuNAMA = obj.MENU_NAMA;
                    var xMenuParentID = obj.MENU_PARENT_ID;
                    var xMenuParentNAMA = obj.MENU_PARENT_NAMA;
                    var xOrderedBY = obj.ORDERED_BY;
                    var xSTATUS = obj.STATUS;

                    param = { id: xMenuID, content: xMenuNAMA, parent: xMenuParentID, ordered: xOrderedBY }
                    arr.push(param);
                    result = arr;
                    hasilJSON = JSON.stringify(result);
                    //console.log(hasilJSON);
                    getNestedChildren(result, result[i].parent);
                }
                //**//

                // activate Nestable list
                $('#nestable').nestable({
                    group: 1,
                    json: hasilJSONsub,
                    contentCallback: function (item) {
                        var content = item.content || '' ? item.content : item.id;
                        //content += ' <i>(id = ' + item.id + ')</i>';
                        return content;
                    }
                }).on('change', updateOutput);//.on('change', updateOutput);
                $('.dd').nestable('collapseAll');

                // output initial serialised data
                updateOutput($('#nestable').data('output', $('#nestable-output')));

                allData = new Array();
                var x = $('#nestable-output').val();
                var dX = JSON.parse(x);
                //console.log(dX);
                recursiveIteration(dX); 

                //expandAll or collapseAll
                $('#nestable-menu').on('click', function (e) {
                    var target = $(e.target),
                            action = target.data('action');
                    if (action === 'expand-all') {
                        $('.dd').nestable('expandAll');
                    }
                    if (action === 'collapse-all') {
                        $('.dd').nestable('collapseAll');
                    }
                });
                //---//
            }
        });
    }

    var allData = [];

    var recursiveIteration = function (object) {
        var ctrX = 0;
        var dataX = [];
        dataX = new Array();
        for (var property in object) {
            if (object.hasOwnProperty(property)) {
                if (typeof object[property] == "object") {
                    var prop = object[property];
                    recursiveIteration(prop);
                } else {
                    //found a property which is not an object, check for your conditions here
                    //console.log(object[property]);
                    
                    if (ctrX == 2) {
                        //for i datax.legth and push to alldata
                        dataX.push(object[property]);
                        
                        //allData[xCount] = {
                        //    xOrder:dataX[0], 
                        //    xParent:dataX[1], 
                        //    xMenu: dataX[2]
                        //}
                        var valueToPush = {
                            DATA1 : dataX[0],
                            DATA2 : dataX[1],
                            DATA3 : dataX[2]
                        }
                        allData.push(valueToPush);

                        //console.log(allData);
                    } else {
                        dataX.push(object[property]);
                        //console.log(dataX);
                        //console.log(ctrX);
                    }
                    ctrX++;
                    //console.log(object[property]);
                    
                }
            }
        }
    }

    
    var showData = function () {
        $('#btn-showAllx').click(function () {
            var xDATA = [];
            swal({
                title: 'Warning',
                text: "Are you sure want to update role menu ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var x2 = (allData.length) / 2;
                    console.log(x2);
                    for (var i = 0; i < allData.length; i++) {
                        var datum = allData[i];
                        xDATA[i] = {
                            xOrder: datum.DATA1,
                            xParent: datum.DATA2,
                            xMenu: datum.DATA3
                        }
                        console.log(xDATA[i]);
                    };
                    //console.log(xDATA);
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(xDATA),
                        method: "POST",
                        url: "MenuSetting/UpdateRoleMenu",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
            //console.log(allData);
            
        });
    }

    return {
        init: function () {
            nestableHandle();
            initTable();
            simpan();
            showData();
            klikubah();
            ubahsimpan();
            hapus();
            RoleMenuChange();
            getnested();
            bsSelectRoleName();
            tombolClear();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MenuSetting.init();
    });
}