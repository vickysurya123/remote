﻿var MonitoringWE = function () {


    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    /*
    var select2BEID = function () {

        function formatRepo(repo) {
            if (repo.loading) return "Processing...";

            var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.BE_NAME + "</div>" +
                            "<div class='select2-result-repository__description'>KODE : " + repo.BE_ID + "</div>" +
                          "</div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            $("#BE_ID").val(repo.BE_ID);
            return repo.BE_MANE;
        }

        $("#BUSINESS_ENTITY").select2({
            width: "off",
            ajax: {
                url: "/ReportBE/getBEID",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    }

    var select2HARBOURCLASS = function () {

        function formatRepo(repo) {
            if (repo.loading) return "Processing...";

            var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.HARBOUR_CLASS + "</div>" +
                          "</div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            $("#HARBOUR_CLASS").val(repo.HARBOUR_CLASS);
            return repo.HARBOUR_CLASS;
        }

        $("#HARBOUR_CLASS").select2({
            width: "off",
            ajax: {
                url: "/ReportBE/getHARBOURClASS",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    } */

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            App.blockUI({ boxed: true });
            var req = null;
            //var txtTerminal = $('#txtTerminal').val();
            var txtKegiatan = $('#txtKegiatan').val();
            var txtPBMNama = $('#select2-txtPBM-container').text();
            var txtPBM = $('#txtPBM').val();
            var txtAsalPortNama = $('#txtAsalKapalNama').val();
            var txtAsalPort = $('#txtAsalKapal').val();
            var txtTujuanPortNama = $('#txtTujuanKapalNama').val();
            var txtTujuanPort = $('#txtTujuanKapal').val();
            var txtTanggal = $('#txtTanggal').val();
            var txtAwalKapalNama = $('#select2-txtAwalKapal-container').text();
            var txtAwalKapal = $('#txtAwalKapal').val();
            var txtTransKapalNama = $('#select2-txtTransKapal-container').text();
            var txtTransKapal = $('#txtTransKapal').val();
            var xTable = $('#table-permohonan').dataTable();
            var xGetData = xTable.fnGetData();
            var xDATA_PMH = new Array(xGetData.length);
            for (i = 0; i < xGetData.length; i++) {
                var xData = xTable.fnGetData(i);

                xDATA_PMH[i] = {
                    XPPKB1_NOMOR: txtAwalKapal,
                    XKD_PBM: txtPBM,
                    XTGL_PMH_WHM: txtTanggal,
                    XKD_PELANGGAN: null,
                    XTIPE_KEGIATAN: txtKegiatan,
                    XKETERANGAN: xData[8],
                    XPORT_ORIGIN: txtAsalPort,
                    XPORT_DESTINATION: txtTujuanPort,
                    XKD_KAPAL_FROM: txtAwalKapal,
                    XKD_KAPAL_TO: txtTransKapal,
                    XKD_DOKUMEN: null,
                    XTIPE_PENUMPUKAN: xData[0],
                    XGUDLAP_ID: xData[1],
                    XKD_BARANG: xData[2],
                    XKD_SATUAN: xData[3],
                    XSIFAT_BARANG: null,
                    XJENIS_KEMASAN: xData[4],
                    XJUMLAH_BARANG: xData[5],
                    XNAMA_KAPAL_FROM: txtAwalKapalNama,
                    XNAMA_KAPAL_TO: txtTransKapalNama,
                    XNAMA_PBM: txtPBMNama,
                    XNAMA_PORT_ORIGIN: txtAsalPortNama,
                    XNAMA_PORT_DESTINATION: txtTujuanPortNama,
                    XKD_ASAL: xData[6],
                    XKD_TUJUAN: xData[7],

                }
            }
            //console.log(xDATA_PMH);
            //console.log(txtAwalKapalNama);
            req = $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(xDATA_PMH),
                method: "POST",
                url: "/PermohonanMasuk/InsertPmhBongkar",
                timeout: 30000
            });
            req.done(function (data) {
                App.unblockUI();
                console.log(data);
                if (data.sts === "S") {
                    clear();
                    swal('Berhasil', "Berhasil Insert Data Permohonan [" + data.msg + "]", 'success');
                } else {
                    swal('Gagal', data.msg, 'error');
                }
            });
        });
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " MENU records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }


    var showFilter = function () {
        $('#btn-filter').click(function () {
            //App.blockUI({ boxed: true });
            var periode = $('#PERIOD').val() + "." + $('#PERIOD2').val();
            var month = $('#PERIOD').val();
            var year = $('#PERIOD2').val();
            var tipe_trans = $('#TYPE_TRANSACTION').val();
            var be = $('#BE_NAME').val();
            var status = $('#STATUS_NOW').val();

            if (month !== "" && year !== "") {

                //    swal('Warning', 'Please Select Years Period !', 'warning');
                $('#row-table').css('display', 'block');
                if ($.fn.DataTable.isDataTable('#table-report')) {
                    $('#table-report').DataTable().destroy();
                }
                $('#table-report').DataTable({
                    "ajax": {
                        "url": "/MonitoringWE/GetDataFilter",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.be = $('#BE_NAME').val();
                            data.periode = $('#PERIOD').val() + "." + $('#PERIOD2').val();;
                            data.tipe_trans = $('#TYPE_TRANSACTION').val();
                            data.status = $('#STATUS_NOW').val();
                            console.log(data);
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak-exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "render": function (data, type, full) {
                                if (full.BE_NAME === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.BE_NAME + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.INSTALLATION_CODE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.INSTALLATION_CODE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.INSTALLATION_TYPE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.INSTALLATION_TYPE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.PERIOD === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.PERIOD + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.PROFIT_CENTER === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.PROFIT_CENTER + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.CUSTOMER_NAME === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.CUSTOMER_NAME + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.POWER_CAPACITY === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.POWER_CAPACITY + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.INSTALLATION_ADDRESS === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.INSTALLATION_ADDRESS + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.POSTING_DATE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.POSTING_DATE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                var str = "";

                                if (full.STATUS_NOW === 'Not Entried') {
                                    str += '<span class="label label-sm label-danger"> ' + full.STATUS_NOW + ' </span>';
                                } else if (full.STATUS_NOW === 'Entried') {
                                    str += '<span class="label label-sm label-info"> ' + full.STATUS_NOW + ' </span>';
                                    if (full.STATUS_POSTING === 'Posted') {
                                        str += '<span class="label label-sm label-info"> ' + full.STATUS_POSTING + ' </span>';
                                    } else if (full.STATUS_POSTING === 'Cancel') {
                                        str += '<span class="label label-sm label-danger"> ' + full.STATUS_POSTING + ' </span>';
                                    } else {
                                        str += '<span class="label label-sm label-warning"> ' + full.STATUS_POSTING + ' </span>';
                                    }
                                }
                                return str;
                                //if (full.STATUS_NOW === null) {
                                //    return '<span> - </span>';
                                //} else {
                                //    return '<span> ' + full.STATUS_NOW + ' </span>';
                                //}
                            },
                            "class": "dt-body-center"
                        },

                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,

                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],
                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true

                });
                //App.unblockUI();
                $('#btn-cetak_exel').show('slow');
            }
            else {
                swal('Warning', 'Please Select Month and Years Period !', 'warning');
            }

        });
    }

    return {
        init: function () {
            //select2BEID();
            //select2HARBOURCLASS();
            dataTableResult();
            showFilter();
            handleBootstrapSelect();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var BE_NAME = $('#BE_NAME').val();
    var TYPE_TRANSACTION = $('#TYPE_TRANSACTION').val() == undefined ? null : $('#TYPE_TRANSACTION').val();
    var PERIOD = $('#PERIOD').val() + "." + $('#PERIOD2').val() == undefined ? null : $('#PERIOD').val() + "." + $('#PERIOD2').val();
    var STATUS_NOW = $('#STATUS_NOW').val() == undefined ? null : $('#STATUS_NOW').val();

    var dataJSON = {
        BE_NAME: BE_NAME,
        TYPE_TRANSACTION: TYPE_TRANSACTION,
        PERIOD: PERIOD,
        STATUS_NOW: STATUS_NOW
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/MonitoringWE/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            //console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/MonitoringWE/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MonitoringWE.init();
    });
}

jQuery(document).ready(function () {
    //$('#table-report > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}