﻿$("#METER_TO").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

$("#METER_FROM").inputmask({
    mask: "9",
    repeat: 10,
    greedy: !1
});

var TransWaterElectricityAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWE";
        });
    }

    var hitung = function () {
        $('#SURCHARGE').mask('000.000.000.000.000', { reverse: true });
        $('#PRICE').mask('000.000.000.000.000', { reverse: true });
        var currency = $('#CURRENCY').val();
            $('#METER_TO').on('change', function (e) {
                
                var prc = $('#PRICE').val();
                var nfPrc = parseFloat(prc.split('.').join(''));

                var surcharge = $('#SURCHARGE').val();

                //var m_factor = $('#MULTIPLY_FACTOR').val();
                var total = 0;

                var to = $('#METER_TO').val();
                var from = $('#METER_FROM').val();
                var qtyUsed = Number(to) - Number(from);
                var minQty = $('#MIN_QUANTITY').val();

                if (qtyUsed <= minQty) {
                    total = Number(minQty) * Number(nfPrc);
                }
                else {
                    total = Number(nfPrc) * Number(qtyUsed);
                }

                var nfSurcharge = parseFloat(surcharge.split('.').join(''));
                var totalBulat = Math.round(total);
                var grandTotal = Number(totalBulat) + Number(nfSurcharge);
                console.log(grandTotal);
                //var grandTotal = totalBulat;

                if (currency == 'USD') {
                    $.ajax({
                        type: "GET",
                        url: "/TransContractOffer/GetDataCurrencyRate",
                        contentType: "application/json",
                        dataType: "json",
                        data: {},
                        success: function (data) {
                            rate = data.Data;
                            totalBulatUsd = totalBulat * rate;
                            grandTotalUsd = grandTotal * rate;
                            var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            var fTotalBulatUsd = totalBulatUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                            $("#AMOUNT").val(fTotalBulatUsd);
                            $("#GRAND_TOTAL").val(fGrandTotalUsd);
                            $("#SUB_TOTAL_USD").val(fTotalBulat);
                            $("#GRAND_TOTAL_USD").val(fGrandTotal);
                            $('#USD').show();
                            $('#RATE').val(rate);
                        }
                    });
                } else {
                    var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    $("#AMOUNT").val(fTotalBulat);
                    $("#GRAND_TOTAL").val(fGrandTotal);
                    $('#USD').hide();
                    $('#RATE').val(1);
                }

                $("#KWH").val(qtyUsed);
            });

            $('#SURCHARGE').on('keyup', function (e) {
                var surcharge = $('#SURCHARGE').val();
                var nfSurcharge = parseFloat(surcharge.split('.').join(''));
                var amount = $('#AMOUNT').val();
                var nfAmount = parseFloat(amount.split('.').join(''));

                var grandTotal = Number(nfSurcharge) + Number(nfAmount);
                //var grandTotal = Number(nfAmount);
                if (currency == 'USD') {
                    $.ajax({
                        type: "GET",
                        url: "/TransContractOffer/GetDataCurrencyRate",
                        contentType: "application/json",
                        dataType: "json",
                        data: {},
                        success: function (data) {
                            rate = data.Data;
                            grandTotalUsd = grandTotal * rate;
                            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                            $("#GRAND_TOTAL").val(fGrandTotalUsd);
                            $("#GRAND_TOTAL_USD").val(fGrandTotal);
                            $('#USD').show();
                            $('#RATE').val(rate);
                        }
                    });
                } else {
                    var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    $("#GRAND_TOTAL").val(fGrandTotal);
                    $('#USD').hide();
                    $('#RATE').val(1);
                }
            });
        }

        var cekPiut = function (doc_type, cust_code) {
            var param = {
                DOC_TYPE: doc_type == undefined ? "1M": doc_type ,
                CUST_CODE: cust_code == undefined ? $('#CUSTOMER_MDM').val() : cust_code,
            };
            $.ajax({
                type: "POST",
                url: "/TransRentalRequest/cekPiutangSAP",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.E_RELEASE_STATUS == "X") {
                        $("#cPiut").val(data.E_RELEASE_STATUS);
                        swal('Warning', data.E_DESCRIPTION, 'error');
                        //return data.E_RELEASE_STATUS;
                    }
                    else {
                        $("#cPiut").val("1");
                    }
                }
            });
        }

        var simpan = function () {
            $('#btn-simpan').click(function () {

                var cekP = $('#CUSTOMER_MDM').val();
                cekPiut("1M", cekP);
                var xPiut = $("#cPiut").val();

                console.log("xPiut : " + xPiut);

                console.log("Customer MDM : " + CUSTOMER_MDM)
                console.log("Customer : " + CUSTOMER)
                console.log("cekp : " + cekP)
                

                var ID_INSTALASI = $('#ID_INSTALASI').val();
                var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
                var PROFIT_CENTER = $('#PROFIT_CENTER').val();
                var CUSTOMER = $('#CUSTOMER').val();
                var CUSTOMER_MDM = $('#CUSTOMER_MDM').val();
                var PERIOD = $('#PERIOD').val();
                var METER_FROM = $('#METER_FROM').val();
                var METER_TO = $('#METER_TO').val();
                var PRICE = $('#PRICE').val();
                //var MULTIPLY_FACTOR = $('#MULTIPLY_FACTOR').val();
                var QUANTITY = $('#KWH').val();
                var AMOUNT = $('#AMOUNT').val();
                var RATE = $('#RATE').val();
                var UNIT = $('#UNIT').val();
                var INSTALLATION_CODE = $('#INSTALLATION_CODE').val();
                var MINIMUM_AMOUNT = $('#MIN_QUANTITY').val();
                var SURCHARGE = $('#SURCHARGE').val();
                var SUB_TOTAL = $('#SUB_TOTAL').val();
                var COA_PROD = $('#coa_prod').val();
                var STATUS_DINAS = $('#STATUS_DINAS').val();

                //var REPORT_ON = $('#REPORT_ON').val();
                var INSTALLATION_NUMBER = $('#INSTALLATION_NUMBER').val();                
                
                if (Number(METER_TO) < Number(METER_FROM)) {
                    swal('Failed', 'Check Your Meter To Value', 'error')
                }
                else {
                    if (METER_FROM && METER_TO) {
                        var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                        var nfSURCHARGE = 0;
                        nfSURCHARGE = parseFloat(SURCHARGE.split('.').join(''));
                        console.log('nf surcharge ' + nfSURCHARGE);
                        var grandTOTAL = Number(nfAmount) + Number(nfSURCHARGE);
                        console.log('grand total ' + grandTOTAL);
                        var nfSubTotal = parseFloat(AMOUNT.split('.').join(''));
                        var nfPrice = parseFloat(PRICE.split('.').join(''));
                        console.log('price ' + nfPrice);
                  
                        var param = {
                            ID_INSTALASI: ID_INSTALASI,
                            INSTALLATION_TYPE: INSTALLATION_TYPE,
                            PROFIT_CENTER: PROFIT_CENTER,
                            CUSTOMER_NAME: CUSTOMER,
                            CUSTOMER:CUSTOMER_MDM,
                            PERIOD: PERIOD,
                            METER_FROM: METER_FROM,
                            METER_TO: METER_TO,
                            PRICE: nfPrice.toString(),
                            //MULTIPLY_FACTOR: MULTIPLY_FACTOR,
                            QUANTITY: QUANTITY,
                            //AMOUNT: nfAmount,
                            AMOUNT: nfAmount.toString(),
                            UNIT: UNIT,
                            RATE: RATE,
                            INSTALLATION_NUMBER: INSTALLATION_NUMBER,
                            INSTALLATION_CODE: INSTALLATION_CODE,
                            SURCHARGE: nfSURCHARGE.toString(),
                            SUB_TOTAL: nfSubTotal.toString(),
                            COA_PROD: COA_PROD,
                            STATUS_DINAS: STATUS_DINAS,
                            GRAND_TOTAL: grandTOTAL.toString()
                            //REPORT_ON: REPORT_ON,
                        };

                        console.log(param);

                        console.log("INI COA PROD : " + COA_PROD);


                        if (COA_PROD == '' || COA_PROD == null) {
                            console.log("coa kosong");
                            swal('Warning', 'Harap mengisi Coa Produksi', 'warning');
                        }
                        else if (PERIOD == '' || PERIOD == null) {
                            console.log("period kosong");
                            swal('Warning', 'Harap mengisi Period', 'warning');
                        }
                        else if (STATUS_DINAS == '' || STATUS_DINAS == null) {
                            console.log("status dinas kosong");
                            swal('Warning', 'Harap mengisi Status Dinas', 'warning');
                        }
                        else if (xPiut == "X") {
                            swal('Warning', 'Punya piutang', 'warning');

                        }
                        else {
                            console.log("coa ada");
                            App.blockUI({ boxed: true });
                            var req = $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "/TransWE/AddData",
                                timeout: 30000
                            });

                            req.done(function (data) {
                                App.unblockUI();
                                if (data.status === 'S') {
                                    swal('Success', data.message, 'success').then(function (isConfirm) {
                                        window.location = "/TransWE";
                                    });
                                } else {
                                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                                        window.location = "/TransWE";
                                    });
                                }
                            });
                            //if (STATUS_DINAS == 'KEDINASAN') {
                            //    console.log("kedinasan");
                            //    App.blockUI({ boxed: true });
                            //    var req = $.ajax({
                            //        contentType: "application/json",
                            //        data: "{ BILL_ID:'" + data["ID"] + "' , BILL_TYPE: '" + data["BILLING_TYPE"] + "', POSTING_DATE: '" + POSTING_DATE + "'}",
                            //        method: "POST",
                            //        url: "TransWEBilling/postToSAP"
                            //    });
                            //}
                        }
                    }
                 else {
                        swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    }
                }
                
            });
        }

        // Validasi Period
        var validasiPeriod = function () {
            $('#PERIOD').change(function () {
                var period_terpilih = $('#PERIOD').val();

                var idInstalasi = $('#ID_INSTALASI').val();
                var installationCode = $('#INSTALLATION_CODE').val();

                $.ajax({
                    type: "GET",
                    url: "/TransWE/getLastPeriod",
                    contentType: "application/json",
                    dataType: "json",
                    data: { id_instalasi: idInstalasi, periode: period_terpilih },
                    success: function (data) {
                        var jsonList = data
                        var listItems1 = "";

                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems1 += jsonList.data[i].PERIOD;
                        }

                        if (listItems1 != '') {
                            swal('Info', 'Sudah Ada Transaksi Untuk Periode ' + period_terpilih + '!', 'info');
                        }
                    }
                });
            })
        }

        return {
            init: function () {
                getCoaProduksi();
                batal();
                hitung();
                simpan();
                validasiPeriod();
                cekPiut();
                cariCoa();
            }
        };
}();

var cariCoa = function () {
    //$('#btn-filter-coa').click(function () {
    if ($.fn.DataTable.isDataTable('#table-coa')) {
        $('#table-coa').DataTable().destroy();
    }
    $('#table-coa').DataTable({

        "ajax":
        {
            "url": "/TransElectricity/GetListCoa",
            "type": "GET",
            "data": function (data) {
                delete data.columns;
            },
            "dataSrc": function (response) {
                var data = {};
                data.draw = response.paging.page;
                data.recordsTotal = response.paging.total;
                data.recordsFiltered = response.paging.total;
                data.data = response.result;
                return data.data;
            },
        },
        "columns": [
            {
                "render": function () {
                    var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                    return aksi;
                },
                "class": "dt-body-center",
            },
            {
                "data": "COA_PROD",
                "class": "dt-body-center",
            },
            {
                "data": "DESKRIPSI",
            },
            {
                "data": "GL_TEXT",
            },

        ],
        "destroy": true,
        "ordering": false,
        "processing": true,
        "serverSide": true,

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],

        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
        },

        "filter": true
    });
    //});

    //choose coa
    $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
        var baris = $(this).parents('tr')[0];
        var table = $('#table-coa').DataTable();
        var data = table.row(baris).data();
        var c_data = null;
        var value = data['COA_PROD'];
        $('#coa_prod').val(value);
        console.log(value);

        $('#coaModal').modal('hide');
    });
}



    if (App.isAngularJsApp() === false) {
        jQuery(document).ready(function () {
            TransWaterElectricityAdd.init();
        });
    }

    function getCoaProduksi() {
        $.ajax({
            type: "GET",
            url: "/TransContractOffer/GetDataDropDownCoaProduksiWater",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose COA Produksi --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#coa_prod").html('');
                $("#coa_prod").append(listItems);
            }
        });
    }
