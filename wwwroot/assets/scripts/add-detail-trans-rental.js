﻿var DetailTransRentalRequest = function () {

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                endDate: "Date.now()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;
                
                $.ajax({
                    type: "POST",
                    url: "/TransRentalRequest/GetDataCustomer",
                    data: "{ MPLG_NAMA:'" + inp + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
                cekPiut("1D", ui.item.sap);
            }
        });
    }

    var cekPiut = function (doc_type, cust_code) {
        $.ajax({
            type: "POST",
            url: "/TransRentalRequest/cekPiutangSAP",
            data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var table = $('#editable_transrental');
    var oTable = table.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "columnDefs": [
            {
                "targets": [0, 2],
                "class": "dt-body-center",
                "visible": false
            },
            {
                "targets": [1],
                "class": "dt-body-center"
            },
            {
                "targets": [5, 6],
                "class": "dt-body-center"
            },
            {
                "targets": [8],
                "class": "dt-body-center",
                "width": "10%"
            },
        ]
    });

    $('#btn-detil-filter').click(function () {
        $('#table-filter-detail').DataTable().destroy();
        $('#table-filter-detail > tbody').html('');
        $('.sembunyi').hide();
        
    });

    
    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

           if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
               $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        data.from = $('#from').val();
                        data.to = $('#to').val();
                        data.luas_tanah_from = $('#luas_tanah_from').val();
                        data.luas_tanah_to = $('#luas_tanah_to').val();
                        data.luas_bangunan_from = $('#luas_bangunan_from').val();
                        data.luas_bangunan_to = $('#luas_bangunan_to').val();
                        console.log(data);
                    }
                    
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
        // Add event listener for opening and closing details

        var industry = function () {

            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDropDownIndustry",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                    }
                    $("#INDUSTRY").append(listItems);
                }
            });
        }
        
        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();
            //var xInd = $('#INDUSTRY').val();
            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);
            industry();
            $('#addDetailModal').modal('hide');
        });

        

        $('#editable_transrental').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });
        $('#editable_transrental').on('click', 'tr #btn-savecus', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data
            var xInd = $('#INDUSTRY').val();
            $('#editable_transrental').dataTable().fnUpdate(xInd, baris, 7, false);       
        });
    }

    var simpan = function () {
        $('#btn-update').click(function () {

            var table = $('#editable_transrental').dataTable();
            if (table.fnSettings().aoData.length === 0) {
                swal('Info', 'No data available in rental request object!', 'info');
            } else {
                //alert('data exists!');

                //Pengecekan save kecil
                var DTRental = $('#editable_transrental').dataTable();
                var all_row = DTRental.fnGetNodes();
                for (var i = 0; i < all_row.length; i++) {
                    var cek = $(all_row[i]).find('select[name="INDUSTRY"]').val();
                }
                console.log('cek ' + cek);

                if (cek) {
                    swal('Warning', 'Please Choose Industry Type And Click Check Button Before You Save!', 'warning');
                    //console.log('warning');
                }
                else {
                    //console.log('jalankan insert');
                    var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
                    var BE_ID = $('#BE_ID').val();
                    var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
                    var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
                    var CUSTOMER_ID = $('#CUSTOMER_ID').val();
                    var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
                    var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
                    var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
                    var CUSTOMER_AR = $('#CUSTOMER_AR').val();
                    var OLD_CONTRACT = $('#OLD_CONTRACT').val();
                    var arrObjects;
                    if (RENTAL_REQUEST_TYPE && BE_ID && RENTAL_REQUEST_NAME && CUSTOMER_ID && CUSTOMER_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && CUSTOMER_AR && CONTRACT_USAGE) {
                        arrObjects = new Array();
                        var countDt = table.fnGetData();
                        //var grandtotal = 0;
                        for (var i = 0; i < countDt.length; i++) {
                            var item = table.fnGetData(i);
                            var param2 = {
                                RO_NUMBER: item[0],
                                OBJECT_ID: item[1],
                                OBJECT_TYPE_ID: item[2],
                                BUILDING_DIMENSION: item[5],
                                LAND_DIMENSION: item[6],
                                INDUSTRY: item[7]
                            };
                            arrObjects.push(param2);
                        }

                        var param = {
                            RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                            BE_ID: BE_ID,
                            RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                            CONTRACT_USAGE: CONTRACT_USAGE,
                            CUSTOMER_ID: CUSTOMER_ID,
                            CUSTOMER_NAME: CUSTOMER_NAME,
                            CONTRACT_START_DATE: CONTRACT_START_DATE,
                            CONTRACT_END_DATE: CONTRACT_END_DATE,
                            CUSTOMER_AR: CUSTOMER_AR,
                            OLD_CONTRACT: OLD_CONTRACT,
                            Objects: arrObjects
                        };

                        var cek = $("#CUSTOMER_AR").val();
                        cekPiut("1M", cek);
                        var xPiut = $("#cPiut").val();
                        console.log(xPiut);
                        if (xPiut != "X") {
                            var document_number = "";
                            App.blockUI({
                                boxed: true
                            });
                            $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "/TransRentalRequest/SaveHeader",
                                timeout: 30000,

                                success: function (data) {
                                    var idHeader = data.trans_id;
                                    App.unblockUI();
                                    if (data.status === 'S') {
                                        swal('Success', 'Rental Request Transaction Added Successfully with Transaction Number:' + idHeader, 'success').then(function (isConfirm) {
                                            window.location = "/TransRentalRequest";
                                        });
                                    } else {
                                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                                            window.location = "/TransRentalRequest";
                                        });
                                    }
                                    arrObjects = [];

                                    //for (var i = 0; i < count_dt.length; i++) {
                                    //    var item = table.fnGetData(i);
                                    //    var param2 = {
                                    //        RENTAL_REQUEST_NO: id_header,
                                    //        RO_NUMBER: item[0],
                                    //        OBJECT_ID: item[1],
                                    //        OBJECT_TYPE_ID: item[2],
                                    //        BUILDING_DIMENSION: item[5],
                                    //        LAND_DIMENSION: item[6],
                                    //        INDUSTRY: item[7]
                                    //    };

                                    //    $.ajax({
                                    //        contentType: "application/json",
                                    //        data: JSON.stringify(param2),
                                    //        method: "POST",
                                    //        url: "/TransRentalRequest/SaveDetail",
                                    //        timeout: 30000,
                                    //        success: function (data) {
                                    //            console.log(data);
                                    //            App.unblockUI();
                                    //            if (data.status === 'S') {
                                    //                swal('Success', 'Rental Request Transaction Added Successfully with Transaction Number:' + id_header, 'success').then(function (isConfirm) {
                                    //                    window.location = "/TransRentalRequest";
                                    //                 });
                                    //             } else {
                                    //                 swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    //                     window.location = "/TransRentalRequest";
                                    //                 });
                                    //             }
                                    //        },
                                    //        error: function (data) {
                                    //            console.log(data.responeText);
                                    //        }

                                    //    });
                                    //}
                                },
                                error: function (data) {
                                    swal('Failed', data.message, 'error').then(function (isConfirm) {
                                        window.location = "/TransRentalRequest";
                                    });
                                    App.unblockUI();
                                    console.log(data.responeText);
                                }
                            });
                        }

                    } // END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM
                    else {
                        swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    }

                }//end of cek save kecil   
            }
      
       });
    }

    var update = function () {
        $('#btn-simpan').click(function () {
            var ID = $('#ID').val();
            var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
            var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
            var BE_ID = $('#BE_ID').val();
            var RENTAL_REQUEST_NAME = $('#RENTAL_REQUEST_NAME').val();
            var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var CUSTOMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            var OLD_CONTRACT = $('#OLD_CONTRACT').val();

            if (Date.parse(CONTRACT_START_DATE) > Date.parse(CONTRACT_END_DATE)) {
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (RENTAL_REQUEST_TYPE && BE_ID && RENTAL_REQUEST_NAME && CONTRACT_USAGE && CUSTOMER_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && CUSTOMER_ID && CUSTOMER_AR)
                {
                    var param = {
                        ID: ID,
                        RENTAL_REQUEST_TYPE: RENTAL_REQUEST_TYPE,
                        BE_ID: BE_ID,
                        RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                        CONTRACT_USAGE: CONTRACT_USAGE,
                        CUSTOMER_NAME: CUSTOMER_NAME,
                        CONTRACT_START_DATE: CONTRACT_START_DATE,
                        CONTRACT_END_DATE: CONTRACT_END_DATE,
                        CUSTOMER_ID: CUSTOMER_ID,
                        CUSTOMER_AR: CUSTOMER_AR,
                        OLD_CONTRACT: OLD_CONTRACT
                        
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransRentalRequest/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location.href = '/TransRentalRequest';
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                //window.location.href = '/TransRentalRequest';
                            });
                            App.unblockUI();
                        }
                    });
                } else {
                    App.blockUI();
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    App.unblockUI();
                }
            }


        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransRentalRequest";
        });
    }

    var ambil_tanggal = function () {
        $('#btn-detil-filter').click(function () {
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();

            $('#from').val(start);
            $('#to').val(end);
        });
    }

    return {
        init: function () {
            //add();
            simpan();
            datePicker();
            mdm();
            filter();
            cekPiut();
            batal();
            ambil_tanggal();
            update();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        DetailTransRentalRequest.init();
    });
}

function check() {
    var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
    var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();

    if (CONTRACT_START_DATE == "" && CONTRACT_END_DATE == "")
    {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_START_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_END_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else {
        document.getElementById("btn-detil-filter").href = "#addDetailModal";
    }
}