﻿var Installation = function () {

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/Installation/AddInstallation';
        });
    }

    var mdm = function () {
        $('#CUSTOMER_MDM').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/Installation/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_IDX").val(ui.item.code);
                $("#CUSTOMER_SAP_AR").val(ui.item.sap);
                $("#INSTALLATION_ADDRESS").val(ui.item.address);

            }
        });
    }

    var kembali = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/Installation';
        });
    }

    var initTableInstallation = function () {
        $('#table-installation').DataTable({
            "ajax": {
                "url": "Installation/GetDataInstallation",
                "type": "GET",
            },
            "columns": [
                {
                    "data": "INSTALLATION_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE",
                },
                {
                    "data": "PROFIT_CENTER",
                },
                {
                    "data": "RO_CODE",
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "TARIFF_CODE",
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                        aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var CUSTOMER_ID = $('#CUSTOMER_IDX').val();
            var CUSTOMER_NAME = $('#CUSTOMER_MDM').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR').val();
            var INSTALLATION_DATE = $('#INSTALLATION_DATE').val();
            //var POWER_CAPACITY = $('#POWER_CAPACITY').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS').val();
            var TARIFF_CODE = $('#TARIFF_CODE').val();
            var MINIMUM_AMOUNT = $('#MINIMUM_AMOUNT').val();
            var RO_NUMBER = $('#RO_NUMBER').val();

            if (INSTALLATION_TYPE && PROFIT_CENTER && CUSTOMER_ID && INSTALLATION_DATE && INSTALLATION_ADDRESS && TARIFF_CODE) {
                var param = {
                    INSTALLATION_TYPE: INSTALLATION_TYPE,
                    PROFIT_CENTER: PROFIT_CENTER,
                    CUSTOMER_ID: CUSTOMER_ID,
                    CUSTOMER_NAME: CUSTOMER_NAME,
                    CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                    INSTALLATION_DATE: INSTALLATION_DATE,
                    //POWER_CAPACITY: POWER_CAPACITY,
                    INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                    TARIFF_CODE: TARIFF_CODE,
                    MINIMUM_AMOUNT: MINIMUM_AMOUNT,
                    RO_NUMBER: RO_NUMBER
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/Installation/AddData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/Installation";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/Installation";
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();
            window.location = "/Installation/EditInstallation/" + data["INSTALLATION_NUMBER"];
        });
    }

    var detail = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();
            console.log(data);
            $("#DETILINSTALLATION_CODE").val(data["INSTALLATION_CODE"]);
            $("#DETILINSTALLATION_TYPE").val(data["INSTALLATION_TYPE"]);
            $("#DETILBE_ID").val(data["PROFIT_CENTER"]);
            $("#DETILCUSTOMER_MDM").val(data["CUSTOMER_NAME"]);
            $("#DETILCUSTOMER_ID").val(data["CUSTOMER_ID"]);
            $("#DETILCUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            $("#DETILINSTALLATION_DATE").val(data["INSTALLATION_DATE"]);
            $("#DETILPOWER_CAPACITY").val(data["POWER_CAPACITY"]);
            $("#DETILINSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETILTARIFF_CODE").val(data["TARIFF_CODE"]);
            $("#DETILTAX_CODE").val(data["TAX_CODE"]);
            $("#DETILMINIMUM_AMOUNT").val(data["MINIMUM_AMOUNT"]);
            $("#DETILCURRENCY").val(data["CURRENCY"]);
            $("#DETILRO_NUMBER").val(data["RO_CODE"]);
        });
    }

    var status = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-installation').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["INSTALLATION_NUMBER"],
                        method: "get",
                        url: "Installation/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var lookupCustomer = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('customer'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        remote: {
            url: '/Installation/GetCustomer?query=%QUERY',
            ajax: {
                dataType: 'jsonp'
            },
            wildcard: '%QUERY'
        }
    });

    lookupCustomer.initialize();

    if (App.isRTL()) {
        $('#CUSTOMER_ID').attr("dir", "rtl");
    }
    $('#CUSTOMER_ID').typeahead(null, {
        name: 'dalookup_customer',
        displayKey: 'MPLG_NAMA',
        source: lookupCustomer.ttAdapter(),
        hint: (App.isRTL() ? false : true),
        templates: {
            suggestion: Handlebars.compile([
              '<div class="media">',

                    '<div class="media-body">',
                        '<h4 class="media-heading">{{MPLG_KODE}}</h4>',
                        '<p>{{MPLG_KODE}}-{{MPLG_NAMA}}</p>',
                    '</div>',
              '</div>',
            ].join(''))
        }
    });

    var changeInstalasi = function () {
        $('#PROFIT_CENTER').on('change', function () {
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var p = {
                PROFIT_CENTER: PROFIT_CENTER
            }

            $.ajax({
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify(PROFIT_CENTER.toString()),
                method: "POST",
                url: "/Installation/GetDataDropDownTarifA",
                timeout: 30000,
                //type: "GET",
                //url: "/Installation/GetDataDropDownTarifA",
                //contentType: "application/json",
                //dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    listItems += '<option value="">-- Choose Tariff Code --</option>';
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].TARIFF_CODE + "'>" + jsonList.data[i].AMOUNT + "</option>";
                    }
                    $("#TARIFF_CODE").html('');
                    $("#TARIFF_CODE").append(listItems);
                }
            });
        });
    }

    return {
        init: function () {
            add();
            kembali();
            initTableInstallation();
            detail();
            status();
            simpan();
            ubah();
            mdm();
            changeInstalasi();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        Installation.init();
    });
}