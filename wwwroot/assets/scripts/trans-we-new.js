﻿var TransWaterElectricityAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWE";
        });
    }
    var hitung = function () {
        $('#METER_TO').on('change', function (e) {
            var prc = $('#PRICE').val();
            var m_factor = $('#MULTIPLY_FACTOR').val();

            var to = $('#METER_TO').val();
            var from = $('#METER_FROM').val();
            var kwh = Number(to) - Number(from);

            var total = Number(prc) * Number(m_factor) * Number(kwh);
            var totalBulat = Math.round(total);

            var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#AMOUNT").val(fTotalBulat);
            $("#KWH").val(kwh);
        });
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            var ID_INSTALASI = $('#ID_INSTALASI').val();
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var CUSTOMER = $('#CUSTOMER').val();
            var PERIOD = $('#PERIOD').val();
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var PRICE = $('#PRICE').val();
            var MULTIPLY_FACTOR = $('#MULTIPLY_FACTOR').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#AMOUNT').val();
            var UNIT = $('#UNIT').val();
            //var REPORT_ON = $('#REPORT_ON').val();
            var INSTALLATION_NUMBER = $('#INSTALLATION_NUMBER').val();

            if (Number(METER_TO) < Number(METER_FROM)) {
                swal('Failed', 'Check Your Meter To Value', 'error')
            }
            else {
                if (METER_FROM && METER_TO) {
                    var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                    var param = {
                        ID_INSTALASI: ID_INSTALASI,
                        INSTALLATION_TYPE: INSTALLATION_TYPE,
                        PROFIT_CENTER: PROFIT_CENTER,
                        CUSTOMER: CUSTOMER,
                        PERIOD: PERIOD,
                        METER_FROM: METER_FROM,
                        METER_TO: METER_TO,
                        PRICE: PRICE,
                        MULTIPLY_FACTOR: MULTIPLY_FACTOR,
                        QUANTITY: QUANTITY,
                        AMOUNT: nfAmount,
                        UNIT: UNIT,
                        //REPORT_ON: REPORT_ON,
                        INSTALLATION_NUMBER: INSTALLATION_NUMBER
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransWE/AddData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/TransWE";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransWE";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransWENew/AddTransWENew';
        });
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            //$('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "GetDataFilter",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "RENTAL_REQUEST_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });


        var filter = function () {
        $('#btn-filter').click(function () {
            //$('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "GetDataFilter",
                    "type": "GET"
                },
                "columns": [
                    {
                        "data": "RENTAL_REQUEST_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    return {
        init: function () {
            batal();
            hitung();
            simpan();
            add();
            filter();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricityAdd.init();
    });
}
