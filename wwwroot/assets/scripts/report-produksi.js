var ReportProduksi = function () {
    
    var dataTableResult = function () {
        var table = $('#table-report');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false
        });
    }
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }
    var filter = function () {
        $('#btn-filter').click(function () {

            var PERIOD = $('#PERIOD').val();
            var BE = $('#BE_NAME').val();

            console.log(PERIOD);
            //console.log(POSTING_DATE_TO);

            if (PERIOD) {
                $('#row-table').css('display', 'block');
                //if ($.fn.DataTable.isDataTable('#table-report')) {
                //    $('#table-report').DataTable().destroy();
                //}
                $('#table-report').DataTable({
                    "ajax": {
                        "url": "ReportProduksi/ShowFilterNew",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.period = $('#PERIOD').val();
                            data.be = $('#BE_NAME').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak_exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {
                            "data": "NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CONTRACT_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "BILLING_NO",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CUSTOMER_NAME",
                            "class": "dt-body-left"
                        },
                        {
                            "data": "PROFIT_CENTER",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_CODE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "RO_NAME",
                            "class": "dt-body-left"
                        },
                        {
                            "data": "CONTRACT_START_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "CONTRACT_END_DATE",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "LUAS",
                            "class": "dt-body-right"
                        },
                        {
                            "data": "SATUAN",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "COA_PROD",
                            "class": "dt-body-center"
                        }

                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,
                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],
                    "pageLength": 10,
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },
                    "filter": false
                });
            }
            else {
                swal('Warning', 'Please Select Period and Business Entity!', 'warning');
            }
        });
    }
    return {
        init: function () {
            dataTableResult();
            handleDatePickers();
            filter();
            $("#PERIOD").datepicker({
                format: "mm.yyyy",
                viewMode: "months",
                minViewMode: "months",
                autoclose:true
            });
        }
    };
}();



var fileExcels;
function defineExcel() {
    var PERIOD = $('#PERIOD').val();
    var BE_ID = $('#BE_NAME').val();

    var dataJSON = {
        PERIOD: PERIOD,
        BE_ID: BE_ID,
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "ReportProduksi/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportTransWater/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

jQuery(document).ready(function () {
    $('#table-report > tbody').html('');
});

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportProduksi.init();
    });
}
