﻿var TableDatatablesEditable = function () {

    //$('#surcharge').mask('000.000.000.000.000', { reverse: true });

    //FUNCTION SUM
    jQuery.fn.dataTable.Api.register('sum()', function () {
        return this.flatten().reduce(function (a, b) {
            if (typeof a === 'string') {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if (typeof b === 'string') {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }
 
            return a + b;
        }, 0);
    });

    var janganEnter = function () {
        $('html').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    }

    var handleTable = function () {


        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input class="form-control" id="mask_date1" type="text" />';
            jqTds[1].innerHTML = '<input class="form-control" id="mask_date2" type="text" />';
            //jqTds[2].innerHTML = ' <select class="js-example-basic-single"><option value="AL">Alabama</option><option value="WY">Wyoming</option></select>';
            jqTds[2].innerHTML = '<input type="text" id="service-code" name="service-code" class="form-control nama-service-code2">';

            //jqTds[2].innerHTML = '<select id="select2-button-addons-single-input-group-sm" class="form-control nama-service-code2"></select>';
            jqTds[3].innerHTML = '<input id="txt-service-name" type="text" class="form-control service_name" disabled >';
            jqTds[4].innerHTML = '<input type="text" id="currency" class="form-control currency" disabled>';
            jqTds[5].innerHTML = '<input id="price" type="text" class="form-control price" disabled>';
            jqTds[6].innerHTML = '<input id="multiply_factor"  type="text" class="form-control multiply_factor" disabled>';
            jqTds[7].innerHTML = '<input id="qty" type="text" class="form-control qty">';
            jqTds[8].innerHTML = '<input type="text" id="unit" class="form-control unit" readonly>';
            jqTds[9].innerHTML = '<input id="surcharge" type="text" class="form-control surcharge">';
            jqTds[10].innerHTML = '<input id="amount" type="text" class="form-control amount" readonly>';
            jqTds[11].innerHTML = '<input id="tax_code" type="text" class="form-control tax_code" disabled>';
            jqTds[12].innerHTML = '<input id="pph_code" type="text" class="form-control pph_code" disabled>';
            jqTds[13].innerHTML = '<input id="keterangan" type="text" class="form-control keterangan">';
            //jqTds[13].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';
            //jqTds[14].innerHTML = '<center><a class="btn default btn-xs green edit" id="save" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red cancel" id="cancel"><i class="fa fa-trash"></i></a></center>';
            jqTds[14].innerHTML = '<center><a class="btn default btn-xs red cancel" id="cancel"><i class="fa fa-trash"></i></a></center>';



            //--- AUTOCOMPLETE SERVICE CODE
            $('.nama-service-code2').autocomplete({
                source: function (request, response) {
                    var inp = $('.nama-service-code2').val();
                    var l = inp.length;

                    var sGroup = $('.SERVICES_GROUP').val();

                    var arraySGroup = sGroup.split("|");
                    var valSGroup = arraySGroup[0];
                    var valTaxSGroup = arraySGroup[1];
                    var valPPHSGroup = arraySGroup[2];

                    $.ajax({
                        type: "POST",
                        url: "/TransVBList/GetDataServiceCode",
                        data: "{ SERVICE_NAME:'" + inp + "', SERVICE_GROUP:'" + valSGroup + "'}",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            console.log(data);
                            response($.map(data, function (el) {
                                    return {
                                        label: el.label,
                                        value: "",
                                        unit: el.unit,
                                        code: el.code,
                                        currency: el.currency,
                                        price: el.price,
                                        multiply: el.multiply,
                                        tax_code: valTaxSGroup,
                                        pph_code: valPPHSGroup,
                                        service_name: el.service_name,
                                        gl_account: el.gl_account
                                    };
                            }));
                        }
                    });

                },
                select: function (event, ui) {
                    event.preventDefault();
                    this.value = ui.item.code;

                    //format biaya2 dengan separator 
                    var raw_price = ui.item.price;
                    //var price = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    price = raw_price;

                    //$(this).closest('tr').find('##txt-service-name').val()
                    $(this).closest('tr').find('.service_name').val(ui.item.service_name);
                    $(this).closest('tr').find('.unit').val(ui.item.unit);
                    $(this).closest('tr').find('.currency').val(ui.item.currency);
                    $(this).closest('tr').find('.price').val(ui.item.price);
                    $(this).closest('tr').find('.multiply_factor').val(ui.item.multiply);
                    $(this).closest('tr').find('.tax_code').val(ui.item.tax_code);
                    $(this).closest('tr').find('.pph_code').val(ui.item.pph_code);
                    $(this).closest('tr').find('.qty').val(1);
                    $(this).closest('tr').find('.surcharge').val(0);
                    var tax = Number(Number(ui.item.price) * 0.1);
                    var amt = Number(ui.item.price);

                    //Perhitungan Pertama
                    //var sub = (Number($(this).closest('tr').find('.price').val(ui.item.price)) * Number($(this).closest('tr').find('.price').val(ui.item.price))) * (Number($(this).closest('tr').find('.multiply_factor').val(ui.item.multiply)) / 100);
                    var sub = (Number(ui.item.price) * Number(ui.item.price)) * (Number(ui.item.multiply) / 100);


                    var taxBulat = Math.round(tax);
                    var amtBulat = Math.round(amt);
                    var subBulat = Math.round(sub);

                    var formatTaxBulat = taxBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatAmtBulat = amtBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var formatSubBulat = subBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    $(this).closest('tr').find('.subtotal').val(formatSubBulat);
                    $(this).closest('tr').find('.tax_amount').val(formatTaxBulat);
                    $(this).closest('tr').find('.amount').val(formatAmtBulat);
                    document.getElementById("qty").focus();
                }
            });

          
            //Sub Total : Amount + Tax 
            $('.qty').on('change', function (e) {
                var amount = $(this).closest('tr').find('.amount').val();
                var prc = $(this).closest('tr').find('.price').val();
                var qty = $(this).closest('tr').find('.qty').val();
                var multiply_factor = $(this).closest('tr').find('.multiply_factor').val();
                var surcharge = $(this).closest('tr').find('.surcharge').val(); // variabel ini sepertinya kemarin kehapus, mknya NaN

                var nfSurcharge = parseFloat(surcharge.split('.').join(''));

                //Amount : (Price*Multiply Factor/100)*Quantity
                //Remove formatting sebelum dihitung
                nfPrc = parseFloat(prc.split('.').join(''));

                var harga = Number(prc) * Number(qty) * (Number(multiply_factor) / 100);
                //var hitungraw = Number(harga) + Number(nfSurcharge);
                var hitungraw = Number(harga);
                console.log('total no format ' + harga);
                console.log('hitung raw ' + hitungraw);
                var rounding = Math.round(hitungraw);
                console.log('hitung rounding ' + rounding);

                //var amount = (Number(nfPrc) * (Number(multiply_factor) / 100)) * (Number(qty) + nfSurcharge);
                var amountBulat = Math.round(amount);

                var formatAmountBulat = rounding.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                $(this).closest('tr').find('.amount').val(formatAmountBulat);
                $(this).closest('tr').find('.surcharge').val(nfSurcharge);
            });

            $("#mask_date1").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $("#mask_date2").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            $("#qtyx").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            $("#surcharge").inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });
        }

        var table = $('#editable_transvb');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 100,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1],
                    "width": "20%"
                },
                {
                    "targets": [12],
                    "width": "30%"
                }
            ],
            "bSort": false
        });

        $('#editable_transvb tbody').on('click', '#cancel', function () {
            oTable.fnDeleteRow(0);
        });

        var tableWrapper = $("#editable_new_transvb");

        var nEditing = null;
        var nNew = false;

        $('#editable_new_transvb').click(function (e) {
            e.preventDefault();
            
            var SERVICE_GROUP = $('#SERVICES_GROUP').val();
            console.log(SERVICE_GROUP);
            if (SERVICE_GROUP =='') {
                swal('Warning', 'Please Select Service Group!', 'warning');

            } else {
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
            }
        });

        $('#btn-update').click(function () {
            var tvb_d = [];
            var id_header = "";
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var POSTING_DATE = $('#POSTING_DATE').val();
            var SERVICES_GROUP = $('#SERVICES_GROUP').val();
            var COSTUMER_ID = $('#CUSTOMER_ID').val();
            var CUSTOMER_MDM = $('#CUSTOMER_MDM').val();
            var INSTALLATION_ADDRESS = $('#INSTALLATION_ADDRESS').val();
            var CUSTOMER_SAP_AR = $('#CUSTOMER_SAP_AR').val();
            var TOTAL = $('#TOTAL').val();
            var CREATED_BY = $('#CREATED_BY').val();
            var BRANCH_ID = $('#BRANCH_ID').val();

            var arraySGroup = SERVICES_GROUP.split("|");
            var valSGroup = arraySGroup[0];
            var valTaxSGroup = arraySGroup[1];
            var valPPHSGroup = arraySGroup[2];


            // Pengecekan save kecil
            var DTAneka = $('#editable_transvb').dataTable();
            var all_row = DTAneka.fnGetNodes();
            for (var i = 0; i < all_row.length; i++) {
                var cek = $(all_row[i]).find('input[name="service-code"]').val();
            }
            console.log('cek ' + cek);

            if (cek) {
                swal('Warning', 'Please Save Transaction Detail!', 'warning');
            }
            else {
                //console.log('query insert');
                if (PROFIT_CENTER && POSTING_DATE && valSGroup && COSTUMER_ID && CUSTOMER_MDM && INSTALLATION_ADDRESS && CUSTOMER_SAP_AR) {
                    // Declare dan getData dari dataTable Detail
                    var DTDetail = $('#editable_transvb').dataTable();
                    var countDTDetail = DTDetail.fnGetData();
                    arrDetail = new Array();

                    for (var i = 0; i < countDTDetail.length; i++) {
                        var item = DTDetail.fnGetData(i);
                        var servicePrice = item[5];
                        var nfServicePrice = parseFloat(servicePrice.split('.').join(''));

                        var amount = item[10];
                        var nfAmount = parseFloat(amount.split('.').join(''));

                        //split unit, ambil kode unti yg dibaca SAP
                        var rawUnit = item[8];
                        var splitRaw = rawUnit.split(" | ");
                        var valueUnitSAP = splitRaw[0];

                        var paramDTDetail = {
                            START_DATE: item[0],
                            END_DATE: item[1],
                            SERVICE_CODE: item[2],
                            SERVICE_NAME: item[3],
                            CURRENCY: item[4],
                            PRICE: servicePrice,
                            MULTIPLY_FACTOR: item[6],
                            QUANTITY: item[7],
                            UNIT: valueUnitSAP,
                            SURCHARGE: item[9],
                            AMOUNT: nfAmount,
                            TAX_CODE: item[11],
                            PPH_CODE: item[12],
                            REMARK: item[13]
                        }
                        arrDetail.push(paramDTDetail);
                    }

                    var param = {
                        PROFIT_CENTER: PROFIT_CENTER,
                        POSTING_DATE: POSTING_DATE,
                        SERVICES_GROUP: valSGroup,
                        TAX_CODE: valTaxSGroup,
                        PPH_CODE: valPPHSGroup,
                        COSTUMER_ID: COSTUMER_ID,
                        COSTUMER_MDM: CUSTOMER_MDM,
                        INSTALLATION_ADDRESS: INSTALLATION_ADDRESS,
                        CUSTOMER_SAP_AR: CUSTOMER_SAP_AR,
                        TOTAL: TOTAL,
                        CREATED_BY: CREATED_BY,
                        KD_CABANG: BRANCH_ID,
                        details: arrDetail
                    };
                    console.log(arrDetail);

                    App.blockUI({ boxed: true });
                    if (arrDetail.length <= 0) {
                        //swal('Warning', 'Detail Transaksi Tidak Ada, Mohon Cek Lagi. Jika Masih Muncul Pesan Ini Silahkan Clear Cache dan Ulangi Transaksi!', 'warning');
                        swal('Warning', 'Detail Transaksi Tidak Ada, Mohon Cek Lagi. Jika Masih Muncul Pesan Ini Silahkan Clear Cache dan Ulangi Transaksi!', 'warning').then(function (isConfirm) {
                            window.location = "/TransVBList";
                        });
                    }
                    else {
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/TransVBList/SaveHeader"
                        });

                        req.done(function (data) {
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location = "/TransVBList";
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransVBList";
                                });
                            }
                        });
                    }
                }// END OF CHECK APAKAH DATA DI FORM SUDAH TERISI ATAU BELUM
                else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }// End of save kecil

        });

        $('#CUSTOMER_MDM').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_MDM').val();
                var l = inp.length;
                console.log(l);

                $.ajax({
                    type: "POST",
                    url: "/TransVBList/Customer",
                    data: "{ MPLG_NAMA:'" + inp + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_ID").val(ui.item.code);
                $("#INSTALLATION_ADDRESS").val(ui.item.address);
                $("#CUSTOMER_SAP_AR").val(ui.item.sap);
                //cekPiut("1D", ui.item.sap);
            }
        });

    }
    
    //--- END OF SELECT 2 OTHER SERVICES
    var cekPiut = function (doc_type, cust_code) {
        $.ajax({
            type: "POST",
            url: "/TransVB/cekPiutangSAP",
            data: "{ DOC_TYPE:'" + doc_type + "', CUST_CODE:'" + cust_code + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }

    var comboServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/TransVBList/GetDataDropDownServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Chose Service Gorup --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
                }
                $("#SERVICES_GROUP").html('');
                $("#SERVICES_GROUP").append(listItems);
            }
        });

    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransVBList";
        });
    }


    return {

        init: function () {
            handleTable();
            batal();
            janganEnter();
            comboServiceGroup();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesEditable.init();

    //ambil tanggal sysdate js
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#POSTING_DATE').val(today);
});