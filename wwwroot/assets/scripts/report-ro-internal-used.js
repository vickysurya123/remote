﻿var ReportRoInternalUsed = function () {

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var CBBE = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportROInternalUsed/GetDataBE",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBYears();
                //$("#BUSINESS_ENTITY").trigger("chosen:updated");
            }
        });
        //$('#BUSINESS_ENTITY').chosen({ width: "100%" });
    }

    var CBYears = function () {
        // Ajax untuk ambil ContractOfferNumber
        var listItems = "";
        listItems += "<option value=''>-- Choose Years --</option>";

        for (i = new Date().getFullYear(); i > 1900; i--) {
            listItems += "<option value='" + i + "'>" + i + "</option>";
        }
        $("#YEARS").html('');
        $("#YEARS").append(listItems);
        CBDataUsageType();
        //$("#YEARS").trigger("chosen:updated");
        //$('#YEARS').chosen({ width: "100%" });
    }

    var CBDataUsageType = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportROInternalUsed/GetDataUsageType",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Usage Type --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#USAGE_TYPE").html('');
                $("#USAGE_TYPE").append(listItems);
                CBDataZone();
                //$("#USAGE_TYPE").trigger("chosen:updated");
            }
        });
        //$('#USAGE_TYPE').chosen({ width: "100%" });
    }

    var CBDataZone = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportROInternalUsed/GetDataZONE",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Zone --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#ZONE").html('');
                $("#ZONE").append(listItems);
                handleBootstrapSelect();
                //$("#ZONE").trigger("chosen:updated");
            }
        });
        //$('#ZONE').chosen({ width: "100%" });
    }

    var dataTableResult = function () {
        var table = $('#table-report-ro-internal-used');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var showFilter = function () {
        $('#btn-filter-document-flow').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-ro-internal-used')) {
                $('#table-report-ro-internal-used').DataTable().destroy();
            }
            $('#table-report-ro-internal-used').DataTable({
                "ajax": {
                    "url": "/ReportROInternalUsed/GetFilterTwoNew",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.val_years = $('#YEARS').val();
                        data.usage_type = $('#USAGE_TYPE').val();
                        data.val_zone = $('#ZONE').val();
                        data.val_status = $('#STATUS').val();

                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "RO_CODE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "BE_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.CREATION_DATE === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.CREATION_DATE + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            console.log(full.REASON);
                            if (full.REASON === 'No Reason') {
                                return '<span> Idle </span>';
                            }
                            if (full.REASON === 'Pemakaian Internal') {
                                return '<span> Internal Used </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.OBJ === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.OBJ + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.RO_ADDRESS === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.RO_ADDRESS + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.USAGES === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.USAGES + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ZONE_RIP === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.ZONE_RIP + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.RO_CERTIFICATE_NUMBER === null) {
                                return '<span> - </span>';
                            } else {
                                return '<span> ' + full.RO_CERTIFICATE_NUMBER + ' </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    return {
        init: function () {
            dataTableResult();
            CBBE();
            //CBYears();
            //CBDataZone();
            //CBDataUsageType();
            showFilter();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var YEARS = $('#YEARS').val();
    var USAGE_TYPE = $('#USAGE_TYPE').val();
    var ZONE = $('#ZONE').val();
    var STATUS = $('#STATUS').val();

    var dataJSON = {
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        YEARS: YEARS,
        USAGE_TYPE: USAGE_TYPE,
        ZONE: ZONE,
        STATUS: STATUS
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportROInternalUsed/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });

            //setTimeout(function () {
            //    exportExcel();
            //}, 2000);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function Clicks() {
    console.log('Yess ' + fileExcels);
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportROInternalUsed/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

jQuery(document).ready(function () {
    ReportRoInternalUsed.init();
});

jQuery(document).ready(function () {
    $('#table-report-ro-internal-used > tbody').html('');
    //$('#BUSINESS_ENTITY', this).chosen();
    //$('#YEARS', this).chosen();
    //$('#USAGE_TYPE', this).chosen();
    //$('#ZONE', this).chosen();
    //$('#STATUS', this).chosen();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportROInternalUsed/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}