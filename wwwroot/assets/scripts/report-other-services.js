﻿
var handleBootstrapSelect = function () {
    $('.bs-select').selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
}

var ReportOTherServices = function () {

    var select2BusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportOtherService/getBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                select2ServiceGroup();
            }
        });
    }

    var select2ProfitCenter = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var valBE = $(this).val();
            CBProfitCenter(valBE);

        });
    }

    var select2ServiceGroup = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportOtherService/getServiceGroup",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Servies Group--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                }
                $("#SERVICE_GROUP").html('');
                $("#SERVICE_GROUP").append(listItems);
                //$("#SERVICE_GROUP").trigger("chosen:updated");
                select2ProfitCenter();
                handleBootstrapSelect();
            }
        });
        //$('#SERVICE_GROUP').chosen({ width: "100%" });
    }

    var clear = function () {
        $("input").val('');
        $(".select2-selection__rendered").text('');
        var xTable = $('#table-permohonan').dataTable();
        var nRow = $(this).parents('tr')[0];
        xTable.fnDeleteRow(nRow);
    }

    var reset = function () {
        $('#btn-reset').click(function () {
            clear();
        });
    }

    var dataTableResult = function () {
        var table = $('#table-report-other-service');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                /*
                {
                    "targets": [12],
                    "visible": false,
                },
                */
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    }

    var showAll = function () {
        $('#btn-show-all').click(function () {

            if ($.fn.DataTable.isDataTable('#table-report-other-service')) {
                $('#table-report-other-service').DataTable().destroy();
            }

            $('#table-report-other-service').DataTable({
                "ajax": {
                    "url": "/ReportOtherService/ShowAll",
                    "type": "GET",
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "BE_NAME",
                    },
                    {
                        "data": "PROFIT_CENTER_NAME",
                    },
                    {
                        "data": "SERVICE_GROUP_NAME"
                    },
                    {
                        "data": "SERVICE_NAME"
                    },
                    {
                        "data": "GL_ACCOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FUNCTION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE == '1') {
                                return '<span class="label label-sm label-success"> ACTIVE </span>';
                            }
                            else {
                                return '<span class="label label-sm label-danger"> INACTIVE </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-other-service')) {
                $('#table-report-other-service').DataTable().destroy();
            }

            $('#table-report-other-service').DataTable({
                "ajax": {
                    "url": "ReportOtherService/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.service_group = $('#SERVICE_GROUP').val();
                        data.status = $('#STATUS').val();
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "BE_NAME",
                    },
                    {
                        "data": "PROFIT_CENTER_NAME",
                    },
                    {
                        "data": "SERVICE_GROUP_NAME"
                    },
                    {
                        "data": "SERVICE_NAME"
                    },
                    {
                        "data": "GL_ACCOUNT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FUNCTION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE == '1') {
                                return '<span class="label label-sm label-success"> ACTIVE </span>';
                            }
                            else {
                                return '<span class="label label-sm label-danger"> INACTIVE </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    return {
        init: function () {
            select2BusinessEntity();
            //select2ProfitCenter();
            //select2ServiceGroup();
            dataTableResult();
            showAll();
            filter();
            reset();
            //CBProfitCenter();
        }
    };
}();

function CBProfitCenter(BE_ID) {

    if (BE_ID == null) {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportOtherService/getProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].TERMINAL_NAME + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);

            }
        });
    } else {
        console.log(BE_ID);

        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportOtherService/getProfitCenterD",
            data: {
                BE_ID: BE_ID
            },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Profit Center--</option>";

                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].TERMINAL_NAME + "</option>";
                }
                $("#PROFIT_CENTER").html('');
                $("#PROFIT_CENTER").append(listItems);

            }
        });
    }
}

var fileExcels;
function defineExcel() {

    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var PROFIT_CENTER = $('#PROFIT_CENTER').val();
    var SERVICE_GROUP = $('#SERVICE_GROUP').val();
    var STATUS = $('#STATUS').val();

    var dataJSON = {
        BE_ID: BUSINESS_ENTITY,
        PROFIT_CENTER_ID: PROFIT_CENTER,
        SERVICE_GROUP_ID: SERVICE_GROUP,
        STATUS: STATUS
    };

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportOtherService/defineExcel",
        data: JSON.stringify(dataJSON),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                    '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportOtherService/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ReportOTherServices.init();
    });
}

jQuery(document).ready(function () {
    $('#table-report-other-service > tbody').html('');
    //$('#SERVICE_GROUP', this).chosen();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}