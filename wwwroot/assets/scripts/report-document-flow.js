﻿var ReportDocumentFlow = function () {

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var CBBE = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataBE",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BE_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
                CBContractOfferNumber();
                //$("#BUSINESS_ENTITY").trigger("chosen:updated");
            }
        });
        //$('#BUSINESS_ENTITY').chosen({ width: "100%" });
    }

    var CBContractOfferNumber = function () {
        // Ajax untuk ambil ContractOfferNumber
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataContractOfferNumb",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Offer Number --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].CONTRACT_OFFER_NO + "'>" + jsonList.data[i].OFFER_NAME + "</option>";
                }
                $("#CONTRACT_OFFER_NUMBER").html('');
                $("#CONTRACT_OFFER_NUMBER").append(listItems);
                CBRentalRequestNumb();
                //$("#CONTRACT_OFFER_NUMBER").trigger("chosen:updated");
            }
        });
        //$('#CONTRACT_OFFER_NUMBER').chosen({ width: "100%" });
    }

    var CBRentalRequestNumb = function () {
        // Ajax untuk ambil RentalRequestNumb
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataRentalRequestNumb",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Rental Request Number --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].RENTAL_REQUEST_NO + "'>" + jsonList.data[i].REQUEST_NAME + "</option>";
                }
                $("#RENTAL_REQUEST_NUMBER").html('');
                $("#RENTAL_REQUEST_NUMBER").append(listItems);
                CBContractOfferStatus();
                //$("#RENTAL_REQUEST_NUMBER").trigger("chosen:updated");
            }
        });
        //$('#RENTAL_REQUEST_NUMBER').chosen({ width: "100%" });
    }

    var CBContractOfferStatus = function () {
        // Ajax untuk ambil ContractOfferStatus
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataContractOfferStatus",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose ContractOffer Status --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].REF_DATA + "'>" + jsonList.data[i].REF_NAME + "</option>";
                }
                $("#CONTRACT_OFFER_STATUS").html('');
                $("#CONTRACT_OFFER_STATUS").append(listItems);
                CBRentalRequestStatus();
                //$("#CONTRACT_OFFER_STATUS").trigger("chosen:updated");
            }
        });
        //$('#CONTRACT_OFFER_STATUS').chosen({ width: "100%" });
    }

    var CBRentalRequestStatus = function () {
        // Ajax untuk ambil RentalRequestStatus
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataRentalRequestStatus",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose RentalRequest Status --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].STATUS + "'>" + jsonList.data[i].STATUS + "</option>";
                }
                $("#RENTAL_REQUEST_STATUS").html('');
                $("#RENTAL_REQUEST_STATUS").append(listItems);
                CBContractNumb();
                //$("#RENTAL_REQUEST_STATUS").trigger("chosen:updated");
            }
        });
        //$('#RENTAL_REQUEST_STATUS').chosen({ width: "100%" });
    }

    var CBContractNumb = function () {
        // Ajax untuk ambil ContractNumb
        $.ajax({
            type: "GET",
            url: "/ReportDocumentFlow/GetDataContractNumb",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Contract Number --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].CONTRACT_NO + "'>" + jsonList.data[i].CONTRACT_NAME + "</option>";
                }
                $("#CONTRACT_NUMBER").html('');
                $("#CONTRACT_NUMBER").append(listItems);
                handleBootstrapSelect();
                //$("#CONTRACT_NUMBER").trigger("chosen:updated");
            }
        });
        //$('#CONTRACT_NUMBER').chosen({ width: "100%" });
    }

    var dataTableResult = function () {
        var table = $('#table-report-doc-flow');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 5, 
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,   
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 1],
                    "width": "20%"
                }
            ]
        });
    } 

    var showFilter = function () {
        $('#btn-filter-document-flow').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-report-doc-flow')) {
                $('#table-report-doc-flow').DataTable().destroy();
            }
            $('#table-report-doc-flow').DataTable({
                "ajax": {
                    "url": "/ReportDocumentFlow/ShowFilterTwo",
                    "type": "GET",
                    "data": function (data) {
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.contract_offer_number = $('#CONTRACT_OFFER_NUMBER').val();
                        data.rental_request_number = $('#RENTAL_REQUEST_NUMBER').val();
                        data.contract_offer_status = $('#CONTRACT_OFFER_STATUS').val();
                        data.rental_request_status = $('#RENTAL_REQUEST_STATUS').val();
                        data.contact_number = $('#CONTRACT_NUMBER').val();
                        
                    },
                    "dataSrc": function (json) {
                        //Make your callback here.
                        $('#btn-cetak_exel').show('slow');
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "BUSINESS_ENTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_CREATED_BY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DATE_RENTAL_REQUEST_CREATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RENTAL_REQUEST_STATUS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DATE_RENTAL_REQUEST_APPROVED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_OFFER_NUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_OFFER_LAST_STATUS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_RENTAL_REQUEST_APPROVED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DATE_OFFER_CREATED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_CONTRACT_OFFER_APPROVED",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RELEASE_TO_WORKFLOW",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_RELEASE_TO_WORKFLOW",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_MANAGER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_APPROVE_BY_MANAGER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_DEPT_GM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_APPROVE_BY_DEPT_GM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_GM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_APPROVE_BY_GM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_SM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_APPROVE_BY_SM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_DIR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DELAYS_APPROVE_BY_DIR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_KOM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "APPROVED_BY_RUPS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    }
                ],
                "columnDefs": [{
                    "width": "80px", "targets": "_all"
                }],
                "lengthMenu": [
                    [5, 10, 15, 20, 10000],
                    [5, 10, 15, 20, "All"]
                ],
                "scrollX": true,
                "pageLength": 10,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "filter": false,

                "order": [
                    [10, "desc"]
                ]
            });
        });
    }

    return {
        init: function () {
            dataTableResult();
            CBBE();
            //CBContractOfferNumber();

            //CBRentalRequestNumb();
            //CBContractOfferStatus();
            //CBRentalRequestStatus();
            //CBContractNumb();
            showFilter();
        }
    };
}();

var fileExcels;
function defineExcel() {
    var BUSINESS_ENTITY = $('#BUSINESS_ENTITY').val();
    var CONTRACT_OFFER_NUMBER = $('#CONTRACT_OFFER_NUMBER').val();
    var RENTAL_REQUEST_NUMBER = $('#RENTAL_REQUEST_NUMBER').val();
    var CONTRACT_OFFER_STATUS = $('#CONTRACT_OFFER_STATUS').val();
    var RENTAL_REQUEST_STATUS = $('#RENTAL_REQUEST_STATUS').val();
    var CONTRACT_NUMBER = $('#CONTRACT_NUMBER').val();

    var dataJSON = "{BUSINESS_ENTITY:" + JSON.stringify(BUSINESS_ENTITY) + ",CONTRACT_OFFER_NUMBER:" + JSON.stringify(CONTRACT_OFFER_NUMBER) +
                   ",RENTAL_REQUEST_NUMBER:" + JSON.stringify(RENTAL_REQUEST_NUMBER) + ",CONTRACT_OFFER_STATUS:" + JSON.stringify(CONTRACT_OFFER_STATUS) +
                   ",RENTAL_REQUEST_STATUS:" + JSON.stringify(RENTAL_REQUEST_STATUS) + ",CONTRACT_NUMBER:" + JSON.stringify(CONTRACT_NUMBER) + "}";

    App.blockUI({ boxed: true });
    $.ajax({
        type: "POST",
        url: "/ReportDocumentFlow/defineExcel",
        data: dataJSON,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            }); 

            //setTimeout(function () {
            //    exportExcel();
            //}, 2000);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function Clicks() {
    console.log('Yess ' + fileExcels);
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/ReportDocumentFlow/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });

}

jQuery(document).ready(function () {
    ReportDocumentFlow.init();
});

jQuery(document).ready(function () {
    $('#table-report-doc-flow > tbody').html('');
    //$('#BUSINESS_ENTITY', this).chosen();
    //$('#CONTRACT_OFFER_NUMBER', this).chosen();
    //$('#RENTAL_REQUEST_NUMBER', this).chosen();
    //$('#CONTRACT_OFFER_STATUS', this).chosen();
    //$('#RENTAL_REQUEST_STATUS', this).chosen();
    //$('#CONTRACT_NUMBER', this).chosen();
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportDocumentFlow/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}