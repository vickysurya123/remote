﻿var MasterBEAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/BusinessEntity";
        });
    }

    var simpan = function () {
        $('#btn-simpan').click(function () {
            var BE_ID = $('#BE_ID').val();
            var BE_NAME = $('#BE_NAME').val();
            var HARBOUR_CLASS = $('#HARBOUR_CLASS').val();
            var VALID_FROM = $('#VALID_FROM').val();
            var VALID_TO = $('#VALID_TO').val();
            var BE_ADDRESS = $('#BE_ADDRESS').val();
            var POSTAL_CODE = $('#POSTAL_CODE').val();
            var BE_CITY = $('#BE_CITY').val();
            var BE_PROVINCE = $('#BE_PROVINCE').val();
            var PHONE_1 = $('#PHONE_1').val();
            var FAX_1 = $('#FAX_1').val();
            var PHONE_2 = $('#PHONE_2').val();
            var FAX_2 = $('#FAX_2').val();
            var EMAIL = $('#EMAIL').val();

            console.log('PROVINCE ' + BE_PROVINCE);

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addMeasurementModal').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                // && VALID_FROM && VALID_FROM && BE_ADDRESS && BE_PROVINCE
                if (BE_ID && BE_NAME && HARBOUR_CLASS && VALID_FROM && VALID_TO && BE_ADDRESS, BE_PROVINCE) {
                    var param = {
                        BE_ID: BE_ID,
                        BE_NAME: BE_NAME,
                        HARBOUR_CLASS: HARBOUR_CLASS,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        BE_ADDRESS: BE_ADDRESS,
                        POSTAL_CODE: POSTAL_CODE,
                        BE_CITY: BE_CITY,
                        BE_PROVINCE: BE_PROVINCE,
                        PHONE_1: PHONE_1,
                        FAX_1: FAX_1,
                        PHONE_2: PHONE_2,
                        FAX_2: FAX_2,
                        EMAIL: EMAIL
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/BusinessEntity/AddData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                //redirect kke edit data RO untuk measurement dan fixture&fitting
                                window.location.href = "/BusinessEntity/AddMeasurement/" + data.be_number;
                                //window.location = "/RentalObject";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/BusinessEntity";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }

        });
    }

    var simpanmeasurement = function () {
        $('#btn-simpanmeasurement').click(function () {
            var BE_ID = $('#BE_ID').val();
            var MEASUREMENT_TYPE = $('#MEASUREMENT_TYPE').val();
            var DESCRIPTION = $('#DESCRIPTION').val();
            var AMOUNT = $('#AMOUNT').val();
            var UNIT = $('#UNIT').val();
            var VALID_FROM = $('#VALID_FROM_M').val();
            var VALID_TO = $('#VALID_TO_M').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addMeasurementModal').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');

            }
            else {
                // $('#addMeasurementModal').modal('hide');
                if (BE_ID && MEASUREMENT_TYPE) {
                    var param = {
                        BE_ID: BE_ID,
                        MEASUREMENT_TYPE: MEASUREMENT_TYPE,
                        DESCRIPTION: DESCRIPTION,
                        AMOUNT: AMOUNT,
                        UNIT: UNIT,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/BusinessEntity/AddDataMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addMeasurementModal').modal('hide');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                            $('#addMeasurementModal').modal('hide');
                        }
                    });

                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    $('#addMeasurementModal').modal('hide');
                }
            }
        });
    }

    var update = function () {
        $('#btn-update').click(function () {
            var BE_ID = $('#BE_ID').val();
            var BE_NAME = $('#BE_NAME').val();
            var BE_ADDRESS = $('#BE_ADDRESS').val();
            var BE_CITY = $('#BE_CITY').val();
            var BE_PROVINCE = $('#BE_PROVINCE').val();
            var BE_CONTACT_PERSON = $('#BE_CONTACT_PERSON').val();
            var BE_NPWP = $('#BE_NPWP').val();
            var BE_NOPPKP = $('#BE_NOPPKP').val();
            var BE_PPKP_DATE = $('#BE_PPKP_DATE').val();
            var CREATION_BY = $('#CREATION_BY').val();
            var CREATION_DATE = $('#CREATION_DATE').val();
            var LAST_UPDATE_BY = $('#LAST_UPDATE_BY').val();
            var LAST_UPDATE_DATE = $('#LAST_UPDATE_DATE').val();
            var VALID_FROM = $('#VALID_FROM').val();
            var VALID_TO = $('#VALID_TO').val();
            var HARBOUR_CLASS = $('#HARBOUR_CLASS').val();
            var POSTAL_CODE = $('#POSTAL_CODE').val();

            var PHONE_1 = $('#PHONE_1').val();
            var FAX_1 = $('#FAX_1').val();
            var PHONE_2 = $('#PHONE_2').val();
            var FAX_2 = $('#FAX_2').val();
            var EMAIL = $('#EMAIL').val();
            var LATITUDE = $('#LATITUDE').val();
            var LONGITUDE = $('#LONGITUDE').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addMeasurementModal').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (BE_ID && BE_NAME && HARBOUR_CLASS && VALID_FROM && BE_ADDRESS && BE_PROVINCE) {
                    var param = {
                        BE_ID: BE_ID,
                        BE_NAME: BE_NAME,
                        BE_ADDRESS: BE_ADDRESS,
                        BE_CITY: BE_CITY,
                        BE_PROVINCE: BE_PROVINCE,
                        BE_CONTACT_PERSON: BE_CONTACT_PERSON,
                        BE_NPWP: BE_NPWP,
                        BE_NOPPKP: BE_NOPPKP,
                        BE_PPKP_DATE: BE_PPKP_DATE,
                        CREATION_BY: CREATION_BY,
                        CREATION_DATE: CREATION_DATE,
                        LAST_UPDATE_BY: LAST_UPDATE_BY,
                        LAST_UPDATE_DATE: LAST_UPDATE_DATE,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        HARBOUR_CLASS: HARBOUR_CLASS,
                        POSTAL_CODE: POSTAL_CODE,
                        LATITUDE: LATITUDE,
                        LONGITUDE: LONGITUDE,

                        PHONE_1: PHONE_1,
                        FAX_1: FAX_1,
                        PHONE_2: PHONE_2,
                        FAX_2: FAX_2,
                        EMAIL: EMAIL

                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/BusinessEntity/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location.href = '/BusinessEntity';
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location.href = '/BusinessEntity';
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }


        });
    }
    var initTableMeasurement = function () {
        var uriId = $('#UriId').val();
        $('#table-measurement').DataTable({

            "ajax":
            {
                "url": "/BusinessEntity/GetDataMeasurement/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },


            "columns": [
                {
                    "data": "MEASUREMENT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION"
                },
                {
                    "data": "AMOUNT"
                },
                {
                    "data": "UNIT"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#editMeasurementModal" class="btn default btn-xs green" id="btn-ubah-measurement"><i class="fa fa-edit"></i> </a>';
                        aksi += '<a class="btn default btn-xs blue" id="btn-status-measurement"><i class="fa fa-exchange"></i></a>';
                        //aksi += '<a class="btn default btn-xs red" id="btn-hapusmeasurement"><i class="fa fa-close"></i> </a>';

                        //var aksi = '<a class="btn btn-icon-only red" id="btn-hapusmeasurement"><i class="fa fa-close"></i></a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });

    }

    // Untuk ditampilkan di textbox edit measurement
    var DataUpdateMeasurement = function () {
        $('body').on('click', 'tr #btn-ubah-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            $("#MEASUREMENT_ID").val(data["ID"]);
            $("#AMOUNT2").val(data["AMOUNT"]);
            $("#UNIT2").val(data["UNIT"]);
            $("#VALID_FROM2").val(data["VAL_FROM"]);
            $("#VALID_TO2").val(data["VAL_TO"]);

        });
    }

    // Ubah status measurement
    var ubahstatusmeasurement = function () {
        $('body').on('click', 'tr #btn-status-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/BusinessEntity/UbahStatusMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    // Save Update Measurement
    var updatemeasurement = function () {
        $('#btn-simpan-update-measurement').click(function () {
            var MEASUREMENT_ID = $('#MEASUREMENT_ID').val();
            var MEASUREMENT_TYPE2 = $('#MEASUREMENT_TYPE2').val();
            var AMOUNT2 = $('#AMOUNT2').val();
            var UNIT2 = $('#UNIT2').val();
            var VALID_FROM2 = $('#VALID_FROM2').val();
            var VALID_TO2 = $('#VALID_TO2').val();

            if (Date.parse(VALID_FROM2) > Date.parse(VALID_TO2)) {
                $('#editMeasurementModal').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (MEASUREMENT_ID && MEASUREMENT_TYPE2 && AMOUNT2 && UNIT2 && VALID_FROM2) {
                    var param = {
                        ID: MEASUREMENT_ID,
                        MEASUREMENT_TYPE: MEASUREMENT_TYPE2,
                        AMOUNT: AMOUNT2,
                        UNIT: UNIT2,
                        VALID_FROM: VALID_FROM2,
                        VALID_TO: VALID_TO2
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/BusinessEntity/EditDataMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#editMeasurementModal').modal('hide');
                            swal('Success', data.message, 'success');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            $('#editMeasurementModal').modal('hide');
                            swal('Failed', data.message, 'error');
                        }
                    });
                } else {
                    $('#editMeasurement').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    var deletemeasurement = function () {
        $('body').on('click', 'tr #btn-hapusmeasurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/BusinessEntity/DeleteData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Berhasil', data.message, 'success');
                        } else {
                            swal('Gagal', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var cbharbour = function () {
        $.ajax({
            type: "GET",
            url: "/BusinessEntity/GetDataHarbourClass",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#HARBOUR_CLASS").append(listItems);
                });

            }
        });
    }

    var cbprovince = function () {
        $.ajax({
            type: "GET",
            url: "/BusinessEntity/GetDataProvince",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].DATA + "'>" + jsonList.data[i].REF_DESC + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#PROVINCE").append(listItems);
                });

            }
        });
    }

    var cbmeasurement = function () {
        $.ajax({
            type: "GET",
            url: "/BusinessEntity/GetDataCBMeasurement",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#MEASUREMENT_TYPEx").append(listItems);
                });

            }
        });
    }

    var cbmeasurement2 = function () {
        $.ajax({
            type: "GET",
            url: "/BusinessEntity/GetDataCBMeasurement",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#MEASUREMENT_TYPE2").append(listItems);
                });

            }
        });
    }

    var oncChageBE = function () {
        $('#BE_ID').change(function () {
            var BE_ID = $('#BE_ID').val();
            console.log('be id ' + BE_ID);
            if (BE_ID) {
                $("#fileupload").css('display', 'block'); //enable upload
            }
            else {
                $('#fileupload').css('display', 'none'); //disable upload
            }
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();
            //console.log(BE_ID);

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {

                    console.log(data);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/BusinessEntity/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { BE_ID: BE_ID },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        var uriId = $('#UriId').val();
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        //data: "id=" + data["ID_ATTACHMENT"],
                        //data: "{ id:'" + data["ID_ATTACHMENT"] + "', directory:'" + data["DIRECTORY"]+"}",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/BusinessEntity/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var initTableAttachment = function () {
        var uriId = $('#UriId').val();
        $('#table-attachment').DataTable({

            "ajax":
            {
                "url": "/BusinessEntity/GetDataAttachment/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                {
                    "data": "FILE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/BusinessEntity/' + uriId + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        //x += '<img src="file:///C:\REMOTE\BusinessEntity\123\FILE_ATTACHMENT_123_20170221113854.jpg">';
                        return x;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });

    }

    var valUpload = function () {
        $('#fileupload').css('display', 'none');
    }
    var PinPoint = function () {
        if ($('#BE_ID').val() === "" || $('#BE_ID').val() === null) return;
        var req = $.ajax({
            contentType: "application/json",
            data: "BE_ID=" + $('#BE_ID').val(),
            method: "get",
            url: "/BusinessEntity/PinPoint",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                console.log(data);
                Maps(data.message.LATITUDE, data.message.LONGITUDE)
            } else {
                //swal('Failed', data.message, 'error');
            }
        });
    }
    function Maps(Lat, Long) {
        var marker;
        var map = L.map('map').setView([Lat, Long], 17);
        marker = new L.marker([Lat, Long])
            .addTo(map);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            //attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        map.on('click', function (ev) {
            var latlng = map.mouseEventToLatLng(ev.originalEvent);
            if (marker) { // check
                map.removeLayer(marker); // remove
            }
            marker = new L.marker([latlng.lat, latlng.lng])
                .addTo(map);
            $('#LATITUDE').val(latlng.lat);
            $('#LONGITUDE').val(latlng.lng);
        });
    }

    return {
        init: function () {
            batal();
            simpan();
            update();
            simpanmeasurement();
            initTableMeasurement();
            deletemeasurement();
            cbharbour();
            cbprovince();
            cbmeasurement();
            cbmeasurement2();
            DataUpdateMeasurement();
            updatemeasurement();
            ubahstatusmeasurement();
            fupload();
            initTableAttachment();
            deleteAttachment();
            //valUpload();
            //oncChageBE();
            PinPoint();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterBEAdd.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg|zip)$");
        if (!(regex.test(val))) {
            $(this).val('');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, and JPG Extension Are Allowed', 'warning');
        }
    });
}); 