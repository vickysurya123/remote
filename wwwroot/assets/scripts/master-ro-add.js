﻿var MasterROAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/RentalObject";
        });
    }


    var sertifikat = function () {
        $("#sertifikat").on('change', function () {
            if (this.value == 0) {
                document.getElementById("KETERANGAN").disabled = false;
                document.getElementById("RO_CERTIFICATE_NUMBER").disabled = true;
            } else {
                document.getElementById("KETERANGAN").disabled = true;
                document.getElementById("RO_CERTIFICATE_NUMBER").disabled = false;
            }
        });

        var ktr = $('#KETERANGAN').val();
        var srt = $('#RO_CERTIFICATE_NUMBER').val();
        $("#sertifikat_edit").on('change', function () {         
           
            
            if (this.value == 0) {
                document.getElementById("KETERANGAN").disabled = false;
                document.getElementById("RO_CERTIFICATE_NUMBER").disabled = true;
                $('#RO_CERTIFICATE_NUMBER').val('');
                $('#KETERANGAN').val(ktr);
            } else {
                document.getElementById("KETERANGAN").disabled = true;
                $('#KETERANGAN').val('');
                $('#RO_CERTIFICATE_NUMBER').val(srt);
                document.getElementById("RO_CERTIFICATE_NUMBER").disabled = false;
            }
        });


        $("#KODE_ASET").select2({
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/RentalObject/GetDataAsset",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            console.log(item);
                            return {
                                id: item.ASSET_NO,
                                text: item.ASSET
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });

        var newOption = new Option(kodeAsset, kodeAsset, false, false);
        $('#KODE_ASET').append(newOption).trigger('change');
    }

    //Function untuk form Add (Form Sesuai Mockup)
    var simpanadd = function () {
        $('#btn-simpan-add').click(function () {
            var BE_ID = $('#BE_ID').val();
            var RO_TYPE = $('#RO_TYPE').val();
            var RO_NAME = $('#RO_NAME').val();
            var ZONE_RIP = $('#ZONE_RIP').val();
            var VALID_FROM = $('#VALID_FROM').val();
            var VALID_TO = $('#VALID_TO').val();
            var RO_CERTIFICATE_NUMBER = $('#RO_CERTIFICATE_NUMBER').val();
            //console.log(RO_CERTIFICATE_NUMBER);
            var KETERANGAN = $('#KETERANGAN').val();
            var SERTIFIKAT = $('#sertifikat').val();
            //var LOCATION = $('#LOCATION').val(); Jaga-jaga bila nanti jadi dipakai !
            //var FUNCTION = $('#FUNCTION').val();
            var USAGE_TYPE = $('#USAGE_TYPE').val();
            var LINI = $('#LINI').val();
            var PERUNTUKAN = $('#PERUNTUKAN').val();
            var RO_ADDRESS = $('#RO_ADDRESS').val();
            var RO_POSTALCODE = $('#RO_POSTALCODE').val();
            var RO_CITY = $('#RO_CITY').val();
            var RO_PROVINCE = $('#RO_PROVINCE').val();
            var MEMO = $('#MEMO').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var FUNCTION_ID = $('#FUNCTION_ID').val();
            var KODE_ASET = $('#KODE_ASET').val();

            //var SERTIFIKAT_ID = $('#SERTIFIKAT_ID').val();

            // console.log('USAGE TYPE '+USAGE_TYPE);

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (BE_ID && RO_TYPE && RO_NAME && ZONE_RIP && VALID_FROM && USAGE_TYPE && PROFIT_CENTER && RO_PROVINCE && FUNCTION_ID) {
                    var param = {
                        BE_ID: BE_ID,
                        RO_TYPE_ID: RO_TYPE,
                        RO_NAME: RO_NAME,
                        ZONE_RIP_ID: ZONE_RIP,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        RO_CERTIFICATE_NUMBER: RO_CERTIFICATE_NUMBER,
                        KETERANGAN: KETERANGAN,
                        //LOCATION_ID: LOCATION, Jaga-jaga bila nanti jadi dipakai !
                        //FUNCTION_ID: FUNCTION,
                        USAGE_TYPE_ID: USAGE_TYPE,
                        LINI: LINI,
                        PERUNTUKAN: PERUNTUKAN,
                        RO_ADDRESS: RO_ADDRESS,
                        RO_POSTALCODE: RO_POSTALCODE,
                        RO_CITY: RO_CITY,
                        RO_PROVINCE: RO_PROVINCE,
                        MEMO: MEMO,
                        PROFIT_CENTER: PROFIT_CENTER,
                        FUNCTION_ID: FUNCTION_ID,
                        KODE_ASET: KODE_ASET,
                        SERTIFIKAT: SERTIFIKAT
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/AddData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                //redirect kke edit data RO untuk measurement dan fixture&fitting
                                window.location = "/RentalObject/EditDetailRO/" + data.ro_id;
                                //window.location = "/RentalObject";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/RentalObject";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    var initTableFixtureFitting = function () {
        var uriId = $('#UriId').val();
        $('#table-fixture-fitting').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataFixtureFitting2/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "REF_DATA",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION"
                },
                {
                    "data": "POWER_CAPACITY"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#editFixtureFitting" class="btn default btn-xs green" id="btn-ubah-fixture-fitting"><i class="fa fa-edit"></i> </a>';
                        aksi += '<a class="btn default btn-xs blue" id="btn-status-fixture-fitting"><i class="fa fa-exchange"></i></a>';
                        //aksi += '<a class="btn default btn-xs red" id="btn-hapus-fixture-fitting"><i class="fa fa-close"></i> </a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableMeasurement = function () {
        var uriId = $('#UriId').val();
        $('#table-measurement').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataMeasurement/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "REF_DATA",
                    "class": "dt-body-center"
                },
                {
                    "data": "DESCRIPTION"
                },
                {
                    "data": "AMOUNT"
                },
                {
                    "data": "UNIT"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#editMeasurement" class="btn default btn-xs green" id="btn-ubah-measurement"><i class="fa fa-edit"></i> </a>';
                        aksi += '<a class="btn default btn-xs blue" id="btn-status-measurement"><i class="fa fa-exchange"></i></a>';

                        //aksi += '<a class="btn default btn-xs red" id="btn-hapus-measurement"><i class="fa fa-close"></i> </a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var initTableOccupancy = function () {
        var uriId = $('#UriId').val();
        $('#table-occupancy').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataOccupancy/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "F_VALID_FROM",
                    "class": "dt-body-center"
                },
                {
                    "data": "F_VALID_TO",
                    "class": "dt-body-center"
                },
                {
                    "data": "REASON",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "STATUS",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        if (full.CONTRACT_NO == null) {
                            var aksi = '<a data-toggle="modal" href="#editOccupancy" class="btn default btn-xs green" id="btn-ubah-occupancy"><i class="fa fa-edit"></i> </a>';

                            return aksi;
                        }
                        else {
                            var aksi = '<button data-toggle="modal" href="#editOccupancy" class="btn default btn-xs green" id="btn-ubah-occupancy" disabled><i class="fa fa-edit"></i> </button>';

                            return aksi;
                        }
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    // Ubah status measurement
    var ubahstatusmeasurement = function () {
        $('body').on('click', 'tr #btn-status-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/RentalObject/UbahStatusMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var initTableMaps = function () {
        var uriId = $('#UriId').val();
        $('#table-maps').DataTable({
            "ajax": {
                "url": "/RentalObject/GetDataMaps/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "NO_URUT",
                },
                {
                    "data": "LATITUDE",
                },
                {
                    "data": "LONGITUDE"
                },
                
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#editMaps" class="btn default btn-xs green" id="btn-ubah-maps"><i class="fa fa-edit"></i> </a>';
                        aksi += '<a class="btn default btn-xs red" id="delete-maps"><i class="fa fa-trash"></i></a>';

                        //aksi += '<a class="btn default btn-xs red" id="btn-hapus-measurement"><i class="fa fa-close"></i> </a>';
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    // Ubah status fixture fitting
    var ubahstatusfixturefitting = function () {
        $('body').on('click', 'tr #btn-status-fixture-fitting', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-fixture-fitting').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/RentalObject/UbahStatusFixtureFitting",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    // Untuk ditampilkan di textbox edit occupancy
    var DataUpdateOccupancy = function () {
        $('body').on('click', 'tr #btn-ubah-occupancy', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-occupancy').DataTable();
            var data = table.row(baris).data();

        });
    }

    // Untuk ditampilkan di textbox edit measurement
    var DataUpdateMeasurement = function () {
        $('body').on('click', 'tr #btn-ubah-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            $('#VALID_FROM_M').val('');
            $('#VALID_TO_M').val('');

            $("#MEASUREMENT_ID").val(data["ID"]);
            $("#AMOUNT_M").val(data["AMOUNT"]);
            $("#VALID_FROM_M").val(data["VAL_FROM"]);
            $("#VALID_TO_M").val(data["VAL_TO"]);

        });
    }

    // Untuk ditampilkan di textbox edit fixture fitting
    var DataUpdateFixtureFitting = function () {
        $('body').on('click', 'tr #btn-ubah-fixture-fitting', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-fixture-fitting').DataTable();
            var data = table.row(baris).data();

            $('#VALID_FROM_FF_F').val('');
            $('#VALID_TO_FF_F').val('');

            $("#FIXTURE_FITTING_ID").val(data["ID"]);
            $("#POWER_CAPACITY_F").val(data["POWER_CAPACITY"]);
            $("#VALID_FROM_FF_F").val(data["VAL_FROM"]);
            $("#VALID_TO_FF_F").val(data["VAL_TO"]);
        });
    }

    // Untuk ditampilkan di textbox edit map
    var DataUpdateMaps = function () {
        $('body').on('click', 'tr #btn-ubah-maps', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-maps').DataTable();
            var data = table.row(baris).data();

            $("#ID_MAPS").val(data["ID"]);
            $("#LATITUDE").val(data["LATITUDE"]);
            $("#LONGITUDE").val(data["LONGITUDE"]);
            $("#NO_URUT").val(data["NO_URUT"]);

        });
    }

    //Function untuk simpan fixture fitting RO
    var save_fixture_fitting = function () {
        $('#save-fixture_fitting').click(function () {
            var RO_NUMBER = $('#RO_NUMBER').val();
            var FIXTURE_FITTING_CODE = $('#FIXTURE_FITTING_CODE').val();
            var VALID_FROM = $('#VALID_FROM_FF').val();
            var VALID_TO = $('#VALID_TO_FF').val();
            var POWER_CAPACITY = $('#POWER_CAPACITY').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addFixtureFitting').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (RO_NUMBER && VALID_FROM) {
                    var param = {
                        RO_NUMBER: RO_NUMBER,
                        FIXTURE_FITTING_CODE: FIXTURE_FITTING_CODE,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        POWER_CAPACITY: POWER_CAPACITY
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/AddDataDetailFixtureFitting",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addFixtureFitting').modal('hide');
                            $('#table-fixture-fitting').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                $('#addFixtureFitting').modal('hide');
                                swal('Failed', data.message, 'error');
                            });
                        }
                    });
                } else {
                    $('#addFixtureFitting').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    //Function untuk simpan measurement RO
    var save_measurement = function () {
        $('#save-measurement').click(function () {
            //alert("asdf");
            var RO_NUMBER = $('#RO_NUMBERA').val();
            var MEASUREMENT_TYPE = $('#MEASUREMENT_TYPE').val();
            var VALID_FROM = $('#VALID_FROMA').val();
            var VALID_TO = $('#VALID_TOA').val();
            var AMOUNT = $('#AMOUNT').val();
            var UNIT = $('#UNIT').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addMeasurement').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (RO_NUMBER && MEASUREMENT_TYPE && VALID_FROM && AMOUNT && UNIT) {
                    var param = {
                        RO_NUMBER: RO_NUMBER,
                        MEASUREMENT_TYPE: MEASUREMENT_TYPE,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        AMOUNT: AMOUNT,
                        UNIT: UNIT
                    };

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/AddDataDetailMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addMeasurement').modal('hide');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                $('#addMeasurement').modal('hide');
                                swal('Failed', data.message, 'error');
                            });
                        }
                    });
                } else {
                    $('#addMeasurement').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    // alert("warning");
                }
            }
        });
    }

    //Function untuk simpan measurement RO
    var save_occupancy = function () {
        $('#save-occupancy').click(function () {
            //alert("asdf");
            var RO_NUMBER = $('#RO_NUMBER_O').val();
            var VALID_FROM = $('#VALID_FROM_O').val();
            var VALID_TO = $('#VALID_TO_O').val();
            var VACANCY_REASON = $('#VACANCY_REASON').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#addOccupancy').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (RO_NUMBER && VALID_FROM && VACANCY_REASON) {
                    var param = {
                        RO_NUMBER: RO_NUMBER,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        VACANCY_REASON: VACANCY_REASON
                    };

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/AddDataOccupancy",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addOccupancy').modal('hide');
                            $('#table-occupancy').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                $('#addOccupancy').modal('hide');
                                swal('Failed', data.message, 'error');
                            });
                        }
                    });
                } else {
                    $('#addOccupancy').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                    // alert("warning");
                }
            }
        });
    }

    var saveMaps = function () {
        $('#save-maps').click(function () {
            var LATITUDE = $('#LATITUDE').val();
            var LONGITUDE = $('#LONGITUDE').val();
            var RO_NUMBER = $('#RO_NUMBERMAPS').val();
            var NO_URUT = $('#NO_URUT').val();
            var ID = $('#ID_MAPS').val();
            if (ID == "") {
                var list = [];
                $(".list_row").each(function () {
                    list.push({
                        'LATITUDE': $(this).find('#LATITUDE').val(),
                        'LONGITUDE': $(this).find('#LONGITUDE').val(),
                        'NO_URUT': $(this).find('#NO_URUT').val()
                    });
                });
                for (var i = 0; i < list.length; i++) {
                    LATITUDE = list[i].LATITUDE;
                    LONGITUDE = list[i].LONGITUDE;
                    RO_NUMBER = $('#RO_NUMBERMAPS').val();
                    NO_URUT = list[i].NO_URUT;
                    ID = 0;
                    controlsave(ID.toString(), RO_NUMBER.toString(), LATITUDE.toString(), LONGITUDE.toString(), NO_URUT.toString());
                }
            } else {
                if (LATITUDE && LONGITUDE && NO_URUT) {
                    controlsave(ID, RO_NUMBER, LATITUDE, LONGITUDE, NO_URUT);
                } else {
                    $('#addMaps').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        })
    }

    var controlsave = function (ID, RO_NUMBER, LATITUDE, LONGITUDE, NO_URUT) {
        var param = {
            ID: ID,
            RO_NUMBER: RO_NUMBER,
            LATITUDE: LATITUDE,
            LONGITUDE: LONGITUDE,
            NO_URUT: NO_URUT,
        };
        App.blockUI({ boxed: true });
        var req = $.ajax({
            contentType: "application/json",
            data: JSON.stringify(param),
            method: "POST",
            url: "/RentalObject/AddMaps",
            timeout: 30000
        });

        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                swal('Success', data.message, 'success');
                $('#addMaps').modal('hide');
                $('#table-maps').dataTable().api().ajax.reload();
            } else {
                swal('Failed', data.message, 'error').then(function (isConfirm) {
                    $('#addMaps').modal('hide');
                    swal('Failed', data.message, 'error');
                    $('#table-maps').dataTable().api().ajax.reload();
                });
            }
        });
    }

    var deletefixturefitting = function () {
        $('body').on('click', 'tr #btn-hapus-fixture-fitting', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-fixture-fitting').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/RentalObject/DeleteDataFixtureFitting",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var deleteMaps = function () {
        $('body').on('click', 'tr #delete-maps', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-maps').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/RentalObject/DeleteMaps",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var deletemeasurement = function () {
        $('body').on('click', 'tr #btn-hapus-measurement', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-measurement').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this data?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/RentalObject/DeleteDataMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var addSertifikat = function () {
        $('#btn-add-sertifikat').click(function () {
            $('#viewSertifikat').modal('show');
            $('#table-sertifikat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataSertifikat/",
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SERTIFIKAT",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "SERTIFIKAT_TYPE",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "LUAS_LAHAN",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0, '', ' m²'),
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a id="btn-save-sertifikat" class="btn btn-icon-only green" title="Save" ><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 5,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });

        $('#table-sertifikat').on('click', 'tr #btn-save-sertifikat', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-sertifikat').DataTable();
            var data = table.row(baris).data();
            //console.log(data['ATTACHMENT']);
            //document.getElementById('SERTIFIKAT').innerHTML = data['ATTACMENT'];
            $('#SERTIFIKAT_ID').val(data['ATTACHMENT_ID']);
            $('#RO_CERTIFICATE_NUMBER').val(data['NO_SERTIFIKAT']);
            //var serti_id = data['ATTACHMENT_ID'];
            //console.log(serti_id);
            //console.log(data)
            //var xInd = $('#INDUSTRY').val();
            //xoTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_TANAH_RO'], data['LUAS_BANGUNAN_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus-ob"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);

            $('#viewSertifikat').modal('hide');
        });
    }


    // Update data sesuai dengan mockup
    var updatero = function () {
        $('#btn-update-ro').click(function () {
            var RO_NUMBER = $('#RO_NUMBER').val();
            var BE_ID = $('#BE_ID').val();
            var RO_TYPE = $('#RO_TYPE').val();
            var RO_NAME = $('#RO_NAME').val();
            var RO_CERTIFICATE_NUMBER = $('#RO_CERTIFICATE_NUMBER').val();
            var RO_ADDRESS = $('#RO_ADDRESS').val();
            var RO_POSTALCODE = $('#RO_POSTALCODE').val();
            var RO_CITY = $('#RO_CITY').val();
            var RO_PROVINCE = $('#RO_PROVINCE').val();
            var MEMO = $('#MEMO').val();
            var VALID_FROM = $('#VALID_FROM').val();
            var VALID_TO = $('#VALID_TO').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var ZONE_RIP = $('#ZONE_RIP').val();
            //var LOCATION = $('#LOCATION').val();
            //var FUNCTION = $('#FUNCTION').val();
            var USAGE_TYPE_ID = $('#USAGE_TYPE').val();
            var FUNCTION_ID = $('#FUNCTION_ID').val();
            var KODE_ASET = $('#KODE_ASET').val();
            var LINI = $('#LINI').val();
            var PERUNTUKAN = $('#PERUNTUKAN').val();
            var SERTIFIKAT_ID = $('#SERTIFIKAT_ID').val();
            var KETERANGAN = $('#KETERANGAN').val();
            var SERTIFIKAT_OR_NOT = $('#sertifikat_edit').val();
           // console.log(SERTIFIKAT_ID);

            //MENGAMBIL NILAI LATITUDE DAN LONGITUDE//
           // var LATITUDE = "";
            //var LONGITUDE = "";
            //var baris = $(this).parents('tr')[0];
            //var table = $('#table-maps').DataTable();
            //var data = table.row(baris).data();
            //if (data !== "") {
             //   var LATITUDE = data['LATITUDE'];
              //  var LONGITUDE = data['LONGITUDE'];
            //} else {
              //  var LATITUDE = "";
                //var LONGITUDE = "";
            //}
            var LATITUDE = "";
            var LONGITUDE = "";
            var baris = $('#table-maps').find('tr').eq(0);
            var table = $('#table-maps').DataTable();
            var data = table.row(baris).data();
            console.log(typeof data);
            if (data === "" || data == null || data === undefined) {
                var LATITUDE = "";
                var LONGITUDE = "";
            } else {
                var LATITUDE = data['LATITUDE'];
                var LONGITUDE = data['LONGITUDE'];
            }
            
            //console.log(LATITUDE);

            //console.log('FUNCTION ID ' + FUNCTION_ID);

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                swal('Failed', 'Invalid Date Range', 'error');
            } 
            else {
                if (BE_ID && RO_TYPE && RO_NAME && ZONE_RIP && VALID_FROM && USAGE_TYPE && PROFIT_CENTER && RO_PROVINCE && FUNCTION_ID) {
                    var param = {
                        RO_NUMBER: RO_NUMBER,
                        BE_ID: BE_ID,
                        RO_NAME: RO_NAME,
                        RO_CERTIFICATE_NUMBER: RO_CERTIFICATE_NUMBER,
                        RO_ADDRESS: RO_ADDRESS,
                        RO_POSTALCODE: RO_POSTALCODE,
                        RO_CITY: RO_CITY,
                        RO_PROVINCE: RO_PROVINCE,
                        MEMO: MEMO,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        PROFIT_CENTER: PROFIT_CENTER,
                        RO_TYPE_ID: RO_TYPE,
                        ZONE_RIP_ID: ZONE_RIP,
                        //LOCATION_ID: LOCATION,
                        //FUNCTION_ID: FUNCTION,
                        USAGE_TYPE_ID: USAGE_TYPE_ID,
                        FUNCTION_ID: FUNCTION_ID,
                        KODE_ASET: KODE_ASET,
                        LATITUDE: LATITUDE,
                        LONGITUDE: LONGITUDE,
                        LINI: LINI,
                        PERUNTUKAN: PERUNTUKAN,
                        SERTIFIKAT_ID: SERTIFIKAT_ID,
                        KETERANGAN: KETERANGAN,
                        SERTIFIKAT: SERTIFIKAT_OR_NOT
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/RentalObject";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/RentalObject";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    // Update data detail measureent
    var updateOccupancy = function () {
        $('#save-edit-occupancy').click(function () {
            var VACANCY_REASON = $('#VACANCY_REASON_O').val();
            //var VALID_FROM = $('#VALID_FROM_OA').val();
            //var VALID_TO = $('#VALID_TO_OA').val();
            var RO_NUMBER = $('#RO_NUMBER_OA').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#editOccupancy').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (VACANCY_REASON) {
                    var param = {
                        VACANCY_REASON: VACANCY_REASON,
                        //VALID_FROM: VALID_FROM,
                        //VALID_TO: VALID_TO,
                        RO_NUMBER: RO_NUMBER
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/EditDataOccupancy",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#editOccupancy').modal('hide');
                            swal('Success', data.message, 'success');
                            $('#table-occupancy').dataTable().api().ajax.reload();
                        } else {
                            $('#editOccupancy').modal('hide');
                            swal('Failed', data.message, 'error');
                        }
                    });
                } else {
                    $('#editOccupancy').modal('hide');
                    swal('Warning', 'Please Choose Vacancy Reason!', 'warning');
                }
            }
        });
    }

    // Update data detail measureent
    var updatemeasurement = function () {
        $('#save-edit-measurement').click(function () {
            var MEASUREMENT_ID = $('#MEASUREMENT_ID').val();
            var MEASUREMENT_TYPE_M = $('#MEASUREMENT_TYPE_M').val();
            var AMOUNT_M = $('#AMOUNT_M').val();
            var UNIT_M = $('#UNIT_M').val();
            var VALID_FROM_M = $('#VALID_FROM_M').val();
            var VALID_TO_M = $('#VALID_TO_M').val();

            if (Date.parse(VALID_FROM_M) > Date.parse(VALID_TO_M)) {
                $('#editMeasurement').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (MEASUREMENT_ID && AMOUNT_M && UNIT_M && VALID_FROM_M && VALID_TO_M) {
                    var param = {
                        ID: MEASUREMENT_ID,
                        MEASUREMENT_TYPE: MEASUREMENT_TYPE_M,
                        AMOUNT: AMOUNT_M,
                        UNIT: UNIT_M,
                        VALID_FROM: VALID_FROM_M,
                        VALID_TO: VALID_TO_M
                    };

                    //console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/EditDataMeasurement",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#editMeasurement').modal('hide');
                            swal('Success', data.message, 'success');
                            $('#table-measurement').dataTable().api().ajax.reload();
                        } else {
                            $('#editMeasurement').modal('hide');
                            swal('Failed', data.message, 'error');
                        }
                    });
                } else {
                    $('#editMeasurement').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    // Update data detail fixture fitting
    var updatefixturefitting = function () {
        $('#save-edit-fixture_fitting').click(function () {
            var FIXTURE_FITTING_ID = $('#FIXTURE_FITTING_ID').val();
            var FIXTURE_FITTING_CODE_F = $('#FIXTURE_FITTING_CODE_F').val();
            var VALID_FROM_FF_F = $('#VALID_FROM_FF_F').val();
            var VALID_TO_FF_F = $('#VALID_TO_FF_F').val();
            var POWER_CAPACITY_F = $('#POWER_CAPACITY_F').val();

            if (Date.parse(VALID_FROM_FF_F) > Date.parse(VALID_TO_FF_F)) {
                $('#editFixtureFitting').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (FIXTURE_FITTING_ID && VALID_FROM_FF_F && VALID_TO_FF_F) {
                    var param = {
                        ID: FIXTURE_FITTING_ID,
                        FIXTURE_FITTING_CODE: FIXTURE_FITTING_CODE_F,
                        VALID_FROM: VALID_FROM_FF_F,
                        VALID_TO: VALID_TO_FF_F,
                        POWER_CAPACITY: POWER_CAPACITY_F
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/RentalObject/EditDataFixtureFitting",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#editFixtureFitting').modal('hide');
                            swal('Success', data.message, 'success');
                            $('#table-fixture-fitting').dataTable().api().ajax.reload();
                        } else {
                            $('#editFixtureFitting').modal('hide');
                            swal('Failed', data.message, 'error');
                        }
                    });
                } else {
                    $('#editFixtureFitting').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    var getBE = function () {
        $.fn.select2.defaults.set("theme", "bootstrap");
        function formatRepo(repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.BE_NAME + " (" + repo.id + ")</div></div>";

            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.BE_NAME || repo.text;
        }

        $(".kode-be").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/RentalObject/GetBEID",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "tekan enter lalu ketikkan nama pelanggan"
        });
    }

    //Combobox Option Measurement
    var cbmeasurement2 = function () {
        $.ajax({
            type: "GET",
            url: "/RentalObject/GetDataCBMeasurement",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].ID + "'>" + jsonList.data[i].REF_DESC + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#MEASUREMENT_TYPE2").append(listItems);
                });

            }
        });
    }

    var generate_ro_number = function () {
        $.ajax({
            type: "GET",
            url: "/RentalObject/GetRO_NUMBER",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    var listItemsText = "";
                    /*
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].RO_NUMBER + "'>" + jsonList.data[i].RO_NUMBER + ' ' + jsonList.data[i].RO_NUMBER + "</option>";
                    }
                    $("#BE_ID").append(listItems);
                    */

                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItemsText += jsonList.data[i].RO_NUMBER;
                    }
                    //document.getElementById('RO_NUMBER').value = "TEST";
                    $("#RO_NUMBER").val(listItemsText);
                });
            }
        });
    }
    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var RO_NUMBER = $('#ID_ATTACHMENT').val();
            //console.log(BE_ID);

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {

                    console.log(data);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    //console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/RentalObject/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { RO_NUMBER: RO_NUMBER },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {
        var uriId = $('#UriId').val();
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var fileName = data['FILE_NAME'];

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        //data: "id=" + data["ID_ATTACHMENT"],
                        //data: "{ id:'" + data["ID_ATTACHMENT"] + "', directory:'" + data["DIRECTORY"]+"}",
                        data: {
                            id: data["ID"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/RentalObject/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var initTableAttachment = function () {
        var uriId = $('#UriId').val();
        $('#table-attachment').DataTable({

            "ajax":
            {
                "url": "/RentalObject/GetDataAttachment/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }

            },
            "columns": [
                //{
                //    "data": "FILE_NAME"
                //},
                {
                    targets: 2,
                    render: function (data, type, full) {
                        return '<img src="' + '../../REMOTE/RentalObject/' + uriId + '\\' + full.FILE_NAME + '"  style ="width: 50px; height: auto;" >'
                    }
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                        var x = '<a href ="' + '../../REMOTE/RentalObject/' + uriId + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                        x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        //x += '<img src="file:///C:\REMOTE\BusinessEntity\123\FILE_ATTACHMENT_123_20170221113854.jpg">';
                        return x;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });

    }

    function PinPoint(type) {
        if (type == 1) {
            var req = $.ajax({
                contentType: "application/json",
                data: "RO_NUMBER=" + $('#RO_NUMBERMAPS').val(),
                method: "get",
                url: "/RentalObject/PinPoint",
                timeout: 30000
            });
        } else {

            var req = $.ajax({
                contentType: "application/json",
                data: "RO_NUMBER=" + $('#RO_NUMBERVIEWMAPS').val(),
                method: "get",
                url: "/RentalObject/PinPoint",
                timeout: 30000
            });
        }
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                if (type == 1) {
                    Maps(data.message.LATITUDE, data.message.LONGITUDE)
                } else {
                    MapsView(data.message.LATITUDE, data.message.LONGITUDE)
                }
            } else {
                //swal('Failed', data.message, 'error');
            }
        });
    }
    var marker;
    var markerglobal;
    var tempmarker;
    var globalarr = -1;

    function Maps(Lat, Long) {
        var element = document.getElementById("map");
        var mapTypeIds = [];
        for (var type in google.maps.MapTypeId) {
            mapTypeIds.push(google.maps.MapTypeId[type]);
        }
        mapTypeIds.push("satellite");

        map = new google.maps.Map(element, {
            center: new google.maps.LatLng(Lat, Long),
            zoom: 16,
            mapTypeId: "satellite",
            // disable the default User Interface
            disableDefaultUI: true,
            // add back fullscreen, streetview, zoom
            zoomControl: true,
            streetViewControl: true,
            //fullscreenControl: true,
            center: new google.maps.LatLng(Lat, Long),
            mapTypeControlOptions: {
                mapTypeIds: mapTypeIds
            }
        });

        // Create the search box and link it to the UI element.
        const input = document.getElementById("pac-input");
        const searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener("bounds_changed", () => {
            searchBox.setBounds(map.getBounds());
        });
        let markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            // Clear out the old markers.
            markers.forEach((marker) => {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                const icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25),
                };
                // Create a marker for each place.
                markers.push(
                    new google.maps.Marker({
                        map,
                        icon,
                        title: place.name,
                        position: place.geometry.location,
                    })
                );

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });

        ID = $('#ID_MAPS').val();
        LATITUDE = $('#LATITUDE').val();
        LONGITUDE = $('#LONGITUDE').val();
        map.mapTypes.set("satellite", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                // See above example if you need smooth wrapping at 180th meridian
                return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));

        if (ID != "") {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(LATITUDE, LONGITUDE),
                map: map,
            });

            marker.setMap(map);
        }
        tempmarker = [];
        markerglobal = [];
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng, ID);
        });
    }

    function MapsView(Lat, Long) {
        var element = document.getElementById("view-maps");
        var mapTypeIds = [];
        for (var type in google.maps.MapTypeId) {
            mapTypeIds.push(google.maps.MapTypeId[type]);
        }
        mapTypeIds.push("satellite");

        maps = new google.maps.Map(element, {
            center: new google.maps.LatLng(Lat, Long),
            zoom: 16,
            mapTypeId: "satellite",
            // disable the default User Interface
            disableDefaultUI: true,
            // add back fullscreen, streetview, zoom
            zoomControl: true,
            streetViewControl: true,
            //fullscreenControl: true,
            center: new google.maps.LatLng(Lat, Long),
            mapTypeControlOptions: {
                mapTypeIds: mapTypeIds
            }
        });

        maps.mapTypes.set("satellite", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                // See above example if you need smooth wrapping at 180th meridian
                return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));

        var req = $.ajax({
            contentType: "application/json",
            data: "RO_NUMBER=" + $('#RO_NUMBERVIEWMAPS').val(),
            method: "get",
            url: "/RentalObject/GetMarker",
            timeout: 30000
        });

        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {

                var marks = data.message;
                var flightPlanCoordinates = [];
                for (var i = 0; i < marks.length; i++) {
                    flightPlanCoordinates.push({ lat: parseFloat(marks[i].LATITUDE), lng: parseFloat(marks[i].LONGITUDE) });
                }
                flightPlanCoordinates.push({ lat: parseFloat(marks[0].LATITUDE), lng: parseFloat(marks[0].LONGITUDE) });
                console.log(flightPlanCoordinates);
                var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                flightPath.setMap(maps);
            }
        });

        //if (ID != "") {
        //    marker = new google.maps.Marker({
        //        position: new google.maps.LatLng(LATITUDE, LONGITUDE),
        //        map: map,
        //    });

        //    marker.setMap(map);
        //}

    }

    function placeMarker(location, ID) {
        if (ID == "") {
            var table = $('#table-maps').dataTable();
            var length = table.fnSettings().fnRecordsTotal();
            var x = $(".list_row").length;
            x = parseInt(x);
            x = x + length;
            x++;

            marker = new google.maps.Marker({
                id:x,
                position: location,
                map: map
            });

            markerglobal.push(marker);
            
            var newRow = '<div class="row list_row">' +
                            '<div class="col-md-3" >' +
                                '<div class="form-group">' +
                                '<input type="number" name="NO_URUT" id="NO_URUT" class="form-control" value="' + x + '">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '<input type="text" name="LONGITUDE" id="LONGITUDE" class="form-control longitude" value="' + marker.getPosition().lng() + '">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '<input type="text" name="LATITUDE" id="LATITUDE" class="form-control latitude" value="' + marker.getPosition().lat() + '">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '<button type="button" class="btn btn-danger btn-sm deletepin" title="Hapus"><i class="fa fa-trash"></i></button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            $('.barisdata').append(newRow);
        } else {
            marker.setPosition(location);
            $('#LATITUDE').val(marker.getPosition().lat());
            $('#LONGITUDE').val(marker.getPosition().lng());
        }

        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();

        tempmarker.push({ "Lat": lat, "Long": lng });
    }

    function deleterow() {
        $('.barisdata').on('click', '.deletepin', function () {
            globalarr++;
            $(this).parents('.list_row')[0].remove();
            var x = $(this).parents('.list_row')[0];
            var live_str = $('<div>', { html: x });
            var id = live_str.find('#NO_URUT').val();
            id = parseInt(id);

            var table = $('#table-maps').dataTable();
            var length = table.fnSettings().fnRecordsTotal();

            var x = $(".list_row").length;
            x = parseInt(x);

            var dataf = id - x + length - globalarr;

            setMapOnAll(id);
        });
    };

    var setMapOnAll = function (id) {
        for (let i = 0; i < markerglobal.length; i++) {
            if (markerglobal[i].id == id) {
                markerglobal[i].setMap(null);
                markerglobal.splice(i, 1);
                console.log(i);
            }
        }
    };

    function openModalMaps() {

        $('body').on('click', '#openModalMaps', function () {
            $('#addMaps').modal('show');
            $('#LATITUDE').val("");
            $('#LONGITUDE').val("");
            $('#ID_MAPS').val(null);
            $('.barisdata').html("");
            PinPoint(1);
        });

        $('body').on('click', 'tr #btn-ubah-maps', function () {
            $('#txt-judul').text('Edit');
            $('#addMaps').modal('show');
            var oTable = $('#table-maps').dataTable();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $('.barisdata').html("");
            var newRow = '<div class="row list_row">' +
                            '<div class="col-md-3" >' +
                                '<div class="form-group">' +
                                '<input type="number" name="NO_URUT" id="NO_URUT" class="form-control" value="">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '<input type="text" name="LONGITUDE" id="LONGITUDE" class="form-control longitude" value="">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '<input type="text" name="LATITUDE" id="LATITUDE" class="form-control latitude" value="">' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<div class="form-group">' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            $('.barisdata').append(newRow);

            $('#LATITUDE').val(aData.LATITUDE);
            $('#LONGITUDE').val(aData.LONGITUDE);
            $('#ID_MAPS').val(aData.ID);
            $('#NO_URUT').val(aData.NO_URUT);
            
            PinPoint(1);

        });

        $('body').on('click', '#openModalViewMaps', function () {

            $('#viewMaps').modal('show');

            PinPoint(2);

        });
    }

    return {
        init: function () {
            save_fixture_fitting();
            initTableFixtureFitting();
            deletefixturefitting();
            deletemeasurement();
            save_measurement();
            initTableMeasurement();
            save_occupancy();
            openModalMaps();
            batal();
            sertifikat();
            simpanadd();
            updatero();
            //generate_ro_number();
            DataUpdateOccupancy();
            DataUpdateMeasurement();
            DataUpdateFixtureFitting();

            updatemeasurement();
            updatefixturefitting();
            updateOccupancy();

            ubahstatusmeasurement();
            ubahstatusfixturefitting();
            saveMaps();
            deleterow();

            initTableOccupancy();
            fupload();
            deleteAttachment();
            initTableAttachment();

            initTableMaps();
            deleteMaps();
            DataUpdateMaps();
            addSertifikat();
        }
    };
}();

$(document).ready(function () {
    $('#example').DataTable({
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
});

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterROAdd.init();
    });
}