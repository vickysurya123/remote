﻿var DataSurat = function () {
    //document.forms["contract-surat"]["disetujui"].checked = true;
    if ($("#TANGGAL").val()) {
        $("#ditentukan").click();
        $("#tglHidden").show();
        //document.forms["contract-surat"]["ditentukan"].checked = true;
        //document.forms["contract-surat"]["disetujui"].checked = false;
    } else {
        $("#disetujui").click();
    }

    var DataEmployee = function () {
        $("#TTD").select2({
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/DisposisiList/GetDataEmployee",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data);
            var atasnama = data.text.split(' - ');
            console.log(atasnama[1]);
            $('#ATAS_NAMA').val(atasnama[1]);
        });

        $("#PARAF").select2({
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/DisposisiList/GetDataEmployee",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.ID,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        $("#TEMBUSAN").select2({
            allowClear: true,
            width: '100%',
            multiple: true,
            ajax: {
                url: "/TransContractOffer/GetDataUserEmail",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.EMAIL,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        $("#KEPADA").select2({
            allowClear: true,
            width: '100%',
            multiple: true,
            ajax: {
                url: "/TransContractOffer/GetDataUserEmail",
                dataType: "json",
                contentType: "application/json",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    }
                },

                processResults: function (result) {
                    var data = {
                        results: $.map(result.results, function (item) {
                            return {
                                id: item.EMAIL,
                                text: item.EMPLOYEE_NAME
                            };
                        }),
                    };
                    return data;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            placeholder: ""
        });
        var optArr = [];
        var optArrKE = [];
        var optArrTembusan = [];
        var optArrKepada = [];
        var namaKepada = nama_kepada.split(";");
        var KepadaId = kepada.split(";");
        var namaTembusan = nama_tembusan.split(";");
        var tembusanId = tembusan.split(";");
        var kepadaEksternal = kepada_eksternal.split(';');
        var tembusanEksternal = tembusan_eksternal.split(';');
        tembusanId.forEach(function (e, i) {
            optArrTembusan.push(e)
        });
        namaTembusan.forEach(function (e,i) {
            var newOption = new Option(e, optArrTembusan[i], true, true);
            optArr.push(newOption);
        });
        $('#TEMBUSAN').append(optArr).trigger('change');

        KepadaId.forEach(function (e, i) {
            optArrKepada.push(e)
        });
        namaKepada.forEach(function (e, i) {
            var newOption = new Option(e, optArrKepada[i], true, true);
            console.log(newOption);
            optArrKE.push(newOption);
        });
        // $('#KEPADA').append(optArrKE).trigger('change');

        $.each(kepadaEksternal, function (i, e) {
            $('<div class="vR"><span class="vN"><div class="vT">' + e + '</div><div class="vM" onclick="hapusKepada($(this))"></div><input type="hidden" name="ti[]" value="' + e + '"</span></div>').insertBefore('#KEPADA_EKSTERNAL');
        });

        $.each(tembusanEksternal, function (i, e) {
            $('<div class="vR"><span class="vN"><div class="vT">' + e + '</div><div class="vM" onclick="hapusTembusan($(this))"></div><input type="hidden" name="to[]" value="' + e + '"</span></div>').insertBefore('#TEMBUSAN_EKSTERNAL');
        });

        var newOption = new Option(nama_ttd, ttd, false, false);
        $('#TTD').append(newOption).trigger('change');
        $("#STATUS_KIRIM").select2({});

        $('body').on('keydown', '#TEMBUSAN_EKSTERNAL', function (e) {
            var text = $('#TEMBUSAN_EKSTERNAL').val();
            if (e.key === "Enter") {
                if (text != '') {
                    $('<div class="vR"><span class="vN"><div class="vT">' + text + '</div><div class="vM" onclick="hapusTembusan($(this))"></div><input type="hidden" name="to[]" value="' + text + '"</span></div>').insertBefore('#TEMBUSAN_EKSTERNAL');
                    $('#TEMBUSAN_EKSTERNAL').val('');
                }
            }
        });

        $('body').on('keydown', '#KEPADA_EKSTERNAL', function (e) {
            var text = $('#KEPADA_EKSTERNAL').val();
            if (e.key === "Enter") {
                if (text != '') {
                    $('<div class="vR"><span class="vN"><div class="vT">' + text + '</div><div class="vM" onclick="hapusKepada($(this))"></div><input type="hidden" name="ti[]" value="' + text + '"</span></div>').insertBefore('#KEPADA_EKSTERNAL');
                    $('#KEPADA_EKSTERNAL').val('');
                }
            }
        });
    };

    var i = 0;
    $('#add_attachment').click(function () {
        i++
        var a = `<div class="row" id="attc` + i + `" style="margin:10px 0px">
                            <div class="col-md-10">
                                <input class="form-control" type="file" accept=".pdf" id="file_`+ i + `" name="filex" />
                                <a href="" id="href_` + i + `" download style="display:none"></a>
                            </div>
                            <div class="col-md-2">

                                <input type="hidden" id="path_`+ i + `" class="path"/>
                                <button type="button" onClick="save_temp(`+ i + `)" class="btn btn-success btn-sm" id="btn_save_temp_` + i + `"><i class="fa fa-check"></i></button>
                                <button type="button" onClick="hapus_attc(`+ i + `)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        `;
        $('#attc').append(a);
    })
    window.hapus_attc = function (id) {
        $('#attc' + id).remove();
        path_build();
    }

    function uploadImage(file) {
        var formData = new FormData();
        formData.append("files", file);
        $.ajax({
            data: formData,
            type: "POST",
            url: '/TransContractOffer/UploadFile',
            cache: false,
            contentType: false,
            processData: false,
            success: function (FileUrl) {
                console.log(FileUrl);
                $('#ISI_SURAT').summernote("insertImage", FileUrl, 'filename');
            },
            error: function (data) {
                //alert(data.responseText);
            }
        });
    }

    var initTableAttachment = function () {
        var idSurat = $('#ID').val();
        $('#table-attachment').DataTable({
            "ajax":
            {
                "url": "/DisposisiList/GetDataAttachment/" + idSurat,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    targets: 2,
                    render: function (data, type, full) {
                        //return '<img src="' + '../../TMP/Information/' + uriId + '\\' + full.File_Name + '"  style ="width: 50px; height: auto;" >'
                        //return '<a href="' + '../../TMP/Information/' + uriId + '\\' + full.File_Name + '" target="_blank">' + full.File_Name + '</a>'
                        return '<a href="' + full.DIRECTORY + '" target="_blank">' + full.FILE_NAME + '</a>'
                    }
                },
                {
                    "render": function (data, type, full) {
                        //var x = '<a href ="' + '../../TMP/Information/' + uriId + '\\' + full.File_Name + '" target="_blank" class="btn default btn-xs blue"><i class="fas fa-search-plus"></i></a>';
                        var x = '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                        return x;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "lengthChange": false,
            "info": false,
            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },
            "filter": false
        });
    };

    var deleteAttachment = function () {
        var uriId = $('#ID').val();
        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();
            var fileName = data['FILE_NAME'];
            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/DisposisiList/DeleteDataAttachment",
                        timeout: 30000
                    });
                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var summernote = function () {
        $('#ISI_SURAT').summernote({
            placeholder: '',
            tabsize: 2,
            height: 120,
            focus: true,                  // set focus to editable area after initializing summernote  
            callbacks: {
                onImageUpload: function (files) {
                    for (let i = 0; i < files.length; i++) {
                        uploadImage(files[i]);
                    }
                }
            },
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['codeview']]
            ]
        });
    }
    $("#hidden-table").show();
    //LINE
    listParaf.forEach(function (data, i) {
        var tpl = $('template#tmpl-line');
        //indexLine++;
        var template = tpl.html().replace(/_INDEX_/g, i);
        $('#line > tbody').append(template);
        checkTable();
        var tr = $('#line tbody tr:last');
        tr.find('#HIRARKI').val(data.HIRARKI);
        tr.find('#NAMA_PARAF').val(data.NAMA);
        tr.find('#ID').val(data.ID);
    });
    var indexLine = 0;
    function addRow() {
        var tpl = $('template#tmpl-line');
        indexLine++;
        var template = tpl.html().replace(/_INDEX_/g, indexLine);
        $('#line > tbody').append(template);
        checkTable();

        var tr = $('#line tbody tr:last');
        tr.find('#HIRARKI').val($('#line tbody tr').length);
        var data = $('#PARAF').select2('data')
        tr.find('#NAMA_PARAF').val(data[0].text);
        tr.find('#ID').val(data[0].id);
    }

    function removeLine(index) {
        var $tr = $('#line tbody tr[data-index="' + index + '"]');
        var line_id = $tr.find('#ID').val();
         if (line_id !== '') {
             $('<input>').attr({
                 type: 'hidden',
                 name: 'deleteLine[]',
                 value: line_id
             }).appendTo('#delete');
         }
        $tr.remove();
        reindexLine();
        checkTable();
    }

    $('#line > tbody').on('click', '[id$="btn-remove-paraf"]', function () {
        removeLine($(this).closest('tr').data('index'));;
    });

    function checkTable() {
        $('#line tbody tr.nodata').remove();
        if ($('#line tbody tr').length < 1) {
            //$('#line tbody').append('<tr class="nodata"><td colspan="2">No Data.</td></tr>');
        }
    }

    function reindexLine() {
        var newIndex = 1;
        $('#line tbody tr').each(function (i, e) {
            $(e).find('td:eq(0)').html(newIndex++ + '.');
        });
        checkTable();
    }

    $('#btn-add-paraf').on('click', function () {
        addRow();
    });
    $('#contract-surat input').on('change', function () {
        var ditentukan = $('input[name=inlineRadioOptions]:checked', '#contract-surat').val();
        var tanggal = "";
        //console.log(ditentukan);
        if (ditentukan == 1) {
            $("#tglHidden").hide();
            $("#TANGGAL").val(tanggal);
        } else {
            $("#tglHidden").show();
        }
    });
    var saveSurat = function () {
        $('#btn-save-surat').click(function () {
            $('#openPDF').modal('hide');
            //var MEMO = $('#MEMO').val();
            //var PEMARAF = $('#PEMARAF').val();
            var textareaValue = $('#ISI_SURAT').summernote('code');
            var dataNoSurat = $("#NO_SURAT").val();
            var dataSubject = $("#SUBJECT").val();
            var kepadaEksternal = '';
            var kpdEksternal = ($('input[name^=ti]'));
            $.each(kpdEksternal, function (e, i) {
                kepadaEksternal += (e > 0 ? ';' : '') + i.value;
            });
            if (textareaValue != "" && dataSubject != "" && ($("#KEPADA").val() != null || kepadaEksternal != "") && $("#TTD").val() != null) {
                App.blockUI({ boxed: true });
                var form = $("#contract-surat")[0];
                var data = new FormData(form);
                var nama_kepada = '';
                var kepada = '';
                var selections = ($("#KEPADA").select2('data'));
                selections.forEach(function (e, i) {
                    nama_kepada += (i > 0 ? ';' : '') + e.text;
                    kepada += (i > 0 ? ';' : '') + e.id;
                });

                var tembusan = '';
                var nama_tembusan = '';
                var selections = ($("#TEMBUSAN").select2('data'));
                selections.forEach(function (e, i) {
                    nama_tembusan += (i > 0 ? ';' : '') + e.text;
                    tembusan += (i > 0 ? ';' : '') + e.id;
                });
                var dataTtd = $('#TTD').select2('data');
                var list = [];
                var rows = $("#line tbody tr");
                $.each(rows, function (index, row) {
                    list.push({
                        'HIRARKI': $(row).find("#HIRARKI").val(),
                        'ID': $(row).find("#ID").val(),
                        'NAMA': $(row).find("#NAMA_PARAF").val(),
                    });
                });
                var tembusanEksternal = '';
                var tbsEksternal = ($('input[name^=to]'));
                $.each(tbsEksternal, function (e, i) {
                    tembusanEksternal += (e > 0 ? ';' : '') + i.value;
                });

                data.append('LIST_FILE', JSON.stringify(listfile));
                data.append('ISI_SURAT', textareaValue);
                data.append('KEPADA', kepada);
                data.append('TEMBUSAN', tembusan);
                data.append('NAMA_TEMBUSAN', nama_tembusan);
                data.append('NAMA_KEPADA', nama_kepada);
                data.append('TEMBUSAN_EKSTERNAL', tembusanEksternal);
                data.append('KEPADA_EKSTERNAL', kepadaEksternal);
                data.append('NAMA_TTD', dataTtd[0].text);
                data.append('CONTRACT_NO', $('#OFFER_ID').val());
                data.append('LIST_PARAF', JSON.stringify(list));
                var req = $.ajax({
                    type: 'POST',
                    url: '/TransContractOffer/EditDataSurat',
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: data,

                    success: function (message) {
                        //alert(message);
                    },
                    error: function () {
                        //alert("there was error uploading files!");
                    }
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.Status === 'S') {
                        swal('Success', data.Msg, 'success').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            window.location.href = '/TransContractOffer';
                        });
                    } else {
                        swal('Failed', data.Msg, 'error').then(function (isConfirm) {
                            //window.location.href = '/Role';
                            clear();
                            table.ajax.reload();
                        });
                    }
                });
            } else {
                swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
            }

        });
    };

    return {
        init: function () {
            DataEmployee();
            summernote();
            saveSurat();
            deleteAttachment();
            initTableAttachment();
        }
    };
}();

var listfile = [];
function save_temp(i) {
    App.blockUI({ boxed: true });
    var formData = new FormData();
    //formData.append("files", $("#file_" + i+").files[0]);
    formData.append('files', $('#file_' + i)[0].files[0]);
    $.ajax({
        data: formData,
        type: "POST",
        url: '/TransContractOffer/UploadTemp',
        enctype: 'multipart/form-data',
        cache: false,
        contentType: false,
        processData: false,
        success: function (FileUrl) {
            App.unblockUI();
            $('#path_' + i).val(FileUrl[0].DIRECTORY);

            $('#href_' + i).text(FileUrl[0].FILE_NAME);
            $('#href_' + i).attr("href", FileUrl[0].DIRECTORY)
            $('#href_' + i).show();
            $('#file_' + i).hide();
            $('#btn_save_temp_' + i).hide();
            swal('success', 'Sukses Upload File', 'success')
            path_build();
            listfile.push({
                'DIRECTORY': $('#href_' + i).attr("href"),
                'FILE_NAME': $('#href_' + i).text(),
            }); 
        },
        error: function (data) {
            App.unblockUI();
            //alert(data.responseText);
        }
    });
}
var LIST_FILE = "";
function path_build() {
    var text = "";
    $(".path").each(function (index) {
        text += (index == 0 ? "" : ";") + $(this).val();
    });
    LIST_FILE = text;
}
var TableDatatablesEditable = function () {
    //-------------------------------------------------BEGIN JS dt-add-trans-co.js-----------------------------------------------------
    //Declare variabel array untuk menampung condition type agar bisa digunakan di form memo
    var arrCondition = [];
    var arrObjectEdit = [];

    var arrCondType = [];
    var buah = [];
    var secondCellArray = [];

    var objTampungSR = {};
    var arrTampungSR = [];

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransContractOffer";
        });
    }
    var resetDatePicker = function () {
        //console.log("reset")
        $('.date-picker').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });

        $('.date-picker').each(function () {
            var date = $(this).val();
            if (date) {
                var datearray = date.split("/");
                var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                }).datepicker('setDate', new Date(newdate)); //set value
            } else {
                $(this).datepicker({
                    format: "dd/mm/yyyy"
                });
            }
        });
    }
    var hitung_bulan = function () {

        //FUNCTION Hitung Bulan
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();

            if (d2.getDate() >= d1.getDate())
                months++

            return months <= 0 ? 0 : months;
        }

        $('#CONTRACT_END_DATE').change(function () {
            var mulai = $('#CONTRACT_START_DATE').val();
            var akhir = $('#CONTRACT_END_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

            var dateAkhir = akhir;
            var datearrayAkhir = dateAkhir.split("/");
            var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

            //console.log(newdateMulai);
            //console.log(newdateAkhir);
            var tim = monthDiff(new Date(newdateMulai), new Date(newdateAkhir));
            $('#TERM_IN_MONTHS').val(tim);
        });

    }

    var hitung_tanggal_sampai = function () {
        function addMonths(date, count) {
            if (date && count) {
                var m, d = (date = new Date(+date)).getDate()

                date.setMonth(date.getMonth() + count, 1)
                m = date.getMonth()
                date.setDate(d)
                if (date.getMonth() !== m) date.setDate(0)
            }
            return date
        }

        $('#TERM_IN_MONTHS').change(function () {
            var tim = $('#TERM_IN_MONTHS').val();
            var mulai = $('#CONTRACT_START_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

            //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
            var f_date = new Date(newdateMulai);
            var n_tim = Number(tim);
            var end_date = addMonths(f_date, n_tim);
            var xdate = new Date(end_date);
            var f_date = xdate.toISOString().slice(0, 10);

            //format f_date menjadi dd/mm/YYYY
            var dateAkhir = f_date;
            var datearrayAkhir = dateAkhir.split("-");
            var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];
            $('#CONTRACT_END_DATE').val(newdateAkhir);

            //console.log(newdateAkhir);
        });
    }


    var setProfitCenter = function (vdata) {
        //console.log(vdata);
        $('#TERMINAL_NAME').val(vdata.data.TERMINAL_NAME);
        $('#PROFIT_CENTER').val(vdata.data.PROFIT_CENTER_ID);
        $('#BUSINESS_ENTITY_ID').val(vdata.data.BRANCH_ID);
        $('#BUSINESS_ENTITY_NAME').val(vdata.data.NAMA_PROFIT_CENTER);
        //var jsonList = data
        //var listItems = "";
        //for (var i = 0; i < jsonList.data.length; i++) {
        //    listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
        //}
        //$("#INDUSTRY").append(listItems);
    }

    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/TransRentalRequest/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        data.from = $('#from').val();
                        data.to = $('#to').val();
                        data.luas_tanah_from = $('#luas_tanah_from').val();
                        data.luas_tanah_to = $('#luas_tanah_to').val();
                        data.luas_bangunan_from = $('#luas_bangunan_from').val();
                        data.luas_bangunan_to = $('#luas_bangunan_to').val();
                        //console.log(data);
                    }

                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        
    }

    var filterRO = function () {
        $('#btn-filter-data-ro').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail-ro')) {
                $('#table-filter-detail-ro').DataTable().destroy();
            }

            $('#table-filter-detail-ro').DataTable({
                "ajax": {
                    "url": "GetDataFilterRO",
                    "type": "GET"
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-ro"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
    }

    /*
    var tablex = $('#condition');
    var oTable = tablex.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false,
        "columnDefs": [
            {
                "targets": [19],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [19],
                "visible": false
            }
        ]
    });
    */

    var addCondition = function () {

        $('#btn-add-condition').on('click', function () {

            $('#condition-option [value=""]').html(" -- Choose Condition Option -- ");
            $('#condition-option [value="Header Document"]').html("Header Document");
             
            // Ajax untuk ambil dan generate checkbox condition type dari parameter ref d
            $('#condition-option').change(function () {
                var condition_option = $('#condition-option').val();

                $('#cekbokList').html('');
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetCheckboxConditionType",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        for (var i = 0; i < jsonList.data.length; i++) {
                            var valueCombobox = jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2;

                            if (condition_option == 'Header Document') {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }
                            else {
                                if (valueCombobox == 'Z009 Administrasi|0' || valueCombobox == 'Z010 Warmeking|0') {
                                    listItems += "<input type='checkbox' disabled + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                                else {
                                    listItems += "<input type='checkbox'  + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";
                                }
                            }

                        }
                        $("#cekbokList").append(listItems);
                    }
                });
            });

        });
        
    }
    
    var showOldContract = function (ro, cond) {
        //console.log(ro, cond)
        var all_value = 0;
        $('.DETAIL_RO').html('');
        for (var i = 0; i < cond.length; i++) {

            var net_val = cond[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //var price = cond[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = cond[i].CALC_OBJECT;

            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_cond = calc_ob.split(' ')[1];

                for (var j = 0; j < ro.length; j++) {
                    ro_data = ro[j];
                    //console.log(ro_data);
                    var ro_id = ro_data.OBJECT_ID;
                    if (ro_id == ro_cond) {
                        var luas = Number(ro_data.LAND_DIMENSION) + Number(ro_data.BUILDING_DIMENSION);
                        var text = $('.DETAIL_RO').html() + '<tr><td>' + ro_data.OBJECT_ID + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CONTRACT_CURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO').html(text);
                    }
                }
            }
        }
        $('#CONTRACT_VALUE').val(all_value);
        $('.CONTRACT_VALUE').html(sep1000(all_value, true));
    }

    var initOldContract = function () {
        // init old contract
        var id_co = $("#OLD_CONTRACT_OFFER").val();
        var id_rr = $("#OLD_RENTAL_REQUEST").val();
        //console.log(id_co, id_rr)
        $('.old-contract').css({ 'display': 'none' });
        if (id_co) {
            $('.old-contract').css({ 'display': 'block' });
            $(".CONTRACT_START_NEW").html($("#CONTRACT_START_DATE").val().replace(/\//g, "."));
            $(".CONTRACT_END_NEW").html($("#CONTRACT_END_DATE").val().replace(/\//g, "."));
            $(".CONTRACT_PERIOD_NEW").html($("#TERM_IN_MONTHS").val());
            $(".CURRENCY").html('&nbsp;' + $("#CONTRACT_CURRENCY").val());
            $("#OLD_CONTRACT").val($("#OLD_CONTRACT").val());
            var arrRo;
            var arrCond;
            var req_Offer = $.ajax({
                type: "GET",
                "url": "/TransContractOffer/GetContractOffer/" + id_co,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (data.Status == "S") {
                        data = data.Message;
                        //console.log(data);

                        $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                        $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                        $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                    } else {
                        //console.log(data.Message);
                    }
                }
            });
            var req_Cond = $.ajax({
                type: "GET",
                "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    arrCond = data.data;
                    //console.log(arrCond);
                }
            });
            var req_Ro = $.ajax({
                type: "GET",
                "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    arrRo = data.data;
                    //console.log(arrRo);
                }
            });
            req_Ro.done(function () {
                req_Offer.done();
                req_Cond.done(function () {
                    showOldContract(arrRo, arrCond);
                });
            });
        }
    }
    
    var getDataObject = function () {
        var rental_request_no = $('#RENTAL_REQUEST_NO').val();

        var tabler = $('#rental-object-co');
        var oTable = tabler.dataTable({
            "lengthMenu": [
                [5, 15, 20, 50],
                [5, 15, 20, 50]
            ],
            "pageLength": 5,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "columnDefs": [
                {
                    "targets": [0, 2],
                    "class": "dt-body-center",
                    "visible": false
                },
                {
                    "targets": [1],
                    "class": "dt-body-center"
                },
                {
                    "targets": [5, 6],
                    "class": "dt-body-center"
                },
                {
                    "targets": [8],
                    "class": "dt-body-center",
                    "width": "10%"
                },
            ]
        });
        console.log("check this", rental_request_no);
        $.ajax({
            type: "GET",
            url: "/TransContractOffer/GetDataObject/" + rental_request_no,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                oTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                var action = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus-ob"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>';

                arrObjectEdit = new Array();
                for (var i = 0; i < jsonList.data.length; i++) {
                    oTable.fnAddData([jsonList.data[i].RO_NUMBER, jsonList.data[i].OBJECT_ID, jsonList.data[i].OBJECT_TYPE_ID, jsonList.data[i].OBJECT_TYPE, jsonList.data[i].RO_NAME, jsonList.data[i].LAND_DIMENSION, jsonList.data[i].BUILDING_DIMENSION, jsonList.data[i].INDUSTRY, action], true);

                    var param = {
                        RO_NUMBER: jsonList.data[i].RO_NUMBER,
                        OBJECT_ID: jsonList.data[i].OBJECT_ID,
                        OBJECT_TYPE_ID: jsonList.data[i].OBJECT_TYPE_ID,
                        OBJECT_TYPE: jsonList.data[i].OBJECT_TYPE,
                        RO_NAME: jsonList.data[i].RO_NAME,
                        LAND_DIMENSION: jsonList.data[i].LAND_DIMENSION,
                        BUILDING_DIMENSION: jsonList.data[i].BUILDING_DIMENSION,
                        INDUSTRY: jsonList.data[i].INDUSTRY
                    }
                    arrObjectEdit.push(JSON.stringify(param));

                    var listItems = "<option value='" + 'RO ' + jsonList.data[i].OBJECT_ID + '|' + jsonList.data[i].RO_NUMBER + "'>" + 'RO ' + jsonList.data[i].OBJECT_ID + ' - ' + jsonList.data[i].RO_NAME + "</option>"
                    $("#condition-option").append(listItems);
                }
                setTimeout(coHitungValue(), 1000);
            }
        });
        
        // Add event listener for opening and closing details

        var industry = function () {

            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDropDownIndustry",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                    }
                    $("#INDUSTRY").append(listItems);
                }
            });
        }

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();
            //console.log(data)
            //var xInd = $('#INDUSTRY').val();
            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus-ob"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);
            industry();
            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataProfitCenter?id=" + data['OBJECT_ID'],
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    setProfitCenter(data)
                }
            });
            $('#addDetailModal').modal('hide');
        });

        $('#rental-object-co').on('click', 'tr #btn-delcus-ob', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();
            //console.log(data);
            $('#condition-option [value="' + 'RO ' + data[1] + '|' + data[0] + '"]').remove();
            oTable.fnDeleteRow(baris);
            //setTimeout(hitungArea(), 2000);
        });

        $('#rental-object-co').on('click', 'tr #btn-savecus-ob', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();
            //console.log(data);
            var listItems = "<option value='" + 'RO ' + data[1] + '|' + data[0] + "'>" + 'RO ' + data[1] + ' - ' + data[4] + "</option>"
            $("#condition-option").append(listItems);


            var xInd = $(baris).find('#INDUSTRY').val();
            $('#rental-object-co').dataTable().fnUpdate(xInd, baris, 7, false);
            //setTimeout(hitungArea(), 2000);
        });


        $('#rental-object-co').on('click', 'tr #btn-editcus-ob', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').dataTable();

            var nRow = $(this).parents('tr')[0];
            var editIndustry = '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>';
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus-ob"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus-ob"><i class="fa fa-check"></i></a>';
            table.fnUpdate(editIndustry, nRow, 7, false);
            table.fnUpdate(tombol, nRow, 8, false);


            industry();

            $(baris).find("#INDUSTRY option[value='" + valPriceCode + "']").prop('selected', true);
            //$("#INDUSTRY option[value='02 - Usaha Industri']").prop('selected', true);
        });


        $('#rental-object-co').on('click', 'tr #btn-cancelcus-ob', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#rental-object-co').DataTable();
            var data = table.row(baris).data();

            var objectID = data[1];

            // Hapus Row 
            oTable.fnDeleteRow(baris);

            var json = JSON.parse('[' + arrObjectEdit + ']');

            var RO_NUMBER = "";
            var OBJECT_ID = "";
            var OBJECT_TYPE_ID = "";
            var OBJECT_TYPE = "";
            var RO_NAME = "";
            var LAND_DIMENSION = "";
            var BUILDING_DIMENSION = "";
            var INDUSTRY = "";

            json.forEach(function (object) {
                if (object.OBJECT_ID == objectID) {
                    RO_NUMBER = object.RO_NUMBER;
                    OBJECT_ID = object.OBJECT_ID;
                    OBJECT_TYPE_ID = object.OBJECT_TYPE_ID;
                    OBJECT_TYPE = object.OBJECT_TYPE;
                    RO_NAME = object.RO_NAME;
                    LAND_DIMENSION = object.LAND_DIMENSION;
                    BUILDING_DIMENSION = object.BUILDING_DIMENSION;
                    INDUSTRY = object.INDUSTRY;
                }
            });

            oTable.fnAddData([RO_NUMBER, OBJECT_ID, OBJECT_TYPE_ID, OBJECT_TYPE, RO_NAME, LAND_DIMENSION, BUILDING_DIMENSION, INDUSTRY, '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus-ob"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);

            ////console.log(arrCondition);

            ////console.log(data[1]);
            //setTimeout(hitungArea(), 2000);
        });

        
    }

    var tablex = $('#condition');
    var xTable = tablex.dataTable({
        "order": [[7, "asc"]],
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 1000,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false,
        "columnDefs": [
            //{
            //    "targets": [6],
            //     render: $.fn.dataTable.render.number(',', '.', 3)
            //},
            //{
            //    "targets": [14],
            //    render: $.fn.dataTable.render.number('.', '.', 0)
            //},
            //{
            //    "targets": [17],
            //    render: $.fn.dataTable.render.number('.', '.', 0)
            //},
            {
                "targets": [20],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [20],
                "visible": false
            }
        ]
    });

    var editDataCondition = function () {
        var offer_id = $('#OFFER_ID').val();

        //var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';
        var tombol = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

        $.ajax({
            type: "GET",
            url: "/TransContractOffer/GetDataDetailConditionEdit/" + offer_id,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                arrCondition = new Array();

                xTable.fnClearTable();
                var jsonList = data
                var listItems = "";
                var calcObject = "";
                var res_condition = "";
                var statistic = "";
                var maskUnitPrice = "";
                var maskTotal = "";
                var maskNetValue = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    if (jsonList.data[i].STATISTIC == 0) {
                        statistic = "No";
                    }
                    else {
                        statistic = "Yes";
                    }

                    maskUnitPrice = jsonList.data[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    maskTotal = jsonList.data[i].TOTAL.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    maskNetValue = jsonList.data[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    xTable.fnAddData([
                        jsonList.data[i].CALC_OBJECT,
                        jsonList.data[i].CONDITION_TYPE,
                        jsonList.data[i].VALID_FROM,
                        jsonList.data[i].VALID_TO,
                        jsonList.data[i].MONTHS,
                        statistic,
                        jsonList.data[i].FORMULA,
                        maskUnitPrice,
                        jsonList.data[i].AMT_REF,
                        jsonList.data[i].FREQUENCY,
                        jsonList.data[i].START_DUE_DATE,
                        jsonList.data[i].MANUAL_NO,
                        jsonList.data[i].MEASUREMENT_TYPE,
                        jsonList.data[i].LUAS,
                        maskTotal,
                        jsonList.data[i].NJOP_PERCENT,
                        jsonList.data[i].KONDISI_TEKNIS_PERCENT,
                        maskNetValue,
                        jsonList.data[i].COA_PROD,
                        tombol,
                        jsonList.data[i].TAX_CODE
                    ]);
                    calcObject = jsonList.data[i].CALC_OBJECT;
                    res_condition = jsonList.data[i].CONDITION_TYPE;
                    var param = {
                        CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                        CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                        VALID_FROM: jsonList.data[i].VALID_FROM,
                        VALID_TO: jsonList.data[i].VALID_TO,
                        MONTHS: jsonList.data[i].MONTHS,
                        STATISTIC: statistic,
                        UNIT_PRICE: maskUnitPrice.toString(),
                        AMT_REF: jsonList.data[i].AMT_REF,
                        FREQUENCY: jsonList.data[i].FREQUENCY,
                        START_DUE_DATE: jsonList.data[i].START_DUE_DATE,
                        MANUAL_NO: jsonList.data[i].MANUAL_NO.toString(),
                        FORMULA: jsonList.data[i].FORMULA,
                        MEASUREMENT_TYPE: jsonList.data[i].MEASUREMENT_TYPE,
                        LUAS: jsonList.data[i].LUAS,
                        TOTAL: maskTotal.toString(),
                        NJOP_PERCENT: jsonList.data[i].NJOP_PERCENT,
                        KONDISI_TEKNIS_PERCENT: jsonList.data[i].KONDISI_TEKNIS_PERCENT,
                        TOTAL_NET_VALUE: maskNetValue.toString(),
                        TAX_CODE: jsonList.data[i].TAX_CODE,
                        COA_PROD: jsonList.data[i].COA_PROD
                    }
                    arrCondition.push(JSON.stringify(param));

                    var arrCond = buah.push(calcObject + ' | ' + res_condition);
                    //console.log(arrCond);

                    var fruits = buah;
                }

                setTimeout(coHitungValue(), 1000);
            }
        });

        $('#btn-condition').on('click', function () {
            // $('#condition').dataTable();

            //raw data value calc object dan di split |
            var id_combo = $('#condition-option').val();
            var splitCalcObject = id_combo.split("|");
            var calcObject = splitCalcObject[0];
            var id_ro = splitCalcObject[1];

            if (id_ro) {
                // Ajax untuk get meas type berdasarkan id_ro yang didapatkan dari split
                $('#meas-type').html('');
                // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih

                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataMeasType/" + id_ro,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        listItems += '<option value="">--Choose--</option>';
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + jsonList.data[i].MEAS_TYPE + '|' + jsonList.data[i].ID_DETAIL + "'>" + jsonList.data[i].MEAS_TYPE + ' - ' + jsonList.data[i].MEAS_DESC + "</option>";
                        }
                        // $("#meas-type").append(listItems);
                        $('select[name="meas-type"]').html(listItems);
                    }
                });


            }

            // Matikan form untuk kondisi Header Document
            if (id_ro) {
                var measType = '<select class="form-control" id="meas-type" name="meas-type" onchange="getLuas(this)"></select>';
            }
            else {
                measType = '';
            }
            if (id_ro) {
                var luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }
            else {
                luas = '';
            }

            //Cek selected value dari combobox calculation apakah header document atau RO
            var vCBCalculation = $("#condition-option").val();

            if (vCBCalculation == 'Header Document') {
                measType = '<input type="text" class="form-control" id="meas-type" name="meas-type" readonly>';
                luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';

            }

            if (calcObject == 'Header Document') {
                var buttonPrice = '';
                var buttonCoa = '';
            }
            else {
                buttonPrice = '<a class="btn default btn-xs blue input-group-addon" id="btn-price" ><i class="fa fa-plus"></i></a>';
                buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';
            }

            var tableDT = $('#condition  > tbody');
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();
            var tim = $('#TERM_IN_MONTHS').val();

            //date Now
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;
            //console.log(today);

            var startD = '<center><input type="text" class="form-control date-picker" name="start-date" value="' + start + '" id="start-date" onfocusout="hitungTermMonths(this)"></center>';
            var endD = '<center><input type="text" class="form-control date-picker" name="end-date" value="' + end + '" id="end-date" onfocusout="hitungTermMonths(this)"></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onfocusout="hitungEndDate(this)"></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center class="input-group">' + buttonPrice + '<input type="text" class="form-control number" id="unit-price" name="unit-price" onchange="isiTotalUnitPrice(this);hitungFormula(this);"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" onchange="infoIsiManual()" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control date-picker" name="start-due-date" value="' + today + '" id="start-due-date"></center>';
            var manualNo = '<center><input type="text" class="form-control" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';
            //var measType = '<select class="form-control" id="meas-type"></select>';
            //var luas = '<input type="text" class="form-control" id="luas">';
            var total = '<center><input type="text" class="form-control number" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control decimal" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control ipercentage" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';
            //var salesRule = '<center><input type="text" class="form-control" id="sales-rule"></center>';
            var totalNetValue = '<center><input type="text" class="form-control number" id="total-net-value" name="total-net-value" readonly></center>';
            //var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
            var coa_prod = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_prod" name="coa_prod" readonly></center>';
            var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_prod" id="coa_prod"><option value="-">-</option></select></center>';


            //Format luas dengan separator . karena
            //var fLuas = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            $('input[name="test"]:checked').each(function () {
                //raw data condition dan di split |
                var raw_cond = $(this).val();
                var split_cond = raw_cond.split("|");
                var res_condition = split_cond[0];
                var tax_code = split_cond[1];

                //console.log("RES CONDITION >> " + res_condition);
                if (res_condition == 'Z009 Administrasi' || res_condition == 'Z010 Warmeking') {
                    xTable.fnAddData([calcObject,
                        res_condition,
                        startD,
                        endD,
                        termMonths,
                        cekbokStatistic,
                        formula,
                        unitPrice,
                        amtRef,
                        frequency,
                        startDueDate,
                        manualNo,
                        measType,
                        luas,
                        total,
                        persenFromNJOP,
                        persenFromKondisiTeknis,
                        totalNetValue,
                        coa_produksi_disabled,
                        '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                        tax_code], true);
                } else {
                    xTable.fnAddData([calcObject,
                        res_condition,
                        startD,
                        endD,
                        termMonths,
                        cekbokStatistic,
                        formula,
                        unitPrice,
                        amtRef,
                        frequency,
                        startDueDate,
                        manualNo,
                        measType,
                        luas,
                        total,
                        persenFromNJOP,
                        persenFromKondisiTeknis,
                        totalNetValue,
                        coa_prod,
                        '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                        tax_code], true);
                }

                var arrCond = buah.push(calcObject + ' | ' + res_condition);
                //console.log(arrCond);

                var fruits = buah;

                //Masking Start Due Date
                $('input[name="start-due-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="start-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="end-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('#addCondition').modal('hide');
                getCoaProduksi();
            });

            //MASKING FIELD UNTUK FORM DATATABLE
            //$("#unit-price").inputmask({
            //    mask: "9",
            //    repeat: 10,
            //    greedy: !1
            //});
            //$('#unit-price').mask('000,000,000,000,000.000', { reverse: true });
            //alert(sep1000(10002343123.3498, true));
            //$('#unit-price').mask("#,##0.00", { reverse: true });

            // MASKING2
            //$('#unit-price').on('change', function () {
            //    var unitPrice = $('#unit-price').val();
            //    var mask = sep1000(unitPrice, true);
            //    $('#unit-price').val(mask);
            //});

            $('.decimal').inputmask('decimal', {
                'digits': 2,
                'digitsOptional': false,
                radixPoint: ".",
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.ipercentage').inputmask('numeric', {
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.number').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                autoGroup: true,
                rightAlign: true,
                oncleared: function () { self.Value('0'); }
            });
            
            resetDatePicker();
        });

        // condition edit
        $('#condition').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#condition').dataTable();
            //console.log('ok', data);
            // Value untuk Data Table Form
            var condtype = data[1];
            var start = data[2];
            var end = data[3];
            var tim = data[4];
            var stat = data[5];
            var vFormula = data[6];
            var uPrice = data[7];
            var aRef = data[8];
            var freq = data[9];
            var dueDate = data[10];
            var manual = data[11];
            var vMeasType = data[12];
            var vLuas = data[13];
            var vTotalValue = data[14];
            var vNJOP = data[15];
            var vKondisiTeknis = data[16];
            var vNetValue = data[17];
            var coa_prod = data[18];


            //console.log(uPrice);

            var sDate = '';
            var sDueDate = '';

            if (dueDate == null) {
                sDueDate = "";
            }
            else {
                sDueDate = dueDate;
            }

            var sStat = '';
            if (stat == 0) {
                sStat = 'No';
            }
            else {
                sStat = 'Yes';
            }

            var mNO = '';
            if (manual == null) {
                mNO = "";
            }
            else {
                mNO = manual;
            }

            var sNJOP = '';
            if (vNJOP == null) {
                sNJOP = '';
            }
            else {
                sNJOP = vNJOP;
            }

            var sMeasType = '';
            if (vMeasType == null) {
                sMeasType = '';
            }
            else {
                sMeasType = vMeasType;
            }

            var sLuas = '';
            if (vLuas == null) {
                sLuas = '';
            }
            else {
                sLuas = vLuas;
            }

            var sKondisiTeknis = '';
            if (vKondisiTeknis == null) {
                sKondisiTeknis = '';
            }
            else {
                sKondisiTeknis = vKondisiTeknis;
            }



            if (condtype == 'Z009 Administrasi' || condtype == 'Z010 Warmeking') {
                var buttonPrice = '';
                var buttonCoa = '';
            }
            else {
                buttonPrice = '<a class="btn default btn-xs blue input-group-addon" id="btn-price" ><i class="fa fa-plus"></i></a>';
                buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';
            }
            //console.log("coa prod >>" + data[18]);
            //console.log("condtype >>" + condtype);



            // Data Table Form
            var startD = '<center><input type="text" class="form-control date-picker" name="start-date" value="' + start + '" id="start-date" onfocusout="hitungTermMonths(this)"></center>';
            var endD = '<center><input type="text" class="form-control date-picker" name="end-date" value="' + end + '" id="end-date" onfocusout="hitungTermMonths(this)"></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onfocusout="hitungEndDate(this)"></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="' + stat + '" id="statistic" name="statistic" ' + (stat === 1 ? 'checked' : '') + ' /></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center class="input-group">' + buttonPrice + '<input type="text" class="form-control number" id="unit-price" value="' + uPrice + '" name="unit-price" onchange="isiTotalUnitPrice(this);hitungFormula(this);"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" onchange="infoIsiManual()" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control date-picker" value="' + sDueDate + '" name="start-due-date" id="start-due-date"></center>';
            var manualNo = '<center><input type="text" class="form-control" value="' + mNO + '" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';

            var total = '<center><input type="text" class="form-control number" value="' + vTotalValue + '" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control decimal" value="' + sNJOP + '" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control ipercentage" value="' + sKondisiTeknis + '" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';

            var totalNetValue = '<center><input type="text" class="form-control number" value="' + vNetValue + '" id="total-net-value" name="total-net-value" readonly></center>';
            //var coa_prod = '<center><select class="form-control coa_prod" name="coa_prod" id="coa_prod"></select></center>';
            var coa_prod = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_prod" value="' + coa_prod + '" name="coa_prod" readonly></center>';
            var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_produksi" id="coa_prod"><option value="-" selected="selected">-</option></select></center>';
            measType = '<input type="text" class="form-control" value="' + sMeasType + '" id="meas-type" name="meas-type" readonly>';
            luas = '<input type="text" class="form-control" id="luas" value="' + sLuas + '" name="luas" readonly>';

            openTable.fnUpdate(startD, nRow, 2, false);
            openTable.fnUpdate(endD, nRow, 3, false);
            openTable.fnUpdate(termMonths, nRow, 4, false);
            openTable.fnUpdate(cekbokStatistic, nRow, 5, false);
            openTable.fnUpdate(formula, nRow, 6, false);
            openTable.fnUpdate(unitPrice, nRow, 7, false);
            openTable.fnUpdate(amtRef, nRow, 8, false);
            openTable.fnUpdate(frequency, nRow, 9, false);
            openTable.fnUpdate(startDueDate, nRow, 10, false);
            openTable.fnUpdate(manualNo, nRow, 11, false);
            openTable.fnUpdate(measType, nRow, 12, false);
            openTable.fnUpdate(luas, nRow, 13, false);
            openTable.fnUpdate(total, nRow, 14, false);
            openTable.fnUpdate(persenFromNJOP, nRow, 15, false);
            openTable.fnUpdate(persenFromKondisiTeknis, nRow, 16, false);
            openTable.fnUpdate(totalNetValue, nRow, 17, false);

            if (condtype == 'Z009 Administrasi' || condtype == 'Z010 Warmeking') {
                openTable.fnUpdate(coa_produksi_disabled, nRow, 18, false);
            } else {
                openTable.fnUpdate(coa_prod, nRow, 18, false);
            }


            openTable.fnUpdate(tombol, nRow, 19, false);

            $(baris).find("#amt-ref option[value='" + aRef + "']").prop('selected', true);
            $(baris).find("#frequency option[value='" + freq + "']").prop('selected', true);
            $(baris).find("#formula option[value='" + vFormula + "']").prop('selected', true);


            //$('#unit-price').mask('000.000.000.000.000', { reverse: true });

            $('.decimal').inputmask('decimal', {
                'digits': 2,
                'digitsOptional': false,
                radixPoint: ".",
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.ipercentage').inputmask('numeric', {
                max: 100,
                min: 0,
                rightAlign: true,
            });

            $('.number').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                autoGroup: true,
                rightAlign: true,
                oncleared: function () { self.Value('0'); }
            });

            // set datepicker condition edit 
            resetDatePicker();
            getCoaProduksi2(data[18], baris);
        });

        // condition cancel
        $('#condition').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var calcObject = data[0];
            var conditionType = data[1];

            // Hapus Row 
            xTable.fnDeleteRow(baris);

            var json = JSON.parse('[' + arrCondition + ']');


            // Deklarasi Variabel       
            var CALC_OBJECT = "";
            var CONDITION_TYPE = "";
            var VALID_FROM = "";
            var VALID_TO = "";
            var MONTHS = "";
            var STATISTIC = "";
            var UNIT_PRICE = "";
            var AMT_REF = "";
            var FREQUENCY = "";
            var START_DUE_DATE = "";
            var MANUAL_NO = "";
            var FORMULA = "";;
            var MEASUREMENT_TYPE = "";
            var LUAS = "";
            var TOTAL = "";
            var NJOP_PERCENT = "";
            var KONDISI_TEKNIS_PERCENT = "";
            var TOTAL_NET_VALUE = "";
            var COA_PROD = "";
            var TAX_CODE = "";

            json.forEach(function (object) {
                if (object.CALC_OBJECT == calcObject && object.CONDITION_TYPE == conditionType) {
                    CALC_OBJECT = object.CALC_OBJECT;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    VALID_FROM = object.VALID_FROM;
                    VALID_TO = object.VALID_TO;
                    MONTHS = object.MONTHS;
                    STATISTIC = object.STATISTIC;
                    UNIT_PRICE = object.UNIT_PRICE;
                    AMT_REF = object.AMT_REF;
                    FREQUENCY = object.FREQUENCY;
                    START_DUE_DATE = object.START_DUE_DATE;
                    MANUAL_NO = object.MANUAL_NO;
                    FORMULA = object.FORMULA;
                    MEASUREMENT_TYPE = object.MEASUREMENT_TYPE;
                    LUAS = object.LUAS;
                    TOTAL = object.TOTAL;
                    NJOP_PERCENT = object.NJOP_PERCENT;
                    KONDISI_TEKNIS_PERCENT = object.KONDISI_TEKNIS_PERCENT;
                    TOTAL_NET_VALUE = object.TOTAL_NET_VALUE;
                    COA_PROD = object.COA_PROD;
                    TAX_CODE = object.TAX_CODE;
                }
            });

            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            xTable.fnAddData([
                CALC_OBJECT,
                CONDITION_TYPE,
                VALID_FROM,
                VALID_TO,
                MONTHS,
                STATISTIC,
                FORMULA,
                UNIT_PRICE,
                AMT_REF,
                FREQUENCY,
                START_DUE_DATE,
                MANUAL_NO,
                MEASUREMENT_TYPE,
                LUAS,
                TOTAL,
                NJOP_PERCENT,
                KONDISI_TEKNIS_PERCENT,
                TOTAL_NET_VALUE,
                COA_PROD,
                tombolC,
                TAX_CODE
            ]);

            ////console.log(arrCondition);

            ////console.log(data[1]);

            setTimeout(coHitungValue(), 1000);
        });

        // save condition edit
        $('#condition').on('click', 'tr #btn-savecus', function () {
            //$(this).hide();
            //hitungFormula(this);
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var start_date = $(baris).find('#start-date').val();
            var end_date = $(baris).find('#end-date').val();
            var term_months = $(baris).find('#term-months').val();

            var unit_price = $(baris).find('#unit-price').val();
            var amt_ref = $(baris).find('#amt-ref').val();
            var frequency = $(baris).find('#frequency').val();
            var start_due_date = $(baris).find('#start-due-date').val();
            var manual_no = $(baris).find('#manual-no').val();
            var formula = $(baris).find('#formula').val();

            var meas_type = $(baris).find('#meas-type').val();
            var splitselectedMeasType = meas_type.split("|");
            var id_detail_meas_type = splitselectedMeasType[0];

            var luas = $(baris).find('#luas').val();
            var total = $(baris).find('#total').val();
            var persen_from_njop = $(baris).find('#persen-from-njop').val();
            var persen_from_kondisi_teknis = $(baris).find('#persen-from-kondisi-teknis').val();
            var coa_prod = $(baris).find('#coa_prod').val();
            //console.log("coa  prod >> " + coa_prod);

            //var sales_rule = $('#sales-rule').val();
            var total_net_value = $(baris).find('#total-net-value').val();

            //Set Value Untuk Statistic
            var cek = $(baris).find('input[id=statistic]').is(":checked");
            var vStatistic = "";
            if (cek) {
                vStatistic = "Yes";
            }
            else {
                vStatistic = "No";
            }

            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();

            var tgl_kontrak = CONTRACT_START_DATE.replace(/\./g, '/')
            var datearray = tgl_kontrak.split("/");
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            var tgl_start_kontrak = new Date(newdate); //set value

            var tgl_condition = start_date.replace(/\./g, '/')
            var datearray_con = tgl_condition.split("/");
            var newdate_con = datearray_con[1] + '/' + datearray_con[0] + '/' + datearray_con[2];
            var tgl_start_condition = new Date(newdate_con); //set value

            var tgl_kontrak2 = CONTRACT_END_DATE.replace(/\./g, '/')
            var datearray_en = tgl_kontrak2.split("/");
            var newdate_kon_end = datearray_en[1] + '/' + datearray_en[0] + '/' + datearray_en[2];
            var tgl_end_kontrak = new Date(newdate_kon_end); //set value end

            var tgl_condition2 = end_date.replace(/\./g, '/')
            var datearray_con_end = tgl_condition2.split("/");
            var newdate_con_end = datearray_con_end[1] + '/' + datearray_con_end[0] + '/' + datearray_con_end[2];
            var tgl_end_condition = new Date(newdate_con_end); //set value end

            // Cek apakah manual no kosong atau tidak
            var cekManualNo = $(baris).find('input[id=manual-no]').val();
            //console.log(cekManualNo);

            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            if (coa_prod == "") {
                swal('Info', 'Mohon Pilih COA Produksi', 'info');
            } 
            else if (cekManualNo == "") {
                //swal('Info', 'Anda Mengisi Manual No Pada Kondisi Ini, Jangan Lupa Untuk Membuat Manual Frequencynya Pada Tabel Dibawah! ', 'info');
                swal('Info', 'Harap Mengisi Data di Tabel Manual Frequency! ', 'info');
            } 
            else if (start_due_date == "") {
                swal('Info', 'Mohon Isi Due Date', 'info');
            } //else if (tgl_start_condition < tgl_start_kontrak) {
                //swal('Info', 'Tidak boleh kurang dari Contract Start Date', 'info');
            //} //else if (tgl_end_condition > tgl_end_kontrak) {
                //swal('Info', 'Tidak boleh lebih dari Contract End Date', 'info');
            //}
            else {
                // swal('Info', 'Harap Mengisi Data di Tabel Manual Frequency! ', 'info');
                $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
                $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
                $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
                $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
                $('#condition').dataTable().fnUpdate(formula, baris, 6, false);
                $('#condition').dataTable().fnUpdate(unit_price, baris, 7, false);
                $('#condition').dataTable().fnUpdate(amt_ref, baris, 8, false);
                $('#condition').dataTable().fnUpdate(frequency, baris, 9, false);
                $('#condition').dataTable().fnUpdate(start_due_date, baris, 10, false);
                $('#condition').dataTable().fnUpdate(manual_no, baris, 11, false);
                $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
                $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
                $('#condition').dataTable().fnUpdate(total, baris, 14, false);
                $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
                $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
                //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
                $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
                $('#condition').dataTable().fnUpdate(coa_prod, baris, 18, false);
                $('#condition').dataTable().fnUpdate(tombolDefault, baris, 19, false);
            }
            // document.getElementById('btn-savecus').disabled = true;

            //var elem = document.getElementById('btn-savecus');
            //elem.parentNode.removeChild(elem);
            //return false;

            setTimeout(coHitungValue(), 1000);
        });

        $('#condition').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            // ambil data by kolom
            var rData = table.row($(this).parents('tr')).data();
            var calc_object = rData[0];
            var condition_type = rData[1];
            var dataRemove = calc_object + ' | ' + condition_type;
            // panggil function untuk remove array buah dan secondCellArray:)
            removeA(buah, dataRemove);
            xTable.fnDeleteRow(baris);

            setTimeout(coHitungValue(), 1000);
        });
    }

    // Edit Data Manual Frequency
    var editDataManualFrequency = function () {
        var tablem = $('#manual-frequency');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 30,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "order": [[0, "asc"]],
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "columnDefs": [
                {
                    "targets": [8],
                    "visible": false
                }
            ]
        });

        var arrManualFrequency = [];
        var offer_id = $('#OFFER_ID').val();
        //var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';
        var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
        var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';
        // Ajax untuk menampilkan data manual frequency sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransContractOffer/GetDataDetailManualFrequencyEdit/" + offer_id,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                arrManualFrequency = new Array();

                oTable.fnClearTable();
                var jsonList = data;
                var res_condition = "";
                var maskNetValueFrequency = "";
                var maskQuantityFrequency = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    maskNetValueFrequency = jsonList.data[i].NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    maskQuantityFrequency = jsonList.data[i].QUANTITY.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    oTable.fnAddData([jsonList.data[i].MANUAL_NO, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].DUE_DATE, maskNetValueFrequency, maskQuantityFrequency, jsonList.data[i].UNIT, tombolAdd, tombolAct, jsonList.data[i].CONDITION]);

                    var param = {
                        MANUAL_NO: jsonList.data[i].MANUAL_NO,
                        CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                        DUE_DATE: jsonList.data[i].DUE_DATE,
                        NET_VALUE: maskNetValueFrequency.toString(),
                        QUANTITY: maskQuantityFrequency,
                        UNIT: jsonList.data[i].UNIT,
                        CONDITION: jsonList.data[i].CONDITION
                    }
                    arrManualFrequency.push(JSON.stringify(param));
                }
            }
        });

        // Variabel untuk handle combobox condition
        var cond = function () {
            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //$("#memo-condition").append(listItems);
            $('select[name="condition-frequency"]').html(listItems);
        }

        var resetCondition = function () {
            $('select[name="condition-frequency"]').each(function (index, elem) {
                var baris = $(this).parents('tr')[0];
                var table = $('#manual-frequency').DataTable();
                var data = table.row(baris).data();
                var manual_no = data[0];

                var condition = function (manual) {
                    var elems = $('#condition').find('tbody tr');
                    var menu = ""
                    $(elems).each(function (index) {
                        var c_data = $('#condition').DataTable().row(index).data();
                        //console.log(c_data[0] + ' | ' + c_data[1], c_data[11], manual);
                        var m = c_data[11];
                        var v = c_data[0] + ' | ' + c_data[1];
                        //console.log(typeof manual, manual);
                        //console.log(typeof m, m);
                        if (manual == m) {
                            menu = v;
                        }
                    });
                    return menu;
                }
                var value = condition(manual_no);
                //console.log(elem);
                //console.log($(elem).find('option[value="' + value + '"]'));
                $(elem).find('option[value="' + value + '"]').attr('selected', 'selected');
                getConditionCode(elem);
            });
        }
        //Generate textbox(form) di data table manual-frequency
        $('#btn-add-manual-frequency').on('click', function () {
            var nomor = 10;

            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var xtable = $('#manual-frequency').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);


            var last = $("#manual-frequency").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            //console.log(l);
            if (l != 0) {
                nomor = (Number(last) + 10);
            }


            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" id="due-date-frequency" name="due-date-frequency">';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';

            oTable.fnAddData([nomor,
                condition,
                dueDateFrequency,
                netValueFrequency,
                qtyFrequency,
                unitFrequency,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                conditionCode], true);
            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //Masking Net Value

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});

            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });

            $('input[name = "qty-frequency"]').inputmask({
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });


            resetDatePicker();
            // Panggil variable yg handle combobox condition
            cond();
            resetCondition();
        });

        $('#manual-frequency').on('click', 'tr #btn-add-sama', function () {
            var table = $('#manual-frequency').DataTable();
            var data = table.row($(this).parents('tr')).data();

            var manual_no_sama = data[0];
            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control" onchange="getConditionCode(this)"></select>';
            //var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" id="due-date-frequency" name="due-date-frequency">';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';


            oTable.fnAddData([manual_no_sama,
                condition,
                dueDateFrequency,
                netValueFrequency,
                qtyFrequency,
                unitFrequency,
                '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                conditionCode], true);

            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});

            
            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }

            });
            
            resetDatePicker();
            // Panggil variable yg handle combobox condition
            cond();
            resetCondition();
        });

        var conditionCancel = '';
        var dueDateCancel = '';
        var netValueCancel = '';

        // BTN EDITCUS MANUAL FREQUENCY
        $('#manual-frequency').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var condVal = data[1];
            var dueDate = data[2];
            var netValue = data[3];
            var qty = data[4];
            var unitVal = data[5];
            var conditionCodeVal = data[8];

            conditionCancel = condVal;
            dueDateCancel = dueDate;
            netValueCancel = netValue;

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus-frequency"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#manual-frequency').dataTable();



            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control date-picker" value="' + dueDate + '" id="due-date-frequency" name="due-date-frequency">';
            var netValueFrequency = '<input type="text" class="form-control" value="' + netValue + '" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" value="' + qty + '" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option><option value="M2">M<sup>2</sup></option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" value="' + conditionCodeVal + '" id="CONDITION_CODE" name="CONDITION_CODE">';

            openTable.fnUpdate(condition, nRow, 1, false);
            openTable.fnUpdate(dueDateFrequency, nRow, 2, false);
            openTable.fnUpdate(netValueFrequency, nRow, 3, false);
            openTable.fnUpdate(qtyFrequency, nRow, 4, false);
            openTable.fnUpdate(unitFrequency, nRow, 5, false);

            openTable.fnUpdate(tombol, nRow, 7, false);
            openTable.fnUpdate(conditionCode, nRow, 8, false);

            resetDatePicker();
            cond();


            $(baris).find("#condition-frequency option[value='" + condVal + "']").prop('selected', true);
            $(baris).find("#unit-frequency option[value='" + unitVal + "']").prop('selected', true);

            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //Masking Net Value

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            //$('input[name = "net-value-frequency"]').on('change', function () {
            //    var netValueFrequency = $('input[name = "net-value-frequency"]').val();
            //    var mask = sep1000(netValueFrequency, true);
            //    $('input[name = "net-value-frequency"]').val(mask);
            //});
            
            $('input[name = "net-value-frequency"]').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }

            });

            $('input[name = "qty-frequency"]').inputmask({
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value('0'); }
            });

            // Panggil variable yg handle combobox condition


        });

        $('#manual-frequency').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var manualCancel = data[0];

            //console.log('manual cancel ' + manualCancel);
            //console.log('condition cancel ' + conditionCancel);

            // Hapus Row 
            oTable.fnDeleteRow(baris);
            var json = JSON.parse('[' + arrManualFrequency + ']');


            // Deklarasi Variabel       
            var MANUAL_NO = "";
            var CONDITION_TYPE = "";
            var DUE_DATE = "";
            var NET_VALUE = "";
            var QUANTITY = "";
            var UNIT = "";
            var CONDITION = "";


            json.forEach(function (object) {
                if (object.MANUAL_NO == manualCancel && object.CONDITION_TYPE == conditionCancel && object.DUE_DATE == dueDateCancel) {
                    MANUAL_NO = object.MANUAL_NO;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    DUE_DATE = object.DUE_DATE;
                    NET_VALUE = object.NET_VALUE;
                    QUANTITY = object.QUANTITY;
                    UNIT = object.UNIT;
                    CONDITION = object.CONDITION;
                }
            });


            var tombolAddSama = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            oTable.fnAddData([MANUAL_NO, CONDITION_TYPE, DUE_DATE, NET_VALUE, QUANTITY, UNIT, tombolAddSama, tombolC, CONDITION]);
        });

        $('#manual-frequency').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#manual-frequency').on('click', 'tr #btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var a = $(baris).find('#condition-frequency').val();
            var b = $(baris).find('#due-date-frequency').val();
            var c = $(baris).find('#net-value-frequency').val();
            var d = $(baris).find('#CONDITION_CODE').val();
            var e = $(baris).find('#qty-frequency').val();
            var f = $(baris).find('#unit-frequency').val();
            //console.log(f);

            //console.log(baris);
            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>';

            if (b == '') {
                swal('Info', "Silahkan isi Start Due Date", 'info')
            } else {
                $('#manual-frequency').dataTable().fnUpdate(a, baris, 1, false);
                oTable.fnUpdate(b, baris, 2, false);
                oTable.fnUpdate(c, baris, 3, false);
                oTable.fnUpdate(e, baris, 4, false);
                oTable.fnUpdate(f, baris, 5, false);
                oTable.fnUpdate(d, baris, 8, false);
                oTable.fnUpdate(tombolDefault, baris, 7, false);
            }


            // document.getElementById('btn-savecus').disabled = true;

            //var elem = document.getElementById('btn-savecus-frequency');
            //elem.parentNode.removeChild(elem);
            return false;

        });
    }

    //Var untuk menghandle data table edit memo
    var editDataMemo = function () {
        var tablem = $('#table-memo');
        var mTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false
        });

        // Ajax untuk get data memo sebelumnya
        var offer_id = $('#OFFER_ID').val();
        var arrMemo = [];

        var tombolAdd = '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>';
        var tombolAct = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
        // Ajax untuk menampilkan data manual frequency sebelumnya
        $.ajax({
            type: "GET",
            url: "/TransContractOffer/GetDataDetailMemoEdit/" + offer_id,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                arrMemo = new Array();
                mTable.fnClearTable();
                var jsonList = data;
                var res_condition = "";
                for (var i = 0; i < jsonList.data.length; i++) {
                    mTable.fnAddData([jsonList.data[i].CALC_OBJECT, jsonList.data[i].CONDITION_TYPE, jsonList.data[i].MEMO, tombolAct]);

                    var param = {
                        CALC_OBJECT: jsonList.data[i].CALC_OBJECT,
                        CONDITION_TYPE: jsonList.data[i].CONDITION_TYPE,
                        MEMO: jsonList.data[i].MEMO
                    }
                    arrMemo.push(JSON.stringify(param));
                }
                //console.log(arrMemo);
            }
        });

        $('#btn-add-memo').on('click', function () {
            $('#memo-condition').html('');
            $('#memo').val('');

            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //console.log(listItems);
            $("#memo-condition").append(listItems);

        });

        $('#btn-add-memo-md').on('click', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var raw_val = $('#memo-condition').val();
            var v_split = raw_val.split("|");

            var memo_calc_object = v_split[0];
            var memo_cond_type = v_split[1];
            var memo = $('#memo').val();

            mTable.fnAddData([memo_calc_object,
                memo_cond_type,
                memo,
                '<center><a class="btn default btn-xs red" id="btn-delcus-memo"><i class="fa fa-trash"></i></a></center>'], true);
            $('#memo').html('');
            $('#addMemo').modal('hide');
        });

        var memoCancel = '';
        var conditionTypeCancel = '';
        var calObjectCancel = ''
        $('#table-memo').on('click', 'tr #btn-editcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var calObjectVal = data[0];
            var memoVal = data[2];
            var conditionTypeVal = data[1];

            calObjectCancel = calObjectVal;
            conditionTypeCancel = conditionTypeVal;
            memoCancel = memoVal;

            var nRow = $(this).parents('tr')[0];
            var tombol = '<center><a class="btn default btn-xs yellow btn-cancelcus" name="btn-cancelcus" id="btn-cancelcus"><i class="fa fa-close"></i></a><a class="btn default btn-xs green btn-savecus" name="btn-savecus" id="btn-savecus"><i class="fa fa-check"></i></a>';

            // Open Data Table
            var openTable = $('#table-memo').dataTable();
            var updateMemo = '<input type="text" id="memo" name="memo" value="' + memoCancel + '" class="form-control">';

            openTable.fnUpdate(updateMemo, nRow, 2, false);
            openTable.fnUpdate(tombol, nRow, 3, false);
        });

        $('#table-memo').on('click', 'tr #btn-cancelcus', function (e) {
            e.preventDefault();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            // Hapus Row 
            mTable.fnDeleteRow(baris);
            var json = JSON.parse('[' + arrMemo + ']');

            // Deklarasi Variabel       
            var CALC_OBJECT = "";
            var CONDITION_TYPE = "";
            var MEMO = "";

            json.forEach(function (object) {
                if (object.CALC_OBJECT == calObjectCancel && object.CONDITION_TYPE == conditionTypeCancel && object.MEMO == memoCancel) {
                    CALC_OBJECT = object.CALC_OBJECT;
                    CONDITION_TYPE = object.CONDITION_TYPE;
                    MEMO = object.MEMO;
                }
            });

            var tombolC = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';

            mTable.fnAddData([CALC_OBJECT, CONDITION_TYPE, MEMO, tombolC]);
        });

        $('#table-memo').on('click', 'tr #btn-savecus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var memo = $(baris).find('#memo').val();

            var tombolDefault = '<center><a class="btn default btn-xs green btn-editcus" name="btn-editcus" id="btn-editcus"><i class="fa fa-pencil"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>';
            $('#table-memo').dataTable().fnUpdate(memo, baris, 2, false);
            $('#table-memo').dataTable().fnUpdate(tombolDefault, baris, 3, false);

            return false;

        });

        $('#table-memo').on('click', 'tr #btn-delcus-memo', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();
            mTable.fnDeleteRow(baris);
        });

        $('#table-memo').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();
            mTable.fnDeleteRow(baris);
        });
    }


    var unitPriceAuto = function () {
        //cari coa
        $('#condition').on('click', 'tr #btn-coa', function () {
            $('#coaModal').modal('show');
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetListCoa",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;
                        return data.data;
                    },
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            //console.log(data,value);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                c_data = $('#condition').DataTable().row(index).node();
                //console.log(index, c_data)

                var elem = $(c_data).find('#coa_prod');
                elem.val(value);

            });
            $('#coaModal').modal('hide');
        });

        $('.input-percentage').inputmask('percentage', {
            'digits': 2,
            'digitsOptional': false,
            rightAlign: true
        });
        // click plus button in condition table
        $('#condition').on('click', 'tr #btn-price', function () {
            var be_id = $('#BUSINESS_ENTITY_ID').val();
            var profit_center = $('#PROFIT_CENTER').val();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();
            var calc_obj = data[0];
            var cond_type = data[1];
            var ro_number = calc_obj.split(" ")[1];
            var ro_data = null
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                }
            });
            if (ro_data) {
                $('#modalTarifPedoman').modal('show');
                $('#CALC_OBJ').val(calc_obj);
                $('#COND_TYPE').val(cond_type);
                var kode_industri = ro_data[7].split(" - ")[0];
                var batas = Number(ro_data[5]) + Number(ro_data[6]);
                var batas_luas = function () {
                    if (kode_industri == "09") {
                        if (batas >= 200) {
                            return 200;
                        } else if (batas >= 100) {
                            return 100;
                        }
                    }
                    return 0;
                }

                if ($.fn.DataTable.isDataTable('#table-pedoman')) {
                    $('#table-pedoman').DataTable().destroy();
                }

                $('#table-pedoman').DataTable({

                    "ajax":
                    {
                        "url": "/TarifPedoman/GetDataActivePedoman",
                        "type": "GET",
                        "data": function (data) {
                            data.be_id = be_id
                            data.kode_industri = kode_industri
                            data.batas_luas = batas_luas
                            return data
                        }
                    },
                    "columns": [
                        {
                            "data": "TIPE_TARIF",
                            "render": function (data, type, full) {
                                if (data == 'PERCENT') {
                                    var aksi = '<label class="btn default btn-xs green btn-pedoman-percent">'
                                        + '<input type="radio" name="btn-cus-pedoman" id="option-' + full.ID + '" autocomplete="off">'
                                        + '</label>'
                                } else if (data == 'TARIF') {
                                    var aksi = '<label class="btn default btn-xs green btn-pedoman-tarif">'
                                        + '<input type="radio" name="btn-cus-pedoman" id="option-' + full.ID + '" autocomplete="off">'
                                        + '</label>'
                                }
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALUE",
                            "class": "dt-body-right",
                            "render": function (data, type, full) {
                                if (full.TIPE_TARIF == 'TARIF') {
                                    var sdata = "Rp " + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                } else if (full.TIPE_TARIF == 'PERCENT') {
                                    var sdata = data.toString() + "%";
                                }
                                return sdata;
                            }
                        },
                        {
                            "data": "TERMINAL_NAME",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "TIPE_TARIF",
                            "class": "dt-body-center"
                        },
                        {
                            "class": "dt-body-center",
                            "render": function (data, type, full) {
                                return full.START_ACTIVE_DATE + " - " + full.END_ACTIVE_DATE
                            }
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,

                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
            }
        });

        // choose pedoman percent
        $('#table-pedoman').on('click', 'tr .btn-pedoman-percent', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();

            $('.percentage').show();
            $('#NJOP_PERCENT').val(data['VALUE']);
            $('#NJOP_PERCENT_DISPLAY').val(data['VALUE']);

            var beid = $('#BUSINESS_ENTITY_ID').val();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().ajax.reload();
            } else {
                $('#table-filter-detail-njop').DataTable({
                    "ajax":
                    {
                        "url": "/TarifPedoman/GetDataActiveNJOP?be_id=" + beid,
                        "type": "GET",
                    },
                    "columns": [
                        {
                            "data": "ACTIVE",
                            "render": function (data) {
                                //console.log(full);
                                var aksi = '<a class="btn default btn-xs green" id="btn-cus-njop"><i class="fa fa-check"></i></a>';
                                return aksi;
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "data": "KEYWORD",
                            "class": "dt-body-center"
                        },
                        {
                            "data": "VALUE",
                            "class": "dt-body-right",
                            "render": function (data, type, full) {
                                var sdata = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                return "Rp " + sdata;
                            }
                        }
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,

                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"]
                    ],

                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                });
            }
        });

        // choose pedoman tarif
        $('#table-pedoman').on('click', 'tr .btn-pedoman-tarif', function () {
            $('#NJOP_PERCENT').val('');
            $('#NJOP_PERCENT_DISPLAY').val('');
            $('.percentage').hide();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().destroy();
            }

            var baris = $(this).parents('tr')[0];
            var table = $('#table-pedoman').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data['VALUE'];

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();

                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val('');
                    var elem3 = $(c_data).find('#persen-from-njop');
                    elem3.val('');
                    var elem2 = $(c_data).find('#formula');
                    elem2.val("D");
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);
                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#modalTarifPedoman').modal('hide');
        });
        
        // choose njop
        $('#table-filter-detail-njop').on('click', 'tr #btn-cus-njop', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail-njop').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var percent = $('#NJOP_PERCENT').val();
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data.VALUE
            //console.log(value, data);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();
                    //console.log(index, c_data)
                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val(100);
                    var elem3 = $(c_data).find('#persen-from-njop');
                    elem3.val(percent);
                    var elem2 = $(c_data).find('#formula');
                    elem2.val("E1");
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);

                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#modalTarifPedoman').modal('hide');
        });
    }

    var unitPriceAuto1 = function () {
        $('.input-percentage').inputmask('percentage', {
            'digits': 2,
            'digitsOptional': false,
            rightAlign: true
        });

        // click plus button in condition table
        $('#condition').on('click', 'tr #btn-price', function () {
            var be_id = $('#BUSINESS_ENTITY_ID').val();
            var profit_center = $('#PROFIT_CENTER').val();

            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();
            var calc_obj = data[0];
            var cond_type = data[1];
            var ro_number = calc_obj.split(" ")[1];
            var ro_data = null
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    //console.log(index);
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                }
            });
            var param = {
                be_id: be_id,
                profit_center: profit_center
            }
            //console.log(ro_data)
            if (ro_data) {
                $.ajax({
                    contentType: "application/json",
                    data: param,
                    method: "GET",
                    url: "/TarifPedoman/GetActivePedoman",
                    success: function (data) {
                        var xdata = null;
                        var batas = Number(ro_data[5]);
                        var kode = ro_data[7].split(" - ")[0];
                        if (data.ID === 0) {
                            swal('Info', 'No data available in tarif pedoman!', 'info')
                        } else {
                            $('#btn-add-njop').prop('href', '/TarifPedoman/Njop?cabang=' + data.BRANCH_ID);
                            $('#CALC_OBJ').val(calc_obj);
                            $('#COND_TYPE').val(cond_type);
                        }

                        // untuk mencari row di table condition

                        // tidak null berarti percent dan tarif
                        if (data.tarif != null) {
                            $.each(data.tarif, function (index, tarif) {
                                //console.log(tarif, kode, batas);
                                if (tarif.kode === kode && tarif.batas <= batas) {
                                    if (xdata === null) {
                                        xdata = tarif;
                                    } else if (xdata.batas < tarif.batas) {
                                        xdata = tarif;
                                    }
                                }
                            })
                            //.then(function () {
                            //console.log(xdata, data);
                            if (data.TIPE_TARIF === 'TARIF') {
                                var elem2 = $(baris).find('#formula');
                                elem2.val("D");
                                var elem = $(baris).find('#unit-price');
                                elem.val(xdata.value);
                                setTimeout(function () {
                                    isiTotalUnitPrice(elem);
                                    //hitungFormula(elem);
                                }, 200);
                            } else if (data.TIPE_TARIF === 'PERCENT') {
                                $('.percentage').show();
                                $('#NJOP_PERCENT').val(xdata.percentage);
                                $('#NJOP_PERCENT_DISPLAY').val(xdata.percentage);
                                $('#tarifNJOP').modal('show');
                            }
                            //});
                        } else if (data.TIPE_TARIF === 'NJOP') {
                            $('.percentage').hide();
                            $('#NJOP_PERCENT').val('');
                            $('#tarifNJOP').modal('show');
                        }
                    }
                })
            }

        })

        // click filter njop button
        $('#btn-filter-data-njop').click(function () {
            var beid = $('#BUSINESS_ENTITY_ID').val();
            var percent = $('#NJOP_PERCENT').val();
            if ($.fn.DataTable.isDataTable('#table-filter-detail-njop')) {
                $('#table-filter-detail-njop').DataTable().destroy();
            }

            $('#table-filter-detail-njop').DataTable({

                "ajax":
                {
                    "url": "/TarifPedoman/GetDataActiveNJOP?be_id=" + beid,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "ACTIVE",
                        "render": function (data) {
                            //console.log(full);
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-njop"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KEYWORD",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALUE",
                        "class": "dt-body-right",
                        "render": function (data, type, full) {
                            var sdata = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            return "Rp " + sdata;
                        }
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        // choose njop
        $('#table-filter-detail-njop').on('click', 'tr #btn-cus-njop', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail-njop').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var percent = $('#NJOP_PERCENT').val();
            var calc_obj = $('#CALC_OBJ').val();
            var cond_type = $('#COND_TYPE').val();
            var value = data.VALUE
            //console.log(value, data);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                if ($(x).html() === calc_obj && $(y).html() === cond_type) {
                    c_data = $('#condition').DataTable().row(index).node();
                    //console.log(index, c_data)
                    var elem4 = $(c_data).find('#persen-from-kondisi-teknis');
                    elem4.val("100");
                    var elem3 = $(c_data).find('#formula');
                    elem3.val("E1");
                    var elem2 = $(c_data).find('#persen-from-njop');
                    elem2.val(percent);
                    var elem = $(c_data).find('#unit-price');
                    elem.val(value);

                    setTimeout(function () {
                        isiTotalUnitPrice(elem);
                        //hitungFormula(elem);
                    }, 200);
                }
            });
            $('#tarifNJOP').modal('hide');
        });
    }

    // Var untuk handle update data
    var simpanUpdateData = function () {
        $('#btn-update-data').click(function () {
            // Header Data
            var RENTAL_REQUEST_NAME = $('#CONTRACT_OFFER_NAME').val();
            var RENTAL_REQUEST_NO = $('#RENTAL_REQUEST_NO').val();
            var CONTRACT_OFFER_TYPE_ID = $('#RENTAL_REQUEST_ID').val();
            var CONTRACT_OFFER_TYPE_NAME = $('#CONTRACT_OFFER_TYPE').val();
            var CONTRACT_OFFER_TYPE = CONTRACT_OFFER_TYPE_NAME.split(" ")[0];
            var BUSINESS_ENTITY_ID = $('#BUSINESS_ENTITY_ID').val();
            //var BUSINESS_ENTITY_NAME = $('#BUSINESS_ENTITY_ID option:selected').text();
            var CONTRACT_OFFER_NAME = $('#CONTRACT_OFFER_NAME').val();
            var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
            var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
            var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
            var CUSTOMER_NAME = $('#CUSTOMER_NAME').val();
            var CUSTOMER_AR = $('#CUSTOMER_AR').val();
            //var PROFIT_CENTER = $('#TERMINAL_NAME').text();
            var PROFIT_CENTER_ID = $('#PROFIT_CENTER').val();
            var USAGE_TYPE_ID = $('#USAGE_TYPE').val();
            var USAGE_TYPE_NAME = $('#USAGE_TYPE option:selected').text();
            var USAGE_TYPE = USAGE_TYPE_NAME.split(" ")[0];;
            var CONTRACT_CURRENCY = $('#CONTRACT_CURRENCY').val();
            var OFFER_STATUS = $('#OFFER_STATUS').val();
            var CONTRACT_OFFER_NO = $('#OFFER_ID').val();
            var arrContractObjects;
            var arrContractConditions;
            var arrContractFrequencies;
            var arrContractMemos;

            var RR_ID = $('#RR_ID').val();
            var RENTAL_REQUEST_TYPE = $('#RENTAL_REQUEST_TYPE').val();
            var CUSTOMER_ID = $('#CUSTOMER_ID').val();
            var OLD_CONTRACT = $('#OLD_CONTRACT').val();
            var arrObjects;

            //cek apakah pada condition terdapat kondisi biaya administrasi atau tidak
            var resultCek = 0;
            var dueDateCek = 0; 
            var DTConditionCek = $('#condition').dataTable();
            var countDTConditionCek = DTConditionCek.fnGetData();
            var DTManualfCek = $('#manual-frequency').dataTable();
            var countDTManualfCek = DTManualfCek.fnGetData();
            secondCellArray = [];
            dueDate = [];

            for (var i = 0; i < countDTConditionCek.length; i++) {
                var itemDTConditionCek = DTConditionCek.fnGetData(i);
                secondCellArray.push(itemDTConditionCek[1]);
            }

            for (var i = 0; i < countDTConditionCek.length; i++) {
                var itemDTConditionCek = DTConditionCek.fnGetData(i);
                dueDate.push(itemDTConditionCek[10]);
            }

            for (var i = 0; i < countDTManualfCek.length; i++) {
                var itemDTConditionCek = DTManualfCek.fnGetData(i);
                dueDate.push(itemDTConditionCek[2]);
            }

            if (secondCellArray.indexOf("Z009 ADMINISTRASI") > -1) {
                resultCek = 1;
                //console.log('ada');
            } else {
                resultCek = 0;
                //console.log('tidak ada');
            }
            //console.log(resultCek);

            if (dueDate.indexOf("") > -1) {
                dueDateCek = 1;
                //console.log('ada');
            } else {
                dueDateCek = 0;
                //console.log('tidak ada');
            }
            //console.log(dueDateCek);


            //if (resultCek == 1) {begin cek adminsitrasi
            //console.log('jalankan insert header dan detail');

            // Cek Apakah data result cek 1

            //console.log(RENTAL_REQUEST_NAME, RENTAL_REQUEST_NO, CONTRACT_OFFER_TYPE, BUSINESS_ENTITY_NAME, CONTRACT_OFFER_NAME, CONTRACT_START_DATE, CONTRACT_END_DATE, TERM_IN_MONTHS, CUSTOMER_NAME, CUSTOMER_AR, PROFIT_CENTER, USAGE_TYPE, CONTRACT_CURRENCY, OFFER_STATUS)
                
            if (RENTAL_REQUEST_NAME && RENTAL_REQUEST_NO && CONTRACT_OFFER_TYPE && CONTRACT_OFFER_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && TERM_IN_MONTHS && CUSTOMER_NAME && CUSTOMER_AR && USAGE_TYPE && CONTRACT_CURRENCY && OFFER_STATUS) {

                // Declare dan getData dari dataTable OBJECT
                var DTObject = $('#rental-object-co').dataTable();
                var countDTObject = DTObject.fnGetData();
                arrContractObjects = new Array();
                arrObjects = new Array();

                for (var i = 0; i < countDTObject.length; i++) {
                    var itemDTObject = DTObject.fnGetData(i);
                    var paramDTObject = {
                        OBJECT_ID: itemDTObject[1],
                        OBJECT_TYPE: itemDTObject[3],
                        RO_NAME: itemDTObject[4],
                        START_DATE: CONTRACT_START_DATE,
                        END_DATE: CONTRACT_END_DATE,
                        LUAS_TANAH: itemDTObject[5],
                        LUAS_BANGUNAN: itemDTObject[6],
                        INDUSTRY: itemDTObject[7]
                    }

                    var param2 = {
                        RO_NUMBER: itemDTObject[0],
                        OBJECT_ID: itemDTObject[1],
                        OBJECT_TYPE_ID: itemDTObject[2],
                        LAND_DIMENSION: itemDTObject[5],
                        BUILDING_DIMENSION: itemDTObject[6],
                        INDUSTRY: itemDTObject[7]
                    };
                    arrObjects.push(param2);

                    arrContractObjects.push(paramDTObject);
                }
                // End Declare dan getData dari dataTable OBJECT

                // start getData dari dataTable CONDITION
                var DTCondition = $('#condition').dataTable();
                var countDTCondition = DTCondition.fnGetData();
                arrContractConditions = new Array();
                for (var i = 0; i < countDTCondition.length; i++) {
                    var itemDTCondition = DTCondition.fnGetData(i);

                    // hitung pembagian total net value berdasarkan frekuensi pembayaran
                    var freq_pembayaran = itemDTCondition[8];
                    var net_value = itemDTCondition[17];
                    var formulanya = itemDTCondition[6];
                    var jumlah_bulan = itemDTCondition[4];
                    var pecah_bayar = 0;

                    // Cek apakah frekuensi pembayaran bulanan atau tahunan
                    if (freq_pembayaran == 'YEARLY') {
                        var tahunan = Number(jumlah_bulan) / 12;
                        pecah_bayar = Number(net_value) / Number(tahunan);
                        //console.log('tahunan ' + pecah_bayar);
                    }
                    else if (freq_pembayaran == 'MONTHLY') {
                        var bulanan = jumlah_bulan;
                        pecah_bayar = Number(net_value) / Number(bulanan);
                        //console.log('bulanan ' + pecah_bayar);
                    }
                    else {
                        pecah_bayar = Number(net_value);
                        //console.log('else ' + pecah_bayar);
                    }

                    // Cek apakah status condition yes atau no
                    var status_condition = itemDTCondition[5];
                    var nilai_condition = 0;
                    if (status_condition == "Yes") {
                        nilai_condition = 1;
                    }
                    else {
                        nilai_condition = 0;
                    }

                    var rUnitPrice = itemDTCondition[7];
                    var nfUnitPrice = parseFloat(rUnitPrice.split(',').join(''));

                    var rTotal = itemDTCondition[14];
                    var nfTotal = parseFloat(rTotal.split(',').join(''));

                    var rTotalNetValue = itemDTCondition[17];
                    var nfTotalNetValue = parseFloat(rTotalNetValue.split(',').join(''));

                    var paramDTCondition = {
                        CALC_OBJECT: itemDTCondition[0],
                        CONDITION_TYPE: itemDTCondition[1],
                        VALID_FROM: itemDTCondition[2],
                        VALID_TO: itemDTCondition[3],
                        MONTHS: itemDTCondition[4],
                        STATISTIC: nilai_condition.toString(),
                        FORMULA: itemDTCondition[6],
                        UNIT_PRICE: nfUnitPrice.toString(),
                        AMT_REF: itemDTCondition[8],
                        FREQUENCY: itemDTCondition[9],
                        START_DUE_DATE: itemDTCondition[10],
                        MANUAL_NO: itemDTCondition[11].toString(),
                        MEASUREMENT_TYPE: itemDTCondition[12],
                        LUAS: itemDTCondition[13],
                        TOTAL: nfTotal.toString(),
                        NJOP_PERCENT: itemDTCondition[15],
                        KONDISI_TEKNIS_PERCENT: itemDTCondition[16],
                        TOTAL_NET_VALUE: nfTotalNetValue.toString(),
                        COA_PROD: itemDTCondition[18],
                        TAX_CODE: itemDTCondition[20],
                        INSTALLMENT_AMOUNT: isNaN(pecah_bayar) ? "0" : pecah_bayar.toString()
                    };
                    arrContractConditions.push(paramDTCondition);
                }
                //END PUSH CONDITION
                //Declare dan get data dari dataTable MANUAL FREQUENCY
                var DTManualFrequency = $('#manual-frequency').dataTable();
                var countDTManualFrequency = DTManualFrequency.fnGetData();
                arrContractFrequencies = new Array();
                for (var i = 0; i < countDTManualFrequency.length; i++) {
                    var itemDTManualFrequency = DTManualFrequency.fnGetData(i);

                    //potong2  
                    var raw_manual_freq = itemDTManualFrequency[1];
                    var splitselectedCondition = raw_manual_freq.split(" | ");
                    var code_lengkap = splitselectedCondition[1];
                    var cut = code_lengkap.substring(0, 4);
                    //console.log(cut);

                    var splitChar = raw_manual_freq.split(" | ");
                    var objectId = splitChar[0];

                    var net_value = itemDTManualFrequency[3];
                    var nfNetVal = parseFloat(net_value.split(',').join(''));
                    var paramDTManualFrequency = {
                        MANUAL_NO: itemDTManualFrequency[0].toString(),
                        CONDITION: cut, //Untuk condition ambil id CONDITION_CODE di index 9 yg di hidden
                        DUE_DATE: itemDTManualFrequency[2],
                        QUANTITY: itemDTManualFrequency[4],
                        UNIT: itemDTManualFrequency[5],
                        NET_VALUE: nfNetVal.toString(),
                        OBJECT_ID: objectId
                    }
                    arrContractFrequencies.push(paramDTManualFrequency);
                }
                //END PUSH FREQUENCY
                //Declare dan get data dari dataTable MEMO
                var DTMemo = $('#table-memo').dataTable();
                var countDTMemo = DTMemo.fnGetData();
                arrContractMemos = new Array();
                for (var i = 0; i < countDTMemo.length; i++) {
                    var itemDTMemo = DTMemo.fnGetData(i);
                    var paramDTMemo = {
                        CALC_OBJECT: itemDTMemo[0],
                        CONDITION_TYPE: itemDTMemo[1],
                        MEMO: itemDTMemo[2]
                    }
                    arrContractMemos.push(paramDTMemo);
                }
                var paramContract = {
                    CONTRACT_OFFER_NO: CONTRACT_OFFER_NO,
                    RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                    RENTAL_REQUEST_NO: RENTAL_REQUEST_NO,
                    CONTRACT_OFFER_F: CONTRACT_OFFER_TYPE,
                    CONTRACT_OFFER_TYPE: CONTRACT_OFFER_TYPE,
                    BUSINESS_ENTITY_NAME: BUSINESS_ENTITY_NAME,
                    BE_ID: BUSINESS_ENTITY_ID,
                    CONTRACT_OFFER_NAME: CONTRACT_OFFER_NAME,
                    CONTRACT_START_DATE: CONTRACT_START_DATE,
                    CONTRACT_END_DATE: CONTRACT_END_DATE,
                    TERM_IN_MONTHS: TERM_IN_MONTHS,
                    CUSTOMER_NAME: CUSTOMER_NAME,
                    CUSTOMER_AR: CUSTOMER_AR,
                    PROFIT_CENTER: PROFIT_CENTER_ID,
                    USAGE_TYPE: USAGE_TYPE_NAME,
                    CONTRACT_USAGE: USAGE_TYPE,
                    CURRENCY: CONTRACT_CURRENCY,
                    OFFER_STATUS: OFFER_STATUS,
                    Objects: arrContractObjects,
                    Conditions: arrContractConditions,
                    Frequencies: arrContractFrequencies,
                    Memos: arrContractMemos,
                    TYPE_EDIT : "revise",
                };
                // Ajax Save Data Header
                //console.log('length json : ' + JSON.stringify(paramContract).length);

                //var serializer = new JavaScriptSerializer();
                //serializer.MaxJsonLength = Int32.MaxValue;

                var paramRequest = {
                    ID: RR_ID,
                    RENTAL_REQUEST_TYPE: CONTRACT_OFFER_TYPE_ID,
                    BE_ID: BUSINESS_ENTITY_ID,
                    RENTAL_REQUEST_NAME: RENTAL_REQUEST_NAME,
                    CONTRACT_USAGE: USAGE_TYPE_ID,
                    CUSTOMER_NAME: CUSTOMER_NAME,
                    CONTRACT_START_DATE: CONTRACT_START_DATE,
                    CONTRACT_END_DATE: CONTRACT_END_DATE,
                    CUSTOMER_ID: CUSTOMER_ID,
                    CUSTOMER_AR: CUSTOMER_AR,
                    OLD_CONTRACT: OLD_CONTRACT,
                    RENTAL_REQUEST_NO: RENTAL_REQUEST_NO,
                    Objects: arrObjects
                };

                //console.log(paramRequest);
                //console.log(paramContract);
                App.blockUI({ boxed: true });

                //simpan contract
                $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(paramRequest),
                    method: "POST",
                    url: "/TransRentalRequest/EditData",
                    timeout: 30000,
                    success: function () {
                        reqContract();
                    }
                });

                var reqContract = function () {
                    //console.log(paramContract);
                    $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(paramContract),
                        // data: serializer.Serialize(JSON.stringify(param)),
                        method: "POST",
                        url: "/TransContractOffer/UpdateHeader",

                        success: function (data) {
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location = "/TransContractOffer/EditOfferSurat/" + data.data ;
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransContractOffer/EditOfferSurat/" + data.data;
                                });
                            }
                            App.unblockUI();
                        },
                        error: function (data) {
                            swal('Failed', 'Failed to Update Data', 'error').then(function (isConfirm) {
                                window.location = "/TransContractOffer";
                            });
                        }
                    });
                }

                arrObjects = [];
                arrContractObjects = [];
                arrContractConditions = [];
                arrContractFrequencies = [];
                arrContractMemos = [];
            }
            else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });


    }


    var mdm = function () {

        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/TransContractOffer/Customer",
                    data: { 'MPLG_NAMA': inp },
                    //contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                //console.log(ui.item);
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
                //$("#CUSTOMER_ADDRESS").val(ui.item.address);
                cekPiut("1B", ui.item.sap);
            }
        });
    }

    var cekPiut = function (doc_type, cust_code) {
        var param = {
            DOC_TYPE: doc_type,
            CUST_CODE: cust_code
        };
        $.ajax({
            type: "POST",
            url: "/TransRentalRequest/cekPiutangSAP",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.E_RELEASE_STATUS == "X") {
                    $("#cPiut").val(data.E_RELEASE_STATUS);
                    swal('Warning', data.E_DESCRIPTION, 'error');
                    //return data.E_RELEASE_STATUS;
                }
                else {
                    $("#cPiut").val("1");
                }
            }
        });
    }
    

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }
    return {
        init: function () {
            unitPriceAuto();
            batal();
            hitung_bulan();
            hitung_tanggal_sampai();
            filter();
            filterRO();
            addCondition();
            simpanUpdateData();
            getDataObject();
            editDataCondition();
            editDataManualFrequency();
            editDataMemo();
            mdm();
            initOldContract();
            datePicker();
        }

    };

}();

// ************** FUNCTION UNTUK HANDLE SALES RULE ********************
//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getUnitST(this_cmb) {
    var selectedSalesType = $(this_cmb).val();
    var splitselectedSalesType = selectedSalesType.split(" - ");
    var id_sales_type = splitselectedSalesType[0];
    var nama_sales_type = splitselectedSalesType[1];

    //Ajax untuk get UNIT
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataUnitST/" + id_sales_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].UNIT;
            }
            $(this_cmb).parents('tr').find('input[name="UNIT_ST"]').val(listItems);
        }
    });
}
// ************** END FUNCTION UNTUK HANDLE SALES RULE ********************

//Function untuk isi condition code
function getConditionCode(this_cmb) {
    var selectedCondition = $(this_cmb).val();

    if (selectedCondition) {
        var splitselectedCondition = selectedCondition.split(" | ");
        var code_lengkap = splitselectedCondition[1];
        var cut = code_lengkap.substring(0, 4);
        //console.log(selectedCondition);
        //console.log(code_lengkap);
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val(cut);
    }
    else {
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val("");
    }
}

//Function untuk add row table manual frequency 
function addManualFreq(this_x) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var tim = data[4];
}

//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getLuas(this_cmb) {
    //console.log($(this_cmb).val());
    var selectedMeasType = $(this_cmb).val();
    var splitselectedMeasType = selectedMeasType.split("|");
    var id_detail_meas_type = splitselectedMeasType[1];

    //Ajax untuk get Luas(M2)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataLuasMeasType/" + id_detail_meas_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].LUAS;
            }
            // $("#luas").val(listItems);
            $(this_cmb).parents('tr').find('input[name="luas"]').val(listItems);


            //Mengisi field total value saat field luas terisi dengan value ajax listItems
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            //formulanya
            var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
            if (formula == 'A') {

            }
            else if (formula == 'U') {

            }
            else {
                //var tim = data[4];
                //console.log('FORMULA <> A DAN U');
                var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
                var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
                var luas = listItems;
                var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

                nMUnitPrice = parseFloat(unitPrice.split(',').join(''));
                var t = hitungTotal(tim, luas, nMUnitPrice, period);
                var fT = Math.round(t).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                //console.log("Total Value = " + fT);
                $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

                //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
                var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
                var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
                var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
                var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

                var nfTotal = parseFloat(total.split(',').join(''));

                var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
                var nfPersenFromKondisiteknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));


                var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiteknis);
                var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                //console.log(fNetValue);
                $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
            }
        }
    });
}


//---------------------------HITUNG TOTAL----------------------------
//ACTION UNTUK MENGISI TOTAL VALUE SAAT CLICK COMBO AMT REF
function isiTotalAMTREF(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var period = $(this_cmb).val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);

    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        //Hitung Total Untuk Formula D
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(unit_price.split(',').join(''));
        var rTotal = hitungTotal(tim, luas, nfHarga, kode_periode);

        var fTotal = rTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fTotal);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = netVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}

function isiTotalUnitPrice(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = Math.round(unit_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        var fT = t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

        var njop;
        var kondisi_teknis;

        // update total
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        //console.log('total bulan ' + total_bulan);
        //console.log('nf harga ' + nfHarga);
        //console.log('luas ' + luas);
        //console.log('kode periode ' + kode_periode);

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //console.log('formula ' + formula);
        //console.log('njop ' + njop);
        //console.log('kondisi teknis ' + persenFromKondisiTeknis);

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}
//---------------------END OF HITUNG TOTAL----------------------------

function hitungTotal(total_bulan, luas, harga, kode_periode) {
    var total = 0;

    if (typeof harga == "string") {
        harga = harga.replace(/,/g, "");
    }
    if (kode_periode == 'YEARLY') {
        var year = Number(total_bulan) / 12;
        total = (Number(luas) * Number(harga)) * Number(year);
        //console.log('luas = ' + luas);
        //console.log('harga = ' + harga);
        //console.log('tahun = ' + year);

        //console.log('TOTAL = ' + total);
    }
    else if (kode_periode == 'MONTHLY') {
        total = (Number(luas) * Number(harga)) * Number(total_bulan);
    }
    else {
        total = Number(luas) * Number(harga);
    }
    return total;
}

//Action Saat Formula Dipilih
function hitungFormula(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
    var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();

    var amt_ref = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split('.').join(''));
    var nfLuas = parseFloat(luas.split('.').join(''));
    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    if (formula == 'A') {
        //var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        // $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        // $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'D') {
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //Formula Lama
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);

        //netValue(formula, total, njop, kondisi_teknis)
        //console.log('PROSEDUR JALAN ');
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        //console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
    }
}

//Action Saat Formula sudah dipilih dan mengisi persentase njop
function hitungFormulaN(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    //var nfPersenFromNjop = parseFloat(persen_from_njop.split('.').join(''));
    var nfPersenFromNjop = persen_from_njop;
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    //console.log('net value ' + net_value);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}

//Action Saat Formula sudah dipilih dan mengisi persentase kondisi teknis
function hitungFormulaT(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = persen_from_njop;
    //var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split('.').join(''));
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // ini masih kurang sip
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}



//Function untuk hitung net value berdasarkan formula yang dipilih
function netValue(formula, total, njop, kondisi_teknis) {
    var totalNetValue = 0;
    if (typeof total == "string") {
        total = total.replace(/,/g, "");
    }
    if (formula == 'D') {
        totalNetValue = Number(total);
    }
    else if (formula == 'E1') {
        if (njop == '' && kondisi_teknis == '') {
            totalNetValue = Number(total);
        }
        else if (njop == '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100);
        }
        else if (kondisi_teknis == '') {
            totalNetValue = Number(total) * (Number(njop) / 100);
        }
        else if (njop != '' && kondisi_teknis != '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100) * (Number(njop) / 100);
        }
        else {

        }
    }
    else if (formula == 'A') {
        totalNetValue = Number(total);
    }
    else {

    }

    return totalNetValue;
}

// Function untuk remove array value berdasarkan value
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function hitungTermMonths(this_cmb) {
    //if ($(this_cmb).not(':focus')) return;
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val();

    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

    var dateAkhir = t;
    var datearrayAkhir = dateAkhir.split("/");
    var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

    var tim = monthDiff2(new Date(newdateMulai), new Date(newdateAkhir));

    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val(tim);

    hitungFormula(this_cmb);
    //// handle perhitungannya
    //var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    //var tim2 = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    //var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    //var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    //console.log(tim2, luas, unitPrice, period);
    //var tx = hitungTotal(tim2, luas, unitPrice, period);
    //console.log(tx);
    //$(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    ////ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    //var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    //var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    //var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    //var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    //console.log(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function hitungEndDate(this_cmb) {
    //if ($(this_cmb).not(':focus')) return;
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

    //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
    var f_date = new Date(newdateMulai);
    var n_tim = Number(tm);
    var end_date = addMonths2(f_date, n_tim);
    var xdate = new Date(end_date);
    var f_date = xdate.toISOString().slice(0, 10);

    //format f_date menjadi dd/mm/YYYY
    var dateAkhir = f_date;
    var datearrayAkhir = dateAkhir.split("-");
    var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];

    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val(newdateAkhir);
    hitungFormula(this_cmb);
    //// handle perhitungannya
    //var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    //var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    //var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    //var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    //var tx = hitungTotal(tim, luas, unitPrice, period);
    //$(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    ////ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    //var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    //var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    //var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    //var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    //var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function monthDiff2(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();

    if (d2.getDate() >= d1.getDate())
        months++

    return months <= 0 ? 0 : months;
}

function addMonths2(date, count) {
    if (date && count) {
        var m, d = (date = new Date(+date)).getDate()

        date.setMonth(date.getMonth() + count, 1)
        m = date.getMonth()
        date.setDate(d)
        if (date.getMonth() !== m) date.setDate(0)
    }
    return date
}


// Function untuk handle save Sales Rule (Formula U)
function saveSalesRule() {
    // Untuk get data table condition

    // Untuk ambil sales rule

    // 
}

function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}

//Function untuk isi condition code
function getCoaProduksi2(coa_prod, this_cmb) {
    //console.log(this_cmb)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(this_cmb).find(".coa_prod").html('');
            $(this_cmb).find(".coa_prod").append(listItems);
            $(this_cmb).find(".coa_prod option[value='" + coa_prod + "']").prop('selected', true);
        }
    });

    //$("#coa_prod").select2({
    //    allowClear: true,
    //    width: '100%',
    //    ajax: {
    //        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
    //        dataType: "json",
    //        contentType: "application/json",
    //        data: function (params) {
    //            return {
    //                search: params.term,
    //                page: params.page,
    //            }
    //        },
    //        processResults: function (result) {
    //            return {
    //                results: $.map(result.data, function (item) {
    //                    return {
    //                        text: item.REF_DESC,
    //                        data: item,
    //                        id: item.VAL1
    //                    }
    //                })
    //            };
    //        }
    //    },
    //    escapeMarkup: function (markup) { return markup; },
    //    minimumInputLength: 3,
    //    placeholder: "-- Choose COA Production --",
    //});
    //var $newOption = $("<option selected='selected'></option>").val(coa_prod).text(coa_prod);
    //$("#coa_prod").append($newOption).trigger('change');
}

function getCoaProduksi() {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            function isEmpty(el) {
                return !$.trim(el.html())
            }
            $(".coa_prod").each(function (i, obj) {
                if (isEmpty($(obj))) {
                    $(obj).append(listItems);
                }
            });
            
        }
    });
    //$("#coa_prod").select2({
    //    allowClear: true,
    //    width: '100%',
    //    ajax: {
    //        url: "/TransContractOffer/GetDataDropDownCoaProduksiLahan",
    //        dataType: "json",
    //        contentType: "application/json",
    //        data: function (params) {
    //            return {
    //                search: params.term,
    //                page: params.page,
    //            }
    //        },
    //        processResults: function (result) {
    //            return {
    //                results: $.map(result.data, function (item) {
    //                    return {
    //                        text: item.REF_DESC,
    //                        data: item,
    //                        id: item.VAL1
    //                    }
    //                })
    //            };
    //        }
    //    },
    //    escapeMarkup: function (markup) { return markup; },
    //    minimumInputLength: 3,
    //    placeholder: "-- Choose COA Production --",
    //});
}
jQuery(document).ready(function () {
    TableDatatablesEditable.init();
    DataSurat.init();
    $('#condition > tbody').html('');
    $('#condition-option > option').html('');
    $('#manual-frequency > tbody').html('');
    $('#table-memo > tbody').html('');
    $('#rental-object-co > tbody').html('');
    $('#sales-based > tbody').html('');
    $('#reporting-rule > tbody').html('');
    $('.sembunyi').hide();

    var currency = $('#CURRENCY').val();
    $("#CONTRACT_CURRENCY option[value='" + currency + "']").prop('selected', true);


});

$.ajaxPrefilter(function (options, original_Options, jqXHR) {
    options.async = true;
});

function infoIsiManual() {
    var valFrequency = $('#frequency').val();

    if (valFrequency == 'MANUAL') {
        swal('Info', 'Mohon Mengisi \'MANUAL NO\' Untuk Pilihan Manual!', 'info');
    }
    else {

    }
}


function check() {
    var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
    var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();

    if (CONTRACT_START_DATE == "" && CONTRACT_END_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_START_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else if (CONTRACT_END_DATE == "") {
        $('#addDetailModal').modal('hide');
        document.getElementById("btn-detil-filter").href = "ubah";
        swal('Info', 'You have to fill Contract Start Date and Contract End Date value', 'info');
    }
    else {
        document.getElementById("btn-detil-filter").href = "#addDetailModal";
    }

    $('#from').val(CONTRACT_START_DATE);
    $('#to').val(CONTRACT_END_DATE);
}
function coHitungArea() {
    var con = $('#rental-object-co').dataTable();
    var all_area = 0;
    for (var i = 0; i < con.fnGetData().length; i++) {
        var area = con.fnGetData(i)[1];
        var area = con.fnGetData(i)[5];
        all_area = all_area + Number(area);
        //console.log(all_area);
    }
    //console.log(all_area);
    $('.LAND_AREA_NEW').html(all_area.toLocaleString('en-US'));
}

function coHitungValue() {
    var con = $('#condition').dataTable();
    var all_value = 0;
    //console.log(con.fnGetData());
    $('.DETAIL_RO_NEW').html('')
    $.each(con.fnGetData(), function (i, fndata) {
        var net_val = con.fnGetData(i)[17];
        //var price = con.fnGetData(i)[6];
        var calc_ob = con.fnGetData(i)[0];
        //console.log(net_val, calc_ob);
        if (calc_ob != 'Header Document') {
            all_value = all_value + parseFloat(net_val.split(',').join(''));
            var ro_number = calc_ob.split(' ')[1];
            var elems = $('#rental-object-co').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() === ro_number) {
                    //console.log(index);
                    ro_data = $('#rental-object-co').DataTable().row(index).data();
                    var luas = Number(ro_data[5]) + Number(ro_data[6]);
                    var text = $('.DETAIL_RO_NEW').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CONTRACT_CURRENCY').val() + '</td></tr>';
                    $('.DETAIL_RO_NEW').html(text)
                    //console.log(text);
                }
            });
        }
    });
    //console.log(text);
    $('.CONTRACT_VALUE_NEW').html(all_value.toLocaleString('en-US'));
}

//$('body').on('click', '#previewPDF', function () {

//    $('#openPDF').modal('show');

//    var id_surat = $('#ID').val();
//    $('.upload-iframe').empty();
//    var tpl = $('#pdf-upload-iframe');
//    var template = tpl.html().replace(/__INDEX__/g, id_surat);
//    $('.upload-iframe').append(template);

//    /*var req = $.ajax({
//        contentType: "application/json",
//        data: "id=" + id_surat,
//        method: "get",
//        url: "/TransContractOffer/GetSurat",
//        timeout: 30000,
//        async: false,
//    });*/
//    //console.log = id_surat;

//});

$('body').on('click', '#previewPDF', function () {
    var dataTables = $('#table-attachment').DataTable();
    var rows = dataTables.rows().data().length;
    var text = "";
    for (var i = 0; i < rows; i++) {
        text += (i > 0 ? ';' : '') + dataTables.rows().data()[i].DIRECTORY;
    }
    console.log(text);
    directory = text;
    var textareaValue = $('#ISI_SURAT').summernote('code');
    var dataNoSurat = $("#NO_SURAT").val();
    var dataSubject = $("#SUBJECT").val();
    var atasNama = $("#ATAS_NAMA").val();

    //if (textareaValue != "" && dataNoSurat != "" && dataSubject != "" && $("#KEPADA").val() != null && $("#TTD").val() != null) {
    //App.blockUI({ boxed: true });
    var form = $("#contract-surat")[0];
    var data = new FormData(form);
    var nama_kepada = '';
    var kepada = '';
    var selections = ($("#KEPADA").select2('data'));
    selections.forEach(function (e, i) {
        nama_kepada += (i > 0 ? ';' : '') + e.text;
        kepada += (i > 0 ? ';' : '') + e.id;
    });

    var tembusan = '';
    var nama_tembusan = '';
    var selections = ($("#TEMBUSAN").select2('data'));
    selections.forEach(function (e, i) {
        nama_tembusan += (i > 0 ? ';' : '') + e.text;
        tembusan += (i > 0 ? ';' : '') + e.id;
    });
    var dataTtd = $('#TTD').select2('data');

    var tembusanEksternal = '';
    var tbsEksternal = ($('input[name^=to]'));
    $.each(tbsEksternal, function (e, i) {
        tembusanEksternal += (e > 0 ? ';' : '') + i.value;
    });
    var kepadaEksternal = '';
    var kpdEksternal = ($('input[name^=ti]'));
    $.each(kpdEksternal, function (e, i) {
        kepadaEksternal += (e > 0 ? ';' : '') + i.value;
    });

    data.append('ISI_SURAT', textareaValue);
    data.append('KEPADA', kepada);
    data.append('TEMBUSAN', tembusan);
    data.append('NAMA_TEMBUSAN', nama_tembusan);
    data.append('NAMA_KEPADA', nama_kepada);
    data.append('KEPADA_EKSTERNAL', kepadaEksternal);
    data.append('TEMBUSAN_EKSTERNAL', tembusanEksternal);
    data.append('NAMA_TTD', dataTtd[0].text);
    data.append('CONTRACT_NO', $('#CONTRACT_OFFER_NO').val());
    data.append('LIST_FILE', LIST_FILE);
    data.append('DIRECTORY', directory);

    $('body').append('<form action="/pdf/previewSuratPdf" method="post" target="iframe-pdf" id="postToIframe"></form>');

    $('#postToIframe').append('<textarea name="ISI_SURAT">' + textareaValue + '</textarea>');
    $('#postToIframe').append('<input type="text" name="NAMA_KEPADA" value="' + nama_kepada + '"/>');
    $('#postToIframe').append('<input type="text" name="NAMA_TEMBUSAN" value="' + nama_tembusan + '"/>');
    $('#postToIframe').append('<input type="text" name="TEMBUSAN_EKSTERNAL" value="' + tembusanEksternal + '"/>');
    $('#postToIframe').append('<input type="text" name="KEPADA_EKSTERNAL" value="' + kepadaEksternal + '"/>');
    $('#postToIframe').append('<input type="text" name="NO_SURAT" value="' + dataNoSurat + '"/>');
    $('#postToIframe').append('<input type="text" name="SUBJECT" value="' + dataSubject + '"/>');
    $('#postToIframe').append('<input type="text" name="NAMA_TTD" value="' + dataTtd[0].text + '"/>');
    $('#postToIframe').append('<input type="text" name="ATAS_NAMA" value="' + atasNama + '"/>');
    $('#postToIframe').append('<input type="text" name="LIST_FILE" value="' + LIST_FILE + '"/>');
    $('#postToIframe').append('<input type="text" name="DIRECTORY" value="' + directory + '"/>');

    $('.upload-iframe').empty();

    var iframe = '<iframe id="iframe-pdf" name="iframe-pdf" width="100%" style="height:100vh !important" allowfullscreen webkitallowfullscreen></iframe>';


    $(iframe).insertBefore('#postToIframe');

    $('#iframe-pdf').appendTo('.upload-iframe');
    $('#postToIframe').submit().remove();
    $('#openPDF').modal('show');
});

function coHitungPeriod() {
    var value = $('#TERM_IN_MONTHS').val();
    var start = $('#CONTRACT_START_DATE').val().replace(/\//g, ".");
    var end = $('#CONTRACT_END_DATE').val().replace(/\//g, ".");
    $('.CONTRACT_START_NEW').html(start);
    $('.CONTRACT_END_NEW').html(end);
    $('.CONTRACT_PERIOD_NEW').html(value);
}

function hapusTembusan(t) {
    console.log(t);
    t.parent().parent().remove();
}

function hapusKepada(t) {
    console.log(t);
    t.parent().parent().remove();
}
