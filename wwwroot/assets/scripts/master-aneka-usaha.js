﻿var AnekaUsaha = function () {

    var initTable = function () {
        $('#table-aneka-usaha').DataTable({

            "ajax":
            {
                "url": "VariousBusiness/GetDataAnekaUsaha",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },
            },

            "columns": [
                {
                    "data": "BE_ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "PROFIT_CENTER",
                    "class": "dt-body-center"
                },
                {
                    "data": "SERVICE_GROUP",
                    "class": "dt-body-center"
                },
                {
                    "data": "SERVICE_CODE",
                    "class": "dt-body-center"
                },
                {
                    "data": "SERVICE_NAME",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a class="btn btn-icon-only blue" id="btn-edit"><i class="fa fa-edit"></i></a>';
                         aksi += '<a class="btn btn-icon-only blue" id="btn-status"><i class="fa fa-exchange"></i></a>';
                         aksi += '<a data-toggle="modal" href="#detailModal" class="btn btn-icon-only green" id="btn-detil"><i class="fa fa-eye"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }

    var btnAddPricing = function () {
        $('#btn-add-pricing').click(function () {
            $('#PRICE').val('');
            $('#TAMBAHMULTIPLY_FUNCTION').val('');
            $('#TAMBAHVALID_FROM').val('');
            $('#TAMBAHVALID_TO').val('');
            $('#LEGAL_CONTRACT_NO').val('');
            $('#CONTRACT_DATE').val('');
            $('#CONTRACT_VALIDITY').val('');
        });
    }

    var simpanpricing = function () {
        $('#save-pricing').click(function () {
            var VB_ID = $('#UriId').val();
            var TAMBAHUNIT_1 = $('#TAMBAHUNIT_1').val();
            var TAMBAHPRICE = $('#TAMBAHPRICE').val();
            // var TAMBAHCONDITION_PRICING_UNIT = $('#TAMBAHCONDITION_PRICING_UNIT').val();
            var TAMBAHMULTIPLY_FUNCTION = $('#TAMBAHMULTIPLY_FUNCTION').val();
            var TAMBAHVALID_FROM = $('#TAMBAHVALID_FROM').val();
            var TAMBAHVALID_TO = $('#TAMBAHVALID_TO').val();
            var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO').val();
            var CONTRACT_DATE = $('#CONTRACT_DATE').val();
            var CONTRACT_VALIDITY = $('#CONTRACT_VALIDITY').val();
            var SERVICE_CODE = $('#SERVICE_CODE').val();
            var RAW_BE_ID = $('#BE_ID').val();
            var RAW_PROFIT_CENTER = $('#PROFIT_CENTER').val();
            
            //raw data BE
            var split_be = RAW_BE_ID.split(" - ");
            var BE_ID = split_be[0];

            //raw data BE
            var split_profit_center = RAW_PROFIT_CENTER.split(" - ");
            var PROFIT_CENTER = split_profit_center[0];
            

            if (Date.parse(TAMBAHVALID_FROM) > Date.parse(TAMBAHVALID_TO)) {
                $('#addPricing').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');

            }
            else {
                
                if (VB_ID && TAMBAHUNIT_1 && TAMBAHPRICE && TAMBAHMULTIPLY_FUNCTION && TAMBAHVALID_FROM) {
                    var param = {
                        VB_ID: VB_ID,
                        UNIT: TAMBAHUNIT_1,
                        PRICE: TAMBAHPRICE,
                        //CONDITION_PRICING_UNIT: TAMBAHCONDITION_PRICING_UNIT,
                        MULTIPLY_FUNCTION: TAMBAHMULTIPLY_FUNCTION,
                        VALID_FROM: TAMBAHVALID_FROM,
                        VALID_TO: TAMBAHVALID_TO,
                        LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO,
                        CONTRACT_DATE: CONTRACT_DATE,
                        CONTRACT_VALIDITY: CONTRACT_VALIDITY,
                        SERVICE_CODE: SERVICE_CODE,
                        BE_ID: BE_ID,
                        PROFIT_CENTER: PROFIT_CENTER
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/VariousBusiness/AddDataPricing",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                            $('#addPricing').modal('hide');
                            $('#table-vbp').dataTable().api().ajax.reload();
                        } else {
                            swal('Failed', data.message, 'error');
                            $('#addPricing').modal('hide');
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!.', 'warning');
                    $('#addPricing').modal('hide');
                }
                
            }
        });
    }

    var simpan = function () {
        $('#btn-simpan-anekausaha').click(function () {
            var BE_ID = $('#BE_ID').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var SERVICE_GROUP = $('#SERVICE_GROUP').val();
            var SERVICE_NAME = $('#SERVICE_NAME').val();
            var UNIT = $('#UNIT').val();
            //var TAX_CODE = $('#TAX_CODE').val();

            if (BE_ID && PROFIT_CENTER && SERVICE_GROUP && SERVICE_NAME && UNIT) {
                var param = {
                    BE_ID: BE_ID,
                    PROFIT_CENTER: PROFIT_CENTER,
                    SERVICE_GROUP: SERVICE_GROUP,
                    SERVICE_NAME: SERVICE_NAME,
                    UNIT: UNIT,
                    TAX_CODE: "1"
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/VariousBusiness/AddData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/VariousBusiness';
                        });

                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location.href = '/VariousBusiness';
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!.', 'warning');
            }
        });
    }

    var ubahstatus = function () {
        $('body').on('click', 'tr #btn-status', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID"],
                        method: "get",
                        url: "/VariousBusiness/UbahStatus",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var detailvb = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();
            var vb_id = data["ID"];

            var tableDetilView = $('#table-view-detail').DataTable();
            tableDetilView.destroy();

            $("#BE_ID").val(data["BE_ID"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#SERVICE_NAME").val(data["SERVICE_NAME"]);
            $("#SERVICE_GROUP").val(data["SERVICE_GROUP"]);
            $("#GL_ACCOUNT").val(data["VAL1"]);
            $("#UNIT").val(data["UNIT"]);
            //$("#TAX_CODE").val(data["TAX_CODE"]);

            $('#table-view-detail').DataTable({
                "ajax":
                {
                    "url": "/VariousBusiness/GetDataPricing/"+vb_id,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "columns": [
                    {
                        "data": "PRICE",
                        "class": "dt-body-center",
                         //render: $.fn.dataTable.render.number(',', ',', 0)
                    },
                    {
                        "data": "UNIT_1",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MULTIPLY_FUNCTION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_VALIDITY",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE_1 === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        
        });
    }

    /*
    var initDetilPricing = function () {
        $('body').on('click', 'tr #btn-detil', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();
            var vb_id = data["VB_ID"];

            $('#table-view-detail').DataTable({
                "ajax": {
                    "url": "/VariousBusiness/GetDataPricing/" + vb_id,
                    "type": "GET",
                },
                "columns": [
                    {
                        "data": "PRICE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT_1"
                    },
                    {
                        "data": "MULTIPLY_FUNCTION"
                    },
                    {
                        "data": "VALID_FROM"
                    },
                    {
                        "data": "VALID_TO"
                    },
                    {
                        "data": "LEGAL_CONTRACT_NO"
                    },
                    {
                        "data": "CONTRACT_DATE"
                    },
                    {
                        "data": "CONTRACT_VALIDITY"
                    },
                    {
                        "render": function (data, type, full) {
                            if (full.ACTIVE_1 === '1') {
                                return '<span class="label label-sm label-success"> Active </span>';
                            } else {
                                return '<span class="label label-sm label-danger"> Inactive </span>';
                            }
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": ""
                },

                "filter": false
            });
        }
    )
    }
    */


    var edit = function () {
        $('body').on('click', 'tr #btn-edit', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-aneka-usaha').DataTable();
            var data = table.row(baris).data();

            window.location = "/VariousBusiness/EditVB/" + data["ID"];
        });
    }



    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/VariousBusiness/AddVB';
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location.href = '/VariousBusiness';
        });
    }

    var initTablePricing = function () {
        var uriId = $('#UriId').val();
        $('#table-vbp').DataTable({
            "ajax": {
                "url": "/VariousBusiness/GetDataPricing/" + uriId,
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },
            },
            "columns": [
                {
                    "data": "PRICE",
                    render: $.fn.dataTable.render.number('.', '.', 0),
                    "class": "dt-body-center"
                },
                {
                    "data": "UNIT_1"
                },
                {
                    "data": "MULTIPLY_FUNCTION"
                },
                {
                    "data": "VALID_FROM"
                },
                {
                    "data": "VALID_TO"
                },
                {
                    "data": "LEGAL_CONTRACT_NO"
                },
                {
                    "data": "CONTRACT_DATE"
                },
                {
                    "data": "CONTRACT_VALIDITY"
                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE_1 === '1') {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Inactive </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#editPricing" class="btn default btn-xs green" id="btn-edit-pricing"><i class="fa fa-edit"></i> </a>';
                        aksi += '<a class="btn default btn-xs blue" id="btn-status-pricing"><i class="fa fa-exchange"></i> </a>';

                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive":false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": false
        });
    }

    var update = function () {
        $('#btn-update').click(function () {
            var ID = $('#ID').val();
            var SERVICE_NAME = $('#SERVICE_NAME').val();
            var UNIT = $('#UNIT').val();

            if (SERVICE_NAME && UNIT) {
                var param = {
                    ID: ID,
                    SERVICE_NAME: SERVICE_NAME,
                    UNIT: UNIT
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/VariousBusiness/EditData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location.href = '/VariousBusiness';
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location.href = '/VariousBusiness';
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var ubahstatuspricing = function () {
        $('body').on('click', 'tr #btn-status-pricing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-vbp').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "id=" + data["ID_BP"],
                        method: "get",
                        url: "/VariousBusiness/UbahStatusPricing",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var dataeditpricing = function () {
        $('body').on('click', 'tr #btn-edit-pricing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-vbp').DataTable();
            var data = table.row(baris).data();

            $("#ID_BP").val(data["ID_BP"]);
            $("#EDITPRICE").val(data["PRICE"]);
            $("#EDITCONDITION_PRICING_UNIT").val(data["CONDITION_PRICING_UNIT"]);
            $("#EDITMULTIPLY_FUNCTION").val(data["MULTIPLY_FUNCTION"]);
            $("#EDITVALID_FROM").val(data["VALID_FROM"]);
            $("#EDITVALID_TO").val(data["VALID_TO"]);
            $("#LEGAL_CONTRACT_NO_EDIT").val(data["LEGAL_CONTRACT_NO"]);
            $("#CONTRACT_DATE_EDIT").val(data["CONTRACT_DATE"]);
            $("#CONTRACT_VALIDITY_EDIT").val(data["CONTRACT_VALIDITY"]);
        });
    }

    var updatedatapricing = function () {
        $('#update-pricing').click(function () {
            var ID_V = $('#ID_BP').val();
            var PRICE = $('#EDITPRICE').val();
            // var CONDITION_PRICING_UNIT = $('#EDITCONDITION_PRICING_UNIT').val();
            var MULTIPLY_FUNCTION = $('#EDITMULTIPLY_FUNCTION').val();
            var UNIT_1 = $('#EDITUNIT_1').val();
            var VALID_FROM = $('#EDITVALID_FROM').val();
            var VALID_TO = $('#EDITVALID_TO').val();
            var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO_EDIT').val();
            var CONTRACT_DATE = $('#CONTRACT_DATE_EDIT').val();
            var CONTRACT_VALIDITY = $('#CONTRACT_VALIDITY_EDIT').val();

            if (Date.parse(VALID_FROM) > Date.parse(VALID_TO)) {
                $('#editPricing').modal('hide');
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (PRICE && MULTIPLY_FUNCTION && VALID_FROM && UNIT_1) {
                    var param = {
                        ID: ID_V,
                        PRICE: PRICE,
                        // CONDITION_PRICING_UNIT: CONDITION_PRICING_UNIT,
                        MULTIPLY_FUNCTION: MULTIPLY_FUNCTION,
                        UNIT_1: UNIT_1,
                        VALID_FROM: VALID_FROM,
                        VALID_TO: VALID_TO,
                        LEGAL_CONTRACT_NO: LEGAL_CONTRACT_NO,
                        CONTRACT_DATE: CONTRACT_DATE,
                        CONTRACT_VALIDITY: CONTRACT_VALIDITY
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/VariousBusiness/EditDataPricing",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            $('#editPricing').modal('hide');
                            swal('Success', data.message, 'success');
                            $('#table-vbp').dataTable().api().ajax.reload();
                        } else {
                            $('#editPricing').modal('hide');
                            swal('Failed', data.message, 'error');
                        }
                    });
                } else {
                    $('#editPricing').modal('hide');
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }

        });
    }
    //var comboServiceGroup = function () {
    //    // Ajax untuk ambil tarif dengan code L
    //    $.ajax({
    //        type: "GET",
    //        url: "/TransVBList/GetDataDropDownServiceGroup",
    //        contentType: "application/json",
    //        dataType: "json",
    //        success: function (data) {
    //            var jsonList = data;
    //            var listItems = "";
    //            listItems += "<option value=''>-- Chose Service Gorup --</option>";
    //            for (var i = 0; i < jsonList.data.length; i++) {
    //                listItems += "<option value='" + jsonList.data[i].VAL1 + '|' + jsonList.data[i].VAL3 + '|' + jsonList.data[i].VAL4 + "'>" + jsonList.data[i].PENDAPATAN + "</option>";
    //            }
    //            $("#SERVICE_GROUP").html('');
    //            $("#SERVICE_GROUP").append(listItems);
    //        }
    //    });

    //}
    return {
        init: function () {
            initTable();
            add();
            ubahstatus();
            batal();
            edit();
            simpan();
            detailvb();
            initTablePricing();
            update();
            ubahstatuspricing();
            dataeditpricing();
            updatedatapricing();
            simpanpricing();
           // initDetilPricing();
            btnAddPricing();
            //comboServiceGroup();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        AnekaUsaha.init();
    });
}

function test() {
    var servgroup = $('#SERVICE_GROUP').val();
    //console.log("testtest");
  //  console.log(servgroup);
    var tttt = $("#SERVICE_GROUP option:selected").text();
    var servgroup2 = tttt.split(" | ");
    var paramCoa = servgroup2[1];
    console.log(paramCoa);
    
   // console.log(paramCoa);

    $.ajax({
        type: "GET",
        url: "/TransVBList/Getunit?param=" + paramCoa,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Choose Unit --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].SATUAN_MDM + "'>" + jsonList.data[i].SATUAN_MDM + "</option>";
            }
            $("#UNIT").html('');
            $("#UNIT").append(listItems);
        }
    });
}