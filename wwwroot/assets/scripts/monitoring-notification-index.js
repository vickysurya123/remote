﻿var MonitoringNotification = function () {

    //----------------------------- JS UNTUK NOTIFICATION CONTRACT --------------------------

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/MonitoringNotification/GetDataBusinessEntityNew",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BRANCH_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BUSINESS_ENTITY").html('');
                $("#BUSINESS_ENTITY").append(listItems);
            }
        });
    }
    
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd/mm/yyyy",
                orientation: "left",
                autoclose: true
            });
        }
    }
    var initTable = function () {
        $('#table-list-notification').DataTable({
            "ajax": {
                //"url": "/MonitoringNotification/GetDataFilterNew",
                "url": "/MonitoringNotification/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                    data.be_id = $('#BUSINESS_ENTITY').val();
                    data.profit_center = $('#PROFIT_CENTER').val();
                    data.contract_no = $('#CONTRACT_NO').val();
                    data.badan_usaha = $('#BADAN_USAHA').val();
                    data.customer_id = $('#CUSTOMER_ID').val();
                    data.date_start = $('#DATE_START').val();
                    data.date_end = $('#DATE_END').val();
                },
                "dataSrc": function (json) {
                    json.data.be_id = $('#BUSINESS_ENTITY').val();
                    json.data.profit_center = $('#PROFIT_CENTER').val();
                    json.data.contract_no = $('#CONTRACT_NO').val();
                    json.data.badan_usaha = $('#BADAN_USAHA').val();
                    json.data.customer_id = $('#CUSTOMER_ID').val();
                    json.data.date_start = $('#DATE_START').val();
                    json.data.date_end = $('#DATE_END').val();
                    //Make your callback here.
                    return json.data;
                }
            },
            "columns": [
                {
                    "data": "CONTRACT_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CUSTOMER_NAME",
                },
                {
                    "data": "ADDRESS",
                    "width": "1000px"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "DAYS",
                    "class": "dt-body-center"
                },
                {
                    "data": "FIRST",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print first notification" data-print="I" class="btn btn-icon-only blue" id="btn-print-i"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "SECOND",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print second notification" data-print="II" class="btn btn-icon-only blue" id="btn-print-ii"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                },
                {
                    "data": "THIRD",
                    "render": function (data, type, full) {
                        if (data) {
                            var aksi = '<a data-toggle="tooltip" title="Print third notification" data-print="III" class="btn btn-icon-only blue" id="btn-print-iii"><i class="fa fa-print"></i></a>';
                        }
                        else {
                            return '';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },
            "filter": false
        });
    }
    var filter = function () {
        $('#btn-filter').click(function () {
            
            if ($.fn.DataTable.isDataTable('#table-list-notification')) {
                $('#table-list-notification').DataTable().destroy();
            }

            $('#table-list-notification').DataTable({
                "ajax": {
                    "url": "/MonitoringNotification/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.be_id = $('#BUSINESS_ENTITY').val();
                        data.profit_center = $('#PROFIT_CENTER').val();
                        data.contract_no = $('#CONTRACT_NO').val();
                        data.badan_usaha = $('#BADAN_USAHA').val();
                        data.customer_id = $('#CUSTOMER_ID').val();
                        data.date_start = $('#DATE_START').val();
                        data.date_end = $('#DATE_END').val();
                    },
                    "dataSrc": function (json) {
                        json.data.be_id = $('#BUSINESS_ENTITY').val();
                        json.data.profit_center = $('#PROFIT_CENTER').val();
                        json.data.contract_no = $('#CONTRACT_NO').val();
                        json.data.badan_usaha = $('#BADAN_USAHA').val();
                        json.data.customer_id = $('#CUSTOMER_ID').val();
                        json.data.date_start = $('#DATE_START').val();
                        json.data.date_end = $('#DATE_END').val();
                        //Make your callback here.
                        return json.data;
                    }
                },
                "columns": [
                    {
                        "data": "CONTRACT_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CUSTOMER_NAME",
                    },
                    {
                        "data": "ADDRESS",
                        "width": "1000px"
                    },
                    {
                        "data": "CONTRACT_START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONTRACT_END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DAYS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FIRST",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print first notification" data-print="I" class="btn btn-icon-only blue" id="btn-print-i"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SECOND",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print second notification" data-print="II" class="btn btn-icon-only blue" id="btn-print-ii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "THIRD",
                        "render": function (data, type, full) {
                            if (data) {
                                var aksi = '<a data-toggle="tooltip" title="Print third notification" data-print="III" class="btn btn-icon-only blue" id="btn-print-iii"><i class="fa fa-print"></i></a>';
                            }
                            else {
                                return '';
                            }
                            return aksi;
                        },
                        "class": "dt-body-center"
                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },
                "filter": false
            });

        });
    }
    
    var mdm = function () {
        $('#CUSTOMER_NAME').change(function () {
            if ($('#CUSTOMER_NAME').val() == "") {
                $("#CUSTOMER_AR").val("");
                $("#CUSTOMER_ID").val("");
            } 
        })
        $('#CUSTOMER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#CUSTOMER_NAME').val();
                //console.log(inp);
                var l = inp.length;

                $.ajax({
                    type: "POST",
                    url: "/NotificationContract/Customer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.label;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#CUSTOMER_ID").val(ui.item.code);
            }
        });
    }
    
    var changeBe = function () {
        $('#BUSINESS_ENTITY').change(function () {
            var be_id = $('#BUSINESS_ENTITY').val();

            $('#PROFIT_CENTER').html('');
            $('#PROFIT_CENTER').html('<option value="0">-- Choose Profit Center --</option>');
            if (be_id) {
                var param = {
                    be_id: be_id
                };
                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(be_id),
                    method: "POST",
                    url: "/RentalObject/getProfitCenter",
                    timeout: 30000
                });
                req.done(function (data) {
                    App.unblockUI();
                    var temp = '';
                    console.log(data);
                    var jsonList = data;
                    for (var i = 0; i < jsonList.data.length; i++) {
                        temp += '<option value="' + jsonList.data[i].PROFIT_CENTER_ID + '">' + jsonList.data[i].TERMINAL_NAME + '</option>';
                    }
                    $('#PROFIT_CENTER').append(temp);
                });
            }

        });
    }
    
    var btnPrint = function () {
        $('#table-list-notification').on('click', 'tr #btn-print-i', function () {
            var peringatanNo = "I";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });
        $('#table-list-notification').on('click', 'tr #btn-print-ii', function () {
            var peringatanNo = "II";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });
        $('#table-list-notification').on('click', 'tr #btn-print-iii', function () {
            var peringatanNo = "III";
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-notification').DataTable();
            var data = table.row(baris).data();
            // noPeringatan = $(noPeringatan)
            // console.log(peringatanNo, data);
            var kodeCabang = data['BRANCH_ID'];
            var contractNo = data['CONTRACT_NO'];
            var type = "pj";
            console.log("/Pdf/cetakEmail?contract_no=" + contractNo + "&peringatan_no=" + peringatanNo + "&kd_cabang=" + kodeCabang + "&type=" + type);
            window.open("/Pdf/cetakEmail?contract_no=" + btoa(contractNo) + "&peringatan_no=" + btoa(peringatanNo) + "&kd_cabang=" + btoa(kodeCabang) + "&type=" + btoa(type), '_blank');

        });
        
    }
    
    return {
        init: function () {
            //-- JS UNTUK NOTIFICATION CONTRACT
            handleDatePickers();
            mdm();
            CBBusinessEntity();
            filter();
            changeBe();
            btnPrint();
            initTable();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MonitoringNotification.init();
    });
}
