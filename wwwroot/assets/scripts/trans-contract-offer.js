﻿var fileWords;
var DataSurat = function () {
    $('#isi_surat').summernote({
        placeholder: '',
        tabsize: 2,
        height: 320,
        focus: true,                  // set focus to editable area after initializing summernote  
        callbacks: {
            onImageUpload: function (files) {
                for (let i = 0; i < files.length; i++) {
                    uploadImage(files[i]);
                }
            }
        }
    });

    $('#isi_surat').summernote('disable');

    window.openModalViewMaps = function (RO_NUMBER) {
        $('#viewMaps').modal('show');

        var req = $.ajax({
            contentType: "application/json",
            data: "RO_NUMBER=" + RO_NUMBER,
            method: "get",
            url: "/RentalObject/PinPoint2",
            timeout: 30000
        });
        req.done(function (data) {
            App.unblockUI();
            if (data.status === 'S') {
                //MapsView(data.message.LATITUDE, data.message.LONGITUDE)
                var element = document.getElementById("view-maps");
                var mapTypeIds = [];
                for (var type in google.maps.MapTypeId) {
                    mapTypeIds.push(google.maps.MapTypeId[type]);
                }
                mapTypeIds.push("satellite");

                maps = new google.maps.Map(element, {
                    center: new google.maps.LatLng(data.message.LATITUDE, data.message.LONGITUDE),
                    zoom: 16,
                    mapTypeId: "satellite",
                    // disable the default User Interface
                    disableDefaultUI: true,
                    // add back fullscreen, streetview, zoom
                    zoomControl: true,
                    streetViewControl: true,
                    //fullscreenControl: true,
                    center: new google.maps.LatLng(data.message.LATITUDE, data.message.LONGITUDE),
                    mapTypeControlOptions: {
                        mapTypeIds: mapTypeIds
                    }
                });

                maps.mapTypes.set("satellite", new google.maps.ImageMapType({
                    getTileUrl: function (coord, zoom) {
                        // See above example if you need smooth wrapping at 180th meridian
                        return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                    },
                    tileSize: new google.maps.Size(256, 256),
                    name: "OpenStreetMap",
                    maxZoom: 18
                }));

                var req = $.ajax({
                    contentType: "application/json",
                    data: "RO_NUMBER=" + RO_NUMBER,
                    method: "get",
                    url: "/RentalObject/GetMarker2",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {

                        var marks = data.message;
                        var flightPlanCoordinates = [];
                        for (var i = 0; i < marks.length; i++) {
                            flightPlanCoordinates.push({ lat: parseFloat(marks[i].LATITUDE), lng: parseFloat(marks[i].LONGITUDE) });
                        }
                        flightPlanCoordinates.push({ lat: parseFloat(marks[0].LATITUDE), lng: parseFloat(marks[0].LONGITUDE) });
                        var flightPath = new google.maps.Polyline({
                            path: flightPlanCoordinates,
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        flightPath.setMap(maps);
                    }
                });
            } else {
                swal('Failed', data.message, 'error');
            }
        });
    }

    return {
        init: function () {
        }
    };
}();
var TransContractOffer = function () {

    var showOldContract = function (ro, cond) {
        ////console.log(ro, cond)
        var all_value = 0;
        $('.DETAIL_RO').html('');
        for (var i = 0; i < cond.length; i++) {

            var net_val = cond[i].TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var price = cond[i].UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var calc_ob = cond[i].CALC_OBJECT;

            if (calc_ob != 'Header Document') {
                all_value = all_value + parseFloat(net_val.split(',').join(''));
                var ro_cond = calc_ob.split(' ')[1];

                for (var j = 0; j < ro.length; j++) {
                    ro_data = ro[j];
                    ////console.log(ro_data);
                    var ro_id = ro_data.OBJECT_ID;
                    if (ro_id == ro_cond) {
                        var luas = Number(ro_data.LAND_DIMENSION) + Number(ro_data.BUILDING_DIMENSION);
                        var text = $('.DETAIL_RO').html() + '<tr><td>' + ro_data.OBJECT_ID + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CURRENCY').val() + '</td></tr>';
                        $('.DETAIL_RO').html(text);
                    }
                }
            }
        }
        $('#CONTRACT_VALUE').val(all_value);
        $('.CONTRACT_VALUE').html(sep1000(all_value, true));
    }

    var initTableTransContractOfferCreated = function () {

        $('#table-trans-created').DataTable({

            "ajax":
            {
                "url": "/TransContractOffer/GetDataTransContractOfferCreated",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {
                        if (full.ACTIVE == '1') {
                            return '<span class="label label-sm label-success"> ACTIVE </span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> INACTIVE </span>';
                        }

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.OFFER_STATUS == 'Created') {
                            return '<span class="label label-sm label-primary">Created</span>';
                        }
                        else {
                            return '<span class="label label-sm label-danger"> ' + full.OFFER_STATUS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                }, {
                    "render": function (data, type, full) {
                        //console.log(full.ACTIVE);
                        if (full.ACTIVE == '1' && full.OFFER_STATUS == 'Created') {

                            var KODE_CABANG = $('#KODE_CABANG').val();
                            if (KODE_CABANG == 0) {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                                if (full.PARAF_LEVEL == -1) {
                                    aksi += '<a id="btn-add-surat" class="btn btn-icon-only purple" title="Create Surat" ><i class="fa fa-envelope-o"></i></a>';
                                    aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                }
                                if (full.PARAF_LEVEL == 0) {
                                    aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-edit-surat"><i class="fa fa-edit"></i></a>';
                                }
                                //aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release"><i class="fa fa-external-link"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                                //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Generate Form" id="btn-export-ijinprinsip"><i class="fa fa-file-word-o"></i></a>';
                                if (full.PARAF_LEVEL == -1) {
                                    aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive" id="btn-inactive"><i class="fa fa-times"></i></a>';
                                }
                            } else {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';
                                if (full.PARAF_LEVEL == -1) {
                                    aksi += '<a id="btn-add-surat" class="btn btn-icon-only purple" title="Create Surat" ><i class="fa fa-envelope-o"></i></a>';
                                    aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                                    aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive" id="btn-inactive"><i class="fa fa-times"></i></a>';
                                }
                                if (full.PARAF_LEVEL == 0) {
                                    aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-edit-surat"><i class="fa fa-edit"></i></a>';
                                }
                                //aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release"><i class="fa fa-external-link"></i></a>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                                //ilang
                                //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Form Ijin Prinsip GM ke Pengguna Jas" id="btn-export-ijinprinsipD"><i class="fa fa-file-word-o"></i></a>';
                                //aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Form Rekomendasi dari GM ke Direksi/SM" id="btn-export-ijinprinsipP"><i class="fa fa-file-word-o"></i></a>';
                            }
                        }
                        else {
                            if (full.ACTIVE == '0' || full.COMPLETED_STATUS == '4') {
                                var aksi = '<a data-toggle="modal" href="#viewModalAttachment" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment"><i class="fa fa-paperclip"></i></a>';

                                if (full.PARAF_LEVEL == -1 && full.COMPLETED_STATUS != '4') {
                                    aksi += '<a id="btn-add-surat" class="btn btn-icon-only purple" title="Create Surat" ><i class="fa fa-envelope-o"></i></a>';
                                    //aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-ubah"><i class="fa fa-edit"></i></a>';

                                }
                                if (full.PARAF_LEVEL == 0) {
                                    //aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract" id="btn-edit-surat"><i class="fa fa-edit"></i></a>';
                                }
                                //aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="release to Workflow" id="btn-status" disabled><i class="fa fa-external-link"></i></button>';
                                aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-created"><i class="fa fa-eye"></i></a>';
                            }
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferWorkflow = function () {
        $('#table-trans-workflow').DataTable({

            "ajax":
            {
                "url": "/TransContractOffer/GetDataTransContractOfferWorkflow",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.TOP_APPROVED_LEVEL + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentWorkflowList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-workflow-list"><i class="fa fa-paperclip"></i></a>';
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-workflow"><i class="fa fa-eye"></i></a>';
                        //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Form Ijin Prinsip GM ke Pengguna Jas" id="btn-export-ijinprinsipD"><i class="fa fa-file-word-o"></i></a>';
                        //aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Form Rekomendasi dari GM ke Direksi/SM" id="btn-export-ijinprinsipP"><i class="fa fa-file-word-o"></i></a>';

                        return aksi;

                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferRevised = function () {

        var ROLE_USER = $('#ROLE_USER').val();

        $('#table-trans-reserved-list').DataTable({

            "ajax":
            {
                "url": "/TransContractOffer/GetDataTransContractOfferRevised",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        return '<span class="label label-sm label-success"> ' + full.TOP_APPROVED_LEVEL_DESC + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {

                        //if (full.TOP_APPROVED_LEVEL <= 50 && (full.OFFER_STATUS == 'Revised by Mgr' || full.OFFER_STATUS == 'Revised by Dept GM' || full.OFFER_STATUS == 'Revised by GM')) {

                        //    if (ROLE_USER == '4' || ROLE_USER == '6' || ROLE_USER == '8' || ROLE_USER == '10') {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                        //        aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></a>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //    else {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised" disabled><i class="fa fa-edit"></i></button>';
                        //        aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised" disabled><i class="fa fa-external-link"></i></button>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //}
                        //else if (full.TOP_APPROVED_LEVEL > 50 && (full.OFFER_STATUS == 'Revised by SM' || full.OFFER_STATUS == 'Revised by Director' || full.OFFER_STATUS == 'Revised by Komisaris' || full.OFFER_STATUS == 'Revised by RUPS')) {
                        //    if (ROLE_USER == '4' || ROLE_USER == '7') {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                        //        aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised" disabled><i class="fa fa-external-link"></i></button>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //    else if (ROLE_USER == '11') {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised" disabled><i class="fa fa-edit"></i></button>';
                        //        aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-approve"><i class="fa fa-external-link"></i></a>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract  Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //    else {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></button>';
                        //        aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></button>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //}
                        //else if (full.TOP_APPROVED_LEVEL > 50 && (full.OFFER_STATUS == 'Revised by Mgr' || full.OFFER_STATUS == 'Revised by Dept GM' || full.OFFER_STATUS == 'Revised by GM')) {
                        //    if (ROLE_USER == '4' || ROLE_USER == '6' || ROLE_USER == '8' || ROLE_USER == '10') {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<a class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></a>';
                        //        aksi += '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></a>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //    else {
                        //        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        //        aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></button>';
                        //        aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></button>';
                        //        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        //    }
                        //}
                        var aksi = '<a data-toggle="modal" href="#viewModalAttachmentRevisedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-reverse-list"><i class="fa fa-paperclip"></i></a>';
                        if (full.DISPOSISI_PARAF != 'disposisi') {
                            aksi += '<button class="btn btn-icon-only blue" data-toggle="tooltip" title="Edit Contract Offer" id="btn-ubah-revised"><i class="fa fa-edit"></i></button>';
                        }
                        //aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Release to Workflow" id="btn-release-revised"><i class="fa fa-external-link"></i></button>';
                        aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-revised"><i class="fa fa-eye"></i></a>';
                        return aksi;

                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initTableTransContractOfferApproved = function () {
        var xRole = $('#ROLE_USER').val();
        var x = document.getElementById("btn-inactive1");
        var btn_inactive = "";
        if (xRole == "12") {
            btn_inactive = '<a class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive" id="btn-inactive1"><i class="fa fa-times"></i></a>'
        } else {
            btn_inactive = '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive" id="btn-inactive1" disabled><i class="fa fa-times"></i></button>'
        }
        $('#table-trans-approved-list').DataTable({

            "ajax":
            {
                "url": "/TransContractOffer/GetDataTransContractOfferApproved",
                "type": "GET",
            },

            "columns": [

                {
                    "data": "CONTRACT_OFFER_NO",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_OFFER_TYPE"
                },
                {
                    "data": "CONTRACT_OFFER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"

                },
                //{
                //    "data": "KODE_BAYAR",
                //    "class": "dt-body-center"

                //},
                //{
                //    "class": "dt-body-center",
                //    "render": function (data, type, full) {
                //        if (full.TGL_LUNAS == null && full.TGL_BATAL == null) {
                //            return '<span class="label label-sm label-warning">Belum Dibayar </span>';
                //        } else if (full.TGL_BATAL != null) {
                //            return '<span class="label label-sm label-info">Dibatalkan </span>';
                //        } else if (full.TGL_LUNAS != null) {
                //            return '<span class="label label-sm label-success">Sudah Dibayar </span>';
                //        } else {
                //            return '<span class="label label-sm label-success"> - </span>';
                //        }
                //    }

                //},
                {
                    "render": function (data, type, full) {
                        if (full.OFFER_ACTIVE == "0") {
                            return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>' + '<br><br><span class="badge badge-danger"> INACTIVE </span>';
                        } else {
                            return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';
                        }
                        //return '<span class="label label-sm label-success"> ' + full.OFFER_STATUS + ' </span>';

                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = null;
                        if (full.OFFER_ACTIVE == "0") {
                            aksi = '<a data-toggle="modal" href="#viewModalAttachmentApprovedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-approved-list"><i class="fa fa-paperclip"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-approved"><i class="fa fa-eye"></i></a>';
                            //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Form Ijin Prinsip GM ke Pengguna Jasa" id="btn-export-ijinprinsipD"><i class="fa fa-file-word-o"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Form Rekomendasi dari GM ke Direksi/SM" id="btn-export-ijinprinsipP"><i class="fa fa-file-word-o"></i></a>';

                        } else if (full.OFFER_ACTIVE != "0" && full.CONTRACT_NUMBER == null) {
                            aksi = '<a data-toggle="modal" href="#viewModalAttachmentApprovedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-approved-list"><i class="fa fa-paperclip"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-approved"><i class="fa fa-eye"></i></a>';
                            //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Form Ijin Prinsip GM ke Pengguna Jasa" id="btn-export-ijinprinsipD"><i class="fa fa-file-word-o"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Form Rekomendasi dari GM ke Direksi/SM" id="btn-export-ijinprinsipP"><i class="fa fa-file-word-o"></i></a>';

                            aksi += btn_inactive;
                        } else {
                            aksi = '<a data-toggle="modal" href="#viewModalAttachmentApprovedList" class="btn btn-icon-only purple" data-toggle="tooltip" title="Attachment" id="btn-attachment-approved-list"><i class="fa fa-paperclip"></i></a>';
                            aksi += '<a data-toggle="modal" href="#viewContractOfferModal" data-toggle="tooltip" title="Contract Offer Details" class="btn btn-icon-only green" id="btn-detail-approved"><i class="fa fa-eye"></i></a>';
                            aksi += '<button class="btn btn-icon-only red" data-toggle="tooltip" title="Inactive" id="btn-inactive1" disabled><i class="fa fa-times"></i></button>';
                            //aksi += '<a class="btn btn-icon-only grey" data-toggle="tooltip" title="Form Ijin Prinsip GM ke Pengguna Jas" id="btn-export-ijinprinsipD"><i class="fa fa-file-word-o"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" data-toggle="tooltip" title="Form Rekomendasi dari GM ke Direksi/SM" id="btn-export-ijinprinsipP"><i class="fa fa-file-word-o"></i></a>';

                        }
                        //CONTRACT_NUMBER
                        return aksi;

                    },
                    "class": "dt-body-center"
                }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });
    }

    var initDetilTransContractOfferCreated = function () {
        $('body').on('click', 'tr #btn-detail-created', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSurat = $('#table-detail-surat').DataTable();
            tableSurat.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#SERTIFIKAT").val(data["SERTIFIKAT"]);
            $("#CURRENCY_RATE").val(data["CURRENCY_RATE"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];

            $('#dataParaf').empty();
            $('#dataHistory').empty();
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            var nextParaf = '';
            req.done(function (data) {
                App.unblockUI();
                data.history.forEach(myFunction);
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }
                data.nextParaf.forEach(nextDataParaf);
                function nextDataParaf(item, index) {
                    nextParaf += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'></td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:70px;width:390px'><div style='height: 70px;' class='form-group'>" +
                        "<label><u>" + item.STATUS + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> "+ item.USER_NAME + " " + item.ROLE_NAME + "</b></td></tr>";
                }
                $('#dataHistory').append(isiTable);
                $('#dataParaf').append(nextParaf);
            });
            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            //var template = tpl.html().replace(/__INDEX__/g, data.dataSurat.ID);
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/'+data+'" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
            ////console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a data-toggle="modal" onClick="openModalViewMaps(' + full.OBJECT_ID + ')"  data-toggle="tooltip" title="MAP RO" id="btn-attachment">' + full.OBJECT_NAME + '</a>';
                            return aksi;
                        },
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_tanah = full.LUAS_TANAH;
                            var fLuas_tanah = luas_tanah.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_tanah;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var luas_bangunan = full.LUAS_BANGUNAN;
                            var fLuas_bangunan = luas_bangunan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fLuas_bangunan;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });


            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            //$('#sales-based').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "SR",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "PAYMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NAME_OF_TERM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CALC_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CALC_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "PERCENTAGE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TARIF",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        },
            //        {
            //            "data": "RR",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MINSALES",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        },
            //        {
            //            "data": "MINPRODUCTION",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        }

            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});

            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            //console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            ////console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrCond = data.data;
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        arrRo = data.data;
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    var initDetilTransContractOfferWorkflow = function () {

        $('body').on('click', 'tr #btn-detail-workflow', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSurat = $('#table-detail-surat').DataTable();
            tableSurat.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            $('#dataHistory').empty();
            $('#dataParaf').empty();
            var isiTable = '';
            var nextParaf = '';
            req.done(function (data) {
                App.unblockUI();
                data.history.forEach(myFunction);
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }

                data.nextParaf.forEach(nextDataParaf);
                function nextDataParaf(item, index) {
                    nextParaf += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'></td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:70px;width:390px'><div style='height: 70px;' class='form-group'>" +
                        "<label><u>" + item.STATUS + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> "+ item.USER_NAME + " " + item.ROLE_NAME + "</b></td></tr>";
                }

                $('#dataHistory').append(isiTable);
                $('#dataParaf').append(nextParaf);
            });

            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/' + data + '" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a data-toggle="modal" onClick="openModalViewMaps(' + full.OBJECT_ID + ')"  data-toggle="tooltip" title="MAP RO" id="btn-attachment">' + full.OBJECT_NAME + '</a>';
                            return aksi;
                        },
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*"data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }

                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
            console.log(9, id_special);
            console.log($('#table-detil-manual'));
            tableDetilManual = $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            console.log(10);
            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });


            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center date-picker"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            //$('#sales-based').DataTable({
            //    "ajax":
            //    {
            //        "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
            //        "type": "GET",

            //    },

            //    "columns": [
            //        {
            //            "data": "SR",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "SALES_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "PAYMENT_TYPE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "NAME_OF_TERM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CALC_FROM",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "CALC_TO",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "UNIT",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "PERCENTAGE",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "TARIF",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        },
            //        {
            //            "data": "RR",
            //            "class": "dt-body-center"
            //        },
            //        {
            //            "data": "MINSALES",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        },
            //        {
            //            "data": "MINPRODUCTION",
            //            "class": "dt-body-center",
            //            render: $.fn.dataTable.render.number('.', '.', 0)
            //        }

            //    ],
            //    "destroy": true,
            //    "ordering": false,
            //    "processing": true,
            //    "serverSide": true,

            //    "lengthMenu": [
            //        [5, 10, 15, 20, -1],
            //        [5, 10, 15, 20, "All"]
            //    ],

            //    "pageLength": 10,

            //    "language": {
            //        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            //    },

            //    "filter": false
            //});


            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            //console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            //console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrCond = data.data;
                        ////console.log(arrCond);
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrRo = data.data;
                        ////console.log(arrRo);
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    var initDetilTransContractOfferRevised = function () {
        $('body').on('click', 'tr #btn-detail-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-deatil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSurat = $('#table-detail-surat').DataTable();
            tableSurat.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];
            $('#dataHistory').empty();
            $('#dataParaf').empty();
            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            var isiTable = '';
            var nextParaf = '';
            req.done(function (data) {
                App.unblockUI();
                data.history.forEach(myFunction);
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }

                data.nextParaf.forEach(nextDataParaf);
                function nextDataParaf(item, index) {
                    nextParaf += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'></td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:70px;width:390px'><div style='height: 70px;' class='form-group'>" +
                        "<label><u>" + item.STATUS + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.USER_NAME + " " + item.ROLE_NAME + "</b></td></tr>";
                }

                $('#dataHistory').append(isiTable);
                $('#dataParaf').append(nextParaf);
            });

            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/' + data + '" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });


            ////console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a data-toggle="modal" onClick="openModalViewMaps(' + full.OBJECT_ID + ')"  data-toggle="tooltip" title="MAP RO" id="btn-attachment">' + full.OBJECT_NAME + '</a>';
                            return aksi;
                        },
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });



            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*
                        "data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });


            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            //console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            //console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrCond = data.data;
                        ////console.log(arrCond);
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrRo = data.data;
                        ////console.log(arrRo);
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    var initDetilTransContractOfferApproved = function () {
        $('body').on('click', 'tr #btn-detail-approved', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approved-list').DataTable();
            var data = table.row(baris).data();
            //console.log(data);
            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var tableDetilManual = $('#table-detil-manual').DataTable();
            tableDetilManual.destroy();

            var tableDetilMemo = $('#table-detil-memo').DataTable();
            tableDetilMemo.destroy();

            var tableHistory = $('#table-history').DataTable();
            tableHistory.destroy();

            var tableSurat = $('#table-detail-surat').DataTable();
            tableSurat.destroy();

            //var tableSalesBased = $('#sales-based').DataTable();
            //tableSalesBased.destroy();

            //var tableReportingRule = $('#reporting-rule').DataTable();
            //tableReportingRule.destroy();

            $("#RENTAL_REQUEST_NO").val(data["RENTAL_REQUEST_NO"]);
            $("#CONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#CONTRACT_OFFER_TYPE").val(data["CONTRACT_OFFER_TYPE"]);
            $("#BE_ID").val(data["BE_ID"]);
            $("#CONTRACT_OFFER_NAME").val(data["CONTRACT_OFFER_NAME"]);
            $("#CONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#CONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#TERM_IN_MONTHS").val(data["TERM_IN_MONTHS"]);
            $("#CUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#CUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#PROFIT_CENTER").val(data["PROFIT_CENTER_NAME"]);
            $("#CONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#CURRENCY").val(data["CURRENCY"]);
            $("#OFFER_STATUS").val(data["OFFER_STATUS"]);

            var id_special = data["CONTRACT_OFFER_NO"];

            var req = $.ajax({
                contentType: "application/json",
                data: "id=" + id_special,
                method: "get",
                url: "/PendingList/GetSurat",
                timeout: 30000,
                async: false,
            });
            $('#dataHistory').empty();
            $('#dataParaf').empty();
            var isiTable = '';
            var nextParaf = '';
            req.done(function (data) {
                App.unblockUI();
                data.history.forEach(myFunction);
                function myFunction(item, index) {
                    isiTable += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'> " + item.TIME_CREATE + "</td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:160px;width:390px'><div style='background-color: #00BCED;height: 160px;' class='form-group'>" +
                        "<label><u>" + item.TYPE + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.NAMA + "</b></td></tr>";
                    if (item.TYPE == "Disposisi") {
                        isiTable += "<tr><td><b>Kepada</b> : " + item.NAMA_KEPADA + "</td></tr>" +
                            "<tr><td><b>Instruksi</b> : " + item.INSTRUKSI + "</td></tr>";
                    }
                    isiTable += "<tr><td><textarea disabled style='width:350px;'>" + item.MEMO + "</textarea></td></tr>" +
                        "</table></div></div></div>";

                }

                data.nextParaf.forEach(nextDataParaf);
                function nextDataParaf(item, index) {
                    nextParaf += "<table align='right' style='margin-right: 20px; font-size: 10px;'><tr>" +
                        "<td align='right'></td></tr ></table>" +
                        "<div style='padding: 5px;background-color: #E1F9FF;' class='panel-body'>" +
                        "<div class='col-md-12' style='height:70px;width:390px'><div style='height: 70px;' class='form-group'>" +
                        "<label><u>" + item.STATUS + "</u></label>" +
                        "<table align='center' style='background-color: #FFFFFF;width: 350px;' class='form-group'>" +
                        "<tr><td><b> " + item.USER_NAME + " " + item.ROLE_NAME + "</b></td></tr>";
                }
                $('#dataParaf').append(nextParaf);
                $('#dataHistory').append(isiTable);
            });

            ////console.log(id_special);
            $('#table-detil-object').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailObject/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a data-toggle="modal" onClick="openModalViewMaps(' + full.OBJECT_ID + ')"  data-toggle="tooltip" title="MAP RO" id="btn-attachment">' + full.OBJECT_NAME + '</a>';
                            return aksi;
                        },
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_BANGUNAN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detail-surat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSurat/" + id_special,
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "NO_SURAT"
                    },
                    {
                        "data": "ID",
                        //"datas":"DIRECTORY",
                        "class": "dt-body-center",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="pdf/disposisilistpdf/' + data + '" target="_blank">' + row.SUBJECT + '</a>';
                            }

                            return data;
                        }
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });


            $('#table-detil-condition').DataTable({
                "ajax": {
                    "url": "/TransContractOffer/GetDataDetailCondition/" + id_special,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },
                "initComplete": function () {
                    if (data["OLD_CONTRACT"]) {
                        setTimeout(coHitungValue(), 1000);
                    }
                },
                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MONTHS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATISTIC",
                        "class": "dt-body-center"
                    },
                    {
                        "render": function (data, type, full) {
                            var unit_price = full.UNIT_PRICE;
                            var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            return fUnitPrice;

                        },
                        "class": "dt-body-center"
                        /*"data": "UNIT_PRICE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                        */
                    },
                    {
                        "data": "AMT_REF",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FREQUENCY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "FORMULA",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEASUREMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "NJOP_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KONDISI_TEKNIS_PERCENT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TOTAL_NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"
                    }
                ],

                "ordering": false,
                "processing": false,
                "serverSide": true,


                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });

            $('#table-detil-manual').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailManualy/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "MANUAL_NO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "KODE_BAYAR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "DUE_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NET_VALUE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-detil-memo').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailMemo/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "CALC_OBJECT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CONDITION_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MEMO",
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            $('#table-history').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetDataHistoryWorkflow/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "USER_LOGIN",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CREATION_DATE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NOTES",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "STATUS_CONTRACT_DESC",
                        "class": "dt-body-center"
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            //Detail Data Table Sales Rule
            $('#sales-based').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataDetailSalesBased/" + id_special,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "SR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "SALES_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PAYMENT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "NAME_OF_TERM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_FROM",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "CALC_TO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PERCENTAGE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "TARIF",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "RR",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "MINSALES",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "MINPRODUCTION",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

            // init old contract
            var id_co = data["OLD_CONTRACT_OFFER"];
            var id_rr = data["OLD_RENTAL_REQUEST"];
            //console.log(id_co, id_rr)
            $("#OLD_CONTRACT").val(data["OLD_CONTRACT"]);
            console.log(id_co, id_rr)
            $('.old-contract').css({ 'display': 'none' });
            if (id_co) {
                //console.log(data["OLD_CONTRACT"]);
                $('.old-contract').css({ 'display': 'block' });
                $(".CONTRACT_START_NEW").html(data["CONTRACT_START_DATE"]);
                $(".CONTRACT_END_NEW").html(data["CONTRACT_END_DATE"]);
                $(".CONTRACT_PERIOD_NEW").html(data["TERM_IN_MONTHS"]);
                $(".CURRENCY").html('&nbsp;' + data["CURRENCY"]);
                var arrRo;
                var arrCond;
                var req_Offer = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetContractOffer/" + id_co,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Status == "S") {
                            data = data.Message;
                            ////console.log(data);

                            $(".CONTRACT_START").html(data["CONTRACT_START_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_END").html(data["CONTRACT_END_DATE"].replace(/\//g, "."));
                            $(".CONTRACT_PERIOD").html(data["TERM_IN_MONTHS"]);
                        } else {
                            //console.log(data.Message);
                        }
                    }
                });
                var req_Cond = $.ajax({
                    type: "GET",
                    "url": "/TransContractOffer/GetDataDetailConditionEdit/" + id_co + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrCond = data.data;
                        ////console.log(arrCond);
                    }
                });
                var req_Ro = $.ajax({
                    type: "GET",
                    "url": "/TransRentalRequest/GetDataDetailRentalEdit/" + id_rr + "?draw=1&start=0&length=100&search%5Bvalue%5D=",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        ////console.log(data.draw);
                        ////console.log(data.recordsFiltered);
                        ////console.log(data.recordsTotal);
                        ////console.log(data.data);
                        arrRo = data.data;
                        ////console.log(arrRo);
                    }
                });
                req_Ro.done(function () {
                    req_Offer.done();
                    req_Cond.done(function () {
                        showOldContract(arrRo, arrCond);
                    });
                });
            }
        });
    }

    var releaseWorkflow = function () {
        $('body').on('click', 'tr #btn-release', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var tableWorkflow = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            ////console.log(data.CONTRACT_OFFER_NO);

            swal({
                title: 'Warning',
                text: "Are you sure want <B>RELEASE</B> to <B>WORKFLOW</B> ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];

                    var param = {

                        data: CONTRACT_OFFER_NO

                    };
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        //contentType: "application/json",
                        data: param,
                        method: "POST",
                        //url: "/TransContractOffer/ReleaseToWorkflow",
                        url: "/TransContractOffer/ReleaseToWorkflowNew",
                        timeout: 30000,

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            tableWorkflow.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                });
                            }
                            else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                });
                            }
                        }
                    });
                }
            });

        });
    }

    var releaseWorkflowRevised = function () {
        $('body').on('click', 'tr #btn-release-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var tableWorkflow = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want <B>RELEASE</B> to <B>WORKFLOW</B> ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];

                    var param = {

                        data: CONTRACT_OFFER_NO

                    };
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        //url: "/TransContractOffer/ReleaseToWorkflow",
                        url: "/TransContractOffer/ReleaseToWorkflowNew",
                        timeout: 30000,

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            tableWorkflow.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                });
                            }
                            else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                });
                            }
                        }
                    });
                }
            });

        });
    }

    var approve = function () {
        $('body').on('click', 'tr #btn-approve', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var tableWorkflow = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            swal({
                title: 'Warning',
                text: "Are you sure want <B>RELEASE</B> to <B>WORKFLOW</B> ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {

                    var contractOfferNo = data["CONTRACT_OFFER_NO"];
                    var approvalLevel = $('#property_role').val();
                    var approvalType = 2;

                    var param = {
                        CONTRACT_OFFER_NO: contractOfferNo,
                        LEVEL_NO: approvalLevel,
                        APPROVAL_TYPE: approvalType
                    }

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransContractOffer/SetApproveWorkflow",
                        timeout: 30000,

                        success: function (data) {
                            App.unblockUI();
                            table.ajax.reload(null, false);
                            tableWorkflow.ajax.reload(null, false);
                            if (data.status === 'S') {
                                swal('Success', 'Contract Offer : ' + contractOfferNo + ' Has Been Released to Workflow', 'success').then(function (isConfirm) {
                                });
                            }
                            else {
                                swal('Failed', 'Sorry, You can not continue this.', 'error').then(function (isConfirm) {
                                });
                            }
                        }
                    });
                }
            });

        });
    }

    var edit = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

            window.location = "/TransContractOffer/EditOffer/" + data["CONTRACT_OFFER_NO"];
        });

        $('body').on('click', 'tr #btn-add-surat', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

            window.location = "/TransContractOffer/AddSurat/" + data["CONTRACT_OFFER_NO"];
        });

        $('body').on('click', 'tr #btn-edit-surat', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');

            window.location = "/TransContractOffer/EditOfferSurat/" + data["CONTRACT_OFFER_NO"];
        });
    }

    var editRevised = function () {
        $('body').on('click', 'tr #btn-ubah-revised', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            //swal('Warning', 'Maaf saat ini untuk menu edit masih belum tersedia!', 'warning');
            //console.log(data["CONTRACT_OFFER_NO"])
            window.location = "/TransContractOffer/EditOfferSurat/" + data["CONTRACT_OFFER_NO"];
        });
    }


    var editStatus = function () {
        $('body').on('click', 'tr #btn-inactive', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();
            idUser = data["CONTRACT_OFFER_NO"];
            xStatus = data["ACTIVE"];
            //console.log(xStatus);
            //console.log("----------------");
            if (xStatus == 1) {
                xStatus = "0";
                //console.log("IF");
                //console.log(xStatus);
            } else {
                //console.log("ELSE");
                xStatus = "1";
                //console.log(xStatus);
            }
            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'

            }).then(function (isConfirm) {
                if (isConfirm) {

                    var param = {
                        xID: parseInt(idUser),
                        xSTATUS: parseInt(xStatus)
                    };

                    //console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransContractOffer/EditStatus1",
                        timeout: 30000
                    });


                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransContractOffer/Add';
        });
    }

    var initAttachment = function () {
        $('body').on('click', 'tr #btn-attachment', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data["CONTRACT_OFFER_NO"];
            $('#kode_offer').html(id_attachment);


            $("#ID_ATTACHMENT").val(data["CONTRACT_OFFER_NO"]);

            var tableDetailAttachment = $('#table-attachment').DataTable();
            tableDetailAttachment.destroy();

            $('#table-attachment').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';

                            x += '<a class="btn default btn-xs red" id="delete-attachment"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentWorkflowList = function () {
        $('body').on('click', 'tr #btn-attachment-workflow-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentWorkflowList = $('#table-trans-workflow').DataTable();
            tableAttachmentWorkflowList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-workflow').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_workflow_list').html(id_attachment);

            $("#ID_ATTACHMENT_WORKFLOWLIST").val(id_attachment);

            $('#table-attachment-workflow-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            x += '<a class="btn default btn-xs red" id="delete-attachment-workflow-list"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentRevisedList = function () {
        $('body').on('click', 'tr #btn-attachment-reverse-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentRevisedList = $('#table-trans-reserved-list').DataTable();
            tableAttachmentRevisedList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-reserved-list').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_revised_list').html(id_attachment);

            $("#ID_ATTACHMENT_REVIESEDLIST").val(id_attachment);

            $('#table-attachment-revised-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            x += '<a class="btn default btn-xs red" id="delete-attachment-revised-list"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var initAttachmentApprovedList = function () {
        $('body').on('click', 'tr #btn-attachment-approved-list', function () {
            var progress = parseInt(0);
            $('.progress .progress-bar').css('width', progress + '%');

            $('.file_name').html('');
            $('.file_type').html('');
            $('.file_size').html('');

            var tableAttachmentApprovedList = $('#table-trans-approved-list').DataTable();
            tableAttachmentApprovedList.destroy();

            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approved-list').DataTable();
            var data = table.row(baris).data();

            var id_attachment = data[0];
            $('#kode_offer_approved_list').html(id_attachment);

            $("#ID_ATTACHMENT_APPROVEDLIST").val(id_attachment);

            $('#table-attachment-approved-list').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataAttachment/" + id_attachment,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                },

                "columns": [
                    {
                        "data": "FILE_NAME"
                    },
                    {
                        "render": function (data, type, full) {
                            //var x = '<a href ="' + 'file\:///' + full.DIRECTORY + '\\' + full.FILE_NAME + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            var x = '<a href ="' + full.DIRECTORY + '" target="_blank" class="btn default btn-xs blue"><i class="fa fa-cloud-download"></i></a>';
                            x += '<a class="btn default btn-xs red" id="delete-attachment-approved-list"><i class="fa fa-trash"></i></a>';
                            return x;
                        },
                        "class": "dt-body-center"
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });
    }

    var fupload = function () {
        $('#fileupload').click(function () {
            $("#fileupload").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT').val();

            $('#fileupload').fileupload({
                beforeSend: function (e, data) {
                    //console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    ////console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                formData: { BE_ID: BE_ID },
                autoUpload: true,
                done: function (e, data) {
                    //console.log(data)
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                ////console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadWorkflowList = function () {
        $('#fileuploadworkflowlist').click(function () {
            $("#fileuploadworkflowlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_WORKFLOWLIST').val();

            $('#fileuploadworkflowlist').fileupload({
                beforeSend: function (e, data) {
                    //console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    ////console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-workflow-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                ////console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadRevisedList = function () {
        $('#fileuploadrevisedlist').click(function () {
            $("#fileuploadrevisedlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_REVIESEDLIST').val();

            $('#fileuploadrevisedlist').fileupload({
                beforeSend: function (e, data) {
                    //console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    ////console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-revised-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                ////console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var fuploadApprovedList = function () {
        $('#fileuploadapprovedlist').click(function () {
            $("#fileuploadapprovedlist").css('display', 'block'); //enable upload

            var BE_ID = $('#ID_ATTACHMENT_APPROVEDLIST').val();
            //console.log(BE_ID);

            $('#fileuploadapprovedlist').fileupload({
                beforeSend: function (e, data) {
                    //console.log(data.loaded);
                    var progress = parseInt(0);
                    $('.progress .progress-bar').css('width', progress + '%');
                    ////console.log('data loaded upload ' + data.loaded);
                    setTimeout(progress, 10000);
                },
                dataType: 'json',
                type: 'POST',
                url: '/TransContractOffer/UploadFiles',
                formData: {
                    BE_ID: BE_ID
                },
                autoUpload: true,
                done: function (e, data) {
                    $('.file_name').html(data.result.name);
                    $('.file_type').html(data.result.type);
                    $('.file_size').html(data.result.size);
                    $('#table-attachment-approved-list').dataTable().api().ajax.reload();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                ////console.log('data loaded upload ' + data.loaded);
                $('.progress .progress-bar').css('width', progress + '%');
            });

        });
    }

    var deleteAttachment = function () {

        $('body').on('click', 'tr #delete-attachment', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment').DataTable();
            var data = table.row(baris).data();

            var uriId = $('#ID_ATTACHMENT').val();
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachment').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachment').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentWorkflowList = function () {
        
        $('body').on('click', 'tr #delete-attachment-workflow-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-workflow-list').DataTable();
            var data = table.row(baris).data();

			var uriId = $('#ID_ATTACHMENT_WORKFLOWLIST').val();												   
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentWorkflowList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentWorkflowList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentWorkflowList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentRevisedList = function () {
        
        $('body').on('click', 'tr #delete-attachment-revised-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-revised-list').DataTable();
            var data = table.row(baris).data();

			var uriId = $('#ID_ATTACHMENT_REVIESEDLIST').val();												   
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentRevisedList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentRevisedList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentRevisedList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var deleteAttachmentApprovedList = function () {
        
        $('body').on('click', 'tr #delete-attachment-approved-list', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-attachment-approved-list').DataTable();
            var data = table.row(baris).data();

			var uriId = $('#ID_ATTACHMENT_APPROVEDLIST').val();												   
            var fileName = data['FILE_NAME'];
            $('#viewModalAttachmentApprovedList').modal('hide');

            swal({
                title: 'Warning',
                text: "Are you sure want to delete this attachment ['" + fileName + "']?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: {
                            id: data["ID_ATTACHMENT"],
                            directory: data["DIRECTORY"],
                            filename: data["FILE_NAME"],
                            uri: uriId
                        },
                        method: "get",
                        url: "/TransContractOffer/DeleteDataAttachment",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            //swal('Success', data.message, 'success');
                            swal({
                                title: 'Info',
                                text: "File Deleted Successfully",
                                type: 'info',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentApprovedList').modal('show');
                            });

                        } else {
                            //swal('Failed', data.message, 'error');
                            swal({
                                title: 'Warning',
                                text: "Failed to Delete Data",
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No'
                            }).then(function (isConfirm) {
                                $('#viewModalAttachmentApprovedList').modal('show');
                            });
                        }

                    });
                }
            });
        });
    }

    var initExportdataijinprinsip = function () {
        $('body').on('click', 'tr #btn-export-ijinprinsip', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            //var tabledua = $('#table-detil-condition').DataTable();
            var data = table.row(baris).data();
            //var datadua = tabledua.row(barisdua).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            var BEID = data["BE_NAMES"];
            var CUSTOMERNAME = data["CUSTOMER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAMES = data["PROFIT_CENTER_NAME"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }

            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];

            var PROFITCENTERNAME = PROFIT_CENTER_NAMES.split('-');
            var PROFITCENTERNAMES = PROFITCENTERNAME[1];
            var PROFIT_CENTER_NAME = PROFITCENTERNAMES.replace('Cabang', '');

            //console.log(BE_ID);
            //console.log(CUSTOMER_NAME);
            //console.log(CONTRACT_USAGE);
            //console.log(PROFIT_CENTER_NAME);

            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var CONTRACT_OFFER_TYPE = data["CONTRACT_OFFER_TYPE"];
            var CONTRACT_OFFER_NAME = data["CONTRACT_OFFER_NAME"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var CURRENCY = data["CURRENCY"];
            var OFFER_STATUS = data["OFFER_STATUS"];
            var BE_CITY = data["BE_CITY"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];
            var REGIONAL_NAME = data["REGIONAL_NAME"];

            var VALID_FROM = data["VALID_FROM"];
            var VALID_TO = data["VALID_TO"];
            var TOTAL_NET_VALUE = data["TOTAL_NET_VALUE"];
            var UNIT_PRICE = data["UNIT_PRICE"];
            var CONDITION_TYPE = data["CONDITION_TYPE"];
            //var NJOP_PERCENT = data["NJOP_PERCENT"];

            //if (NJOP_PERCENT == undefined) {
            //    NJOP_PERCENT = '0';
            //}
            var dataJSON = {

                CONTRACT_OFFER_NO: CONTRACT_OFFER_NO

            };
            var dataJSON = "{CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) + "}";
            $.ajax({
                type: "POST",
                url: "/TransContractOffer/exportWordIjinprinsipDS",
                data: JSON.stringify(CONTRACT_OFFER_NO),
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI_TANAH, NORMALISASI_BANGUNAN, PENGALIHAN, ADMINISTRASI, WARMERKING, NJOP_DARATAN, NJOP_BANGUNAN, NJOP_NORMALISASI, NJOP_PENGALIHAN, NJOP_ADMINISTRASI, NJOP_WARMERKING,
                        TOTAL_SEWA_BANGUNAN, TOTAL_SEWA_DARATAN, VALID_FROM_DARATAN, VALID_FROM_BANGUNAN, VALID_FROM_NORMALISASI, VALID_FROM_PENGALIHAN, VALID_TO_DARATAN, VALID_TO_BANGUNAN, VALID_TO_NORMALISASI,
                        VALID_TO_PENGALIHAN, TOTAL_NOR_DARATAN, TOTAL_NOR_BANGUNAN, LUAS_NOR_DARATAN, LUAS_NOR_BANGUNAN;

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            SEWA_DARATAN = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            SEWA_BANGUNAN = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NORMALISASI_TANAH = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z020 Normalisasi Bangunan') {
                            NORMALISASI_BANGUNAN = response.data[i].UNIT_PRICE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            PENGALIHAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            ADMINISTRASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            WARMERKING = response.data[i].TOTAL_NET_VALUE;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            VALID_FROM_DARATAN = response.data[i].VALID_FROM;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            VALID_FROM_BANGUNAN = response.data[i].VALID_FROM;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            VALID_FROM_NORMALISASI = response.data[i].VALID_FROM;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            VALID_FROM_PENGALIHAN = response.data[i].VALID_FROM;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            VALID_TO_DARATAN = response.data[i].VALID_TO;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            VALID_TO_BANGUNAN = response.data[i].VALID_TO;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            VALID_TO_NORMALISASI = response.data[i].VALID_TO;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            VALID_TO_PENGALIHAN = response.data[i].VALID_TO;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            NJOP_DARATAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            NJOP_BANGUNAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NJOP_NORMALISASI = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            NJOP_PENGALIHAN = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            NJOP_ADMINISTRASI = response.data[i].NJOP_PERCENT;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            NJOP_WARMERKING = response.data[i].NJOP_PERCENT;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            TOTAL_SEWA_DARATAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            TOTAL_SEWA_BANGUNAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            TOTAL_NOR_DARATAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z020 Normalisasi Bangunan') {
                            TOTAL_NOR_BANGUNAN = response.data[i].TOTAL_NET_VALUE;
                        }
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            LUAS_NOR_DARATAN = response.data[i].LUAS;
                        } else if (response.data[i].CONDITION_TYPE == 'Z020 Normalisasi Bangunan') {
                            LUAS_NOR_BANGUNAN = response.data[i].LUAS;
                        }
                    }


                    if (SEWA_DARATAN == undefined) {
                        SEWA_DARATAN = '0';
                    }

                    if (SEWA_BANGUNAN == undefined) {
                        SEWA_BANGUNAN = '0';
                    }

                    if (NORMALISASI_TANAH == undefined) {
                        NORMALISASI_TANAH = '0';
                    }
                    if (NORMALISASI_BANGUNAN == undefined) {
                        NORMALISASI_BANGUNAN = '0';
                    }

                    if (PENGALIHAN == undefined) {
                        //PENGALIHAN = '....................';
                        PENGALIHAN = '0';
                    }

                    if (ADMINISTRASI == undefined) {
                        ADMINISTRASI = '0';
                    }

                    if (WARMERKING == undefined) {
                        //WARMERKING = '...............';
                        WARMERKING = '0';
                    }

                    if (NJOP_DARATAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_DARATAN = '0';
                    }
                    if (NJOP_BANGUNAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_BANGUNAN = '0';
                    }
                    if (NJOP_ADMINISTRASI == undefined) {
                        //WARMERKING = '...............';
                        NJOP_ADMINISTRASI = '0';
                    }
                    if (NJOP_NORMALISASI == undefined) {
                        //WARMERKING = '...............';
                        NJOP_NORMALISASI = '0';
                    }
                    if (NJOP_WARMERKING == undefined) {
                        //WARMERKING = '...............';
                        NJOP_WARMERKING = '0';
                    }
                    if (NJOP_PENGALIHAN == undefined) {
                        //WARMERKING = '...............';
                        NJOP_PENGALIHAN = '0';
                    }
                    if (TOTAL_SEWA_DARATAN == undefined) {
                        //WARMERKING = '...............';
                        TOTAL_SEWA_DARATAN = '0';
                    }
                    if (TOTAL_SEWA_BANGUNAN == undefined) {
                        //WARMERKING = '...............';
                        TOTAL_SEWA_BANGUNAN = '0';
                    }
                    if (VALID_FROM_NORMALISASI == undefined) {
                        VALID_FROM_NORMALISASI = '-';
                    }
                    if (VALID_TO_NORMALISASI == undefined) {
                        VALID_TO_NORMALISASI = '-';
                    }
                    if (LUAS_NOR_DARATAN == undefined) {
                        LUAS_NOR_DARATAN = '0';
                    }
                    if (LUAS_NOR_BANGUNAN == undefined) {
                        LUAS_NOR_BANGUNAN = '0';
                    }
                    if (TOTAL_NOR_DARATAN == undefined) {
                        TOTAL_NOR_DARATAN = '0';
                    }
                    if (TOTAL_NOR_BANGUNAN == undefined) {
                        TOTAL_NOR_BANGUNAN = '0';
                    }
                    if (VALID_FROM_BANGUNAN == undefined) {
                        VALID_FROM_BANGUNAN = '-';
                    }
                    if (VALID_FROM_DARATAN == undefined) {
                        VALID_FROM_DARATAN = '-';
                    }
                    if (VALID_TO_BANGUNAN == undefined) {
                        VALID_TO_BANGUNAN = '-';
                    }
                    if (VALID_TO_DARATAN == undefined) {
                        VALID_TO_DARATAN = '-';
                    }


                    var dataJSONS = {
                        CONTRACT_OFFER_NO: CONTRACT_OFFER_NO, CONTRACT_OFFER_TYPE: CONTRACT_OFFER_TYPE, BE_ID: BE_ID
                        , CONTRACT_START_DATE: CONTRACT_START_DATE, CONTRACT_END_DATE: CONTRACT_END_DATE, TERM_IN_MONTHS: TERM_IN_MONTHS
                        , CUSTOMER_NAME: CUSTOMER_NAME, PROFIT_CENTER_NAME: PROFIT_CENTER_NAME, CONTRACT_USAGE: CONTRACT_USAGE
                        , BE_CITY: BE_CITY, LUAS_TANAH: LUAS_TANAH, LUAS_BANGUNAN: LUAS_BANGUNAN, RO_ADDRESS: RO_ADDRESS
                        , REGIONAL_NAME: REGIONAL_NAME, VALID_FROM: VALID_FROM, VALID_TO: VALID_TO, TOTAL: TOTAL_NET_VALUE
                        , UNIT_PRICE: UNIT_PRICE, CONDITION_TYPE: CONDITION_TYPE, NJOP_NORMALISASI: NJOP_NORMALISASI
                        , SEWA_DARATAN: SEWA_DARATAN, SEWA_BANGUNAN: SEWA_BANGUNAN, PENGALIHAN: PENGALIHAN
                        , ADMINISTRASI: ADMINISTRASI, WARMERKING: WARMERKING, TOTAL_SEWA_DARATAN: TOTAL_SEWA_DARATAN, TOTAL_SEWA_BANGUNAN: TOTAL_SEWA_BANGUNAN
                        , NJOP_DARATAN: NJOP_DARATAN, NJOP_BANGUNAN: NJOP_BANGUNAN, VALID_FROM_NORMALISASI: VALID_FROM_NORMALISASI
                        , VALID_TO_NORMALISASI: VALID_TO_NORMALISASI, LUAS_NOR_DARATAN: LUAS_NOR_DARATAN, LUAS_NOR_BANGUNAN: LUAS_NOR_BANGUNAN
                        , TOTAL_NOR_DARATAN: TOTAL_NOR_DARATAN, TOTAL_NOR_BANGUNAN: TOTAL_NOR_BANGUNAN, VALID_FROM_DARATAN: VALID_FROM_DARATAN
                        , VALID_FROM_BANGUNAN: VALID_FROM_BANGUNAN, VALID_TO_BANGUNAN: VALID_TO_BANGUNAN, VALID_TO_DARATAN: VALID_TO_DARATAN
                    };

                    if (SEWA_BANGUNAN == "0") {
                        var SEWA = SEWA_DARATAN;
                    }
                    else if (SEWA_DARATAN == "0") {
                        SEWA = SEWA_BANGUNAN;
                    }
                    else {
                        SEWA = SEWA_BANGUNAN;
                    }


                    if (LUAS_TANAH == "0") {
                        var LUAS = LUAS_BANGUNAN;
                    }
                    else if (LUAS_BANGUNAN == "0") {
                        LUAS = LUAS_TANAH;
                    }
                    else {
                        LUAS = LUAS_TANAH;
                    }

                    var Years = parseFloat(TERM_IN_MONTHS) / 12;

                    var jumlah_sewa = parseFloat(LUAS) * (0.05 * parseFloat(SEWA)) * Years;
                    var ppn = 0.1 * jumlah_sewa;

                    var jumlah_seluruh = jumlah_sewa + ppn + parseFloat(ADMINISTRASI) + parseFloat(WARMERKING);



                    console.log("sewa : " + jumlah_sewa + ", ppn : " + ppn + ", seluruh: " + jumlah_seluruh);

                    console.log(dataJSONS);

                    App.blockUI({ boxed: true });
                    $.ajax({
                        type: "POST",
                        url: "/TransContractOffer/exportWordIjinprinsip",
                        data: JSON.stringify(dataJSONS),
                        contentType: "application/json; charset-utf8",
                        datatype: "jsondata",
                        async: "true",
                        success: function (data) {
                            fileWords = data.data;
                            App.unblockUI();
                            swal({
                                title: "File Successfully Generated",
                                text: "[" + data.data + "]" +
                                    '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                                type: "success",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        error: function (response) {
                            //console.log(response);
                        }
                    });
                },
                error: function (response) {
                    //console.log(response);
                }
            });


            //var dataJSON = "{CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) + ",CONTRACT_OFFER_TYPE:" + JSON.stringify(CONTRACT_OFFER_TYPE) + ",BE_ID:" + JSON.stringify(BE_ID) +
            //               ",CONTRACT_START_DATE:" + JSON.stringify(CONTRACT_START_DATE) + ",CONTRACT_END_DATE:" + JSON.stringify(CONTRACT_END_DATE) + ",TERM_IN_MONTHS:" + JSON.stringify(TERM_IN_MONTHS) +
            //               ",CUSTOMER_NAME:" + JSON.stringify(CUSTOMER_NAME) + ",PROFIT_CENTER_NAME:" + JSON.stringify(PROFIT_CENTER_NAME) + ",CONTRACT_USAGE:" + JSON.stringify(CONTRACT_USAGE) +
            //               ",BE_CITY:" + JSON.stringify(BE_CITY) + ",LUAS_TANAH:" + JSON.stringify(LUAS_TANAH) + ",LUAS_BANGUNAN:" + JSON.stringify(LUAS_BANGUNAN) + ",RO_ADDRESS:" + JSON.stringify(RO_ADDRESS) + 
            //               ",REGIONAL_NAME:" + JSON.stringify(REGIONAL_NAME) + ",VALID_FROM:" + JSON.stringify(VALID_FROM) + ",VALID_TO:" + JSON.stringify(VALID_TO) + ",TOTAL:" + JSON.stringify(TOTAL_NET_VALUE) + 
            //               ",UNIT_PRICE:" + JSON.stringify(UNIT_PRICE) + ",CONDITION_TYPE:" + JSON.stringify(CONDITION_TYPE) + 
            //               ",SEWA_DARATAN:" + JSON.stringify(SEWA_DARATAN) + ",SEWA_BANGUNAN:" + JSON.stringify(SEWA_BANGUNAN) + ",NORMALISASI:" + JSON.stringify(NORMALISASI) + ",PENGALIHAN:" + JSON.stringify(PENGALIHAN) + 
            //               ",ADMINISTRASI:" + JSON.stringify(ADMINISTRASI) + ",WARMERKING:" + JSON.stringify(WARMERKING) +"}";

            //console.log(dataJSON);

            //App.blockUI({ boxed: true });
            //$.ajax({
            //    type: "POST",
            //    url: "/TransContractOffer/exportWordIjinprinsip",
            //    data: dataJSON,
            //    contentType: "application/json; charset-utf8",
            //    datatype: "jsondata",
            //    async: "true",
            //    success: function (data) {
            //        fileWords = data.data;
            //        App.unblockUI();
            //        swal({
            //            title: "File Successfully Generated",
            //            text: "[" + data.data + "]" +
            //                  '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
            //                  ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
            //            type: "success",
            //            showConfirmButton: false,
            //            allowOutsideClick: false
            //        });
            //    },
            //    error: function (response) {
            //        //console.log(response);
            //    }
            //});

        });
    }

    var initExportdataijinprinsip

    condition = function () {
        $('body').on('click', 'tr #btn-export-ijinprinsip', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            //var tabledua = $('#table-detil-condition').DataTable();
            var data = table.row(baris).data();
            //var datadua = tabledua.row(barisdua).data();

            //var tableDetilObject = $('#table-detil-object').DataTable();
            //tableDetilObject.destroy();

            var tableDetilCondition = $('#table-detil-condition').DataTable();
            tableDetilCondition.destroy();

            //var BEID = data["BE_NAMES"];
            //var CUSTOMERNAME = data["CUSTOMER_NAMES"];
            //var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            //var PROFIT_CENTER_NAMES = data["PROFIT_CENTER_NAME"];

            //var BE_ID = BEID.replace('Cabang', '');
            //var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            //var CUSTOMER_NAME;
            //if (CUSTOMERNAMES[1] == undefined) {
            //    CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            //} else {
            //    CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            //}


            //var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            //var CONTRACT_USAGE = CONTRACTUSAGE[1];

            //var PROFITCENTERNAME = PROFIT_CENTER_NAMES.split('-');
            //var PROFITCENTERNAMES = PROFITCENTERNAME[1];
            //var PROFIT_CENTER_NAME = PROFITCENTERNAMES.replace('Cabang', '');

            //var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            //var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            //var CONTRACT_OFFER_TYPE = data["CONTRACT_OFFER_TYPE"];
            //var CONTRACT_OFFER_NAME = data["CONTRACT_OFFER_NAME"];
            //var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            //var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            //var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            //var CUSTOMER_AR = data["CUSTOMER_AR"];
            //var CURRENCY = data["CURRENCY"];
            //var OFFER_STATUS = data["OFFER_STATUS"];
            //var BE_CITY = data["BE_CITY"];
            //var LUAS_TANAH = data["LUAS_TANAH"];
            //var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            //var RO_ADDRESS = data["RO_ADDRESS"];
            //var REGIONAL_NAME = data["REGIONAL_NAME"];

            var VALID_FROM = data["VALID_FROM"];
            var VALID_TO = data["VALID_TO"];


            var dataJSON = "{VALID_FROM:" + JSON.stringify(VALID_FROM) + ",VALID_TO:" + JSON.stringify(VALID_TO) + "}";

            console.log(dataJSON);
            console.log("From : " + VALID_FROM);
            console.log("To : " + VALID_TO);

            App.blockUI({ boxed: true });
            //$.ajax({
            //    type: "POST",
            //    url: "/TransContractOffer/exportWordIjinprinsip",
            //    data: dataJSON,
            //    contentType: "application/json; charset-utf8",
            //    datatype: "jsondata",
            //    async: "true",
            //    success: function (data) {
            //        fileWords = data.data;
            //        App.unblockUI();
            //        swal({
            //            title: "File Successfully Generated",
            //            text: "[" + data.data + "]" +
            //                  '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
            //                  ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
            //            type: "success",
            //            showConfirmButton: false,
            //            allowOutsideClick: false
            //        });
            //    },
            //    error: function (response) {
            //        //console.log(response);
            //    }
            //});

        });
    }

    var initExportdataijinprinsipD = function () {
        $('body').on('click', 'tr #btn-export-ijinprinsipD', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var BEID = data["BE_NAMES"];
            var CUSTOMERNAME = data["CUSTOMER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAMES = data["PROFIT_CENTER_NAME"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }


            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];

            var PROFITCENTERNAME = PROFIT_CENTER_NAMES.split('-');
            var PROFITCENTERNAMES = PROFITCENTERNAME[1];
            var PROFIT_CENTER_NAME = PROFITCENTERNAMES.replace('Cabang', '');

            //console.log(BE_ID);
            //console.log(CUSTOMER_NAME);
            //console.log(CONTRACT_USAGE);
            //console.log(PROFIT_CENTER_NAME);


            var CREATION_DATE = data["CREATION_DATE"];
            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var CONTRACT_OFFER_TYPE = data["CONTRACT_OFFER_TYPE"];
            var CONTRACT_OFFER_NAME = data["CONTRACT_OFFER_NAME"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var CURRENCY = data["CURRENCY"];
            var OFFER_STATUS = data["OFFER_STATUS"];
            var BE_CITY = data["BE_CITY"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];

            var dataJSON = "{CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) + "}";
            $.ajax({
                type: "POST",
                url: "/TransContractOffer/exportWordIjinprinsipDS",
                data: JSON.stringify(CONTRACT_OFFER_NO),
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI, PENGALIHAN, ADMINISTRASI, WARMERKING;
                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            SEWA_DARATAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            SEWA_BANGUNAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NORMALISASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            PENGALIHAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            ADMINISTRASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            WARMERKING = response.data[i].TOTAL_NET_VALUE;
                        }
                    }

                    if (SEWA_DARATAN == undefined) {
                        SEWA_DARATAN = '....................';
                    }

                    if (SEWA_BANGUNAN == undefined) {
                        SEWA_BANGUNAN = '....................';
                    }

                    if (NORMALISASI == undefined) {
                        NORMALISASI = '....................';
                    }

                    if (PENGALIHAN == undefined) {
                        PENGALIHAN = '....................';
                    }

                    if (ADMINISTRASI == undefined) {
                        ADMINISTRASI = '....................';
                    }

                    if (WARMERKING == undefined) {
                        WARMERKING = '...............';
                    }

                    var dataJSONS = {
                        SEWA_DARATAN: SEWA_DARATAN, SEWA_BANGUNAN: SEWA_BANGUNAN, NORMALISASI: NORMALISASI,
                        PENGALIHAN: PENGALIHAN, ADMINISTRASI: ADMINISTRASI, WARMERKING: WARMERKING,
                        CONTRACT_OFFER_TYPE: CONTRACT_OFFER_TYPE, BE_ID: BE_ID,
                        CONTRACT_START_DATE: CONTRACT_START_DATE,
                        CONTRACT_END_DATE: CONTRACT_END_DATE, TERM_IN_MONTHS: TERM_IN_MONTHS,
                        CUSTOMER_NAME: CUSTOMER_NAME, PROFIT_CENTER_NAME: PROFIT_CENTER_NAME,
                        CONTRACT_USAGE: CONTRACT_USAGE,
                        BE_CITY: BE_CITY, LUAS_TANAH: LUAS_TANAH,
                        LUAS_BANGUNAN: LUAS_BANGUNAN, RO_ADDRESS: RO_ADDRESS,
                        CREATION_DATE: CREATION_DATE
                    };

                    App.blockUI({ boxed: true });
                    $.ajax({
                        type: "POST",
                        url: "/TransContractOffer/exportWordIjinprinsipD",
                        data: JSON.stringify(dataJSONS),
                        contentType: "application/json; charset-utf8",
                        datatype: "jsondata",
                        async: "true",
                        success: function (data) {
                            fileWords = data.data;
                            App.unblockUI();
                            swal({
                                title: "File Successfully Generated",
                                text: "[" + data.data + "]" +
                                    '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                                type: "success",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        error: function (response) {
                            //console.log(response);
                        }
                    });
                },
                error: function (response) {
                    //console.log(response);
                }
            });
        });
    }

    var initExportdataijinprinsipP = function () {
        $('body').on('click', 'tr #btn-export-ijinprinsipP', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-created').DataTable();
            var data = table.row(baris).data();

            var tableDetilObject = $('#table-detil-object').DataTable();
            tableDetilObject.destroy();

            var BEID = data["BE_NAMES"];
            var CUSTOMERNAME = data["CUSTOMER_NAMES"];
            var CONTRACT_USAGES = data["CONTRACT_USAGE"];
            var PROFIT_CENTER_NAMES = data["PROFIT_CENTER_NAME"];

            var BE_ID = BEID.replace('Cabang', '');
            var CUSTOMERNAMES = CUSTOMERNAME.split(',');
            var CUSTOMER_NAME;
            if (CUSTOMERNAMES[1] == undefined) {
                CUSTOMER_NAME = 'saudara ' + CUSTOMERNAMES[0];
            } else {
                CUSTOMER_NAME = CUSTOMERNAMES[1] + '.' + CUSTOMERNAMES[0];
            }

            var CONTRACTUSAGE = CONTRACT_USAGES.split('-');
            var CONTRACT_USAGE = CONTRACTUSAGE[1];

            var PROFITCENTERNAME = PROFIT_CENTER_NAMES.split('-');
            var PROFITCENTERNAMES = PROFITCENTERNAME[1];
            var PROFIT_CENTER_NAME = PROFITCENTERNAMES.replace('Cabang', '');

            var CREATION_DATE = data["CREATION_DATE"];
            var RENTAL_REQUEST_NO = data["RENTAL_REQUEST_NO"];
            var CONTRACT_OFFER_NO = data["CONTRACT_OFFER_NO"];
            var CONTRACT_OFFER_TYPE = data["CONTRACT_OFFER_TYPE"];
            var CONTRACT_OFFER_NAME = data["CONTRACT_OFFER_NAME"];
            var CONTRACT_START_DATE = data["CONTRACT_START_DATE"];
            var CONTRACT_END_DATE = data["CONTRACT_END_DATE"];
            var TERM_IN_MONTHS = data["TERM_IN_MONTHS"];
            var CUSTOMER_AR = data["CUSTOMER_AR"];
            var CURRENCY = data["CURRENCY"];
            var OFFER_STATUS = data["OFFER_STATUS"];
            var BE_CITY = data["BE_CITY"];
            var LUAS_TANAH = data["LUAS_TANAH"];
            var LUAS_BANGUNAN = data["LUAS_BANGUNAN"];
            var RO_ADDRESS = data["RO_ADDRESS"];

            var dataJSON = "{CONTRACT_OFFER_NO:" + JSON.stringify(CONTRACT_OFFER_NO) + "}";
            $.ajax({
                type: "POST",
                url: "/TransContractOffer/exportWordIjinprinsipP",
                data: JSON.stringify(CONTRACT_OFFER_NO),
                contentType: "application/json; charset-utf8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    var SEWA_DARATAN, SEWA_BANGUNAN, NORMALISASI, PENGALIHAN, ADMINISTRASI, WARMERKING;
                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].CONDITION_TYPE == 'Z003 Sewa Daratan') {
                            SEWA_DARATAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z005 Sewa Bangunan') {
                            SEWA_BANGUNAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z019 Normalisasi Tanah/Daratan') {
                            NORMALISASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z007 Biaya Pengalihan') {
                            PENGALIHAN = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z009 Administrasi') {
                            ADMINISTRASI = response.data[i].TOTAL_NET_VALUE;
                        } else if (response.data[i].CONDITION_TYPE == 'Z010 Warmeking') {
                            WARMERKING = response.data[i].TOTAL_NET_VALUE;
                        }
                    }

                    if (SEWA_DARATAN == undefined) {
                        SEWA_DARATAN = '....................';
                    }

                    if (SEWA_BANGUNAN == undefined) {
                        SEWA_BANGUNAN = '....................';
                    }

                    if (NORMALISASI == undefined) {
                        NORMALISASI = '....................';
                    }

                    if (PENGALIHAN == undefined) {
                        PENGALIHAN = '....................';
                    }

                    if (ADMINISTRASI == undefined) {
                        ADMINISTRASI = '....................';
                    }

                    if (WARMERKING == undefined) {
                        WARMERKING = '...............';
                    }

                    var dataJSONS = {
                        SEWA_DARATAN: SEWA_DARATAN, SEWA_BANGUNAN: SEWA_BANGUNAN,
                        NORMALISASI: NORMALISASI, PENGALIHAN: PENGALIHAN, ADMINISTRASI: ADMINISTRASI, WARMERKING: WARMERKING,
                        CONTRACT_OFFER_TYPE: CONTRACT_OFFER_TYPE, BE_ID: BE_ID,
                        CONTRACT_START_DATE: CONTRACT_START_DATE, CONTRACT_END_DATE: CONTRACT_END_DATE, TERM_IN_MONTHS: TERM_IN_MONTHS,
                        CUSTOMER_NAME: CUSTOMER_NAME, PROFIT_CENTER_NAME: PROFIT_CENTER_NAME, CONTRACT_USAGE: CONTRACT_USAGE,
                        BE_CITY: BE_CITY, LUAS_TANAH: LUAS_TANAH, LUAS_BANGUNAN: LUAS_BANGUNAN, RO_ADDRESS: RO_ADDRESS,
                        CREATION_DATE: CREATION_DATE
                    };

                    App.blockUI({ boxed: true });
                    $.ajax({
                        type: "POST",
                        url: "/TransContractOffer/exportWordIjinprinsipQ",
                        data: JSON.stringify(dataJSONS),
                        contentType: "application/json; charset-utf8",
                        datatype: "jsondata",
                        async: "true",
                        success: function (data) {
                            fileWords = data.data;
                            App.unblockUI();
                            swal({
                                title: "File Successfully Generated",
                                text: "[" + data.data + "]" +
                                    '<br /><br />' + '<a href="../REMOTE/DownloadForm/' + data.data + '" target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                                    ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                                type: "success",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        error: function (response) {
                            //console.log(response);
                        }
                    });
                },
                error: function (response) {
                    //console.log(response);
                }
            });
        });
    }

    var datePicker = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: "new Date().getTime()",
                format: "dd/mm/yyyy"
            });
            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var changeStatus = function () {
        $('body').on('click', 'tr #btn-inactive1', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-trans-approved-list').DataTable();
            var data = table.row(baris).data();
            xNo = data["CONTRACT_OFFER_NO"];
            swal({
                title: 'Warning',
                text: "Are you sure want to update this data status?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'

            }).then(function (isConfirm) {
                if (isConfirm) {

                    var param = {
                        xNO: xNo
                    };

                    //console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransContractOffer/EditStatusApproval1",
                        timeout: 30000
                    });


                    req.done(function (data) {
                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            swal('Failed', data.message, 'error');
                        }
                    });
                }
            });
        });
    }

    return {
        init: function () {
            add();
            initTableTransContractOfferCreated();
            initTableTransContractOfferWorkflow();
            initTableTransContractOfferRevised();
            initTableTransContractOfferApproved();
            initDetilTransContractOfferCreated();
            initDetilTransContractOfferWorkflow();
            initDetilTransContractOfferRevised();
            initDetilTransContractOfferApproved();
            releaseWorkflow();
            releaseWorkflowRevised();
            approve();
            edit();
            editRevised();
            editStatus();
            fupload();
            fuploadWorkflowList();
            fuploadRevisedList();
            fuploadApprovedList();
            deleteAttachment();
            deleteAttachmentWorkflowList();
            deleteAttachmentRevisedList();
            deleteAttachmentApprovedList();
            initAttachment();
            initAttachmentWorkflowList();
            initAttachmentRevisedList();
            initAttachmentApprovedList();
            initExportdataijinprinsip();
            initExportdataijinprinsipD();
            initExportdataijinprinsipP();
            //initExportdataijinprinsipcondition();
            changeStatus();
            datePicker();
        }
    };
}();

function Clicks() {
    //console.log('Yess ' + fileWords);
    var jsonData = "{fileName:'" + fileWords + "'}";
    setTimeout(function () {
        deleteExcel(jsonData);
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/TransContractOffer/deleteWord",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            //console.log("Sukses");
        },
        error: function (response) {
            //console.log(response);
        }
    });

}


function coHitungArea() {
    var con = $('#table-detil-object').dataTable();
    var all_area = 0;
    for (var i = 0; i < con.fnGetData().length; i++) {
        var area1 = con.fnGetData(i).BUILDING_DIMENSION;
        var area2 = con.fnGetData(i).LAND_DIMENSION;
        all_area = all_area + Number(area1) + Number(area2);
        ////console.log(all_area);
    }
    ////console.log(all_area);
    $('.LAND_AREA_NEW').html(all_area.toLocaleString('en-US'));
}

function coHitungValue() {
    var con = $('#table-detil-condition').DataTable();
    var all_value = 0;
    //var data = con.data()
    ////console.log(data);
    $('.DETAIL_RO_NEW').html('');
    for (var i = 0; i < con.data().count(); i++) {
        var row = con.row(i).data();
        ////console.log(row);
        var net_val = row.TOTAL_NET_VALUE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var price = row.UNIT_PRICE.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var calc_ob = row.CALC_OBJECT;
        ////console.log(net_val, calc_ob);
        if (calc_ob != 'Header Document') {
            all_value = all_value + parseFloat(net_val.split(',').join(''));
            var ro_number = calc_ob.split(' ')[1];
            var elems = $('#table-detil-object').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                if ($(x).html() == ro_number) {
                    ////console.log(index);
                    ro_data = $('#table-detil-object').DataTable().row(index).data();
                    //console.log(ro_data);
                    var luas = Number(ro_data.LUAS_BANGUNAN) + Number(ro_data.LUAS_TANAH);
                    var text = $('.DETAIL_RO_NEW').html() + '<tr><td>' + calc_ob.split(" ")[1] + '</td><td>|</td><td>' + luas + ' m<sup>2</sup> </td><td>|</td><td> ' + net_val + ' ' + $('#CURRENCY').val() + '</td></tr>';
                    $('.DETAIL_RO_NEW').html(text)
                    //console.log(text);
                }
            });
        }
    }
    ////console.log(text);
    $('.CONTRACT_VALUE_NEW').html(all_value.toLocaleString('en-US'));
}

function coHitungPeriod() {
    var value = $('#TERM_IN_MONTHS').val();
    var start = $('#CONTRACT_START_DATE').val().replace(/\//g, ".");
    var end = $('#CONTRACT_END_DATE').val().replace(/\//g, ".");
    $('.CONTRACT_START_NEW').html(start);
    $('.CONTRACT_END_NEW').html(end);
    $('.CONTRACT_PERIOD_NEW').html(value);
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransContractOffer.init();
        DataSurat.init();
    });
}

$(document).ready(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase();
        var regex = new RegExp("(.*?)\.(docx|doc|pdf|xls|xlsx|jpg|jpeg|zip)$");
        if (!(regex.test(val))) {
            $(this).val('');
            $('#viewModalAttachment').modal('hide');
            swal('Warning', 'Please select correct file format! Only Ms. Word, Ms. Excel, Pdf, JPG, and ZIP Extension Are Allowed', 'warning');
        }
    });
});



function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
        , sep = usa ? ',' : '.'
        , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
            , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}
