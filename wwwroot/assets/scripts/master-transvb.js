﻿var TransVB = function () {

    var initTableVBTransaction = function () {
        $('#table-transvb').DataTable({

            "ajax":
            {
                "url": "TransVB/GetDataVBTrans",
                "type": "GET",

            },

            "columns": [
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "POSTING_DATE",
                    "class": "dt-body-center"

                },
                {
                    "data": "PROF_CENTER"
                },
                {
                    "data": "COSTUMER_MDM"
                },
                {
                    "data": "SERVICES_GROUP"
                },
                {
                    "data": "TOTAL",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.SAP_DOCUMENT_NUMBER != null) {
                            if(full.CANCEL_STATUS!=null){
                                return full.SAP_DOCUMENT_NUMBER + '<br><span class="badge badge-danger"> CANCEL </span>';
                            }
                            else {
                                return full.SAP_DOCUMENT_NUMBER;
                            }                                   
                        } else {
                            if (full.CANCEL_STATUS != null) {
                                return '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billingx"><i class="fa fa-paper-plane-o"></i></a>' + '<br><span class="badge badge-danger"> CANCEL </span>';
                            }
                            else {
                                return '<a data-toggle="modal" class="btn btn-icon-only blue" id="btn-post-billing"><i class="fa fa-paper-plane-o"></i></a>';

                            }
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                         var aksi = '<a data-toggle="modal" href="#viewVBPTransModal" class="btn btn-icon-only green" id="btn-detail-vbp-trans"><i class="fa fa-eye"></i></a>';
                         return aksi;
                     },
                     "class": "dt-body-center"
                 }

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],

            "pageLength": 10,

            //"language": {
            //    "url": ""
            //},

            "filter": true
        });

    }
    
    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail-vbp-trans', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb').DataTable();
            var data = table.row(baris).data();

            $("#DETAIL_ID").val(data["ID"]);
            $("#DETAIL_PROFIT_CENTER").val(data["PROF_CENTER"]);
            $("#DETAIL_POSTING_DATE").val(data["POSTING_DATE"]);
            $("#DETAIL_SERVICE_GROUP").val(data["SERVICES_GROUP"]);
            $("#DETAIL_CUSTOMER_MDM").val(data["COSTUMER_MDM"]);
            $("#CUSTOMER_ID").val(data["COSTUMER_ID"]);
            $("#DETAIL_INSTALLATION_ADDRESS").val(data["INSTALLATION_ADDRESS"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);
            $("#DETAIL_CUSTOMER_SAP_AR").val(data["CUSTOMER_SAP_AR"]);
            var tot = data["TOTAL"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#DETAIL_TOTAL").val(tot);
            $("#DETAIL_SAP_DOCUMENT_NUMBER").val(data["SAP_DOCUMENT_NUMBER"]);
            $("#DETAIL_TAX_CODE").val(data["TAX_CODE"]);

            var id_vb = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/TransVB/GetDataTransaction/" + id_vb,
                    "type": "GET",

                },

                "columns": [
                    {
                        "data": "VB_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "START_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "END_DATE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SERVICE_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CM_PRICE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)
                    },
                    {
                        "data": "AMOUNT",
                        "class": "dt-body-center",
                        render: $.fn.dataTable.render.number('.', '.', 0)

                    },
                    {
                        "data": "REMARK",
                        "class": "dt-body-center"

                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,


                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                //"language": {
                //    "url": ""
                //},

                "filter": false
            });

        });
    }

    var add = function () {
        $('#btn-add').click(function () {
            window.location.href = '/TransVB/AddTransVB';
        });
    }

    // Aksi Button Untuk POST BILLING
    var postBilling = function () {
        $('body').on('click', 'tr #btn-post-billing', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transvb').DataTable();
            var data = table.row(baris).data();
            console.log('billing type ' + data['BILLING_TYPE']);
            var billing_no = data["ID"];

            swal({
                title: 'Warning',
                text: "Are you sure want to post this data to Billing?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (isConfirm) {
                if (isConfirm) {
                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: "{ BILL_ID:'" + data["ID"] + "' , BILL_TYPE: '" + data["BILLING_TYPE"] + "'}",
                        method: "POST",
                        url: "TransVB/postToSAP"
                    });
                    req.done(function (data) {

                        App.unblockUI();
                        table.ajax.reload(null, false);
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success');
                        } else {
                            // swal('Failed', data.message, 'error');
                            //console.log(billing_no);
                            $.ajax({
                                type: "POST",
                                url: "/TransVB/GetMsgSAP",
                                data: "{ BILL_ID:'" + billing_no + "'}",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var tampung = "";
                                    for (var i = 0; i < data.length; i++) {
                                        var x = i % 2;
                                        
                                        //console.log(data[i]);
                                        if (x === 1) {
                                            console.log(data[i]);
                                            tampung += '<ul><li>'+data[i] + '</li></ul>';
                                        }
                                    }
                                    swal('Failed', tampung, 'error').then(function (isConfirm) {
                                        window.location = "/TransVB";
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    }

    return {
        init: function () {
            add();
            initTableVBTransaction();
            initDetilTransaction();
            postBilling();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransVB.init();
    });
}

