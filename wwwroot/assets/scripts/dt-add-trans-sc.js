﻿var TransSpecialContract = function () {

    var arrObject;
    arrObject = new Array();

    var arrayObjectVal;
    arrObjectVal = new Array();

    var buah = [];


    var mdm = function () {

        $('#BUSINESS_PARTNER_NAME').autocomplete({
            source: function (request, response) {
                var inp = $('#BUSINESS_PARTNER_NAME').val();
                var l = inp.length;
                var params = { MPLG_NAMA : inp};
                $.ajax({
                    type: "POST",
                    url: "/TransSpecialContract/GetDataCustomer",
                    data: JSON.stringify(inp),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (el) {
                            return {
                                label: el.label,
                                value: "",
                                address: el.address,
                                sap: el.sap,
                                code: el.code,
                                name: el.name
                            };
                        }));
                    }
                });

            },
            select: function (event, ui) {
                event.preventDefault();
                this.value = ui.item.name;
                $("#CUSTOMER_AR").val(ui.item.sap);
                $("#BUSINESS_PARTNER").val(ui.item.code);
            }
        });
    }

    var hitung_bulan = function () {

        //FUNCTION Hitung Bulan
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();

            if (d2.getDate() >= d1.getDate())
                months++

            return months <= 0 ? 0 : months;
        }

        $('#CONTRACT_END_DATE').change(function () {
            var mulai = $('#CONTRACT_START_DATE').val();
            var akhir = $('#CONTRACT_END_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

            var dateAkhir = akhir;
            var datearrayAkhir = dateAkhir.split("/");
            var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

            //console.log(newdateMulai);
            //console.log(newdateAkhir);
            var tim = monthDiff(new Date(newdateMulai), new Date(newdateAkhir));
            $('#TERM_IN_MONTHS').val(tim);
        });

    }

    var hitung_tanggal_sampai = function () {
        function addMonths(date, count) {
            if (date && count) {
                var m, d = (date = new Date(+date)).getDate()

                date.setMonth(date.getMonth() + count, 1)
                m = date.getMonth()
                date.setDate(d)
                if (date.getMonth() !== m) date.setDate(0)
            }
            return date
        }

        $('#TERM_IN_MONTHS').change(function () {
            var tim = $('#TERM_IN_MONTHS').val();
            var mulai = $('#CONTRACT_START_DATE').val();

            var dateMulai = mulai;
            var datearrayMulai = dateMulai.split("/");
            var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

            //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
            var f_date = new Date(newdateMulai);
            var n_tim = Number(tim);
            var end_date = addMonths(f_date, n_tim);
            var xdate = new Date(end_date);
            var f_date = xdate.toISOString().slice(0, 10);

            //format f_date menjadi dd/mm/YYYY
            var dateAkhir = f_date;
            var datearrayAkhir = dateAkhir.split("-");
            var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];
            $('#CONTRACT_END_DATE').val(newdateAkhir);

            //console.log(newdateAkhir);
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransSpecialContract";
        });
    }

    var rTable = $('#editable_transrental');
    var rlTable = rTable.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "columnDefs": [
            {
                "targets": [0, 2],
                "class": "dt-body-center",
                "visible": false
            },
            {
                "targets": [1],
                "class": "dt-body-center"
            },
            {
                "targets": [5, 6],
                "class": "dt-body-center"
            },
            {
                "targets": [8],
                "class": "dt-body-center",
                "width": "10%"
            },
        ]
    });

    var filter = function () {
        $('#btn-filter').click(function () {
            $('#row-table').css('display', 'block');

            if ($.fn.DataTable.isDataTable('#table-filter-detail')) {
                $('#table-filter-detail').DataTable().destroy();
            }

            $('#table-filter-detail').DataTable({
                "ajax": {
                    "url": "/TransRentalRequest/GetDataFilter",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                        data.from = $('#from').val();
                        data.to = $('#to').val();
                        data.luas_tanah_from = $('#luas_tanah_from').val();
                        data.luas_tanah_to = $('#luas_tanah_to').val();
                        data.luas_bangunan_from = $('#luas_bangunan_from').val();
                        data.luas_bangunan_to = $('#luas_bangunan_to').val();
                        //console.log(data);
                    }

                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NUMBER",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE_ID",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "visible": false
                    },
                    {
                        "data": "OBJECT_ID"
                    },
                    {
                        "data": "RO_NAME"
                    },
                    {
                        "data": "LUAS_BANGUNAN_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LUAS_TANAH_RO",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "VALID_FROM",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },
                    {
                        "data": "VALID_TO",
                        "class": "dt-body-center",
                        "render": function (data) {
                            return moment(data).format("DD/MM/YYYY");
                        }
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "responsive": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });
        // Add event listener for opening and closing details

        var industry = function () {

            $.ajax({
                type: "GET",
                url: "/TransRentalRequest/GetDataDropDownIndustry",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].REF_DESC1 + "'>" + jsonList.data[i].REF_DESC1 + "</option>";
                    }
                    $("#INDUSTRY").append(listItems);
                }
            });
        }

        $('#table-filter-detail').on('click', 'tr #btn-cus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-filter-detail').DataTable();
            var data = table.row(baris).data();
            //var xInd = $('#INDUSTRY').val();
            rlTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);
            industry();
            $('#addDetailModal').modal('hide');
        });

        $('#editable_transrental').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data();
            rlTable.fnDeleteRow(baris);
        });

        $('#editable_transrental').on('click', 'tr #btn-savecus', function () {
            $(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#editable_transrental').DataTable();
            var data = table.row(baris).data();
            var xInd = $('#INDUSTRY').val();
            $('#editable_transrental').dataTable().fnUpdate(xInd, baris, 7, false);


            var kdRO1 = data[0];
            var kdRO = data[1];
            var nmRO = data[4];
            var rawArr = 'RO ' + kdRO + ' - ' + nmRO;
            var rawArrVal = 'RO ' + kdRO + '|' + kdRO1;
            console.log('raw arr ' + rawArr);
            $('#condition-option').html('');

            if (arrObject.indexOf(rawArr) === -1) {
                arrObject.push(rawArr);
                arrObjectVal.push(rawArrVal);
            }
            else {
                console.log("This item already exists");
            }


        });
    }

    var tablex = $('#condition');
    var oTable = tablex.dataTable({
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"]
        ],
        "paging": false,
        "pageLength": 5,
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "searching": false,
        "lengthChange": false,
        "bSort": false,
        "columnDefs": [
            {
                "targets": [20],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [20],
                "visible": false
            }
        ]
    });

    var addCondition = function () {
        $('#btn-add-condition').on('click', function () {

            var listItems = '';
            listItems += "<option value='Header Document'>Header Document</option>";
            //arrObject.forEach(function (element) {
            //    $('#condition-option').html('');
            //    console.log(element);
            //    listItems += "<option value='" + element + "'>" + element + "</option>";

            //    $("#condition-option").append(listItems);
            //});

            $.each(arrObject, function (i, item) {
                console.log(arrObject[i], arrObjectVal[i]);

                $('#condition-option').html('');
                listItems += "<option value='" + arrObjectVal[i] + "'>" + arrObject[i] + "</option>";

                $("#condition-option").append(listItems);
            });


            $('#cekbokList').html('');
            $.ajax({
                type: "GET",
                url: "/TransContractOffer/GetCheckboxConditionType",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var jsonList = data
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<input type='checkbox' + value='" + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + '|' + jsonList.data[i].VAL2 + "' name='test'> " + jsonList.data[i].REF_DATA + ' ' + jsonList.data[i].REF_DESC + "</input><br>";

                    }
                    $("#cekbokList").append(listItems);
                }
            });
        });
        
        $('#btn-condition').on('click', function () {
            // $('#condition').dataTable();

            //raw data value calc object dan di split |
            var id_combo = $('#condition-option').val();
            var splitCalcObject = id_combo.split("|");
            var calcObject = splitCalcObject[0];
            var id_ro = splitCalcObject[1];

            if (id_ro) {
                // Ajax untuk get meas type berdasarkan id_ro yang didapatkan dari split
                $('#meas-type').html('');
                // Ajax untuk ambil combobox RO sesuai dengan rental request yang dipilih

                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataMeasType/" + id_ro,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        var jsonList = data
                        var listItems = "";
                        listItems += '<option value="">--Choose--</option>';
                        for (var i = 0; i < jsonList.data.length; i++) {
                            listItems += "<option value='" + jsonList.data[i].MEAS_TYPE + '|' + jsonList.data[i].ID_DETAIL + "'>" + jsonList.data[i].MEAS_TYPE + ' - ' + jsonList.data[i].MEAS_DESC + "</option>";
                        }
                        // $("#meas-type").append(listItems);
                        $('select[name="meas-type"]').html(listItems);
                    }
                });
            }

            // Matikan form untuk kondisi Header Document
            if (id_ro) {
                var measType = '<select class="form-control" id="meas-type" name="meas-type" onchange="getLuas(this)"></select>';
            }
            else {
                measType = '';
            }
            if (id_ro) {
                var luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }
            else {
                luas = '';
            }

            //Cek selected value dari combobox calculation apakah header document atau RO
            var vCBCalculation = $("#condition-option").val();

            if (vCBCalculation == 'Header Document') {
                measType = '<input type="text" class="form-control" id="meas-type" name="meas-type" readonly>';
                luas = '<input type="text" class="form-control" id="luas" name="luas" readonly>';
            }

            buttonCoa = '<a class="btn default btn-xs blue input-group-addon" id="btn-coa" ><i class="fa fa-search"></i></a>';

            var tableDT = $('#condition  > tbody');
            var start = $('#CONTRACT_START_DATE').val();
            var end = $('#CONTRACT_END_DATE').val();
            var tim = $('#TERM_IN_MONTHS').val();

            var startD = '<center><input type="text" class="form-control" name="start-date" value="' + start + '" id="start-date"></center>';
            var endD = '<center><input type="text" class="form-control" name="end-date" value="' + end + '" id="end-date" onchange="hitungTermMonths(this)"></center>';
            var termMonths = '<center><input type="text" class="form-control" name="term-months" value="' + tim + '" id="term-months" onchange="hitungEndDate(this)"></center>';

            var kosong = '';
            var cekbokStatistic = '<center><input type="checkbox" value="1" id="statistic" name="statistic"></center>';
            var btnSaveCus = '<a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a>';
            var btnDelCus = '<a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a>';
            var unitPrice = '<center><input type="text" class="form-control" id="unit-price" name="unit-price" onchange="isiTotalUnitPrice(this)"></center>';
            var amtRef = '<center><select class="form-control" id="amt-ref" name="amt-ref" onchange="isiTotalAMTREF(this)"><option value="">--Choose--</option><option value="YEARLY">YEARLY</option><option value="ONETIME">ONETIME</option><option value="MONTHLY">MONTHLY</option></select></center>';
            var frequency = '<center><select class="form-control" onchange="infoIsiManual()" id="frequency"><option value="">--Choose--</option><option value="MANUAL">MANUAL</option><option value="MONTHLY">MONTHLY</option><option value="YEARLY">YEARLY</option></select></center>';
            var startDueDate = '<center><input type="text" class="form-control" name="start-due-date" id="start-due-date"></center>';
            var manualNo = '<center><input type="text" onchange="infoIsiManual()" class="form-control" id="manual-no"></center>';
            var formula = '<center><select class="form-control" id="formula" name="formula" onchange="hitungFormula(this)"><option value="">--Choose--</option><option value="D">D - OBJECT MEASUREMENT</option><option value="A">A - FIX AMOUNT</option><option value="E1">E1 - %SHARE IN COND</option><option value="U">U - SALES BASED RENT</option></select></center>';
            //var measType = '<select class="form-control" id="meas-type"></select>';
            //var luas = '<input type="text" class="form-control" id="luas">';
            var total = '<center><input type="text" class="form-control" id="total" name="total" readonly></center>';
            var persenFromNJOP = '<center><input type="text" class="form-control" id="persen-from-njop" name="persen-from-njop" onchange="hitungFormulaN(this)"></center>';
            var persenFromKondisiTeknis = '<center><input type="text" class="form-control" id="persen-from-kondisi-teknis" name="persen-from-kondisi-teknis" onchange="hitungFormulaT(this)"></center>';
            //var salesRule = '<center><input type="text" class="form-control" id="sales-rule"></center>';
            var totalNetValue = '<center><input type="text" class="form-control" id="total-net-value" name="total-net-value" readonly></center>';
            //var coa_produksi = '<center><select class="form-control coa_produksi" name="coa_produksi" id="coa_produksi"></select></center>';
            var coa_produksi = '<center class="input-group">' + buttonCoa + '<input type="text" class="form-control" id="coa_produksi" name="coa_produksi" readonly></center>';
            var coa_produksi_disabled = '<center><select disabled class="form-control coa_produksi_disabled" name="coa_produksi" id="coa_produksi"><option value="-">-</option></select></center>';
            //getCoaProduksi();

            //Format luas dengan separator . karena
            //var fLuas = raw_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            $('input[name="test"]:checked').each(function () {
                //raw data condition dan di split |
                var raw_cond = $(this).val();
                var split_cond = raw_cond.split("|");
                var res_condition = split_cond[0];
                var tax_code = split_cond[1];


                oTable.fnAddData([calcObject,
                                  res_condition,
                                  startD,
                                  endD,
                                  termMonths,
                                  cekbokStatistic,
                                  unitPrice,
                                  amtRef,
                                  frequency,
                                  startDueDate,
                                  manualNo,
                                  formula,
                                  measType,
                                  luas,
                                  total,
                                  persenFromNJOP,
                                  persenFromKondisiTeknis,
                                  totalNetValue,
                                  coa_produksi,
                                  '<center><a class="btn default btn-xs green" id="btn-savecus" name="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>',
                                  tax_code], true);

                var arrCond = buah.push(calcObject + ' | ' + res_condition);
                //console.log(arrCond);

                //var fruits = buah;

                //Masking Start Due Date
                $('input[name="start-due-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="start-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('input[name="end-date"]').inputmask("d/m/y", {
                    "placeholder": "dd/mm/yyyy"
                });

                $('#addCondition').modal('hide');
            });

            //$('#unit-price').mask('000.000.000.000.000', { reverse: true });
            $('#unit-price').on('change', function () {
                var unitPrice = $('#unit-price').val();
                var mask = sep1000(unitPrice, true);
                $('#unit-price').val(mask);
            });
        });

        $('#condition').on('click', 'tr #btn-editcus', function () {
            $('#condition > tbody').html('');
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data;

            oTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_BANGUNAN_RO'], data['LUAS_TANAH_RO'], '<select id="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus"><i class="fa fa-trash"></i></a></center>'], true);

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input class="form-control" id="mask_date1" type="text" />';

            }

            //table.fnUpdate(xInd, baris, 18, false);
            document.getElementById('btn-savecus').disabled = true;

        });

        //cari coa
        $('#condition').on('click', 'tr #btn-coa', function () {
            console.log(111);
            $('#coaModal').modal('show');
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransContractOffer/GetListCoa",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;
                        return data.data;
                    },
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        });

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            //console.log(data,value);

            var elems = $('#condition').find('tbody tr');
            $(elems).each(function (index) {
                var x = $(this).find('td')[0];
                var y = $(this).find('td')[1];
                c_data = $('#condition').DataTable().row(index).node();
                //console.log(index, c_data)

                var elem = $(c_data).find('#coa_produksi');
                elem.val(value);

            });
            $('#coaModal').modal('hide');
        });


        $('#condition').on('click', 'tr #btn-savecus', function () {
            //$(this).hide();
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            var start_date = $('#start-date').val();
            var end_date = $('#end-date').val();
            var term_months = $('#term-months').val();

            var unit_price = $('#unit-price').val();
            var amt_ref = $('#amt-ref').val();
            var frequency = $('#frequency').val();
            var start_due_date = $('#start-due-date').val();
            var manual_no = $('#manual-no').val();
            var formula = $('#formula').val();

            var meas_type = $('#meas-type').val();
            var splitselectedMeasType = meas_type.split("|");
            var id_detail_meas_type = splitselectedMeasType[0];

            var luas = $('#luas').val();
            var total = $('#total').val();
            var persen_from_njop = $('#persen-from-njop').val();
            var persen_from_kondisi_teknis = $('#persen-from-kondisi-teknis').val();

            //var sales_rule = $('#sales-rule').val();
            var total_net_value = $('#total-net-value').val();

            //Set Value Untuk Statistic
            var cek = $('input[id=statistic]').is(":checked");
            var vStatistic = "";
            if (cek) {
                vStatistic = "Yes";
            }
            else {
                vStatistic = "No";
            }

            // Cek apakah manual no kosong atau tidak
            var cekManualNo = $('input[id=manual-no]').val();

            var coa_produksi = $('#coa_produksi').val();
            console.log(coa_produksi);

            $('#condition').dataTable().fnUpdate(start_date, baris, 2, false);
            $('#condition').dataTable().fnUpdate(end_date, baris, 3, false);
            $('#condition').dataTable().fnUpdate(term_months, baris, 4, false);
            $('#condition').dataTable().fnUpdate(vStatistic, baris, 5, false);
            $('#condition').dataTable().fnUpdate(unit_price, baris, 6, false);
            $('#condition').dataTable().fnUpdate(amt_ref, baris, 7, false);
            $('#condition').dataTable().fnUpdate(frequency, baris, 8, false);
            $('#condition').dataTable().fnUpdate(start_due_date, baris, 9, false);
            $('#condition').dataTable().fnUpdate(manual_no, baris, 10, false);
            $('#condition').dataTable().fnUpdate(formula, baris, 11, false);
            $('#condition').dataTable().fnUpdate(id_detail_meas_type, baris, 12, false);
            $('#condition').dataTable().fnUpdate(luas, baris, 13, false);
            $('#condition').dataTable().fnUpdate(total, baris, 14, false);
            $('#condition').dataTable().fnUpdate(persen_from_njop, baris, 15, false);
            $('#condition').dataTable().fnUpdate(persen_from_kondisi_teknis, baris, 16, false);
            //$('#condition').dataTable().fnUpdate(sales_rule, baris, 17, false);
            $('#condition').dataTable().fnUpdate(total_net_value, baris, 17, false);
            $('#condition').dataTable().fnUpdate(coa_produksi, baris, 18, false);
            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus');
            elem.parentNode.removeChild(elem);
            //return false;

            console.log(cekManualNo);
            if (cekManualNo != '') {
                swal('Info', 'Anda Mengisi Manual No Pada Kondisi Ini, Jangan Lupa Untuk Membuat Manual Frequencynya Pada Tabel Dibawah! ', 'info');
            }
            else {

            }
        });

        $('#condition').on('click', 'tr #btn-delcus', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            // ambil data by kolom
            var rData = table.row($(this).parents('tr')).data();
            var calc_object = rData[0];
            var condition_type = rData[1];
            var dataRemove = calc_object + ' | ' + condition_type;
            // panggil function untuk remove array buah dan secondCellArray:)
            removeA(buah, dataRemove);
            oTable.fnDeleteRow(baris);
        });
    }

    //Var untuk menghandle data table manual-frequency
    var manual_frequency = function () {
        var tablem = $('#manual-frequency');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 100000,
            "paging": false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "lengthChange": false,
            "bSort": false,
            "columnDefs": [
            {
                "targets": [8],
                "visible": false
            }
            ]
        });

        // Variabel untuk handle combobox condition
        var cond = function () {
            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //$("#memo-condition").append(listItems);
            $('select[name="condition-frequency"]').html(listItems);
        }

        //Generate textbox(form) di data table manual-frequency
        $('#btn-add-manual-frequency').on('click', function () {
            var nomor = 10;

            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var xtable = $('#manual-frequency').dataTable();
            var data = table.row(baris).data();
            var g = xtable.fnGetData();
            var l = g.length;
            var d = xtable.fnGetData(g - 1);


            var last = $("#manual-frequency").find("tr:last td:first").text();
            //console.log('last ' + last);
            //Untuk Button Add Atas
            var oke = 0;
            console.log(l);
            if (l != 0) {
                nomor = (Number(last) + 10);
            }


            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control"  onchange="getConditionCode(this)"></select>';
            // var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control" id="due-date-frequency" name="due-date-frequency">';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';

            oTable.fnAddData([nomor,
                              condition,
                              dueDateFrequency,
                              netValueFrequency,
                              qtyFrequency,
                              unitFrequency,
                              '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                              '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                              conditionCode], true);
            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //Masking Net Value

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            $('input[name = "net-value-frequency"]').on('change', function () {
                var netValueFrequency = $('input[name = "net-value-frequency"]').val();
                var mask = sep1000(netValueFrequency, true);
                $('input[name = "net-value-frequency"]').val(mask);
            });

            $('input[name = "qty-frequency"]').inputmask({
                mask: "9",
                repeat: 10,
                greedy: !1
            });

            // Panggil variable yg handle combobox condition
            cond();
        });

        $('#manual-frequency').on('click', 'tr #btn-add-sama', function () {
            var table = $('#manual-frequency').DataTable();
            var data = table.row($(this).parents('tr')).data();

            var manual_no_sama = data[0];
            //Form input di data table manual-frequency
            var manualNo = '<input type="text" class="form-control" id="manual-no" name="manual-no">';
            var condition = '<select id="condition-frequency" name="condition-frequency" class="form-control" onchange="getConditionCode(this)"></select>';
            //var condition = '<input type="text" class="form-control" id="condition-frequency" name="condition-frequency">';
            var dueDateFrequency = '<input type="text" class="form-control" id="due-date-frequency" name="due-date-frequency">';
            var netValueFrequency = '<input type="text" class="form-control" id="net-value-frequency" name="net-value-frequency">';
            var qtyFrequency = '<input type="text" class="form-control" id="qty-frequency" name="qty-frequency">';
            var unitFrequency = '<select class="form-control" id="unit-frequency" name="unit-frequency"><option value="">-- Choose Unit --</option><option value="TON">TON</option></select>';
            var conditionCode = '<input type="text" class="form-control sembunyi" id="CONDITION_CODE" name="CONDITION_CODE">';


            oTable.fnAddData([manual_no_sama,
                              condition,
                              dueDateFrequency,
                              netValueFrequency,
                              qtyFrequency,
                              unitFrequency,
                              '<center><a class="btn default btn-xs green btn-add-sama" id="btn-add-sama"><i class="fa fa-plus"></i></a></center>',
                              '<center><a class="btn default btn-xs green" id="btn-savecus-frequency"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-frequency"><i class="fa fa-trash"></i></a></center>',
                              conditionCode], true);

            //Masking Due Date
            $('input[name="due-date-frequency"]').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });

            //$('input[name = "net-value-frequency"]').mask('000.000.000.000.000', { reverse: true });
            $('input[name = "net-value-frequency"]').on('change', function () {
                var netValueFrequency = $('input[name = "net-value-frequency"]').val();
                var mask = sep1000(netValueFrequency, true);
                $('input[name = "net-value-frequency"]').val(mask);
            });

            // Panggil variable yg handle combobox condition
            cond();
        });

        $('#manual-frequency').on('click', 'tr #btn-delcus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });

        $('#manual-frequency').on('click', 'tr #btn-savecus-frequency', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#manual-frequency').DataTable();
            var data = table.row(baris).data();

            var a = $('#condition-frequency').val();
            var b = $('#due-date-frequency').val();
            var c = $('#net-value-frequency').val();
            var d = $('#CONDITION_CODE').val();
            var e = $('#qty-frequency').val();
            var f = $('#unit-frequency').val();
            console.log(f);

            //console.log(baris);

            $('#manual-frequency').dataTable().fnUpdate(a, baris, 1, false);
            oTable.fnUpdate(b, baris, 2, false);
            oTable.fnUpdate(c, baris, 3, false);
            oTable.fnUpdate(e, baris, 4, false);
            oTable.fnUpdate(f, baris, 5, false);
            oTable.fnUpdate(d, baris, 8, false);

            // document.getElementById('btn-savecus').disabled = true;

            var elem = document.getElementById('btn-savecus-frequency');
            elem.parentNode.removeChild(elem);
            return false;

        });
    }

    //Var untuk menghandle data table memo
    var memo = function () {
        var tablem = $('#table-memo');
        var oTable = tablem.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "pageLength": 20,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false,
            "paging": false,
            "lengthChange": false,
            "bSort": false
        });

        $('#btn-add-memo').on('click', function () {
            $('#memo-condition').html('');
            $('#memo').val('');

            var fruits = buah;
            var listItems = "";
            listItems += "<option value=''>--Choose--</option>";
            for (var i = 0; i < fruits.length; i++) {
                listItems += "<option value='" + fruits[i] + "'>" + fruits[i] + "</option>";
            }
            //console.log(listItems);
            $("#memo-condition").append(listItems);

        });

        $('#btn-add-memo-md').on('click', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();

            var raw_val = $('#memo-condition').val();
            var v_split = raw_val.split("|");

            var memo_calc_object = v_split[0];
            var memo_cond_type = v_split[1];
            var memo = $('#memo').val();

            oTable.fnAddData([memo_calc_object,
                              memo_cond_type,
                              memo,
                             '<center><a class="btn default btn-xs red" id="btn-delcus-memo"><i class="fa fa-trash"></i></a></center>'], true);
            $('#memo').html('');
            $('#addMemo').modal('hide');
        });

        $('#table-memo').on('click', 'tr #btn-delcus-memo', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-memo').DataTable();
            var data = table.row(baris).data();
            oTable.fnDeleteRow(baris);
        });
    }

    var addSertifikat = function () {
        $('#btn-add-sertifikat').click(function () {
            $('#viewSertifikat').modal('show');
            $('#table-sertifikat').DataTable({
                "ajax":
                {
                    "url": "/TransContractOffer/GetDataSertifikat/",
                    "type": "GET",
                    //"data": function (data) {
                    //    delete data.columns;
                    //}

                },
                "columns": [
                    {
                        "data": "ATTACHMENT",
                        "class": "dt-body-center",
                    },
                    {
                        "render": function (data, type, full) {
                            var aksi = '<a id="btn-save-sertifikat" class="btn btn-icon-only green" title="Save" ><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });
        });

        $('#table-sertifikat').on('click', 'tr #btn-save-sertifikat', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-sertifikat').DataTable();
            var data = table.row(baris).data();
            console.log(data['ATTACHMENT']);
            //document.getElementById('SERTIFIKAT').innerHTML = data['ATTACMENT'];
            $('#SERTIFIKAT').val(data['ATTACHMENT']);
            //console.log(data)
            //var xInd = $('#INDUSTRY').val();
            //xoTable.fnAddData([data['RO_NUMBER'], data['OBJECT_ID'], data['OBJECT_TYPE_ID'], data['OBJECT_TYPE'], data['RO_NAME'], data['LUAS_TANAH_RO'], data['LUAS_BANGUNAN_RO'], '<select id="INDUSTRY" name="INDUSTRY" class="form-control"></select>', '<center><a class="btn default btn-xs green" id="btn-savecus-ob"><i class="fa fa-check"></i></a><a class="btn default btn-xs red" id="btn-delcus-ob"><i class="fa fa-trash"></i></a></center>'], true);
            //var nRow = $('#editable_transrental').DataTable();
            //var xC = nRow.data();
            //var z = (xC.count() / 9) - 2;
            //$('#editable_transrental').dataTable().fnUpdate(xInd, z, 7, false);

            $('#viewSertifikat').modal('hide');
        });
    }



    var simpanData = function () {
        $('#btn-simpan').click(function () {
            var table = $('#table-memo').dataTable();
            if (table.fnSettings().aoData.length === 0) {
                swal('Info', 'Please Fill Memo!', 'info');
            }
            else {
                // Header Data
                // Var Header Data
                var CONTRACT_TYPE = $('#CONTRACT_TYPE').val();
                var BE_ID = $('#BE_ID').val();
                var CONTRACT_NAME = $('#CONTRACT_NAME').val();
                var CONTRACT_START_DATE = $('#CONTRACT_START_DATE').val();
                var CONTRACT_END_DATE = $('#CONTRACT_END_DATE').val();
                var TERM_IN_MONTHS = $('#TERM_IN_MONTHS').val();
                var BUSINESS_PARTNER_NAME = $('#BUSINESS_PARTNER_NAME').val();
                var BUSINESS_PARTNER = $('#BUSINESS_PARTNER').val();
                var CUSTOMER_AR = $('#CUSTOMER_AR').val();
                var PROFIT_CENTER = $('#PROFIT_CENTER').val();
                var CONTRACT_USAGE = $('#CONTRACT_USAGE').val();
                var IJIN_PRINSIP_NO = $('#IJIN_PRINSIP_NO').val();
                var IJIN_PRINSIP_DATE = $('#IJIN_PRINSIP_DATE').val();
                var LEGAL_CONTRACT_NO = $('#LEGAL_CONTRACT_NO').val();
                var LEGAL_CONTRACT_DATE = $('#LEGAL_CONTRACT_DATE').val();
                var CURRENCY = $('#CURRENCY').val();
                var arrContractObjects;
                var arrContractConditions;
                var arrContractFrequencies;
                var arrContractMemos;

                //cek apakah pada condition terdapat kondisi biaya administrasi atau tidak
                var resultCek = 0;
                var DTConditionCek = $('#condition').dataTable();
                var countDTConditionCek = DTConditionCek.fnGetData();
                secondCellArray = [];
                for (var i = 0; i < countDTConditionCek.length; i++) {
                    var itemDTConditionCek = DTConditionCek.fnGetData(i);
                    secondCellArray.push(itemDTConditionCek[1]);
                }

                if (secondCellArray.indexOf("Z009 ADMINISTRASI") > -1) {
                    resultCek = 1;
                    console.log('ada');
                } else {
                    resultCek = 0;
                    console.log('tidak ada');
                }
                console.log(resultCek);

                if (CONTRACT_TYPE && BE_ID && CONTRACT_NAME && CONTRACT_START_DATE && CONTRACT_END_DATE && TERM_IN_MONTHS && BUSINESS_PARTNER_NAME && BUSINESS_PARTNER) {

                    // Declare dan getData dari dataTable OBJECT
                    var DTObject = $('#editable_transrental').dataTable();
                    var countDTObject = DTObject.fnGetData();
                    arrContractObjects = new Array();
                    for (var i = 0; i < countDTObject.length; i++) {
                        var itemDTObject = DTObject.fnGetData(i);
                        var paramDTObject = {
                            OBJECT_ID: itemDTObject[1],
                            OBJECT_TYPE: itemDTObject[3],
                            RO_NAME: itemDTObject[4],
                            LUAS_TANAH: itemDTObject[5],
                            LUAS_BANGUNAN: itemDTObject[6],
                            INDUSTRY: itemDTObject[7]
                        }
                        arrContractObjects.push(paramDTObject);
                    }
                    // End Declare dan getData dari dataTable OBJECT

                    // start getData dari dataTable CONDITION
                    var DTCondition = $('#condition').dataTable();
                    var countDTCondition = DTCondition.fnGetData();
                    arrContractConditions = new Array();
                    for (var i = 0; i < countDTCondition.length; i++) {
                        var itemDTCondition = DTCondition.fnGetData(i);

                        // hitung pembagian total net value berdasarkan frekuensi pembayaran
                        var freq_pembayaran = itemDTCondition[8];
                        var net_value = itemDTCondition[18];
                        var formulanya = itemDTCondition[11];
                        var jumlah_bulan = itemDTCondition[4];
                        var pecah_bayar = 0;

                        // Cek apakah frekuensi pembayaran bulanan atau tahunan
                        if (freq_pembayaran == 'YEARLY') {
                            var tahunan = Number(jumlah_bulan) / 12;
                            pecah_bayar = Number(net_value) / Number(tahunan);
                            console.log('tahunan ' + pecah_bayar);
                        }
                        else if (freq_pembayaran == 'MONTHLY') {
                            var bulanan = jumlah_bulan;
                            pecah_bayar = Number(net_value) / Number(bulanan);
                            console.log('bulanan ' + pecah_bayar);
                        }
                        else {
                            pecah_bayar = Number(net_value);
                            console.log('else ' + pecah_bayar);
                        }

                        // Cek apakah status condition yes atau no
                        var status_condition = itemDTCondition[5];
                        var nilai_condition = 0;
                        if (status_condition == "Yes") {
                            nilai_condition = 1;
                        }
                        else {
                            nilai_condition = 0;
                        }

                        var rUnitPrice = itemDTCondition[6];
                        var nfUnitPrice = parseFloat(rUnitPrice.split(',').join(''));

                        var rTotal = itemDTCondition[14];
                        var nfTotal = parseFloat(rTotal.split(',').join(''));

                        var rTotalNetValue = itemDTCondition[17];
                        var nfTotalNetValue = parseFloat(rTotalNetValue.split(',').join(''));

                        var paramDTCondition = {
                            CALC_OBJECT: itemDTCondition[0],
                            CONDITION_TYPE: itemDTCondition[1],
                            VALID_FROM: itemDTCondition[2],
                            VALID_TO: itemDTCondition[3],
                            MONTHS: itemDTCondition[4],
                            STATISTIC: nilai_condition.toString(),
                            UNIT_PRICE: nfUnitPrice.toString(),
                            AMT_REF: itemDTCondition[7],
                            FREQUENCY: itemDTCondition[8],
                            START_DUE_DATE: itemDTCondition[9],
                            MANUAL_NO: itemDTCondition[10],
                            FORMULA: itemDTCondition[11],
                            MEASUREMENT_TYPE: itemDTCondition[12],
                            LUAS: itemDTCondition[13],
                            TOTAL: nfTotal.toString(),
                            NJOP_PERCENT: itemDTCondition[15],
                            KONDISI_TEKNIS_PERCENT: itemDTCondition[16],
                            TOTAL_NET_VALUE: nfTotalNetValue.toString(),
                            TAX_CODE: itemDTCondition[20],
                            INSTALLMENT_AMOUNT: pecah_bayar.toString(),
                            COA_PROD: itemDTCondition[18]
                        };
                        arrContractConditions.push(paramDTCondition);
                    }
                    //END PUSH CONDITION
                    //Declare dan get data dari dataTable MANUAL FREQUENCY
                    var DTManualFrequency = $('#manual-frequency').dataTable();
                    var countDTManualFrequency = DTManualFrequency.fnGetData();
                    arrContractFrequencies = new Array();
                    for (var i = 0; i < countDTManualFrequency.length; i++) {
                        var itemDTManualFrequency = DTManualFrequency.fnGetData(i);

                        //potong2  
                        var raw_manual_freq = itemDTManualFrequency[1];
                        var splitselectedCondition = raw_manual_freq.split(" | ");
                        var code_lengkap = splitselectedCondition[1];
                        var cut = code_lengkap.substring(0, 4);
                        console.log(cut);

                        var splitChar = raw_manual_freq.split(" | ");
                        var objectId = splitChar[0];

                        var net_value = itemDTManualFrequency[3];
                        var nfNetVal = parseFloat(net_value.split(',').join(''));
                        var paramDTManualFrequency = {
                            MANUAL_NO: itemDTManualFrequency[0].toString(),
                            CONDITION: cut, //Untuk condition ambil id CONDITION_CODE di index 9 yg di hidden
                            DUE_DATE: itemDTManualFrequency[2],
                            QUANTITY: itemDTManualFrequency[4],
                            UNIT: itemDTManualFrequency[5],
                            NET_VALUE: nfNetVal.toString(),
                            OBJECT_ID: objectId
                        }
                        arrContractFrequencies.push(paramDTManualFrequency);
                    }
                    //END PUSH FREQUENCY
                    //Declare dan get data dari dataTable MEMO
                    var DTMemo = $('#table-memo').dataTable();
                    var countDTMemo = DTMemo.fnGetData();
                    arrContractMemos = new Array();
                    for (var i = 0; i < countDTMemo.length; i++) {
                        var itemDTMemo = DTMemo.fnGetData(i);
                        var paramDTMemo = {
                            OBJECT_CALC: itemDTMemo[0],
                            CONDITION_TYPE: itemDTMemo[1],
                            MEMO: itemDTMemo[2]
                        }
                        arrContractMemos.push(paramDTMemo);
                    }
                    var param = {
                        CONTRACT_TYPE: CONTRACT_TYPE,
                        BE_ID: BE_ID,
                        CONTRACT_NAME: CONTRACT_NAME,
                        CONTRACT_START_DATE: CONTRACT_START_DATE, 
                        CONTRACT_END_DATE: CONTRACT_END_DATE,
                        TERM_IN_MONTHS: TERM_IN_MONTHS,
                        BUSINESS_PARTNER_NAME: BUSINESS_PARTNER_NAME,
                        BUSINESS_PARTNER: BUSINESS_PARTNER,
                        CUSTOMER_AR: CUSTOMER_AR,
                        PROFIT_CENTER: PROFIT_CENTER,
                        CONTRACT_USAGE: CONTRACT_USAGE,
                        IJIN_PRINSIP_NO: IJIN_PRINSIP_NO,
                        IJIN_PRINSIP_DATE: IJIN_PRINSIP_DATE,
                        LEGAL_CONTRACT_NO: LEGAL_CONTRACT_DATE,
                        LEGAL_CONTRACT_DATE: LEGAL_CONTRACT_DATE,
                        CURRENCY: CURRENCY,  
                    
                        Objects: arrContractObjects,
                        Conditions: arrContractConditions,
                        Frequencies: arrContractFrequencies,
                        Memos: arrContractMemos
                    };
                    // Ajax Save Data Header
                    App.blockUI({ boxed: true });
                    $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/TransSpecialContract/SaveHeader",

                        success: function (data) {
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location = "/TransSpecialContract";
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransSpecialContract";
                                });
                            }
                            App.unblockUI();
                        },
                        error: function (data) {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/TransSpecialContract";
                            });
                        }
                    });
                    arrContractObjects = [];
                    arrContractConditions = [];
                    arrContractFrequencies = [];
                    arrContractMemos = [];
                }
                else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    return {
        init: function () {
            mdm();
            hitung_bulan();
            hitung_tanggal_sampai();
            batal();
            filter();
            addCondition();
            manual_frequency();
            memo();
            simpanData();
            addSertifikat();
        }
    };
}();


// ************** FUNCTION UNTUK HANDLE SALES RULE ********************
//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getUnitST(this_cmb) {
    var selectedSalesType = $(this_cmb).val();
    var splitselectedSalesType = selectedSalesType.split(" - ");
    var id_sales_type = splitselectedSalesType[0];
    var nama_sales_type = splitselectedSalesType[1];

    //Ajax untuk get UNIT
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataUnitST/" + id_sales_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].UNIT;
            }
            $(this_cmb).parents('tr').find('input[name="UNIT_ST"]').val(listItems);
        }
    });
}
// ************** END FUNCTION UNTUK HANDLE SALES RULE ********************

//Function untuk isi condition code
function getConditionCode(this_cmb) {
    var selectedCondition = $(this_cmb).val();

    if (selectedCondition) {
        var splitselectedCondition = selectedCondition.split(" | ");
        var code_lengkap = splitselectedCondition[1];
        var cut = code_lengkap.substring(0, 4);
        console.log(selectedCondition);
        console.log(code_lengkap);
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val(cut);
    }
    else {
        $(this_cmb).parents('tr').find('input[name="CONDITION_CODE"]').val("");
    }
}

//Function untuk add row table manual frequency 
function addManualFreq(this_x) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var tim = data[4];
}

//ACTION ON CLICK MEAS TYPE UNTUK MENGISI LUAS(M2)
function getLuas(this_cmb) {
    console.log($(this_cmb).val());
    var selectedMeasType = $(this_cmb).val();
    var splitselectedMeasType = selectedMeasType.split("|");
    var id_detail_meas_type = splitselectedMeasType[1];

    //Ajax untuk get Luas(M2)
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataLuasMeasType/" + id_detail_meas_type,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems = jsonList.data[i].LUAS;
            }
            // $("#luas").val(listItems);
            $(this_cmb).parents('tr').find('input[name="luas"]').val(listItems);


            //Mengisi field total value saat field luas terisi dengan value ajax listItems
            var baris = $(this).parents('tr')[0];
            var table = $('#condition').DataTable();
            var data = table.row(baris).data();

            //formulanya
            var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
            if (formula == 'A') {

            }
            else if (formula == 'U') {

            }
            else {
                //var tim = data[4];
                var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
                var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
                var luas = listItems;
                var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

                nMUnitPrice = parseFloat(unitPrice.split(',').join(''));
                var t = hitungTotal(tim, luas, nMUnitPrice, period);
                var fT = Math.round(t).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

                //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
                var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
                var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
                var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
                var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

                var nfTotal = parseFloat(total.split(',').join(''));
                var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
                var nfPersenFromKondisiteknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));


                var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiteknis);
                var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                console.log(fNetValue);
                $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
            }
        }
    });
}


//---------------------------HITUNG TOTAL----------------------------
//ACTION UNTUK MENGISI TOTAL VALUE SAAT CLICK COMBO AMT REF
function isiTotalAMTREF(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var period = $(this_cmb).val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);

    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        //Hitung Total Untuk Formula D
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(unit_price.split(',').join(''));
        var rTotal = hitungTotal(tim, luas, nfHarga, kode_periode);

        var fTotal = rTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fTotal);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = netVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}

function isiTotalUnitPrice(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    //var tim = data[4];
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split(',').join(''));
    var nfLuas = parseFloat(luas.split(',').join(''));

    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();

    if (formula == 'A') {
        var fUnitPrice = Math.round(unit_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
    }
    else if (formula == 'D') {
        var fT = t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fT);

        var njop;
        var kondisi_teknis;

        // update total
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        console.log('total bulan ' + total_bulan);
        console.log('nf harga ' + nfHarga);
        console.log('luas ' + luas);
        console.log('kode periode ' + kode_periode);

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = htTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        console.log('formula ' + formula);
        console.log('njop ' + njop);
        console.log('kondisi teknis ' + persenFromKondisiTeknis);

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {

    }
}
//---------------------END OF HITUNG TOTAL----------------------------

function hitungTotal(total_bulan, luas, harga, kode_periode) {
    var total = 0;
    if (kode_periode == 'YEARLY') {
        var year = Number(total_bulan) / 12;
        total = (Number(luas) * Number(harga)) * Number(year);
    }
    else if (kode_periode == 'MONTHLY') {
        total = (Number(luas) * Number(harga)) * Number(total_bulan);
    }
    else {
        total = Number(luas) * Number(harga);
    }
    return total;
}

//Action Saat Formula Dipilih
function hitungFormula(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = parseFloat(persen_from_njop.split(',').join(''));
    var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split(',').join(''));

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var unit_price = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();

    var amt_ref = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();

    var nfUnitPrice = parseFloat(unitPrice.split('.').join(''));
    var nfLuas = parseFloat(luas.split('.').join(''));
    var t = hitungTotal(tim, nfLuas, nfUnitPrice, period);


    if (formula == 'A') {
        //var fUnitPrice = unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        // $(this_cmb).parents('tr').find('input[name="total"]').val(fUnitPrice);
        // $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fUnitPrice);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'D') {
        //hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
    }
    else if (formula == 'E1') {
        //Formula Lama
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);

        //netValue(formula, total, njop, kondisi_teknis)
        var njop;
        var persenFromKondisiTeknis;
        var tot = $(this_cmb).parents('tr').find('input[name="total"]').val();

        //Hitung Total
        //hitungTotal(total_bulan, luas, harga, kode_periode)
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var nfHarga = parseFloat(harga.split(',').join(''));

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);
        //console.log('ht total ' + htTotal);
        //Hitung Total Net Value
        njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
        persenFromKondisiTeknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

        //netValue(formula, total, njop, kondisi_teknis)
        var netVal = netValue(formula, htTotal, njop, persenFromKondisiTeknis);
        console.log('net val ' + netVal);

        var fNetVal = Math.round(netVal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetVal);
    }
    else {
        var total_bulan = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
        var luas = 1;
        var harga = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
        var nfHarga = parseFloat(harga.split(',').join(''));
        var kode_periode = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

        var htTotal = hitungTotal(total_bulan, luas, nfHarga, kode_periode);
        var fHtTotal = Math.round(htTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this_cmb).parents('tr').find('input[name="total"]').val(fHtTotal);

        $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fHtTotal);
        //var fNetValue = net_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //$(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
    }
}

//Action Saat Formula sudah dipilih dan mengisi persentase njop
function hitungFormulaN(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var nfTotal = parseFloat(total.split(',').join(''));
    //var nfPersenFromNjop = parseFloat(persen_from_njop.split('.').join(''));
    var nfPersenFromNjop = persen_from_njop;
    var nfPersenFromKondisiTeknis = 0;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    console.log('net value ' + net_value);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}

//Action Saat Formula sudah dipilih dan mengisi persentase kondisi teknis
function hitungFormulaT(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).val();

    var nfTotal = parseFloat(total.split(',').join(''));
    var nfPersenFromNjop = 0;
    //var nfPersenFromKondisiTeknis = parseFloat(persen_from_kondisi_teknis.split('.').join(''));
    var nfPersenFromKondisiTeknis = persen_from_kondisi_teknis;

    var net_value = netValue(formula, nfTotal, nfPersenFromNjop, nfPersenFromKondisiTeknis);
    var fNetValue = Math.round(net_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // ini masih kurang sip
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(fNetValue);
}



//Function untuk hitung net value berdasarkan formula yang dipilih
function netValue(formula, total, njop, kondisi_teknis) {
    var totalNetValue = 0;
    if (formula == 'D') {
        totalNetValue = Number(total);
    }
    else if (formula == 'E1') {
        if (njop == '' && kondisi_teknis == '') {
            totalNetValue = Number(total);
        }
        else if (njop == '') {
            totalNetValue = Number(total) * (Number(kondisi_teknis) / 100);
        }
        else if (kondisi_teknis == '') {
            totalNetValue = Number(total) * (Number(njop) / 100);
        }
        else {

        }
    }
    else if (formula == 'A') {
        totalNetValue = Number(total);
    }
    else {

    }

    return totalNetValue;
}

// Function untuk remove array value berdasarkan value
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function hitungTermMonths(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val();

    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[1] + '/' + datearrayMulai[0] + '/' + datearrayMulai[2];

    var dateAkhir = t;
    var datearrayAkhir = dateAkhir.split("/");
    var newdateAkhir = datearrayAkhir[1] + '/' + datearrayAkhir[0] + '/' + datearrayAkhir[2];

    var tim = monthDiff2(new Date(newdateMulai), new Date(newdateAkhir));

    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val(tim);

    // handle perhitungannya
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var tim2 = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tx = hitungTotal(tim2, luas, unitPrice, period);
    $(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function hitungEndDate(this_cmb) {
    var baris = $(this).parents('tr')[0];
    var table = $('#condition').DataTable();
    var data = table.row(baris).data();

    var f = $(this_cmb).parents('tr').find('input[name="start-date"]').val();
    var tm = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var dateMulai = f;
    var datearrayMulai = dateMulai.split("/");
    var newdateMulai = datearrayMulai[2] + ',' + datearrayMulai[1] + ',' + datearrayMulai[0];

    //Set Tanggal Berdasarkan Tanggal Mulai dan Jumlah Bulan
    var f_date = new Date(newdateMulai);
    var n_tim = Number(tm);
    var end_date = addMonths2(f_date, n_tim);
    var xdate = new Date(end_date);
    var f_date = xdate.toISOString().slice(0, 10);

    //format f_date menjadi dd/mm/YYYY
    var dateAkhir = f_date;
    var datearrayAkhir = dateAkhir.split("-");
    var newdateAkhir = datearrayAkhir[2] + '/' + datearrayAkhir[1] + '/' + datearrayAkhir[0];

    var t = $(this_cmb).parents('tr').find('input[name="end-date"]').val(newdateAkhir);

    // handle perhitungannya
    var luas = $(this_cmb).parents('tr').find('input[name="luas"]').val();
    var tim = $(this_cmb).parents('tr').find('input[name="term-months"]').val();
    var unitPrice = $(this_cmb).parents('tr').find('input[name="unit-price"]').val();
    var period = $(this_cmb).parents('tr').find('select[name="amt-ref"]').val();

    var tx = hitungTotal(tim, luas, unitPrice, period);
    $(this_cmb).parents('tr').find('input[name="total"]').val(tx);

    //ini net value ketika formula sudah dipilih duluan sebelum mengisi meas type
    var formula = $(this_cmb).parents('tr').find('select[name="formula"]').val();
    var total = $(this_cmb).parents('tr').find('input[name="total"]').val();
    var persen_from_njop = $(this_cmb).parents('tr').find('input[name="persen-from-njop"]').val();
    var persen_from_kondisi_teknis = $(this_cmb).parents('tr').find('input[name="persen-from-kondisi-teknis"]').val();

    var net_value = netValue(formula, total, persen_from_njop, persen_from_kondisi_teknis);
    $(this_cmb).parents('tr').find('input[name="total-net-value"]').val(net_value);
}

function monthDiff2(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();

    if (d2.getDate() >= d1.getDate())
        months++

    return months <= 0 ? 0 : months;
}

function addMonths2(date, count) {
    if (date && count) {
        var m, d = (date = new Date(+date)).getDate()

        date.setMonth(date.getMonth() + count, 1)
        m = date.getMonth()
        date.setDate(d)
        if (date.getMonth() !== m) date.setDate(0)
    }
    return date
}


// Function untuk handle save Sales Rule (Formula U)
function saveSalesRule() {
    // Untuk get data table condition

    // Untuk ambil sales rule

    // 
}

function sep1000(somenum, usa) {
    var dec = String(somenum).split(/[.,]/)
       , sep = usa ? ',' : '.'
       , decsep = usa ? '.' : ',';

    return xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');

    function xsep(num, sep) {
        var n = String(num).split('')
           , i = -3;
        while (n.length + i > 0) {
            n.splice(i, 0, sep);
            i -= 4;
        }
        return n.join('');
    }
}


//Function untuk isi condition code
function getCoaProduksi() {
    $.ajax({
        type: "GET",
        url: "/TransContractOffer/GetDataDropDownCoaProduksi",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var jsonList = data
            var listItems = "";
            listItems += "<option value=''>-- Chose COA Produksi --</option>";
            for (var i = 0; i < jsonList.data.length; i++) {
                listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
            }
            $(".coa_produksi").html('');
            $(".coa_produksi").append(listItems);
        }
    });
}

// Function untuk remove array value berdasarkan value
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

function infoIsiManual() {
    var valFrequency = $('#frequency').val();

    if (valFrequency == 'MANUAL') {
        swal('Info', 'Mohon Mengisi \'MANUAL NO\' Untuk Pilihan Manual!', 'info');
    }
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransSpecialContract.init();
    });
}

jQuery(document).ready(function () {
    $('#condition > tbody').html('');
    $('#condition-option > option').html('');
    $('.sembunyi').hide();
});