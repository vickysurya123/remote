﻿var MonitoringPembayaran = function () {

    var CBBusinessEntity = function () {
        // Ajax untuk ambil tarif dengan code L
        $.ajax({
            type: "GET",
            url: "/MonitoringPembayaran/GetDataBusinessEntity",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var jsonList = data
                var listItems = "";
                listItems += "<option value=''>-- Choose Business Entity --</option>";
                for (var i = 0; i < jsonList.data.length; i++) {
                    listItems += "<option value='" + jsonList.data[i].BRANCH_ID + "'>" + jsonList.data[i].BE_NAME + "</option>";
                }
                $("#BE_NAME").html('');
                $("#BE_NAME").append(listItems);
            }
        });
    }

    var handleBootstrapSelect = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }
    var showFilter = function () {
        $('#btn-filter').click(function () {
            App.blockUI({ boxed: true });
            var tagihan = $('#JENIS_TAGIHAN').val();
            var nota_ = $('#NOTA_TYPE').val();
            var be = $('#BE_NAME').val();
            var text_ = ($('#JENIS_TAGIHAN').val() == 'PROPERTI' ? 'NO. KONTRAK' : ($('#JENIS_TAGIHAN').val() == 'AIR' ? 'NO. INSTALASI': 'KELOMPOK PENDAPATAN'));
            $('#text_').text(text_);
            if (tagihan != "") {
                $('#row-table').css('display', 'block');
                if ($.fn.DataTable.isDataTable('#table-report')) {
                    $('#table-report').DataTable().destroy();
                }
                $('#table-report').DataTable({
                    "ajax": {
                        "url": "/MonitoringPembayaran/GetDataFilterNew",
                        "type": "GET",
                        "data": function (data) {
                            delete data.columns;
                            data.tipe = $('#NOTA_TYPE').val();
                            data.jenis_tagihan = $('#JENIS_TAGIHAN').val();
                            data.be = $('#BE_NAME').val();
                        },
                        "dataSrc": function (json) {
                            //Make your callback here.
                            $('#btn-cetak-exel').show('slow');
                            return json.data;
                        }
                    },
                    "columns": [
                        {                          
                            "render": function (data, type, full) {
                                if (full.JENIS_TAGIHAN === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.JENIS_TAGIHAN + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.BILLING_NO === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.BILLING_NO + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.CODE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.CODE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.CUSTOMER_CODE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.CUSTOMER_CODE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.CUSTOMER_NAME === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.CUSTOMER_NAME + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.PSTNG_DATE === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.PSTNG_DATE + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.TGL_LUNAS === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.TGL_LUNAS + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.TGL_BATAL === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> ' + full.TGL_BATAL + ' </span>';
                                }
                            },
                            "class": "dt-body-center"
                        },
                        {
                            "render": function (data, type, full) {
                                if (full.DOC_NUMBER === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span>' + full.DOC_NUMBER + ' </span>';
                                }
                            },
                            "class": "dt-body-right"
                        },
                        {                            
                            "render": function (data, type, full) {
                                if (full.AMOUNT === null) {
                                    return '<span> - </span>';
                                } else {
                                    return '<span> Rp. ' + full.AMOUNT + ' </span>';
                                }
                            },
                            "class": "dt-body-right"
                        },
                        
                        {
                            "render": function (data, type, full) {
                                var str = "";

                                if (full.STATUS_LUNAS === 'Nota Belum Lunas') {
                                    str += '<span class="label label-sm label-danger"> ' + full.STATUS_LUNAS + ' </span>';
                                } else if (full.STATUS_LUNAS === 'Nota Lunas') {
                                    str += '<span class="label label-sm label-success"> ' + full.STATUS_LUNAS + ' </span>';
                                } else {
                                    str += '<span class="label label-sm label-info"> ' + full.STATUS_LUNAS + ' </span>';
                                }
                                return str;
                            },
                            "class": "dt-body-right"
                        },
                                                          
                    ],
                    "destroy": true,
                    "ordering": false,
                    "processing": true,
                    "serverSide": true,
                    "responsive": false,

                    "lengthMenu": [
                        [5, 10, 15, 20, 50],
                        [5, 10, 15, 20, 50]
                    ],
                    "pageLength": 10,

                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                    },

                    "filter": true
                    
                });
                App.unblockUI();
                $('#btn-cetak_exel').show('slow');
            }
            else {
                swal('Warning', 'Please Select Type of Bill !', 'warning');
                App.unblockUI();
            }

        });
    }

    return {
        init: function () {
            //select2BEID();
            //select2HARBOURCLASS();
            showFilter();
            //handleBootstrapSelect();
            CBBusinessEntity();
        }
    };
}();

var fileExcels;
function defineExcel() {
    App.blockUI({ boxed: true });
    var BUSINESS_ENTITY = $('#BE_NAME').val();
    var JENIS_TAGIHAN = $('#JENIS_TAGIHAN').val() == undefined ? null : $('#JENIS_TAGIHAN').val();
    var NOTA_TYPE = $('#NOTA_TYPE').val() == undefined || $('#NOTA_TYPE').val() == null ? null : $('#NOTA_TYPE').val();

    var param = {
        BUSINESS_ENTITY: BUSINESS_ENTITY,
        JENIS_TAGIHAN: JENIS_TAGIHAN,
        NOTA_TYPE: NOTA_TYPE
    };
    $.ajax({
        type: "POST",
        url: "/MonitoringPembayaran/defineExcel",
        data: JSON.stringify(param),
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (data) {
            fileExcels = data.data;
            App.unblockUI();
            swal({
                title: "File Successfully Generated",
                text: "[" + data.data + "]" +
                      '<br /><br />' + '<a href="../REMOTE/Excel/' + data.data + '" data-excels=' + data.data + ' target="_blank" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">DOWNLOAD FILE</button></a>' +
                      ' <a href="#" onClick="return Clicks()"><button type="button" class="btn yellow-crusta">Cancel</button></a>',
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
        },
        error: function (data) {
        }
    });
}

function Clicks() {
    var jsonData = "{fileName:'" + fileExcels + "'}";
    setTimeout(function () {
        deleteExcel(JSON.stringify(fileExcels));
    }, 2000);
}

function deleteExcel(jsonData) {
    $.ajax({
        type: "POST",
        url: "/MonitoringPembayaran/deleteExcel",
        data: jsonData,
        contentType: "application/json; charset-utf8",
        datatype: "jsondata",
        async: "true",
        success: function (response) {
            console.log("Sukses");
        },
        error: function (response) {
            console.log(response);
        }
    });
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MonitoringPembayaran.init();
    });
}

jQuery(document).ready(function () {
    //$('#table-report > tbody').html('');
});

//function exportExcel() {
//    $.ajax({
//        type: "POST",
//        url: "/ReportContractOffer/exportExcel",
//        data: "{}",
//        contentType: "application/json; charset-utf8",
//        datatype: "jsondata",
//        async: "true",
//        timeout: 100000,
//        success: function (response) {
//            var valStrings = response.substring(2, response.length - 2);
//            window.location.href = "../../REMOTE/Excel/" + valStrings + "";

//            var jsonData = "{fileName:'" + valStrings + "'}";
//            setTimeout(function () {
//                deleteExcel(jsonData);
//            }, 2000);
//        },
//        error: function (response) {
//            console.log(response);
//        }
//    });
//}