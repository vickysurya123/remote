﻿var TransWaterElectricity = function () {
    var Base64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = Base64._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = Base64._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } }

    var maskMeterTo = function () {
        $('METER_TO').inputmask({
            mask: '9',
            repeat: 10,
            greedy: !1
        });
    }

    var maskMeterFrom = function () {
        $("#METER_FROM").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        });
    }
    
    
    var initTableWETransaction = function () {
        $('#table-transwe').DataTable({

            "ajax":
            {
                "url": "TransWEList/GetDataWETransListNew",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                },
            },

            "columns": [
                /*
                {
                    "render": function(data, type, full){
                        var check = "<label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'><input type='checkbox' class='checkboxes' value='1' /><span></span></label>";
                        return check;
                    },
                    "class": "dt-body-center"

                        
                },*/
                {
                    "data": "ID",
                    "class": "dt-body-center"
                },
                {
                    "data": "INSTALLATION_TYPE"

                },
                {
                    "data": "PROFIT_CENTER"

                },
                {
                    "data": "CUSTOMER_NAME"

                },
                {
                    "data": "POWER_CAPACITY"

                },
                {
                    "data": "PERIOD",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return '<span> USD </span>';
                        } else {
                            return '<span> IDR </span>';
                        }
                    },
                },
                {
                    "data": "AMOUNT",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                
                {
                    "data": "SURCHARGE",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "render": function (data, type, full) {
                        if (full.RATE > 1) {
                            return full.GRAND_TOTAL_USD;
                        } else {
                            return " - ";
                        }
                    },
                    "class": "dt-body-right",
                    //render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "GRAND_TOTAL",
                    "class": "dt-body-right",
                    render: $.fn.dataTable.render.number('.', '.', 0)
                },
                {
                    "data": "POSTING_DATE",
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.STATUS_DINAS === null) {
                            return '<span> - </span>';
                        } else {
                            return '<span> ' + full.STATUS_DINAS + ' </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        if (full.SAP_DOCUMENT_NUMBER != null ) {
                            return '<span class="label label-sm label-success"> BILLED (' + full.SAP_DOCUMENT_NUMBER + ')</span>';
                        } else if (full.STATUS_DINAS === 'NON KEDINASAN') {
                            return '<span class="label label-sm label-danger"> NOT BILLED YET </span>';
                            //return '<span> - </span>';
                        }
                            else {
                            //return '<span class="label label-sm label-danger"> NOT BILLED YET </span>';
                            return '<span> - </span>';
                        }
                    },
                    "class": "dt-body-center"
                },
                 {
                     "render": function (data, type, full) {
                             if (full.CANCEL_STATUS != null) {
                                 return '<span class="badge badge-danger"> CANCELED </span>';
                             }
                             else {
                                 return '';
                             }
                         
                     },
                     "class": "dt-body-center"
                 },
                {
                    "render": function (data, type, full) {
                        var aksi = '';
                        //aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';

                        if (full.SAP_DOCUMENT_NUMBER != null) {
                            aksi += '<a class="btn btn-icon-only blue" id="btn-ubahx"><i class="fa fa-edit"></i></a>';
                            
                            if (full.INSTALLATION_TYPE == 'L-Sambungan Listrik') {
                                aksi += '<a data-toggle="modal" href="#viewModalListrik" class="btn btn-icon-only green" id="btn-detail-listrik"><i class="fa fa-eye"></i></a>';
                                //aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                                aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';

                            }
                            else {
                                aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                                aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota-air" title="Pranota"><i class="fa fa-print"></i></a>';

                            }
                        }
                        else {
                            aksi += '<a class="btn btn-icon-only blue" id="btn-ubah"><i class="fa fa-edit"></i></a>';
                            //aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';

                            if (full.INSTALLATION_TYPE == 'L-Sambungan Listrik') {
                                aksi += '<a data-toggle="modal" href="#viewModalListrik" class="btn btn-icon-only green" id="btn-detail-listrik"><i class="fa fa-eye"></i></a>';
                                //aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                                aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota" title="Pranota"><i class="fa fa-print"></i></a>';

                            }
                            else {
                                aksi += '<a data-toggle="modal" href="#viewModal" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                                aksi += '<a class="btn btn-icon-only yellow" id="btn-pranota-air" title="Pranota"><i class="fa fa-print"></i></a>';

                            }
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
                

            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "responsive": false,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "filter": true
        });       
        
    }       

    var hitung = function () {
        $('#SURCHARGE').mask('000.000.000.000.000', { reverse: true });
        $('#PRICE').mask('000.000.000.000.000', { reverse: true });
        $('#AMOUNT').mask('000.000.000.000.000', { reverse: true });
        $('#GRAND_TOTAL').mask('000.000.000.000.000', { reverse: true });
        var currency = $('#CURRENCY').val();
        $('#METER_TO').on('keyup', function (e) {
            var prc = $('#PRICE').val();
            var nfPrc = parseFloat(prc.split('.').join(''));

            var surcharge = $('#SURCHARGE').val();
            console.log('surcharge '+surcharge);

            //var m_factor = $('#MULTIPLY_FACTOR').val();
            var total = 0;

            var to = $('#METER_TO').val();
            var from = $('#METER_FROM').val();
            var qtyUsed = Number(to) - Number(from);
            var minQty = $('#MIN_QUANTITY').val();

            if (qtyUsed <= minQty) {
                total = Number(minQty) * Number(nfPrc);
            }
            else {
                total = Number(nfPrc) * Number(qtyUsed);
            }

            var nfSurcharge = parseFloat(surcharge.split('.').join(''));
            var totalBulat = Math.round(total);
            var grandTotal = Number(totalBulat) + Number(nfSurcharge);
            var grandTotal = grandTotal;


            if (currency == 'USD') {
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataCurrencyRate",
                    contentType: "application/json",
                    dataType: "json",
                    data: {},
                    success: function (data) {
                        rate = data.Data;
                        totalBulatUsd = totalBulat * rate;
                        grandTotalUsd = grandTotal * rate;
                        var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var fTotalBulatUsd = totalBulatUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        
                        $("#AMOUNT").val(fTotalBulatUsd);
                        $("#GRAND_TOTAL").val(fGrandTotalUsd);
                        $("#SUB_TOTAL_USD").val(fTotalBulat);
                        $("#GRAND_TOTAL_USD").val(fGrandTotal);
                        $('#USD').show();
                        $('#RATE').val(rate);
                    }
                });
            } else {
                var fTotalBulat = totalBulat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                $("#AMOUNT").val(fTotalBulat);
                $("#GRAND_TOTAL").val(fGrandTotal);
                $('#USD').hide();
                $('#RATE').val(1);
            }

            $("#KWH").val(qtyUsed);
        });

        $('#SURCHARGE').on('keyup', function (e) {
            var surcharge = $('#SURCHARGE').val();
            var nfSurcharge = parseFloat(surcharge.split('.').join(''));
            var amount = $('#AMOUNT').val();
            var nfAmount = parseFloat(amount.split('.').join(''));

            var grandTotal = Number(nfSurcharge) + Number(nfAmount);
            //var grandTotal = Number(nfAmount);
            if (currency == 'USD') {
                $.ajax({
                    type: "GET",
                    url: "/TransContractOffer/GetDataCurrencyRate",
                    contentType: "application/json",
                    dataType: "json",
                    data: {},
                    success: function (data) {
                        rate = data.Data;
                        grandTotalUsd = grandTotal * rate;
                        var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                        $("#GRAND_TOTAL").val(fGrandTotalUsd);
                        $("#GRAND_TOTAL_USD").val(fGrandTotal);
                        $('#USD').show();
                        $('#RATE').val(rate);
                    }
                });
            } else {
                var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                $("#GRAND_TOTAL").val(fGrandTotal);
                $('#USD').hide();
                $('#RATE').val(1);
            }
        });
    }

    var ubah = function () {
        $('body').on('click', 'tr #btn-ubah', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var idTransaksi = data["ID"];
            var installationType = data["INSTALLATION_TYPE"];
            var installationNumber = data["INSTALLATION_NUMBER"];
            console.log(installationNumber);
            console.log('id transaksi ' + idTransaksi);

            //console.log(installationType);
            if (installationType == 'A-Sambungan Air') {
                window.location = "/TransWEList/Edit/" + data["ID"];
            }
            else {
                //swal('Info', 'Edit transaksi listrik akan segera tersedia, mohon maaf atas ketidaknyamanannya saat ini.', 'info')
                var insNumber = data["INSTALLATION_NUMBER"];
                var transNumber = data["ID"];
                console.log('Trans Number ' + idTransaksi);
                
                var enInstNumber = Base64.encode(insNumber);
                var enTransnumber = Base64.encode(transNumber);
                console.log('inst Number ' + enInstNumber);
                console.log('trans Number ' + enTransnumber);
                
                window.location = "/TransWEList/EditElectricity/?xId=" + data["INSTALLATION_ID"] + '&xTd=' + data["ID"];
                //window.location.href = "/TransWEList/EditElectricity/?inumb=" + insNumber + "&tnumb=" + transNumber;
            }

        });
    }

    var initDetilTransaction = function () {
        $('body').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTrans = $('#table-detil-transaction').DataTable();
            detailTrans.destroy();

            //16 items
            $('#table-detil-transaction').DataTable({
                
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionWater",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "CUSTOMER_NAME",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "QUANTITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "UNIT",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PRICE", 
                        render: $.fn.dataTable.render.number( '.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SUB_TOTAL",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SURCHARGE",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "data": "AMOUNT",
                        render: $.fn.dataTable.render.number('.', '.', 0),
                        "class": "dt-body-center"

                    },
                    {
                        "render": function (data, type, full) {
                            if (full.RATE > 1) {
                                return full.GRAND_TOTAL_USD;
                            } else {
                                return " - ";
                            }
                        },
                        "class": "dt-body-right",

                    },
                    {
                        "data": "INSTALLATION_CODE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BILLING_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SAP_DOCUMENT_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,
                
                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "filter": false
            });

        });   

        $('body').on('click', 'tr #btn-pranota-air', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var billingNo = data["ID"];
            var kodeCabang = "";

            //var id_special = data["ID"];
            //var contractNo = data["BILLING_CONTRACT_NO"];
            //var billingNo = data["BILLING_NO"];
            //var kodeCabang = data["BRANCH_ID"];
            var usage = data["BILLING_TYPE"];
            console.log("usage : " + usage)
            // window.open("/Pdf/PdfSakti?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');
            window.open("/Pdf/cetakPdfList?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');


        });

    }

    var initDetilTransactionListrik = function () {
        $('body').on('click', 'tr #btn-detail-listrik', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var id_trans = data["ID"];
            var installation_code = data["INSTALLATION_CODE"];

            var detailTransPricing = $('#table-detil-transaction-listrik').DataTable();
            detailTransPricing.destroy();

            var detailTransCosting = $('#table-detil-transaction-listrik-costing').DataTable();
            detailTransCosting.destroy();

            var detailTransUsed = $('#table-detil-transaction-listrik-used').DataTable();
            detailTransUsed.destroy();

            var currency = data["RATE"];
            var grandTotal = data["AMOUNT"];
            var grandTotalUsd = data["GRAND_TOTAL_USD"];
            var fGrandTotalUsd = grandTotalUsd.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            var fGrandTotal = grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $('#GRAND_TOT').val(fGrandTotal);
            $('#GRAND_USD').val(fGrandTotalUsd);
            if (currency > 1) {
                $('#USD').show();
            } else {
                $('#USD').hide();
            }

            $('#table-detil-transaction-listrik').DataTable({

                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransaction",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: function (data) {
                        delete data.columns;
                        data.id_trans = id_trans;
                        data.installation_code = installation_code;
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "KD_TRANSAKSI",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "INSTALLATION_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KODE_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "NAMA_CUSTOMER_MDM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_ADDRESS",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "DESKRIPSI_PROFIT_CENTER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INSTALLATION_NUMBER",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PERIOD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "POWER_CAPACITY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "MULTIPLY_FACTOR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "PPJU",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "REDUKSI",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIl
            $('#table-detil-transaction-listrik-costing').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetail",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "METER_FROM",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "METER_TO",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "USED",
                        "class": "dt-body-center USED"

                    },
                    {
                        "data": "CURRENCY",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TARIFF",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "URL_FOTO",
                        "class": "dt-body-center",
                        "render": function (data, type, full, meta) {
                            if (data != null) {
                                return '<a href="' + data + '" target="blank"><img src="' + data + '" height="100" width="100" /></a>';
                            } else {
                                return " - ";
                            }
                        }
                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "filter": false
            });

            // DATA DETAIlS USED
            $('#table-detil-transaction-listrik-used').DataTable({
                "ajax":
                {
                    "type": "get",
                    "url": "/TransWEList/GetDataTransactionDetailsUsed",
                    //"data": "{ id_trans:'" + id_trans + "', installation_code:'" + installation_code + "'}",
                    data: {
                        id_trans: id_trans,
                        installation_code: installation_code
                    },
                    cache: false,
                },

                "columns": [
                    {
                        "data": "SUMBER",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "PRICE_TYPE",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TGL_STAN_AWAL",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "TGL_STAN_AKHIR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "STAN_AWAL",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "STAN_AKHIR",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "SELISIH",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KWH_TENAN",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "KWH_DITAGIHKAN",
                        "class": "dt-body-center KWH_DITAGIHKAN"

                    }
                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                "bInfo": false,
                "bLengthChange": false,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "pageLength": 10,

                "filter": false,
                "initComplete": function () {
                    if ($('#table-detil-transaction-listrik-used').DataTable().rows().count() == 0){
                        $('.details_used').hide();
                    }else{
                        $('.details_used').show();
                    }
                }
            });
        });

        $('body').on('click', 'tr #btn-pranota', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-transwe').DataTable();
            var data = table.row(baris).data();

            var billingNo = data["ID"];
            var kodeCabang = "";

            //console.log(billingNo);
            //console.log(kodeCabang);
            console.log(data);

            //var id_special = data["ID"];
            //var contractNo = data["BILLING_CONTRACT_NO"];
            //var billingNo = data["BILLING_NO"];
            //var kodeCabang = data["BRANCH_ID"];
            var usage = data["BILLING_TYPE"];
            console.log("usage : " + usage)
            // window.open("/Pdf/PdfSakti?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');
            window.open("/Pdf/cetakPdfList?tp=" + btoa("1M") + "&kc=" + btoa(data["BRANCH_ID"]) + "&bn=" + btoa(billingNo) + "&us=" + btoa(usage), '_blank');


        });
        
    }


    //Function untuk form Add (Form Sesuai Mockup)
    var simpanupdate = function () {
        $('#btn-simpan-update').click(function () {
            var METER_FROM = $('#METER_FROM').val();
            var METER_TO = $('#METER_TO').val();
            var ID = $('#ID_INSTALASI').val();
            var QUANTITY = $('#KWH').val();
            var AMOUNT = $('#AMOUNT').val();
            var SURCHARGE = $('#SURCHARGE').val();
            var STATUS_DINAS = $('#STATUS_DINAS').val();
            var COA_PROD = $('#coa_prod').val();
            var GRAND_TOTAL = $('#GRAND_TOTAL').val();
            var RATE = $('#RATE').val();

            console.log(STATUS_DINAS);

            if (Number(METER_TO) < Number(METER_FROM)) {
                swal('Failed', 'Check Your Meter To Value', 'error')
            }
            else {
                                
                if (METER_FROM && METER_TO) {
                    var nfAmount = parseFloat(AMOUNT.split('.').join(''));
                    var nfSurcharge = parseFloat(SURCHARGE.split('.').join(''));
                    var netValue = Number(nfAmount) + Number(nfSurcharge);
                    var param = {
                        METER_FROM: METER_FROM,
                        METER_TO: METER_TO,
                        ID: ID,
                        QUANTITY: QUANTITY,
                        AMOUNT: nfAmount.toString(),
                        SURCHARGE: nfSurcharge.toString(),
                        SUB_TOTAL: nfAmount.toString(),
                        GRAND_TOTAL: netValue.toString(),
                        COA_PROD: COA_PROD,
                        STATUS_DINAS: STATUS_DINAS,
                        RATE : RATE
                    };

                    console.log(param);

                    console.log("INI COA PROD : " + COA_PROD);
                    if (COA_PROD == '' || COA_PROD == null) {
                        console.log("COA kosong");
                        swal('Warning', 'mohon isi Coa produksi', 'warning');
                    } else {
                        console.log("COA ADA");
                        App.blockUI({ boxed: true });
                        var req = $.ajax({
                            contentType: "application/json",
                            data: JSON.stringify(param),
                            method: "POST",
                            url: "/TransWEList/EditData",
                            timeout: 30000
                        });
                        req.done(function (data) {
                            App.unblockUI();
                            if (data.status === 'S') {
                                swal('Success', data.message, 'success').then(function (isConfirm) {
                                    window.location = "/TransWEList";
                                });
                            } else {
                                swal('Failed', data.message, 'error').then(function (isConfirm) {
                                    window.location = "/TransWEList";
                                });
                            }
                        });
                    }
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
             
        });
    }
    var cariCoa = function () {
        //$('#btn-filter-coa').click(function () {
            if ($.fn.DataTable.isDataTable('#table-coa')) {
                $('#table-coa').DataTable().destroy();
            }
            $('#table-coa').DataTable({

                "ajax":
                {
                    "url": "/TransElectricity/GetListCoa",
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    },
                    "dataSrc": function (response) {
                        var data = {};
                        data.draw = response.paging.page;
                        data.recordsTotal = response.paging.total;
                        data.recordsFiltered = response.paging.total;
                        data.data = response.result;
                        return data.data;
                    },
                },
                "columns": [
                    {
                        "render": function () {
                            var aksi = '<a class="btn default btn-xs green" id="btn-cus-coa"><i class="fa fa-check"></i></a>';
                            return aksi;
                        },
                        "class": "dt-body-center",
                    },
                    {
                        "data": "COA_PROD",
                        "class": "dt-body-center",
                    },
                    {
                        "data": "DESKRIPSI",
                    },
                    {
                        "data": "GL_TEXT",
                    },

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": true
            });
        //});

        //choose coa
        $('#table-coa').on('click', 'tr #btn-cus-coa', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-coa').DataTable();
            var data = table.row(baris).data();
            var c_data = null;
            var value = data['COA_PROD'];
            $('#coa_prod').val(value);
            console.log(value);

            $('#coaModal').modal('hide');
        });
    }

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/TransWEList";
        });
    }

    return {
        init: function () {
            getCoaProduksi();
            initTableWETransaction();
            initDetilTransaction();
            initDetilTransactionListrik();
            batal();
            ubah();
            simpanupdate();
            maskMeterTo();
            maskMeterFrom();
            hitung();
            cariCoa();
        }
    };
}();


if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        TransWaterElectricity.init();
    });
}

function getCoaProduksi() {
    
    //$.ajax({
    //    type: "GET",
    //    url: "/TransContractOffer/GetDataDropDownCoaProduksiWater",
    //    contentType: "application/json",
    //    dataType: "json",
    //    success: function (data) {
    //        var jsonList = data
    //        var listItems = "";
    //        listItems += "<option value=''>-- ChoOse COA Produksi --</option>";
    //        for (var i = 0; i < jsonList.data.length; i++) {
    //            listItems += "<option value='" + jsonList.data[i].VAL1 + "'>" + jsonList.data[i].REF_DESC + "</option>";
    //        }
    //        $("#coa_prod").html('');
    //        $("#coa_prod").append(listItems);
    //    }
    //});
    //$("#coa_prod").select2({
    //    allowClear: true,
    //    width: '100%',
    //    ajax: {
    //        url: "/TransContractOffer/GetDataDropDownCoaProduksiWater",
    //        dataType: "json",
    //        contentType: "application/json",
    //        data: function (params) {
    //            return {
    //                search: params.term,
    //                page: params.page,
    //            }
    //        },
    //        processResults: function (result) {
    //            return {
    //                results: $.map(result.data, function (item) {
    //                    return {
    //                        text: item.REF_DESC,
    //                        data: item,
    //                        id: item.VAL1
    //                    }
    //                })
    //            };
    //        }
    //    },
    //    escapeMarkup: function (markup) { return markup; },
    //    minimumInputLength: 0,
    //    placeholder: "-- Choose COA Production --",
    //});
    //var $newOption = $("<option selected='selected'></option>").val(coaproduksi).text(coaproduksi);
    //$("#coa_prod").append($newOption).trigger('change');
}