﻿var MonitorAnjungan = function () {

    var initTable = function () {
        $('#table-list-anjungan').DataTable({

            "ajax":
            {
                "url": "/MonitoringAnjungan/GetData",
                "type": "GET",
                "data": function (data) {
                    delete data.columns;
                }
            },
            "columns": [
                {
                    "data": "CONTRACT_NAME"
                },
                {
                    "data": "CONTRACT_TYPE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CUSTOMER_NAME"
                },
                {
                    "data": "CONTRACT_START_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "CONTRACT_END_DATE",
                    "class": "dt-body-center"
                },
                {
                    "data": "STATUS",
                    "render": function (data, type, full) {
                        return '<span class="label label-sm label-primary"> '+ data +' </span>';
                    },
                    "class": "dt-body-center"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a data-toggle="modal" href="#detailModal" data-toggle="tooltip" title="Request Details" class="btn btn-icon-only green" id="btn-detail"><i class="fa fa-eye"></i></a>';
                        if (full.STATUS == 'CREATED') {
                            aksi += '<a target="_blank" href="/TransContractOffer/Add?mimoto=' + btoa(full.ID) + '" data-toggle="tooltip" title="Accept Request" class="btn btn-icon-only blue"><i class="fa fa-check"></i></a>';
                            aksi += '<a data-toggle="modal" href="#deleteModal" data-toggle="tooltip" title="Reject Request" class="btn btn-icon-only red" id="btn-delete"><i class="fa fa-times"></i></a>';
                        }
                        return aksi;
                    },
                    "class": "dt-body-center"
                }
            ],
            "destroy": true,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "lengthMenu": [
                [5, 10, 15, 20, 50],
                [5, 10, 15, 20, 50]
            ],

            "pageLength": 10,

            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
            },

            "filter": true
        });

    }
    
    var initDetail = function () {
        $('#table-list-anjungan').on('click', 'tr #btn-detail', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-anjungan').DataTable();
            var data = table.row(baris).data();

            $("#DETAILCONTRACT_NO").val(data["CONTRACT_NO"]);
            $("#DETAILCONTRACT_OFFER_NO").val(data["CONTRACT_OFFER_NO"]);
            $("#DETAILCONTRACT_NO").val(data["CONTRACT_NO"]);
            $("#DETAILCONTRACT_TYPE").val(data["CONTRACT_TYPE"]);
            $("#DETAILCONTRACT_USAGE").val(data["CONTRACT_USAGE"]);
            $("#DETAILBE_ID").val(data["BE_ID"]);
            $("#DETAILPROFIT_CENTER").val(data["PROFIT_CENTER"]);
            $("#DETAILCONTRACT_NAME").val(data["CONTRACT_NAME"]);
            $("#DETAILCONTRACT_START_DATE").val(data["CONTRACT_START_DATE"]);
            $("#DETAILCONTRACT_END_DATE").val(data["CONTRACT_END_DATE"]);
            $("#DETAILCUSTOMER_NAME").val(data["CUSTOMER_NAME"]);
            $("#DETAILCUSTOMER_AR").val(data["CUSTOMER_AR"]);
            $("#DETAILOLD_CONTRACT").val(data["OLD_CONTRACT"]);
            $("#DETAILSTATUS").val(data["STATUS"]);
            $("#DETAILREJECT_NOTE").val(data["REJECT_NOTE"]);
            $("#DETAILKODE_BAYAR").val(data["KODE_BAYAR"]);


            var id_rental = data["ID"];

            $('#table-detil-transaction').DataTable({

                "ajax":
                {
                    "url": "/MonitoringAnjungan/GetDataDetail/" + id_rental,
                    "type": "GET",
                    "data": function (data) {
                        delete data.columns;
                    }
                },
                "columns": [
                    {
                        "data": "OBJECT_ID",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "OBJECT_TYPE",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "RO_NAME",
                        "class": "dt-body-center"
                    },
                    {
                        "data": "LAND_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "BUILDING_DIMENSION",
                        "class": "dt-body-center"

                    },
                    {
                        "data": "INDUSTRY",
                        "class": "dt-body-center"

                    }

                ],
                "destroy": true,
                "ordering": false,
                "processing": true,
                "serverSide": true,

                "lengthMenu": [
                    [5, 10, 15, 20, 50],
                    [5, 10, 15, 20, 50]
                ],

                "pageLength": 10,

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/English.json"
                },

                "filter": false
            });

        });
    }

    var reject = function () {
        $('body').on('click', 'tr #btn-delete', function () {
            var baris = $(this).parents('tr')[0];
            var table = $('#table-list-anjungan').DataTable();
            var data = table.row(baris).data();
            var id = data['ID'];
            var note = "";
            $('#DELETE_ID').val(id);
            $('#DELETE_NOTE').val(note);
        });
        $('#btn-confirm-delete').click(function () {
            var table = $('#table-list-anjungan').DataTable();
            var mimoto = $('#DELETE_ID').val();
            var note = $('#DELETE_NOTE').val();
            mimoto = btoa(mimoto);
            $('#deleteModal').modal('hide');
            if (note == "") {
                swal({
                    title: 'Info',
                    text: "Please insert Note",
                    type: 'info',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                })
                return;
            }
            var param = {
                mimoto: mimoto,
                note: note
            };
            console.log(param)
            App.blockUI({ boxed: true });
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                url: "/MonitoringAnjungan/Reject",
                data: JSON.stringify(param),
                timeout: 30000
            });

            req.done(function (data) {
                App.unblockUI();
                table.ajax.reload(null, false);
                if (data.status === 'S') {
                    //swal('Success', data.message, 'success');
                    swal({
                        title: 'Info',
                        text: "Request has been rejected",
                        type: 'info',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No'
                    });

                } else {
                    //swal('Failed', data.message, 'error');
                    swal({
                        title: 'Warning',
                        text: "Reject request failed",
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No'
                    });
                }
            });

        });
    }
    return {
        init: function () {
            initTable();
            initDetail();
            reject();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MonitorAnjungan.init();
    });
}