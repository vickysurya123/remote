﻿var MasterROAdd = function () {

    var batal = function () {
        $('#btn-kembali').click(function () {
            window.location = "/WaterAndElectricity";
        });
    }

    var ambilSesuaiSebelumnya = function () {
        var installationTypeEdit = $('#INSTALLATION_TYPE_EDIT').val();
        var currencyEdit = $('#CURRENCY_EDIT').val();
        var unitEdit = $('#UNIT_EDIT').val();

        $("#INSTALLATION_TYPE option[value='" + installationTypeEdit + "']").prop('selected', true);
        $("#CURRENCY option[value='" + currencyEdit + "']").prop('selected', true);
        $("#UNIT option[value='" + unitEdit + "']").prop('selected', true);


    }

    var simpan = function () {
        $('#btn-simpan-update').click(function () {
            var INSTALLATION_TYPE = $('#INSTALLATION_TYPE').val();
            var DESCRIPTION = $('#DESCRIPTION').val();
            var AMOUNT = $('#AMOUNT').val();
            var CURRENCY = $('#CURRENCY').val();
            var X_FACTOR = $('#X_FACTOR').val();
            var FALID_FROM = $('#FALID_FROM').val();
            var FALID_TO = $('#FALID_TO').val();
            var ACTIVE = $('#ACTIVE').val();
            var UNIT = $('#UNIT').val();
            var PROFIT_CENTER = $('#PROFIT_CENTER').val();
            var TARIFF_CODE = $('#TARIFF_CODE').val();

            if (Date.parse(FALID_FROM) > Date.parse(FALID_TO)) {
                swal('Failed', 'Invalid Date Range', 'error');
            }
            else {
                if (INSTALLATION_TYPE && DESCRIPTION && AMOUNT && CURRENCY && FALID_FROM && ACTIVE && UNIT && PROFIT_CENTER && TARIFF_CODE) {
                    var param = {
                        INSTALLATION_TYPE: INSTALLATION_TYPE,
                        DESCRIPTION: DESCRIPTION,
                        AMOUNT: AMOUNT,
                        CURRENCY: CURRENCY,
                        X_FACTOR: X_FACTOR,
                        FALID_FROM: FALID_FROM,
                        FALID_TO: FALID_TO,
                        ACTIVE: ACTIVE,
                        //PER: PER,
                        UNIT: UNIT,
                        PROFIT_CENTER: PROFIT_CENTER,
                        TARIFF_CODE: TARIFF_CODE
                    };

                    console.log(param);

                    App.blockUI({ boxed: true });
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/WaterAndElectricity/EditData",
                        timeout: 30000
                    });

                    req.done(function (data) {
                        App.unblockUI();
                        if (data.status === 'S') {
                            swal('Success', data.message, 'success').then(function (isConfirm) {
                                window.location = "/WaterAndElectricity";
                            });
                        } else {
                            swal('Failed', data.message, 'error').then(function (isConfirm) {
                                window.location = "/WaterAndElectricity";
                            });
                        }
                    });
                } else {
                    swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
                }
            }
        });
    }

    // combo box business entity
    var cbprofitcenter = function () {
        $.ajax({
            type: "GET",
            url: "/WaterAndElectricity/GetDataDropdownProfitCenter",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                var jsonList = data

                $(document).ready(function () {
                    var listItems = "";
                    for (var i = 0; i < jsonList.data.length; i++) {
                        listItems += "<option value='" + jsonList.data[i].PROFIT_CENTER_ID + "'>" + jsonList.data[i].PROFIT_CENTER_ID + ' - ' + jsonList.data[i].TERMINAL_NAME + "</option>";
                    }
                    //$("#dd-business-entity").html(listItems);
                    $("#PROFIT_CENTER").append(listItems);

                    var profitCenterEdit = $('#PROFIT_CENTER_EDIT').val();
                    $("#PROFIT_CENTER option[value='" + profitCenterEdit + "']").prop('selected', true);
                });
            }
        });
    }

    var update = function () {
        $('#btn-update').click(function () {
            var RO_NUMBER = $('#RO_NUMBER').val();
            var BE_ID = $('#BE_ID').val();
            var RO_NAME = $('#RO_NAME').val();
            var RO_CERTIFICATE_NUMBER = $('#RO_CERTIFICATE_NUMBER').val();
            var RO_ADDRESS = $('#RO_ADDRESS').val();
            var RO_POSTALCODE = $('#RO_POSTALCODE').val();
            var RO_CITY = $('#RO_CITY').val();
            var RO_PROVINCE = $('#RO_PROVINCE').val();
            var MEMO = $('#MEMO').val();


            if (RO_NUMBER && BE_ID && RO_NAME && RO_CERTIFICATE_NUMBER && RO_ADDRESS && RO_POSTALCODE && RO_CITY && RO_PROVINCE && MEMO) {
                var param = {
                    RO_NUMBER: RO_NUMBER,
                    BE_ID: BE_ID,
                    RO_NAME: RO_NAME,
                    RO_CERTIFICATE_NUMBER: RO_CERTIFICATE_NUMBER,
                    RO_ADDRESS: RO_ADDRESS,
                    RO_POSTALCODE: RO_POSTALCODE,
                    RO_CITY: RO_CITY,
                    RO_PROVINCE: RO_PROVINCE,
                    MEMO: MEMO
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/RentalObject/EditData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/RentalObject";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/RentalObject";
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    var updatero = function () {
        $('#btn-update-ro').click(function () {
            var RO_NUMBER = $('#RO_NUMBER').val();
            var BE_ID = $('#BE_ID').val();
            var RO_NAME = $('#RO_NAME').val();
            var RO_CERTIFICATE_NUMBER = $('#RO_CERTIFICATE_NUMBER').val();
            var RO_ADDRESS = $('#RO_ADDRESS').val();
            var RO_POSTALCODE = $('#RO_POSTALCODE').val();
            var RO_CITY = $('#RO_CITY').val();
            var RO_PROVINCE = $('#RO_PROVINCE').val();
            var MEMO = $('#MEMO').val();


            if (RO_NUMBER && BE_ID && RO_NAME && RO_CERTIFICATE_NUMBER && RO_ADDRESS && RO_POSTALCODE && RO_CITY && RO_PROVINCE && MEMO) {
                var param = {
                    RO_NUMBER: RO_NUMBER,
                    BE_ID: BE_ID,
                    RO_NAME: RO_NAME,
                    RO_CERTIFICATE_NUMBER: RO_CERTIFICATE_NUMBER,
                    RO_ADDRESS: RO_ADDRESS,
                    RO_POSTALCODE: RO_POSTALCODE,
                    RO_CITY: RO_CITY,
                    RO_PROVINCE: RO_PROVINCE,
                    MEMO: MEMO
                };

                console.log(param);

                App.blockUI({ boxed: true });
                var req = $.ajax({
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    method: "POST",
                    url: "/RentalObject/EditData",
                    timeout: 30000
                });

                req.done(function (data) {
                    App.unblockUI();
                    if (data.status === 'S') {
                        swal('Success', data.message, 'success').then(function (isConfirm) {
                            window.location = "/RentalObject";
                        });
                    } else {
                        swal('Failed', data.message, 'error').then(function (isConfirm) {
                            window.location = "/RentalObject";
                        });
                    }
                });
            } else {
                swal('Warning', 'Please Make Sure All Required Fields Are Filled Out Correctly!', 'warning');
            }
        });
    }

    return {
        init: function () {
            batal();
            simpan();
            update();
            updatero();
            cbprofitcenter();
            ambilSesuaiSebelumnya();
        }
    };
}();




if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        MasterROAdd.init();
    });
}