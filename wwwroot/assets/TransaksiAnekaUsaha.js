﻿var FuncTrxAnekaUsaha = function () {
    var baseUrl = 'http://' + window.location.host;
    var _actCode = "ANEKAUSAHA_ADD";
    var _bussEntityID = 1021;
    var _kodeCabang = 2;
    var lov_status = 0;

    var text2 = $("#lookup_customer").tautocomplete({
        width: "500px",
        columns: ['Name', 'CustomerCode'],
        hide:  [false,true,true],
        ajax: {
            url: '/AnekaUsaha/GetCustomer',
            type: "GET",
            data: function () { var x = { query: text2.searchdata()}; return x; },//nambah querystringnya disini rem by redy
            success: function (data) {

                var filterData = [];

                var searchData = eval("/" + text2.searchdata() + "/gi");

                $.each(data, function (i, v) {
                    if (v.MPLG_NAMA.search(new RegExp(searchData)) !== -1) {
                        var d = new Array(v.MPLG_KODE,v.MPLG_NAMA, v.MPLG_KODE);
                        filterData.push(d);
                    }
                });
                return filterData;
            }
        },
        onchange: function () {
            console.log('al: ' + text2.all());
            //$("#ta-all").html(JSON.stringify(text2.all()));
            var jsonString = JSON.stringify(text2.all());
            var jsonobject = JSON.parse(jsonString);
          
            $("#customer_code_mdm").val(jsonobject.CustomerCode);
            $("#CUSTOMER_ID").val(text2.id());
        }
    });

//    var custom = new Bloodhound({
//        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('customer'),
//        queryTokenizer: Bloodhound.tokenizers.whitespace,
//        limit: 10,
//        remote: {
//            url: '/AnekaUsaha/GetCustomer?query=%QUERY',
//            ajax: {
//                dataType: 'jsonp'
//            },
//            wildcard: '%QUERY'
//        }
//});

//    custom.initialize();

//    if (App.isRTL()) {
//        $('#lookup_customer').attr("dir", "rtl");
//    }
//    $('#lookup_customer').typeahead(
//        {
//            hint: false,
//            highlight: true,
//            minLength: 1    
//        },
//        {
//        name: 'dalookup_customer',
//        displayKey: 'MPLG_NAMA',
//        source: custom.ttAdapter(),
//        hint: (App.isRTL() ? false : true),
//        templates: {
//            header: '<h3 class="league-name">Customer Code &nbsp;&nbsp;&nbsp;Customer Name</h3>',
//            suggestion: Handlebars.compile([
//              '<div class="media">',
//                    '<div class="media-body">',
//                        '<h4 class="media-heading">{{value}}</h4>',
//                        '<p>{{MPLG_KODE}}-{{MPLG_NAMA}}</p>',
//                    '</div>',
//              '</div>',
//            ].join(''))
//        }
//    });

$('#customerCode')
    .keyup(function (e) {
        var key = (e.keyCode ? e.keyCode : e.charCode);
        switch (key) {
            case 120: /* Tekan Tombol F9 */
                $('[rel=customerCode-rel]').popover({
                    trigger: 'manual',
                    placement: 'right',
                    html: true,
                    content: '<div>' +
                        '<table class="table table-striped table-bordered table-hover" id="tbl-customerCode">' +
                        '<thead><tr>' +
                        '<th>Customer Code</th>' +
                        '<th>Customer Name</th>' +
                        '</tr></thead>' +
                        '<tbody></tbody></table>' +
                        '</div>'
                }).parent().on('keyup', '', function (e) {
                    FuncTableCustomerCode(_kodeCabang, _actCode, _bussEntityID);
                    e.stopPropagation();
                });
                $('[rel=customerCode-rel]').popover('show');
                lov_status = 7;
                break;
            case 27: /* Tekan Tombol Esc */
                $('[rel=customerCode-rel]').popover('hide');
                lov_status = 0;
                break;
        }
    });
var FuncTableCustomerCode = function (kodeCabang, ActCode, BussEntityId) {
    $('#tbl-customerCode').dataTable({
        "aoColumns": [
            { "bSortable": false, "sClass": "alignLeft" },
            { "bSortable": false, "sClass": "alignLeft" }
        ],
        "bLengthChange": false,
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "bDestroy": true,
        "sAjaxSource": 'http://localhost:14280/api/Customer/DataCustomerDataTable/',
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "paramStandar.KodeCabang", "value": kodeCabang },
                { "name": "paramStandar.ActCode", "value": "DataCustomerDataTable" },
                { "name": "paramStandar.BusinessEntityId", "value": BussEntityId }
            );
        },
        "iDisplayLength": 5,
        "bStateSave": true,
        "oLanguage": {
            "sProcessing": '<i class="fa fa-hourglass-start"></i>&nbsp;Please wait...',
            "sLengthMenu": "_MENU_ records",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
    }).fnFilterOnReturn();
    jQuery('#tbl-ppkb1-pmh_wrapper .dataTables_filter input').addClass("form-control input-small");
    jQuery('#tbl-ppkb1-pmh_wrapper .dataTables_length select').addClass("form-control input-small");
    jQuery('#tbl-ppkb1-pmh_wrapper .dataTables_length select').select2();
    jQuery('#tbl-ppkb1-pmh_wrapper .dataTables_filter input').focus();
}
return {

    init: function () {

        if (!jQuery().dataTable) {
            return;
        }
        $(document).ajaxStop($.unblockUI);

    }

};

}();