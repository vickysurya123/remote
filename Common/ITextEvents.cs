﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

namespace Common
{
    public class ITextEvents : PdfPageEventHelper
    {
        // This is the contentbyte object of the writer  
        PdfContentByte cb;

        // we will put the final number of pages in a template  
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer  
        BaseFont bf = null;

        // This keeps track of the creation time  
        DateTime PrintTime = DateTime.Now;

        #region Fields  
        private string _header;
        #endregion

        #region Properties  
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion
        public string beName;
        public string beCity;
        public string beAddress;
        public string postalCode;
        public string beCP;
        public ITextEvents(string BE_NAME, string BE_CITY, string BE_ADDRESS, string POSTAL_CODE, string BE_CONTACT_PERSON)
        {
            beName = BE_NAME;
            beCity = BE_CITY;
            beAddress = BE_ADDRESS;
            postalCode = POSTAL_CODE;
            beCP = BE_CONTACT_PERSON;
        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(100, 100);
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            //Phrase p1Header = new Phrase("Sample Header Here", baseFontNormal);

            //Create PdfTable object  
            PdfPTable pdfTab = new PdfPTable(3);

            //We will have to create separate cells to include image logo and 2 separate strings  
            //Row 1  
            PdfPCell pdfCell1 = new PdfPCell();
            //PdfPCell pdfCell2 = new PdfPCell(p1Header);
            PdfPCell pdfCell3 = new PdfPCell();
            //String text = "Page " + writer.PageNumber + " of ";
            String textLeftTop = beName;
            String textLeftMiddle = beAddress;
            String textLeftBottom = "Jawa Timur " + postalCode;
            String textMiddleMid = "T: +623132919926";
            String textMiddleBottom = "F: +62313293994";
            //String textRightBottom = beAddress + " " + beCity;
            String textRightBottom = beAddress;
            String text  = beName;
            String text2 = beAddress+@"                     T: +623132919926";
            String text3 = "Jawa Timur "+ postalCode +@"                                              F: +62313293994                       "+beAddress+" "+beCity;

            //Add paging to header  
            //{
            //    cb.BeginText();
            //    cb.SetFontAndSize(bf, 12);
            //    cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
            //    cb.ShowText(text);
            //    cb.EndText();
            //    float len = bf.GetWidthPoint(text, 12);
            //    //Adds "12" in Page 1 of 12  
            //    cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
            //}
            //Add paging to footer  
            {
                float len = 0;
                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(40), document.PageSize.GetBottom(10));
                cb.ShowText(textLeftBottom);
                cb.EndText();
                len = bf.GetWidthPoint(textLeftBottom, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(40) + len, document.PageSize.GetBottom(10));

                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(40), document.PageSize.GetBottom(25));
                cb.ShowText(textLeftMiddle);
                cb.EndText();
                len = bf.GetWidthPoint(textLeftMiddle, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(40) + len, document.PageSize.GetBottom(25));

                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(40), document.PageSize.GetBottom(40));
                cb.ShowText(textLeftTop);
                cb.EndText();
                len = bf.GetWidthPoint(textLeftTop, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(40) + len, document.PageSize.GetBottom(40));

                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(280), document.PageSize.GetBottom(10));
                cb.ShowText(textMiddleBottom);
                cb.EndText();
                len = bf.GetWidthPoint(textMiddleBottom, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(280) + len, document.PageSize.GetBottom(10));

                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(280), document.PageSize.GetBottom(25));
                cb.ShowText(textMiddleMid);
                cb.EndText();
                len = bf.GetWidthPoint(textMiddleMid, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(280) + len, document.PageSize.GetBottom(25));

                cb.BeginText();
                cb.SetFontAndSize(bf, 9);
                cb.SetTextMatrix(document.PageSize.GetLeft(450), document.PageSize.GetBottom(10));
                cb.ShowText(textRightBottom);
                cb.EndText();
                len = bf.GetWidthPoint(textRightBottom, 10);
                cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(450) + len, document.PageSize.GetBottom(10));

                //cb.BeginText();
                //cb.SetFontAndSize(bf, 9);
                //cb.SetTextMatrix(document.PageSize.GetLeft(40), document.PageSize.GetBottom(20));
                //cb.ShowText(text2);
                //cb.EndText();
                //len = bf.GetWidthPoint(text2, 10);
                //cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(40) + len, document.PageSize.GetBottom(20));

                //cb.BeginText();
                //cb.SetFontAndSize(bf, 9);
                //cb.SetTextMatrix(document.PageSize.GetLeft(40), document.PageSize.GetBottom(30));
                //cb.ShowText(text);
                //cb.EndText();
                //len = bf.GetWidthPoint(text, 10);
                //cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(40) + len, document.PageSize.GetBottom(30));
            }

            //Row 2  
            //PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));
            PdfPCell pdfCell4 = new PdfPCell();

            //Row 3   
            PdfPCell pdfCell5 = new PdfPCell();
            //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
            PdfPCell pdfCell6 = new PdfPCell();
            PdfPCell pdfCell7 = new PdfPCell();
            //PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

            //set the alignment of all three cells and set border to 0  
            pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
            //pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;

            //pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
            pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
            pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;

            pdfCell4.Colspan = 3;

            pdfCell1.Border = 0;
            //pdfCell2.Border = 0;
            pdfCell3.Border = 0;
            pdfCell4.Border = 0;
            pdfCell5.Border = 0;
            pdfCell6.Border = 0;
            pdfCell7.Border = 0;

            //add all three cells into PdfTable  
            pdfTab.AddCell(pdfCell1);
            //pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);
            pdfTab.AddCell(pdfCell4);
            pdfTab.AddCell(pdfCell5);
            pdfTab.AddCell(pdfCell6);
            pdfTab.AddCell(pdfCell7);

            pdfTab.TotalWidth = document.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 70;

            //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;      

            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable  
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write  
            //Third and fourth param is x and y position to start writing  
            //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value  

            //Move the pointer and draw line to separate header section from rest of page  
            //cb.MoveTo(40, document.PageSize.Height - 100);
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
            //cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            //cb.MoveTo(40, document.PageSize.GetBottom(50));
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            //cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 10);
            headerTemplate.SetTextMatrix(0, 0);
            //headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            //headerTemplate.ShowText((writer.PageNumber).ToString());
            headerTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 10);
            footerTemplate.SetTextMatrix(0, 0);
            //footerTemplate.ShowText((writer.PageNumber).ToString());
            //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();
        }
    }
}
