﻿using Dapper;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Remote.Helper;
using Oracle.ManagedDataAccess.Client;
using Remote.DAL;
using Remote.Models.EmailConfiguration;
using Remote.Helpers;

namespace Remote.SchedulerTask
{
    public class TaskNotifExpired : IJob
    {
        Task IJob.Execute(IJobExecutionContext context)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            NotifExpired();
            stopwatch.Stop();
            return Task.CompletedTask;
            //throw new NotImplementedException();
        }
        public void NotifExpired()
        {
            using (
                
                var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                try
                {
                    //data contract > 10 tahun
                    var cariData10 = @"SELECT * FROM VW_EMAIL_NOTIF_CUSTOMER WHERE MONTHS > 120 AND MONTH_NOTIF = 36";
                    dynamic hasilData10 = connection.Query<dynamic>(cariData10).ToList();
                    string body = "";
                    TestEmail t = new TestEmail();
                    foreach (dynamic item in hasilData10)
                    {
                        if (item.DAYS == 26)
                        {
                            if (item.MPLG_EMAIL_ADDRESS != null)
                            {
                                body = string.Format(@"Yth "+ item.MPLG_NAMA + @"</br> Dengan ini disampaikan bahwa perjanjian kerjasama " + item.CONTRACT_NAME + @" dengan nomor kontrak: " + item.CONTRACT_NO + @" akan berakhir pada tanggal: "+ item.CONTRACT_END_DATE +@"</br> Mohon untuk direview kembali. Apabila ada hal yang ingin didiskusikan dapat langsung menghubungi CS kami.");

                                EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? hasilData10.MPLG_EMAIL_ADDRESS : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                                //email.EMAIL_TO = "fahmbek@gmail.com";//"hafidz.lazuardy@gmail.com";//
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                email.DESC = "name : " + item.MPLG_NAMA + ", email : " + item.MPLG_EMAIL_ADDRESS;
                                email.SUBJECT = "Informasi";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                //var sendingemail = dalEmail.SendMail(email);
                            }
                        }
                    }

                    //  data contract > 5 >= 10 tahun
                    var cariData5 = @"SELECT * FROM VW_EMAIL_NOTIF_CUSTOMER A WHERE A.MONTHS <= 120 AND A.MONTHS > 60 AND MONTH_NOTIF = 24";
                    dynamic hasilData5 = connection.Query<dynamic>(cariData5).ToList();
                    foreach (dynamic item in hasilData5)
                    {
                        if (item.DAYS == 0)
                        {
                            if (item.MPLG_EMAIL_ADDRESS != null)
                            {
                                body = string.Format(@"Yth " + item.MPLG_NAMA + @"</br> Dengan ini disampaikan bahwa perjanjian kerjasama " + item.CONTRACT_NAME + @" dengan nomor kontrak: " + item.CONTRACT_NO + @" akan berakhir pada tanggal: " + item.CONTRACT_END_DATE + @"</br> Mohon untuk direview kembali. Apabila ada hal yang ingin didiskusikan dapat langsung menghubungi CS kami.");

                                EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? item.MPLG_EMAIL_ADDRESS : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                email.DESC = "name : " + item.MPLG_NAMA + ", email : " + item.MPLG_EMAIL_ADDRESS;
                                email.SUBJECT = "Informasi";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                //var sendingemail = dalEmail.SendMail(email);
                            }
                        }
                    }

                    //data contract > 2 >= 5 tahun
                    var cariData25 = @"SELECT * FROM VW_EMAIL_NOTIF_CUSTOMER A WHERE A.MONTHS <= 60 AND A.MONTHS > 24 AND MONTH_NOTIF = 12";
                    dynamic hasilData25 = connection.Query<dynamic>(cariData25).ToList();
                    foreach (dynamic item in hasilData25)
                    {
                        if (item.DAYS == 0)
                        {
                            if (item.MPLG_EMAIL_ADDRESS != null)
                            {
                                body = string.Format(@"Yth " + item.MPLG_NAMA + @"</br> Dengan ini disampaikan bahwa perjanjian kerjasama " + item.CONTRACT_NAME + @" dengan nomor kontrak: " + item.CONTRACT_NO + @" akan berakhir pada tanggal: " + item.CONTRACT_END_DATE + @"</br> Mohon untuk direview kembali. Apabila ada hal yang ingin didiskusikan dapat langsung menghubungi CS kami.");

                                EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? item.MPLG_EMAIL_ADDRESS : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                email.DESC = "name : " + item.MPLG_NAMA + ", email : " + item.MPLG_EMAIL_ADDRESS;
                                email.SUBJECT = "Informasi";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                //var sendingemail = dalEmail.SendMail(email);
                            }
                        }
                    }

                    //data contract 2 tahun
                    var cariData2 = @"SELECT * FROM VW_EMAIL_NOTIF_CUSTOMER A WHERE A.MONTHS <= 24 AND MONTH_NOTIF = 6";
                    dynamic hasilData2 = connection.Query<dynamic>(cariData2).ToList();
                    foreach (dynamic item in hasilData2)
                    {
                        if (item.DAYS == 0)
                        {
                            if (hasilData2.MPLG_EMAIL_ADDRESS != null)
                            {
                                body = string.Format(@"Yth " + item.MPLG_NAMA + @"</br> Dengan ini disampaikan bahwa perjanjian kerjasama " + item.CONTRACT_NAME + @" dengan nomor kontrak: " + item.CONTRACT_NO + @" akan berakhir pada tanggal: " + item.CONTRACT_END_DATE + @"</br> Mohon untuk direview kembali. Apabila ada hal yang ingin didiskusikan dapat langsung menghubungi CS kami.");

                                EmailConfigurationDAL dalEmail = new EmailConfigurationDAL();
                                //send email
                                EmailConf email = new EmailConf();
                                email.EMAIL_TO = string.IsNullOrEmpty(t.TEST_EMAIL_MAKER()) ? item.MPLG_EMAIL_ADDRESS : t.TEST_EMAIL_MAKER();// "guntur.herdiawan@pelindo.co.id";//"hafidz.lazuardy@gmail.com";//
                                email.CASE = "1";//1.email to user || 2.email to pengguna jasa
                                email.BODY = body;
                                email.DESC = "name : " + item.MPLG_NAMA + ", email : " + item.MPLG_EMAIL_ADDRESS;
                                email.SUBJECT = "Informasi";
                                EmailConfigurationDAL dal = new EmailConfigurationDAL();
                                //var sendingemail = dalEmail.SendMail(email);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
            }
        }
    }
}
