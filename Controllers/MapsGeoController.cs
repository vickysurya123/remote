﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Remote.Models.ListNotifikasi;

namespace Remote.Controllers
{
    [Authorize]
    public class MapsGeoController : Controller
    {
        // GET: MapsGeo
        public ActionResult Index()
        {
            var user = CurrentUser;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            UserViewModels result = new UserViewModels();
            result.DDBranch = dal.GetDataListBranch(user.KodeCabang);
            //var viewModel = new UserViewModels
            //{
            //    DDBranch = dal.GetDataListBranch(user.KodeCabang),
            //};
            ViewBag.kodeCabang = user.KodeCabang;
            ViewBag.namaCabang = user.NamaCabang;
            ViewBag.isCabang = dal.IsCabang(user.KodeCabang);


            return View(result);
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult PinPoint(string BRANCH_ID)
        {
            dynamic message = null;

            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.PinPoint(BRANCH_ID);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        [HttpGet]
        public JsonResult PinPointMobile(string BRANCH_ID)
        {
            dynamic response = null;
            dynamic message = null;

            ResultsNotif res = new ResultsNotif();
            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.PinPoint(BRANCH_ID);
                //if (res.status == "E")
                //{
                //    response = new { Status = "E", Message = res.message };
                //}
                //else
                //{
                    response = new { Status = "S", Message = message };
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }
        public JsonResult PinRO(string BRANCH_ID, string STATUS = null, string ADDRESS = null)
        {
            dynamic message = null;

            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.PinRO(BRANCH_ID, STATUS, ADDRESS);
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult GetImage(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.GetImage(RO_NUMBER);
                foreach(dynamic x in message.message)
                {
                    x.DIRECTORY = Url.Content(x.DIRECTORY);
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }
    }
}