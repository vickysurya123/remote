﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransVB;
using Remote.Models.TransVBList;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class TransVBBillingController : Controller
    {
        //
        // GET: /TransVBBilling/
        public ActionResult Index()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
            };
            ViewBag.KodeCabang = CurrentUser.KodeCabang;
            ViewBag.UserRole = CurrentUser.UserRoleID;
            return View("TransVBBillingIndex", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataVBTransBilling()
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    TransVBBillingDAL dal = new TransVBBillingDAL();
                    result = dal.GetDataVBTransBilling(draw, start, length, search, KodeCabang, UserRole);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------------- NEW GET DATA ------------------------------
        public JsonResult GetDataVBTransBillingNew()
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string select_cabang = queryString["select_cabang"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    string UserID = CurrentUser.UserID;
                    TransVBBillingDAL dal = new TransVBBillingDAL();
                    result = dal.GetDataVBTransBillingnew(draw, start, length, search, KodeCabang, UserRole, select_cabang, UserID);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }


        public JsonResult GetDataTransaction(string id)
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransVBBillingDAL dal = new TransVBBillingDAL();
                    result = dal.GetDataTransactionVB(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult postToSAP([FromBody] PostSAP data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    TransVBBillingDAL dal = new TransVBBillingDAL();
                    var result = dal.PostSAP(data.BILL_ID, data.BILL_TYPE, data.POSTING_DATE, KodeCabang, UserRole);

                    if (string.IsNullOrEmpty(result.DOCUMENT_NUMBER))
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "E",
                            message = "Failed to Post Data."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "S",
                            message = "Data Has Been Posted to SAP With Document Number " + result.DOCUMENT_NUMBER
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult GetMsgSAP([FromBody] string BILL_ID)
        {
            TransVBBillingDAL dal = new TransVBBillingDAL();
            Array ret = dal.GetDataMsgSAP(BILL_ID);
            return Json(ret);
        }

        public JsonResult sendToSilapor(string BILL_ID)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    var result = dal.PostSilapor(BILL_ID);

                    message = new
                    {
                        status = "S",
                        message = "Data Has Been Posted to SAP With Document Number "
                    };
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}