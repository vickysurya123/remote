﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransWE;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class TransWEController : Controller
    {
        //
        // GET: /TransWE/
        public ActionResult Index()
        {
            return View("TransWEIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //------------------- DATATABLE TRANS WE TRANS ----------------------------
        public JsonResult GetDataWETrans()
        {
            DataTablesTransWE result = new DataTablesTransWE();
             
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEDAL dal = new TransWEDAL();
                    result = dal.GetDataTransWE(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATATABLE TRANS WE TRANS NEW ----------------------------
        public JsonResult GetDataWETransnew()
        {
            DataTablesTransWE result = new DataTablesTransWE();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEDAL dal = new TransWEDAL();
                    result = dal.GetDataTransWEnew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------------ADD TRANS WE----------------------------

        public ActionResult AddTransWE(string id)
        {
            ViewBag.WE_ID = id;

            try
            {
                TransWEDAL dal = new TransWEDAL();
                AUP_TRANS_WE_WEBAPPS data = dal.GetDataForEdit(id);

                float hitung = 0;
                string bulansekarang = "";
                string bulankemarin = "";
                string bulankemarin1 = "";
                string bulankemarin2 = "";
                string bulankemarin3 = "";
                string bulankemarin4 = "";
                string bulankemarin5 = "";
                string bulankemarin6 = "";
                string bulankemarin7 = "";
                string bulankemarin8 = "";
                string bulankemarin9 = "";
                string bulankemarin10 = "";

                bulansekarang = DateTime.Now.ToString("MM.yyyy");
                bulankemarin = DateTime.Now.AddMonths(-1).ToString("MM.yyyy");
                bulankemarin1 = DateTime.Now.AddMonths(-2).ToString("MM.yyyy");
                bulankemarin2 = DateTime.Now.AddMonths(-3).ToString("MM.yyyy");
                bulankemarin3 = DateTime.Now.AddMonths(-4).ToString("MM.yyyy");
                bulankemarin4 = DateTime.Now.AddMonths(-5).ToString("MM.yyyy");
                bulankemarin5 = DateTime.Now.AddMonths(-6).ToString("MM.yyyy");
                bulankemarin6 = DateTime.Now.AddMonths(-7).ToString("MM.yyyy");
                bulankemarin7 = DateTime.Now.AddMonths(-8).ToString("MM.yyyy");
                bulankemarin8 = DateTime.Now.AddMonths(-9).ToString("MM.yyyy");
                bulankemarin9 = DateTime.Now.AddMonths(-10).ToString("MM.yyyy");
                bulankemarin10 = DateTime.Now.AddMonths(-11).ToString("MM.yyyy");

                hitung = data.AMOUNT * data.X_FACTOR;


                ViewBag.ID = data.ID;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.INSTALLATION_NUMBER = data.INSTALLATION_NUMBER;
                ViewBag.UNIT = data.UNIT;
                ViewBag.X_FACTOR = data.X_FACTOR;
                ViewBag.PRICE = data.PRICE;
                ViewBag.BULAN_KEMARIN = bulankemarin;
                ViewBag.BULAN_KEMARIN1 = bulankemarin1;
                ViewBag.BULAN_KEMARIN2 = bulankemarin2;
                ViewBag.BULAN_KEMARIN3 = bulankemarin3;
                ViewBag.BULAN_KEMARIN4 = bulankemarin4;
                ViewBag.BULAN_KEMARIN5 = bulankemarin5;
                ViewBag.BULAN_KEMARIN6 = bulankemarin6;
                ViewBag.BULAN_KEMARIN7 = bulankemarin7;
                ViewBag.BULAN_KEMARIN8 = bulankemarin8;
                ViewBag.BULAN_KEMARIN9 = bulankemarin9;
                ViewBag.BULAN_KEMARIN10 = bulankemarin10;
                ViewBag.BULAN_SEKARANG = bulansekarang;
                ViewBag.METER_FROM = data.METER_FROM;
                ViewBag.AMOUNT = data.AMOUNT;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.INSTALLATION_CODE = data.INSTALLATION_CODE;
                ViewBag.MINIMUM_AMOUNT = data.MINIMUM_AMOUNT;
                ViewBag.STATUS_DINAS = data.STATUS_DINAS;
            }
            catch (Exception)
            {

            }
            return View();
        }

        //-------------------TAMBAH DATA TRANSAKSI OTHER SERVICES----------------------------

        public JsonResult AddData([FromBody] DataTransWE data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEDAL dal = new TransWEDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.WE_NUMBER,
                            status = "S",
                            message = "Data Added Successfully with Water Transaction ID :" + result.WE_NUMBER

                            //message = "Data berhasil ditambahkan."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult getLastPeriod(string id_instalasi, string periode)
        {
            TransElectricityDAL dal = new TransElectricityDAL();
            string KodeCabang = CurrentUser.KodeCabang;
            IEnumerable<AUP_TRANS_WE_WEBAPPS> result = dal.getLastPeriod(id_instalasi, periode, KodeCabang);
            return Json(new { data = result });
        }
	}
}