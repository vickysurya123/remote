﻿using System;
using System.Collections.Generic;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransSpecialContract;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
//using Remote.SAP_CEK_PIUTANG;
using System.Security.Claims;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using SAP_CHECK_PIUTANG;

namespace Remote.Controllers
{
    [Authorize]
    public class TransSpecialContractController : Controller
    {
        //
        // GET: /TransSpecialContract/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("TransSpecialContractIndex");
        }

        public ActionResult AddTransSpecialContract()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransSpecialContractDAL dal = new TransSpecialContractDAL();
            var viewModel = new TransSpecialContract
            {
                DDBusinessEntity = dal.GetDataBE2(KodeCabang),
                DDProfitCenter = dal.GetDataProfitCenter(KodeCabang),
                DDContractUsage = dal.GetDataContractUsage()
            };
            return View(viewModel); ;
        }

        //----------------------------- INSERT DATA HEADER ------------------------
        [HttpPost]
        public JsonResult SaveHeader([FromBody]DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    var result = dal.AddHeader(data, User.Identity.Name, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        var prosedurBilling = dal.prosedurBillingCashflow(result.ID_TRANS, CurrentUser.Name);
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "S",
                            message = "Contract Added Successfully with Contract Number: " + result.ID_TRANS
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------- GENERATE CASHFLOW ------------
        public JsonResult GenerateCashFlow(string id)
        {
            dynamic message = null;

            TransSpecialContractDAL dal = new TransSpecialContractDAL();
            var prosedurBilling = dal.prosedurBillingCashflow(id, CurrentUser.Name);
            return Json(message);
        }

        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpPost]
        public ActionResult GetDataCustomer([FromBody]string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //------------------- DATA TABLE HEADER DATA -----------------------
        public JsonResult GetDataTransSpecialContract()
        {
            DataTablesSpecialContract result = new DataTablesSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataTransSpecialContract(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATA TABLE DETAIL OBJECT -----------------------
        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesSpecialContract result = new DataTablesSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        //------------------- DATA TABLE DETAIL CONDITION -----------------------
        public JsonResult GetDataDetailCondition(string id)
        {
            DataTablesSpecialContract result = new DataTablesSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string id)
        {
            DataTablesSpecialContract result = new DataTablesSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailMemo(string id)
        {
            DataTablesSpecialContract result = new DataTablesSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------------- UBAH STATUS ---------------------
        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                TransSpecialContractDAL dal = new TransSpecialContractDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //---------------------- CEK PIUTANG SAP ------------------
        //public JsonResult cekPiutangSAP(string DOC_TYPE, string CUST_CODE)
        //{
        //    string KodeCabang = CurrentUser.KodeCabang;
        //    retCheckLock retLock = new retCheckLock();
        //    CheckLockPiutang cSAP = new CheckLockPiutang();
        //    retLock = cSAP.inboundCheckLock_P3("", DOC_TYPE, KodeCabang, CUST_CODE);
        //    return Json(retLock);
        //}

        [HttpPost]
        public JsonResult cekPiutangSAP([FromBody] Models.TransRentalRequest.CheckPiutangSapParam param)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            retCheckLock retLock = new retCheckLock();
            CheckLockPiutangSoapClient cSAP = new CheckLockPiutangSoapClient(CheckLockPiutangSoapClient.EndpointConfiguration.CheckLockPiutangSoap);
            try
            {

                retLock = cSAP.inboundCheckLock_P3Async("", param.DOC_TYPE, KodeCabang, param.CUST_CODE).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {

                //throw;
            }
            return Json(retLock);
        }

        //---------------------- GET DATA ATTACHMENT ------------------------
        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachmentContract result = new DataTablesAttachmentContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransSpecialContractDAL dal = new TransSpecialContractDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------------- UPLOAD FILES -------------

        [HttpPost]
        public ContentResult UploadFiles([FromForm] Models.TransContractOffer.AttachmentOffer data)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "SpecialContract", subPath);

                bool exists = System.IO.Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/SpecialContract/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                TransSpecialContractDAL dal = new TransSpecialContractDAL();
                bool result = dal.AddFiles(sFile, directory, KodeCabang, data.BE_ID);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        //--------------------------- DELETE ATTACHMENT --------------------
        [HttpGet]
        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "SpecialContract", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                TransSpecialContractDAL dal = new TransSpecialContractDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }
    }
}