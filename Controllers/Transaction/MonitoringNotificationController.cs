﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Remote.DAL;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using Remote.Models.NotificationContract;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class MonitoringNotificationController : Controller
    {
        // GET: MonitoringNotification
        public ActionResult Index()
        {
            return View("MonitoringNotificationIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntitynew(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    IDataTable data = dal.GetDatanew(draw, start, length, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //-------------------- NEW GET DATA -----------------------------
        public JsonResult GetDataNew()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    IDataTable data = dal.GetDatanew(draw, start, length, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }
        
        public JsonResult GetDataFilter(string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            IDataTable result = new IDataTable();
            profit_center   = (string.IsNullOrWhiteSpace(profit_center) ? "0" : profit_center);
            be_id           = (string.IsNullOrWhiteSpace(be_id) ? "0" : be_id);
            contract_no     = (string.IsNullOrWhiteSpace(contract_no) ? "0" : contract_no);
            customer_id     = (string.IsNullOrWhiteSpace(customer_id) ? "0" : customer_id);
            badan_usaha     = (string.IsNullOrWhiteSpace(badan_usaha) ? "x" : badan_usaha);
            date_start      = (string.IsNullOrWhiteSpace(date_start) ? "x" : date_start);
            date_end        = (string.IsNullOrWhiteSpace(date_end) ? "x" : date_end);

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    IDataTable data = dal.GetFilternew(draw, start, length, search, KodeCabang, profit_center, be_id, contract_no, customer_id, badan_usaha, date_start, date_end);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //-------------------- NEW GET DATA FILTER -----------------------------
        public JsonResult GetDataFilterNew(string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            IDataTable result = new IDataTable();
            profit_center = (string.IsNullOrWhiteSpace(profit_center) ? "0" : profit_center);
            be_id = (string.IsNullOrWhiteSpace(be_id) ? "0" : be_id);
            contract_no = (string.IsNullOrWhiteSpace(contract_no) ? "0" : contract_no);
            customer_id = (string.IsNullOrWhiteSpace(customer_id) ? "0" : customer_id);
            badan_usaha = (string.IsNullOrWhiteSpace(badan_usaha) ? "x" : badan_usaha);
            date_start = (string.IsNullOrWhiteSpace(date_start) ? "x" : date_start);
            date_end = (string.IsNullOrWhiteSpace(date_end) ? "x" : date_end);

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    IDataTable data = dal.GetFilternew(draw, start, length, search, KodeCabang, profit_center, be_id, contract_no, customer_id, badan_usaha, date_start, date_end);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //-------------------- GET DATA FILTER HISTORY -----------------------------
        public JsonResult GetDataFilterHistory(string profit_center, string be_id, string contract_no, string customer_id, string badan_usaha, string date_start, string date_end)
        {
            IDataTable result = new IDataTable();
            profit_center = (string.IsNullOrWhiteSpace(profit_center) ? "0" : profit_center);
            be_id = (string.IsNullOrWhiteSpace(be_id) ? "0" : be_id);
            contract_no = (string.IsNullOrWhiteSpace(contract_no) ? "0" : contract_no);
            customer_id = (string.IsNullOrWhiteSpace(customer_id) ? "0" : customer_id);
            badan_usaha = (string.IsNullOrWhiteSpace(badan_usaha) ? "x" : badan_usaha);
            date_start = (string.IsNullOrWhiteSpace(date_start) ? "x" : date_start);
            date_end = (string.IsNullOrWhiteSpace(date_end) ? "x" : date_end);

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    IDataTable data = dal.GetFilterHistory(draw, start, length, search, KodeCabang, profit_center, be_id, contract_no, customer_id, badan_usaha, date_start, date_end);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //-------------------- NEW GET BE -----------------------
        public JsonResult GetDataBusinessEntityNew()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntitynew(KodeCabang);
            return Json(new { data = result });
        }


        public JsonResult ConfirmationDate(DataNotificationContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
                    bool result = dal.ConfirmationDate(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Email Has Been Stopped."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

    }
}