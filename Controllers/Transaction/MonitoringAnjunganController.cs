﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Helpers;
using Remote.Models.DynamicDatatable;
using Remote.Helper;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class MonitoringAnjunganController : Controller
    {
        // GET: MonitorAnjungan
        public ActionResult Index()
        {
            return View("MonitoringAnjunganIndex");
        }


        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringAnjunganDAL dal = new MonitoringAnjunganDAL();
                    IDataTable data = dal.GetDatanew(draw, start, length, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetail(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int id_n = int.Parse(id);
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    
                    MonitoringAnjunganDAL dal = new MonitoringAnjunganDAL();
                    IDataTable data = dal.GetDataDetail(draw, start, length, search, id_n);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult GetHeader(string id)
        {
            CONTRACT_ANJUNGAN_HEADER result = new CONTRACT_ANJUNGAN_HEADER();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    int id_n = int.Parse(id);
                    MonitoringAnjunganDAL dal = new MonitoringAnjunganDAL();
                    CONTRACT_ANJUNGAN_HEADER data = dal.GetHeader(id_n);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult Reject([FromBody] Models.MonAnjunganParam param)
        {
            string id = Base64Utils.DecodeFrom64(param.mimoto);
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var user = CurrentUser;
                    MonitoringAnjunganDAL dal = new MonitoringAnjunganDAL();
                    var r = dal.SetReject(int.Parse(id), param.note);
                    if (r > 0)
                    {
                        result = new
                        {
                            status = "S",
                            message = "Rejected Successfully "
                        };
                    }
                    else
                    {
                        result = new
                        {
                            status = "E",
                            message = "Failed to Reject Data. "
                        };
                    }
                }
                catch (Exception e)
                {
                    result = new
                    {
                        status = "E",
                        message = "Failed to Reject Data. " + e.Message
                    };
                }
            }

            return Json(result);
        }
    }
}