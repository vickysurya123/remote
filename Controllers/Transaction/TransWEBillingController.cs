﻿using System;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransWEBilling;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Remote.Models.TransVB;

namespace Remote.Controllers
{
    [Authorize]
    public class TransWEBillingController : Controller
    {
        //
        // GET: /TransWEBilling/
        public ActionResult Index()
        {
            return View("TransWEBillingIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //------------------- DATATABLE TRANS WE TRANS BILLING----------------------------
        public JsonResult GetDataWETransBilling()
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransWEBilling(draw, start, length, search, status_dinas, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATATABLE TRANS WE TRANS BILLING NEW----------------------------
        public JsonResult GetDataWETransBillingNew()
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransWEBillingnew(draw, start, length, search, status_dinas, KodeCabang, CurrentUser.UserID);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATATABLE TRANS WE TRANS BILLING DINAS ----------------------------
        public JsonResult GetDataWETransBillingDinas()
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransWEBillingDinas(draw, start, length, search, status_dinas, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATATABLE TRANS WE TRANS BILLING DINAS NEW----------------------------
        public JsonResult GetDataWETransBillingDinasNew()
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransWEBillingDinasnew(draw, start, length, search, status_dinas, KodeCabang, CurrentUser.UserID);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------GET DATA TRANSACTION-----------
        public JsonResult GetDataTransaction(string id_trans, string installation_code)
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransactionWE(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataTransactionDinas(string id_trans, string installation_code)
        {
            DataTablesTransWEBilling result = new DataTablesTransWEBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    result = dal.GetDataTransactionWEDinas(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------POSTING BILLING-------------
        public JsonResult PostingBilling(string id)
        {
            dynamic message = null;

            try
            {
                TransWEBillingDAL dal = new TransWEBillingDAL();
                message = dal.PostingBilling(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }


        //--POSTING BILLING TO SAP
        [HttpPost]
        public JsonResult postToSAP([FromBody] PostSAP data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    var result = dal.PostSAP(data.BILL_ID, data.BILL_TYPE, data.POSTING_DATE);

                    //string.IsNullOrEmpty(result.DOCUMENT_NUMBER) == false
                    // Before result.DOCUMENT_NUMBER != null
                    if (string.IsNullOrEmpty(result.DOCUMENT_NUMBER) || result.DOCUMENT_NUMBER == "/0000")
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "E",
                            message = "Failed to Post Data to SAP."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            bill_id = result.BILL_ID,
                            status = "S",
                            message = "Billing ID " + result.BILL_ID + ", " + "Data Has Been Posted to SAP With Document Number " + result.DOCUMENT_NUMBER
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult GetMsgSAP([FromBody] string BILL_ID)
        {
            TransWEBillingDAL dal = new TransWEBillingDAL();
            Array ret = dal.GetDataMsgSAP(BILL_ID);
            return Json(ret);
        }

        [HttpPost]
        public JsonResult sendToSilapor([FromBody] string BILL_ID)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    TransWEBillingDAL dal = new TransWEBillingDAL();
                    var result = dal.PostSilapor(BILL_ID);

                    message = new
                    {
                        status = "S",
                        message = "Data Has Been Posted to SAP With Document Number "
                    };
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}