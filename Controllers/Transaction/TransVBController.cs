﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransVB;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SAP_CHECK_PIUTANG;
using Remote.Models.TransRentalRequest;
//using Remote.SAP_CEK_PIUTANG;

namespace Remote.Controllers
{
    [Authorize]
    public class TransVBController : Controller
    {
        //
        // GET: /TransVB/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            ViewBag.KodeCabang = KodeCabang;
            ViewBag.UserRole = UserRole;
            return View("TransVBIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddTransVB()
         {
             string KodeCabang = CurrentUser.KodeCabang;             
             TransVBDAL dal = new TransVBDAL();
             var viewModel = new TransVB
             {
                 DDProfitCenters = dal.GetDataProfitCenter(KodeCabang),
                 DDServiceGroup = dal.GetDataService(),
             };
             return View(viewModel);
         }

        //------------------- DATATABLE TRANS VB TRANS ----------------------------
        public JsonResult GetDataVBTrans()
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    TransVBDAL dal = new TransVBDAL();
                    result = dal.GetDataTransVB(draw, start, length, search, KodeCabang, UserRole);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------GET DATA TRANSACTION-----------
        public JsonResult GetDataTransaction(string id)
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransVBDAL dal = new TransVBDAL();
                    result = dal.GetDataTransactionVB(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------ DROP DOWN CUSTOMER -------------------------
        public ActionResult Customer(string MPLG_NAMA)
        {
            string UserRole = CurrentUser.UserRoleID;
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBDAL dal = new TransVBDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang, UserRole);
            return Json(xcustomer);
        }


        //------------------ DROP DOWN SERVICE CODE -------------------------
        public ActionResult ServiceCode(string SERVICE_NAME)
        {
            TransVBDAL dal = new TransVBDAL();
            var xservice = dal.DDServiceCode2(SERVICE_NAME);
            return Json(xservice);
        }        

        public JsonResult GetDataDropdownSC()
        {
            TransVBDAL dal = new TransVBDAL();
            IEnumerable<DDServiceCode> result = dal.GetDataDDServiceCode();
            return Json(new { data = result });
        }

        //----------------------------- SELECT 2 SERVICE CODE ---------------------
        public ActionResult GetDataServiceCodeSelect2(string q, string page)
        {
            try
            {
                /*
                Select2Gudlap apLists = new Select2Gudlap();
                apLists = _helper.GetGudlap("2", q);
                return Json(apLists);
                 * */
                TransVBDAL dal = new TransVBDAL();
                var serviceCode = dal.GetServiceCodeSelect2("2", q);
                return Json(serviceCode);
                
            }
            catch (Exception)
            {
                return RedirectToAction("login", "auth");
            }
        }
        //----------------------------- INSERT DATA HEADER ------------------------
        public JsonResult SaveHeader(DataTransVB data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransVBDAL dal = new TransVBDAL();
                    var result = dal.AddHeader(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS, 
                            status = "S",
                            message = "Other Services Transaction Added Successfully with Transaction Number: " + result.ID_TRANS
                        }; 
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "E",
                            message = "Failed to Post Data to SAP."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
        
        //----------------------- INSERT DATA DETAIL ---------------------------
        public JsonResult SaveDetail(DataTransVB data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    TransVBDAL dal = new TransVBDAL();
                    bool result = dal.AddDetail(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Other Services Added Successfully"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //--CEK PIUTANG SAP--
        [HttpPost]
        public JsonResult cekPiutangSAP([FromBody] CheckPiutangSapParam param)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            retCheckLock retLock = new retCheckLock();
            CheckLockPiutangSoapClient cSAP = new CheckLockPiutangSoapClient(CheckLockPiutangSoapClient.EndpointConfiguration.CheckLockPiutangSoap);
            try
            {

                retLock = cSAP.inboundCheckLock_P3Async("", param.DOC_TYPE, KodeCabang, param.CUST_CODE).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                //throw;
            }
            return Json(retLock);
        }


        //--POSTING BILLING TO SAP
        /*
        public JsonResult postToSAP(string BILL_ID, string BILL_TYPE)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    TransVBDAL dal = new TransVBDAL();
                    string result = dal.PostSAP(BILL_ID, BILL_TYPE);

                    if (result.Contains("XRESULT"))
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Posted to SAP Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }
            return Json(message);
        }
        */

        //--POSTING BILLING TO SAP
        public JsonResult postToSAP(string BILL_ID, string BILL_TYPE)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            { 
                try
                {
                    TransVBDAL dal = new TransVBDAL();
                    //string result = dal.PostSAP(BILL_ID, BILL_TYPE);
                    var result = dal.PostSAP(BILL_ID, BILL_TYPE);

                    if (string.IsNullOrEmpty(result.DOCUMENT_NUMBER))
                    {
                         message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "E",
                            message = "Failed to Post Data."
                        };
                    }
                    else 
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "S",
                            message = "Data Has Been Posted to SAP With Document Number " + result.DOCUMENT_NUMBER
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }
        

        public ActionResult GetDataServiceCode(string SERVICE_NAME, string SERVICE_GROUP)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBDAL dal = new TransVBDAL();
            var xcustomer = dal.GetDataServiceCode(SERVICE_NAME, SERVICE_GROUP, KodeCabang);
            return Json(xcustomer);
        }

        //Dropdown service group
        public JsonResult GetDataDropDownServiceGroup()
        {
            TransVBDAL dal = new TransVBDAL();
            IEnumerable<DDServiceGroup> result = dal.GetDataDropDownServiceGroup();
            return Json(new { data = result });
        }

        public JsonResult GetMsgSAP(string BILL_ID)
        {
            TransVBDAL dal = new TransVBDAL();
            Array ret = dal.GetDataMsgSAP(BILL_ID);
            return Json(ret);
        }
    }
}