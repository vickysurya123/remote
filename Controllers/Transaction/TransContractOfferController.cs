﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractOffer;
using Remote.Models.TransRentalRequest;
using Remote.Models.TransRR;
using Remote.Models.PendingList;
using Remote.Models;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Newtonsoft.Json;
using System.IO;
//using System.Web.Services;
using Remote.Helpers;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using Remote.Helper;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using iTextSharp.text;
using iTextSharp.text.pdf;
using RestSharp;
using System.Dynamic;
using System.Collections;
using System.Net.Http;
using Remote.Models.GeneralResult;
using RestSharp.Authenticators;

namespace Remote.Controllers
{
    [Authorize]
    public class TransContractOfferController : Controller
    {
        //
        // GET: /TransContractOffer/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            ViewBag.KodeCabang = KodeCabang;
            ViewBag.UserRole = UserRole;
            ViewBag.PROPERTY_ROLE = CurrentUser.UserRoleProperty;
            return View("TransContractOfferIndex");
        }

        public ActionResult AddOld()
        {
            return View();
        }

        public ActionResult Add(string mimoto = "")
        {
            string KodeTerminal = CurrentUser.KodeTerminal;
            string KodeCabang = CurrentUser.KodeCabang;
            string ProfitCenter = CurrentUser.ProfitCenter;
            string NamaCabang = CurrentUser.NamaCabang;
            var queryString = Request.Query;
            try
            {
                if (string.IsNullOrEmpty(mimoto))
                {
                    //throw new NullReferenceException();
                }
                string id = Base64Utils.DecodeFrom64(mimoto);
                int id_n = int.Parse(id);
                MonitoringAnjunganDAL dal1 = new MonitoringAnjunganDAL();
                CONTRACT_ANJUNGAN_HEADER data = dal1.GetHeader(id_n);
                ViewBag.AnjunganID = data.ID;
            }
            catch (Exception ex)
            {
                ViewBag.AnjunganID = "";
                string aaa = ex.ToString();
            }

            TransRentalRequestDAL dal = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal.GetDataBE2(KodeCabang),
                DDRentalType = dal.GetDataRentalType(),
                DDContractUsage = dal.GetDataContractUsage(),
                DDCurrencys = dal.GetDataCurrency(),
            };
            if(KodeTerminal == "10")
            {
                ViewBag.BE_ID = "1062";
            }

            ViewBag.ProfitCenter = ProfitCenter;
            ViewBag.NamaCabang = NamaCabang;
            return View(viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddTransContractOffer()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            TransContractOfferDAL dal = new TransContractOfferDAL();

            var viewModel = new ContractOffer
            {
                DDBusinessEntitys = dal.GetDataBE(KodeCabang),
                DDProfitCenters = dal.GetDataProfitCenter(KodeCabang),
                DDContractUsages = dal.GetDataContractUsage(),
            };
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult InsertSurat(DataSurat data, IEnumerable<IFormFile> filex)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;

            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                TransContractOfferDAL dal = new TransContractOfferDAL();

                var r = new List<UploadFilesResult>();
                List<object> listUrl = new List<object>();

                var LIST_FILE = JsonConvert.DeserializeObject<dynamic>(data.LIST_FILE);

                foreach (var item in LIST_FILE)
                {
                    listUrl.Add(new
                    {
                        directory = item.DIRECTORY,
                        file_name = item.FILE_NAME
                    });
                }


                /***//*//*if (filex.Count() > 0)
                {
                    foreach (var file in filex)
                    {
                        //var split = data.NO_SURAT.Replace('/', '-');
                        //string subPath = split;
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        string filePath = Path.Combine(rootPath, "REMOTE", "ImageSurat");

                        bool exists = Directory.Exists(filePath);

                        if (!exists)
                            Directory.CreateDirectory(filePath);

                        var sFile = "";
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileExt = Path.GetExtension(file.FileName);

                        sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                        string savedFileName = Path.Combine(filePath, sFile);

                        using (var stream = new FileStream(savedFileName, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        //string directory = "~/REMOTE/ImageSurat/" + subPath;
                        //string KodeCabang = CurrentUser.KodeCabang;
                        dynamic getData = null;
                        GetUrl test = new GetUrl();
                        string url = test.UploadCloadUrl + "upload";
                        RestClient client = new RestClient(url);
                        RestRequest request = new RestRequest(Method.POST)
                        {
                            RequestFormat = DataFormat.None,
                            AlwaysMultipartFormData = true
                        };
                        request.AddHeader("content-type", "multipart/form-data");
                        request.AddHeader("Accept", "application/json");
                        request.AddParameter("folder", "REMOTE/FileSurat");
                        request.AddFile("file", savedFileName);
                        request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                        IRestResponse responseOrg = client.Execute<dynamic>(request);
                        var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                        string file2 = list.message.url;

                        //var x = new ExpandoObject() as IDictionary<string, object>;
                        //x["directory"] = file2;
                        //x["nama"] = fileName;

                        listUrl.Add(new
                        {
                            directory = file2,
                            file_name = fileName
                        });

                        r.Add(new UploadFilesResult()
                        {
                            Name = sFile,
                            Length = Convert.ToInt32(file.Length),
                            Type = file.ContentType,
                        });
                    }
                }*/

                result = dal.AddDataSurat(data, dataUser, "", "", listUrl);

            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Json(result);

        }

        public JsonResult EditDataSurat(DataSurat data, IEnumerable<IFormFile> filex)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                TransContractOfferDAL dal = new TransContractOfferDAL();

                var r = new List<UploadFilesResult>();

                List<object> listUrl = new List<object>();

                var LIST_FILE = JsonConvert.DeserializeObject<dynamic>(data.LIST_FILE);

                foreach (var item in LIST_FILE)
                {
                    listUrl.Add(new
                    {
                        directory = item.DIRECTORY,
                        file_name = item.FILE_NAME
                    });
                }

               /* if (filex.Count() > 0)
                {
                    foreach (var file in filex)
                    {
                        //var split = data.NO_SURAT.Replace('/', '-');
                        //string subPath = split;
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        string filePath = Path.Combine(rootPath, "REMOTE", "ImageSurat");

                        bool exists = Directory.Exists(filePath);

                        if (!exists)
                            Directory.CreateDirectory(filePath);

                        var sFile = "";
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileExt = Path.GetExtension(file.FileName);

                        sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                        string savedFileName = Path.Combine(filePath, sFile);

                        using (var stream = new FileStream(savedFileName, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        //string directory = "~/REMOTE/ImageSurat/" + subPath;
                        //string KodeCabang = CurrentUser.KodeCabang;
                        dynamic getData = null;
                        GetUrl test = new GetUrl();
                        string url = test.UploadCloadUrl + "upload";
                        RestClient client = new RestClient(url);
                        RestRequest request = new RestRequest(Method.POST)
                        {
                            RequestFormat = DataFormat.None,
                            AlwaysMultipartFormData = true
                        };
                        request.AddHeader("content-type", "multipart/form-data");
                        request.AddHeader("Accept", "application/json");
                        request.AddParameter("folder", "REMOTE/FileSurat");
                        request.AddFile("file", savedFileName);
                        request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                        IRestResponse responseOrg = client.Execute<dynamic>(request);
                        var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                        string file2 = list.message.url;

                        listUrl.Add(new
                        {
                            directory = file2,
                            file_name = fileName
                        });

                        r.Add(new UploadFilesResult()
                        {
                            Name = sFile,
                            Length = Convert.ToInt32(file.Length),
                            Type = file.ContentType,
                        });
                    }
                }*/

                result = dal.EditDataSurat(data, dataUser, null, null, listUrl);

                
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Json(result);

        }

        //--------------------------- DATA TABLES HEADER CREATED --------------------- 
        public JsonResult GetContractOffer(string id)
        {
            dynamic result = new { Status = "E", Message = "Initial" };

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    var data = dal.EditOffer(id);
                    result = new { Status = "S", Message = data };
                }
                catch (Exception e)
                {
                    result = new { Status = "E", Message = e.Message };
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES HEADER CREATED --------------------- 
        public JsonResult GetDataTransContractOfferCreated()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataTransContractOfferCreatednew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES HEADER WORKFLOW --------------------- 
        public JsonResult GetDataTransContractOfferWorkflow()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataTransContractOfferWorkflownew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES HEADER DATA REVISED --------------------- 
        public JsonResult GetDataTransContractOfferRevised()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataTransContractOfferRevisednew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES HEADER DATA APPROVED --------------------- 
        public JsonResult GetDataTransContractOfferApproved()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;
                    string KodeRole = CurrentUser.UserRoleID;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataTransContractOfferApprovednew(draw, start, length, search, KodeCabang, KodeRole);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL OBJECT --------------------- 
        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL SURAT --------------------- 
        public JsonResult GetDataDetailSurat(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailSurat(draw, start, length, search, id);
                    if (result.draw > 0)
                    {
                        foreach (dynamic data in result.data)
                        {
                            data.ID = UrlEncoder.Encode(data.ID.ToString());
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailCondition(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailMemo(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL SALES RULE --------------------- 
        public JsonResult GetDataDetailSalesBased(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailSalesBased(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL REPORTING RULE --------------------- 
        public JsonResult GetDataDetailReportingRule(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataDetailReportingRule(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataSertifikat()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataSertifikat(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-----------------------------DROPDOWN REQUEST NAME------------------------
        public ActionResult GetDataRequestName(string REQUEST_NAME)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            var xservice = dal.DDRequestName(REQUEST_NAME);
            return Json(xservice);
        }

        //----------------------------AUTOCOMPLETE CUSTOMER MDM--------------------------
        [HttpPost]
        public ActionResult Customer(string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //--------------------------FILTER SEARCHING DATA RENTAL REQUEST----------------------------
        public JsonResult GetDataFilter()
        {
            DataTablesTransRR result = new DataTablesTransRR();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataTransRentalRequest(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------------------GET DATA UNTUK FILTER RO-------------------------------- 
        public JsonResult GetDataFilterRO()
        {
            DataTablesGetRO result = new DataTablesGetRO();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];


                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    DataTablesGetRO data = dal.GetDataFilterRentalObject(draw, start, length, search);

                    result = data;
                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result, "application/json");
        }

        //-------------------------GET DATA RENTAL OBJECT UNTUK CONTRACT OFFFER--------------------
        public JsonResult GetDataRO(string id)
        {
            DataTablesRO result = new DataTablesRO();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataRO(draw, start, length, search, id);

                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------------------- DROP DOWN RENTAL OBJECT --------------
        public JsonResult GetDataDropDownRO(string id)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDRo> result = dal.GetDataRO(id);
            return Json(new { data = result });
        }

        //------------------------------- GET DATA OBJECT --------------
        public JsonResult GetDataObject(string id)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<CO_RO_OBJECT> result = dal.GetDataObject(id);
            return Json(new { data = result });
        }

        //----------------------------- CHECKBOX CONDITION TYPE----------------
        public JsonResult GetCheckboxConditionType()
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<CBConditionType> result = dal.GetDataCheckboxConditonType();
            return Json(new { data = result });
        }

        //-------------------------------- DROP DOWN MEAS TYPE --------------
        public JsonResult GetDataMeasType(string id)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDMeasType> result = dal.GetDataMeasType(id);
            return Json(new { data = result });
        }

        //--------------------------- GET LUAS SESUAI DENGAN MEAS TYPE YANG DIPILIH----
        public JsonResult GetDataLuasMeasType(string id)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDMeasType> result = dal.GetDataLuasMeasType(id);
            return Json(new { data = result });
        }

        //----------------------------- INSERT DATA HEADER ------------------------
        [HttpPost]
        public JsonResult SaveHeader([FromBody] DataTransContractOffer data)
        {
            dynamic message = null;
            //  DataTransContractOffer d = new DataTransContractOffer();
            //d = (DataTransContractOffer) data;
            // var x = JsonConvert.SerializeObject(data);

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    string Currency= data.CURRENCY;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    var result = dal.AddHeader(User.Identity.Name, data, KodeCabang,Currency);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "S",
                            message = "Contract Offer Transaction Added Successfully with Contract Offer Number: " + result.ID_TRANS
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "E",
                            message = "Failed to Add Data. " + result.MessageResult
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------------------- RELEASE WORKFLOW ------------ 
        public JsonResult ReleaseToWorkflow(string data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                string userId = CurrentUser.UserLoginID;
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    var result = dal.ReleaseToWorkflow(CurrentUser.Name, userId, data, KodeCabang);

                    if (result.ResultCode.Equals("S"))
                    {
                        message = new
                        {
                            trans_id = result.TransactionCode,
                            status = "S",
                            message = "This Contract Offer Has Been Released to Workflow "
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.TransactionCode,
                            status = "E",
                            message = "Failed Release to Workflow. " + result.ResultDescription
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong. " + e.Message
                    };
                }
            }

            return Json(message);

        }

        public JsonResult SetApproveWorkflow(ContractOfferApprovalParameter param)
        {
            dynamic message = null;
            ContractOfferApprovalParameter result = new ContractOfferApprovalParameter();

            if (IsAjaxUtil.validate(HttpContext))
            {
                result = param;
                result.BRANCH_ID = CurrentUser.KodeCabang;
                result.LAST_UPDATE_BY = CurrentUser.UserLoginID;
                result.USER_LOGIN = CurrentUser.UserLoginID;
                PendingListDAL dal = new PendingListDAL();
                try
                {
                    var res = dal.setContractOfferApproval(result);
                    if (res != null)
                    {
                        message = new
                        {
                            status = "S",
                            message = "This Contract Offer Has Been Released to Workflow!"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed Released to Workflow"
                        };
                    }
                }
                catch (Exception ex)
                {
                    message = new
                    {
                        status = "E",
                        message = "Your can not continue release to workflow. <br> Error Code: " + ex.Message
                    };
                }

            }
            return Json(message);
        }

        public ActionResult EditOffer(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(id);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;
                ViewBag.SERTIFIKAT = data.SERTIFIKAT;
            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage(),
                DDCurrencys = dal2.GetDataCurrency()
            };
            return View("EditOfferNew", viewModel);
        }

        public ActionResult EditOfferSurat(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(id);
                DataSurat dataSurat = dal.GetDataDisposisi(data.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiHistory> history = dal.GetDataHistoryDisposisi(data.CONTRACT_OFFER_NO);
                IEnumerable<LIST_PARAF> dataParaf = dal.GetDataParaf(dataSurat.ID);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;

                //data surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;

                ViewBag.LINK = dataSurat.LINK;
                ViewBag.ATAS_NAMA = dataSurat.ATAS_NAMA;
                ViewBag.TTD = dataSurat.TTD;
                ViewBag.NAMA_TTD = dataSurat.NAMA_TTD;
                ViewBag.NAMA_KEPADA = dataSurat.NAMA_KEPADA;
                ViewBag.NAMA_TEMBUSAN = dataSurat.NAMA_TEMBUSAN;
                ViewBag.TEMBUSAN = dataSurat.TEMBUSAN;
                ViewBag.KEPADA = dataSurat.KEPADA;
                ViewBag.KEPADA_EKSTERNAL = dataSurat.KEPADA_EKSTERNAL;
                ViewBag.TEMBUSAN_EKSTERNAL = dataSurat.TEMBUSAN_EKSTERNAL;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_DETAIL_PARAF = dataParaf;

                if (dataSurat.TANGGAL != null)
                {
                    DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                    var tgl = Convert.ToDateTime(dataSurat.TANGGAL, usDtfi);
                    ViewBag.TANGGAL = tgl.ToString("dd/MM/yyyy");
                } else
                {
                    ViewBag.TANGGAL = dataSurat.TANGGAL;
                }
                
            }
            catch (Exception e)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("EditOfferSurat", viewModel);
        }

        public ActionResult AddSurat(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(id);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;
            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("AddSurat", viewModel);
        }

        public ActionResult OldEditOffer(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(id);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;

            }
            catch (Exception)
            {

            }

            return View("EditOffer");
        }

        //-------------------------------- UPDATE STATUS CONTRACT ------------ 
        [HttpPost]
        public JsonResult EditStatus1([FromBody] DataTransContrOff data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    bool result2 = dal.UpdateStatusContractDAL(data);

                    if (result2)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult EditStatusApproval1(DataTransContrOff data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    bool result2 = dal.UpdateStatusApprovalDAL(data);

                    if (result2)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataDetailConditionEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailConditionEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataDetailManualFrequencyEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailManualFrequencyEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataDetailMemoEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailMemoEdit(id, KodeCabang);
            return Json(new { data = result });
        }


        //-------------------------------------- UPDATE HEADER --------------------
        [HttpPost]
        public JsonResult UpdateHeader([FromBody] DataTransContractOffer data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    string Currency = data.CURRENCY;
                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    bool result = dal.UpdateHeader(User.Identity.Name, data, KodeCabang, User.Identity.Name, Currency);

                    if (result)
                    {
                        message = new
                        {
                            data = data.CONTRACT_OFFER_NO,
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //---------------------------------- HISTORY WORFKLOW -----------------------
        public JsonResult GetDataHistoryWorkflow(string id)
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataHistoryWorkflownew(draw, start, length, search, KodeCabang, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        
        [HttpPost]
        public async Task<ContentResult> UploadFiles([FromForm] AttachmentOffer data)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "ContractOffer", subPath);

                bool exists = System.IO.Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/ContractOffer/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

				dynamic getData = null;
                GetUrl test = new GetUrl();
                string url = test.UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/ContractOffer/" + subPath);
                request.AddFile("file", savedFileName);
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;

                TransContractOfferDAL dal = new TransContractOfferDAL();
                bool result = dal.AddFiles(sFile, file2, KodeCabang, data.BE_ID);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpGet]
        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachmentOffer result = new DataTablesAttachmentOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "ContractOffer", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                TransContractOfferDAL dal = new TransContractOfferDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public string TerbilangINA(double amount, string valuta)
        {
            string valutas = (valuta == "USD") ? " DOLLAR" : " RUPIAH";
            string word = "";
            double divisor = 1000000000000.00; double large_amount = 0;
            double tiny_amount = 0;
            double dividen = 0; double dummy = 0;
            string weight1 = ""; string unit = ""; string follower = "";
            string[] prefix = { "SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            string[] sufix = { "SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            large_amount = Math.Abs(Math.Truncate(amount));
            tiny_amount = Math.Round((Math.Abs(amount) - large_amount) * 100);
            if (large_amount > divisor)
                return "OUT OF RANGE";
            while (divisor >= 1)
            {
                dividen = Math.Truncate(large_amount / divisor);
                large_amount = large_amount % divisor;
                unit = "";
                if (dividen > 0)
                {
                    if (divisor == 1000000000000.00)
                        unit = "TRILYUN ";
                    else
                        if (divisor == 1000000000.00)
                        unit = "MILYAR ";
                    else
                            if (divisor == 1000000.00)
                        unit = "JUTA ";
                    else
                                if (divisor == 1000.00)
                        unit = "RIBU ";
                }
                weight1 = "";
                dummy = dividen;
                if (dummy >= 100)
                    weight1 = prefix[(int)Math.Truncate(dummy / 100) - 1] + "RATUS ";
                dummy = dividen % 100;
                if (dummy < 10)
                {
                    if (dummy == 1 && unit == "RIBU ")
                        weight1 += "SE";
                    else
                        if (dummy > 0)
                        weight1 += sufix[(int)dummy - 1];
                }
                else
                    if (dummy >= 11 && dummy <= 19)
                {
                    weight1 += prefix[(int)(dummy % 10) - 1] + "BELAS ";
                }
                else
                {
                    weight1 += prefix[(int)Math.Truncate(dummy / 10) - 1] + "PULUH ";
                    if (dummy % 10 > 0)
                        weight1 += sufix[(int)(dummy % 10) - 1];
                }
                word += weight1 + unit;
                divisor /= 1000.00;
            }
            if (Math.Truncate(amount) == 0)
                word = "NOL ";
            follower = "";
            if (tiny_amount < 10)
            {
                if (tiny_amount > 0)
                    follower = "KOMA NOL " + sufix[(int)tiny_amount - 1];
            }
            else
            {
                follower = "KOMA " + sufix[(int)Math.Truncate(tiny_amount / 10) - 1];
                if (tiny_amount % 10 > 0)
                    follower += sufix[(int)(tiny_amount % 10) - 1];
            }
            word += follower;
            if (amount < 0)
            {
                word = "MINUS " + word + valutas;
            }
            else
            {
                word = word + valutas;
            }
            return word.Trim();
        }

        [HttpPost]
        public JsonResult exportWordIjinprinsip([FromBody] ExportWordIjinprinsipParam param)
        {
            nama result = new nama();
            string fileName = "";

            int Month = Convert.ToInt32(param.TERM_IN_MONTHS) % 12;
            int Years = Convert.ToInt32(param.TERM_IN_MONTHS) / 12;

            IDbConnection connection = DatabaseFactory.GetConnection();
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            //List<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> hasil = null;


            string LUAS = string.Empty;
            if (param.LUAS_TANAH == "0")
            {
                LUAS = param.LUAS_BANGUNAN;
            }
            else if (param.LUAS_BANGUNAN == "0")
            {
                LUAS = param.LUAS_TANAH;
            }
            else
            {
                LUAS = param.LUAS_TANAH;
            }

            string LUAS_NORMALISASI = string.Empty;
            if (param.LUAS_NOR_DARATAN == "0")
            {
                LUAS_NORMALISASI = param.LUAS_NOR_BANGUNAN;
            }
            else if (param.LUAS_NOR_BANGUNAN == "0")
            {
                LUAS_NORMALISASI = param.LUAS_NOR_DARATAN;
            }
            else
            {
                LUAS_NORMALISASI = param.LUAS_NOR_DARATAN;
            }

            string VALID_FROM = string.Empty;
            if (param.VALID_FROM_DARATAN == "-")
            {
                VALID_FROM = param.VALID_FROM_BANGUNAN;
            }
            else if (param.VALID_FROM_BANGUNAN == "-")
            {
                VALID_FROM = param.VALID_FROM_DARATAN;
            }
            else
            {
                VALID_FROM = param.VALID_FROM_DARATAN;
            }

            string VALID_TO = string.Empty;
            if (param.VALID_TO_DARATAN == "-")
            {
                VALID_TO = param.VALID_TO_BANGUNAN;
            }
            else if (param.VALID_TO_BANGUNAN == "-")
            {
                VALID_TO = param.VALID_TO_DARATAN;
            }
            else
            {
                VALID_TO = param.VALID_TO_DARATAN;
            }

            string SEWA = string.Empty;
            if (param.SEWA_BANGUNAN == "0")
            {
                SEWA = param.SEWA_DARATAN;
            }
            else if (param.SEWA_DARATAN == "0")
            {
                SEWA = param.SEWA_BANGUNAN;
            }
            else
            {
                SEWA = param.SEWA_BANGUNAN;
            }

            string SEWA_NOR = string.Empty;
            if (param.NORMALISASI_BANGUNAN == "0")
            {
                SEWA_NOR = param.NORMALISASI_TANAH;
            }
            else if (param.NORMALISASI_TANAH == "0")
            {
                SEWA_NOR = param.NORMALISASI_BANGUNAN;
            }
            else
            {
                SEWA_NOR = param.NORMALISASI_BANGUNAN;
            }

            string TOTALNYA = string.Empty;
            if (param.TOTAL_SEWA_BANGUNAN == "0")
            {
                TOTALNYA = param.TOTAL_SEWA_DARATAN;
            }
            else if (param.TOTAL_SEWA_DARATAN == "0")
            {
                TOTALNYA = param.TOTAL_SEWA_BANGUNAN;
            }
            else
            {
                TOTALNYA = param.TOTAL_SEWA_BANGUNAN;
            }

            string TOTAL_NORMALISASI = string.Empty;
            if (param.TOTAL_NOR_BANGUNAN == "0")
            {
                TOTAL_NORMALISASI = param.TOTAL_NOR_DARATAN;
            }
            else if (param.TOTAL_NOR_DARATAN == "0")
            {
                TOTAL_NORMALISASI = param.TOTAL_NOR_BANGUNAN;
            }
            else
            {
                TOTAL_NORMALISASI = param.TOTAL_NOR_BANGUNAN;
            }

            string NJOP_PERCENTT = string.Empty;
            if (param.NJOP_BANGUNAN == "0")
            {
                NJOP_PERCENTT = param.NJOP_DARATAN;
            }
            else if (param.NJOP_DARATAN == "0")
            {
                NJOP_PERCENTT = param.NJOP_BANGUNAN;
            }
            else
            {
                NJOP_PERCENTT = param.NJOP_BANGUNAN;
            }

            //string VALID_FROM = string.Empty;
            //if (NJOP_BANGUNAN == null)
            //{
            //    NJOP_PERCENTT = NJOP_DARATAN;
            //}
            //else if (NJOP_DARATAN == null)
            //{
            //    NJOP_PERCENTT = NJOP_BANGUNAN;
            //}
            //else
            //{
            //    NJOP_PERCENTT = NJOP_BANGUNAN;
            //}

            var JUMLAH_SEWA = 0.0;
            if (NJOP_PERCENTT == "0")
            {
                JUMLAH_SEWA = Convert.ToDouble(LUAS) * Convert.ToDouble(SEWA) * Convert.ToDouble(Years);
            }
            else
            {
                JUMLAH_SEWA = Convert.ToDouble(LUAS) * (Convert.ToDouble(NJOP_PERCENTT) * Convert.ToDouble(SEWA)) * Convert.ToDouble(Years);
            }

            var JUMLAH_NOR = 0.0;
            if (param.NJOP_NORMALISASI == "0")
            {
                JUMLAH_NOR = Convert.ToDouble(LUAS_NORMALISASI) * Convert.ToDouble(SEWA_NOR) * Convert.ToDouble(Years);
            }
            else
            {
                JUMLAH_NOR = Convert.ToDouble(LUAS_NORMALISASI) * (Convert.ToDouble(param.NJOP_NORMALISASI) * Convert.ToDouble(SEWA_NOR)) * Convert.ToDouble(Years);
            }

            //var JUMLAH_SEWA = Convert.ToDouble(LUAS) * (Convert.ToDouble(NJOP_PERCENT) * Convert.ToDouble(SEWA)) * Convert.ToDouble(Years);            
            var PPN = 0.1 * Convert.ToDouble(TOTALNYA);

            var JUMLAH_SELURUH = Convert.ToDouble(TOTALNYA) + Convert.ToDouble(PPN) + Convert.ToDouble(param.ADMINISTRASI) + Convert.ToDouble(param.WARMERKING) + 6000;

            //var JUMLAH_NOR = Convert.ToDouble(LUAS_NORMALISASI) * Years;

            var PPN_NOR = 0.1 * Convert.ToDouble(JUMLAH_NOR);

            var JUMLAH_NORMALISASI = Convert.ToDouble(JUMLAH_NOR) + Convert.ToDouble(PPN_NOR);

            var TERBILANG = TerbilangINA(Convert.ToDouble(JUMLAH_SELURUH), "RUPIAH");
            var ADMIN = Convert.ToDouble(param.ADMINISTRASI);
            var WARME = Convert.ToDouble(param.WARMERKING);
            var SUM = Convert.ToDouble(TOTALNYA);
            var SEW = Convert.ToDouble(SEWA);

            var TOTAL_SEMUA = Convert.ToDouble(JUMLAH_SELURUH) + Convert.ToDouble(JUMLAH_NORMALISASI);

            string BULAN = String.Format("{0:MMMM}", VALID_FROM);

            var application = new Microsoft.Office.Interop.Word.Application();
            var document = new Microsoft.Office.Interop.Word.Document();

            if (param.CONTRACT_OFFER_TYPE == "ZC01 - Permohonan Baru")
            {
                //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/Persetujuanpenggunaanbaru(ZC01).docx")));
                //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/FormIjinPrinsip.docx")));

                application.Visible = true;
                fileName = "ZC01PermohonanBaru";
            }
            else if (param.CONTRACT_OFFER_TYPE == "ZC02 - Permohonan Perpanjangan")
            {
                //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/FormIjinPrinsip.docx")));
                application.Visible = true;
                fileName = "ZC02PermohonanPerpanjangan";
            }
            else if (param.CONTRACT_OFFER_TYPE == "ZC03 - Permohonan Peralihan")
            {
                //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/FormIjinPrinsip.docx")));
                application.Visible = true;
                fileName = "ZC03PermohonanPeralihan";
            }
            else if (param.CONTRACT_OFFER_TYPE == "ZC04 - Permohonan Keringanan")
            {
                //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/FormIjinPrinsip.docx")));
                application.Visible = true;
                fileName = "ZC04PermohonanKeringanan";
            }

            foreach (Microsoft.Office.Interop.Word.Field field in document.Fields)
            {
                if (field.Code.Text.Contains("Kota"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_CITY);
                }
                else if (field.Code.Text.Contains("Customername"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CUSTOMER_NAME);
                }
                else if (field.Code.Text.Contains("Cabang"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_ID);
                }
                else if (field.Code.Text.Contains("Luas"))
                {
                    field.Select();
                    application.Selection.TypeText(LUAS);

                    //foreach(var item in hasil)
                    //{
                    //    if(item.CALC_OBJECT != "Header Document")
                    //    {
                    //        application.Selection.TypeText(LUAS);
                    //    }
                    //    else
                    //    {

                    //    }
                    //}

                }
                else if (field.Code.Text.Contains("AlamatRO"))
                {
                    field.Select();
                    application.Selection.TypeText(param.RO_ADDRESS);
                }
                else if (field.Code.Text.Contains("Contractusage"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_USAGE);
                }
                else if (field.Code.Text.Contains("Periode"))
                {
                    field.Select();
                    application.Selection.TypeText(param.TERM_IN_MONTHS);
                }
                else if (field.Code.Text.Contains("Tahun"))
                {
                    field.Select();
                    if (Years == 0)
                    {
                        application.Selection.TypeText("");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Years) + " Tahun ");
                    }
                }
                else if (field.Code.Text.Contains("Bulan"))
                {
                    field.Select();
                    if (Month == 0)
                    {
                        application.Selection.TypeText(" ");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Month) + " Bulan ");
                    }
                }
                else if (field.Code.Text.Contains("Contractstartdate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_START_DATE);
                }
                else if (field.Code.Text.Contains("Contractenddate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_END_DATE);
                }
                else if (field.Code.Text.Contains("Regional"))
                {
                    field.Select();
                    application.Selection.TypeText(param.REGIONAL_NAME);
                }
                else if (field.Code.Text.Contains("Total"))
                {
                    field.Select();
                    application.Selection.TypeText(SUM.ToString("n0"));
                }
                else if (field.Code.Text.Contains("Validfrom"))
                {
                    field.Select();
                    application.Selection.TypeText(VALID_FROM);
                }
                else if (field.Code.Text.Contains("Validto"))
                {
                    field.Select();
                    application.Selection.TypeText(VALID_TO);
                }
                else if (field.Code.Text.Contains("Profitcenter"))
                {
                    field.Select();
                    application.Selection.TypeText(param.PROFIT_CENTER_NAME);
                }
                else if (field.Code.Text.Contains("Sewa"))
                {
                    field.Select();
                    application.Selection.TypeText(SEW.ToString("n0"));
                }
                //else if (field.Code.Text.Contains("Sewabangunan"))
                //{
                //    field.Select();
                //    application.Selection.TypeText(SEWA_BANGUNAN);
                //}
                else if (field.Code.Text.Contains("Normalisasi"))
                {
                    field.Select();
                    application.Selection.TypeText(SEWA_NOR);
                }
                else if (field.Code.Text.Contains("Pengalihan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.PENGALIHAN);
                }
                else if (field.Code.Text.Contains("Administrasi"))
                {
                    field.Select();
                    application.Selection.TypeText(ADMIN.ToString("n0"));
                }
                else if (field.Code.Text.Contains("Warmerking"))
                {
                    field.Select();
                    application.Selection.TypeText(WARME.ToString("n0"));
                }
                else if (field.Code.Text.Contains("Njop"))
                {
                    field.Select();
                    application.Selection.TypeText(NJOP_PERCENTT);
                }
                else if (field.Code.Text.Contains("JumSewa"))
                {
                    field.Select();
                    application.Selection.TypeText(JUMLAH_SEWA.ToString());
                }
                else if (field.Code.Text.Contains("Keseluruhan"))
                {
                    field.Select();
                    application.Selection.TypeText(JUMLAH_SELURUH.ToString("n0"));
                }
                else if (field.Code.Text.Contains("PPN"))
                {
                    field.Select();
                    application.Selection.TypeText(PPN.ToString("n0"));
                }
                else if (field.Code.Text.Contains("JumNor"))
                {
                    field.Select();
                    application.Selection.TypeText(JUMLAH_NOR.ToString());
                }
                else if (field.Code.Text.Contains("PPNor"))
                {
                    field.Select();
                    application.Selection.TypeText(PPN_NOR.ToString());
                }
                else if (field.Code.Text.Contains("FullNor"))
                {
                    field.Select();
                    application.Selection.TypeText(JUMLAH_NORMALISASI.ToString());
                }
                else if (field.Code.Text.Contains("Terbilang"))
                {
                    field.Select();
                    application.Selection.TypeText(TERBILANG.ToString());
                }
                else if (field.Code.Text.Contains("TotalSemua"))
                {
                    field.Select();
                    application.Selection.TypeText(TOTAL_SEMUA.ToString());
                }
                else if (field.Code.Text.Contains("LuasNor"))
                {
                    field.Select();
                    application.Selection.TypeText(LUAS_NORMALISASI.ToString());
                }
                else if (field.Code.Text.Contains("TotalNor"))
                {
                    field.Select();
                    application.Selection.TypeText(TOTAL_NORMALISASI.ToString());
                }
                else if (field.Code.Text.Contains("NjopNor"))
                {
                    field.Select();
                    application.Selection.TypeText(param.NJOP_NORMALISASI.ToString());
                }
                else if (field.Code.Text.Contains("ValidFromNor"))
                {
                    field.Select();
                    application.Selection.TypeText(param.VALID_FROM_NORMALISASI.ToString());
                }
                else if (field.Code.Text.Contains("ValidToNor"))
                {
                    field.Select();
                    application.Selection.TypeText(param.VALID_TO_NORMALISASI.ToString());
                }

                //else if (field.Code.Text.Contains("Warmeking"))
                //{
                //    if (CONDITION_TYPE == "Z010 Warmeking")
                //    {
                //        field.Select();
                //        application.Selection.TypeText(UNIT_PRICE);
                //    }
                //    else
                //    {
                //        field.Select();
                //        application.Selection.TypeText(" - ");
                //    }

                //}
            }

            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "DownloadForm", fileName + ".docx");
            document.SaveAs(filePath);

            //document.SaveAs(FileName: @"C:\testREMOTE\" + fileName + ".docx");


            document.Close();
            application.Quit();

            result.fileNames = fileName + ".docx";
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult exportWordIjinprinsipDS([FromBody] string CONTRACT_OFFER_NO)
        {
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;
            TransContractOfferDAL dal = new TransContractOfferDAL();
            listData = dal.GetDataDetailObjectP(CONTRACT_OFFER_NO);

            return Json(new { data = listData });
        }

        [HttpPost]
        public JsonResult exportWordIjinprinsipD([FromBody] ExportWordIjinPrinsiDParam param)
        {
            nama result = new nama();

            int Month = Convert.ToInt32(param.TERM_IN_MONTHS) % 12;
            int Years = Convert.ToInt32(param.TERM_IN_MONTHS) / 12;

            string LUAS = string.Empty;
            if (param.LUAS_TANAH == "0")
            {
                LUAS = param.LUAS_BANGUNAN;
            }
            else if (param.LUAS_BANGUNAN == "0")
            {
                LUAS = param.LUAS_TANAH;
            }
            else
            {
                LUAS = param.LUAS_TANAH;
            }

            var application = new Microsoft.Office.Interop.Word.Application();
            var document = new Microsoft.Office.Interop.Word.Document();

            //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormIjinPrinsip/FormIjinPrinsipCabang/PenggunaJasa.docx")));
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "FormIjinPrinsip", "FormIjinPrinsip.docx");
            document = application.Documents.Add(Template: filePath);

            application.Visible = true;

            foreach (Microsoft.Office.Interop.Word.Field field in document.Fields)
            {
                if (field.Code.Text.Contains("Kota"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_CITY);
                }
                else if (field.Code.Text.Contains("Customername"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CUSTOMER_NAME);
                }
                else if (field.Code.Text.Contains("Cabang"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_ID);
                }
                else if (field.Code.Text.Contains("Luas"))
                {
                    field.Select();
                    application.Selection.TypeText(LUAS);
                }
                else if (field.Code.Text.Contains("AlamatRO"))
                {
                    field.Select();
                    application.Selection.TypeText(param.RO_ADDRESS);
                }
                else if (field.Code.Text.Contains("Contractusage"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_USAGE);
                }
                else if (field.Code.Text.Contains("Periode"))
                {
                    field.Select();
                    application.Selection.TypeText(param.TERM_IN_MONTHS);
                }
                else if (field.Code.Text.Contains("Tahun"))
                {
                    field.Select();
                    if (Years == 0)
                    {
                        application.Selection.TypeText("");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Years) + " Tahun ");
                    }
                }
                else if (field.Code.Text.Contains("Bulan"))
                {
                    field.Select();
                    if (Month == 0)
                    {
                        application.Selection.TypeText(" ");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Month) + " Bulan ");
                    }
                }
                else if (field.Code.Text.Contains("Contractstartdate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_START_DATE);
                }
                else if (field.Code.Text.Contains("Contractenddate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_END_DATE);
                }
                else if (field.Code.Text.Contains("CreationDate"))
                {
                    field.Select();
                    if (param.CREATION_DATE == null)
                    {
                        application.Selection.TypeText(".........");
                    }
                    else
                    {
                        application.Selection.TypeText(param.CREATION_DATE);
                    }
                }
                else if (field.Code.Text.Contains("Sewadaratan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.SEWA_DARATAN);
                }
                else if (field.Code.Text.Contains("Sewabangunan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.SEWA_BANGUNAN);
                }
                else if (field.Code.Text.Contains("Normalisasi"))
                {
                    field.Select();
                    application.Selection.TypeText(param.NORMALISASI);
                }
                else if (field.Code.Text.Contains("Pengalihan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.PENGALIHAN);
                }
                else if (field.Code.Text.Contains("Administrasi"))
                {
                    field.Select();
                    application.Selection.TypeText(param.ADMINISTRASI);
                }
                else if (field.Code.Text.Contains("Warmerking"))
                {
                    field.Select();
                    application.Selection.TypeText(param.WARMERKING);
                }
            }
            string filePath2 = Path.Combine(rootPath, "REMOTE", "DownloadForm", "FormIjinPrinsip.docx");
            document.SaveAs(filePath2);

            document.Close();
            application.Quit();

            result.fileNames = "PenggunaJasa.docx";
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult exportWordIjinprinsipP([FromBody] string CONTRACT_OFFER_NO)
        {
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> listData = null;
            TransContractOfferDAL dal = new TransContractOfferDAL();
            listData = dal.GetDataDetailObjectP(CONTRACT_OFFER_NO);

            return Json(new { data = listData });
        }

        [HttpPost]
        public JsonResult exportWordIjinprinsipQ([FromBody] ExportWordIjinprinsipQParam param)
        {
            nama result = new nama();

            int Month = Convert.ToInt32(param.TERM_IN_MONTHS) % 12;
            int Years = Convert.ToInt32(param.TERM_IN_MONTHS) / 12;

            string LUAS = string.Empty;
            if (param.LUAS_TANAH == "0")
            {
                LUAS = param.LUAS_BANGUNAN;
            }
            else if (param.LUAS_BANGUNAN == "0")
            {
                LUAS = param.LUAS_TANAH;
            }
            else
            {
                LUAS = param.LUAS_TANAH;
            }

            var application = new Microsoft.Office.Interop.Word.Application();
            var document = new Microsoft.Office.Interop.Word.Document();

            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "FormIjinPrinsip", "FormIjinPrinsipCabang", "RekomendasiGMDireksi.docx");
            document = application.Documents.Add(Template: filePath);
            application.Visible = true;

            foreach (Microsoft.Office.Interop.Word.Field field in document.Fields)
            {
                if (field.Code.Text.Contains("CreationDate"))
                {
                    field.Select();
                    if (param.CREATION_DATE == null)
                    {
                        application.Selection.TypeText(".........");
                    }
                    else
                    {
                        application.Selection.TypeText(param.CREATION_DATE);
                    }
                }
                else if (field.Code.Text.Contains("Kota"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_CITY);
                }
                else if (field.Code.Text.Contains("Customername"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CUSTOMER_NAME);
                }
                else if (field.Code.Text.Contains("Cabang"))
                {
                    field.Select();
                    application.Selection.TypeText(param.BE_ID);
                }
                else if (field.Code.Text.Contains("Luas"))
                {
                    field.Select();
                    application.Selection.TypeText(LUAS);
                }
                else if (field.Code.Text.Contains("AlamatRO"))
                {
                    field.Select();
                    application.Selection.TypeText(param.RO_ADDRESS);
                }
                else if (field.Code.Text.Contains("Contractusage"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_USAGE);
                }
                else if (field.Code.Text.Contains("Periode"))
                {
                    field.Select();
                    application.Selection.TypeText(param.TERM_IN_MONTHS);
                }
                else if (field.Code.Text.Contains("Tahun"))
                {
                    field.Select();
                    if (Years == 0)
                    {
                        application.Selection.TypeText("");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Years) + " Tahun ");
                    }
                }
                else if (field.Code.Text.Contains("Bulan"))
                {
                    field.Select();
                    if (Month == 0)
                    {
                        application.Selection.TypeText(" ");
                    }
                    else
                    {
                        application.Selection.TypeText(Convert.ToString(Month) + " Bulan ");
                    }
                }
                else if (field.Code.Text.Contains("Contractstartdate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_START_DATE);
                }
                else if (field.Code.Text.Contains("Contractenddate"))
                {
                    field.Select();
                    application.Selection.TypeText(param.CONTRACT_END_DATE);
                }
                else if (field.Code.Text.Contains("Sewadaratan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.SEWA_DARATAN);
                }
                else if (field.Code.Text.Contains("Sewabangunan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.SEWA_BANGUNAN);
                }
                else if (field.Code.Text.Contains("Normalisasi"))
                {
                    field.Select();
                    application.Selection.TypeText(param.NORMALISASI);
                }
                else if (field.Code.Text.Contains("Pengalihan"))
                {
                    field.Select();
                    application.Selection.TypeText(param.PENGALIHAN);
                }
                else if (field.Code.Text.Contains("Administrasi"))
                {
                    field.Select();
                    application.Selection.TypeText(param.ADMINISTRASI);
                }
                else if (field.Code.Text.Contains("Warmerking"))
                {
                    field.Select();
                    application.Selection.TypeText(param.WARMERKING);
                }
            }

            string filePath2 = Path.Combine(rootPath, "REMOTE", "DownloadForm", "RekomendasiGMDireksi.docx");
            document.SaveAs(filePath2);

            //document.SaveAs(FileName: @"C:\testREMOTE\RekomendasiGMDireksi.docx");
            //document.SaveAs(FileName: @"C:\testREMOTE\RekomendasiGMDireksi.docx");
            document.Close();
            application.Quit();

            result.fileNames = "RekomendasiGMDireksi.docx";
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpGet]
        public JsonResult deleteWord(string filename)
        {
            dynamic message = null;

            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "DownloadForm", filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        public JsonResult GetDataDropDownCoaProduksi()
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownCoaProduksi();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownCoaProduksiLahan(string search)
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownCoaProduksiLahan(search);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownCoaProduksiWater()
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownCoaProduksiWater();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownCoaProduksiElectricity()
        {
            TransContractOfferDAL dal = new TransContractOfferDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownCoaProduksiElectricity();
            return Json(new { data = result });
        }

        ////----------------------- INSERT DATA DETAIL CONDITION------------------------
        //public JsonResult SaveDetailCondition(TransContractOfferCondition data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailCondition(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Other Services Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        ////----------------------- INSERT DATA DETAIL MANUAL FREQUENCY-------------------
        //public JsonResult SaveDetailManualFrequency(TransContractOfferFrequency data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailManualFrequency(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Other Services Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        ////----------------------- INSERT DATA DETAIL MEMO-----------------------
        //public JsonResult SaveDetailMemo(TransContractOfferMemo data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailMemo(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Detail Memo Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        ////----------------------- INSERT DATA DETAIL MEMO-----------------------
        //public JsonResult SaveDetailObject(TransContractOfferObject data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailObject(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Detail Memo Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        //----------------------- INSERT DATA SALES RULE------------------------
        //public JsonResult SaveDetailSalesRule(DataTransContractOffer data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailSalesRule(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Sales rule Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        //----------------------- INSERT DATA REPORTING RULE ------------------------
        //public JsonResult SaveDetailReportingRule(DataTransContractOffer data)
        //{
        //    dynamic message = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
        //    {
        //        try
        //        {
        //            TransContractOfferDAL dal = new TransContractOfferDAL();
        //            bool result = dal.AddDetailReportingRule(User.Identity.Name, data);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Reporting Rule Added Successfully."
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}

        //----------------- DROP DOWN SALES RULE --------------
        //public JsonResult GetDataDropDownSalesRule()
        //{
        //    TransContractOfferDAL dal = new TransContractOfferDAL();
        //    IEnumerable<DDSalesRule> result = dal.GetDataSalesRule();
        //    return Json(new { data = result });
        //}

        ////----------------- GET UNIT SESUAI SALES TYPE YG DIPILIH ----
        //public JsonResult GetDataUnitST(string id)
        //{
        //    TransContractOfferDAL dal = new TransContractOfferDAL();
        //    IEnumerable<DDSalesRule> result = dal.GetDataUnitST(id);
        //    return Json(new { data = result });
        //}
        //-------------------------------- RELEASE WORKFLOW ------------ 
        [HttpPost]
        public ActionResult ReleaseToWorkflowNew(string data)
        {
            dynamic message = null;
            //testing
            //var test = "21000295";
            //data = test;
            if (IsAjaxUtil.validate(HttpContext))
            {
                string userId = CurrentUser.UserLoginID;
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    var result = dal.ReleaseToWorkflowNew(CurrentUser.Name, userId, data, KodeCabang);

                    if (result.ResultCode.Equals("S"))
                    {
                        message = new
                        {
                            trans_id = result.TransactionCode,
                            status = "S",
                            message = "This Contract Offer Has Been Released to Workflow "
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.TransactionCode,
                            status = "E",
                            message = "Failed Release to Workflow. " + result.ResultDescription
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong. " + e.Message
                    };
                }
            }

            return Json(message);

        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IEnumerable<IFormFile> files)
        {
            dynamic dataUser = CurrentUser;
            string directory = string.Empty; ;
            try
            {
                var r = new List<UploadFilesResult>();

                foreach (var file in files)
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    string filePath = Path.Combine(rootPath, "REMOTE", "ImageSummernote");

                    bool exists = Directory.Exists(filePath);

                    if (!exists)
                        Directory.CreateDirectory(filePath);

                    var sFile = "";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileExt = Path.GetExtension(file.FileName);

                    sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                    string savedFileName = Path.Combine(filePath, sFile);

                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    //string directory = "~/REMOTE/SertifikatHpl/" + subPath;
                    string KodeCabang = CurrentUser.KodeCabang;

                    dynamic getData = null;
                    GetUrl test = new GetUrl();
                    string url = test.UploadCloadUrl + "upload";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.None,
                        AlwaysMultipartFormData = true
                    };
                    request.AddHeader("content-type", "multipart/form-data");
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("folder", "REMOTE/ImageIsiSurat");
                    request.AddFile("file", savedFileName);
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    string file2 = list.message.url;

                    directory = file2;

                    r.Add(new UploadFilesResult()
                    {
                        Name = sFile,
                        Length = Convert.ToInt32(file.Length),
                        Type = file.ContentType,
                    });
                }
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }
            return Json(directory);
        }

        [HttpGet]
        public JsonResult GetDataUserEmail(string q)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            DisposisiDal dal = new DisposisiDal();
            IEnumerable<DDEmailEmployee> result = dal.GetDataEmailEmployee(KodeCabang, q);

            return Json(new
            {
                results = result
            });
        }
        [HttpGet]
        public JsonResult GetDataKlasifikasi(string q)
        {
            DisposisiDal dal = new DisposisiDal();
            dynamic result = dal.GetDataKlasifikasi(q);

            return Json(new
            {
                results = result
            });
        }

        [HttpPost]
        public async Task<IActionResult> UploadTemp(IEnumerable<IFormFile> files)
        {
            var x = Request.Form.Files;
            dynamic dataUser = CurrentUser;
            string directory = string.Empty;
            string namaFile = string.Empty;
            var r = new List<UploadFilesResult>();
            try
            {
                
                foreach (var file in files)
                {
                    string url = new GetUrl().UploadCloadUrl + "upload";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.None,
                        AlwaysMultipartFormData = true
                    };
                    request.AddHeader("content-type", "multipart/form-data");
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("folder", "REMOTE/FileSurat");
                    request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");

                    var Name = file.FileName;
                    StreamContent streamContent = new StreamContent(file.OpenReadStream());
                    request.AddFileBytes("file", streamContent.ReadAsByteArrayAsync().Result, file.FileName);

                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    string file2 = list.message.url;
                    directory = file2;

                    r.Add(new UploadFilesResult()
                    {
                        FILE_NAME = Name,
                        DIRECTORY = directory
                    });

                    /*string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    string filePath = Path.Combine(rootPath, "REMOTE", "ImageTemp");

                    bool exists = Directory.Exists(filePath);

                    if (!exists)
                        Directory.CreateDirectory(filePath);

                    var sFile = "";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileExt = Path.GetExtension(file.FileName);

                    sFile = fileName+"_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                    string savedFileName = Path.Combine(filePath, sFile);

                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    directory = sFile;
                    string KodeCabang = CurrentUser.KodeCabang;

                    //directory = file2;

                    r.Add(new UploadFilesResult()
                    {
                        Name = sFile,
                        Length = Convert.ToInt32(file.Length),
                        Type = file.ContentType,
                    });
                }
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }*/
                }
            }

            catch (Exception e)
            {
                string aaa = e.ToString();
            }
            return Json(r);
        }

        [HttpGet]
        public async Task<IActionResult> GetListCoa()
        {
            try
            {
                var queryString = Request.Query;
                string draw = queryString["draw"];
                int start = int.Parse(queryString["start"]);
                int length = int.Parse(queryString["length"]);
                string search = queryString["search[value]"];
                string tes = "%" + search;

                int panjang = ((start + 10) / 10);

                // api get coa
                string url = "https://fuse19.pelindo.co.id/master/mdm/silaporcoaprod/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                if (search != "")
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "407",
                            STAT = "D",
                            DESKRIPSI = tes,
                            COA_PROD = tes,
                            GL_TEXT = tes,
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }
                else
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "407",
                            STAT = "D",
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                //var returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);

                //List<object> result = new List<object>();

                //    result.Add(new
                //    {
                //        draw = draw,
                //        recordsFiltered = returnData.paging.totalpages,
                //        recordsTotal = returnData.paging.total,
                //        data = returnData.result,
                //    });



                return Ok(responseOrg.Content);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = e.Message
                });
            }

        }

        public Results GetDataCurrencyRate()
        {
            Results result = new Results();
            try
            {
                var date = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                // api get currency
                string url = "https://fuse19.pelindo.co.id/master/mdm/safrvalutakurs/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                var body = new
                {
                    identity = new
                    {
                        applicationid = "REMOTE",
                        reqtxnid = "e1",
                        reqdate = DateTime.Now
                    },
                    paging = new
                    {
                        page = "1",
                        limit = "1"
                    },
                    parameter = new
                    {
                        TGL_MULAI_RATE = date,
                        column = "KODE_VALUTA, TGL_MULAI_RATE, NILAI_RATE,KD_AKTIF"
                    }
                };

                request.AddJsonBody(body);

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                dynamic rates = "1";
                if (returnData.status.responsecode != "404")
                {
                    rates = returnData.result[0].NILAI_RATE.ToString();
                } else
                {
                    date = DateTime.Now.AddDays(-4).ToString("yyyy-MM-dd");
                    // api get currency
                    url = "https://fuse19.pelindo.co.id/master/mdm/safrvalutakurs/data/search-criteria";
                    client = new RestClient(url);
                    request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.Json
                    };

                    var bodys = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "e1",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = "1",
                            limit = "1"
                        },
                        parameter = new
                        {
                            TGL_MULAI_RATE = date,
                            column = "KODE_VALUTA, TGL_MULAI_RATE, NILAI_RATE,KD_AKTIF"
                        }
                    };

                    request.AddJsonBody(bodys);

                    client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                    responseOrg = client.Execute<dynamic>(request);
                    returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    rates = "1";
                    if (returnData.status.responsecode != "404")
                    {
                        rates = returnData.result[0].NILAI_RATE.ToString();
                    }
                }

                result.Status = "S";
                result.Data = rates;
            }
            catch (Exception e)
            {
                result.Msg = "Terjadi error. " + e.Message;
                result.Status = "E";
            }
            return result;
        }
    }

}