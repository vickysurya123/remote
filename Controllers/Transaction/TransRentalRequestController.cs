﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransRentalRequest;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using SAP_CHECK_PIUTANG;

namespace Remote.Controllers
{
    public class TransRentalRequestController : Controller
    {
        //
        // GET: /TransRentalRequest/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            ViewBag.KodeCabang = KodeCabang;
            ViewBag.UserRole = UserRole;
            return View("TransRentalRequestIndex");
        }

        public ActionResult AddTransRentalRequest()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            TransRentalRequestDAL dal = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {
                DDBusinessEntity = dal.GetDataBE2(KodeCabang),
                DDRentalType = dal.GetDataRentalType(),
                DDContractUsage = dal.GetDataContractUsage()
            };
            return View(viewModel);
        }

        //----------------- VIEW EDIT DATA -----------------
        public ActionResult EditTransRentalRequest(string id)
        {
            ViewBag.ID = id;
            string KodeCabang = CurrentUser.KodeCabang;

            TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                TransRentalRequestDAL dal = new TransRentalRequestDAL();
                AUP_TRANS_RENTAL_REQUEST_Remote data = dal.GetDataForEdit(id);

                ViewBag.ID = data.ID;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_TYPE = data.RENTAL_REQUEST_TYPE;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;

            }
            catch (Exception)
            {
                ViewBag.ID = string.Empty;
                ViewBag.RENTAL_REQUEST_NO = string.Empty;
                ViewBag.RENTAL_REQUEST_TYPE = string.Empty;
                ViewBag.BE_ID = string.Empty;
                ViewBag.RENTAL_REQUEST_NAME = string.Empty;
                ViewBag.CONTRACT_USAGE = string.Empty;
                ViewBag.CUSTOMER_NAME = string.Empty;
                ViewBag.CONTRACT_START_DATE = string.Empty;
                ViewBag.CONTRACT_END_DATE = string.Empty;
                ViewBag.CUSTOMER_ID = string.Empty;
                ViewBag.CUSTOMER_AR = string.Empty;
                ViewBag.OLD_CONTRACT = string.Empty;
            }

            var viewModel = new TransRentalRequest
            {
                DDBusinessEntity = dal1.GetDataBE2(KodeCabang),
                DDRentalType = dal1.GetDataRentalType(),
                DDContractUsage = dal1.GetDataContractUsage()
            };

            return View(viewModel);
        }

        //-------------------UBAH DATA----------------------------
        [HttpPost]
        public JsonResult EditData([FromBody]DataTransRentalRequest data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    bool result = dal.Edit(User.Identity.Name, data, KodeCabang, User.Identity.Name);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------- UBAH STATUS -----------------------
        public JsonResult StatusApproval(DataTransRentalRequest data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    bool result = dal.StatusApproval(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult changeStatus(DataTransRentalRequest data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    bool result = dal.StatusUpdate(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- UBAH STATUS ---------------------
        public JsonResult UbahStatus(string xid, string xno)
        {
            dynamic message = null;

            try
            {
                TransRentalRequestDAL dal = new TransRentalRequestDAL();
                bool result = dal.UbahStatus(xid, xno);
                if (result)
                {
                    message = new
                    {
                        status = "S",
                        message = "Status is Inactive."
                    };
                }
                else
                {
                    message = new
                    {
                        status = "S",
                        message = "Status is Active."
                    };
                }
            }
            catch (Exception e)
            {
                message = new
                {
                    status = "E",
                    message = "There is an error. " + e.Message
                };
            }

            return Json(message);
        }

        //------------------- DATA TABLE HEADER DATA -----------------------
        public JsonResult GetDataTransRental()
        {
            DataTablesTransRentalRequest result = new DataTablesTransRentalRequest();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;

                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    result = dal.GetDataTransRentalRequest(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataTransRentalApproved()
        {
            DataTablesTransRentalRequest result = new DataTablesTransRentalRequest();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;

                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    result = dal.GetDataTransRentalRequestApproved(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATA TABLE DETAIL DATA -----------------------
        public JsonResult GetDataDetailRental(string id)
        {
            DataTablesTransRentalRequest result = new DataTablesTransRentalRequest();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    result = dal.GetDataDetailRental(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //---------------------- FILTER DATA DETAIL ---------------
        [HttpGet]
        public JsonResult GetDataFilter()
        {
            DataTablesRentalDetail result = new DataTablesRentalDetail();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string to = queryString["to"];
                    string from = queryString["from"];
                    string luas_tanah_from = queryString["luas_tanah_from"];
                    string luas_tanah_to = queryString["luas_tanah_to"];
                    string luas_bangunan_from = queryString["luas_bangunan_from"];
                    string luas_bangunan_to = queryString["luas_bangunan_to"];
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    DataTablesRentalDetail data = dal.GetDataFilterRentalDetail(draw, start, length, to, from, luas_tanah_from, luas_tanah_to, luas_bangunan_from, luas_bangunan_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }

        //---------------------- CEK PIUTANG SAP ------------------
        [HttpPost]
        public JsonResult cekPiutangSAP([FromBody] CheckPiutangSapParam param)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            retCheckLock retLock = new retCheckLock();
            CheckLockPiutangSoapClient cSAP = new CheckLockPiutangSoapClient(CheckLockPiutangSoapClient.EndpointConfiguration.CheckLockPiutangSoap);
            try
            {

                retLock = cSAP.inboundCheckLock_P3Async("", param.DOC_TYPE, KodeCabang, param.CUST_CODE).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {

                //throw ex;
            }
            return Json(retLock);
        }

        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpPost]
        public ActionResult GetDataCustomer(string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public ActionResult GetDataCustomer_S2(string p)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(p, KodeCabang);
            return Json(new { items = xcustomer });
        }

        //----------------- DROP DOWN INDUSTRY --------------
        public JsonResult GetDataDropDownIndustry()
        {
            TransRentalRequestDAL dal = new TransRentalRequestDAL();
            IEnumerable<DDIndustry> result = dal.GetDataIndustry();
            return Json(new { data = result });
        }

        //----------------------- INSERT DATA HEADER ---------------------------
        [HttpPost]
        public JsonResult SaveHeader([FromBody]DataTransRentalRequest data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransRentalRequestDAL dal = new TransRentalRequestDAL();
                    var result = dal.AddHeader(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "S",
                            message = "Rental Request Transaction Added Successfully with Transaction Number: " + result.ID_TRANS
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data." + result.MessageInfo
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }


        // Ambil data detail untuk edit
        public JsonResult GetDataDetailRentalEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransRentalRequestDAL dal = new TransRentalRequestDAL();
            IEnumerable<DT_DETAIL_RENTAL_REQUEST> result = dal.GetDataDetailRentalEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        //-------------------------- INSERT DATA DETAIL -------------------------

        //public JsonResult SaveDetail(DataTransRentalRequest data)
        //{
        //    dynamic message = null;

        //    if (IsAjaxUtil.validate(HttpContext))
        //    {
        //        try
        //        {
        //            string KodeCabang = CurrentUser.KodeCabang;

        //            TransRentalRequestDAL dal = new TransRentalRequestDAL();
        //            bool result = dal.AddDetail(User.Identity.Name, data, KodeCabang);

        //            if (result)
        //            {
        //                message = new
        //                {
        //                    status = "S",
        //                    message = "Data Other Services Added Successfully with number: "
        //                };
        //            }
        //            else
        //            {
        //                message = new
        //                {
        //                    status = "E",
        //                    message = "Failed to Add Data."
        //                };
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            message = new
        //            {
        //                status = "E",
        //                message = "Terjadi error insert detail. " + e.Message
        //            };
        //        }
        //    }

        //    return Json(message);
        //}
        public JsonResult GetDataProfitCenter(string id)
        {
            TransRentalRequestDAL dal = new TransRentalRequestDAL();
            DDProfitCenter result = dal.GetDataPC(id);
            return Json(new { data = result });
        }
    }
}