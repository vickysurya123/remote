﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Remote.DAL;
using Remote.Models.DynamicDatatable;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class MonitoringMultipleBillingController : Controller
    {
        public ActionResult Index()
        {
            return View("MonitoringMultipleBillingIndex");
        }


        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MultipleBillingDAL dal = new MultipleBillingDAL();
                    result = dal.GetData(draw, start, length, search, KodeCabang);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        //---------------------- NEW GET DATA --------------------------
        public JsonResult GetDataNew()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MultipleBillingDAL dal = new MultipleBillingDAL();
                    result = dal.GetDatanew(draw, start, length, search, KodeCabang);
                }
                catch (Exception) { }
            }

            return Json(result);
        }


        public JsonResult GetDataDetail(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    int contract = int.Parse(id);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MultipleBillingDAL dal = new MultipleBillingDAL();
                    result = dal.GetDataDetail(contract, draw, start, length, search);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

    }
}