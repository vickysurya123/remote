﻿using System;
using System.Collections.Generic;
using Remote.DAL;
using Remote.ViewModels;
using System.Security.Claims;
using Remote.Models.DynamicDatatable;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using Remote.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;

namespace Remote.Controllers
{
    [Authorize]
    public class MonitoringPembayaranController : Controller
    {
        // GET: MonitoringPembayaran
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string NamaCabang = CurrentUser.NamaCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            ViewBag.NamaCabang = NamaCabang;
            return View("Index", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataFilter()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be = queryString["be"];
                    string tipe = queryString["tipe"];
                    string jenis_tagihan = queryString["jenis_tagihan"];
                    string search = queryString["search[value]"];

      

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringPembayaranDAL dal = new MonitoringPembayaranDAL();
                    IDataTable data = dal.GetDataFilter(draw, start, length, be, tipe, jenis_tagihan, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result, "application/json");
        }

        //---------------------- FILTER NEW --------------------------
        public JsonResult GetDataFilterNew()
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be = queryString["be"];
                    string tipe = queryString["tipe"];
                    string jenis_tagihan = queryString["jenis_tagihan"];
                    string search = queryString["search[value]"];



                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringPembayaranDAL dal = new MonitoringPembayaranDAL();
                    IDataTable data = dal.GetDataFilternew(draw, start, length, be, tipe, jenis_tagihan, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        // GET: MonitoringPembayaran/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MonitoringPembayaran/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MonitoringPembayaran/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MonitoringPembayaran/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MonitoringPembayaran/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MonitoringPembayaran/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MonitoringPembayaran/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public class nama
        {
            public string fileNames { get; set; }
        }

        [HttpPost]
        public JsonResult defineExcel([FromBody] MonitoringPembayaran data)
        {
            nama result = new nama();
            var kolomName = "";
            var jenis = (data.JENIS_TAGIHAN == "AIR" ? "AIR DAN LISTRIK" : data.JENIS_TAGIHAN);
            if (data.NOTA_TYPE == null || data.NOTA_TYPE == "")
            {
                kolomName = jenis;
            }
            else
            {
                kolomName = jenis + " - " + data.NOTA_TYPE;
            }
            
            string fileName = "MonitoringPembayaran(" + kolomName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                MonitoringPembayaranDAL dal = new MonitoringPembayaranDAL();
                var dalShowdata = dal.GetDetailExcel(data.BUSINESS_ENTITY, data.JENIS_TAGIHAN, data.NOTA_TYPE, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[11] {
                new DataColumn("JENIS TAGIHAN", typeof(string)),
                new DataColumn("NOMOR NOTA", typeof(string)),
                new DataColumn(kolomName,typeof(string)),
                new DataColumn("KODE CUSTOMER", typeof(string)),
                new DataColumn("NAMA CUSTOMER ",typeof(string)),
                new DataColumn("TANGGAL TRANSAKSI ",typeof(string)),
                new DataColumn("TANGGAL LUNAS ",typeof(string)),
                new DataColumn("TANGGAL BATAL ",typeof(string)),
                new DataColumn("NO. SAP ",typeof(string)),
                new DataColumn("JUMLAH TAGIHAN ",typeof(string)),
                new DataColumn("STATUS", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.JENIS_TAGIHAN, valData.BILLING_NO, valData.CODE,
                                valData.CUSTOMER_CODE, valData.CUSTOMER_NAME, valData.PSTNG_DATE, 
                                valData.TGL_LUNAS, valData.TGL_BATAL, valData.DOC_NUMBER, valData.AMOUNT, valData.STATUS_LUNAS);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Dashboard Piutang");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            MonitoringPembayaranDAL dal = new MonitoringPembayaranDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }
    }
}
