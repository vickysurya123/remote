﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractBilling;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
//using Remote.SAP_CEK_PIUTANG;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Remote.Models.TransVB;

namespace Remote.Controllers
{
    [Authorize]
    public class TransContractBillingController : Controller
    {
        //
        // GET: /CreateContractBilling/
        public ActionResult Index()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
            };
            ViewBag.roleId = CurrentUser.UserRoleID;
            return View("CreateContractBillingIndex", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        
        //------------------- FILTER DATA BILLING -----------------------
        public JsonResult GetDataFilterBilling()
        {
            DataTablesTransContractBilling result = new DataTablesTransContractBilling();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string contract = queryString["contract"];
                    string customer = queryString["customer"];
                    string periode = queryString["periode"];
                    string date_end = queryString["date_end"];
                    string select_cabang = queryString["select_cabang"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    DataTablesTransContractBilling data = dal.GetDataFilterBillingnew(draw, start, length, contract, customer, periode, date_end, search, KodeCabang, select_cabang, CurrentUser.UserID);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //---------------------------- FILTER SHOW ALL DATA ------------------------
        public JsonResult GetDataShowAll()
        {
            DataTablesTransContractBilling result = new DataTablesTransContractBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    //result = dal.GetDataShowAll(draw, start, length, search, KodeCabang);
                    result = dal.GetDataShowAllnew(draw, start, length, search, KodeCabang, CurrentUser.UserID);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------- POSTING BILLING TO SAP ---------------
        [HttpPost]
        public JsonResult postToSAP([FromBody] PostSAP data)
        {
            dynamic message = null;


            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    //var result = dal.PostSAPdummy(BILL_ID, BILL_TYPE);
                    var result = dal.PostSAP(data.BILL_ID, data.BILL_TYPE);

                    if (string.IsNullOrEmpty(result.DOCUMENT_NUMBER) || result.DOCUMENT_NUMBER == "/0000")
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "E",
                            message = "Failed to Post Data to SAP."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "S",
                            message = "Data Has Been Posted to SAP With Document Number " + result.DOCUMENT_NUMBER
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //---------------- MESSAGE ERROR SAP -----------
        [HttpPost]
        public JsonResult GetMsgSAP([FromBody] string BILL_ID)
        {
            TransContractBillingDAL dal = new TransContractBillingDAL();
            Array ret = dal.GetDataMsgSAP(BILL_ID);
            return Json(ret);
        }

        //---------------- SAVE HEADER BILLING -----------
        [HttpPost]
        public JsonResult SaveHeader([FromBody] DataTransContractBilling data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    var result = dal.AddHeader(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            billing_contract_no = result.BILLING_CONTRACT_NO,
                            billing_no = result.BILLING_NO,
                            status = "S",
                            message = "Data Has Been Posted to SAP With Billing Number : " + result.BILLING_NO + "and Document Number : " + result.DOCUMENT_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //---------------------- DATA DETIL BILLING ----------------
        public JsonResult GetDataDetailBilling(string id)
        {
            DataTablesTransContractBilling result = new DataTablesTransContractBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    result = dal.GetDataDetailBilling(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public JsonResult sendToSilapor(string ID, string BILLING_CONTRACT_NO, string BILL_ID, string SAP_DOC_NO, string BILL_TYPE, string POSTING_DATE)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    var result = dal.PostSilapor(BILLING_CONTRACT_NO);

                    message = new
                    {
                        status = "S",
                        message = "Data Has Been Posted to SAP With Document Number "
                    };
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}