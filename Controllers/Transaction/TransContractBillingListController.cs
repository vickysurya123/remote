﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractBilling;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Remote.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class TransContractBillingListController : Controller
    {
        //
        // GET: /TransContractBillingList/
        public ActionResult Index()
        {
            return View("TransListContractBilling");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //------------------- DATA TABLE HEADER DATA DI LIST BILLING -----------------------

        public JsonResult GetDataTransContractBillingList()
        {
            DataTablesTransContractBillingList result = new DataTablesTransContractBillingList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransContractBillingListDAL dal = new TransContractBillingListDAL();
                    result = dal.GetDataTransContractBillingListNew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------------------------- DATA DETAIL LIST BILLING -----------------
        public JsonResult GetDataDetailBillingList(string id)
        {
            DataTablesTransContractBillingList result = new DataTablesTransContractBillingList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractBillingListDAL dal = new TransContractBillingListDAL();
                    result = dal.GetDataDetailContractBillingList(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------------------------- RE POSTING SAP --------------------
        public JsonResult postToSAP(string BILL_ID, string BILL_TYPE)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    var result = dal.PostSAP(BILL_ID, BILL_TYPE);
                    //var result = dal.PostSAPdummy(BILL_ID, BILL_TYPE);

                    if (result.DOCUMENT_NUMBER != null)
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "S",
                            message = "Data Has Been Posted to SAP With Document Number " + result.DOCUMENT_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            document_number = result.DOCUMENT_NUMBER,
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetMsgSAP(string BILL_ID)
        {
            TransContractBillingListDAL dal = new TransContractBillingListDAL();
            Array ret = dal.GetDataMsgSAP(BILL_ID);
            return Json(ret);
        }
        public JsonResult sendToSilapor(string BILLING_CONTRACT_NO)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    IDbConnection connection = DatabaseFactory.GetConnection();
                    //var sql = @"select a.SAP_DOC_NO,to_char(a.BILLING_ID) as ID,to_char(a.POSTING_DATE , 'DD/MM/YYYY')POSTING_DATE  from PROP_CONTRACT_BILLING_POST a
                    //            where a.BILLING_NO = :a";
                    //dynamic data_bill = connection.Query<dynamic>(sql, new
                    //{
                    //    a = BILL_ID
                    //}).DefaultIfEmpty().FirstOrDefault();
                    TransContractBillingDAL dal = new TransContractBillingDAL();
                    //var result = dal.PostSilapor(data_bill.ID, BILL_ID,"111111" , "ZB01", data_bill.POSTING_DATE, CurrentUser.UserLoginID);//data_bill.SAP_DOC_NO
                    var result = dal.PostSilapor(BILLING_CONTRACT_NO);
                    message = new
                    {
                        status = "S",
                        message = "Data Has Been Posted to SAP With Document Number "
                    };
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}