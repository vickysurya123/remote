﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models;
using Remote.ViewModels;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Collections;
using System.Dynamic;
using RestSharp;
using Remote.Helpers;

namespace Remote.Controllers
{
    [Authorize]
    public class DisposisiList : Controller
    {
        // GET: Parameter   
        public ActionResult Index()
        {
            return View("DisposisiListIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetDataDisposisi()
        {
            dynamic dataUser = CurrentUser;
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    DisposisiDal dal = new DisposisiDal();
                    result = dal.GetDataDisposisi(draw, start, length, search, dataUser);
                }
                catch (Exception)
                {
                }
            }
            if(result.draw > 0)
            {
                foreach (dynamic data in result.data)
                {
                    data.encodedId = UrlEncoder.Encode(data.ID.ToString());
                }
            }
            
            return Json(result);
        }

        public JsonResult GetDataParaf()
        {
            dynamic dataUser = CurrentUser;
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    DisposisiDal dal = new DisposisiDal();
                    result = dal.GetDataParaf(draw, start, length, search, dataUser);
                }
                catch (Exception)
                {
                }
            }
            if(result.draw > 0)
            {
                foreach (dynamic data in result.data)
                {
                    data.encodedId = UrlEncoder.Encode(data.ID.ToString());
                }
            }
            
            return Json(result);
        }

        public ActionResult AddDisposisi(string id)
        {
            string decodeId = UrlEncoder.Decode(id);
            string KodeCabang = CurrentUser.KodeCabang;
            int userId = Convert.ToInt32(CurrentUser.UserID);

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(decodeId);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(dataSurat.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiListSurat> list_surat = dalDisposisi.GetDataListDisposisiSurat(data.CONTRACT_NUMBER);
                var hiddenCreateSurat = dalDisposisi.GetDataHiddenCreateSurat(dataSurat.CONTRACT_OFFER_NO, CurrentUser.UserRoleID);

                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;

                //surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_SURAT = list_surat;
                ViewBag.ADD_SURAT = 0;
                if(userId == 501)
                {
                    ViewBag.HIDDEN_CREATE_SURAT = 1;
                } else
                {
                    ViewBag.HIDDEN_CREATE_SURAT = Convert.ToInt32(hiddenCreateSurat);
                }
                ViewBag.PARAF_DISPOSISI = 0; //type approve paraf berdasarkan dari data disposisi atau surat pertama

            }
            catch (Exception e)
            {
                throw e;
            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("AddDisposisi", viewModel);
        }


        public ActionResult AddSuratDisposisi(string id)
        {
            string decodeId = UrlEncoder.Decode(id);
            string KodeCabang = CurrentUser.KodeCabang;
            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(decodeId);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(dataSurat.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiListSurat> list_surat = dalDisposisi.GetDataListDisposisiSurat(data.CONTRACT_OFFER_NO);
                IEnumerable<LIST_PARAF> dataParaf = dalDisposisi.ListDataParaf(decodeId);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;

                //surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_SURAT = list_surat;
                ViewBag.ADD_SURAT = 1;
                ViewBag.PARAF_DISPOSISI = 0; //type approve paraf berdasarkan dari data disposisi atau surat pertama
                ViewBag.LIST_DETAIL_PARAF = dataParaf;

            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("AddSuratDisposisi", viewModel);
        }

        public ActionResult EditSuratDisposisi(string id)
        {
            string decodeId = UrlEncoder.Decode(id);
            string KodeCabang = CurrentUser.KodeCabang;
            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DataSurat dataSurat2 = dalDisposisi.GetDataSurat2(decodeId);
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(dataSurat2.PREV_SURAT);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(dataSurat.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiListSurat> list_surat = dalDisposisi.GetDataListDisposisiSurat(data.CONTRACT_OFFER_NO);
                IEnumerable<LIST_PARAF> dataParaf = dalDisposisi.ListDataParaf(dataSurat2.PREV_SURAT);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;

                //surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_SURAT = list_surat;
                ViewBag.ADD_SURAT = 1;
                ViewBag.DATA_SURAT2 = dataSurat2;
                ViewBag.LIST_DETAIL_PARAF = dataParaf;
            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("EditSuratDisposisi", viewModel);
        }

        public ActionResult Paraf(string id)
        {
            string decodeId = UrlEncoder.Decode(id);
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(decodeId);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(dataSurat.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiListSurat> list_surat = dalDisposisi.GetDataListDisposisiSurat(dataSurat.CONTRACT_OFFER_NO);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;
                ViewBag.PARAF_LEVEL = data.PARAF_LEVEL;

                //surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_SURAT = list_surat;
                ViewBag.ADD_SURAT = 2; //paraf
                ViewBag.HIDDEN_CREATE_SURAT = 0;
                ViewBag.PARAF_DISPOSISI = 1; //type approve paraf berdasarkan dari data disposisi atau surat pertama
            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("AddDisposisi", viewModel);
        }

        public ActionResult ParafDisposisi(string id)
        {
            string decodeId = UrlEncoder.Decode(id);
            string KodeCabang = CurrentUser.KodeCabang;

            //TransRentalRequestDAL dal1 = new TransRentalRequestDAL();
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(decodeId);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(dataSurat.CONTRACT_OFFER_NO);
                IEnumerable<DisposisiListSurat> list_surat = dalDisposisi.GetDataListDisposisiSurat(dataSurat.CONTRACT_OFFER_NO);

                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.RENTAL_REQUEST_NO = data.RENTAL_REQUEST_NO;
                ViewBag.RENTAL_REQUEST_NAME = data.RENTAL_REQUEST_NAME;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CONTRACT_USAGE_ID = data.CONTRACT_USAGE_ID_REF;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_OFFER_TYPE_ID = data.CONTRACT_OFFER_TYPE_ID_REF;
                ViewBag.CONTRACT_NUMBER = data.CONTRACT_NUMBER;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OLD_CONTRACT = data.OLD_CONTRACT;
                ViewBag.OLD_CONTRACT_OFFER = data.OLD_CONTRACT_OFFER;
                ViewBag.OLD_RENTAL_REQUEST = data.OLD_RENTAL_REQUEST;
                ViewBag.RR_ID = data.RENTAL_REQUEST_ID;
                ViewBag.PARAF_LEVEL = data.PARAF_LEVEL;

                //surat
                ViewBag.SURAT_ID = dataSurat.ID;
                ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                ViewBag.SUBJECT = dataSurat.SUBJECT;
                ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                ViewBag.DETAIL_HISTORY = history;
                ViewBag.LIST_SURAT = list_surat;
                ViewBag.ADD_SURAT = 2; //paraf
                ViewBag.HIDDEN_CREATE_SURAT = 0;
                ViewBag.PARAF_DISPOSISI = 2; //type approve paraf berdasarkan dari data disposisi atau surat pertama
            }
            catch (Exception)
            {

            }

            TransRentalRequestDAL dal2 = new TransRentalRequestDAL();
            var viewModel = new TransRentalRequest
            {

                DDProfitCenters = dal2.GetDataProfitCenter(KodeCabang),
                DDBusinessEntity = dal2.GetDataBE2(KodeCabang),
                DDRentalType = dal2.GetDataRentalType(),
                DDContractUsage = dal2.GetDataContractUsage()
            };
            return View("AddDisposisi", viewModel);
        }

        //insert data disposisi
        [HttpPost]
        public JsonResult Insert([FromBody] Disposisi data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    string id = queryString["id"];

                    DisposisiDal dal = new DisposisiDal();

                    string[] dataDisposisi = data.DISPOSISI_KE.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] dataNama = data.DISPOSISI_NAMA.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    List<object> listDataDisposisiParaf = new List<object>();
                    int i = 0;
                    foreach (var item in dataDisposisi)
                    {
                        listDataDisposisiParaf.Add(new
                        {
                            disposisi_id = item,
                            disposisi_nama = dataNama[i]
                        });
                        i++;
                    }

                    result = dal.AddData(data, dataUser, listDataDisposisiParaf);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult InsertSurat(DataSurat data, IEnumerable<IFormFile> filex)
        {
            dynamic result = null;
            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                DisposisiDal dal = new DisposisiDal();

                var r = new List<UploadFilesResult>();
                List<object> listUrl = new List<object>();

                var LIST_FILE = JsonConvert.DeserializeObject<dynamic>(data.LIST_FILE);

                foreach (var item in LIST_FILE)
                {
                    listUrl.Add(new
                    {
                        directory = item.DIRECTORY,
                        file_name = item.FILE_NAME
                    });
                }

                /*if (filex.Count() > 0)
                {
                    foreach (var file in filex)
                    {
                        //var split = data.NO_SURAT.Replace('/', '-');
                        //string subPath = split;
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        string filePath = Path.Combine(rootPath, "REMOTE", "ImageSurat");

                        bool exists = Directory.Exists(filePath);

                        if (!exists)
                            Directory.CreateDirectory(filePath);

                        var sFile = "";
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileExt = Path.GetExtension(file.FileName);

                        sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                        string savedFileName = Path.Combine(filePath, sFile);

                        using (var stream = new FileStream(savedFileName, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        //string directory = "~/REMOTE/ImageSurat/" + subPath;
                        //string KodeCabang = CurrentUser.KodeCabang;
                        dynamic getData = null;
                        GetUrl test = new GetUrl();
                        string url = test.UploadCloadUrl + "upload";
                        RestClient client = new RestClient(url);
                        RestRequest request = new RestRequest(Method.POST)
                        {
                            RequestFormat = DataFormat.None,
                            AlwaysMultipartFormData = true
                        };
                        request.AddHeader("content-type", "multipart/form-data");
                        request.AddHeader("Accept", "application/json");
                        request.AddParameter("folder", "REMOTE/FileSurat");
                        request.AddFile("file", savedFileName);
                        request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                        IRestResponse responseOrg = client.Execute<dynamic>(request);
                        var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                        string file2 = list.message.url;

                        listUrl.Add(new
                        {
                            directory = file2,
                            file_name = fileName
                        });

                        r.Add(new UploadFilesResult()
                        {
                            Name = sFile,
                            Length = Convert.ToInt32(file.Length),
                            Type = file.ContentType,
                        });
                    }
                }*/

                result = dal.AddDataSurat(data, CurrentUser, "", "", listUrl);
                
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Json(result);
        }

        [HttpPost]

        public JsonResult EditSurat(DataSurat data, IEnumerable<IFormFile> filex)
        {
            dynamic result = null;
            try
            {
                dynamic ids = null;
                int x = 0;
                if (Int32.TryParse(data.ID, out x))
                {

                }
                else
                {
                    ids = UrlEncoder.Decode(data.ID);
                }
                var queryString = Request.Query;
                string id = queryString["id"];

                DisposisiDal dal = new DisposisiDal();

                var r = new List<UploadFilesResult>();

                List<object> listUrl = new List<object>();

                var LIST_FILE = JsonConvert.DeserializeObject<dynamic>(data.LIST_FILE);

                foreach (var item in LIST_FILE)
                {
                    listUrl.Add(new
                    {
                        directory = item.DIRECTORY,
                        file_name = item.FILE_NAME
                    });
                }

                /*if (filex.Count() > 0)
                {
                    foreach (var file in filex)
                    {
                        //var split = data.NO_SURAT.Replace('/', '-');
                        //string subPath = split;
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        string filePath = Path.Combine(rootPath, "REMOTE", "ImageSurat");

                        bool exists = Directory.Exists(filePath);

                        if (!exists)
                            Directory.CreateDirectory(filePath);

                        var sFile = "";
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileExt = Path.GetExtension(file.FileName);

                        sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                        string savedFileName = Path.Combine(filePath, sFile);

                        using (var stream = new FileStream(savedFileName, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        //string directory = "~/REMOTE/ImageSurat/" + subPath;
                        //string KodeCabang = CurrentUser.KodeCabang;
                        dynamic getData = null;
                        GetUrl test = new GetUrl();
                        string url = test.UploadCloadUrl + "upload";
                        RestClient client = new RestClient(url);
                        RestRequest request = new RestRequest(Method.POST)
                        {
                            RequestFormat = DataFormat.None,
                            AlwaysMultipartFormData = true
                        };
                        request.AddHeader("content-type", "multipart/form-data");
                        request.AddHeader("Accept", "application/json");
                        request.AddParameter("folder", "REMOTE/FileSurat");
                        request.AddFile("file", savedFileName);
                        request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                        IRestResponse responseOrg = client.Execute<dynamic>(request);
                        var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                        string file2 = list.message.url;

                        listUrl.Add(new
                        {
                            directory = file2,
                            file_name = fileName
                        });

                        r.Add(new UploadFilesResult()
                        {
                            Name = sFile,
                            Length = Convert.ToInt32(file.Length),
                            Type = file.ContentType,
                        });
                    }
                }*/

                result = dal.EditDataSurat(data, CurrentUser, "", "", listUrl);
                
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult ApproveParaf([FromBody] DataSurat data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    DisposisiDal dal = new DisposisiDal();

                    result = dal.ApproveParaf(data, dataUser);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult ApproveParafDisposisi([FromBody] DataSurat data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    DisposisiDal dal = new DisposisiDal();

                    result = dal.ApproveParafDisposisi(data, dataUser);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult RejectedParaf([FromBody] DataSurat data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    DisposisiDal dal = new DisposisiDal();

                    result = dal.RejectedParaf(data, dataUser);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult RejectedParafDisposisi([FromBody] DataSurat data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    DisposisiDal dal = new DisposisiDal();

                    result = dal.RejectedParafDisposisi(data, dataUser);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IEnumerable<IFormFile> files)
        {
            dynamic dataUser = CurrentUser;
            string directory = string.Empty; ;
            try
            {
                var r = new List<UploadFilesResult>();

                foreach (var file in files)
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    string filePath = Path.Combine(rootPath, "REMOTE", "ImageSummernote");

                    bool exists = Directory.Exists(filePath);

                    if (!exists)
                        Directory.CreateDirectory(filePath);

                    var sFile = "";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileExt = Path.GetExtension(file.FileName);

                    sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                    string savedFileName = Path.Combine(filePath, sFile);

                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    dynamic getData = null;
                    GetUrl test = new GetUrl();
                    string url = test.UploadCloadUrl + "upload";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.None,
                        AlwaysMultipartFormData = true
                    };
                    request.AddHeader("content-type", "multipart/form-data");
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("folder", "REMOTE/ImageIsiSurat");
                    request.AddFile("file", savedFileName);
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    string file2 = list.message.url;

                    directory = file2;

                    r.Add(new UploadFilesResult()
                    {
                        Name = sFile,
                        Length = Convert.ToInt32(file.Length),
                        Type = file.ContentType,
                    });
                }
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }
            return Json(directory);
        }

        public JsonResult GetHistoryDisposisi(string id)
        {
            DisposisiDal dal = new DisposisiDal();
            IEnumerable<DisposisiHistory> result = dal.GetDataHistoryDisposisi(id);

            return Json(new
            {
                results = result
            });
        }

        [HttpGet]
        public JsonResult GetDataEmployee(string q)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            DisposisiDal dal = new DisposisiDal();
            IEnumerable<DDDisposisiEmployee> result = dal.GetDataEmployee(KodeCabang, q);

            return Json(new
            {
                results = result
            });
        }

        public JsonResult GetContract(string CONTRACT_NO)
        {
            dynamic message = null;

            try
            {
                TransContractOfferDAL dal = new TransContractOfferDAL();
                AUP_TRANS_CONTRACT_OFFER_WEBAPPS data = dal.EditOffer(CONTRACT_NO);
                message = data;
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult GetSurat(string id)
        {
            dynamic message = new ExpandoObject(); // recommended 

            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                DISPOSISI_LIST dataSurat = dalDisposisi.GetDataDisposisi(id);
                IEnumerable<DisposisiHistory> history = dalDisposisi.GetDataHistoryDisposisi(dataSurat.CONTRACT_OFFER_NO);
                message.dataSurat = dataSurat;
                message.history = history;
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult getDataAttachment(string id)
        {
            int x = 0;
            if (Int32.TryParse(id, out x))
            {

            }
            else
            {
                id = UrlEncoder.Decode(id);
            }
            IDataTable result = new IDataTable();
            //if (IsAjaxUtil.validate(HttpContext))
            //{
            try
            {

                var queryString = Request.Query;
                int draw = int.Parse(queryString["draw"]);
                int start = int.Parse(queryString["start"]);
                int length = int.Parse(queryString["length"]);
                string search = queryString["search[value]"];
                DisposisiDal dalDisposisi = new DisposisiDal();

                result = dalDisposisi.GetDataAttachment(draw, start, length, search, id);
            }
            catch (Exception)
            {
            }
            //}
            return Json(result);

        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                DisposisiDal dalDisposisi = new DisposisiDal();
                message = dalDisposisi.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }
            return Json(message);
        }
    }
}