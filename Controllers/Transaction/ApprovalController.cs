﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractOffer;
using Remote.Models.TransRentalRequest;
using Remote.Models.TransRR;
using Remote.Models.PendingList;
using Remote.Models;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Newtonsoft.Json;
using System.IO;
using Remote.Helpers;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using Remote.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using iTextSharp.text;
using iTextSharp.text.pdf;
using RestSharp;
using System.Dynamic;
using System.Collections;
using System.Net.Http;
using Remote.Models.GeneralResult;
using RestSharp.Authenticators;

namespace Remote.Controllers.Transaction
{
    public class ApprovalController : Controller
    {
        // GET: ApprovalController
        public ActionResult Index()
        {
            return View("ApprovalExceptionIndex");
        }

        //--------------------------- DATA TABLES HEADER APPROVAL EXCEPTION --------------------- 
        public JsonResult GetDataTransApprovalException()
        {
            DataTablesTransContractOffer result = new DataTablesTransContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransApprovalExceptionDAL dal = new TransApprovalExceptionDAL();
                    result = dal.GetDataTransApprovalException(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult SetApprove([FromBody] ContractOfferApprovalParameter data)
        {
            dynamic message = null;
            // ContractOfferApprovalParameter result = new ContractOfferApprovalParameter();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransApprovalExceptionDAL dal = new TransApprovalExceptionDAL();
                    bool result = dal.UpdateApprove(data);
                    
                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Approve Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Approve Failed."
                        };
                    }
                }
                catch (Exception ex)
                {
                    message = new
                    {
                        status = "E",
                        message = "Your can not continue the approval release. <br> Error Code: " + ex.Message
                    };
                }

            }
            return Json(message);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        // GET: ApprovalController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ApprovalController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApprovalController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ApprovalController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ApprovalController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
