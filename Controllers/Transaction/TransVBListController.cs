﻿using System;
using System.Collections.Generic;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransVB; 
using Remote.Models.TransVBList;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Models;

using System.Text;

using System.IO;
//using NPOI.HSSF.UserModel;
//using NPOI.HPSF;
//using NPOI.POIFS.FileSystem;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Remote.Controllers
{
    [Authorize]
    public class TransVBListController : Controller
    {
        //
        // GET: /TransVBList/
        public ActionResult Index()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
            };
            ViewBag.roleId = CurrentUser.UserRoleID;
            return View("TransVBListIndex", viewModel);
        }

        public ActionResult Edit(string id)
        {        
            try
            {

                string KodeCabang = CurrentUser.KodeCabang;
                ViewBag.UPDATED_BY = User.Identity.Name;
                ViewBag.BRANCH_ID = KodeCabang;

                TransVBListDAL dal = new TransVBListDAL();
                AUP_TRANSVB_WEBAPPS data = dal.GetDataForEdit(id);

                ViewBag.ID = data.ID;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.GL_ACCOUNT = data.GL_ACCOUNT;
                ViewBag.COSTUMER_MDM = data.COSTUMER_MDM;
                ViewBag.COSTUMER_ID = data.COSTUMER_ID;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;
                ViewBag.CUSTOMER_SAP_AR = data.CUSTOMER_SAP_AR;
                ViewBag.TOTAL = data.TOTAL;
                ViewBag.TAX_CODE = data.TAX_CODE;
                ViewBag.PPH_CODE = data.PPH_CODE;
                ViewBag.RATE = data.RATE;

            }
            catch (Exception ex)
            {
            }
            return View();
        }

        public ActionResult AddTransVB()
        {
            ViewBag.CREATED_BY = User.Identity.Name;

            string KodeCabang = CurrentUser.KodeCabang;
            string user_id = CurrentUser.UserID;
            ViewBag.BRANCH_ID = KodeCabang;
            TransVBDAL dal = new TransVBDAL();
            var viewModel = new TransVB
            {
                DDProfitCenters = dal.GetDataProfitCenter(user_id),
                DDServiceGroup = dal.GetDataService(),
            };
            return View(viewModel);
        }

        public AppUser CurrentUser
        { 
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataVBTransList()
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    TransVBListDAL dal = new TransVBListDAL();
                    result = dal.GetDataVBTransList(draw, start, length, search, KodeCabang, UserRole);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataVBTransListNew()
        {
            DataTablesTransVB result = new DataTablesTransVB();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string select_cabang = queryString["select_cabang"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    TransVBListDAL dal = new TransVBListDAL();
                    result = dal.GetDataVBTransListnew(draw, start, length, search, KodeCabang, UserRole, select_cabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDropDownServiceGroup()
        {
            TransVBListDAL dal = new TransVBListDAL();
            string roleId = CurrentUser.UserRoleID;
            string user_id = CurrentUser.UserID; 
            IEnumerable<DDServiceGroup> result = dal.GetDataDropDownServiceGroup(user_id,roleId);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBListDAL dal = new TransVBListDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataDropDownProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public ActionResult GetDataServiceCodeSelect2(string q, string page)
        {
            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                TransVBListDAL dal = new TransVBListDAL();
                var serviceCode = dal.GetServiceCodeSelect2("2", q, KodeCabang);
                return Json(serviceCode);

            }
            catch (Exception)
            {
                return RedirectToAction("login", "auth");
            }
        }

        [HttpPost]
        public ActionResult GetDataServiceCode([FromBody] ServiceTransVB data)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBListDAL dal = new TransVBListDAL();
            var xcustomer = dal.GetDataServiceCode(data.SERVICE_NAME, data.SERVICE_GROUP, KodeCabang);
            return Json(xcustomer);
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string UserRole = CurrentUser.UserRoleID;
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBListDAL dal = new TransVBListDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang, UserRole);
            return Json(xcustomer);
        }

        [HttpPost]
        public JsonResult SaveHeader([FromBody] DataTransVB data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransVBListDAL dal = new TransVBListDAL();
                    var result = dal.AddHeader(User.Identity.Name, data, KodeCabang, User.Identity.Name);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "S",
                            message = "Other Services Transaction Added Successfully with Transaction Number: " + result.ID_TRANS
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "E",
                            message = "Failed to Save Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult SaveHeaderEdit([FromBody] DataTransVB data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransVBListDAL dal = new TransVBListDAL();
                    var result = dal.SaveHeaderEdit(User.Identity.Name, data, KodeCabang, User.Identity.Name);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Other Services Transaction Updated Successfully"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Save Data"
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataDetaiVBEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransVBListDAL dal = new TransVBListDAL();
            IEnumerable<AUP_TRANSVB_WEBAPPS> result = dal.GetDataDetailRentalEdit(id, KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataDropDownCoaProduksi(string param)
        {
            TransVBListDAL dal = new TransVBListDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownCoaProduksi(param);
            return Json(new { data = result });
        }

        public JsonResult Getunit(string param)
        {
            TransVBListDAL dal = new TransVBListDAL();
            IEnumerable<DDRefD> result = dal.GetDataDropDownunit(param);
            return Json(new { data = result });
        }

        public async Task<IActionResult> GetListCoa(string param)
        {
            try
            {
                var queryString = Request.Query;
                string draw = queryString["draw"];
                int start = int.Parse(queryString["start"]);
                int length = int.Parse(queryString["length"]);
                string search = queryString["search[value]"];
                string tes = "%" + search;

                int panjang = ((start + 10) / 10);

                // api get coa
                string url = "https://fuse19.pelindo.co.id/master/mdm/silaporcoaprod/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                if (search != "")
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = param,
                            STAT = "D",
                            DESKRIPSI = tes,
                            COA_PROD = tes,
                            GL_TEXT = tes,
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }
                else
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = param,
                            STAT = "D",
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);

                List<object> result = new List<object>();

                result.Add(new
                {
                    draw = draw,
                    recordsFiltered = returnData.paging.totalpages,
                    recordsTotal = returnData.paging.total,
                    data = returnData.result,
                });



                return Ok(responseOrg.Content);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = e.Message
                });
            }

        }
    }
}