﻿using System;
using System.Collections.Generic;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractOffer;
using Remote.Models.PendingList;
using Remote.Models;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.Dynamic;
using RestSharp;

namespace Remote.Controllers
{
    [Authorize]
    public class PendingListController : Controller
    {
        //
        // GET: /PendingList/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("PendingListIndex");
        }

        //--------------------------- DATA TABLES HEADER CONTRACT OFFER --------------------- 
        public JsonResult GetDataHeaderContractOffer()
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;
                    string KodeTerminal = CurrentUser.KodeTerminal;
                    string UserRole = CurrentUser.UserRoleID;
                    string _roleProperty = CurrentUser.UserRoleProperty;

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataHeaderContractOffernew(draw, start, length, search, KodeCabang, UserRole, _roleProperty,KodeTerminal);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES HISTORY WORKFLOW --------------------- 
        public JsonResult GetDataHistoryWorkflow(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataHistoryWorkflownew(draw, start, length, search, KodeCabang, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL OBJECT --------------------- 
        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailCondition(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailMemo(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL SALES RULE --------------------- 
        public JsonResult GetDataDetailSalesBased(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataDetailSalesBased(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- VIEW APPROVE PAGE ------------------------
        public ActionResult ApprovePage(string id)
        {
            ViewBag.CONTRACT_OFFER_NO = id;

            try
            {

                PendingListDAL dal = new PendingListDAL();
                AUP_PENDING_LIST_WEBAPPS data = dal.GetDataForApprovePage(id);
                DataSurat dataSurat = dal.GetDataDisposisi(id);
                IEnumerable<DisposisiHistory> history = dal.GetDataHistoryDisposisi(id);

                ViewBag.PROPERTY_ROLE = CurrentUser.UserRoleProperty;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.CONTRACT_OFFER_NAME = data.CONTRACT_OFFER_NAME;
                ViewBag.CONTRACT_OFFER_NO = data.CONTRACT_OFFER_NO;
                ViewBag.CONTRACT_OFFER_TYPE = data.CONTRACT_OFFER_TYPE;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CUSTOMER_AR = data.CUSTOMER_AR;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.CONTRACT_USAGE = data.CONTRACT_USAGE;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.OFFER_STATUS = data.OFFER_STATUS;
                ViewBag.APPROVAL_NEXT_LEVEL = data.APPROVAL_NEXT_LEVEL;

                if(dataSurat == null)
                {
                    ViewBag.SURAT_ID = null;
                    ViewBag.NO_SURAT = "";
                    ViewBag.SUBJECT = "";
                    ViewBag.ISI_SURAT = "";
                    ViewBag.DETAIL_HISTORY = null;
                    ViewBag.DISPOSISI_PARAF = "";
                } else
                {
                    ViewBag.SURAT_ID = dataSurat.ID;
                    ViewBag.NO_SURAT = dataSurat.NO_SURAT;
                    ViewBag.SUBJECT = dataSurat.SUBJECT;
                    ViewBag.ISI_SURAT = dataSurat.ISI_SURAT;
                    ViewBag.DETAIL_HISTORY = history;
                    ViewBag.DISPOSISI_PARAF = data.DISPOSISI_PARAF;
                }

            }
            catch (Exception)
            { }

            return View();
        }

        public JsonResult SetApproveWorkflow(ContractOfferApprovalParameter param)
        {
            dynamic message = null;
            ContractOfferApprovalParameter result = new ContractOfferApprovalParameter();

            if (IsAjaxUtil.validate(HttpContext))
            {
                result = param;
                result.BRANCH_ID = CurrentUser.KodeCabang;
                result.LAST_UPDATE_BY = CurrentUser.UserLoginID;
                result.USER_LOGIN = CurrentUser.UserLoginID;
                PendingListDAL dal = new PendingListDAL();
                try
                {
                    //var res = dal.setContractOfferApproval(result);
                    string res = "";
                    if (res != null)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Your account has released approval!"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "You can not continue the approval release"
                        };
                    }
                }
                catch (Exception ex)
                {
                    message = new
                    {
                        status = "E",
                        message = "Your can not continue the approval release. <br> Error Code: " + ex.Message
                    };
                }

            }
            return Json(message);
        }

        //--------------------------- DATA TABLE OBJECT ------------------------
        public JsonResult GetDataObject(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLE CONDITION ------------------------
        public JsonResult GetDataCondition(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLE MANUAL ------------------------
        public JsonResult GetDataManual(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataManual(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLE MEMO ------------------------
        public JsonResult GetDataMemo(string id)
        {
            DataTablesPendingList result = new DataTablesPendingList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    PendingListDAL dal = new PendingListDAL();
                    result = dal.GetDataMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ContentResult UploadFiles([FromForm] PendingListAttachment data)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "ContractOffer", subPath);

                bool exists = System.IO.Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/ContractOffer/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                dynamic getData = null;
                GetUrl test = new GetUrl();
                string url = test.UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/ContractOffer/" + subPath);
                request.AddFile("file", savedFileName);
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;

                PendingListDAL dal = new PendingListDAL();
                bool result = dal.AddFiles(sFile, file2, KodeCabang, subPath);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachmentOffer result = new DataTablesAttachmentOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractOfferDAL dal = new TransContractOfferDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "ContractOffer", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                PendingListDAL dal = new PendingListDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult SetApproveWorkflownew([FromBody] ContractOfferApprovalParameter param)
        {
            dynamic dataUser = CurrentUser;
            dynamic message = null;
            ContractOfferApprovalParameter result = new ContractOfferApprovalParameter();

            if (IsAjaxUtil.validate(HttpContext))
            {
                result = param;
                result.BRANCH_ID = CurrentUser.KodeCabang;
                result.LAST_UPDATE_BY = CurrentUser.UserLoginID;
                result.USER_LOGIN = CurrentUser.UserLoginID;
                PendingListDAL dal = new PendingListDAL();
                try
                {
                    //var res = dal.setContractOfferApproval(result);
                    var res = dal.setContractOfferApprovalnew(result, dataUser);
                    if (res != null && res.ResultCode.Equals("S"))
                    {
                        message = new
                        {
                            status = "S",
                            message = "Your account has released approval!"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "You can not continue the approval release"
                        };

                        if (res != null)
                        {
                            message.message += "\n" + res.ResultDescription;
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = new
                    {
                        status = "E",
                        message = "Your can not continue the approval release. <br> Error Code: " + ex.Message
                    };
                }

            }
            return Json(message);
        }

        [HttpGet]
        public JsonResult Test(string to)
        {
            Models.GeneralResult.Results result = new Models.GeneralResult.Results();
            EmailConfigurationDAL dal = new EmailConfigurationDAL();
            var body = "asdasdasd";
            body += dal.emailfooter();

            //send email
            Models.EmailConfiguration.EmailConf email = new Models.EmailConfiguration.EmailConf();
            email.EMAIL_TO = to;// "guntur.herdiawan@pelindo.co.id"; //
            email.CASE = "1";//1.email to user || 2.email to pengguna jasa
            email.BODY = body;
            email.DESC = "name : this is teting purpose";
            email.SUBJECT = "[ya] Permohonan Kontrak offer no.";
            var sendingemail = dal.SendMail(email);
            result.Msg = "Sukses kirim email. ";
            result.Status = "S";

            return Json(result);
        }

        [HttpGet]
        public string Test2(string to)
        {

            string url = Helper.ConfigurationFactory.GetConfiguration("urlEmailService", "url");
            //var c = new RestSharp.RestClient("http://10.0.130.15:8089");
            var c = new RestSharp.RestClient(url);
            var verifikasinumber = string.Empty;

            try
            {
                string encryptKey = string.Empty;
                string endMail = "<br><br><br><font size='3' color='blue'><b>Remote</b><br/></font>&nbsp;<font size='2' color='blue'>Virtual office for you</font><br><font size='2' color='green'><i>paperless systems that support go green programs</i></font>";

                string base64 = string.Empty;
                string base64Email = string.Empty;
                string base64Username = string.Empty;


                var request = new RestSharp.RestRequest("/api/MailService", RestSharp.Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.RequestFormat = RestSharp.DataFormat.Json;

                object mail = new
                {
                    emailTo = to,
                    subject = "[Testing mail]",
                    body = "This is testing purpose."+ endMail,
                    apiKey = "201"
                };

                var param = JsonConvert.SerializeObject(mail);
                request.AddParameter("text/json", param, RestSharp.ParameterType.RequestBody);

                // execute the request
                RestSharp.IRestResponse response = c.Execute(request);
                var content = response.Content; // raw content as string

                return content;

            }
            catch (Exception e)
            {
                return "F";
            }
        }

        public JsonResult GetSurat(string id)
        {
            dynamic dataUser = CurrentUser;
            dynamic message = new ExpandoObject(); // recommended 

            try
            {
                PendingListDAL dal = new PendingListDAL();
                DISPOSISI_LIST dataSurat = dal.GetDataSurat(id, dataUser);
                IEnumerable<DisposisiHistory> history = dal.GetDataHistoryDisposisi(id);
                IEnumerable<NextParaf> dataParaf = dal.GetDataNextParaf(id);
                message.dataSurat = dataSurat;
                message.history = history;
                message.nextParaf = dataParaf;
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }
    }
}