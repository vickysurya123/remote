﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContract;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
//using Remote.SAP_CEK_PIUTANG;
using System.Security.Claims;
using System.IO;
//using System.Web.Services;
//using Microsoft.Office.Interop.Word;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Remote.Models.TransContractOffer;
using RestSharp;
using Newtonsoft.Json;

namespace Remote.Controllers
{
    [Authorize]
    public class TransContractController : Controller
    {
        //
        // GET: /TransSpecialContract/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("TransContractIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddTransContract()
        {

            TransContractDAL dal = new TransContractDAL();
            var viewModel = new TransContract
            {
                DDBusinessEntity = dal.GetDataBE2(),
                DDProfitCenter = dal.GetDataProfitCenter(),
                DDContractType = dal.GetDataContractType(),
                DDContractUsage = dal.GetDataContractUsage()
            };
            return View(viewModel);
        }

        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpGet]
        public ActionResult GetDataCustomer(string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //------------------- DATA TABLE HEADER DATA -----------------------
        public JsonResult GetDataTransContract()
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataTransContractnew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATA TABLE DETAIL OBJECT -----------------------
        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATA TABLE DETAIL CONDITION -----------------------
        public JsonResult GetDataDetailCondition(string id)
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string id)
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailMemo(string id)
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------------- UBAH STATUS ---------------------
        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                TransContractDAL dal = new TransContractDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //---------------------- CEK PIUTANG SAP ------------------
        //public JsonResult cekPiutangSAP(string DOC_TYPE, string CUST_CODE)
        //{
        //    string KodeCabang = CurrentUser.KodeCabang;
        //    retCheckLock retLock = new retCheckLock();
        //    CheckLockPiutang cSAP = new CheckLockPiutang();
        //    retLock = cSAP.inboundCheckLock_P3("", DOC_TYPE, KodeCabang, CUST_CODE);
        //    return Json(retLock);
        //}

        //------FILTER SEARCHING DATA CONTRACT OFFER---------------
        public JsonResult GetDataFilter()
        {
            DataTablesContract result = new DataTablesContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataTransContractOffernew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------- GET DATA OBJECT --------------
        public JsonResult GetDataObject(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_RO_OBJECT> result = dal.GetDataObject(id);
            return Json(new { data = result });
        }

        //----------------- GET DATA CONDITION-------------
        public JsonResult GetDataCondition(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_CONDITION> result = dal.GetDataCondition(id);
            return Json(new { data = result });
        }

        //----------------- GET DATA MANUAL FREQUNCY-------
        public JsonResult GetDataManualFrequency(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_MANUAL_FREQUENCY> result = dal.GetDataManualFrequency(id);
            return Json(new { data = result });
        }

        //----------------- GET DATA MEMO------------------
        public JsonResult GetDataMemo(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_MEMO> result = dal.GetDataMemo(id);
            return Json(new { data = result });
        }

        //----------------- GET DATA SALES BASED ---------------
        public JsonResult GetDataSalesBased(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_SALES_BASED> result = dal.GetDataSalesBased(id);
            return Json(new { data = result });
        }

        //----------------- GET DATA REPORTING RULE ------------
        public JsonResult GetDataReportingRule(string id)
        {
            TransContractDAL dal = new TransContractDAL();
            IEnumerable<CO_REPORTING_RULE> result = dal.GetDataReportingRule(id);
            return Json(new { data = result });
        }

        //----------------------------- INSERT DATA HEADER ------------------------
        [HttpPost]
        public JsonResult SaveHeader([FromBody]DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransContractDAL dal = new TransContractDAL();
                    var result = dal.AddHeaderProcedure(KodeCabang, data, User.Identity.Name);

                    if (result.RESULT_STAT)
                    {
                        //var prosedurBilling = dal.prosedurBillingCashflow(result.ID_TRANS, CurrentUser.Name);
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "S",
                            message = "Contract Added Successfully with Contract Number: " + result.ID_TRANS
                        };
                    }
                    else
                    {
                        message = new
                        {
                            trans_id = result.ID_TRANS,
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something Went Wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- INSERT DATA DETAIL OBJECT--------------------
        public JsonResult SaveDetailObject(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    bool result = dal.AddDetailObject(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Detail Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- INSERT DATA DETAIL CONDITION------------------------
        public JsonResult SaveDetailCondition(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    bool result = dal.AddDetailCondition(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Other Services Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- INSERT DATA DETAIL MANUAL FREQUENCY-------------------
        public JsonResult SaveDetailManualFrequency(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    bool result = dal.AddDetailManualFrequency(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Other Services Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- INSERT DATA DETAIL MEMO-----------------------
        public JsonResult SaveDetailMemo(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    //bool result = dal.AddDetailMemo(data);
                    var result = dal.AddDetailMemo(User.Identity.Name, data);


                    if (result.RESULT_STAT)
                    {
                        //var prosedurBilling = dal.prosedurBillingCashflow(result.ID_TRANS, CurrentUser.Name);
                        message = new
                        {
                            status = "S",
                            message = "Data Detail Memo Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }


        //----------------------- INSERT DATA DETAIL SALES BASED-----------------------
        public JsonResult SaveDetailSalesBased(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    //bool result = dal.AddDetailMemo(data);
                    var result = dal.AddDetailSalesBased(User.Identity.Name, data);


                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Detail Sales Based Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- INSERT DATA DETAIL REPORTING RULE ---------------------
        public JsonResult SaveDetailReportingRule(DataTransContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    //bool result = dal.AddDetailMemo(data);
                    var result = dal.AddDetailReportingRule(User.Identity.Name, data);

                    if (result.RESULT_STAT)
                    {
                        //var prosedurBilling = dal.prosedurBillingCashflow(result.ID_TRANS, CurrentUser.Name);
                        message = new
                        {
                            status = "S",
                            message = "Data Detail Reporting Rule Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error insert detail. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------- GENERATE CASHFLOW ------------
        public JsonResult GenerateCashFlow(string id)
        {
            dynamic message = null;

            TransContractDAL dal = new TransContractDAL();
            var prosedurBilling = dal.prosedurBillingCashflow(id, CurrentUser.Name);
            return Json(message);
        }

        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachmentContract result = new DataTablesAttachmentContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransContractDAL dal = new TransContractDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ContentResult UploadFiles([FromForm] AttachmentOffer data)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "Contract", subPath);

                bool exists = System.IO.Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/Contract/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                dynamic getData = null;
                GetUrl test = new GetUrl();
                string url = test.UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/Contract/" + subPath);
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                request.AddFile("file", savedFileName);
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;

                TransContractDAL dal = new TransContractDAL();
                bool result = dal.AddFiles(sFile, file2, KodeCabang, data.BE_ID);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpGet]
        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "Contract", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                TransContractDAL dal = new TransContractDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult exportWordContractCondition(string CONTRACT_NO)
        {
            IEnumerable<AUP_TRANS_CONTRACT_WEBAPPS> listData = null;
            TransContractDAL dal = new TransContractDAL();
            listData = dal.GetCondition(CONTRACT_NO);

            return Json(new { data = listData });
        }

        public string TerbilangINA(double amount, string valuta)
        {
            string valutas = (valuta == "USD") ? " DOLLAR" : " RUPIAH";
            string word = "";
            double divisor = 1000000000000.00; double large_amount = 0;
            double tiny_amount = 0;
            double dividen = 0; double dummy = 0;
            string weight1 = ""; string unit = ""; string follower = "";
            string[] prefix = { "SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            string[] sufix = { "SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            large_amount = Math.Abs(Math.Truncate(amount));
            tiny_amount = Math.Round((Math.Abs(amount) - large_amount) * 100);
            if (large_amount > divisor)
                return "OUT OF RANGE";
            while (divisor >= 1)
            {
                dividen = Math.Truncate(large_amount / divisor);
                large_amount = large_amount % divisor;
                unit = "";
                if (dividen > 0)
                {
                    if (divisor == 1000000000000.00)
                        unit = "TRILYUN ";
                    else
                        if (divisor == 1000000000.00)
                        unit = "MILYAR ";
                    else
                            if (divisor == 1000000.00)
                        unit = "JUTA ";
                    else
                                if (divisor == 1000.00)
                        unit = "RIBU ";
                }
                weight1 = "";
                dummy = dividen;
                if (dummy >= 100)
                    weight1 = prefix[(int)Math.Truncate(dummy / 100) - 1] + "RATUS ";
                dummy = dividen % 100;
                if (dummy < 10)
                {
                    if (dummy == 1 && unit == "RIBU ")
                        weight1 += "SE";
                    else
                        if (dummy > 0)
                        weight1 += sufix[(int)dummy - 1];
                }
                else
                    if (dummy >= 11 && dummy <= 19)
                {
                    weight1 += prefix[(int)(dummy % 10) - 1] + "BELAS ";
                }
                else
                {
                    weight1 += prefix[(int)Math.Truncate(dummy / 10) - 1] + "PULUH ";
                    if (dummy % 10 > 0)
                        weight1 += sufix[(int)(dummy % 10) - 1];
                }
                word += weight1 + unit;
                divisor /= 1000.00;
            }
            if (Math.Truncate(amount) == 0)
                word = "NOL ";
            follower = "";
            if (tiny_amount < 10)
            {
                if (tiny_amount > 0)
                    follower = "KOMA NOL " + sufix[(int)tiny_amount - 1];
            }
            else
            {
                follower = "KOMA " + sufix[(int)Math.Truncate(tiny_amount / 10) - 1];
                if (tiny_amount % 10 > 0)
                    follower += sufix[(int)(tiny_amount % 10) - 1];
            }
            word += follower;
            if (amount < 0)
            {
                word = "MINUS " + word + valutas;
            }
            else
            {
                word = word + valutas;
            }
            return word.Trim();
        }

        public string TerbilangAngka(double angka)
        {
            string word = "";
            double divisor = 1000000000000.00; double large_amount = 0;
            double tiny_amount = 0;
            double dividen = 0; double dummy = 0;
            string weight1 = ""; string unit = ""; string follower = "";
            string[] prefix = { "SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            string[] sufix = { "SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            large_amount = Math.Abs(Math.Truncate(angka));
            tiny_amount = Math.Round((Math.Abs(angka) - large_amount) * 100);
            if (large_amount > divisor)
                return "OUT OF RANGE";
            while (divisor >= 1)
            {
                dividen = Math.Truncate(large_amount / divisor);
                large_amount = large_amount % divisor;
                unit = "";
                if (dividen > 0)
                {
                    if (divisor == 1000000000000.00)
                        unit = "TRILYUN ";
                    else
                        if (divisor == 1000000000.00)
                        unit = "MILYAR ";
                    else
                            if (divisor == 1000000.00)
                        unit = "JUTA ";
                    else
                                if (divisor == 1000.00)
                        unit = "RIBU ";
                }
                weight1 = "";
                dummy = dividen;
                if (dummy >= 100)
                    weight1 = prefix[(int)Math.Truncate(dummy / 100) - 1] + "RATUS ";
                dummy = dividen % 100;
                if (dummy < 10)
                {
                    if (dummy == 1 && unit == "RIBU ")
                        weight1 += "SE";
                    else
                        if (dummy > 0)
                        weight1 += sufix[(int)dummy - 1];
                }
                else
                    if (dummy >= 11 && dummy <= 19)
                {
                    weight1 += prefix[(int)(dummy % 10) - 1] + "BELAS ";
                }
                else
                {
                    weight1 += prefix[(int)Math.Truncate(dummy / 10) - 1] + "PULUH ";
                    if (dummy % 10 > 0)
                        weight1 += sufix[(int)(dummy % 10) - 1];
                }
                word += weight1 + unit;
                divisor /= 1000.00;
            }
            if (Math.Truncate(angka) == 0)
                word = "NOL ";
            follower = "";
            if (tiny_amount < 10)
            {
                if (tiny_amount > 0)
                    follower = "KOMA NOL " + sufix[(int)tiny_amount - 1];
            }
            else
            {
                follower = "KOMA " + sufix[(int)Math.Truncate(tiny_amount / 10) - 1];
                if (tiny_amount % 10 > 0)
                    follower += sufix[(int)(tiny_amount % 10) - 1];
            }
            word += follower;
            if (angka < 0)
            {
                word = "MINUS " + word ;
            }
            else
            {
                word = word ;
            }
            return word.Trim();

            //string strterbilang = "";
            //// membuat array untuk mengubah 1 - 11 menjadi terbilang
            //string[] a = { "", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas" };

            //if (angka < 12)
            //{
            //    strterbilang = " " + a[angka];
            //}
            //else if (angka < 20)
            //{
            //    strterbilang = this.TerbilangAngka(angka - 10) + " belas";
            //}
            //else if (angka < 100)
            //{
            //    strterbilang = this.TerbilangAngka(angka / 10) + " puluh" + this.TerbilangAngka(angka % 10);
            //}
            //else if (angka < 200)
            //{
            //    strterbilang = " seratus" + this.TerbilangAngka(angka - 100);
            //}
            //else if (angka < 1000)
            //{
            //    strterbilang = this.TerbilangAngka(angka / 100) + " ratus" + this.TerbilangAngka(angka % 10);
            //}
            //else if (angka < 2000)
            //{
            //    strterbilang = " seribu" + this.TerbilangAngka(angka - 1000);
            //}
            //else if (angka < 1000000)
            //{
            //    strterbilang = this.TerbilangAngka(angka / 1000) + " ribu" + this.TerbilangAngka(angka % 1000);
            //}
            //else if (angka < 1000000000)
            //{
            //    strterbilang = this.TerbilangAngka(angka / 1000000) + " juta" + this.TerbilangAngka(angka % 1000000);
            //}

            //// menghilangkan multiple space
            //strterbilang = System.Text.RegularExpressions.Regex.Replace(strterbilang, @"^\s+|\s+$", " ");
            //// mengembalikan hasil terbilang
            //return strterbilang;
        }

            //[WebMethod]
        public JsonResult exportWordContract(string BE_ID, string CUSTOMER_NAME, string RO_CERTIFICATE_NUMBER,
                                          string LUAS_TANAH, string LUAS_BANGUNAN, string RO_ADDRESS, string USER_NAME,
                                          string CONTRACT_USAGE, string TERM_IN_MONTHS, string CONTRACT_START_DATE, string BUSINESS_PARTNER_NAME,
                                          string CONTRACT_END_DATE, string CONTRACT_TYPE, string NORMALISASI, string REGIONAL_NAME,
                                          string TOTAL_NET_VALUE, string VALID_FROM, string VALID_TO, string SEWA_DARATAN, string SEWA_BANGUNAN,
                                          string PROFIT_CENTER_NAME, string ADMINISTRASI, string WARMERKING, string PENGALIHAN, string NJOP_BANGUNAN, string NJOP_DARATAN,
                                          string TOTAL_SEWA_DARATAN, string TOTAL_SEWA_BANGUNAN)
        {

            nama result = new nama();
            string fileName = "";
            
            int Month = Convert.ToInt32(TERM_IN_MONTHS) % 12;
            int Years = Convert.ToInt32(TERM_IN_MONTHS) / 12;

            string LUAS = string.Empty;
            if (LUAS_TANAH == "0")
            {
                LUAS = LUAS_BANGUNAN;
            }
            else if (LUAS_BANGUNAN == "0")
            {
                LUAS = LUAS_TANAH;
            }
            else
            {
                LUAS = LUAS_TANAH;
            }

            string SEWA = string.Empty;
            if (SEWA_BANGUNAN == "0")
            {
                SEWA = SEWA_DARATAN;
            }
            else if (SEWA_DARATAN == "0")
            {
                SEWA = SEWA_BANGUNAN;
            }
            else
            {
                SEWA = SEWA_BANGUNAN;
            }

            string TOTALNYA = string.Empty;
            if (TOTAL_SEWA_BANGUNAN == "0")
            {
                TOTALNYA = TOTAL_SEWA_DARATAN;
            }
            else if (TOTAL_SEWA_DARATAN == "0")
            {
                TOTALNYA = TOTAL_SEWA_BANGUNAN;
            }
            else
            {
                TOTALNYA = TOTAL_SEWA_BANGUNAN;
            }

            string NJOP_PERCENTT = string.Empty;
            if (NJOP_BANGUNAN == null)
            {
                NJOP_PERCENTT = NJOP_DARATAN;
            }
            else if (NJOP_DARATAN == null)
            {
                NJOP_PERCENTT = NJOP_BANGUNAN;
            }
            else
            {
                NJOP_PERCENTT = NJOP_BANGUNAN;
            }

            var JUMLAH_SEWA = 0.0;
            if (NJOP_PERCENTT == "0")
            {
                JUMLAH_SEWA = Convert.ToDouble(LUAS) * Convert.ToDouble(SEWA) * Convert.ToDouble(Years);
            }
            else
            {
                JUMLAH_SEWA = Convert.ToDouble(LUAS) * (Convert.ToDouble(NJOP_PERCENTT) * Convert.ToDouble(SEWA)) * Convert.ToDouble(Years);
            }

            var PPN = 0.1 * Convert.ToDouble(TOTALNYA);

            var JUMLAH_SELURUH = Convert.ToDouble(TOTALNYA) + Convert.ToDouble(PPN) + Convert.ToDouble(ADMINISTRASI) + Convert.ToDouble(WARMERKING) + 6000;

            var JUMLAH_NOR = Convert.ToDouble(LUAS) * (Convert.ToDouble(NJOP_PERCENTT) * Convert.ToDouble(NORMALISASI)) * Years;
            var PPN_NOR = 0.1 * JUMLAH_NOR;

            var JUMLAH_NORMALISASI = JUMLAH_NOR + PPN_NOR;

            var TERBILANG = TerbilangINA(Convert.ToDouble(TOTALNYA), "RUPIAH");
            var TERBILANG_BULAN = TerbilangAngka(Month);
            var TERBILANG_TAHUN = TerbilangAngka(Years);
            var TERBILANG_LUAS = TerbilangAngka(Convert.ToDouble(LUAS));
            var ADMIN = Convert.ToDouble(ADMINISTRASI);
            var WARME = Convert.ToDouble(WARMERKING);
            var SUM = Convert.ToDouble(TOTALNYA);
            var SEW = Convert.ToDouble(SEWA);

            //var application = new Application();
            //var document = new Document();

            //var SELURUH = SEWA + ADMINISTRASI + WARMERKING;

            //if (CONTRACT_TYPE == "ZC01 - Kontrak Baru")
            //{
            //    document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormKontrak/FormPerjanjian.docx")));
            //    application.Visible = true;
            //    fileName = "ZC01 - Permohonan Baru";
            //}
            //else if (CONTRACT_TYPE == "ZC02 - Kontrak Perpanjangan")
            //{
            //    document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormKontrak/FormPerjanjian.docx")));
            //    application.Visible = true;
            //    fileName = "ZC02 - Permohonan Perpanjangan";
            //}
            //else if (CONTRACT_TYPE == "ZC03 - Kontrak Peralihan")
            //{
            //    document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormKontrak/FormPerjanjian.docx")));
            //    application.Visible = true;
            //    fileName = "ZC03 - Permohonan Peralihan";
            //}

            //foreach (Field field in document.Fields)
            //{
            //    if (field.Code.Text.Contains("Customername"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CUSTOMER_NAME);
            //    }
            //    else if (field.Code.Text.Contains("Cabang"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(BE_ID);
            //    }
            //    else if (field.Code.Text.Contains("Legalcontractno"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(RO_CERTIFICATE_NUMBER);
            //    }
            //    else if (field.Code.Text.Contains("Luas"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(LUAS);
            //    }
            //    else if (field.Code.Text.Contains("AlamatRO"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(RO_ADDRESS);
            //    }   
            //    else if (field.Code.Text.Contains("Contractusage")) 
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_USAGE);
            //    }
            //    else if (field.Code.Text.Contains("Tahun"))
            //    {
            //        field.Select();
            //        if (Years == 0)
            //        {
            //            application.Selection.TypeText("");
            //        }
            //        else
            //        {
            //            application.Selection.TypeText(Convert.ToString(Years) + " Tahun ");
            //        }
            //    }
            //    else if (field.Code.Text.Contains("Bulan"))
            //    {
            //        field.Select();
            //        if (Month == 0)
            //        {
            //            application.Selection.TypeText(" ");
            //        }
            //        else
            //        {
            //            application.Selection.TypeText(Convert.ToString(Month) + " Bulan ");
            //        }
            //    }
            //    else if (field.Code.Text.Contains("Contractstartdate"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_START_DATE);
            //    }
            //    else if (field.Code.Text.Contains("Contractenddate"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_END_DATE);
            //    }
            //    else if (field.Code.Text.Contains("Regional"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(REGIONAL_NAME);
            //    }
            //    else if (field.Code.Text.Contains("Total"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(SUM.ToString("n0"));
            //    }
            //    else if (field.Code.Text.Contains("Validfrom"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(VALID_FROM);
            //    }
            //    else if (field.Code.Text.Contains("Validto"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(VALID_TO);
            //    }
            //    else if (field.Code.Text.Contains("Profitcenter"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(PROFIT_CENTER_NAME);
            //    }
            //    else if (field.Code.Text.Contains("Sewa"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(SEW.ToString("n0"));
            //    }
            //    else if (field.Code.Text.Contains("Normalisasi"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(NORMALISASI);
            //    }
            //    else if (field.Code.Text.Contains("Pengalihan"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(PENGALIHAN);
            //    }
            //    else if (field.Code.Text.Contains("Administrasi"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(ADMINISTRASI);
            //    }
            //    else if (field.Code.Text.Contains("Warmerking"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(WARMERKING);
            //    }
            //    else if (field.Code.Text.Contains("Partner"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(BUSINESS_PARTNER_NAME);
            //    }                
            //    else if (field.Code.Text.Contains("Username"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(USER_NAME);
            //    }
            //    else if (field.Code.Text.Contains("Njop"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(NJOP_PERCENTT);
            //    }
            //    else if (field.Code.Text.Contains("Terbilang"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(TERBILANG.ToString());
            //    }
            //    else if (field.Code.Text.Contains("TerbilangT"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(TERBILANG_TAHUN.ToString());
            //    }
            //    else if (field.Code.Text.Contains("TerbilangLuas"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(TERBILANG_LUAS.ToString());
            //    }
            //    else if (field.Code.Text.Contains("PPN"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(PPN.ToString("n0"));
            //    }

            //}

            //document.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/DownloadForm/") + fileName + ".docx"));
            //document.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/DownloadForm/") + fileName + ".docx"));
            //document.Close();
            //application.Quit();

            result.fileNames = fileName + ".docx";
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }
         

        //[WebMethod]
        public JsonResult exportWordBA(string BE_ID, string CUSTOMER_NAME, string RO_CERTIFICATE_NUMBER,
                                          string LUAS_TANAH, string LUAS_BANGUNAN, string RO_ADDRESS,
                                          string CONTRACT_USAGE, string TERM_IN_MONTHS, string CONTRACT_START_DATE,
                                          string CONTRACT_END_DATE, string CONTRACT_TYPE)
        {
            nama result = new nama();
            string fileName = "BAijinprinsip";

            int Month = Convert.ToInt32(TERM_IN_MONTHS) % 12; 
            int Years = Convert.ToInt32(TERM_IN_MONTHS) / 12;
             
            string LUAS = string.Empty;
            if (LUAS_TANAH == "0")
            {
                LUAS = LUAS_BANGUNAN;
            }
            else if (LUAS_BANGUNAN == "0")
            {
                LUAS = LUAS_TANAH;
            }
            else
            {
                LUAS = LUAS_TANAH;
            }

            //var application = new Application();
            //var document = new Document();

            //document = application.Documents.Add(Template: Path.Combine(Server.MapPath("~/REMOTE/FormBAijinprinsip/BAijinprinsip.docx")));
            //application.Visible = true;

            //foreach (Field field in document.Fields)
            //{
            //    if (field.Code.Text.Contains("Customername"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CUSTOMER_NAME); 
            //    }
            //    else if (field.Code.Text.Contains("Cabang"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(BE_ID);
            //    }
            //    else if (field.Code.Text.Contains("Luas"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(LUAS);
            //    }
            //    else if (field.Code.Text.Contains("AlamatRO"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(RO_ADDRESS);
            //    }
            //    else if (field.Code.Text.Contains("Contractusage"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_USAGE);
            //    }
            //    else if (field.Code.Text.Contains("Contractstartdate"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_START_DATE);
            //    }
            //    else if (field.Code.Text.Contains("Contractenddate"))
            //    {
            //        field.Select();
            //        application.Selection.TypeText(CONTRACT_END_DATE);
            //    }
            //}

            //document.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/DownloadForm/") + fileName + ".docx"));
            //document.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/DownloadForm/") + fileName + ".docx"));
            //document.Close();
            //application.Quit();

            result.fileNames = fileName + ".docx";
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        //[WebMethod]
        //public void deleteWord(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/DownloadForm/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //-------- TERMINATED CONTRACT --------
        [HttpPost]
        public JsonResult TerminatedContract([FromBody]DataTerminateContract data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    bool result = dal.getTerminatedContract(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Contract Has Been Terminated."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Terminate Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);            
        }
        public JsonResult GetOldData(string id)
        {
            dynamic result = new { Status = "E", Message = "Initial" };

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    TransContractDAL dal = new TransContractDAL();
                    var data = dal.GetOldData(id);
                    result = new { Status = "S", Message = data };
                }
                catch (Exception e)
                {
                    result = new { Status = "E", Message = e.Message };
                }
            }

            return Json(result);
        }
        public JsonResult CheckBilling(string id)
        {
            dynamic message = null;

            try
            {
                TransContractDAL dal = new TransContractDAL();
                message = dal.CheckBilling(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }
    }
}