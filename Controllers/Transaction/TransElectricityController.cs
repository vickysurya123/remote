﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransWE;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Remote.Models;
using Remote.Models.TransElectricity;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Remote.Controllers
{
    public class TransElectricityController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //
        // GET: /TransElectricity/
        public ActionResult Index()
        {
            return View("TransElectricityIndex");
        }

        // Belum ada data transaksi bulan sebelumnya tapi bisa
        public ActionResult AddTransElectricity(string id)
        {
             try
            {
                TransElectricityDAL dal = new TransElectricityDAL();
                AUP_TRANS_ELECTRICITY data = dal.GetDataForEdit(id);

                string bulansekarang = "";
                string bulankemarin = "";
                string bulankemarin1 = "";
                string bulankemarin2 = "";
                string bulankemarin3 = "";
                string bulankemarin4 = "";
                string bulankemarin5 = "";
                string bulankemarin6 = "";
                string bulankemarin7 = "";
                string bulankemarin8 = "";
                string bulankemarin9 = "";
                string bulankemarin10 = "";

                bulansekarang = DateTime.Now.ToString("MM.yyyy");
                bulankemarin = DateTime.Now.AddMonths(-1).ToString("MM.yyyy");
                bulankemarin1 = DateTime.Now.AddMonths(-2).ToString("MM.yyyy");
                bulankemarin2 = DateTime.Now.AddMonths(-3).ToString("MM.yyyy");
                bulankemarin3 = DateTime.Now.AddMonths(-4).ToString("MM.yyyy");
                bulankemarin4 = DateTime.Now.AddMonths(-5).ToString("MM.yyyy");
                bulankemarin5 = DateTime.Now.AddMonths(-6).ToString("MM.yyyy");
                bulankemarin6 = DateTime.Now.AddMonths(-7).ToString("MM.yyyy");
                bulankemarin7 = DateTime.Now.AddMonths(-8).ToString("MM.yyyy");
                bulankemarin8 = DateTime.Now.AddMonths(-9).ToString("MM.yyyy");
                bulankemarin9 = DateTime.Now.AddMonths(-10).ToString("MM.yyyy");
                bulankemarin10 = DateTime.Now.AddMonths(-11).ToString("MM.yyyy");

                ViewBag.BULAN_KEMARIN = bulankemarin;
                ViewBag.BULAN_KEMARIN1 = bulankemarin1;
                ViewBag.BULAN_KEMARIN2 = bulankemarin2;
                ViewBag.BULAN_KEMARIN3 = bulankemarin3;
                ViewBag.BULAN_KEMARIN4 = bulankemarin4;
                ViewBag.BULAN_KEMARIN5 = bulankemarin5;
                ViewBag.BULAN_KEMARIN6 = bulankemarin6;
                ViewBag.BULAN_KEMARIN7 = bulankemarin7;
                ViewBag.BULAN_KEMARIN8 = bulankemarin8;
                ViewBag.BULAN_KEMARIN9 = bulankemarin9;
                ViewBag.BULAN_KEMARIN10 = bulankemarin10;
                ViewBag.BULAN_SEKARANG = bulansekarang;
               
                string KodeCabang = CurrentUser.KodeCabang;

                ViewBag.KODE_CABANG = KodeCabang;
                ViewBag.ID = data.ID;
                ViewBag.INSTALLATION_CODE = data.INSTALLATION_CODE;
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.INSTALLATION_NUMBER = data.INSTALLATION_NUMBER;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.MINIMUM_USED = data.MINIMUM_USED;
                ViewBag.POWER_CAPACITY = data.POWER_CAPACITY;
                ViewBag.MINIMUM_PAYMENT = data.MINIMUM_PAYMENT;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.BIAYA_ADMIN = data.BIAYA_ADMIN;
                ViewBag.BIAYA_BEBAN = data.BIAYA_BEBAN;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;

                // Ambil nilai terakhir untuk LWBP, WBP, KVARH, BLOK1, BLOK2
            }
            catch (Exception)
            {

            }
            return View();
        }

        public JsonResult getLastLWBP(string id_instalasi)
        {
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastLWBP(id_instalasi);
            return Json(new { data = result });
        }

        public JsonResult getLastWBP(string id_instalasi)
        {
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_WBP> result = dal.getLastWBP(id_instalasi);
            return Json(new { data = result });
        }

        public JsonResult getLastKVARH(string id_instalasi)
        {
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastKVARH(id_instalasi);
            return Json(new { data = result });
        }

        public JsonResult getLastBLOKSATU(string id_instalasi)
        {
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastBLOKSATU(id_instalasi);
            return Json(new { data = result });
        }

        // Tidak jadi terpakai
        public JsonResult getDataEelectricityInstallation(string id, DataTransElectricity data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.getDataEelectricityInstallation(User.Identity.Name, data, id);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            ro_number = result.RO_NUMBER,
                            ro_id = result.RO_ID,
                            status = "S",
                            message = "Rental Object Has Been Created, RO Number : " + result.RO_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to fetch data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Someting went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }
        

        public JsonResult GetDataPricing(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DT_PRICING> result = dal.GetDataPricing(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataCosting(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransElectricityDAL dal = new TransElectricityDAL();
            IEnumerable<DT_COSTING> result = dal.GetDataCosting(id, KodeCabang);
            return Json(new { data = result });
        }
        
        [HttpPost]
        public JsonResult AddData([FromBody] DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.WE_NUMBER,
                            status = "S",
                            message = "Data Added Successfully with Electricity Transaction ID :" + result.WE_NUMBER

                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult AddDataUsed([FromForm] DataTransWE data)
        {
            // var data_detail = Request.Form.ToList();

            // JObject jsonObject = JsonConvert.DeserializeObject<JObject>(data.GetRawText());
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.AddDetailUsed(data);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            
                            status = "S",
                            message = "Data Added Successfully"

                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult AddDetailPerhitungan(DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.AddDetailPerhitungan(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.WE_NUMBER,
                            status = "S",
                            message = "Data Added Successfully with Electricity Transaction ID :" + result.WE_NUMBER

                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult AddDetailPricing(DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.AddDetailPricing(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.WE_NUMBER,
                            status = "S",
                            message = "Data Added Successfully with Electricity Transaction ID :" + result.WE_NUMBER

                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }   

        public JsonResult AddDetailCosting(DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    var result = dal.AddDetailCosting(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.WE_NUMBER,
                            status = "S",
                            message = "Data Added Successfully with Electricity Transaction ID :" + result.WE_NUMBER

                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        // Update Data Header Transaksi Listrik
        [HttpPost]
        public JsonResult UpdateDataHeader([FromBody] DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransElectricityDAL dal = new TransElectricityDAL();
                    bool result = dal.UpdateDataHeader(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        // Save Detail Pricing
        [HttpPost]
        public JsonResult UpdateDetailPricing([FromBody] DataTransWE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    TransElectricityDAL dal = new TransElectricityDAL();
                    bool result = dal.UpdateDetailPricing(User.Identity.Name, data, KodeCabang);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult getLastPeriod(string id_instalasi, string periode)
        {
            TransWEDAL dal = new TransWEDAL();
            string KodeCabang = CurrentUser.KodeCabang;
            IEnumerable<AUP_TRANS_WE_WEBAPPS> result = dal.getLastPeriod(id_instalasi, periode, KodeCabang);
            return Json(new { data = result });
        }

        [HttpGet]
        public async Task<IActionResult> GetListCoa()
        {
            try
            {
                var queryString = Request.Query;
                string draw = queryString["draw"];
                int start = int.Parse(queryString["start"]);
                int length = int.Parse(queryString["length"]);
                string search = queryString["search[value]"];
                string tes = "%" + search;

                int panjang = ((start + 10) / 10);

                // api get coa
                string url = "https://fuse19.pelindo.co.id/master/mdm/silaporcoaprod/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                if (search != "")
                {
                    draw = "1";
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "408",
                            COA_PROD2 = "01",
                            STAT = "D",
                            DESKRIPSI = tes,
                            COA_PROD = tes,
                            GL_TEXT = tes,
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }
                else
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "408",
                            COA_PROD2 = "01",
                            STAT = "D",
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var returnData = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);

                List<object> result = new List<object>();

                result.Add(new
                {
                    draw = draw,
                    recordsFiltered = returnData.paging.totalpages,
                    recordsTotal = returnData.paging.total,
                    data = returnData.result,
                });

                return Ok(responseOrg.Content);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = e.Message
                });
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetListCoaElectric()
        {
            try
            {
                var queryString = Request.Query;
                string draw = queryString["draw"];
                int start = int.Parse(queryString["start"]);
                int length = int.Parse(queryString["length"]);
                string search = queryString["search[value]"];
                string tes = "%" + search;

                int panjang = ((start + 10) / 10);

                // api get coa
                string url = "https://fuse19.pelindo.co.id/master/mdm/silaporcoaprod/data/search-criteria";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                if (search != "")
                {
                    draw = draw;
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "408",
                            COA_PROD2 = "02",
                            STAT = "D",
                            DESKRIPSI = tes,
                            COA_PROD = tes,
                            GL_TEXT = tes,
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }
                else
                {
                    var body = new
                    {
                        identity = new
                        {
                            applicationid = "REMOTE",
                            reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c",
                            reqdate = DateTime.Now
                        },
                        paging = new
                        {
                            page = panjang.ToString(),
                            limit = "10"
                        },
                        parameter = new
                        {
                            COA_PROD1 = "408",
                            COA_PROD2 = "02",
                            STAT = "D",
                            column = "COA_PROD, COA_PROD1, GL_TEXT, DESKRIPSI"
                        }
                    };
                    request.AddJsonBody(body);
                }

                client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
                dynamic responseOrg = client.Execute<dynamic>(request);
                var returnData = responseOrg.Data;
                dynamic result = null;
                if(returnData.Count > 2)
                {
                    result = new
                    {
                        draw = draw,
                        recordsFiltered = returnData["paging"]["totalpages"],
                        recordsTotal = returnData["paging"]["total"],
                        data = returnData["result"],
                    };
                }
                else
                {
                    result = new
                    {
                        draw = draw,
                        recordsFiltered = 0,
                        recordsTotal = 0,
                        data = new List<object>(),
                    };
                }

                return Ok(result);
            }
            catch (Exception e)
            {
                 return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                  message = e.Message
                });
                
            }

        }
    }
}