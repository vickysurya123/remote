﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models.MonitoringKronologisMaster;
using Remote.Models;
using Remote.ViewModels;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
/*using System.Security.Claims;*/
using Microsoft.AspNetCore.Http;
using ClosedXML.Excel;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using RestSharp;
using System.Net.Http;

namespace Remote.Controllers
{
    [Authorize]
    public class MonitoringKronologisController : Controller
    {
        // GET: Parameter   
        public ActionResult Index(string cabang)
        {
            try
            {
                ViewBag.PARAM = cabang;
            }
            catch
            {
                ViewBag.PARAM = string.Empty;
            }

            ApprovalSettingDAL dal1 = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBusinessEntity = dal1.GetDataListBusinessEntity(CurrentUser.KodeCabang),
            };
            return View("MonitoringKronologisIndex", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MonitoringKronologisDal dal = new MonitoringKronologisDal();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                    /*{D:\pel3remotev3-web\Models\NotificationContract\;*/
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetail(string id)
        {
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MonitoringKronologisDal dal = new MonitoringKronologisDal();
                    result = dal.GetDataDetail(draw, start, length, search, id);
                }
                catch (Exception)
                {
                    /*{D:\pel3remotev3-web\Models\NotificationContract\;*/
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetailEdit(string id)
        {
            MonitoringKronologisDal dal = new MonitoringKronologisDal();
            IEnumerable<AUP_MONITORING_KRONOLOGIS> result = dal.GetDataDetailEdit(id);

            return Json(new { data = result });
        }

        public JsonResult GetDataDownload(string id)
        {
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MonitoringKronologisDal dal = new MonitoringKronologisDal();
                    result = dal.GetDataDownload(draw, start, length, search, id);
                }
                catch (Exception)
                {
                    /*{D:\pel3remotev3-web\Models\NotificationContract\;*/
                }
            }

            return Json(result);
        }

        public IActionResult Add()
        {
            ViewBag.CREATED_BY = User.Identity.Name;

            MonitoringKronologisDal dal = new MonitoringKronologisDal();
            ViewBag.DDCabangs = dal.GetDataCabang();
            ViewBag.DDRegionals = dal.GetDataRegional();

            DataMonitoringKronologis dataMonitoringKronologis = new DataMonitoringKronologis();
            dataMonitoringKronologis.detailKronologis = new List<detailKronologis>();
            return View("../MonitoringKronologis/Form", dataMonitoringKronologis);
        }

        public IActionResult Edit(string id)
        {
            ViewBag.CREATED_BY = User.Identity.Name;

            MonitoringKronologisDal dal = new MonitoringKronologisDal();

            ViewBag.DDCabangs = dal.GetDataCabang();
            ViewBag.DDRegionals = dal.GetDataRegional();

            DataMonitoringKronologis dataMonitoringKronologis = dal.GetFullData(id);

            if (dataMonitoringKronologis == null)
            {
                return BadRequest("Not Found");
            }

            return View("../MonitoringKronologis/Form", dataMonitoringKronologis);
        }

        public JsonResult GetDataMonitoring(string be_id)
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int beid = int.Parse(be_id);
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MonitoringKronologisDal dal = new MonitoringKronologisDal();
                    result = dal.GetDataMonitoring(beid, draw, start, length, search);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult Delete([FromBody] DataMonitoringKronologis data, String id)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    MonitoringKronologisDal dal = new MonitoringKronologisDal();
                    result = dal.DeleteDataHeader(data, id);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        /* public async Task<IActionResult> InsertHeader(DataMonitoringKronologis data, IEnumerable<IFormFile> files)
         {*/
        public JsonResult SavHeaderMonitoring([FromBody] DataMonitoringKronologis data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    string id = queryString["id"];

                    MonitoringKronologisDal dal = new MonitoringKronologisDal();

                    result = (data.ID != null ? dal.EditDataMonitoring(data, dataUser) : dal.AddDataMonitoring(data, dataUser));

                    //result = dal.AddDataHeader(data, id);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public IActionResult save(DataMonitoringKronologis dataMonitoringKronologis)
        {
            if (dataMonitoringKronologis.detailKronologis == null)
            {
                dataMonitoringKronologis.detailKronologis = new List<detailKronologis>();
            }
            foreach (var detailKronologis in dataMonitoringKronologis.detailKronologis)
            {
                if(detailKronologis.FILE == null)
                {
                    continue;
                }

                string url = new GetUrl().UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/MonKronologis");
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");

                var file = detailKronologis.FILE;
                StreamContent streamContent = new StreamContent(file.OpenReadStream());
                request.AddFileBytes("file", streamContent.ReadAsByteArrayAsync().Result, file.FileName);

                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;
                detailKronologis.DIRECTORY = file2;
                detailKronologis.FILE_NAME = file.FileName;
            }

            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                MonitoringKronologisDal dal = new MonitoringKronologisDal();
                var r = new List<UploadFilesResult>();
                bool result = dal.AddDataMonitoring(dataMonitoringKronologis, CurrentUser);

                if (result)
                {
                    return Ok(new
                    {
                        STATUS = "S"
                    });
                } else
                {
                    return Ok(new
                    {
                        STATUS = "E"
                    });
                }

            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Ok(new
            {
                STATUS = "S"
            });
        }

        [HttpPost]
        public IActionResult saveEdit(DataMonitoringKronologis dataMonitoringKronologis)
        {
            if (dataMonitoringKronologis.detailKronologis == null)
            {
                dataMonitoringKronologis.detailKronologis = new List<detailKronologis>();
            }
            foreach (var detailKronologis in dataMonitoringKronologis.detailKronologis)
            {
                if (detailKronologis.FILE == null)
                {
                    continue;
                }

                string url = new GetUrl().UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/MonKronologis"); 
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");

                var file = detailKronologis.FILE;
                StreamContent streamContent = new StreamContent(file.OpenReadStream());
                request.AddFileBytes("file", streamContent.ReadAsByteArrayAsync().Result, file.FileName);

                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;
                detailKronologis.DIRECTORY = file2;
                detailKronologis.FILE_NAME = file.FileName;
            }

            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                MonitoringKronologisDal dal = new MonitoringKronologisDal();
                var r = new List<UploadFilesResult>();
                bool result = dal.AddDataMonitoringEdit(dataMonitoringKronologis, CurrentUser);

                if (result)
                {
                    return Ok(new
                    {
                        STATUS = "S"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        STATUS = "E"
                    });
                }

            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return Ok(new
            {
                STATUS = "S"
            });
        }

        [HttpPost]
        public IActionResult Approve(string id)
        {
            try
            {
                MonitoringKronologisDal dal = new MonitoringKronologisDal();
                var stats = dal.Approve(id, CurrentUser);

                if (stats)
                {
                    return Ok(new
                    {
                        STATUS = "S"
                    });
                } else
                {
                    return Ok(new
                    {
                        STATUS = "E"
                    });
                }
                
            }
            catch (Exception)
            {
                return Ok( new {
                    STATUS = "E"
                });
            }
        }

        private string DeleteFile(string filename, string uri)
        {
            dynamic message = null;
            var kronologi = uri.Split('/').Last();
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "MonitoringKronologis", kronologi, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        [HttpPost]
        public JsonResult ExportExcel([FromBody] DashExcel data)
        {
            nama result = new nama();
            //dynamic message = null;
            var tableName = data.JUDUL;
            var cabang = data.CABANG;
            var regional = data.REGIONAL;
            var tanggal = data.TANGGAL;

            string fileName = "ReportMonitoringkronologis(" + tableName + ")(" + cabang + ")(" + regional + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                DataMonitoringKronologis dataa = new DataMonitoringKronologis();
                dataa.ID = data.ID;
                MonitoringKronologisDal dal = new MonitoringKronologisDal();
                var list = dal.GetExcel(data);
                dynamic dalShowdata = list.Data;

               

                DataTable dtt = new DataTable();
                dtt.Columns.AddRange(new DataColumn[7] {
                    new DataColumn("NO SURAT", typeof(string)),
                    new DataColumn("TANGGAL SURAT", typeof(string)),
                    new DataColumn("KETERANGAN", typeof(string)),
                    new DataColumn("TINDAK LANJUT",typeof(string)),
                    new DataColumn("KENDALA",typeof(string)),
                    new DataColumn("PROGRESS",typeof(string)),
                    new DataColumn("ATTACHMENT",typeof(string))
                    });

                foreach (var valData2 in dalShowdata)
                {
                    dtt.Rows.Add(valData2.NO_SURAT, valData2.TANGGAL_SURAT,
                                valData2.KETERANGAN, valData2.TINDAK_LANJUT, valData2.KENDALA,
                                valData2.PROGRESS, valData2.FILE_NAME);

                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    //wb.Worksheets.Add(dt, "Report Monitoring Kronologis");
                    /* wb.Worksheets.Add(dtt, "Report Monitoring Detail");
                     wb.Cell(1, 1).InsertData(titles);*/
                    var ws = wb.Worksheets.Add("Report Monitoring Detail"); //add worksheet to workbook

                    ws.Cell(1, 1).InsertData(new string[] { "Cabang" });
                    ws.Cell(1, 2).InsertData(new string[] { cabang });
                    ws.Cell(2, 1).InsertData(new string[] { "Regional" });
                    ws.Cell(2, 2).InsertData(new string[] { regional });
                    ws.Cell(3, 1).InsertData(new string[] { "Judul" });
                    ws.Cell(3, 2).InsertData(new string[] { tableName });
                    ws.Cell(4, 1).InsertData(new string[] { "Tanggal" });
                    ws.Cell(4, 2).InsertData(new string[] { tanggal });
                    ws.Cell(6, 1).InsertTable(dtt);
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));


                }

            }
            catch (Exception) { }
            //return Json(message);

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

    }
}
