﻿using System;
using System.Collections.Generic;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransWEList;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class TransWEListController : Controller
    {
        //
        // GET: /TransWEList/
        public ActionResult Index()
        {
            return View("TransWEListIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Edit(string id)
        {
            TransWEListDAL dal = new TransWEListDAL();
            AUP_TRANS_WE_LIST_WEBAPPS data = dal.GetDataForEdit(id);

            ViewBag.ID = data.ID;
            ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
            ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
            ViewBag.CUSTOMER = data.CUSTOMER;
            ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
            ViewBag.PERIOD = data.PERIOD;
            ViewBag.F_PERIOD = data.F_PERIOD;
            ViewBag.REPORT_ON = data.REPORT_ON;
            ViewBag.METER_FROM = data.METER_FROM;
            ViewBag.METER_TO = data.METER_TO;
            ViewBag.QUANTITY = data.QUANTITY;
            ViewBag.UNIT = data.UNIT;
            ViewBag.MULTIPLY_FACTOR = data.MULTIPLY_FACTOR;
            ViewBag.PRICE = data.PRICE;
            ViewBag.AMOUNT = data.AMOUNT;
            ViewBag.STATUS = data.STATUS;
            ViewBag.INSTALLATION_NUMBER = data.INSTALLATION_NUMBER;
            ViewBag.BILLING_TYPE = data.BILLING_TYPE;
            ViewBag.CANCEL_STATUS = data.CANCEL_STATUS;
            ViewBag.SAP_DOCUMENT_NUMBER = data.SAP_DOCUMENT_NUMBER;
            //ViewBag.REPORT_ON = data.REPORT_ON;
            ViewBag.F_REPORT_ON = data.F_REPORT_ON;
            ViewBag.SURCHARGE = data.SURCHARGE;
            ViewBag.SUB_TOTAL = data.SUB_TOTAL;
            ViewBag.MINIMUM_AMOUNT = data.MINIMUM_AMOUNT;
            ViewBag.GL_ACCOUNT = data.GL_ACCOUNT;
            ViewBag.STATUS_DINAS = data.STATUS_DINAS;
            ViewBag.GRAND_TOTAL = data.GRAND_TOTAL;
            if (string.IsNullOrWhiteSpace(data.RATE) ||  Int32.Parse(data.RATE) <= 1)
            {
                ViewBag.CURRENCY = "IDR";
                ViewBag.SUB_TOTAL_USD = 0;
                ViewBag.GRAND_TOTAL_USD = 0;
            } else
            {
                ViewBag.CURRENCY = "USD";
                ViewBag.SUB_TOTAL_USD =  Int32.Parse(data.SUB_TOTAL) / Int32.Parse(data.RATE);
                ViewBag.GRAND_TOTAL_USD = data.GRAND_TOTAL / Int32.Parse(data.RATE);
            }
            ViewBag.RATE = data.RATE;


            return View();
        }

        //public ActionResult EditElectricity(string inumb, string transaction_number)

        public ActionResult EditElectricity(string transaction_number)
        {
            //ViewBag.WE_ID = inumb;

            try
            {
                TransWEListDAL dal = new TransWEListDAL();
                

                string bulansekarang = "";
                string bulankemarin = "";
                string bulankemarin1 = "";
                string bulankemarin2 = "";
                string bulankemarin3 = "";
                string bulankemarin4 = "";
                string bulankemarin5 = "";
                string bulankemarin6 = "";
                string bulankemarin7 = "";
                string bulankemarin8 = "";
                string bulankemarin9 = "";
                string bulankemarin10 = "";

                bulansekarang = DateTime.Now.ToString("MM.yyyy");
                bulankemarin = DateTime.Now.AddMonths(-1).ToString("MM.yyyy");
                bulankemarin1 = DateTime.Now.AddMonths(-2).ToString("MM.yyyy");
                bulankemarin2 = DateTime.Now.AddMonths(-3).ToString("MM.yyyy");
                bulankemarin3 = DateTime.Now.AddMonths(-4).ToString("MM.yyyy");
                bulankemarin4 = DateTime.Now.AddMonths(-5).ToString("MM.yyyy");
                bulankemarin5 = DateTime.Now.AddMonths(-6).ToString("MM.yyyy");
                bulankemarin6 = DateTime.Now.AddMonths(-7).ToString("MM.yyyy");
                bulankemarin7 = DateTime.Now.AddMonths(-8).ToString("MM.yyyy");
                bulankemarin8 = DateTime.Now.AddMonths(-9).ToString("MM.yyyy");
                bulankemarin9 = DateTime.Now.AddMonths(-10).ToString("MM.yyyy");
                bulankemarin10 = DateTime.Now.AddMonths(-11).ToString("MM.yyyy");

                ViewBag.BULAN_KEMARIN = bulankemarin;
                ViewBag.BULAN_KEMARIN1 = bulankemarin1;
                ViewBag.BULAN_KEMARIN2 = bulankemarin2;
                ViewBag.BULAN_KEMARIN3 = bulankemarin3;
                ViewBag.BULAN_KEMARIN4 = bulankemarin4;
                ViewBag.BULAN_KEMARIN5 = bulankemarin5;
                ViewBag.BULAN_KEMARIN6 = bulankemarin6;
                ViewBag.BULAN_KEMARIN7 = bulankemarin7;
                ViewBag.BULAN_KEMARIN8 = bulankemarin8;
                ViewBag.BULAN_KEMARIN9 = bulankemarin9;
                ViewBag.BULAN_KEMARIN10 = bulankemarin10;
                ViewBag.BULAN_SEKARANG = bulansekarang;

                string KodeCabang = CurrentUser.KodeCabang;
                AUP_TRANS_ELECTRICITY data = dal.GetDataForEditElectricity(transaction_number);
                ViewBag.KODE_CABANG = KodeCabang;
                ViewBag.ID = data.ID;
                
                ViewBag.INSTALLATION_CODE = data.INSTALLATION_CODE;
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.INSTALLATION_NUMBER = data.INSTALLATION_NUMBER;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.MINIMUM_USED = data.MINIMUM_USED;
                ViewBag.POWER_CAPACITY = data.POWER_CAPACITY;
                ViewBag.MINIMUM_PAYMENT = data.MINIMUM_PAYMENT;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.BIAYA_ADMIN = data.BIAYA_ADMIN;
                ViewBag.BIAYA_BEBAN = data.BIAYA_BEBAN;
            }
            catch (Exception)
            {

            }
            return View();
        }

        //------------------- DATATABLE TRANS WE LIST----------------------------
        public JsonResult GetDataWETransList()
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransWEList(draw, start, length, search, status_dinas, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------- DATATABLE TRANS WE LIST NEW ----------------------------
        public JsonResult GetDataWETransListNew()
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string status_dinas = queryString["status_dinas"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransWEListnew(draw, start, length, search, status_dinas, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //Header data yg akan di edi
        [HttpPost]
        public JsonResult getHeaderElectricityEdit([FromBody] HeaderTransWEList data)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<AUP_TRANS_WE_LIST_WEBAPPS> result = dal.getHeaderElectricityEdit(data.TRANSACTION_ID, data.INSTALLATION_ID);
            return Json(new { data = result });
        }

        public JsonResult getLastLWBPFrom(string id_transaksi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastLWBPFrom(id_transaksi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastLWBPTo(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastLWBPTo(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastWBPFrom(string id_transaksi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_WBP> result = dal.getLastWBPFrom(id_transaksi, KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult getUsedLWBP(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_WBP> result = dal.getUsedLWBP(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastWBPTo(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_WBP> result = dal.getLastWBPTo(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getUsedWBP(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_WBP> result = dal.getUsedWBP(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastKVARHFrom(string id_transaksi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastKVARHFrom(id_transaksi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastKVARHTo(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastKVARHTo(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getUsedKVARH(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getUsedKVARH(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastBLOKSATUFrom(string id_transaksi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastBLOKSATUFrom(id_transaksi, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getLastBLOKDUAFrom(string id_transaksi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastBLOKDUAFrom(id_transaksi, KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult getLastBLOKSATUTo(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getLastBLOKSATUTo(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult getUsedBLOKSATU(string id_instalasi)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            TransWEListDAL dal = new TransWEListDAL();
            IEnumerable<DD_ELECTRICITY_TRANS_D> result = dal.getUsedBLOKSATU(id_instalasi, KodeCabang);
            return Json(new { data = result });
        }

        //------------------GET DATA TRANSACTION-----------
        public JsonResult GetDataTransactionWater(string id_trans, string installation_code)
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransactionWater(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception ex)
                {
                    string err;
                    err = ex.Message;

                }
            }

            return Json(result);
        }
        public JsonResult GetDataTransaction(string id_trans, string installation_code)
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransactionWE(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------GET DATA TRANSACTION-----------
        public JsonResult GetDataTransactionDetail(string id_trans, string installation_code)
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransactionWEDetail(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------GET DATA TRANSACTION USED DETAIL-----------
        public JsonResult GetDataTransactionDetailsUsed(string id_trans, string installation_code)
        {
            DataTablesTransWEList result = new DataTablesTransWEList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TransWEListDAL dal = new TransWEListDAL();
                    result = dal.GetDataTransactionWEDetailsUsed(draw, start, length, search, id_trans, installation_code);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-----------UPDATE DATA TRANS WE LIST
        [HttpPost]
        public JsonResult EditData([FromBody] DataTransWEList data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    TransWEListDAL dal = new TransWEListDAL();
                    bool result = dal.Edit(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
	}
}