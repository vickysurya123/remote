﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractOffer;
using Remote.Models.TransContract;
using Remote.Models.TransRentalRequest;
using Remote.Models.TransRR;
using Remote.Models.ContractChange;
using Remote.Models;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace Remote.Controllers
{
    public class ContractChangeController : Controller
    {
        //
        // GET: /ContractChange/
        public ActionResult Index()
        {
            return View("ContractChangeIndex");
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult EditContract(string id)
        {
            ViewBag.CONTRACT_NO = id;
            try
            {
                ContractChangeDAL dal = new ContractChangeDAL();
                AUP_TRANS_CONTRACT_WEBAPPS data = dal.GetDataForEdit(id);

                ViewBag.CONTRACT_NAME = data.CONTRACT_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.IJIN_PRINSIP_NO = data.IJIN_PRINSIP_NO;
                ViewBag.IJIN_PRINSIP_DATE = data.IJIN_PRINSIP_DATE;
                ViewBag.LEGAL_CONTRACT_NO = data.LEGAL_CONTRACT_NO;
                ViewBag.LEGAL_CONTRACT_DATE = data.LEGAL_CONTRACT_DATE;
            }
            catch
            {

            }
            return View("ContractChangeIndex");
        }

        public ActionResult ViewContract(string id)
        {
            ViewBag.CONTRACT_NO = id;
            try
            {
                ContractChangeDAL dal = new ContractChangeDAL();
                AUP_TRANS_CONTRACT_WEBAPPS data = dal.GetDataForEdit(id);

                ViewBag.CONTRACT_NAME = data.CONTRACT_NAME;
                ViewBag.CONTRACT_START_DATE = data.CONTRACT_START_DATE;
                ViewBag.CONTRACT_END_DATE = data.CONTRACT_END_DATE;
                ViewBag.TERM_IN_MONTHS = data.TERM_IN_MONTHS;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.IJIN_PRINSIP_NO = data.IJIN_PRINSIP_NO;
                ViewBag.IJIN_PRINSIP_DATE = data.IJIN_PRINSIP_DATE;
                ViewBag.LEGAL_CONTRACT_NO = data.LEGAL_CONTRACT_NO;
                ViewBag.LEGAL_CONTRACT_DATE = data.LEGAL_CONTRACT_DATE;
            }
            catch
            {

            }
            return View("ContractChangeView");
        }

        public JsonResult GetDataFilter()
        {
            DataTablesContractChange result = new DataTablesContractChange();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataFilterContract(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataObject(string id)
        {
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<CO_RO_OBJECT> result = dal.GetDataObject(id);
            return Json(new { data = result });
        }

        public JsonResult GetDataDetailConditionEdit(string id)
        { 
            string KodeCabang = CurrentUser.KodeCabang;
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailConditionEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataDetailManualFrequencyEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailManualFrequencyEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataDetailMemoEdit(string id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<AUP_TRANS_CONTRACT_OFFER_WEBAPPS> result = dal.GetDataDetailMemoEdit(id, KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownRO(string id)
        {
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<CO_RO_OBJECT> result = dal.GetDataRO(id);
            return Json(new { data = result });
        }

        public JsonResult GetDataMeasType(string id)
        {
            ContractChangeDAL dal = new ContractChangeDAL();
            IEnumerable<DDMeasType> result = dal.GetDataMeasType(id);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult UpdateHeader([FromBody]DataTransContract data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    ContractChangeDAL dal = new ContractChangeDAL();
                    bool result = dal.UpdateHeader(User.Identity.Name, data, KodeCabang, User.Identity.Name);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Contract Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Contract Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataHistoryContract(string id)
        {
            DataTablesHistoryContractChanges result = new DataTablesHistoryContractChanges();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataHistoryContract(draw, start, length, search, KodeCabang, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesContract result = new DataTablesContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetDataDetailCondition(string contract_no, string id_perubahan)
        {
            DataTablesContract result = new DataTablesContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, contract_no, id_perubahan);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string contract_no, string id_perubahan)
        {
            DataTablesContract result = new DataTablesContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, contract_no, id_perubahan);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetailMemo(string contract_no, string id_perubahan)
        {
            DataTablesContract result = new DataTablesContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, contract_no, id_perubahan);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetailPostedBilling(string id)
        {
            DataTablesPostedBilling result = new DataTablesPostedBilling();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailPostedBilling(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataDetailPostedBillingD(string id)
        {
            DataTablesPostedBilling result = new DataTablesPostedBilling();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ContractChangeDAL dal = new ContractChangeDAL();
                    result = dal.GetDataDetailPostedBillingD(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
	}
}