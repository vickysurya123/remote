﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;

namespace Remote.Controllers
{
    [Authorize]
    public class CurrencyRate : Controller
    {
        // GET: Parameter   
        public ActionResult Index()
        {
            return View("../MasterData/CurrencyRate/CurrencyRate");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        [HttpGet]
        public JsonResult GetData()
        {

            //return "Tes";
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasgterCurrencyRate dal = new MasgterCurrencyRate();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [HttpPost]

        public JsonResult Insert([FromBody] CURRENCY_RATE data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    string id = queryString["id"];

                    MasgterCurrencyRate dal = new MasgterCurrencyRate();

                    result = (data.ID != null ? dal.EditDataHeader(data, dataUser) : dal.AddDataHeader(data, dataUser));

                    //result = dal.AddDataHeader(data, id);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult Delete([FromBody] CURRENCY_RATE data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    MasgterCurrencyRate dal = new MasgterCurrencyRate();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetDataCurrency(String q)
        {

            MasgterCurrencyRate dal = new MasgterCurrencyRate();
            IEnumerable<DDCurrency> result = dal.GetDataCurrency(q);

            return Json(new
            {
                results = result
            });
        }
    }
}