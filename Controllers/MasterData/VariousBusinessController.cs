﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterVariousBusiness;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class VariousBusinessController : Controller
    {
        //
        // GET: /AnekaUsaha/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            ViewBag.KodeCabang = KodeCabang;
            ViewBag.UserRole = UserRole;
            return View("AUIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddVB()
        {
            string xPRC = CurrentUser.ProfitCenter;
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            string UserId = CurrentUser.UserID;
            MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
            var viewModel = new VariousBusiness
            {
                DDBusinessEntitys = dal.GetDataBE2(KodeCabang, UserRole),
                DDProfitCenter = dal.GetDataProfit_2(KodeCabang, UserRole, UserId),
                DDServiceGroup = dal.GetDataServiceNew(UserRole, UserId),
                DDUnit = dal.GetDataUnit()
            };
            return View(viewModel);
        }

        //------------------- DATA TABLE ANEKA USAHA ---------------------

        public JsonResult GetDataAnekaUsaha()
        {
            DataTablesVariousBusiness result = new DataTablesVariousBusiness();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    result = dal.GetDataAnekaUsahanew(draw, start, length, search, KodeCabang, UserRole);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------------- INSERT DATA ANEKA USAHA ---------------------------

        [HttpPost]
        public JsonResult AddData([FromBody] DataVariousBusiness data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            service_code = result.SERVICE_CODE,
                            status = "S",
                            message = "Other Services Has Been Created, Service Code :" + result.SERVICE_CODE
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------------- INSERT DATA PRICING ----------------------------

        [HttpPost]
        public JsonResult AddDataPricing([FromBody] DataVariousBusiness data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    bool result = dal.AddDataPricing(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //------------------------------ VIEW DATA EDIT --------------------------------

        public ActionResult EditVB(string id)
        {
            ViewBag.ID = id;
            MasterVariousBusinessDAL dal1 = new MasterVariousBusinessDAL();
            try
            {
                MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                AUP_VARIOUSBUSINESS_Remote data = dal.GetDataForEdit(id);

                ViewBag.ID = data.ID;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.SERVICE_NAME = data.SERVICE_NAME;
                ViewBag.SERVICE_GROUP = data.SERVICE_GROUP;
                ViewBag.GL_ACCOUNT = data.VAL1;
                ViewBag.UNIT = data.UNIT;
                ViewBag.SERVICE_CODE = data.SERVICE_CODE;
            }
            catch (Exception)
            {
                ViewBag.ID = string.Empty;
                ViewBag.BE_ID = string.Empty;
                ViewBag.PROFIT_CENTER = string.Empty;
                ViewBag.SERVICE_NAME = string.Empty;
                ViewBag.SERVICE_GROUP = string.Empty;
                ViewBag.GL_ACCOUNT = string.Empty;
                ViewBag.UNIT = string.Empty;
                ViewBag.SERVICE_CODE = string.Empty;
            }

            var viewModel = new VariousBusiness
            {
                DDUnit = dal1.GetDataUnit()
            };

            return View(viewModel);
        }

        //----------------------- EDIT DATA ANEKA USAHA -----------------

        [HttpPost]
        public JsonResult EditData([FromBody] DataVariousBusiness data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    bool result = dal.Edit(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //------------------------- UBAH STATUS ------------------------

        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //------------------ UBAH STATUS PRICING -----------------

        public JsonResult UbahStatusPricing(string id)
        {
            dynamic message = null;

            try
            {
                MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                message = dal.UbahStatusPricing(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //------------------------- VIEW DATA PRICING ------------------------

        public JsonResult GetDataPricing(string id)
        {
            DataTablesVariousBusiness result = new DataTablesVariousBusiness();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    result = dal.GetDataPricingDetail(draw, start, length, search, id);

                }
                catch (Exception)
                {

                }
            }

            return Json(result);
        }

        //--------------- EDIT DATA PRICING ----------------

        [HttpPost]
        public JsonResult EditDataPricing([FromBody] DataVariousBusiness data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    MasterVariousBusinessDAL dal = new MasterVariousBusinessDAL();
                    bool result = dal.EditPricing(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
        
	}
}