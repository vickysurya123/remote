﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterInstallation;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class InstallationController : Controller
    {
        //
        // GET: /Installation/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("InstallationIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddInstallation()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            MasterInstallationDAL dal = new MasterInstallationDAL();
            var viewModel = new Installation
            {
                DDProfitCenter = dal.GetDataProfitCenter(KodeCabang),
                DDTariffCode = dal.GetDataTariff(),
                DDRentalObject = dal.GetDataRental(KodeCabang),
            };
            return View(viewModel);
        }

        public JsonResult GetDataInstallation()
        {
            DataTablesInstallation result = new DataTablesInstallation();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterInstallationDAL dal = new MasterInstallationDAL();
                    result = dal.GetDataInstallationnew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult AddData([FromBody] DataInstallation data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax)
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterInstallationDAL dal = new MasterInstallationDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            id_installation = result.INSTALLATION_NUMBER,
                            status = "S",
                            message = "Installation Data Has Been Created, Installation No : " + result.INSTALLATION_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public ActionResult EditInstallation(string id)
        {
            ViewBag.INSTALLATION_NUMBER = id;

            try
            {
                MasterInstallationDAL dal = new MasterInstallationDAL();
                AUP_INSTALLATION_Remote data = dal.GetDataForEdit(id);

                ViewBag.INSTALLATION_NUMBER = data.INSTALLATION_NUMBER;
                ViewBag.INSTALLATION_DATE = data.INSTALLATION_DATE;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.CUSTOMER_SAP_AR = data.CUSTOMER_SAP_AR;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;
                ViewBag.TARIFF_CODE = data.TARIFF_CODE;
                ViewBag.MINIMUM_AMOUNT = data.MINIMUM_AMOUNT;

                /*
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.INSTALLATION_DATE = data.INSTALLATION_DATE;
                ViewBag.POWER_CAPACITY = data.POWER_CAPACITY;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;
                ViewBag.TARIFF_CODE = data.TARIFF_CODE;
                ViewBag.TAX_CODE = data.TAX_CODE;
                ViewBag.MINIMUM_AMOUNT = data.MIN_AMOUNT;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.RO_CODE = data.RO_CODE;
                */
            }
            catch (Exception)
            {
               
            }

            return View();
        }

        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                MasterInstallationDAL dal = new MasterInstallationDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetCustomer()
        {
            if (IsAjaxUtil.validate(HttpContext))
            {
                var queryString = Request.Query;
                string sSearch = queryString["query"];
                CustomerDAL dal = new CustomerDAL();
                if (sSearch.Length > 2)
                {
                    var result = dal.GetAllLikeNamaPelanggan(sSearch);
                    return Json(result);
                }
            }
            return null;

        }

        //------------------ DROP DOWN CUSTOMER -------------------------
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //----------------- DROP DOWN TARIF -------------- 
        public JsonResult GetDataDropDownTarifL()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            MasterInstallationDAL dal = new MasterInstallationDAL();
            IEnumerable<DDTariffCode> result = dal.GetDataDropDownTarifL(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult GetDataDropDownTarifA([FromBody] string PROFIT_CENTER)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            MasterInstallationDAL dal = new MasterInstallationDAL();
            IEnumerable<DDTariffCode> result = dal.GetDataDropDownTarifA(KodeCabang, PROFIT_CENTER);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            MasterInstallationDAL dal = new MasterInstallationDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataDropDownProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult EditData([FromBody] DataInstallation data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterInstallationDAL dal = new MasterInstallationDAL();
                    bool result = dal.Edit(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}