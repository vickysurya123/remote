﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterInstallation;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Remote.Models;
using Remote.Models.InstallationB;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class InstallationBController : Controller
    {
        //
        // GET: /Installation/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("InstallationBIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
            
        public ActionResult AddInstallationB()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            InstallationBDAL dal = new InstallationBDAL();
            var viewModel = new Installation
            {
                DDProfitCenter = dal.GetDataProfitCenter(KodeCabang),
                DDTariffCode = dal.GetDataTariff(KodeCabang),
                DDRentalObject = dal.GetDataRental(KodeCabang),
            };
            return View(viewModel);
        }

        public ActionResult AddInstallationPricingB(string id)
        {
            try
            {
                InstallationBDAL dal = new InstallationBDAL();
                AUP_INSTALLATION_Remote data = dal.GetDataForEdit(id);

                //ViewBag.BE_ID = data.BE_ID;
                ViewBag.ID = data.ID;
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.CUSTOMER_NAME = data.CUSTOMER_NAME;
                ViewBag.CUSTOMER_SAP_AR = data.CUSTOMER_SAP_AR;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.INSTALLATION_DATE = data.INSTALLATION_DATE;
                ViewBag.POWER_CAPACITY = data.POWER_CAPACITY;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;
                ViewBag.MINIMUM_AMOUNT = data.MINIMUM_AMOUNT;
                ViewBag.BIAYA_BEBAN = data.BIAYA_BEBAN;
                ViewBag.MINIMUM_USED = data.MINIMUM_USED;
                ViewBag.MINIMUM_PAYMENT = data.MINIMUM_PAYMENT;
                ViewBag.MULTIPLY_FACT = data.MULTIPLY_FACT;
                ViewBag.TARIFF_CODE = data.TARIFF_CODE;
                ViewBag.RO_CODE = data.RO_CODE;
                ViewBag.BIAYA_ADMIN = data.BIAYA_ADMIN;
                ViewBag.INSTALLATION_CODE = data.INSTALLATION_CODE;
                ViewBag.SERIAL_NUMBER = data.SERIAL_NUMBER;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                ViewBag.BE_ID = data.BE_ID;

            }
            catch (Exception)
            {
            }

            return View("AddInstallationPricingB");
        }

        public JsonResult GetDataInstallationB()
        {
            DataTablesInstallation result = new DataTablesInstallation();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax) 
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    result = dal.GetDataInstallationnew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult AddData([FromBody] DataInstallation data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if (isAjax) 
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            id_installation = result.INSTALLATION_NUMBER,
                            status = "S",
                            message = "Installation Data Has Been Created, Installation No : " + result.INSTALLATION_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public ActionResult EditInstallation(string id)
        {
            ViewBag.INSTALLATION_NUMBER = id;

            try
            {
                InstallationBDAL dal = new InstallationBDAL();
                AUP_INSTALLATION_Remote data = dal.GetDataForEdit(id);

                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.CUSTOMER_ID = data.CUSTOMER_ID;
                ViewBag.INSTALLATION_DATE = data.INSTALLATION_DATE;
                ViewBag.POWER_CAPACITY = data.POWER_CAPACITY;
                ViewBag.INSTALLATION_ADDRESS = data.INSTALLATION_ADDRESS;
                ViewBag.TARIFF_CODE = data.TARIFF_CODE;
                ViewBag.TAX_CODE = data.TAX_CODE;
                ViewBag.MINIMUM_AMOUNT = data.MINIMUM_AMOUNT;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.RO_CODE = data.RO_CODE;

            }
            catch (Exception)
            {
                ViewBag.INSTALLATION_TYPE = string.Empty;
                ViewBag.BE_ID = string.Empty;
                ViewBag.CUSTOMER_ID = string.Empty;
                ViewBag.INSTALLATION_DATE = string.Empty;
                ViewBag.POWER_CAPACITY = string.Empty;
                ViewBag.INSTALLATION_ADDRESS = string.Empty;
                ViewBag.TARIFF_CODE = string.Empty;
                ViewBag.TAX_CODE = string.Empty;
                ViewBag.MINIMUM_AMOUNT = string.Empty;
                ViewBag.CURRENCY = string.Empty;
                ViewBag.RO_CODE = string.Empty;
            }

            return View();
        }

        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                InstallationBDAL dal = new InstallationBDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }
        public JsonResult GetCustomer()
        {
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax) 
            {
                var queryString = Request.Query;
                string sSearch = queryString["query"];
                CustomerDAL dal = new CustomerDAL();
                if (sSearch.Length > 2)
                {
                    var result = dal.GetAllLikeNamaPelanggan(sSearch);
                    return Json(result);
                }
            }
            return null;

        }

        //------------------ DROP DOWN CUSTOMER -------------------------
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //----------------- DROP DOWN TARIF -------------- 
        [HttpPost]
        public JsonResult GetDataDropDownTarifL([FromBody] string PROFIT_CENTER)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            InstallationBDAL dal = new InstallationBDAL();
            IEnumerable<DDTariffCode> result = dal.GetDataDropDownTarifL(KodeCabang, PROFIT_CENTER);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownTarifA()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            InstallationBDAL dal = new InstallationBDAL();
            IEnumerable<DDTariffCode> result = dal.GetDataDropDownTarifA(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            InstallationBDAL dal = new InstallationBDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataRental()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            InstallationBDAL dal = new InstallationBDAL();
            IEnumerable<DDRentalObject> result = dal.GetDataRental(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult AddPricing([FromBody] InstallationBPricing data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    bool result = dal.AddPricing(User.Identity.Name, data, KodeCabang);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataPricing(string id)
        {
            DataTablePricing result = new DataTablePricing();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    result = dal.GetDataPricing(draw, start, length, search, id, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-----------Edit Data Pricing 

        [HttpPost]
        public JsonResult editPricing([FromBody] InstallationBPricing data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    InstallationBDAL dal = new InstallationBDAL();
                    bool result = dal.editPricing(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult deletePricing(string id)
        {
            dynamic message = null;

            try
            {
                InstallationBDAL dal = new InstallationBDAL();
                message = dal.deletePricing(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult AddCosting([FromBody] InstallationBPricing data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax) 
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    bool result = dal.AddCosting(User.Identity.Name, data, KodeCabang);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataCosting(string id)
        {
            DataTablePricing result = new DataTablePricing();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    InstallationBDAL dal = new InstallationBDAL();
                    result = dal.GetDataCosting(draw, start, length, search, id, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-----------Edit Data Costing 
        [HttpPost]
        public JsonResult editCosting([FromBody] InstallationBPricing data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    InstallationBDAL dal = new InstallationBDAL();
                    bool result = dal.editCosting(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult deleteCosting(string id)
        {
            dynamic message = null;

            try
            {
                InstallationBDAL dal = new InstallationBDAL();
                message = dal.deleteCosting(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        //-----------Edit Header 
        [HttpPost]
        public JsonResult UpdateDataHeader([FromBody] DataInstallation data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax) 
            {
                try
                {
                    InstallationBDAL dal = new InstallationBDAL();
                    bool result = dal.UpdateDataHeader(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
    }
}