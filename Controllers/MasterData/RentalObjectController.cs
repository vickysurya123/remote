﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterRentalObject;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Remote.Models.RentalObject;
using Remote.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using RestSharp;
using Newtonsoft.Json;

namespace Remote.Controllers
{
    [Authorize]
    public class RentalObjectController : Controller
    {
        //
        // GET: /RO/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };
            
            ViewBag.KodeCabang = KodeCabang;
            ViewBag.Role = CurrentUser.UserRoleID;
            return View("ROIndex", viewModel);
        }

        public ActionResult Status()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            return View(viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Add()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            var viewModel = new RentalObject
            {
                DDBusinessEntitys = dal.GetDataBE2(KodeCabang),
                DDRoTypes = dal.GetDataRoType(),
                DDZoneRips = dal.GetDataZoneRip(),
                DDRoFunctions = dal.GetDataRoFunction(),
                DDUsageTypeRos = dal.GetDataUsageTypeRo(),
                DDProvinces = dal.GetDataProvince(),
                DDPeruntukans = dal.GetDataPeruntukan(),
                DDProfitCenters = dal.GetDataProfitCenter(KodeCabang),
                //DDRoLocations = dal.GetDataRoLocation(), jaga-jaga kalau nanti jadi dipakai
            };
            return View(viewModel);
        }

        //GRID BE_ID_T
        public List<DROPDOWN_LIST_BE_ID_T> cBE_ID_T(string BE_NAME)
        {
            // IDbConnection connection = DatabaseFactory.GetConnection();
            List<DROPDOWN_LIST_BE_ID_T> exec = null;
            using (var connection = new OracleConnection(DatabaseHelper.GetConnectionString()))
            {
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                try
                {
                    string str = "SELECT * FROM BUSINESS_ENTITY";
                    exec = connection.Query<DROPDOWN_LIST_BE_ID_T>(str, new
                    {
                        BE_NAME = BE_NAME.ToUpper()
                    }).ToList();

                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
           
            return exec;
        }

        public JsonResult GetDataRentalObject()
        {
            DataTablesMasterRentalObject result = new DataTablesMasterRentalObject();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    //string BusinessEntityList = queryString["BusinessEntityList"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataRentalObjectnew(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        

        [AllowAnonymous]
        [HttpGet]
        public JsonResult GetDataFixtureFitting2(string id)
        {
            DataTablesFixtureFitting2 result = new DataTablesFixtureFitting2();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataFixtureFitting2(draw, start, length, search, id);

                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [AllowAnonymous]
        public JsonResult GetDataMeasurement(string id)
        {
            
            DataTablesMeasurement result = new DataTablesMeasurement();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataMeasurement(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [AllowAnonymous]
        public JsonResult GetDataOccupancy(string id)
        {

            DataTablesOccupancy result = new DataTablesOccupancy();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataOccupancy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        // Ubah status data header
        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //-----------UBAH STATUS MEASUREMENT
        public JsonResult UbahStatusMeasurement(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.UbahStatusMeasurement(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //-----------UBAH STATUS FIXTURE FITTING
        public JsonResult UbahStatusFixtureFitting(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.UbahStatusFixtureFitting(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetDataDropdownBE()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DataDDBusinessEntity> result = dal.GetDataDDBO();
            return Json(new { data = result });
        }

        //-------------------- LIST DROP DOWN MEASUREMENT --------------------------------
        public JsonResult GetDataCBMeasurement()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DDMeasurement> result = dal.GetDataMeasurement();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropdownZONA_RIP()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DataDDZONA_RIP> result = dal.GetDataDDZONA_RIP();
            return Json(new { data = result });
        }

        
        public JsonResult GetDataDropdownRO_TYPE()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DataDDRO_TYPE> result = dal.GetDataRO_TYPE();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropdownUSAGE_TYPE()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DataDDUSAGE_TYPE> result = dal.GetDataUSAGE_TYPE();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropdownPROVINCE()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DataDDProvince> result = dal.GetDataPROVINCE();
            return Json(new { data = result });
        }

        public JsonResult GetDataMaps(string id)
        {

            DataTablesMaps result = new DataTablesMaps();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataMaps(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult AddData([FromBody]DataRentalObject data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            ro_number = result.RO_NUMBER,
                            ro_id = result.RO_ID, 
                            status = "S",
                            message = "Rental Object Has Been Created, RO Number : " + result.RO_NUMBER
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Someting went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        // Controller ambil data DropDown Business Entity
        public ActionResult BusinessEntityList(string id)
        {

            if (id != null)
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                var be = dal.GetDataBusinessEntity();

                //if (HttpContext.Request.IsAjaxRequest())
                //    return Json(new SelectList(be, "ID", "BE_ID", "BE_NAME"));

                return View(be);
            }
            return View();
        }

        [HttpPost]
        public JsonResult AddDataDetailFixtureFitting([FromBody]DataFixtureFitting data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.AddDataFixtureFitting(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- ADD DATA MEASUREMENT ---------------------
        [HttpPost]
        public JsonResult AddDataDetailMeasurement([FromBody]DataMeasurement data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.AddDataMeasurement(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //----------------------- ADD DATA OCCUPANCY ------------------------
        [HttpPost]
        public JsonResult AddDataOccupancy([FromBody]DataOccupancy data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.AddDataOccupancy(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult EditData([FromBody]DataRentalObject data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.EditNew(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-----------Edit Data Occupancy
        [HttpPost]
        public JsonResult EditDataOccupancy([FromBody]DataOccupancy data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.EditOccupancy(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }
        
        //-----------Edit Data Measurement 
        [HttpPost]
        public JsonResult EditDataMeasurement([FromBody]DataMeasurement data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.EditMeasurement(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-----------Edit Data Fixture  
        [HttpPost]
        public JsonResult EditDataFixtureFitting([FromBody]DataFixtureFitting data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    bool result = dal.EditFixtureFitting(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult DeleteData(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.DeleteData(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult DeleteDataFixtureFitting(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.DeleteDataFixtureFitting(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult DeleteDataMeasurement(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.DeleteDataMeasurement(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public ActionResult Edit(string id)
        {
            //ViewBag.RO_NUMBER = id;

            string KodeCabang = CurrentUser.KodeCabang;
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            AUP_RENTALOBJECT_Remote data = dal.GetDataForEdit(int.Parse(id));

            try
            {

                ViewBag.RO_CODE = data.RO_CODE;
                ViewBag.RO_NUMBER = data.RO_NUMBER;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.RO_NAME = data.RO_NAME;
                ViewBag.RO_CERTIFICATE_NUMBER = data.RO_CERTIFICATE_NUMBER;
                ViewBag.RO_ADDRESS = data.RO_ADDRESS;
                ViewBag.RO_POSTALCODE = data.RO_POSTALCODE;
                ViewBag.RO_CITY = data.RO_CITY;
                ViewBag.MEMO = data.MEMO;
                ViewBag.VAL_FROM = data.VAL_FROM;
                ViewBag.VAL_TO = data.VAL_TO;
                ViewBag.ZONE_RIP_ID = data.ZONE_RIP_ID;
                ViewBag.RO_TYPE_ID = data.RO_TYPE_ID;
                //ViewBag.LOCATION_ID = data.LOCATION_ID;
                ViewBag.FUNCTION_ID = data.FUNCTION_ID;
                ViewBag.USAGE_TYPE_ID = data.USAGE_TYPE_ID;
                ViewBag.PROVINCE_ID = data.RO_PROVINCE;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                ViewBag.KODE_ASET = data.KODE_ASET;
                ViewBag.LINI = data.LINI;
                ViewBag.PERUNTUKAN = data.PERUNTUKAN;
                ViewBag.SERTIFIKAT = data.SERTIFIKAT_OR_NOT;
                ViewBag.KETERANGAN = data.KETERANGAN;

            }
            catch (Exception e)
            {
                //ViewBag.RO_NUMBER = string.Empty;
                //ViewBag.BE_ID = string.Empty;
                //ViewBag.RO_NAME = string.Empty;
                //ViewBag.RO_CERTIFICATE_NUMBER = string.Empty;
                //ViewBag.RO_ADDRESS = string.Empty;
                //ViewBag.RO_POSTALCODE = string.Empty;
                //ViewBag.RO_CITY = string.Empty;
                //ViewBag.MEMO = string.Empty;
                string aaa = e.ToString();
            }

            var viewModel = new RentalObject
            {
                DDBusinessEntitys = dal.GetDataBE2(KodeCabang),
                DDRoTypes = dal.GetDataRoType(),
                DDZoneRips = dal.GetDataZoneRip(),
                DDUsageTypeRos = dal.GetDataUsageTypeRo(),
                DDProvinces = dal.GetDataProvince(),
                DDProfitCenters = dal.GetDataProfitCenter(KodeCabang),
                DDRoLocations = dal.GetDataRoLocation(),
                DDMeasurementTypes = dal.GetDataMeasurementType(),
                DDVacancyReason = dal.GetDataVacancyReason(),
                DDFixFits = dal.GetDataFF(),
                DDRoFunctions = dal.GetDataRoFunction(),
                DDPeruntukans = dal.GetDataPeruntukan(),
            };
            return View(viewModel);

           // return View();
        }

        public ActionResult EditRO(string id)
        {
            ViewBag.RO_NUMBER = id;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                AUP_RENTALOBJECT_Remote data = dal.GetDataForEdit(int.Parse(id));

                ViewBag.RO_NUMBER = data.RO_NUMBER;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.RO_NAME = data.RO_NAME;
                ViewBag.RO_CERTIFICATE_NUMBER = data.RO_CERTIFICATE_NUMBER;
                ViewBag.RO_ADDRESS = data.RO_ADDRESS;
                ViewBag.RO_POSTALCODE = data.RO_POSTALCODE;
                ViewBag.RO_CITY = data.RO_CITY;
                ViewBag.RO_PROVINCE = data.RO_PROVINCE;
                ViewBag.MEMO = data.MEMO;
            }
            catch (Exception)
            {
                ViewBag.RO_NUMBER = string.Empty;
                ViewBag.BE_ID = string.Empty;
                ViewBag.RO_NAME = string.Empty;
                ViewBag.RO_CERTIFICATE_NUMBER = string.Empty;
                ViewBag.RO_ADDRESS = string.Empty;
                ViewBag.RO_POSTALCODE = string.Empty;
                ViewBag.RO_CITY = string.Empty;
                ViewBag.RO_PROVINCE = string.Empty;
                ViewBag.MEMO = string.Empty;
            }

            return View();
        }


        public ActionResult EditDetailRO(string id)
        {
            /*
             * created by virul
             * creation date 10 10 2016
             * description: tuujuan..
             * */
            ViewBag.RO_NUMBER = id;
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            try
            {
                AUP_RENTALOBJECT_Remote data = dal.GetDataForEdit(int.Parse(id));

                ViewBag.RO_NUMBER = data.RO_NUMBER;
            }
            catch (Exception)
            {
                ViewBag.RO_NUMBER = string.Empty;
            }

            var viewModel = new RentalObject
            {
                DDMeasurementTypes = dal.GetDataMeasurementType(),
                DDFixFits = dal.GetDataFF(),
            };
            return View(viewModel);
        }

        [HttpGet]
        public JsonResult GetRO_NUMBER()
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<getGenerateRONumber> result = dal.GenerateRoNumber();
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult getProfitCenter([FromBody]string be_id)
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<FDDProfitCenter> result = dal.GetProfitCenter(be_id);
            return Json(new { data = result });
        }

        public ActionResult PrintQr(string id = "", string kodeCabang = "", string kodeProfit = "")
        {
            if(id != "")
            {
                string longurl = "http://anjungan.pelindo.co.id/enotav2/cetakPdf?";
                var uriBuilder = new UriBuilder(longurl);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["me"] = id;
                uriBuilder.Query = query.ToString();
                string longurls = uriBuilder.ToString();  //END BUILD URL QR CODE            
                ViewBag.QrUrl = longurls;
                ViewBag.RO_NUMBER = id;
            }
            return View("PrintQr", ViewBag);
        }

        public ActionResult viewQr(string code)
        {
            
            return View();
        }
        public JsonResult GetDataRentalObjectfilter(int be, int pc)
        {
            DataTablesMasterRentalObject result = new DataTablesMasterRentalObject();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string occupancy = queryString["occupancy"];

                    //string BusinessEntityList = queryString["BusinessEntityList"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataRentalObjectfilternew(draw, start, length, search, KodeCabang, be, pc, occupancy);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public JsonResult PinPoint(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.PinPoint(RO_NUMBER);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult PinPoint2(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.PinPoint2(RO_NUMBER);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetMarker(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.GetMarker(RO_NUMBER);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetMarker2(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.GetMarker2(RO_NUMBER);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetROStatus(string RO_CODE)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetROStatus(RO_CODE);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ContentResult UploadFiles([FromForm] RentalObjectAttachment data)//, [FromForm] List<Microsoft.AspNetCore.Http.IFormFile> files)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.RO_NUMBER;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "RentalObject", subPath);

                bool exists = Directory.Exists(filePath);

                if (!exists)
                    Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/RentalObject/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                dynamic getData = null;
                GetUrl test = new GetUrl();
                string url = test.UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/RentalObject");
                request.AddFile("file", savedFileName);
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;


                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                bool result = dal.AddFiles(sFile, file2, KodeCabang, data.RO_NUMBER);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "RentalObject", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult GetDataAttachment(string id)
        {
            DataTablesRoAttachment result = new DataTablesRoAttachment();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //----------------------- ADD DATA MAPS ------------------------
        [HttpPost]
        public JsonResult AddMaps([FromBody] DataMaps data)
        {
            dynamic message = null;
            dynamic dataUser = CurrentUser;
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                    var getid = (data.ID == "0" ? "" : data.ID);
                    result = (getid == "" ? dal.AddDataMaps(data, dataUser) : dal.EditMaps(data, dataUser));

                    //bool result = dal.AddDataMaps(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data (Data Double)"
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult DeleteMaps(string id)
        {
            dynamic message = null;

            try
            {
                MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
                message = dal.DeleteMaps(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        [HttpGet]
        public JsonResult GetDataAsset(string q)
        {

            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<DDAsset> result = dal.GetDataAsset(q);

            return Json(new
            {
                results = result
            });
        }
    }
}