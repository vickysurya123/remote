﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;

namespace Remote.Controllers
{
    [Authorize]
    public class Currency : Controller
    {
        // GET: Parameter   
        public ActionResult Index()
        {
            return View("../MasterData/Currency/Currency");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasgterCurrency dal = new MasgterCurrency();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [HttpPost]

        public JsonResult Insert([FromBody] CURRENCY data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    string id = queryString["id"];

                    MasgterCurrency dal = new MasgterCurrency();

                    result = (data.ID != null ? dal.EditDataHeader(data, dataUser) : dal.AddDataHeader(data, dataUser));

                    //result = dal.AddDataHeader(data, id);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult Delete([FromBody] CURRENCY data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    MasgterCurrency dal = new MasgterCurrency();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetDataBusinessEntity(string q)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            MasgterDataLahanDal dal = new MasgterDataLahanDal();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang, q);

            return Json(new
            {
                results = result
            });
        }
    }
}