﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models.MasterWaterAndElectricity;
using Remote.Entities;
using Remote.DAL;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class WaterAndElectricityController : Controller
    {
        //
        // GET: /WaterAndElectricity/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
           string KodeCabang = CurrentUser.KodeCabang;
           ViewBag.KodeCabang = KodeCabang;
           return View("WaterAndELectricityIndex");
        }

        public ActionResult AddWE()
        {
            return View();
        }

        public ActionResult Edit(string id)
        {
            try
            {
                MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                AUP_WE_PRICING data = dal.GetDataForEdit(id);
                ViewBag.INSTALLATION_TYPE = data.INSTALLATION_TYPE;
                ViewBag.PROFIT_CENTER = data.PROFIT_CENTER;
                ViewBag.DESCRIPTION = data.DESCRIPTION;
                ViewBag.AMOUNT = data.AMOUNT;
                ViewBag.UNIT = data.UNIT;
                ViewBag.CURRENCY = data.CURRENCY;
                ViewBag.X_FACTOR = data.X_FACTOR;
                ViewBag.VALID_FROM = data.FALID_FROM;
                ViewBag.VALID_TO = data.FALID_TO;
                ViewBag.TARIFF_CODE = data.TARIFF_CODE;
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }
            return View("EditWE");

            // return View();
        }

        public JsonResult GetDataWaterAndElectricity()
        {
            DataTablesMasterWaterAndElectricity result = new DataTablesMasterWaterAndElectricity();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                    result = dal.GetDataWaterAndElectricity(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult EditData([FromBody] DataWaterAndElectricity data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                    bool result = dal.EditData(User.Identity.Name, data, KodeCabang);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult GetDataDropdownProfitCenter()
        {
            //Json Get Data WE
            string KodeCabang = CurrentUser.KodeCabang;
            MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
            IEnumerable<DataDDProfitCenter> result = dal.GetDataDDProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult AddData([FromBody] DataWaterAndElectricity data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                    var result = dal.Add(User.Identity.Name, data, KodeCabang);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            be_number = result.TARIFF_CODE,
                            status = "S",
                            message = "Tariff Code Has Been Created, Tariff Code : " + result.TARIFF_CODE
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult DeleteData(string id)
        {
            dynamic message = null;

            try
            {
                MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                message = dal.DeleteData(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                MasterWaterAndElectricityDAL dal = new MasterWaterAndElectricityDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }
	}
}