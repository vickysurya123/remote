﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.ViewModels;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using RestSharp;

namespace Remote.Controllers
{
    [Authorize]
    public class SertifikatHpl : Controller
    {
        // GET: Parameter   
        public ActionResult Index()
        {

            SertifikatHplDAL dal = new SertifikatHplDAL();
            var viewModel = new UserViewModels
            {
                DDBusinessEntity = dal.GetDataListBusinessEntity(CurrentUser.KodeCabang),
            };

            return View("../MasterData/SertifikatHpl/SertifikatHpl", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    SertifikatHplDAL dal = new SertifikatHplDAL();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetTypeSertifikat(string SERTIFIKAT_TYPE)
        {
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    string SERTIFIKATTYPE = queryString["SERTIFIKAT_TYPE"];
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    SertifikatHplDAL dal = new SertifikatHplDAL();
                    result = dal.GetDataType( SERTIFIKATTYPE ,draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetSertifikatBeid(string BUSINESS_ENTITY_ID)
        {
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    SertifikatHplDAL dal = new SertifikatHplDAL();
                    result = dal.GetSertifikatBeid(BUSINESS_ENTITY_ID, draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }



        [HttpPost]
        public async Task<IActionResult> insertData(SERTIFIKAT_HPL data, IEnumerable<IFormFile> files)
        //public ContentResult InsertData([FromForm] SERTIFIKAT_HPL data)
        {
            dynamic dataUser = CurrentUser;
            dynamic result = null;
            try
            {
                var queryString = Request.Query;
                string id = queryString["id"];

                SertifikatHplDAL dal = new SertifikatHplDAL();

                var r = new List<UploadFilesResult>();

                if(files.Count() == 0 && data.ID != null)
                {
                    result = dal.EditDataHeader(data, dataUser, null, null);
                }
                else
                {
                    foreach (var file in files)
                    {
                        var split = data.NO_SERTIFIKAT.Replace('/', '-');
                        string subPath = split;
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        string filePath = Path.Combine(rootPath, "REMOTE", "SertifikatHpl", subPath);

                        bool exists = System.IO.Directory.Exists(filePath);

                        if (!exists)
                            System.IO.Directory.CreateDirectory(filePath);

                        var sFile = "";
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileExt = Path.GetExtension(file.FileName);

                        sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                        string savedFileName = Path.Combine(filePath, sFile);

                        using (var stream = new FileStream(savedFileName, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        string directory = "~/REMOTE/SertifikatHpl/" + subPath;
                        string KodeCabang = CurrentUser.KodeCabang;
                        //upload cloud p3
                        dynamic getData = null;
                        GetUrl test = new GetUrl();
                        string url = test.UploadCloadUrl + "upload";
                        RestClient client = new RestClient(url);
                        RestRequest request = new RestRequest(Method.POST)
                        {
                            RequestFormat = DataFormat.None,
                            AlwaysMultipartFormData = true
                        };
                        request.AddHeader("content-type", "multipart/form-data");
                        request.AddHeader("Accept", "application/json");
                        request.AddParameter("folder", "TMP/Information/" + subPath);
                        request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                        request.AddFile("file", savedFileName);
                        IRestResponse responseOrg = client.Execute<dynamic>(request);
                        var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                        string file2 = list.message.url;
                        //end

                        result = (data.ID != null ? dal.EditDataHeader(data, dataUser, file2, sFile) : dal.AddDataHeader(data, dataUser, file2, sFile));
                        
                        //DeleteFile(data.ATTACHMENT, data.PATCH);
                        
                        //bool uploadFile = dal.AddFiles(sFile, directory, KodeCabang, data.NO_SERTIFIKAT);

                        r.Add(new UploadFilesResult()
                        {
                            Name = sFile,
                            Length = Convert.ToInt32(file.Length),
                            Type = file.ContentType,
                        });
                    }
                }
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult Delete([FromBody] SERTIFIKAT_HPL data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    SertifikatHplDAL dal = new SertifikatHplDAL();
                    result = dal.DeleteDataHeader(data);
                    DeleteFile(data.ATTACHMENT, data.PATCH);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            
            return Json(result);
        }

        private string DeleteFile(string filename, string uri)
        {
            dynamic message = null;
            var sertifikat = uri.Split('/').Last();
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "SertifikatHpl", sertifikat, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
            }
            catch (Exception)
            {

            }

            return Json(message);
        }


        [HttpGet]
        public JsonResult GetDataBusinessEntity(string q)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            SertifikatHplDAL dal = new SertifikatHplDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang, q);
            return Json(new
            {
                results = result
            });
        }
    }
}