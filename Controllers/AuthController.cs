﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Dapper;
using Remote.Helpers;
using Remote.Entities;
using Remote.DAL;
using Microsoft.AspNetCore.Http;

namespace Remote.Controllers
{
    public class AuthController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        public AuthController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        [Route("[controller]/Login")]
        public IActionResult LoginView(LoginViewModel loginViewModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View("./Login", loginViewModel);
        }

        [HttpPost]
        [Route("[controller]/Login")]
        public IActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            APP_USER userInformation = new APP_USER();
            userInformation.ID = 999999999;//empty
            userInformation.USER_LOGIN = "-";
            ViewBag.MessageErrorUser = string.Empty;

            //string generateID = CurrentUser.generateID;
            string generateID = "test";

            try
            {
                AuthDAL dal = new AuthDAL();

                userInformation = dal.GetUserInformation(loginViewModel.username, loginViewModel.password, generateID);

                if (userInformation != null)
                {
                    if (userInformation.ID != 9999999)
                    {
                        List<VW_MENU_ACTIVITIES> userActivities = dal.GetUserActivities(loginViewModel.username);
                        if (userActivities.Count > 0)
                        {
                            CreateIdentity(userInformation, userActivities);

                            return Redirect(GetRedirectUrl(returnUrl));
                        }
                    }
                    else
                    {
                        ViewBag.MessageErrorUser = userInformation.USER_LOGIN + " / " + userInformation.USER_NAMA;
                    }
                }
                else
                {
                    return View("./Login", loginViewModel);
                }
            }

            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            return View();
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Home");
            }

            return returnUrl;
        }

        public async void CreateIdentity(dynamic userInformation, dynamic userActivities)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userInformation.USER_LOGIN + " | " + userInformation.USER_NAMA),
                    new Claim("USER_ID", userInformation.ID.ToString()),
                    new Claim("USER_LOGIN", userInformation.USER_LOGIN),
                    new Claim("KD_CABANG", userInformation.KD_CABANG),
                    new Claim("NAMA_CABANG", userInformation.NAMA_CABANG),
                    new Claim("USER_ROLE_ID", userInformation.USER_ROLE_ID),
                    new Claim("ROLE_NAME", userInformation.USER_NAMA + " " + userInformation.ROLE_NAME),
                    new Claim("PROPERTY_ROLE", String.IsNullOrEmpty(userInformation.PROPERTY_ROLE)?"":userInformation.PROPERTY_ROLE),
                    new Claim("PARAM1", String.IsNullOrEmpty(userInformation.PARAM1)?"":userInformation.PARAM1),
                    new Claim("PARAM2", String.IsNullOrEmpty(userInformation.PARAM2)?"":userInformation.PARAM2),
                    new Claim("PARAM3", String.IsNullOrEmpty(userInformation.PARAM3)?"":userInformation.PARAM3),
                    new Claim("USER_EMAIL", String.IsNullOrEmpty(userInformation.USER_EMAIL)?"":userInformation.USER_EMAIL),
                    new Claim("USER_PHONE", String.IsNullOrEmpty(userInformation.USER_PHONE)?"":userInformation.USER_PHONE),
                    new Claim("KD_TERMINAL", userInformation.KD_TERMINAL),
                    new Claim("PROFIT_CENTER", String.IsNullOrEmpty(userInformation.PROFIT_CENTER)?"":userInformation.PROFIT_CENTER)
                };

            //if (userActivities.Count > 0)
            //{
            //    foreach (var item in userActivities)
            //    {
            //        if (item.PERMISSION_ADD.Equals("1"))
            //            claims.Add(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Add"));
            //        if (item.PERMISSION_DEL.Equals("1"))
            //            claims.Add(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Delete"));
            //        if (item.PERMISSION_UPD.Equals("1"))
            //            claims.Add(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Edit"));
            //        if (item.PERMISSION_VIW.Equals("1"))
            //            claims.Add(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".View"));
            //    }
            //}

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var userPrincipal = new ClaimsPrincipal(claimsIdentity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, userPrincipal, new AuthenticationProperties
            {
                IsPersistent = true
            });
        }

        public async Task<IActionResult> Logout(LoginViewModel loginViewModel)
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("LoginView");
        }

    }
}
