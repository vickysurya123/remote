﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
//using System.Web.Script.Serialization;
//using System.Web.Services;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.MasterRentalObject;
using Remote.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportROController : Controller
    {
        //
        // GET: /ReportRO/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;

            return View("ReportROIndex", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataRO()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataRO(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenter(string BE_ID)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult GetProfitCenternew([FromBody] string be_id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<FDDProfitCenter> result = dal.GetDataProfitCenternew(be_id);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataRONumb()
        {
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDRo> result = dal.GetDataRONumb();
            return Json(new { data = result });
        }

        public JsonResult GetDataZoneRip()
        {
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDZoneRip> result = dal.GetDataZoneRip();
            return Json(new { data = result });
        }

        public JsonResult GetDataUsageType()
        {
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDUsageTypeRo> result = dal.GetDataUsageType();
            return Json(new { data = result });
        }

        public JsonResult GetDataFunction()
        {
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDRoFunction> result = dal.GetDataFunction();
            return Json(new { data = result });
        }

        public JsonResult GetDataVacancyReason()
        {
            ReportRODAL dal = new ReportRODAL();
            IEnumerable<DDRoFunction> result = dal.GetDataVacancyReason();
            return Json(new { data = result });
        }
        
        public JsonResult GetDataFilter()
        {
            DataTableReportRO result = new DataTableReportRO();


            if (IsAjaxUtil.validate(HttpContext))
            { 
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string ro_number = queryString["ro_number"];
                    string zone_rip = queryString["zone_rip"];
                    string usage_type = queryString["usage_type"];
                    string function = queryString["function"];
                    string vacancy_status = queryString["vacancy_status"];
                    string vacancy_reason = queryString["vacancy_reason"];
                    string status = queryString["status"];
                    string available_date = queryString["available_date"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportRODAL dal = new ReportRODAL();
                    DataTableReportRO data = dal.GetDataFilternew(draw, start, length, be_id, profit_center, ro_number, zone_rip, usage_type, function, vacancy_status, vacancy_reason, status, available_date, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] RentalObjectExcel data)
        {
            nama result = new nama();
            string fileName = "ReportRO(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportRODAL dal = new ReportRODAL();
                var dalShowdata = dal.ExportExcelnew(data.BE_ID, data.PROFIT_CENTER, data.RO_NUMBER, data.ZONE_RIP, data.USAGE_TYPE, data.FUNCTION, data.VACANCY_STATUS, data.VACANCY_REASON, data.STATUS, data.AVAILABLE_DATE, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[23] { 
                new DataColumn("BUSINESS ENTITY", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("RO NUMBER", typeof(string)),
                new DataColumn("RO NAME",typeof(string)),
                new DataColumn("ZONE ZIP", typeof(string)),
                new DataColumn("LAND DIMENSION",typeof(string)),
                new DataColumn("BUILDING DIMENSION", typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("CONTRACT OFFER", typeof(string)),
                new DataColumn("CUSTOMER", typeof(string)),
                new DataColumn("BADAN USAHA", typeof(string)),
                new DataColumn("CERTIFICATE NO",typeof(string)),
                new DataColumn("USAGE TYPE", typeof(string)),
                new DataColumn("FUNCTION",typeof(string)),
                new DataColumn("VACANCY STATUS", typeof(string)),
                new DataColumn("VACANCY REASON",typeof(string)),
                new DataColumn("VALID FROM",typeof(string)),
                new DataColumn("VALID TO",typeof(string)),
                new DataColumn("ADDRESS",typeof(string)),
                new DataColumn("POSTAL CODE", typeof(string)),
                new DataColumn("CITY",typeof(string)),
                new DataColumn("PROVINCE",typeof(string)),
                new DataColumn("STATUS",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                    if (valData.ACTIVE == "1")
                    {
                        dt.Rows.Add(valData.BE_ID, valData.PROFIT_CENTER, valData.RO_CODE, valData.RO_NAME,
                                    valData.ZONE_RIP, valData.LAND_DIMENSION, valData.BUILDING_DIMENSION,
                                    valData.CONTRACT_NO, valData.CONTRACT_OFFER_NO, valData.MPLG_KODE + " - "+ valData.MPLG_NAMA, valData.MPLG_BADAN_USAHA,
                                    valData.RO_CERTIFICATE_NUMBER, valData.USAGE_TYPE, valData.FUNCTION_ID,
                                    valData.STATUS, valData.REASON, valData.VALID_FROM, valData.VALID_TO, 
                                    valData.RO_ADDRESS, valData.RO_POSTALCODE, valData.RO_CITY, valData.PROVINCE, "ACTIVE");
                    }
                    else
                    {
                        dt.Rows.Add(valData.BE_ID, valData.PROFIT_CENTER, valData.RO_CODE, valData.RO_NAME,
                                    valData.ZONE_RIP, valData.LAND_DIMENSION, valData.BUILDING_DIMENSION,
                                    valData.CONTRACT_NO, valData.CONTRACT_OFFER_NO, valData.MPLG_KODE + " - " + valData.MPLG_NAMA, valData.MPLG_BADAN_USAHA,
                                    valData.RO_CERTIFICATE_NUMBER, valData.USAGE_TYPE, valData.FUNCTION_ID,
                                    valData.STATUS, valData.REASON, valData.VALID_FROM, valData.VALID_TO, 
                                    valData.RO_ADDRESS, valData.RO_POSTALCODE, valData.RO_CITY, valData.PROVINCE, "INACTIVE");
                    }
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportRO");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));

                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "ReportRO");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}