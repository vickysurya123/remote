﻿using System;
using System.Collections.Generic;
using Remote.Models;
using Remote.Models.ReportGeneralContract;
using System.Data;
using System.Security.Claims;
using Remote.Entities;
using Remote.DAL;
using System.IO;
using ClosedXML.Excel;
//using System.Web.Services;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportGeneralContractController : Controller
    {
        //
        // GET: /ReportGeneralContract/
        public ActionResult Index()
        {
            return View("ReportGeneralContractIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataContractType()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataContractTypeTwo()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
            IEnumerable<DDContractType> result = dal.GetDataContractTypeTwo(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult ShowFilter()
        {
            DataTablesReportGeneralContract result = new DataTablesReportGeneralContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string customer_id = queryString["customer_id"];
                    string contract_type = queryString["contract_type"];
                    string contract_no = queryString["contract_no"];
                    string contract_offer_no = queryString["contract_offer_no"];
                    string rental_request_no = queryString["rental_request_no"];
                    string contract_status = queryString["contract_status"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
                    DataTablesReportGeneralContract data = dal.ShowFilter(draw, start, length, be_id, profit_center, customer_id, contract_type, contract_no, contract_offer_no, rental_request_no, contract_status, valid_from, valid_to, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //------------------- new filter ----------------------------
        public JsonResult ShowFilterNew()
        {
            DataTablesReportGeneralContract result = new DataTablesReportGeneralContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string customer_id = queryString["customer_id"];
                    string contract_type = queryString["contract_type"];
                    string contract_no = queryString["contract_no"];
                    string contract_offer_no = queryString["contract_offer_no"];
                    string rental_request_no = queryString["rental_request_no"];
                    string contract_status = queryString["contract_status"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
                    DataTablesReportGeneralContract data = dal.ShowFilternew(draw, start, length, be_id, profit_center, customer_id, contract_type, contract_no, contract_offer_no, rental_request_no, contract_status, valid_from, valid_to, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }


        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        [HttpPost]
        public JsonResult defineExcel([FromBody] ExcelReportGeneralContract data)
        {
            nama result = new nama();
            string fileName = "ReportGeneralContract(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportGeneralContractDAL dal = new ReportGeneralContractDAL();
                var dalShowdata = dal.GenerateExcelnew(data.BUSINESS_ENTITY, data.PROFIT_CENTER, data.CUSTOMER_ID, data.CONTRACT_TYPE, data.CONTRACT_NO, data.CONTRACT_OFFER_NO, data.RENTAL_REQUEST_NO, data.CONTRACT_STATUS, data.VALID_FROM, data.VALID_TO, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[24] { 
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("CONTRACT TYPE", typeof(string)),
                new DataColumn("RENTAL REQUEST NO",typeof(string)),
                new DataColumn("CREATE CONTRACT BY",typeof(string)),
                new DataColumn("CONTRACT OFFER NO", typeof(string)),
                new DataColumn("CONTRACT NAME",typeof(string)),
                new DataColumn("START DATE", typeof(string)),
                new DataColumn("END DATE",typeof(string)),
                new DataColumn("CUSTOMER NAME", typeof(string)),
                new DataColumn("CUSTOMER AR",typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CONTRACT USAGE",typeof(string)),
                new DataColumn("CONTRACT CURRENCY", typeof(string)),
                new DataColumn("IJIN PRINSIP NO", typeof(string)),
                new DataColumn("LEGAL CONTRACT NO",typeof(string)),
                new DataColumn("OBJECT ID", typeof(string)),
                new DataColumn("NAME OF RO",typeof(string)),
                new DataColumn("INDUSTRY TYPE", typeof(string)),
                new DataColumn("LUAS TANAH (m2)",typeof(string)),
                new DataColumn("LUAS BANGUNAN (m2)", typeof(string)),
                new DataColumn("NOMINAL VALUE", typeof(string)),
                new DataColumn("CONDITION TYPE",typeof(string)),
                new DataColumn("CONTRACT STATUS", typeof(string)),
                new DataColumn("STATUS MIGRASI",typeof(string))});

                string temp = "";
                foreach (var valData in dalShowdata)
                {
                    if (temp != valData.CONTRACT_NO)
                        dt.Rows.Add(valData.CONTRACT_NO, valData.CONTRACT_TYPE, valData.RENTAL_REQUEST_NO, valData.CREATION_BY, valData.CONTRACT_OFFER_NO,
                                    valData.CONTRACT_NAME, valData.CONTRACT_START_DATE,
                                    valData.CONTRACT_END_DATE, valData.BUSINESS_PARTNER_NAME, valData.CUSTOMER_AR,
                                    valData.PROFIT_CENTER, valData.CONTRACT_USAGE, valData.CURRENCY, valData.IJIN_PRINSIP_NO,
                                    valData.LEGAL_CONTRACT_NO, valData.OBJECT_ID, valData.OBJECT_NAME, valData.INDUSTRY,
                                    valData.LUAS_TANAH, valData.LUAS_BANGUNAN, valData.TOTAL_NET_VALUE, valData.CONDITION_TYPE, valData.CONTRACT_STATUS, valData.STATUS_MIGRASI);
                    else
                        dt.Rows.Add(valData.CONTRACT_NO, valData.CONTRACT_TYPE, valData.RENTAL_REQUEST_NO, valData.CREATION_BY, valData.CONTRACT_OFFER_NO,
                                    valData.CONTRACT_NAME, valData.CONTRACT_START_DATE,
                                    valData.CONTRACT_END_DATE, valData.BUSINESS_PARTNER_NAME, valData.CUSTOMER_AR,
                                    valData.PROFIT_CENTER, valData.CONTRACT_USAGE, valData.CURRENCY, valData.IJIN_PRINSIP_NO,
                                    valData.LEGAL_CONTRACT_NO, valData.OBJECT_ID, valData.OBJECT_NAME, valData.INDUSTRY,
                                    "0", "0", valData.TOTAL_NET_VALUE, valData.CONDITION_TYPE, valData.CONTRACT_STATUS, valData.STATUS_MIGRASI);
                    temp = valData.CONTRACT_NO;
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportGeneralContract");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "ReportGeneralContract");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}