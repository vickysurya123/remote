﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
//using System.Web.Script.Serialization;
//using System.Web.Services;
using Remote.Entities;
using Remote.Models;
using Microsoft.AspNetCore.Mvc;
using Remote.Models.ReportMasterInstallation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportMasterInstallationController : Controller
    {
        // 
        // GET: /ReportMasterInstalasi/
        public ActionResult Index()
        {
            return View();
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult getInstallationType(string p)
        {
            IEnumerable<VW_INSTALLATION_TYPE> result = null;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            result = dal.getInstallationType(p);
            return Json(new { items = result });
        }
        public JsonResult getInstallationCode(string p)
        {
            IEnumerable<VW_INSTALLATION_TYPE> result = null;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            result = dal.getInstallationCode(p);
            return Json(new { items = result });
        }
        public JsonResult getCostumerMDM(string p)
        {
            IEnumerable<VW_INSTALLATION_TYPE> result = null;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            result = dal.getCostumerMDM(p);
            return Json(new { items = result });
        }
        public JsonResult getCostumerSAP(string p)
        {
            IEnumerable<VW_INSTALLATION_TYPE> result = null;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            result = dal.getCostumerSAP(p);
            return Json(new { items = result });
        }
        public JsonResult ShowAll()
        {
            DataTableReportMasterInstallation result = new DataTableReportMasterInstallation();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
                    result = dal.ShowAll(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public JsonResult GetDataFilter()
        {
            DataTableReportMasterInstallation result = new DataTableReportMasterInstallation();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string profit_center = queryString["profit_center"];
                    string installation_code = queryString["installation_code"];
                    string customer_mdm = queryString["customer_mdm"];
                    string customer_sap = queryString["customer_sap"];
                    string status = queryString["status"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
                    DataTableReportMasterInstallation data = dal.GetDataFilternew(draw, start, length, profit_center, installation_code, customer_mdm, customer_sap, status, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_MASTER_INSTALLATION> listData = null;
            var queryString = Request.Query;

            string profit_center = queryString["profit_center"];
            string installation_code = queryString["installation_code"];
            string customer_mdm = queryString["customer_mdm"];
            string customer_sap = queryString["customer_sap"];
            string status = queryString["status"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            listData = dal.ShowFilterTwo(profit_center, installation_code, customer_mdm, customer_sap, status, KodeCabang);
            return Json(new { data = listData });
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenternew(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
            var xcustomer = dal.DDCustomernew(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }


        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportMasterInstallationExcel data)
        {
            nama result = new nama();
            string fileName = "Report Master Installation Electricity(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";
            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportMasterInstallationDAL dal = new ReportMasterInstallationDAL();
                var dalShowdata = dal.ShowFilterTwonew(data.PROFIT_CENTER, data.INSTALLATION_CODE, data.CUSTOMER_ID, data.CUSTOMER_SAP, data.STATUS, KodeCabang); 

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[19] { 
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("INSTALLATION TYPE", typeof(string)),
                new DataColumn("INSTALLATION CODE",typeof(string)),
                new DataColumn("SERIAL_NUMBER",typeof(string)),
                new DataColumn("MDM CUSTOMER", typeof(string)),
                new DataColumn("CUSTOMER SAP",typeof(string)),
                new DataColumn("INSTALLATION ADDRESS", typeof(string)),
                new DataColumn("INSTALLATION DATE",typeof(string)),
                new DataColumn("BIAYA BEBAN", typeof(string)),
                new DataColumn("STATUS",typeof(string)),
                new DataColumn("TARIF BLOCK1",typeof(string)),
                new DataColumn("TARIF BLOCK2",typeof(string)),
                new DataColumn("TARIF KVARH",typeof(string)),
                new DataColumn("TARIF LWBP",typeof(string)),
                new DataColumn("TARIF WBP",typeof(string)),
                new DataColumn("PPJU",typeof(string)),
                new DataColumn("REDUKSI",typeof(string)),
                new DataColumn("MIN USED",typeof(string)),
                new DataColumn("ASSIGNMENT RO",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    if (valData.STATUS == "1")
                    {
                        dt.Rows.Add(valData.PROFIT_CENTER, valData.INSTALLATION_TYPE, valData.INSTALLATION_CODE, valData.SERIAL_NUMBER, valData.CUSTOMER_NAME,
                                    valData.CUSTOMER_SAP_AR, valData.INSTALLATION_ADDRESS, valData.INSTALLATION_DATE,
                                    valData.TARIFF_CODE, "ACTIVE", valData.TARIF_BLOCK1, valData.TARIF_BLOCK2, valData.TARIF_KVARH, valData.TARIF_LWBP, valData.TARIF_WBP, valData.PPJU, valData.REDUKSI, valData.MINIMUM_USED, valData.ASSIGNMENT_RO);
                    }
                    else
                    {
                        dt.Rows.Add(valData.PROFIT_CENTER, valData.INSTALLATION_TYPE, valData.INSTALLATION_CODE, valData.SERIAL_NUMBER, valData.CUSTOMER_NAME,
                                        valData.CUSTOMER_SAP_AR, valData.INSTALLATION_ADDRESS, valData.INSTALLATION_DATE,
                                        valData.TARIFF_CODE, "INACTIVE", valData.TARIF_BLOCK1, valData.TARIF_BLOCK2, valData.TARIF_KVARH, valData.TARIF_LWBP, valData.TARIF_WBP, valData.PPJU, valData.REDUKSI, valData.MINIMUM_USED, valData.ASSIGNMENT_RO);
                    }
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Master Installation");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));

                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}