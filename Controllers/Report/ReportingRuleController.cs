﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportingRuleController : Controller
    {
        //
        // GET: /ReportinRule/
        public ActionResult Index()
        {
            return View("ReportingRuleIndex");
        }

        public ActionResult Add()
        {
            return View("Add");
        }
	}
}