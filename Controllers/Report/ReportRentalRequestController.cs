﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Entities;
using Remote.DAL;
using System.IO;
using ClosedXML.Excel;
//using System.Web.Services;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportRentalRequestController : Controller
    {
        //
        // GET: /ReportRentalRequest/
        public ActionResult Index()
        {
            return View("ReportRentalRequestIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataRentalRequestType()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            IEnumerable<DDRentalType> result = dal.GetDataRentalRequestType(KodeCabang);
            return Json(new { data = result });
        }
           
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataFilter()
        {
            DataTableReportRentalRequest result = new DataTableReportRentalRequest();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string customer_id = queryString["customer_id"];
                    string active_status = queryString["active_status"];
                    string rental_request_status = queryString["rental_request_status"];
                    string rental_request_type = queryString["rental_request_type"];
                    string rental_request_no = queryString["rental_request_no"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
                    DataTableReportRentalRequest data = dal.GetDataFiltersnew(draw, start, length, be_id, profit_center, customer_id, active_status, rental_request_status, rental_request_type, rental_request_no, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_RENTAL_REQUEST> listData = null;
            var queryString = Request.Query;

            string be_id = queryString["be_id"];
            string profit_center = queryString["profit_center"];
            string customer_id = queryString["customer_id"];
            string active_status = queryString["active_status"];
            string rental_request_status = queryString["rental_request_status"];
            string rental_request_type = queryString["rental_request_type"];
            string rental_request_no = queryString["rental_request_no"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
            listData = dal.ShowFilterTwo(be_id, profit_center, customer_id, active_status, rental_request_status, rental_request_type, rental_request_no, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportRentalRequestParam param)
        {
            nama result = new nama();
            string fileName = "ReportRentalRequest(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportRentalRequestDAL dal = new ReportRentalRequestDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.BUSINESS_ENTITY, param.PROFIT_CENTER_ID, param.CUSTOMER_ID, param.ACTIVE_STATUS, param.RENTAL_REQUEST_STATUS, param.RENTAL_REQUEST_TYPE, param.RENTAL_REQUEST_NO, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[15] { 
                new DataColumn("RENTAL REQUEST NO", typeof(string)),
                new DataColumn("RENTAL REQUEST TYPE", typeof(string)),
                new DataColumn("RENTAL REQUEST NAME",typeof(string)),
                new DataColumn("START DATE", typeof(string)),
                new DataColumn("END DATE",typeof(string)),
                new DataColumn("CONTRACT USAGE", typeof(string)),
                new DataColumn("CUSTOMER NAME",typeof(string)),
                new DataColumn("CUSTOMER AR",typeof(string)),
                new DataColumn("OLD CONTRACT", typeof(string)),
                new DataColumn("OBJECT ID", typeof(string)),
                new DataColumn("RO NAME",typeof(string)),
                new DataColumn("LAND AREA (m2)", typeof(string)),
                new DataColumn("BUILDING AREA (m2)", typeof(string)),
                new DataColumn("INDUSTRY TYPE",typeof(string)),
                new DataColumn("REQUEST STATUS", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.RENTAL_REQUEST_NO, valData.REQUEST_TYPE, valData.RENTAL_REQUEST_NAME,
                                valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE, valData.USAGE_TYPE,
                                valData.CUSTOMER, valData.CUSTOMER_AR, valData.OLD_CONTRACT,
                                valData.OBJECT_ID, valData.RO_NAME, valData.LAND_DIMENSION,
                                valData.BUILDING_DIMENSION, valData.INDUSTRY, valData.STATUS);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportRentalRequest");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}