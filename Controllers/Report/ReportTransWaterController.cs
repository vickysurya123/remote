﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Remote.Models;
using Remote.Models.ReportTransWater;
using Remote.ViewModels;

namespace Remote.Controllers.Report
{
    [Authorize]
    public class ReportTransWaterController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            var viewModel = new ReportTransWater
            {
                DDBusinessEntity = dal.GetDataBEnew(KodeCabang),
                DDProfitCenter = dal.GetDataProfitCenternew(KodeCabang)
            };
            return View("ReportTransWaterIndex", viewModel);
        }


        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            var xcustomer = dal.DDCustomernew(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenternew(KodeCabang);
            return Json(new { data = result });
        }

        //---------------------------- BUTTON FILTER -----------------
        public JsonResult ShowFilter()
        {
            DataTableReportTransWater result = new DataTableReportTransWater();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string profit_center = queryString["profit_center"];
                    string transaction_no = queryString["transaction_no"];
                    string sap_doc_no = queryString["sap_doc_no"];
                    string period = queryString["period"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string installation_code = queryString["installation_code"];
                    string customer = queryString["customer"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransWaterDAL dal = new ReportTransWaterDAL();
                    DataTableReportTransWater data = dal.GetDataFilter(draw, start, length, profit_center, transaction_no, sap_doc_no, period, posting_date_from, posting_date_to, installation_code, customer, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {

                }
            }

            return Json(result);
        }

        //------------------------------- filter new ---------------------------------------
        public JsonResult ShowFilterNew()
        {
            DataTableReportTransWater result = new DataTableReportTransWater();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string profit_center = queryString["profit_center"];
                    string transaction_no = queryString["transaction_no"];
                    string sap_doc_no = queryString["sap_doc_no"];
                    string period = queryString["period"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string installation_code = queryString["installation_code"];
                    string customer = queryString["customer"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransWaterDAL dal = new ReportTransWaterDAL();
                    DataTableReportTransWater data = dal.GetDataFilternew(draw, start, length, profit_center, transaction_no, sap_doc_no, period, posting_date_from, posting_date_to, installation_code, customer, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    var aa = ex.ToString();
                }
            }

            return Json(result);
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportTransWaterParam param)
        {
            nama result = new nama();
            string fileName = "ReportTransWater(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportTransWaterDAL dal = new ReportTransWaterDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.PROFIT_CENTER, param.TRANSACTION_NO, param.SAP_DOC_NO, param.PERIOD, param.POSTING_DATE_FROM, param.POSTING_DATE_TO, param.INSTALLATION_CODE, param.CUSTOMER_ID, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[15] {
                new DataColumn("TRANSACTION NUMBER", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CUSTOMER MDM", typeof(string)),
                new DataColumn("INSTALLATION NO",typeof(string)),
                new DataColumn("INSTALLATION ADDRESS", typeof(string)),
                new DataColumn("PERIOD",typeof(string)),
                new DataColumn("METER FROM-TO", typeof(string)),
                new DataColumn("USED QUANTITY",typeof(string)),
                new DataColumn("MIN USED", typeof(string)),
                new DataColumn("TARIF",typeof(string)),
                new DataColumn("SURCHARGES", typeof(string)),
                new DataColumn("GRAND TOTAL",typeof(string)),
                new DataColumn("POSTING DATE",typeof(string)),
                new DataColumn("SAP DOC NO", typeof(string)),
                new DataColumn("STATUS",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                    if (valData.CANCEL_STATUS == "1")
                    {
                        dt.Rows.Add(valData.ID, valData.PROFIT_CENTER_NAME, valData.CUSTOMER_NAME, valData.INSTALLATION_CODE,
                                    valData.INSTALLATION_ADDRESS, valData.PERIOD, valData.METER_FROM_TO,
                                    valData.QUANTITY, valData.MIN_USED, valData.TARIFF_CODE,
                                    valData.SURCHARGE, valData.SUB_TOTAL, valData.POSTING_DATE, valData.SAP_DOCUMENT_NUMBER,
                                    "CANCEL");
                    }
                    else
                    {
                        dt.Rows.Add(valData.ID, valData.PROFIT_CENTER_NAME, valData.CUSTOMER_NAME, valData.INSTALLATION_CODE,
                                    valData.INSTALLATION_ADDRESS, valData.PERIOD, valData.METER_FROM_TO,
                                    valData.QUANTITY, valData.MIN_USED, valData.TARIFF_CODE,
                                    valData.SURCHARGE, valData.SUB_TOTAL, valData.POSTING_DATE, valData.SAP_DOCUMENT_NUMBER,
                                    "BILLED");
                    }
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportTransWater");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

    }
}
