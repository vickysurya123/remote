﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportDocumentFlow;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportDocumentFlowController : Controller
    {
        //
        // GET: /ReportDocumentFlow TESTING/
        public ActionResult Index()
        {
            return View("ReportDocumentFlow");
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetDataBE()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBE(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataContractOfferNumb()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDDocumentFlow> result = dal.GetDataContractOfferNumb(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataRentalRequestNumb()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDDocumentFlow> result = dal.GetDataRentalRequestNumb(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataContractOfferStatus()
        {
            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDDocumentFlow> result = dal.GetDataContractOfferStatus();
            return Json(new { data = result });
        }
        public JsonResult GetDataRentalRequestStatus()
        {
            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDDocumentFlow> result = dal.GetDataRentalRequestStatus();
            return Json(new { data = result });
        }
        public JsonResult GetDataContractNumb()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            IEnumerable<DDDocumentFlow> result = dal.GetDataContractNumb(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_DOCUMENT_FLOW> listData = null;
            var queryString = Request.Query;

            string be_id = queryString["be_id"];
            string rental_request_number = queryString["rental_request_number"];
            string rental_request_status = queryString["rental_request_status"];
            string contract_offer_number = queryString["contract_offer_number"];
            string contract_offer_status = queryString["contract_offer_status"];
            string contact_number = queryString["contact_number"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
            listData = dal.ShowFilterTwo(be_id, rental_request_number, rental_request_status, contract_offer_number, contract_offer_status, contact_number, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        public JsonResult defineExcel(string BUSINESS_ENTITY, string CONTRACT_OFFER_NUMBER, string RENTAL_REQUEST_NUMBER, string CONTRACT_OFFER_STATUS, string RENTAL_REQUEST_STATUS, string CONTRACT_NUMBER)
        {
            nama result = new nama();
            string fileName = "ReportDocumentFlow(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {   
                string KodeCabang = CurrentUser.KodeCabang;
                ReportDocumentFlowDAL dal = new ReportDocumentFlowDAL();
                var dalShowdata = dal.ShowFilterTwo(BUSINESS_ENTITY, RENTAL_REQUEST_NUMBER, RENTAL_REQUEST_STATUS, CONTRACT_OFFER_NUMBER, CONTRACT_OFFER_STATUS, CONTRACT_NUMBER, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[27] { 
                new DataColumn("BUSINESS_ENTITY", typeof(int)),
                new DataColumn("RENTAL_REQUEST_NUMBER", typeof(string)),
                new DataColumn("RENTAL_REQUEST_CREATED_BY",typeof(string)),
                new DataColumn("DATE_RENTAL_REQUEST_CREATE", typeof(string)),
                new DataColumn("RENTAL_REQUEST_STATUS",typeof(string)),
                new DataColumn("DATE_RENTAL_REQUEST_APPROVED", typeof(string)),
                new DataColumn("CONTRACT_OFFER_NUMBER",typeof(string)),
                new DataColumn("CONTRACT_OFFER_LAST_STATUS", typeof(string)),
                new DataColumn("DELAYS_RENTAL_REQUEST_APPROVED",typeof(string)),
                new DataColumn("DATE_OFFER_CREATED", typeof(string)),
                new DataColumn("DELAYS_CONTRACT_OFFER_APPROVED",typeof(string)),
                new DataColumn("RELEASE_TO_WORKFLOW", typeof(string)),
                new DataColumn("DELAYS_RELEASE_TO_WORKFLOW",typeof(string)),
                new DataColumn("APPROVED_BY_MANAGER", typeof(string)),
                new DataColumn("DELAYS_APPROVE_BY_MANAGER",typeof(string)),
                new DataColumn("APPROVED_BY_DEPT_GM", typeof(string)),
                new DataColumn("DELAYS_APPROVE_BY_DEPT_GM",typeof(string)),
                new DataColumn("APPROVED_BY_GM", typeof(string)),
                new DataColumn("DELAYS_APPROVE_BY_GM",typeof(string)),
                new DataColumn("APPROVED_BY_SM", typeof(string)),
                new DataColumn("DELAYS_APPROVE_BY_SM",typeof(string)),
                new DataColumn("APPROVED_BY_DIR", typeof(string)),
                new DataColumn("DELAYS_APPROVE_BY_DIR",typeof(string)),
                new DataColumn("APPROVED_BY_KOM", typeof(string)),
                new DataColumn("APPROVED_BY_RUPS",typeof(string)),
                new DataColumn("CONTRACT_NO",typeof(string)),
                new DataColumn("CREATION_DATE", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BUSINESS_ENTITY, valData.RENTAL_REQUEST_NUMBER, valData.RENTAL_REQUEST_CREATED_BY,
                                valData.DATE_RENTAL_REQUEST_CREATE, valData.RENTAL_REQUEST_STATUS, valData.DATE_RENTAL_REQUEST_APPROVED,
                                valData.CONTRACT_OFFER_NUMBER, valData.CONTRACT_OFFER_LAST_STATUS, valData.DELAYS_RENTAL_REQUEST_APPROVED,
                                valData.DATE_OFFER_CREATED, valData.DELAYS_CONTRACT_OFFER_APPROVED, valData.RELEASE_TO_WORKFLOW,
                                valData.DELAYS_RELEASE_TO_WORKFLOW, valData.APPROVED_BY_MANAGER, valData.DELAYS_APPROVE_BY_MANAGER,
                                valData.APPROVED_BY_DEPT_GM, valData.DELAYS_APPROVE_BY_DEPT_GM, valData.APPROVED_BY_GM,
                                valData.DELAYS_APPROVE_BY_GM, valData.APPROVED_BY_SM, valData.DELAYS_APPROVE_BY_SM,
                                valData.APPROVED_BY_DIR, valData.DELAYS_APPROVE_BY_DIR, valData.APPROVED_BY_KOM,
                                valData.APPROVED_BY_RUPS, valData.CONTRACT_NO, valData.CREATION_DATE);
                } 

                //Codes for the Closed XML 
                //using (XLWorkbook wb = new XLWorkbook())
                //{ 
                //    wb.Worksheets.Add(dt, "Customers");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();
                      
                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
            
                //}
            }
            catch (Exception){}

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}