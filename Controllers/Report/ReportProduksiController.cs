﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Remote.Models;
using Remote.Models.ReportProduksi;
using Remote.ViewModels;

namespace Remote.Controllers.Report
{
    [Authorize]
    public class ReportProduksiController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            var viewModel = new ReportTransWater
            {
                DDBusinessEntity = dal.GetDataBEnew(KodeCabang),
                DDProfitCenter = dal.GetDataProfitCenternew(KodeCabang)
            };
            return View("ReportProduksiIndex", viewModel);
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenternew(KodeCabang);
            return Json(new { data = result });
        }


        //------------------------------- filter new ---------------------------------------
        public JsonResult ShowFilterNew()
        {
            DataTableReportProduksi result = new DataTableReportProduksi();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string period = queryString["period"];
                    string be = queryString["be"];
                    string[] dataPeriod = period.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                    string bulan = dataPeriod[0];
                    string year = dataPeriod[1];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportProduksiDAL dal = new ReportProduksiDAL();
                    DataTableReportProduksi data = dal.GetDataFilternew(draw, start, length, bulan, year, be);

                    result = data;
                }
                catch (Exception ex)
                {
                    var aa = ex.ToString();
                }
            }

            return Json(result);
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportTransWaterParam param)
        {
            nama result = new nama();
            string fileName = "ReportProduksi(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportProduksiDAL dal = new ReportProduksiDAL();
                string period = param.PERIOD;
                string[] dataPeriod = period.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                string bulan = dataPeriod[0];
                string year = dataPeriod[1];
                string be = param.BE_ID;
                var dalShowdata = dal.ShowFilterTwonew(bulan, year, be);

                DataTable dt = new DataTable();

                dt.Columns.AddRange(new DataColumn[12] {
                new DataColumn("NO", typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("PRANOTA", typeof(string)),
                new DataColumn("CUSTOMER", typeof(string)),
                new DataColumn("PROFIT CENTER",typeof(string)),
                new DataColumn("RO CODE", typeof(string)),
                new DataColumn("RO NAME",typeof(string)),
                new DataColumn("CONTRACT START", typeof(string)),
                new DataColumn("CONTRACT END",typeof(string)),
                new DataColumn("LUAS", typeof(string)),
                new DataColumn("SATUAN",typeof(string)),
                new DataColumn("COA",typeof(string)),
                });
                var temp = "";
                var temp_ro = "";
                foreach (var valData in dalShowdata)
                {
                    if(temp == valData.CONTRACT_NO && temp_ro == valData.OBJECT_ID)
                        dt.Rows.Add(valData.NO, valData.CONTRACT_NO, valData.BILLING_NO, valData.CUSTOMER_NAME, valData.PROFIT_CENTER,
                                        valData.RO_CODE, valData.RO_NAME, valData.CONTRACT_START_DATE,
                                        valData.CONTRACT_END_DATE, 0, valData.SATUAN, valData.COA_PROD);
                    else
                        dt.Rows.Add(valData.NO, valData.CONTRACT_NO, valData.BILLING_NO, valData.CUSTOMER_NAME, valData.PROFIT_CENTER,
                                        valData.RO_CODE, valData.RO_NAME, valData.CONTRACT_START_DATE,
                                        valData.CONTRACT_END_DATE, valData.LUAS, valData.SATUAN, valData.COA_PROD);
                    temp = valData.CONTRACT_NO;
                    temp_ro = valData.OBJECT_ID;
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    var ws = wb.Worksheets.Add("ReportProduksi");
                    ws.Cell(1, 1).InsertData(new string[] { "Periode" });
                    ws.Cell(1, 2).InsertData(new string[] { param.PERIOD });
                    ws.Cell(2, 1).InsertData(new string[] { "Tahun" });
                    ws.Cell(2, 2).InsertData(new string[] { year });
                    ws.Cell(4, 1).InsertTable(dt);
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

    }
}
