﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportPriceWaterElectricityController : Controller
    {
        //
        // GET: /ReportPriceWaterElectricity/ 
        public ActionResult Index()
        {
            return View("ReportPriceWaterElectricityIndex");
        }
	}
}