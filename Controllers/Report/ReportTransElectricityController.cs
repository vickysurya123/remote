﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportTransElectricity;
using Remote.Models.Select2;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportTransElectricityController : Controller
    {
        //
        // GET: /ReportTransOtherService/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;

            ReportTransElectricityDAL dal = new ReportTransElectricityDAL();
            var viewModel = new ReportTransWater
            {
                DDBusinessEntity = dal.GetDataBEnew(KodeCabang),
                DDProfitCenter = dal.GetDataProfitCenternew(KodeCabang)
            };
            return View("ReportTransElectricityIndex", viewModel);
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransElectricityDAL dal = new ReportTransElectricityDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenternew(KodeCabang);
            return Json(new { data = result });
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //-------------------------------- BUTTON FILTER -------------------
        public JsonResult ShowFilter()
        {
            DataTableReportTransElectricity result = new DataTableReportTransElectricity();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string customer_id = queryString["customer_id"];
                    string power_capacity = queryString["power_capacity"];
                    string period = queryString["period"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string sap_document_number = queryString["sap_document_number"];
                    string condition_used = queryString["condition_used"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransElectricityDAL dal = new ReportTransElectricityDAL();
                    DataTableReportTransElectricity data = dal.GetDataFilter(draw, start, length, profit_center, billing_no, customer_id, power_capacity, period, posting_date_from, posting_date_to, sap_document_number, condition_used, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //-------------------------------- filter new -------------------
        public JsonResult ShowFilterNew()
        {
            DataTableReportTransElectricity result = new DataTableReportTransElectricity();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string customer_id = queryString["customer_id"];
                    string power_capacity = queryString["power_capacity"];
                    string period = queryString["period"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string sap_document_number = queryString["sap_document_number"];
                    string condition_used = queryString["condition_used"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransElectricityDAL dal = new ReportTransElectricityDAL();
                    DataTableReportTransElectricity data = dal.GetDataFilternew(draw, start, length, profit_center, billing_no, customer_id, power_capacity, period, posting_date_from, posting_date_to, sap_document_number, condition_used, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            CustomerDAL dal = new CustomerDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }


        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportTransElectricityParam param)
        {
            nama result = new nama();
            string fileName = "ReportTransElectricity(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportTransElectricityDAL dal = new ReportTransElectricityDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.PROFIT_CENTER, param.BILLING_NO, param.CUSTOMER_ID, param.POWER_CAPACITY, param.PERIOD, param.POSTING_DATE_FROM, param.POSTING_DATE_TO, param.SAP_DOCUMENT_NUMBER, param.CONDITION_USED, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[31] {
                new DataColumn("TRANSACTION NUMBER", typeof(string)),
                new DataColumn("STATUS_DINAS", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CUSTOMER MDM", typeof(string)),
                new DataColumn("CUSTOMER NAME",typeof(string)),
                new DataColumn("INSTALLATION NUMBER", typeof(string)),
                new DataColumn("INSTALLATION ADDRESS",typeof(string)),
                new DataColumn("POWER CAPACITY", typeof(string)),
                new DataColumn("MIN USED",typeof(string)),
                new DataColumn("PERIOD", typeof(string)),
                new DataColumn("METER FROM - TO(BLOCK)",typeof(string)),
                new DataColumn("USED QTY(BLOCK)", typeof(string)),
                new DataColumn("METER FROM - TO(LWBP)",typeof(string)),
                new DataColumn("USED QTY(LWBP)",typeof(string)),
                new DataColumn("METER FROM - TO(WBP)", typeof(string)),
                new DataColumn("USED QTY(WBP)",typeof(string)),
                new DataColumn("METER FROM - TO(KVARH)",typeof(string)),
                new DataColumn("USED QTY(KVARH)",typeof(string)),
                new DataColumn("FAKTOR KALI",typeof(string)),
                new DataColumn("TARIF BLOCK1",typeof(string)),
                new DataColumn("TARIF BLOCK2",typeof(string)),
                new DataColumn("TARIF LWBP",typeof(string)),
                new DataColumn("TARIF WBP",typeof(string)),
                new DataColumn("TARIF KVARH",typeof(string)),
                new DataColumn("EXPENSE TARIF",typeof(string)),
                new DataColumn("PPJU",typeof(string)),
                new DataColumn("REDUKSI",typeof(string)),
                new DataColumn("GRAND TOTAL",typeof(string)),
                new DataColumn("POSTING DATE",typeof(string)),
                new DataColumn("SAP DOCUMENT NUMBER",typeof(string)),
                new DataColumn("TRANSACTION STATUS",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.ID, valData.STATUS_DINAS, valData.PROFIT_CENTER, valData.CUSTOMER_MDM, valData.CUSTOMER_NAME,
                                    valData.INSTALLATION_CODE, valData.INSTALLATION_ADDRESS, valData.POWER_CAPACITY,
                                    valData.MINIMUM_USED, valData.PERIOD, valData.METER_FROM_TO_BLOK,
                                    valData.USED_BLOK, valData.METER_FROM_TO_LWBP, valData.USED_LWBP, valData.METER_FROM_TO_WBP,
                                    valData.USED_WBP, valData.METER_FROM_TO_KVARH, valData.USED_KVARH, valData.MULTIPLY_FACT,
                                    valData.TARIFF_BLOK, valData.TARIFF_BLOK2, valData.TARIFF_LWBP, valData.TARIFF_WBP,
                                    valData.TARIFF_KVARH, valData.EXPENSE_TARIFF, valData.PPJU, valData.REDUKSI,
                                    valData.GRAND_TOTAL, valData.POSTING_DATE, valData.SAP_DOCUMENT_NUMBER, "BILLED");
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportTransElectricity");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

    }
}