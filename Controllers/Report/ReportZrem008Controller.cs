﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReportZrem008SAPReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportZrem008Controller : Controller
    {
        public ActionResult Index()
        {
            return View("Zrem008Index");
        }

        public async Task<IActionResult> GetDataZrem008(string ProfitCenter = "", string Contract = "", string Semester = "", string TahunSemester = "", string Triwulan = "", string TahunTriwulan = "", string PostingFrom = "", string PostingTo = "", string Tahun = "")
        {
            object response = new { };
            string posting_dari = Tahun != null ? Tahun + "0101" : Semester == "1"? TahunSemester + "0101" : Semester == "2" ? TahunSemester + "0701" : Triwulan == "1" ? TahunTriwulan + "0101" : Triwulan == "2" ? TahunTriwulan + "0401" : Triwulan == "3" ? TahunTriwulan + "0701" : Triwulan == "4" ? TahunTriwulan + "1001" : PostingFrom ;
            string posting_sampai = Tahun != null ? Tahun + "1231" : Semester == "1" ? TahunSemester + "0630" : Semester == "2" ? TahunSemester + "1231" : Triwulan == "1" ? TahunTriwulan + "0331" : Triwulan == "2" ? TahunTriwulan + "0630" : Triwulan == "3" ? TahunTriwulan + "0930" : Triwulan == "4" ? TahunTriwulan + "1231" : PostingTo;

            try
            {
                REMOTEIBZREM008SoapClient client = new REMOTEIBZREM008SoapClient(REMOTEIBZREM008SoapClient.EndpointConfiguration.REMOTEIBZREM008Soap);

                inboundZrem008Request request = new inboundZrem008Request();

                //ZST_ZFMREM_IB_007D[] selectedContract = new ZST_ZFMREM_IB_007D[1];
                //selectedContract[0] = new ZST_ZFMREM_IB_007D { CONTRACT = Contract };

                ZST_ZFMREM_IB_007B[] selectedProfitCenter = new ZST_ZFMREM_IB_007B[1];
                selectedProfitCenter[0] = new ZST_ZFMREM_IB_007B { PROFIT_CENTER = "00000" + ProfitCenter };


                request.I_REPORT_TYPE = "Y";
                //request.I_BUKRS = "1000";
                //if (!String.IsNullOrEmpty(Contract))
                //{
                //    request.T_CONTRACT = selectedContract;
                //}
                if (!String.IsNullOrEmpty(ProfitCenter))
                {
                    request.T_PROFIT_CENTER = selectedProfitCenter;
                }                    
                request.I_POSTING_DATE_FR = posting_dari;
                request.I_POSTING_DATE_TO = posting_sampai;

                inboundZrem008Response result = await client.inboundZrem008Async(request);
                if(result.T_DATA.Length > 0)
                {
                    response = new { Status = "S", Message = result.T_DATA };
                }
                else
                {
                    response = new { Status = "E", Message = "Mohon maaf data tidak ditemukan"};
                }
                
            }
            catch (Exception e)
            {
                response = new { Status = "E", Message = "Gagal Mengirim, " + e.Message };
            }

            return Ok(response);
        }

    }
}