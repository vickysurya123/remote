﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.ReportSpecialContract;
using System.Data;
using System.Security.Claims;
using Remote.Entities;
using Remote.DAL;
using System.IO;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using WebApps.DAL;
using Remote.Models.NotificationContract;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class NotificationContractController : Controller
    {
        //
        // GET: /Notification/
        public ActionResult Index()
        {
            return View("NotificationContractIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            NotificationContractDAL dal = new NotificationContractDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataContractType()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            NotificationContractDAL dal = new NotificationContractDAL();
            IEnumerable<DDContractType> result = dal.GetDataContractType(KodeCabang);
            return Json(new { data = result });
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            NotificationContractDAL dal = new NotificationContractDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //------------------------- FILTER & EXCEL UNTUK NOTIFICATION CONTRACT --------
        public JsonResult ShowFilter()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string be_id = Query["be_id"];
                    string contract_no = Query["contract_no"];
                    string contract_type = Query["contract_type"];
                    string customer_id = Query["customer_id"];
                    string date_start = Query["date_start"];
                    string date_end = Query["date_end"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterNotificationnew(draw, start, length, be_id, contract_no, contract_type, customer_id, date_start, date_end, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterHistory()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterHistorynew(draw, start, length, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }

        //------------------ new filter ---------------------
        public JsonResult ShowFilterHistoryNew()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterHistorynew(draw, start, length, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }


        //------------------ filter new ----------------------
        public JsonResult ShowFilterNew()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string be_id = Query["be_id"];
                    string contract_no = Query["contract_no"];
                    string contract_type = Query["contract_type"];
                    string customer_id = Query["customer_id"];
                    string date_start = Query["date_start"];
                    string date_end = Query["date_end"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterNotificationnew(draw, start, length, be_id, contract_no, contract_type, customer_id, date_start, date_end, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }


        //------------------- filter warning new -----------------
        public JsonResult ShowFilterWarningNew()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);
                    string be_id_warning = Query["be_id_warning"];
                    string contract_no_warning = Query["contract_no_warning"];
                    string contract_type_warning = Query["contract_type_warning"];
                    string customer_id_warning = Query["customer_id_warning"];
                    string date_start_warning = Query["date_start_warning"];
                    string date_end_warning = Query["date_end_warning"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterWarningnew(draw, start, length, be_id_warning, contract_no_warning, contract_type_warning, customer_id_warning, date_start_warning, date_end_warning, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }


        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcelNotification([FromBody] ExcelNotificationContract data)
        {
            nama result = new nama();
            string fileName = "NotificationContract" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                NotificationContractDAL dal = new NotificationContractDAL();
                var dalShowdata = dal.GenerateExcelNotificationnew(data.BUSINESS_ENTITY, data.CONTRACT_NO, data.CONTRACT_TYPE, data.CUSTOMER_ID, data.DATE_START, data.DATE_END, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[18] {
                new DataColumn("CONTRACT_END_DATE", typeof(string)),
                new DataColumn("DAYS REMAINING", typeof(string)),
                new DataColumn("CUSTOMER_NAME",typeof(string)),
                new DataColumn("EMAIL", typeof(string)),
                new DataColumn("PHONE",typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_TYPE_DESC", typeof(string)),
                new DataColumn("LEGAL_CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_NAME", typeof(string)),
                new DataColumn("OBJECT_ID",typeof(string)),
                new DataColumn("OBJECT_NAME", typeof(string)),
                new DataColumn("RO_ADDRESS",typeof(string)),
                new DataColumn("BE_NAME", typeof(string)),
                new DataColumn("CONTRACT_START_DATE",typeof(string)),
                new DataColumn("STOP_NOTIFICATION_DATE", typeof(string)),
                new DataColumn("MEMO_NOTIFICATION",typeof(string)),
                new DataColumn("STATUS",typeof(string))});

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_END_DATE, valData.DAYS, valData.CUSTOMER_NAME,
                                valData.EMAIL, valData.PHONE, valData.ADDRESS,
                                valData.CONTRACT_NO, valData.CONTRACT_TYPE_DESC, valData.LEGAL_CONTRACT_NO,
                                valData.CONTRACT_NAME, valData.OBJECT_ID, valData.OBJECT_NAME,
                                valData.RO_ADDRESS, valData.BE_NAME, valData.CONTRACT_START_DATE,
                                valData.STOP_NOTIFICATION_DATE, valData.MEMO_NOTIFICATION, valData.STATUS);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Contract");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "Contract");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        //[/*WebMethod*/]
        public JsonResult defineExcelNotificationHistory()
        {
            nama result = new nama();
            string fileName = "NotificationContract" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                NotificationContractDAL dal = new NotificationContractDAL();
                var dalShowdata = dal.GenerateExcelNotificationHistorynew(KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[17] {
                new DataColumn("CONTRACT_END_DATE", typeof(string)),
                new DataColumn("CUSTOMER_NAME",typeof(string)),
                new DataColumn("EMAIL", typeof(string)),
                new DataColumn("PHONE",typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_TYPE_DESC", typeof(string)),
                new DataColumn("LEGAL_CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_NAME", typeof(string)),
                new DataColumn("OBJECT_ID",typeof(string)),
                new DataColumn("OBJECT_NAME", typeof(string)),
                new DataColumn("RO_ADDRESS",typeof(string)),
                new DataColumn("BE_NAME", typeof(string)),
                new DataColumn("CONTRACT_START_DATE",typeof(string)),
                new DataColumn("STOP_NOTIFICATION_DATE", typeof(string)),
                new DataColumn("MEMO_NOTIFICATION",typeof(string)),
                new DataColumn("STATUS",typeof(string))});

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_END_DATE, valData.CUSTOMER_NAME,
                                valData.EMAIL, valData.PHONE, valData.ADDRESS,
                                valData.CONTRACT_NO, valData.CONTRACT_TYPE_DESC, valData.LEGAL_CONTRACT_NO,
                                valData.CONTRACT_NAME, valData.OBJECT_ID, valData.OBJECT_NAME,
                                valData.RO_ADDRESS, valData.BE_NAME, valData.CONTRACT_START_DATE,
                                valData.STOP_NOTIFICATION_DATE, valData.MEMO_NOTIFICATION, valData.STATUS);
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "Contract");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        public JsonResult ConfirmationDate(DataNotificationContract data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    NotificationContractDAL dal = new NotificationContractDAL();
                    bool result = dal.ConfirmationDate(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Email Has Been Stopped."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------------- FILTER UNTUK WARNING NOTIFICATION CONTRACT --------
        public JsonResult ShowFilterWarning()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);
                    string be_id_warning = Query["be_id_warning"];
                    string contract_no_warning = Query["contract_no_warning"];
                    string contract_type_warning = Query["contract_type_warning"];
                    string customer_id_warning = Query["customer_id_warning"];
                    string date_start_warning = Query["date_start_warning"];
                    string date_end_warning = Query["date_end_warning"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterWarningnew(draw, start, length, be_id_warning, contract_no_warning, contract_type_warning, customer_id_warning, date_start_warning, date_end_warning, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterWarningHistory()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterWarningHistorynew(draw, start, length, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }

        //------------------------- history warning new ----------------------
        public JsonResult ShowFilterWarningHistoryNew()
        {
            DataTablesNotificationContract result = new DataTablesNotificationContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var Query = Request.Query;
                    int draw = int.Parse(Query["draw"]);
                    int start = int.Parse(Query["start"]);
                    int length = int.Parse(Query["length"]);

                    string KodeCabang = CurrentUser.KodeCabang;
                    NotificationContractDAL dal = new NotificationContractDAL();
                    DataTablesNotificationContract data = dal.ShowFilterWarningHistorynew(draw, start, length, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }


        [HttpPost]
        public JsonResult defineExcelWarning([FromBody] ExcelNotificationContractWarning data)
        {
            nama result = new nama();
            string fileName = "ExpiredContract" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                NotificationContractDAL dal = new NotificationContractDAL();
                var dalShowdata = dal.GenerateExcelWarningnew(data.BUSINESS_ENTITY_WARNING, data.CONTRACT_NO_WARNING, data.CONTRACT_TYPE_WARNING, data.CUSTOMER_ID_WARNING, data.DATE_START_WARNING, data.DATE_END_WARNING, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[18] {
                new DataColumn("CONTRACT_END_DATE", typeof(string)),
                new DataColumn("EXPIRED DAY", typeof(string)),
                new DataColumn("CUSTOMER_NAME",typeof(string)),
                new DataColumn("EMAIL", typeof(string)),
                new DataColumn("PHONE",typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_TYPE_DESC", typeof(string)),
                new DataColumn("LEGAL_CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_NAME", typeof(string)),
                new DataColumn("OBJECT_ID",typeof(string)),
                new DataColumn("OBJECT_NAME", typeof(string)),
                new DataColumn("RO_ADDRESS",typeof(string)),
                new DataColumn("BE_NAME", typeof(string)),
                new DataColumn("CONTRACT_START_DATE",typeof(string)),
                new DataColumn("STOP_WARNING_DATE", typeof(string)),
                new DataColumn("MEMO_WARNING",typeof(string)),
                new DataColumn("STATUS",typeof(string))});

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_END_DATE, valData.DAYS, valData.CUSTOMER_NAME,
                                valData.EMAIL, valData.PHONE, valData.ADDRESS,
                                valData.CONTRACT_NO, valData.CONTRACT_TYPE_DESC, valData.LEGAL_CONTRACT_NO,
                                valData.CONTRACT_NAME, valData.OBJECT_ID, valData.OBJECT_NAME,
                                valData.RO_ADDRESS, valData.BE_NAME, valData.CONTRACT_START_DATE,
                                valData.STOP_WARNING_DATE, valData.MEMO_WARNING, valData.STATUS);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Contract");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        public JsonResult defineExcelWarningHistory()
        {
            nama result = new nama();
            string fileName = "ExpiredContract" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                NotificationContractDAL dal = new NotificationContractDAL();
                var dalShowdata = dal.GenerateExcelWarningHistorynew(KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[18] {
                new DataColumn("CONTRACT_END_DATE", typeof(string)),
                new DataColumn("EXPIRED DAY", typeof(string)),
                new DataColumn("CUSTOMER_NAME",typeof(string)),
                new DataColumn("EMAIL", typeof(string)),
                new DataColumn("PHONE",typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_TYPE_DESC", typeof(string)),
                new DataColumn("LEGAL_CONTRACT_NO",typeof(string)),
                new DataColumn("CONTRACT_NAME", typeof(string)),
                new DataColumn("OBJECT_ID",typeof(string)),
                new DataColumn("OBJECT_NAME", typeof(string)),
                new DataColumn("RO_ADDRESS",typeof(string)),
                new DataColumn("BE_NAME", typeof(string)),
                new DataColumn("CONTRACT_START_DATE",typeof(string)),
                new DataColumn("STOP_WARNING_DATE", typeof(string)),
                new DataColumn("MEMO_WARNING",typeof(string)),
                new DataColumn("STATUS",typeof(string))});

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_END_DATE, valData.DAYS, valData.CUSTOMER_NAME,
                                valData.EMAIL, valData.PHONE, valData.ADDRESS,
                                valData.CONTRACT_NO, valData.CONTRACT_TYPE_DESC, valData.LEGAL_CONTRACT_NO,
                                valData.CONTRACT_NAME, valData.OBJECT_ID, valData.OBJECT_NAME,
                                valData.RO_ADDRESS, valData.BE_NAME, valData.CONTRACT_START_DATE,
                                valData.STOP_WARNING_DATE, valData.MEMO_WARNING, valData.STATUS);
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "Contract");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult CheckOutDate([FromBody]DataNotificationContract data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    NotificationContractDAL dal = new NotificationContractDAL();
                    bool result = dal.CheckOutDate(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Email Has Been Stopped."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------------- EXCEL NAMES ----------
        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        private static string[] GetFileNames(string path, string filter)
        {
            string[] files = Directory.GetFiles(path, filter);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]);
            return files;
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

    }
}