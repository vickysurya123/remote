﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.Models;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Entities;
using Remote.DAL;
using System.IO;
using ClosedXML.Excel;
//using System.Web.Services;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using System.ServiceModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportContractOfferController : Controller
    {
        //
        // GET: /ReportContractOffer/
        public ActionResult Index()
        {
            return View("ReportContractOfferIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataOfferStatus()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> result = dal.GetDataOfferStatus(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataOfferType()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> result = dal.GetDataOfferType(KodeCabang);
            return Json(new { data = result });
        }
        
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataFilter()
        {
            DataTableReportContractOffer result = new DataTableReportContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string customer_id = queryString["customer_id"];
                    string offer_status = queryString["offer_status"];
                    string offer_type = queryString["offer_type"];
                    string contract_offer_no = queryString["contract_offer_no"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportContractOfferDAL dal = new ReportContractOfferDAL();
                    DataTableReportContractOffer data = dal.GetDataFilters(draw, start, length, be_id, profit_center, customer_id, offer_status, offer_type, contract_offer_no, valid_from, valid_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //--------------------------- FILTER NEW --------------------------
        public JsonResult GetDataFilterNew()
        {
            DataTableReportContractOffer result = new DataTableReportContractOffer();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string customer_id = queryString["customer_id"];
                    string offer_status = queryString["offer_status"];
                    string offer_type = queryString["offer_type"];
                    string contract_offer_no = queryString["contract_offer_no"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportContractOfferDAL dal = new ReportContractOfferDAL();
                    DataTableReportContractOffer data = dal.GetDataFiltersnew(draw, start, length, be_id, profit_center, customer_id, offer_status, offer_type, contract_offer_no, valid_from, valid_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_CONTRACT_OFFER> listData = null;
            var queryString = Request.Query;

            /*
                data.be_id = $('#BUSINESS_ENTITY').val();
                data.profit_center = $('#PROFIT_CENTER').val();
                data.customer_id = $('#CUSTOMER_ID').val();
                data.offer_status = $('#OFFER_STATUS').val();
                data.offer_type = $('#OFFER_TYPE').val();
                data.rental_request_no = $('#RENTAL_REQUEST_NO').val();
                data.contract_offer_no = $('#CONTRACT_OFFER_NO').val();
            */

            string be_id = queryString["be_id"];
            string profit_center = queryString["profit_center"];
            string customer_id = queryString["customer_id"];
            string offer_status = queryString["offer_status"];
            string offer_type = queryString["offer_type"];
            string contract_offer_no = queryString["contract_offer_no"];
            string valid_from = queryString["valid_from"];
            string valid_to = queryString["valid_to"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportContractOfferDAL dal = new ReportContractOfferDAL();
            listData = dal.ShowFilterTwo(be_id, profit_center, customer_id, offer_status, offer_type, contract_offer_no, valid_from, valid_to, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportContractOfferParam param)
        {
            nama result = new nama();
            string fileName = "ReportContractOffer(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportContractOfferDAL dal = new ReportContractOfferDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.BUSINESS_ENTITY, param.PROFIT_CENTER, param.CUSTOMER_ID, param.OFFER_STATUS, param.OFFER_TYPE, param.CONTRACT_OFFER_NO, param.VALID_FROM, param.VALID_TO, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[20] {
                new DataColumn("CONTRACT OFFER NO", typeof(string)),
                new DataColumn("CREATE CONTRACT OFFER BY", typeof(string)),
                new DataColumn("RENTAL REQUEST NO", typeof(string)),
                new DataColumn("CONTRACT OFFER TYPE",typeof(string)),
                new DataColumn("CONTRACT OFFER NAME", typeof(string)),
                new DataColumn("VALID FROM",typeof(string)),
                new DataColumn("VALID TO", typeof(string)),
                new DataColumn("CUSTOMER NAME",typeof(string)),
                new DataColumn("CUSTOMER AR", typeof(string)),
                new DataColumn("PROFIT CENTER",typeof(string)),
                new DataColumn("CONTRACT USAGE", typeof(string)),
                new DataColumn("CONTRACT CURRENCY",typeof(string)),
                new DataColumn("OBJECT ID", typeof(string)),
                new DataColumn("NAME OF RO", typeof(string)),
                new DataColumn("INDUSTRY TYPE",typeof(string)),
                new DataColumn("LUAS TANAH (m2)", typeof(string)),
                new DataColumn("LUAS BANGUNAN (m2)",typeof(string)),
                new DataColumn("CONTRACT NOMINAL VALUE", typeof(string)),
                new DataColumn("CONDITION TYPE CONTRACT OFFER",typeof(string)),
                new DataColumn("OFFER STATUS",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_OFFER_NO, valData.CREATION_BY, valData.RENTAL_REQUEST_NO, valData.OFFER_TYPE,
                                valData.CONTRACT_OFFER_NAME, valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE,
                                valData.CUSTOMER_NAME, valData.CUSTOMER_AR, valData.PROFIT_CENTER,
                                valData.CONTRACT_USAGE, valData.CURRENCY, valData.OBJECT_ID, valData.RO_NAME,
                                valData.INDUSTRY, valData.LAND_DIMENSION, valData.BUILDING_DIMENSION, valData.TOTAL_NET_VALUE, valData.CONDITION_TYPE, valData.CONTRACT_OFFER_STATUS);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportContractOffer");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}