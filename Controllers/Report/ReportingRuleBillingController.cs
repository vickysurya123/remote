﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.ReportingRuleBilling;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportingRuleBillingController : Controller
    {
        //
        // GET: /ReportingRuleBilling/
        public ActionResult Index()
        {
            return View("ReportingRuleBillingIndex");
        }

        //------------------- DATATABLE BE ----------------------------
        public JsonResult GetDataReportingRule()
        {
            DataTableReportingRuleBilling result = new DataTableReportingRuleBilling();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ReportingRuleBillingDAL dal = new ReportingRuleBillingDAL();
                    result = dal.GetDataReportingRule(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
	}
}