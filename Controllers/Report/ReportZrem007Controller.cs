﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReportZrem007SAPReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportZrem007Controller : Controller
    {
        public ActionResult Index()
        {
            return View("Zrem007Index");
        }

        public async Task<IActionResult> GetDataZrem007(string ProfitCenter = "", string Contract = "", string Semester = "", string TahunSemester = "", string Triwulan = "", string TahunTriwulan = "", string PostingFrom = "", string PostingTo = "")
        {
            object response = new { };
            string posting_dari = Semester == "1"? TahunSemester + "0101" : Semester == "2" ? TahunSemester + "0701" : Triwulan == "1" ? TahunTriwulan + "0101" : Triwulan == "2" ? TahunTriwulan + "0401" : Triwulan == "3" ? TahunTriwulan + "0701" : Triwulan == "4" ? TahunTriwulan + "1001" : PostingFrom ;
            string posting_sampai = Semester == "1" ? TahunSemester + "0630" : Semester == "2" ? TahunSemester + "1231" : Triwulan == "1" ? TahunTriwulan + "0331" : Triwulan == "2" ? TahunTriwulan + "0630" : Triwulan == "3" ? TahunTriwulan + "0930" : Triwulan == "4" ? TahunTriwulan + "1231" : PostingTo;

            try
            {
                REMOTEIBZREM007SoapClient client = new REMOTEIBZREM007SoapClient(REMOTEIBZREM007SoapClient.EndpointConfiguration.REMOTEIBZREM007Soap);

                inboundZrem007Request request = new inboundZrem007Request();

                ZST_ZFMREM_IB_007D[] selectedContract = new ZST_ZFMREM_IB_007D[1];
                selectedContract[0] = new ZST_ZFMREM_IB_007D { CONTRACT = Contract };

                ZST_ZFMREM_IB_007B[] selectedProfitCenter = new ZST_ZFMREM_IB_007B[1];
                selectedProfitCenter[0] = new ZST_ZFMREM_IB_007B { PROFIT_CENTER = "00000" + ProfitCenter };


                request.I_BUKRS = "1000";
                if (!String.IsNullOrEmpty(Contract))
                {
                    request.T_CONTRACT = selectedContract;
                }
                if (!String.IsNullOrEmpty(ProfitCenter))
                {
                    request.T_PROFIT_CENTER = selectedProfitCenter;
                }                    
                request.I_POSTING_DATE_FR = posting_dari;
                request.I_POSTING_DATE_TO = posting_sampai;

                inboundZrem007Response result = await client.inboundZrem007Async(request);
                if(result.T_DATA.Length > 0)
                {
                    response = new { Status = "S", Message = result.T_DATA };
                }
                else
                {
                    response = new { Status = "E", Message = "Mohon maaf data tidak ditemukan"};
                }
                
            }
            catch (Exception e)
            {
                response = new { Status = "E", Message = "Gagal Mengirim, " + e.Message };
            }

            return Ok(response);
        }

    }
}