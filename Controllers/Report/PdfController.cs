﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.EmailConfiguration;
using Remote.Models.UserModels;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Remote.Helper;
using Rotativa.AspNetCore.Options;
using QRCoder;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using System.Data;
using Remote.Helpers;
using Dapper;
using System.Dynamic;
using iTextSharp.tool.xml;
using Remote.Models;

namespace Remote.Controllers
{
    //[Authorize]
    public class PdfController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        [AllowAnonymous]
        public ActionResult cetakPdf(string tp, string pc, string kc, string bn = "", string cn = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";

            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string tipe = Base64Utils.DecodeFrom64(tp);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string profitCenter = Base64Utils.DecodeFrom64(pc);
            ActionAsPdf PdfInvoice = null;
            //EnotaV2DAL dal = new EnotaV2DAL();

            //Dictionary<string, string> cookieCollection = new Dictionary<string, string>();

            try
            {
                if (tipe == "1B")
                {
                    if (kodeCabang == "99")
                    {
                        kc = CurrentUser.KodeCabang;
                    }
                    PdfInvoice = new ActionAsPdf("PranotaPropertiRemote", new { bn = bn, cn = cn, pc = pc, kc = kc });
                }

            }
            catch (Exception ex)
            {
                //PdfInvoice = new ActionAsPdf("Error");
                return RedirectToAction("Error");
            }

            //PdfInvoice.Cookies = cookieCollection;
            PdfInvoice.PageSize = Rotativa.AspNetCore.Options.Size.A4;
            PdfInvoice.PageMargins = new Margins(2, 0, 0, 0);
            //PdfInvoice.CustomSwitches = "--disable-smart-shrinking";
            return PdfInvoice;
        }

        [AllowAnonymous]
        public ActionResult cetakPdf2(string tp, string kc, string bn = "", string cn = "", string us = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";

            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string tipe = Base64Utils.DecodeFrom64(tp);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string usage = Base64Utils.DecodeFrom64(us);
            ActionAsPdf PdfInvoice = null;
            //EnotaV2DAL dal = new EnotaV2DAL();

            //Dictionary<string, string> cookieCollection = new Dictionary<string, string>();

            try
            {
                if (tipe == "1B")
                {
                    if (kodeCabang == "99")
                    {
                        kc = CurrentUser.KodeCabang;
                    }
                    PdfInvoice = new ActionAsPdf("PranotaPostBilling", new { bn = bn, cn = cn, kc = kc });
                }
                else if (tipe == "1M")
                {
                    if (usage == "ZM03")
                    {
                        PdfInvoice = new ActionAsPdf("PranotaOther", new { bn = bn, cn = cn, kc = kc });
                    }
                    else if (usage == "ZM01")
                    {
                        PdfInvoice = new ActionAsPdf("PranotaWater", new { bn = bn, kc = kc });
                    }
                    else
                    {
                        PdfInvoice = new ActionAsPdf("PranotaElectricity", new { bn = bn, kc = kc });
                    }
                }
                //else if (tipe == "1W")
                //{
                //    if(usage == "ZM01" && kodeCabang == "99")
                //    {
                //        kc = CurrentUser.KodeCabang;
                //        PdfInvoice = new ActionAsPdf("PranotaWater", new { bn = bn, cn = cn, kc = kc });
                //    }
                //}
                //else if (tipe == "1E")
                //{
                //    if(usage == "ZM02")
                //    {
                //        PdfInvoice = new ActionAsPdf("PranotaElectricity", new { bn = bn, cn = cn, kc = kc });
                //    }
                //}

            }
            catch (Exception ex)
            {
                //PdfInvoice = new ActionAsPdf("Error");
                return RedirectToAction("Error");
            }

            //PdfInvoice.Cookies = cookieCollection;
            PdfInvoice.PageSize = Rotativa.AspNetCore.Options.Size.A4;
            PdfInvoice.PageMargins = new Margins(2, 0, 0, 0);
            //PdfInvoice.CustomSwitches = "--disable-smart-shrinking";
            return PdfInvoice;
        }

        [AllowAnonymous]
        public ActionResult cetakPdfList(string tp, string kc, string bn = "", string cn = "", string us = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";

            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string tipe = Base64Utils.DecodeFrom64(tp);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string usage = Base64Utils.DecodeFrom64(us);
            ActionAsPdf PdfInvoice = null;
            //EnotaV2DAL dal = new EnotaV2DAL();

            //Dictionary<string, string> cookieCollection = new Dictionary<string, string>();

            try
            {
                if (tipe == "1B")
                {
                    if (kodeCabang == "99")
                    {
                        kc = CurrentUser.KodeCabang;
                    }
                    PdfInvoice = new ActionAsPdf("PranotaPostBilling", new { bn = bn, cn = cn, kc = kc });
                }
                else if (tipe == "1M")
                {
                    if (usage == "ZM03")
                    {
                        PdfInvoice = new ActionAsPdf("PranotaOther", new { bn = bn, cn = cn, kc = kc });
                    }
                    else if (usage == "ZM01")
                    {
                        PdfInvoice = new ActionAsPdf("PranotaWater", new { bn = bn, kc = kc });
                    }
                    else
                    {
                        PdfInvoice = new ActionAsPdf("PranotaElectricityList", new { bn = bn, kc = kc });
                    }
                }
                //else if (tipe == "1W")
                //{
                //    if(usage == "ZM01" && kodeCabang == "99")
                //    {
                //        kc = CurrentUser.KodeCabang;
                //        PdfInvoice = new ActionAsPdf("PranotaWater", new { bn = bn, cn = cn, kc = kc });
                //    }
                //}
                //else if (tipe == "1E")
                //{
                //    if(usage == "ZM02")
                //    {
                //        PdfInvoice = new ActionAsPdf("PranotaElectricity", new { bn = bn, cn = cn, kc = kc });
                //    }
                //}

            }
            catch (Exception ex)
            {
                //PdfInvoice = new ActionAsPdf("Error");
                return RedirectToAction("Error");
            }

            //PdfInvoice.Cookies = cookieCollection;
            PdfInvoice.PageSize = Rotativa.AspNetCore.Options.Size.A4;
            PdfInvoice.PageMargins = new Margins(2, 0, 0, 0);
            //PdfInvoice.CustomSwitches = "--disable-smart-shrinking";
            return PdfInvoice;
        }


        [AllowAnonymous]
        public ActionResult PranotaPropertiRemote(string pc, string kc, string bn = "", string cn = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string profitCenter = Base64Utils.DecodeFrom64(pc);
            PranotaPropertiViewModel model = new PranotaPropertiViewModel();
            List<PRANOTA_PROPERTI_DETAIL> detail = null;
            AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS header = null;
            List<PRANOTA_PROPERTY_MEMO> detailMemo = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetPropertiHeader(kodeCabang, contractNo, billingNo);
                detail = dal.GetPropertiDetail(kodeCabang, contractNo, billingNo);
                detailMemo = dal.GetPropertiMemo(contractNo, billingNo);
                manager = dal.GetManagerCabangPdf(kodeCabang, profitCenter);

                model.Detail.AddRange(detail);
                model.DetailMemo.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                string KODE_VALUTA = header.CURRENCY_CODE;

                ////totalALLBiaya = headerPranota.GRAND_TOTAL;
                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);
                var dates = header.POSTING_DATE.Split(' ');
                //Fill Model
                model.BILLING_NO = billingNo;
                model.NamaCabang = header.BE_NAME;
                model.AlamatCabang = header.BE_ADDRESS;
                model.ATAS_BEBAN = header.CUSTOMER_NAME;
                model.ALAMAT = header.MPLG_ALAMAT;
                model.PROFIT_CENTER = header.PROFIT_CENTER_CODE + " - " + header.PROFIT_CENTER_NAME;
                model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                model.Kota = header.BE_CITY;
                model.TglPosting = dates[0];
                model.Jabatan =
                model.Jabatan = manager == null ? "-" : manager.ROLE_NAME;
                model.NamaManager = manager == null ? "-" : manager.USER_NAME;
                model.LIVE_1PELINDO = header.LIVE_1PELINDO;
                //model.Jabatan = TTD == null ? "No Data Found. Please contact Administrator" : TTD.JABATAN;
                //model.PathTTD = TTD == null ? "" : "/assets/img/upd/" + TTD.PATHBELUMLUNAS;
                //model.NamaManager = TTD == null ? "" : TTD.NAMA_MANAGER;
                //model.NRK = TTD == null ? "" : TTD.NRK;
            }
            catch (Exception ex)
            {
                //return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaPropertiRemote", model);
        }

        [AllowAnonymous]
        public ActionResult PranotaPostBilling(string kc, string bn = "", string cn = "", string bill_id = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string billId = Base64Utils.DecodeFrom64(bill_id);
            PranotaPropertiViewModel model = new PranotaPropertiViewModel();
            List<PRANOTA_PROPERTI_DETAIL> detail = null;
            AUP_TRANS_CONTRACT_BILLING_LIST_WEBAPPS header = null;
            List<PRANOTA_PROPERTY_MEMO_DUA> detailMemo = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetPropertiHeader(kodeCabang, contractNo, billingNo);
                //detail = dal.GetPropertiDetail(kodeCabang, contractNo, billingNo);
                detail = dal.GetPropertiDetaildua(kodeCabang, contractNo, billingNo, billId);
                detailMemo = dal.GetPropertiMemodua(contractNo);
                manager = dal.GetManagerCabang(kodeCabang);

                model.Detail.AddRange(detail);
                model.DetailMemodua.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                string KODE_VALUTA = header.CURRENCY_CODE;

                ////totalALLBiaya = headerPranota.GRAND_TOTAL;
                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);
                var dates = header.POSTING_DATE.Split(' ');
                //Fill Model
                model.BILLING_NO = billingNo;
                model.NamaCabang = header.BE_NAME;
                model.AlamatCabang = header.BE_ADDRESS;
                model.ATAS_BEBAN = header.CUSTOMER_NAME;
                model.ALAMAT = header.MPLG_ALAMAT;
                model.PROFIT_CENTER = header.PROFIT_CENTER_CODE + " - " + header.PROFIT_CENTER_NAME;
                model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                model.Kota = header.BE_CITY;
                model.TglPosting = dates[0];
                model.LIVE_1PELINDO = header.LIVE_1PELINDO;
                if (manager != null)
                {
                    model.Jabatan = manager.ROLE_NAME;
                    model.NamaManager = manager.USER_NAME;
                }

                model.BILLING_ID = billId;
                //model.Jabatan = TTD == null ? "No Data Found. Please contact Administrator" : TTD.JABATAN;
                //model.PathTTD = TTD == null ? "" : "/assets/img/upd/" + TTD.PATHBELUMLUNAS;
                //model.NamaManager = TTD == null ? "" : TTD.NAMA_MANAGER;
                //model.NRK = TTD == null ? "" : TTD.NRK;
            }
            catch (Exception ex)
            {
                //return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaPostBilling", model);
        }

        [AllowAnonymous]
        public ActionResult PranotaWater(string kc, string bn = "", string cn = "", string bill_id = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string billId = Base64Utils.DecodeFrom64(bill_id);
            PranotaPropertiViewModel model = new PranotaPropertiViewModel();
            List<PRANOTA_WATER> detail = null;
            AUP_TRANS_WE_INSTALLATION_WEBAPPS header = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetWEHeader(kodeCabang, billingNo);
                detail = dal.GetWEDetail(kodeCabang, contractNo, billingNo, billId);
                manager = dal.GetManagerCabang(kodeCabang);

                //model.Detail.AddRange(detail);
                //model.DetailMemodua.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                decimal satuan = 0;
                decimal biaya_admin = 0;
                decimal surcharge = 0;
                decimal grandTotal = 0;
                string KODE_VALUTA = header.CURRENCY;

                //totalALLBiaya = header.AMOUNT;
                totalALLBiaya = header.GRAND_TOTAL;
                satuan = header.PRICE;
                //biaya_admin = header.BIAYA_ADMIN;
                biaya_admin = header.SURCHARGE;
                surcharge = header.SURCHARGE;
                grandTotal = header.GRAND_TOTAL;

                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);

                var dates = header.INSTALLATION_DATE;
                //Fill Model
                model.BILLING_NO = billingNo;
                model.NamaCabang = header.BE_NAME;
                model.AlamatCabang = header.BE_ADDRESS;
                model.ATAS_BEBAN = header.CUSTOMER_NAME;
                model.ALAMAT = header.INSTALLATION_ADDRESS;
                model.PROFIT_CENTER = header.PROFIT_CENTER;
                model.INSTALLATION_CODE = header.INSTALLATION_CODE;
                model.PERIOD = header.PERIOD;
                model.Kota = header.BE_CITY;
                model.TglPosting = header.POSTING_DATE;
                //model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                model.TarifWater = KODE_VALUTA + " " + satuan.ToString("N2", CultureInfo.CurrentCulture);
                model.Admin = KODE_VALUTA + " " + biaya_admin.ToString("N2", CultureInfo.CurrentCulture);
                model.DetailWater = detail;
                model.LIVE_1PELINDO = header.LIVE_1PELINDO;

                //model.Kota = header.BE_CITY;
                //model.TglPosting = dates[0];


                if (manager != null)
                {
                    model.Jabatan = manager.ROLE_NAME;
                    model.NamaManager = manager.USER_NAME;
                }

                model.BILLING_ID = billId;
            }
            catch (Exception ex)
            {
                //return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaWater", model);
        }

        [AllowAnonymous]
        public ActionResult PranotaOther(string kc, string bn = "", string cn = "", string bill_id = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string billId = Base64Utils.DecodeFrom64(bill_id);
            PranotaOtherViewModel model = new PranotaOtherViewModel();
            List<PRANOTA_OTHER_DETAIL> detail = null;
            AUP_TRANSVB_WEBAPPS header = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetOtherHeader(kodeCabang, billingNo);
                detail = dal.GetOtherDetail(billingNo);
                manager = dal.GetManagerCabang(kodeCabang);

                //model.Detail.AddRange(detail);
                //model.DetailMemodua.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                decimal satuan = 0;
                decimal biaya_admin = 0;
                string KODE_VALUTA = header.CURRENCY;

                totalALLBiaya = header.TOTAL;
                //satuan = header.PRICE;
                //biaya_admin = header.BIAYA_ADMIN;

                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);

                //var dates = header.INSTALLATION_DATE;
                //Fill Model
                model.ID = billingNo;
                model.BE_NAME = header.BE_NAME;
                model.BE_ADDRESS = header.BE_ADDRESS;
                model.COSTUMER_MDM = header.COSTUMER_MDM;
                model.INSTALLATION_ADDRESS = header.INSTALLATION_ADDRESS;
                model.PROFIT_CENTER = header.PROF_CENTER;
                model.BILLING_NO = header.ID;
                //model.PERIOD = header.PERIOD;
                model.BE_CITY = header.BE_CITY;
                model.TglPosting = header.POSTING_DATE;
                //model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                //model.TotalTagihan = KODE_VALUTA + " " + satuan.ToString("N2", CultureInfo.CurrentCulture);
                //model.Admin = KODE_VALUTA + " " + biaya_admin.ToString("N2", CultureInfo.CurrentCulture);
                model.Detail = detail;
                model.LIVE_1PELINDO = header.LIVE_1PELINDO;

                //model.Kota = header.BE_CITY;
                //model.TglPosting = dates[0];


                if (manager != null)
                {
                    //model.Jabatan = manager.ROLE_NAME;
                    //model.NamaManager = manager.USER_NAME;
                }

                //model.BILLING_ID = billId;
            }
            catch (Exception ex)
            {
                //return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaOther", model);
        }

        [AllowAnonymous]
        public ActionResult PranotaElectricity(string kc, string bn = "", string cn = "", string bill_id = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string billId = Base64Utils.DecodeFrom64(bill_id);
            PranotaPropertiViewModel model = new PranotaPropertiViewModel();
            List<PRANOTA_ELECTRICITY> detail = null;
            AUP_TRANS_WE_INSTALLATION_WEBAPPS header = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetElectricityHeader(kodeCabang, billingNo);
                detail = dal.GetElectricityDetail(kodeCabang, contractNo, billingNo, billId);
                manager = dal.GetManagerCabang(kodeCabang);

                //model.Detail.AddRange(detail);
                //model.DetailMemodua.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                decimal satuan = 0;
                decimal biaya_admin = 0;
                string KODE_VALUTA = header.CURRENCY;

                totalALLBiaya = header.AMOUNT;
                satuan = header.PRICE;
                biaya_admin = header.BIAYA_ADMIN;

                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);

                if (header.BIAYA_BEBAN == null)
                {
                    model.BEBAN = "0";
                }
                else
                {
                    model.BEBAN = header.BIAYA_BEBAN;
                }

                var dates = header.INSTALLATION_DATE;
                //Fill Model
                model.BILLING_NO = billingNo;
                model.NamaCabang = header.BE_NAME;
                model.AlamatCabang = header.BE_ADDRESS;
                model.ATAS_BEBAN = header.CUSTOMER_NAME;
                model.ALAMAT = header.INSTALLATION_ADDRESS;
                model.PROFIT_CENTER = header.PROFIT_CENTER;
                model.INSTALLATION_CODE = header.INSTALLATION_CODE;
                model.PERIOD = header.PERIOD;
                //model.BEBAN = header.BIAYA_BEBAN;
                model.DAYA = header.POWER_CAPACITY;
                model.FAKTOR_KALI = header.MULTIPLY_FACTOR;
                model.Valuta = KODE_VALUTA;
                model.PERCEN_PPJU = header.PPJU;
                model.PERCEN_REDUKSI = header.REDUKSI;
                model.Kota = header.BE_CITY;
                model.TglPosting = header.POSTING_DATE;
                //model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                model.BRANCH_ID = kodeCabang;

                //model.PRICE = KODE_VALUTA + " " + satuan.ToString("N2", CultureInfo.CurrentCulture);
                //model.BIAYA_ADMIN = KODE_VALUTA + " " + biaya_admin.ToString("N2", CultureInfo.CurrentCulture);
                model.DetailElectricity = detail;
                model.LIVE_1PELINDO = header.LIVE_1PELINDO;

                //model.Kota = header.BE_CITY;
                //model.TglPosting = dates[0];                               

                if (manager != null)
                {
                    model.Jabatan = manager.ROLE_NAME;
                    model.NamaManager = manager.USER_NAME;
                }

                model.BILLING_ID = billId;
            }
            catch (Exception ex)
            {
                //return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaElectricity", model);
        }

        [AllowAnonymous]
        public ActionResult PranotaElectricityList(string kc, string bn = "", string cn = "", string bill_id = "")
        {
            string kdCabang = "";
            string kdPelanggan = "";
            string Username = "";
            string Name = "";
            string billingNo = Base64Utils.DecodeFrom64(bn);
            string contractNo = Base64Utils.DecodeFrom64(cn);
            string kodeCabang = Base64Utils.DecodeFrom64(kc);
            string billId = Base64Utils.DecodeFrom64(bill_id);
            PranotaPropertiViewModel model = new PranotaPropertiViewModel();
            List<PRANOTA_ELECTRICITY> detail = null;
            AUP_TRANS_WE_INSTALLATION_WEBAPPS header = null;
            DataUser manager = null;
            PranotaDAL dal = new PranotaDAL();

            try
            {
                header = dal.GetElectricityHeader(kodeCabang, billingNo);
                detail = dal.GetElectricityDetail(kodeCabang, contractNo, billingNo, billId);
                manager = dal.GetManagerCabang(kodeCabang);

                //model.Detail.AddRange(detail);
                //model.DetailMemodua.AddRange(detailMemo);

                decimal totalALLBiaya = 0;
                decimal satuan = 0;
                decimal biaya_admin = 0;
                string KODE_VALUTA = header.CURRENCY;

                totalALLBiaya = header.AMOUNT;
                satuan = header.PRICE;
                biaya_admin = header.BIAYA_ADMIN;

                string jmlTerbilang = TerbilangINA(Convert.ToDouble(totalALLBiaya), KODE_VALUTA);

                if (header.BIAYA_BEBAN == null)
                {
                    model.BEBAN = "0";
                }
                else
                {
                    model.BEBAN = header.BIAYA_BEBAN;
                }

                var dates = header.INSTALLATION_DATE;
                //Fill Model
                model.BILLING_NO = billingNo;
                model.NamaCabang = header.BE_NAME;
                model.AlamatCabang = header.BE_ADDRESS;
                model.ATAS_BEBAN = header.CUSTOMER_NAME;
                model.ALAMAT = header.INSTALLATION_ADDRESS;
                model.PROFIT_CENTER = header.PROFIT_CENTER;
                model.INSTALLATION_CODE = header.INSTALLATION_CODE;
                model.PERIOD = header.PERIOD;
                //model.BEBAN = header.BIAYA_BEBAN;
                model.DAYA = header.POWER_CAPACITY;
                model.FAKTOR_KALI = header.MULTIPLY_FACTOR;
                model.Valuta = KODE_VALUTA;
                model.PERCEN_PPJU = header.PPJU;
                model.PERCEN_REDUKSI = header.REDUKSI;
                model.Kota = header.BE_CITY;
                model.TglPosting = header.POSTING_DATE;
                //model.Valuta = KODE_VALUTA;
                model.Terbilang = jmlTerbilang;
                model.TotalTagihan = KODE_VALUTA + " " + totalALLBiaya.ToString("N2", CultureInfo.CurrentCulture);
                model.BRANCH_ID = kodeCabang;

                //model.PRICE = KODE_VALUTA + " " + satuan.ToString("N2", CultureInfo.CurrentCulture);
                //model.BIAYA_ADMIN = KODE_VALUTA + " " + biaya_admin.ToString("N2", CultureInfo.CurrentCulture);
                model.DetailElectricity = detail;

                //model.Kota = header.BE_CITY;
                //model.TglPosting = dates[0];                               

                if (manager != null)
                {
                    model.Jabatan = manager.ROLE_NAME;
                    model.NamaManager = manager.USER_NAME;
                }

                model.BILLING_ID = billId;
            }
            catch (Exception ex)
            {
                return View("DataNotFound");
            }

            //return View("PranotaPropertiRemote", model);
            return View("PranotaPrint", model);
        }

        public ActionResult cetakQr(string id = "", string kodeCabang = "", string kodeProfit = "")
        {
            ActionAsPdf PdfInvoice = null;
            try
            {
                PdfInvoice = new ActionAsPdf("DataQr", new { id = id, kodeCabang = kodeCabang, kodeProfit = kodeProfit });

            }
            catch (Exception ex)
            {
                //PdfInvoice = new ActionAsPdf("Error");
                return RedirectToAction("Error");
            }
            //PdfInvoice.Cookies = cookieCollection;
            PdfInvoice.PageSize = Rotativa.AspNetCore.Options.Size.A4;
            PdfInvoice.PageMargins = new Margins(2, 0, 0, 0);
            //PdfInvoice.CustomSwitches = "--disable-smart-shrinking";
            return PdfInvoice;
        }

        [AllowAnonymous]
        public ActionResult DataQr(string id = "", string kodeCabang = "", string kodeProfit = "")
        {
            id = Base64Utils.DecodeFrom64(id);
            kodeCabang = Base64Utils.DecodeFrom64(kodeCabang);
            kodeProfit = Base64Utils.DecodeFrom64(kodeProfit);
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            List<AUP_RENTALOBJECT_Remote> result = null;

            if (id != "")
            {
                result = dal.getDataPrintQr(id, "", "");
            }
            else
            {
                result = dal.getDataPrintQr("", kodeCabang, kodeProfit);
            }
            return View("CetakQr", result);
        }

        [AllowAnonymous]
        [HttpGet]
        public FileContentResult PrintQr(string isi)
        {
            isi = !string.IsNullOrEmpty(isi) ? isi : "NO DATA";
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(isi, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            System.Drawing.Bitmap qrCodeImage = qrCode.GetGraphic(20);

            System.Drawing.Image gambarqr = qrCodeImage as System.Drawing.Image;

            using (var ms = new MemoryStream())
            {
                gambarqr.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return File(ms.ToArray(), "image/png");
            }
        }

        public ActionResult cetakEmail(string contract_no, string peringatan_no, string kd_cabang, string type)
        {
            ActionAsPdf PdfInvoice = null;
            try
            {
                PdfInvoice = new ActionAsPdf("DataEmail", new { contract_no = contract_no, peringatan_no = peringatan_no, kd_cabang = kd_cabang, type = type });

            }
            catch (Exception ex)
            {
                //PdfInvoice = new ActionAsPdf("Error");
                return RedirectToAction("Error");
            }
            //PdfInvoice.Cookies = cookieCollection;
            PdfInvoice.PageSize = Rotativa.AspNetCore.Options.Size.A4;
            PdfInvoice.PageMargins = new Margins(2, 0, 0, 0);
            //PdfInvoice.CustomSwitches = "--disable-smart-shrinking";
            return PdfInvoice;
        }

        [AllowAnonymous]
        public ActionResult DataEmail(string contract_no = "", string peringatan_no = "", string kd_cabang = "", string type = "")
        {
            contract_no = Base64Utils.DecodeFrom64(contract_no);
            peringatan_no = Base64Utils.DecodeFrom64(peringatan_no);
            kd_cabang = Base64Utils.DecodeFrom64(kd_cabang);
            type = Base64Utils.DecodeFrom64(type);

            MonitoringNotificationDAL dal = new MonitoringNotificationDAL();
            EmailNotificationViewModel model = new EmailNotificationViewModel();
            try
            {
                AUP_BE_WEBAPPS header = dal.GetHeader(kd_cabang);
                EmailConf result = dal.GetPrint(contract_no, peringatan_no, kd_cabang, type);
                model.Body = result.BODY;
                model.AlamatCabang = header.BE_ADDRESS;
                model.NamaCabang = header.BE_NAME;
                model.Kota = header.BE_CITY;
                model.Title = result.SUBJECT;

            }
            catch (Exception)
            {
                model.Body = string.Empty;
                model.AlamatCabang = string.Empty;
                model.NamaCabang = string.Empty;
                model.Kota = string.Empty;
                model.Title = "Pemberitahuan";
            }
            return View("CetakEmail", model);
        }

        [AllowAnonymous]
        public ActionResult DetailRo(string id)
        {
            //id = Base64Utils.DecodeFrom64(id);
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            List<AUP_RENTALOBJECT_Remote> data = dal.getDataPrintQr(id);
            ViewBag.RO_CODE = data[0].RO_CODE;
            ViewBag.RO_NUMBER = data[0].RO_NUMBER;
            ViewBag.BE_NAME = data[0].BE_NAME;
            ViewBag.RO_NAME = data[0].RO_NAME;
            ViewBag.RO_CERTIFICATE_NUMBER = data[0].RO_CERTIFICATE_NUMBER;
            ViewBag.RO_ADDRESS = data[0].RO_ADDRESS;
            ViewBag.RO_POSTALCODE = data[0].RO_POSTALCODE;
            ViewBag.RO_CITY = data[0].RO_CITY;
            ViewBag.MEMO = data[0].MEMO;
            ViewBag.VALID_FROM = data[0].VALID_FROM;
            ViewBag.VALID_TO = data[0].VALID_TO;
            ViewBag.ZONE_RIP_ID = data[0].ZONE_RIP_ID;
            ViewBag.RO_TYPE_ID = data[0].RO_TYPE_ID;
            //ViewBag.LOCATION_ID = data.LOCATION_ID;
            ViewBag.FUNCTION_ID = data[0].FUNCTION_ID;
            ViewBag.USAGE_TYPE = data[0].USAGE_TYPE;
            ViewBag.PROVINCE = data[0].PROVINCE;
            ViewBag.ZONE_RIP = data[0].ZONE_RIP;
            ViewBag.RO_TYPE = data[0].RO_TYPE;
            ViewBag.LOCATION_ID = data[0].LOCATION_ID;
            ViewBag.PROFIT_CENTER = data[0].PROFIT_CENTER;
            var viewModel = new RentalObject
            {

            };

            return View("DetailRo");
        }
        public string TerbilangINA(double amount, string valuta)
        {
            string valutas = (valuta == "USD") ? " DOLLAR" : " RUPIAH";
            string word = "";
            double divisor = 1000000000000.00; double large_amount = 0;
            double tiny_amount = 0;
            double dividen = 0; double dummy = 0;
            string weight1 = ""; string unit = ""; string follower = "";
            string[] prefix = { "SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            string[] sufix = { "SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            large_amount = Math.Abs(Math.Truncate(amount));
            tiny_amount = Math.Round((Math.Abs(amount) - large_amount) * 100);
            if (large_amount > divisor)
                return "OUT OF RANGE";
            while (divisor >= 1)
            {
                dividen = Math.Truncate(large_amount / divisor);
                large_amount = large_amount % divisor;
                unit = "";
                if (dividen > 0)
                {
                    if (divisor == 1000000000000.00)
                        unit = "TRILYUN ";
                    else
                        if (divisor == 1000000000.00)
                        unit = "MILYAR ";
                    else
                            if (divisor == 1000000.00)
                        unit = "JUTA ";
                    else
                                if (divisor == 1000.00)
                        unit = "RIBU ";
                }
                weight1 = "";
                dummy = dividen;
                if (dummy >= 100)
                    weight1 = prefix[(int)Math.Truncate(dummy / 100) - 1] + "RATUS ";
                dummy = dividen % 100;
                if (dummy < 10)
                {
                    if (dummy == 1 && unit == "RIBU ")
                        weight1 += "SE";
                    else
                        if (dummy > 0)
                        weight1 += sufix[(int)dummy - 1];
                }
                else
                    if (dummy >= 11 && dummy <= 19)
                {
                    weight1 += prefix[(int)(dummy % 10) - 1] + "BELAS ";
                }
                else
                {
                    weight1 += prefix[(int)Math.Truncate(dummy / 10) - 1] + "PULUH ";
                    if (dummy % 10 > 0)
                        weight1 += sufix[(int)(dummy % 10) - 1];
                }
                word += weight1 + unit;
                divisor /= 1000.00;
            }
            if (Math.Truncate(amount) == 0)
                word = "NOL ";
            follower = "";
            if (tiny_amount < 10)
            {
                if (tiny_amount > 0)
                    follower = "KOMA NOL " + sufix[(int)tiny_amount - 1];
            }
            else
            {
                follower = "KOMA " + sufix[(int)Math.Truncate(tiny_amount / 10) - 1];
                if (tiny_amount % 10 > 0)
                    follower += sufix[(int)(tiny_amount % 10) - 1];
            }
            word += follower;
            if (amount < 0)
            {
                word = "MINUS " + word + valutas;
            }
            else
            {
                word = word + valutas;
            }
            return word.Trim();
        }

        [HttpPost]
        public JsonResult GetCabang([FromBody] string be_id)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    PranotaDAL dal = new PranotaDAL();
                    result = dal.GetCabang(be_id);

                }
                catch (Exception e)
                {
                }
            }

            return Json(result);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult PdfSakti()
        {
            try
            {
                string body = $"<h1>Testing keren</h1><br /><span style='font-size:20px;color:red;'>ini HTML LO</span>";
                StringReader sr = new StringReader(body);
                Document pdfDoc = new Document(PageSize.A4, 5f, 5f, 5f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                //PdfReader reader = new PdfReader("http://galleda.s3.pelindo.co.id/REMOTE/FileSurat/PMFCTt5ziodQyU98FDbUbrIYFbc3psfhqLpAI71t.pdf");//alamat attachment
                PdfContentByte pdfCopy = null;
                PdfImportedPage page = null;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();
                    pdfCopy = writer.DirectContent;

                    //tulis text/html pdf
                    htmlparser.Parse(sr);

                    //Copy file attachment
                    //for (int i = 0; i < reader.NumberOfPages; i++)
                    //{
                    //    pdfDoc.NewPage();
                    //    page = writer.GetImportedPage(reader, i + 1);
                    //    pdfCopy.AddTemplate(page, 0, 0);
                    //}

                    //reader.Close();
                    pdfDoc.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    return File(memoryStream.ToArray(), "application/pdf");
                }
            }
            catch (Exception ex)
            {
            }

            return NotFound();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult PdfViewerSakti()
        {
            //klo setting bener hasilnya kyk ini 
            //https://mozilla.github.io/pdf.js/web/viewer.html
            //https://mozilla.github.io/pdf.js/examples/index.html#interactive-examples
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult PdfViewerSaktiV2()
        {
            //viewer native browser
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("pdf/disposisilistpdf/{id_surat}")]
        [Obsolete]
        public ActionResult DisposisiListPdf(string id_surat)
        {
            try
            {
                int x = 0;
                if (Int32.TryParse(id_surat, out x))
                {

                }
                else
                {
                    id_surat = UrlEncoder.Decode(id_surat);
                }

                var urlLogo = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/assets/global/img/pelindo.png";
                var urlTtd = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/assets/global/img/qrcode-remote.png";

                dynamic cabang = null;
                dynamic ListData = null;
                dynamic ListDataDet = null;
                string sql = @"SELECT * FROM PROP_SURAT WHERE ID = :id_suratx";
                string sqlDet = @"SELECT * FROM PROP_SURAT_ATTACHMENT WHERE SURAT_ID = :id_suratx";

                //IDbConnection connection = ServiceConfig.AppRepoDbConnection; //---CONN APP_REPO DB
                IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                ListData = connection.Query(sql, new
                {
                    id_suratx = id_surat
                }).FirstOrDefault();

                ListDataDet = connection.Query(sqlDet, new
                {
                    id_suratx = id_surat
                }).ToList();

                var kc = ListData.BRANCH_ID;
                var sql2 = @"select SUBSTR(a.BE_ADDRESS,0, 35) BE_ADDRESS, a.BE_CITY, a.BE_NAME, a.POSTAL_CODE, a.BE_CONTACT_PERSON from BUSINESS_ENTITY a where a.BRANCH_ID =:a ";
                cabang = connection.Query<dynamic>(sql2, new { a = kc }).FirstOrDefault();

                connection.Close();

                dynamic dataEmpl = "";
                using (IDbConnection conn = DatabaseFactory.GetConnection("app_repo"))
                {
                    string dataCreate = @"SELECT USER_EMAIL EMAIL , KD_CABANG BE_ID, CONCAT(CONCAT(USER_NAME,' '),C.ROLE_NAME) EMPLOYEE_NAME 
                                    FROM APP_USER A
                                    JOIN APP_USER_ROLE B ON B.USER_ID = A.ID
                                    JOIN APP_ROLE c on C.ROLE_ID = B.ROLE_ID and B.APP_ID = A.APP_ID 
                                    WHERE A.ID =:b ";

                    dataEmpl = conn.Query(dataCreate, new { b = ListData.USER_ID_CREATE }).DefaultIfEmpty(null).FirstOrDefault();
                    connection.Close();
                }
                var namaEmpl = dataEmpl.EMPLOYEE_NAME;


                //string body = settingBody();
                string res = "";
                var nomor_surat = "-";
                if (ListData.STATUS_APV == 1)
                {
                    nomor_surat = ListData.NO_SURAT;
                }
                res = string.Format(@"<div style='font-size: 13px; font-family: Arial;'>
                    <div style='text-align:right'>
                        <table width='100%' style='width:100%'>
                            <tbody>
                                <tr>
                                    <td style='width:20%; text-align:left'><img
                                            src= '" + urlLogo + @"' alt='logo' width='200px' height='75px style=''/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                            <div class='header' style='text-align: center' >
                                <h3 style='padding:0px; margin:0px'><b>SURAT</b></h3>
                                Nomor : " + nomor_surat + @"
                            </div>
                    <br />
                
                <table style='font-size: 13px; font-family: Arial; width='96%'>
                    <tr>
                        <td width='10%' align='left'>Kepada : Yth.</td>
                    </tr>
                </table>");
                int count = 0;
                if (ListData.NAMA_KEPADA != null)
                {
                    string[] namaKepada = ListData.NAMA_KEPADA.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    
                    foreach (var temp in namaKepada)
                    {
                        count++;
                        res += string.Format(@"
                        <table style='font-size: 13px; font-family: Arial; width='96%'> 
                            <tr>
			                    <td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + count + @". " + temp + @"</td>
		                    </tr>
                        </table>");
                    };
                }
                
                if (ListData.KEPADA_EKSTERNAL != null)
                {
                    string[] kepadaEksternal = ListData.KEPADA_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var temp in kepadaEksternal)
                    {
                        count++;
                        res += string.Format(@"
                        <table style='font-size: 13px; font-family: Arial; width='96%'> 
                            <tr>
			                    <td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + count + @". " + temp + @"</td>
		                    </tr>
                        </table>");
                    };
                }

                res += string.Format(@"<div style='font-size: 13px; font-family: Arial;text-align:justify;'>
                    Perihal &nbsp;&nbsp;:" + ListData.SUBJECT + @"</div>
                    <hr />
                    &nbsp;&nbsp;&nbsp;&nbsp;" + ListData.ISI_SURAT.Replace("<br>", "<br />") + @"
                    <br /><br />");

                if (ListData.NAMA_TTD != null)
                {
                    string[] dataTTD = ListData.NAMA_TTD.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                    if (ListData.STATUS_APV == 1)
                    {
                        res += string.Format(@"<table style='font-size: 13px; font-family: Arial;' width='100%'>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span>" + ListData.TANGGAL.ToString("dd-MM-yyyy") + @"</span></td>
                        </tr>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><b>" + dataTTD[1] + @"</b></span></td>
                        </tr>
                        <tr line-height='50px'>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><br /><img src='" + urlTtd + @"'width='70px' height='70px'/>
                                                        <br /></span></td>
                        </tr>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><br /><b>" + dataTTD[0] + @"</b></span></td>
                        </tr>
                        </table>");
                    }
                    else
                    {
                        res += string.Format(@"<br /><table style='font-size: 13px; font-family: Arial;' width='98%'>
                                        <tr>
                                            <td align='right'>
                                                <span>
                                                    <b>
                                                        " + dataTTD[1] + @"
                                                        <br />
                                                        <br />" + dataTTD[0] + @" 
                                                    </b>						
                                                </span>
                                            </td>
                                        </tr>
                                </table>");
                    }
                }

                if (ListData.NAMA_TEMBUSAN != null || ListData.TEMBUSAN_EKSTERNAL != null)
                {
                    int no = 0;
                    res += string.Format(@"<p></p>
                                <table style='font-size: 13px; font-family: Arial;' width='96%'>
                                        <tr>
                                            <td width='15%' align='left'>Tembusan : </td>
                                   <td></td>
                                        </tr>
                                </table>");
                    if (ListData.NAMA_TEMBUSAN != null)
                    {
                        string[] dataKepada = ListData.NAMA_TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in dataKepada)
                        {
                            no++;
                            res += string.Format(@"
                                <table style='font-size: 13px; font-family: Arial;' width='80%'>
                                        <tr>
                                            <td width='12%' align='left'>" + no + @". " + temp + @" </td>
                                        </tr>
                                </table> ");
                        }
                    }

                    if (ListData.TEMBUSAN_EKSTERNAL != null)
                    {
                        string[] dataKepada = ListData.TEMBUSAN_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in dataKepada)
                        {
                            no++;
                            res += string.Format(@"
                                <table style='font-size: 13px; font-family: Arial;' width='80%'>
                                        <tr>
                                            <td width='12%' align='left'>" + no + @". " + temp + @"</td>
                                        </tr>
                                </table> ");
                        }
                    }
                }
                res += string.Format("</div>");

                string prop_surat = "";
                string directory = null;
                //query prop surat tabel prop_surat where id_surat
                if (ListData.ISI_SURAT != null)
                {
                    //prop_surat = ListData.ISI_SURAT;
                    prop_surat = res;
                }
                else
                {
                    return NotFound();
                }

                //if (ListData.DIRECTORY != null)
                //{
                //    directory = ListData.DIRECTORY;
                //    directory = directory.Replace("https://", "http://");
                //}

                //join pdf
                StringReader sr = new StringReader(prop_surat);
                Document pdfDoc = new Document(PageSize.A4, 5f, 5f, 5f, 0f);
                pdfDoc.SetMargins(15, 15, 10, 100);
                //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                StyleSheet styles = new StyleSheet();
                //styles.LoadStyle("table", "background-color", "#363636");
                //styles.LoadStyle("td", "border-right", "2px solid #363636");
                //styles.LoadStyle("redBigText", "size", "20pt");
                //styles.LoadStyle("redBigText", "color", "#ff0000");

                //styles.LoadStyle("footer", "position", "fixed");
                //styles.LoadStyle("footer", "left", "0");
                //styles.LoadStyle("footer", "bottom", "0");
                //styles.LoadStyle("footer", "width", "100%");
                //styles.LoadStyle("footer", "background-color", "red");
                //styles.LoadStyle("footer", "color", "blue");
                //styles.LoadStyle("footer", "text-align", "center");

                //PdfReader reader = new PdfReader(directory);//alamat attachment
                //htmlparser.SetStyleSheet(styles);
                PdfContentByte pdfCopy = null;
                PdfImportedPage page = null;

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                byte[] htmlResult;
                using (MemoryStream htmlStream = new MemoryStream())
                {
                    Document htmlDoc = new Document(PageSize.A4, 50f, 40f, 20f, 65f);
                    //htmlDoc.SetMargins(30, 30, 30, 55);
                    PdfWriter htmlWriter = PdfWriter.GetInstance(htmlDoc, htmlStream);

                    //footer dan header
                    htmlWriter.PageEvent = new Common.ITextEvents(cabang.BE_NAME, cabang.BE_CITY, cabang.BE_ADDRESS, cabang.POSTAL_CODE, cabang.BE_CONTACT_PERSON);

                    htmlDoc.Open();

                    // instantiate custom tag processor and add to `HtmlPipelineContext`.
                    //var tagProcessorFactory = iTextSharp.tool.xml.html.Tags.GetHtmlTagProcessorFactory();
                    //tagProcessorFactory.AddProcessor(
                    //    new TableDataProcessor(),
                    //    new string[] { HTML.Tag.TD }
                    //);
                    //var htmlPipelineContext = new iTextSharp.tool.xml.pipeline.html.HtmlPipelineContext(null);
                    ////htmlPipelineContext.SetTagFactory(tagProcessorFactory);

                    //var pdfWriterPipeline = new iTextSharp.tool.xml.pipeline.end.PdfWriterPipeline(htmlDoc, htmlWriter);
                    //var htmlPipeline = new iTextSharp.tool.xml.pipeline.html.HtmlPipeline(htmlPipelineContext, pdfWriterPipeline);

                    //// get an ICssResolver and add the custom CSS
                    //var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
                    ////cssResolver.AddCss("", "utf-8", true);
                    //var cssResolverPipeline = new iTextSharp.tool.xml.pipeline.css.CssResolverPipeline(
                    //    cssResolver, htmlPipeline
                    //);

                    //var worker = new XMLWorker(pdfWriterPipeline, true);
                    //var parser = new iTextSharp.tool.xml.parser.XMLParser(worker);
                    //using (var stringReader = new StringReader(prop_surat))
                    //{
                    //    parser.Parse(stringReader);
                    //}


                    //HTMLWorker htmlWorker = new HTMLWorker(htmlDoc);
                    //htmlWorker.SetStyleSheet(styles);
                    XMLWorkerHelper.GetInstance().ParseXHtml(htmlWriter, htmlDoc, sr);
                    //htmlWorker.Parse(sr);


                    htmlDoc.Close();
                    htmlResult = htmlStream.ToArray();
                }

                byte[] pdfResult;
                using (MemoryStream pdfStream = new MemoryStream())
                {
                    Document doc = new Document();
                    PdfCopy copyWriter = new PdfCopy(doc, pdfStream);
                    doc.Open();

                    PdfReader htmlPdfReader = new PdfReader(htmlResult);
                    copyWriter.AddDocument(htmlPdfReader);
                    foreach (var item in ListDataDet)
                    {
                        directory = item.DIRECTORY;
                        //directory = directory.Replace("https://", "http://");
                        System.Net.WebClient wc = new System.Net.WebClient();

                        using (MemoryStream ms = new MemoryStream(wc.DownloadData(directory)))
                        {
                            PdfReader reader = new PdfReader(ms.ToArray());//alamat attachment
                            copyWriter.AddDocument(reader);
                            reader.Close();
                        }
                       
                    }
                    //if (ListData.DIRECTORY != null)
                    //{
                    //    PdfReader reader = new PdfReader(directory);//alamat attachment
                    //    //for (int i = 0; i < reader.NumberOfPages; i++)
                    //    //{
                    //    //    pdfDoc.NewPage();
                    //    //    page = writer.GetImportedPage(reader, i + 1);
                    //    //    pdfCopy.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                    //    //}
                    //    //pdfDoc.NewPage();
                    //    copyWriter.AddDocument(reader);
                    //    reader.Close();
                    //}
                    doc.Close();

                    pdfResult = pdfStream.ToArray();
                    pdfStream.Close();
                    return File(pdfResult, "application/pdf");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NotFound();
        }

        [HttpPost]
        public ActionResult PreviewSuratPdf(DataSurat data)
        {
            try
            {
                dynamic cabang = null;
                var kc = CurrentUser.KodeCabang;
                var urlLogo = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/assets/global/img/pelindo.png";
                var urlTtd = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/assets/global/img/qrcode-remote.png";

                //string body = settingBody();
                string res = "";
                var nomor_surat = "-";

                IDbConnection connection = DatabaseFactory.GetConnection("default"); //---CONN REMOTE DB
                if (connection.State.Equals(ConnectionState.Closed))
                    connection.Open();
                var sql2 = @"select SUBSTR(a.BE_ADDRESS,0, 35) BE_ADDRESS, a.BE_CITY, a.BE_NAME, a.POSTAL_CODE, a.BE_CONTACT_PERSON from BUSINESS_ENTITY a where a.BRANCH_ID =:a ";
                cabang = connection.Query<dynamic>(sql2, new { a = kc }).FirstOrDefault();
                connection.Close();

                if (data.STATUS_APV == 1)
                {
                    nomor_surat = data.NO_SURAT;
                }
                res = string.Format(@"<div style='font-size: 13px; font-family: Arial;'>
                    <div style='text-align:right'>
                        <table width='100%' style='width:100%'>
                            <tbody>
                                <tr>
                                    <td style='width:20%; text-align:left'><img
                                            src= '" + urlLogo + @"' alt='logo' width='200px' height='75px style=''/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                            <div class='header' style='text-align: center' >
                                <h3 style='padding:0px; margin:0px'><b>SURAT</b></h3>
                                Nomor : " + nomor_surat + @"
                            </div>
                    <br />
                
                <table style='font-size: 13px; font-family: Arial; width='96%'>
                    <tr>
                        <td width='10%' align='left'>Kepada : Yth.</td>
                    </tr>
                </table> ");
                int count = 0;
                if (data.NAMA_KEPADA != null)
                {
                    string[] namaKepada = data.NAMA_KEPADA.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    
                    foreach (var temp in namaKepada)
                    {
                        count++;
                        res += string.Format(@"
                        <table style='font-size: 13px; font-family: Arial; width='96%'> 
                            <tr>
			                    <td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + count + @". " + temp + @"</td>
		                    </tr>
                        </table>");
                    };
                }
                if (data.KEPADA_EKSTERNAL != null)
                {
                    string[] kepadaEksternal = data.KEPADA_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var temp in kepadaEksternal)
                    {
                        count++;
                        res += string.Format(@"
                        <table style='font-size: 13px; font-family: Arial; width='96%'> 
                            <tr>
			                    <td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + count + @". " + temp + @"</td>
		                    </tr>
                        </table>");
                    };
                }

                res += string.Format(@"
                <table style='font-size: 13px; font-family: Arial; width='96%'>
		            <tr>
                        <td align='left'>Perihal &nbsp;&nbsp;: " + data.SUBJECT + @"</td>
                    </tr>                            
                </table>
                   <hr />
                &nbsp;&nbsp;&nbsp;&nbsp;" + data.ISI_SURAT.Replace("<br>", "<br />") + @"
                <br /><br />");

                if (data.NAMA_TTD != null)
                {
                    string[] dataTTD = data.NAMA_TTD.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                    if (data.STATUS_APV == 1)
                    {
                        res += string.Format(@"<table style='width:100%; margin-bottom:200px;'>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span>" + data.TANGGAL + @"</span></td>
                        </tr>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><b>" + dataTTD[1] + @"</b></span></td>
                        </tr>
                        <tr line-height='50px'>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><br /><img src='" + urlTtd + @"'width='70px' height='70px'/>
                                                        <br /></span></td>
                        </tr>
                        <tr>
                            <td width='35%' align='center'></td>
                            <td width='35%' align='center'></td>
                            <td width='30%' align='center'><span><br /><b>" + dataTTD[0] + @"</b></span></td>
                        </tr>
                        </table>");
                    }
                    else
                    {
                        res += string.Format(@"<br /><table style='font-size: 13px; font-family: Arial;' width='98%'>
                                        <tr>
                                            <td align='right'>
                                                <span>
                                                    <b>
                                                        " + dataTTD[1] + @"
                                                        <br />
                                                        <br />" + dataTTD[0] + @" 
                                                    </b>						
                                                </span>
                                            </td>
                                        </tr>
                                </table>");
                    }
                }

                if (data.NAMA_TEMBUSAN != null || data.TEMBUSAN_EKSTERNAL != null)
                {
                    int no = 0;
                    res += string.Format(@"<p></p>
                                <table style='font-size: 13px; font-family: Arial;' width='96%'>
                                        <tr>
                                            <td width='15%' align='left'>Tembusan : </td>
                                   <td></td>
                                        </tr>
                                </table>");
                    if(data.NAMA_TEMBUSAN != null)
                    {
                        string[] dataKepada = data.NAMA_TEMBUSAN.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in dataKepada)
                        {
                            no++;
                            res += string.Format(@"
                                <table style='font-size: 13px; font-family: Arial;' width='80%'>
                                        <tr>
                                            <td width='12%' align='left'>" + no + @". " + temp + @" </td>
                                        </tr>
                                </table> ");
                        }
                    }

                    if (data.TEMBUSAN_EKSTERNAL != null)
                    {
                        string[] dataKepada = data.TEMBUSAN_EKSTERNAL.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in dataKepada)
                        {
                            no++;
                            res += string.Format(@"
                                <table style='font-size: 13px; font-family: Arial;' width='80%'>
                                        <tr>
                                            <td width='12%' align='left'>" + no + @". " + temp + @"</td>
                                        </tr>
                                </table> ");
                        }
                    }
                }
                res += string.Format("</div>");

                string prop_surat = "";
                string directory = null;
                //query prop surat tabel prop_surat where id_surat
                if (data.ISI_SURAT != null)
                {
                    //prop_surat = ListData.ISI_SURAT;
                    prop_surat = res;
                }
                else
                {
                    return NotFound();
                }

                if (data.DIRECTORY != null)
                {
                    directory = data.DIRECTORY;
                    //directory = directory.Replace("https://", "http://");
                }

                //join pdf
                StringReader sr = new StringReader(prop_surat);
                Document pdfDoc = new Document(PageSize.A4, 5f, 5f, 5f, 5f);
                pdfDoc.SetMargins(15, 15, 10, 100);
                StyleSheet styles = new StyleSheet();
                PdfContentByte pdfCopy = null;
                PdfImportedPage page = null;

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                byte[] htmlResult;
                using (MemoryStream htmlStream = new MemoryStream())
                {
                    Document htmlDoc = new Document(PageSize.A4, 20f, 20f, 0f, 50f);
                    PdfWriter htmlWriter = PdfWriter.GetInstance(htmlDoc, htmlStream);

                    //footer dan header
                    htmlWriter.PageEvent = new Common.ITextEvents(cabang.BE_NAME, cabang.BE_CITY, cabang.BE_ADDRESS, cabang.POSTAL_CODE, cabang.BE_CONTACT_PERSON);

                    htmlDoc.Open();

                    XMLWorkerHelper.GetInstance().ParseXHtml(htmlWriter, htmlDoc, sr);
                    //htmlWorker.Parse(sr);


                    htmlDoc.Close();
                    htmlResult = htmlStream.ToArray();
                }

                byte[] pdfResult;
                using (MemoryStream pdfStream = new MemoryStream())
                {
                    Document doc = new Document();
                    PdfCopy copyWriter = new PdfCopy(doc, pdfStream);
                    doc.Open();

                    PdfReader htmlPdfReader = new PdfReader(htmlResult);
                    copyWriter.AddDocument(htmlPdfReader);

                    if (data.DIRECTORY != null)
                    {
                        string[] file_temp = directory.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in file_temp)
                        {
                            PdfReader reader = new PdfReader(temp);//alamat attachment

                            copyWriter.AddDocument(reader);
                            reader.Close();
                        }
                    }
                    if(data.LIST_FILE != null)
                    {
                        string[] file_temp = data.LIST_FILE.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var temp in file_temp)
                        {
                            PdfReader reader = new PdfReader(temp);//alamat attachment

                            copyWriter.AddDocument(reader);
                            reader.Close();
                        }
                    }
                    
                    doc.Close();

                    pdfResult = pdfStream.ToArray();
                    pdfStream.Close();
                    return File(pdfResult, "application/pdf");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NotFound();
        }
    }
}