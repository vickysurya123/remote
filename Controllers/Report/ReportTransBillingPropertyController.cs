﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportTransBillingProperty;
using Remote.Models.Select2;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using System.IO;
using ClosedXML.Excel;
//using System.Web.Services;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportTransBillingPropertyController : Controller
    {
        //
        // GET: /ReportTransBillingProperty/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
            //string KodeCabang = CurrentUser.KodeCabang;
            //ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            //var viewModel = new ReportTransBillingProperty
            //{
            //    //DDBusinessEntity = dal.GetDataBE(KodeCabang),
            //    //DDProfitCenter = dal.GetDataProfitCenter(KodeCabang)
            //};
            return View("ReportTransBillingPropertyIndex");
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterTwo(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataConditionType()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            IEnumerable<DDContractType> result = dal.GetDataConditionType(KodeCabang);
            return Json(new { data = result });
        }

        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataFilter()
        {
            DataTableReportTransBillingPropertyTwo result = new DataTableReportTransBillingPropertyTwo();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string sap_doc_no = queryString["sap_doc_no"];
                    string customer_id = queryString["customer_id"];
                    string condition_type = queryString["condition_type"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
                    DataTableReportTransBillingPropertyTwo data = dal.GetDataFilters(draw, start, length, be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result, "application/json");
        }

        //---------------- FILTER DATA NEW -------------------
        public JsonResult GetDataFilterNew()
        {
            DataTableReportTransBillingPropertyTwo result = new DataTableReportTransBillingPropertyTwo();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string sap_doc_no = queryString["sap_doc_no"];
                    string customer_id = queryString["customer_id"];
                    string condition_type = queryString["condition_type"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
                    DataTableReportTransBillingPropertyTwo data = dal.GetDataFiltersnew(draw, start, length, be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_TRANS_BILLING_PROPERTY_TWO> listData = null;
            var queryString = Request.Query;

            /*
              data.profit_center = $('#PROFIT_CENTER').val();
                            data.be_id = $('#BUSINESS_ENTITY').val();
                            data.billing_no = $('#BILLING_NO').val();
                            data.sap_doc_no = $('#SAP_DOC_NO').val();
                            data.condition_type = $('#CONDITION_TYPE').val();
                            data.posting_date_from = $('#POSTING_DATE_FROM').val();
                            data.posting_date_to = $('#POSTING_DATE_TO').val();
                            data.customer_id = $('#CUSTOMER_ID').val();
            */

            string be_id = queryString["be_id"];
            string profit_center = queryString["profit_center"];
            string billing_no = queryString["billing_no"];
            string sap_doc_no = queryString["sap_doc_no"];
            string customer_id = queryString["customer_id"];
            string condition_type = queryString["condition_type"];
            string posting_date_from = queryString["posting_date_from"];
            string posting_date_to = queryString["posting_date_to"];
            string KodeCabang = CurrentUser.KodeCabang;

            //be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, KodeCabang

            ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
            listData = dal.ShowFilterTwo(be_id, profit_center, billing_no, sap_doc_no, customer_id, condition_type, posting_date_from, posting_date_to, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        [HttpPost]
        public JsonResult defineExcel([FromBody] ExcelReportTransBillingPropertyTwo data)
        {
            nama result = new nama();
            string fileName = "ReportTransBillingProperty(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportTransBillingPropertyDAL dal = new ReportTransBillingPropertyDAL();
                var dalShowdata = dal.ShowFilterTwonew(data.BUSINESS_ENTITY, data.PROFIT_CENTER, data.BILLING_NO, data.SAP_DOC_NO, data.CUSTOMER_ID, data.CONDITION_TYPE, data.POSTING_DATE_FROM, data.POSTING_DATE_TO, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[18] { 
                new DataColumn("BILLING NO", typeof(string)),
                new DataColumn("SAP DOC NO", typeof(string)),
                new DataColumn("BUSINESS ENTITY",typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("POSTING DATE",typeof(string)),
                new DataColumn("CREATE POSTING BY",typeof(string)),
                new DataColumn("CUSTOMER NAME", typeof(string)),
                new DataColumn("CUSTOMER AR",typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("CONDITION TYPE",typeof(string)),
                new DataColumn("GL ACCOUNT",typeof(string)),
                new DataColumn("CONDITION TYPE DESCRIPTION",typeof(string)),
                new DataColumn("CONTRACT VALID FROM",typeof(string)),
                new DataColumn("CONTRACT VALID TO",typeof(string)),
                new DataColumn("LUAS M2",typeof(string)),
                new DataColumn("OBJECT_ID", typeof(string)),
                new DataColumn("CURRENCY",typeof(string)),
                new DataColumn("AMOUNT", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BILLING_NO, valData.SAP_DOC_NO, valData.BUSINESS_ENTITY,
                                valData.PROFIT_CENTER, valData.POSTING_DATE, valData.CREATION_BY, valData.BUSINESS_PARTNER_NAME,
                                valData.CUSTOMER_AR, valData.BILLING_CONTRACT_NO, valData.CONDITION_TYPE,
                                valData.GL_ACOUNT_NO, valData.CONDITION_TYPE_DESC, valData.VALID_FROM, valData.VALID_TO, valData.LUAS,
                                valData.OBJECT_ID, valData.CURRENCY, valData.AMOUNT);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Trans Billing Property");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        //[WebMethod]
        //public void deleteExcel(string fileName)
        //{
        //    var fleName = Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName);
        //    if (Directory.Exists(Path.GetDirectoryName(fleName)))
        //    {
        //        System.IO.File.Delete(fleName);
        //    }
        //}

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}