﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportTransOtherService;
using Remote.Models.Select2;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
//using System.Web.Services;
using ClosedXML.Excel;
using System.IO;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportTransOtherServiceController : Controller
    {
        //
        // GET: /ReportTransOtherService/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;

            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            var viewModel = new ReportTransOtherService
            {
                DDBusinessEntity = dal.GetDataBEnew(KodeCabang, UserRole),
                DDProfitCenter = dal.GetDataProfitCenternew(KodeCabang, UserRole),
                DDServiceGroup = dal.GetDataService(),
            };
            return View("ReportTransOtherServiceIndex", viewModel);
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterTwo(KodeCabang);
            return Json(new { data = result });
        }

        //------------------------------- BUTTON SHOW ALL ------------------
        public JsonResult ShowAll()
        {
            DataTableReportTransOtherService result = new DataTableReportTransOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;


                    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
                    result = dal.ShowAll(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult ShowAllTwo()
        {
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listData = null;
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            listData = dal.ShowAllTwo(KodeCabang);
            return Json(new { data = listData });
        }

        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_TRANS_OTHER_SERVICES> listData = null;
            var queryString = Request.Query;

            string be_id = queryString["be_id"];
            string profit_center = queryString["profit_center"];
            string billing_no = queryString["billing_no"];
            string sap_no = queryString["sap_no"];
            string customer_id = queryString["customer_id"];
            string service_group = queryString["service_group"];
            string posting_date_from = queryString["posting_date_from"];
            string posting_date_to = queryString["posting_date_to"];
            string search = queryString["search[value]"];

            string KodeCabang = CurrentUser.KodeCabang;
            string UserRole = CurrentUser.UserRoleID;
            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            listData = dal.ShowFilterTwo(be_id, profit_center, billing_no, sap_no, customer_id, service_group, posting_date_from, posting_date_to, KodeCabang, UserRole);
            return Json(new { data = listData });
        }

        //-------------------------------- BUTTON FILTER -------------------
        public JsonResult GetDataFilter()
        {
            DataTableReportTransOtherService result = new DataTableReportTransOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string sap_no = queryString["sap_no"];
                    string customer_id = queryString["customer_id"];
                    string service_group = queryString["service_group"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
                    DataTableReportTransOtherService data = dal.GetDataFilter(draw, start, length, be_id, profit_center, billing_no, sap_no, customer_id, service_group, posting_date_from, posting_date_to, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult GetDataFilterTwo()
        {
            DataTableReportTransOtherService result = new DataTableReportTransOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string sap_no = queryString["sap_no"];
                    string customer_id = queryString["customer_id"];
                    string service_group = queryString["service_group"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
                    DataTableReportTransOtherService data = dal.GetDataFilterTwo(draw, start, length, be_id, profit_center, billing_no, sap_no, customer_id, service_group, posting_date_from, posting_date_to, search, KodeCabang, UserRole);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //----------------------- filter new -----------------------------
        public JsonResult GetDataFilterTwoNew()
        {
            DataTableReportTransOtherService result = new DataTableReportTransOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string billing_no = queryString["billing_no"];
                    string sap_no = queryString["sap_no"];
                    string customer_id = queryString["customer_id"];
                    string service_group = queryString["service_group"];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
                    DataTableReportTransOtherService data = dal.GetDataFilterTwonew(draw, start, length, be_id, profit_center, billing_no, sap_no, customer_id, service_group, posting_date_from, posting_date_to, search, KodeCabang, UserRole);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
            var xcustomer = dal.DDCustomer(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportTransOtherServiceParam param)
        {
            nama result = new nama();
            string fileName = "ReportTransOtherServices(" + DateTime.Now.ToString("yyyyMMddHHmmss") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                string UserRole = CurrentUser.UserRoleID;
                ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.BUSINESS_ENTITY, param.PROFIT_CENTER, param.BILLING_NO, param.SAP_NO, param.CUSTOMER_ID, param.SERVICE_GROUP_ID, param.POSTING_DATE_FROM, param.POSTING_DATE_TO, KodeCabang, UserRole);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[17] {
                new DataColumn("BE NAME", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("BILLING NO", typeof(string)),
                new DataColumn("POSTING DATE",typeof(string)),
                new DataColumn("CUSTOMER MDM", typeof(string)),
                new DataColumn("CUSTOMER SAP",typeof(string)),
                new DataColumn("SERVICE GROUP NAME", typeof(string)),
                new DataColumn("SERVICE NAME",typeof(string)),
                new DataColumn("QTY", typeof(string)),
                new DataColumn("UNIT",typeof(string)),
                new DataColumn("PRICE", typeof(string)),
                new DataColumn("CURRENCY",typeof(string)),
                new DataColumn("MULTIPLY FACTOR %",typeof(string)),
                new DataColumn("SURCHARGE", typeof(string)),
                new DataColumn("TOTAL AMOUNT",typeof(string)),
                new DataColumn("DOC SAP NO",typeof(string)),
                new DataColumn("STATUS",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE_NAME, valData.PROFIT_CENTER_NAME, valData.ID, valData.POSTING_DATE,
                                    valData.COSTUMER_MDM_NAME, valData.CUSTOMER_SAP_AR, valData.SERVICES_GROUP_NAME,
                                    valData.SERVICE_NAME, valData.QUANTITY, valData.UNIT,
                                    valData.PRICE, valData.CURRENCY, valData.MULTIPLY_FACTOR, valData.SURCHARGE,
                                    valData.AMOUNT, valData.SAP_DOCUMENT_NUMBER, "BILLED");
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportTransOtherServices");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}

        ////----------------------------------------- SELECT2 BE -------------------
        //public JsonResult getBusinessEntity(string p)
        //{
        //    IEnumerable<VW_BUSINESS_ENTITY> result = null;
        //    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
        //    result = dal.getBusinessEntity(p);
        //    return Json(new { items = result });
        //}

        ////------------------------------------- SELECT2 PROFIT CENTER -----------------------------
        //public JsonResult getProfitCenter(string p)
        //{
        //    IEnumerable<VW_PROFIT_CENTER> result = null;
        //    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
        //    result = dal.getProfitCenter(p);
        //    return Json(new { items = result });
        //}

        ////-------------------------------------- SELECT2 SERVICE GROUP --------------------------
        //public JsonResult getServiceGroup(string p)
        //{
        //    IEnumerable<VW_SERVICE_GROUP> result = null;
        //    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
        //    result = dal.getServiceGroup(p);
        //    return Json(new { items = result });
        //}

        ////--------------------------------------- SELECT2 CUSTOMER MDM ------------------------------
        //public JsonResult getCustomerMDM(string p)
        //{
        //    IEnumerable<VW_CUSTOMER_MDM> result = null;
        //    ReportTransOtherServiceDAL dal = new ReportTransOtherServiceDAL();
        //    result = dal.getCustomerMDM(p);
        //    return Json(new { items = result });
        //}

    }
}