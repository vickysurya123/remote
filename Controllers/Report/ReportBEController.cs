﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.MasterBusinessEntity;
using Remote.ViewModels;
using System.Data;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportBEController : Controller
    {
        //
        // GET: /ReportBE/
        public ActionResult Index()
        {
            //string KodeCabang = CurrentUser.KodeCabang;
            //ReportBEDAL dal = new ReportBEDAL();
            //var viewModel = new BusinessEntity
            //{
            //    DDBusinessEntity = dal.GetDataBE(KodeCabang),
            //    //DDProfitCenter = dal.GetDataProfitCenter(KodeCabang),
            //    //DDServiceGroup = dal.GetDataService(),
            //};
            return View("ReportBEIndex");
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetDataBE()
        {
            ReportBEDAL dal = new ReportBEDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBE();
            return Json(new { data = result });
        }
        public JsonResult GetDataHarbourClass()
        {
            ReportBEDAL dal = new ReportBEDAL();
            IEnumerable<DDHarbourClass> result = dal.GetDataHarbourClass();
            return Json(new { data = result });
        }
        public JsonResult getBEID(string p)
        {
            IEnumerable<VW_BUSINESS_ENTITY> result = null;
            ReportBEDAL dal = new ReportBEDAL();
            result = dal.getBEID(p);
            return Json(new { items = result });
        }
        public JsonResult getHARBOURClASS(string p)
        {
            IEnumerable<AUP_BE_WEBAPPS> result = null;
            ReportBEDAL dal = new ReportBEDAL();
            result = dal.getHARBOURClASS(p);
            return Json(new { items = result });
        }
        public JsonResult ShowAll()
        {
            DataTableReportBE result = new DataTableReportBE();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportBEDAL dal = new ReportBEDAL();
                    result = dal.ShowAll(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult ShowAllTwo()
        {
            IEnumerable<AUP_REPORT_BE> listData = null;
            string KodeCabang = CurrentUser.KodeCabang;
            ReportBEDAL dal = new ReportBEDAL();
            listData = dal.ShowAllTwo(KodeCabang);
            return Json(new { data = listData });
        }

        public JsonResult GetDataFilter()
        {
            DataTableReportBE result = new DataTableReportBE();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be = queryString["be"];
                    string harbour_class = queryString["harbour_class"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportBEDAL dal = new ReportBEDAL();
                    DataTableReportBE data = dal.GetDataFilter(draw, start, length, be, harbour_class, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result, "application/json");
        }

        //----------------------- NEW FILTER -------------------------------
        public JsonResult GetDataFilterNew()
        {
            DataTableReportBE result = new DataTableReportBE();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be = queryString["be"];
                    string harbour_class = queryString["harbour_class"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportBEDAL dal = new ReportBEDAL();
                    DataTableReportBE data = dal.GetDataFilternew(draw, start, length, be, harbour_class, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }


        public JsonResult GetDataFilterTwo()
        {
            var queryString = Request.Query;
            IEnumerable<AUP_REPORT_BE> listData = null;
            string KodeCabang = CurrentUser.KodeCabang;
            string be = queryString["be"];
            string harbour_class = queryString["harbour_class"];

            ReportBEDAL dal = new ReportBEDAL();
            listData = dal.GetDataFilterTwo(be, harbour_class, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        public JsonResult defineExcel(string BUSINESS_ENTITY, string HARBOUR_CLASS)
        {
            nama result = new nama();
            string fileName = "ReportBE(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";
            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportBEDAL dal = new ReportBEDAL();
                var dalShowdata = dal.GetDataFilterTwo(BUSINESS_ENTITY, HARBOUR_CLASS, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[12] {
                new DataColumn("BUSINESS ENTITY", typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("POSTAL CODE", typeof(string)),
                new DataColumn("CITY",typeof(string)),
                new DataColumn("PROVINCE", typeof(string)),
                new DataColumn("AREA",typeof(string)),
                new DataColumn("HARBOUR CLASS", typeof(string)),
                new DataColumn("TELEPHONE",typeof(string)),
                new DataColumn("FAX", typeof(string)),
                new DataColumn("EMAIL",typeof(string)),
                new DataColumn("VALID FROM", typeof(string)),
                new DataColumn("VALID TO",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE, valData.BE_ADDRESS, valData.POSTAL_CODE, valData.BE_CITY,
                                valData.BE_PROVINCE, valData.AMOUNT, valData.HARBOUR_CLASS,
                                valData.PHONE, valData.FAX, valData.EMAIL, valData.VAL_FROM, valData.VAL_TO);
                }

                //Codes for the Closed XML

                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "ReportBE");
                //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}

            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        //--------------------------- NEW EXCEL ------------------------------
        public JsonResult defineExcelNew(string BUSINESS_ENTITY, string HARBOUR_CLASS)
        {
            nama result = new nama();
            string fileName = "ReportBE(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";
            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportBEDAL dal = new ReportBEDAL();
                var dalShowdata = dal.GetDataFilterTwonew(BUSINESS_ENTITY, HARBOUR_CLASS, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[12] {
                new DataColumn("BUSINESS ENTITY", typeof(string)),
                new DataColumn("ADDRESS", typeof(string)),
                new DataColumn("POSTAL CODE", typeof(string)),
                new DataColumn("CITY",typeof(string)),
                new DataColumn("PROVINCE", typeof(string)),
                new DataColumn("AREA",typeof(string)),
                new DataColumn("HARBOUR CLASS", typeof(string)),
                new DataColumn("TELEPHONE",typeof(string)),
                new DataColumn("FAX", typeof(string)),
                new DataColumn("EMAIL",typeof(string)),
                new DataColumn("VALID FROM", typeof(string)),
                new DataColumn("VALID TO",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE, valData.BE_ADDRESS, valData.POSTAL_CODE, valData.BE_CITY,
                                valData.BE_PROVINCE, valData.AMOUNT, valData.HARBOUR_CLASS,
                                valData.PHONE, valData.FAX, valData.EMAIL, valData.VAL_FROM, valData.VAL_TO);
                }

                //Codes for the Closed XML

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportBE");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));

                }

            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }
        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}