﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportROThirdPartyUsed;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportROThirdPartyUsedController : Controller
    {
        //
        // GET: /ReportROThirdPartyUsed/
        public ActionResult Index()
        {
            return View("ReportROThirdPartyUsed");
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetDataBE()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBE(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataBENew()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBEnew(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataUsageType()
        {
            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            IEnumerable<DDZoneRip> result = dal.GetDataUsageType();
            return Json(new { data = result });
        }
        public JsonResult GetDataZONE()
        {
            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            IEnumerable<DDZoneRip> result = dal.GetDataZONE();
            return Json(new { data = result });
        }

        public JsonResult GetDataContractUsage()
        {
            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            IEnumerable<DDContractUsage> result = dal.GetDataContractUsage();
            return Json(new { data = result });
        }

        public JsonResult GetFilterTwo()
        {
            DataTablesReportROThirdPartyUsed result = new DataTablesReportROThirdPartyUsed();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string val_years = queryString["val_years"];
                    string usage_type = queryString["usage_type"];
                    string val_zone = queryString["val_zone"];
                    string val_contract_usage = queryString["val_contract_usage"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
                    DataTablesReportROThirdPartyUsed data = dal.GetFilterTwo(draw, start, length, be_id, val_years, usage_type, val_zone, val_contract_usage, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult GetFilterTwoNew()
        {
            DataTablesReportROThirdPartyUsed result = new DataTablesReportROThirdPartyUsed();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string val_years = queryString["val_years"];
                    string usage_type = queryString["usage_type"];
                    string val_zone = queryString["val_zone"];
                    string val_contract_usage = queryString["val_contract_usage"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
                    DataTablesReportROThirdPartyUsed data = dal.GetFilterTwonew(draw, start, length, be_id, val_years, usage_type, val_zone, val_contract_usage, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }


        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_RO_THIRD_PARTY_USED> listData = null;
            var queryString = Request.Query;

            string be_id = queryString["be_id"];
            string val_years = queryString["val_years"];
            string usage_type = queryString["usage_type"];
            string val_zone = queryString["val_zone"];
            string val_status = queryString["val_status"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
            listData = dal.ShowFilterTwo(be_id, val_years, usage_type, val_zone, val_status, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportROThirdPartyUsedParam param)
        {
            nama result = new nama();
            string fileName = "ReportROThirdPartyUsed(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportROThirdPartyUsedDAL dal = new ReportROThirdPartyUsedDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.BUSINESS_ENTITY, param.YEARS, param.USAGE_TYPE, param.ZONE, param.CONTRACT_USAGE, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[28] { 
                new DataColumn("BUSINESS ENTITY", typeof(string)),
                new DataColumn("YEAR", typeof(string)),
                new DataColumn("USAGE TYPE",typeof(string)),
                new DataColumn("CUSTOMER NAME", typeof(string)),
                new DataColumn("KELOMPOK PENGGUNA",typeof(string)),
                new DataColumn("RO ADDRESS", typeof(string)),
                new DataColumn("MEASUREMENT (M2)",typeof(string)),
                new DataColumn("CONTRACT USAGE", typeof(string)),
                new DataColumn("ZONE RIP/DRAFT RIP", typeof(string)),
                new DataColumn("CERTIFICATE NUMBER HPL",typeof(string)),
                new DataColumn("LEGAL CONTRACT NUMBER",typeof(string)),
                new DataColumn("CONTRACT START DATE", typeof(string)),
                new DataColumn("CONTRACT END DATE",typeof(string)),
                new DataColumn("TERM IN MONTH",typeof(string)),
                new DataColumn("CONDITION TYPE",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z003-SEWADARATAN)", typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z003-SEWADARATAN)",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z005-SEWABANGUNAN)",typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z005-SEWABANGUNAN)",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z007-BIAYAPENGALIHAN)",typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z007-BIAYAPENGALIHAN)",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z008-BIAYAANGGUNAN)",typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z008-BIAYAANGGUNAN)",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z013-TROUGHTPUTFEE)",typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z013-TROUGHTPUTFEE)",typeof(string)),
                new DataColumn("INCOME PER MONTH(Z014-LUMPSUMDEPO)",typeof(string)),
                new DataColumn("TOTAL REVENUE CONTRACT(Z014-LUMPSUMDEPO)",typeof(string)),
                new DataColumn("INSTALLMENT AMOUNT",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE_NAME, valData.VALID_FROM, valData.USAGE_TYPES,
                                valData.CUSTOMER_NAME, valData.KELOMPOK_USAHA, valData.RO_ADDRESS,
                                valData.LUAS, valData.CONTRACT_USAGES, valData.ZONA_RIPS,
                                valData.RO_CERTIFICATE_NUMBER, valData.LEGAL_CONTRACT_NO,
                                valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE,
                                valData.TERM_IN_MONTHS, valData.CONDITION_TYPE, 
                                valData.JENIS_TARIF_Z003, valData.INSTALLMENT_AMOUNT_Z003,
                                valData.JENIS_TARIF_Z005, valData.INSTALLMENT_AMOUNT_Z005,
                                valData.JENIS_TARIF_Z007, valData.INSTALLMENT_AMOUNT_Z007,
                                valData.JENIS_TARIF_Z008, valData.INSTALLMENT_AMOUNT_Z008,
                                valData.JENIS_TARIF_Z013, valData.INSTALLMENT_AMOUNT_Z013,
                                valData.JENIS_TARIF_Z014, valData.INSTALLMENT_AMOUNT_Z014,
                                valData.INSTALLMENT_AMOUNT);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportROThirdPartyUsed");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }


        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}