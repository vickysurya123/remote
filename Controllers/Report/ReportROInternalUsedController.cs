﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Remote.DAL;
using Remote.Entities;
using Remote.Models; 
using Remote.Models.ReportROInternalUsed;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportROInternalUsedController : Controller
    {
        //
        // GET: /ReportSelfUsage/
        public ActionResult Index()
        {
            return View("ReportROInternalUsed");
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetDataBE()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBE(KodeCabang);
            return Json(new { data = result });
        }
        public JsonResult GetDataZONE()
        {
            ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
            IEnumerable<DDZoneRip> result = dal.GetDataZONE();
            return Json(new { data = result });
        }
        public JsonResult GetDataUsageType()
        {
            ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
            IEnumerable<DDZoneRip> result = dal.GetDataUsageType();
            return Json(new { data = result });
        }

        public JsonResult GetFilterTwo()
        {
            DataTablesReportROInternalUsed result = new DataTablesReportROInternalUsed();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string val_years = queryString["val_years"];
                    string usage_type = queryString["usage_type"];
                    string val_zone = queryString["val_zone"];
                    string val_status = queryString["val_status"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
                    DataTablesReportROInternalUsed data = dal.GetFilterTwo(draw, start, length, be_id, val_years, usage_type, val_zone, val_status, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //----------------------- NEW FILTER ----------------------
        public JsonResult GetFilterTwoNew()
        {
            DataTablesReportROInternalUsed result = new DataTablesReportROInternalUsed();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string val_years = queryString["val_years"];
                    string usage_type = queryString["usage_type"];
                    string val_zone = queryString["val_zone"];
                    string val_status = queryString["val_status"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
                    DataTablesReportROInternalUsed data = dal.GetFilterTwonew(draw, start, length, be_id, val_years, usage_type, val_zone, val_status, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }


        public JsonResult ShowFilterTwo()
        {
            IEnumerable<AUP_REPORT_RO_INTERNAL_USED> listData = null;
            var queryString = Request.Query;

            string be_id = queryString["be_id"];
            string val_years = queryString["val_years"];
            string usage_type = queryString["usage_type"];
            string val_zone = queryString["val_zone"];
            string val_status = queryString["val_status"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
            listData = dal.ShowFilterTwo(be_id, val_years, usage_type, val_zone, val_status, KodeCabang);
            return Json(new { data = listData });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportROInternalUsedParam param)
        {
            nama result = new nama();
            string fileName = "ReportROInternalused(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportROInternalUsedDAL dal = new ReportROInternalUsedDAL();
                var dalShowdata = dal.ShowFilterTwonew(param.BUSINESS_ENTITY, param.YEARS, param.USAGE_TYPE, param.ZONE, param.STATUS, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[11] { 
                new DataColumn("RO CODE", typeof(string)),
                new DataColumn("RO NAME", typeof(string)),
                new DataColumn("DESCRIPTION BE",typeof(string)),
                new DataColumn("YEAR", typeof(string)),
                new DataColumn("OBJECT USED DESCRIPTION",typeof(string)),
                new DataColumn("TYPE RENTAL OBJECT (TANAH/BANGUNAN)", typeof(string)),
                new DataColumn("PERSIL ADDRESS",typeof(string)),
                new DataColumn("MEASUREMENT (M2)",typeof(string)),
                new DataColumn("FUNCTION", typeof(string)),
                new DataColumn("ZONE RIP/DRAFT RIP", typeof(string)),
                new DataColumn("CERTIFICATE NUMBER HPL",typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.RO_CODE, valData.RO_NAME, valData.BE_NAME,
                                valData.CREATION_DATE, valData.REASON, valData.OBJ,
                                valData.RO_ADDRESS, valData.AMOUNT, valData.USAGES,
                                valData.ZONE_RIP, valData.RO_CERTIFICATE_NUMBER);
                }

                //Codes for the Closed XML

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "ReportROInternalused");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }


        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
	}
}