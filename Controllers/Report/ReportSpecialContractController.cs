﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.Models;
using Remote.Models.ReportSpecialContract;
using System.Data;
using System.Security.Claims;
using Remote.Entities;
using Remote.DAL;
using System.IO;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportSpecialContractController : Controller
    {
        //
        // GET: /ReportGeneralContract/
        public ActionResult Index()
        {
            return View("ReportSpecialContractIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
            IEnumerable<DDBusinessEntity> result = dal.GetDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult GetDataProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        [HttpPost]
        public ActionResult Customer([FromBody] string BUSINESS_PARTNER_NAME)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
            var xcustomer = dal.DDCustomer(BUSINESS_PARTNER_NAME, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult ShowFilter()
        {
            DataTablesReportSpecialContract result = new DataTablesReportSpecialContract();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string business_partner = queryString["business_partner"];
                    string contract_no = queryString["contract_no"];
                    string contract_status = queryString["contract_status"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
                    DataTablesReportSpecialContract data = dal.ShowFilter(draw, start, length, be_id, profit_center, business_partner, contract_no, contract_status, valid_from, valid_to, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //------------------ new filter ---------------------
        public JsonResult ShowFilterNew()
        {
            DataTablesReportSpecialContract result = new DataTablesReportSpecialContract();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string business_partner = queryString["business_partner"];
                    string contract_no = queryString["contract_no"];
                    string contract_status = queryString["contract_status"];
                    string valid_from = queryString["valid_from"];
                    string valid_to = queryString["valid_to"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
                    DataTablesReportSpecialContract data = dal.ShowFilternew(draw, start, length, be_id, profit_center, business_partner, contract_no, contract_status, valid_from, valid_to, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }


        [HttpPost]
        public JsonResult defineExcelReportSpecialContract([FromBody] ExcelReportSpecialContract data)
        {
            nama result = new nama();
            string fileName = "ReportGeneralContract(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportSpecialContractDAL dal = new ReportSpecialContractDAL();
                var dalShowdata = dal.GenerateExcelReportSpecialContractnew(data.BUSINESS_ENTITY, data.PROFIT_CENTER, data.BUSINESS_PARTNER, data.CONTRACT_NO, data.CONTRACT_STATUS, data.VALID_FROM, data.VALID_TO, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[21] { 
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("CONTRACT TYPE", typeof(string)),
                new DataColumn("CREATE BY",typeof(string)),
                new DataColumn("CONTRACT NAME", typeof(string)),
                new DataColumn("CONDITION TYPE",typeof(string)),
                new DataColumn("START DATE", typeof(string)),
                new DataColumn("END DATE",typeof(string)),
                new DataColumn("CUSTOMER NAME", typeof(string)),
                new DataColumn("CUSTOMER AR",typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CONTRACT USAGE",typeof(string)),
                new DataColumn("CURRENCY", typeof(string)),
                new DataColumn("IJIN PRINSIP NO",typeof(string)),
                new DataColumn("LEGAL CONTRACT NO", typeof(string)),
                new DataColumn("OBJECT ID",typeof(string)),
                new DataColumn("OBJECT NAME", typeof(string)),
                new DataColumn("INDUSTRY TYPE",typeof(string)),
                new DataColumn("LAND DIMENSION (m2)",typeof(string)),
                new DataColumn("BUILDING DIMENSION (m2)",typeof(string)),
                new DataColumn("NILAI NOMINAL",typeof(string)),
                new DataColumn("CONTRACT STATUS",typeof(string))});

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.CONTRACT_NO, valData.CONTRACT_TYPE, valData.CREATION_BY,
                                valData.CONDITION_TYPE, valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE,
                                valData.BUSINESS_PARTNER_NAME, valData.CUSTOMER_AR, valData.PROFIT_CENTER,
                                valData.CONTRACT_USAGE, valData.CURRENCY, valData.IJIN_PRINSIP_NO,
                                valData.LEGAL_CONTRACT_NO, valData.OBJECT_ID, valData.OBJECT_NAME,
                                valData.INDUSTRY, valData.LUAS_TANAH, valData.LUAS_BANGUNAN,
                                valData.TOTAL_NET_VALUE, valData.CONTRACT_STATUS);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Special Contract");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    string path1 = "~/REMOTE/Excel/";
                //    wb.Worksheets.Add(dt, "Special Contract");
                //    //wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    wb.SaveAs(Path.Combine(path1 + fileName));
                //    HttpContext.Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    HttpContext.Response.AppendTrailer("content-disposition", "attachment; filename=" + fileName);
                //    //Response.Flush();
                //    //Response.End();
                //}
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }
}