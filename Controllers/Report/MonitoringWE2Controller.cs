﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Remote.DAL;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using Remote.ViewModels;

namespace Remote.Controllers.Report
{
    [Authorize]
    public class MonitoringWE2Controller : Controller
    {
        // GET: MonitoringWE
        public ActionResult Index()
        {
            string bulansekarang = "";
            string bulankemarin = "";
            string bulankemarin1 = "";
            string bulankemarin2 = "";
            string bulankemarin3 = "";
            string bulankemarin4 = "";
            string bulankemarin5 = "";
            string bulankemarin6 = "";
            string bulankemarin7 = "";
            string bulankemarin8 = "";
            string bulankemarin9 = "";
            string bulankemarin10 = "";
            string bulankemarin11 = "";

            float hitung = 0;
            //bulansekarang = DateTime.Now.ToString("MM.yyyy");
            bulankemarin = DateTime.Now.ToString("yyyy");
            bulankemarin1 = DateTime.Now.AddYears(-1).ToString("yyyy");
            bulankemarin2 = DateTime.Now.AddYears(-2).ToString("yyyy");
            bulankemarin3 = DateTime.Now.AddYears(-3).ToString("yyyy");
            bulankemarin4 = DateTime.Now.AddYears(-4).ToString("yyyy");
            bulankemarin5 = DateTime.Now.AddYears(-5).ToString("yyyy");
            //bulankemarin6 = DateTime.Now.AddMonths(-7).ToString("MM.yyyy");
            //bulankemarin7 = DateTime.Now.AddMonths(-8).ToString("MM.yyyy");
            //bulankemarin8 = DateTime.Now.AddMonths(-9).ToString("MM.yyyy");
            //bulankemarin9 = DateTime.Now.AddMonths(-10).ToString("MM.yyyy");
            //bulankemarin10 = DateTime.Now.AddMonths(-11).ToString("MM.yyyy");

            string KodeCabang = CurrentUser.KodeCabang;
            string NamaCabang = CurrentUser.NamaCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            ViewBag.NamaCabang = NamaCabang;
            ViewBag.BULAN_KEMARIN = bulankemarin;
            ViewBag.BULAN_KEMARIN1 = bulankemarin1;
            ViewBag.BULAN_KEMARIN2 = bulankemarin2;
            ViewBag.BULAN_KEMARIN3 = bulankemarin3;
            ViewBag.BULAN_KEMARIN4 = bulankemarin4;
            ViewBag.BULAN_KEMARIN5 = bulankemarin5;
            //ViewBag.BULAN_KEMARIN6 = bulankemarin6;
            //ViewBag.BULAN_KEMARIN7 = bulankemarin7;
            //ViewBag.BULAN_KEMARIN8 = bulankemarin8;
            //ViewBag.BULAN_KEMARIN9 = bulankemarin9;
            //ViewBag.BULAN_KEMARIN10 = bulankemarin10;
            //ViewBag.BULAN_KEMARIN11 = bulankemarin11;
            //ViewBag.BULAN_SEKARANG = bulansekarang;
            return View("Index", viewModel);

        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User);
            }
        }


        public JsonResult GetDataFilter()
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be = queryString["be"];
                    string periode = queryString["periode"];
                    string tipe_trans = queryString["tipe_trans"];
                    string status = queryString["status"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MonitoringWEDAL dal = new MonitoringWEDAL();
                    IDataTable data = dal.GetDataFilter(draw, start, length, be, periode, tipe_trans, status, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }



        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        [HttpPost]
        public JsonResult defineExcel([FromBody] MonitoringWEParam param)
        {
            nama result = new nama();
            MonitoringWEDAL dal = new MonitoringWEDAL();
            string KodeCabang = CurrentUser.KodeCabang;
            string namaCabang = "";

            var kolomName = param.PERIOD;
            var jenis = (param.TYPE_TRANSACTION == "AIR" ? "AIR" : param.TYPE_TRANSACTION);

            if (param.BE_NAME != null && param.BE_NAME != "")
            {
                //kolomName = jenis;
                namaCabang = dal.GetNamaCabang(param.BE_NAME);
                kolomName += "_" + namaCabang;
            }
            else
            {
                namaCabang = dal.GetNamaCabang(KodeCabang);
                kolomName += "_" + namaCabang;
            }
            if (param.TYPE_TRANSACTION != null && param.TYPE_TRANSACTION != "")
            {
                //kolomName = jenis;
                kolomName += "_" + param.TYPE_TRANSACTION;
            }
            if (param.STATUS_NOW != null && param.STATUS_NOW != "")
            {
                //kolomName = jenis;
                kolomName += "_" + param.STATUS_NOW;
            }

            string fileName = "MonitoringTransaksiWE(" + kolomName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";
            try
            {


                var dalShowdata = dal.GetDetailExcel(param.BE_NAME, param.TYPE_TRANSACTION, param.PERIOD, param.STATUS_NOW, KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {
                new DataColumn("CABANG", typeof(string)),
                new DataColumn("NO INSTALLATION", typeof(string)),
                new DataColumn("INSTALLATION TYPE", typeof(string)),
                new DataColumn("PERIODE", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CUSTOMER NAME",typeof(string)),
                new DataColumn("POWER CAPACITY", typeof(string)),
                new DataColumn("INSTALLATION ADDRESS",typeof(string)),
                new DataColumn("POSTING DATE", typeof(string)),
                new DataColumn("POSTING STATUS",typeof(string)) });
                foreach (var valData in dalShowdata)
                {
                    var temp = (valData.STATUS_NOW == "Entried" ? " - " + valData.STATUS_POSTING : "");
                    dt.Rows.Add(valData.BE_NAME, valData.INSTALLATION_CODE, valData.INSTALLATION_TYPE, valData.PERIOD,
                                valData.PROFIT_CENTER, valData.CUSTOMER_NAME,
                                valData.POWER_CAPACITY, valData.INSTALLATION_ADDRESS, valData.POSTING_DATE,
                                valData.STATUS_NOW + temp);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "MonitoringWE");
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }
    }
}
