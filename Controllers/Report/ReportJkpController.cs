﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Remote.Models;
using Remote.Models.ReportJkp;
using Remote.ViewModels;

namespace Remote.Controllers.Report
{
    [Authorize]
    public class ReportJkpController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            var viewModel = new ReportTransWater
            {
                DDBusinessEntity = dal.GetDataBEnew(KodeCabang),
                DDProfitCenter = dal.GetDataProfitCenternew(KodeCabang)
            };
            return View("ReportJkpIndex", viewModel);
        }


        //------------------- AUTOCOMPLETE DATA CUSTOMER ---------------------
        [HttpPost]
        public ActionResult Customer([FromBody] string MPLG_NAMA)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            var xcustomer = dal.DDCustomernew(MPLG_NAMA, KodeCabang);
            return Json(xcustomer);
        }

        public JsonResult GetDataProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportTransWaterDAL dal = new ReportTransWaterDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenternew(KodeCabang);
            return Json(new { data = result });
        }


        //------------------------------- filter new ---------------------------------------
        public JsonResult ShowFilterNew()
        {
            DataTableReportJkp result = new DataTableReportJkp();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);

                    string period = queryString["period"];
                    string[] dataPeriod = period.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                    string bulan = dataPeriod[0];
                    string year = dataPeriod[1];
                    string posting_date_from = queryString["posting_date_from"];
                    string posting_date_to = queryString["posting_date_to"];
                    string search = queryString["search[value]"];
                    string branchID = queryString["branchID"];
                    string profitCenter = queryString["profitCenter"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportJkpDAL dal = new ReportJkpDAL();
                    DataTableReportJkp data = dal.GetDataFilternew(draw, start, length, bulan, year, posting_date_from, posting_date_to, KodeCabang, branchID, profitCenter);

                    result = data;
                }
                catch (Exception ex)
                {
                    var aa = ex.ToString();
                }
            }

            return Json(result);
        }

        //[WebMethod]
        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportTransWaterParam param)
        {
            nama result = new nama();
            string fileName = "ReportJkp(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                string period = param.PERIOD;
                string[] dataPeriod = period.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                string bulan = dataPeriod[0];
                string year = dataPeriod[1];
                ReportJkpDAL dal = new ReportJkpDAL();
                var dalShowdata = dal.ShowFilterTwonew(bulan, year, KodeCabang, param.branchID, param.profitCenter);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[18] {
                new DataColumn("NO", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("CUSTOMER", typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("VALUE CONTRACT", typeof(string)),
                new DataColumn("COA", typeof(string)),
                new DataColumn("VALUE PER MONTH", typeof(string)),
                new DataColumn("CONTRACT START",typeof(string)),
                new DataColumn("CONTRACT END", typeof(string)),
                new DataColumn("MASA SEWA",typeof(string)),
                new DataColumn("ACCRUED REVENUE(PREV PERIODE)", typeof(string)),
                new DataColumn("PAID REVENUE(PREV PERIODE)",typeof(string)),
                new DataColumn("ACCRUED REVENUE(CURRENT YEAR)", typeof(string)),
                new DataColumn("PAID REVENUE(CURRENT YEAR)",typeof(string)),
                new DataColumn("JANGKA PENDEK", typeof(string)),
                new DataColumn("NEXT YEAR",typeof(string)),
                new DataColumn("END CONTRACT",typeof(string)),
                new DataColumn("PROYEKSI CONTRACT",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                   
                        dt.Rows.Add(valData.NO,valData.PROFIT_CENTER_NAMA, valData.BUSINESS_PARTNER_NAME ,valData.CONTRACT_NO, valData.VAL_CONTRACT, valData.COA_PROD,
                                    valData.VAL_PERMONTH, valData.CONTRACT_START_DATE,
                                    valData.CONTRACT_END_DATE, valData.TERM_IN_MONTHS, valData.PREV_PERIOD,
                                    valData.PAID_PERIOD, valData.ACC_REVENUE, valData.PAID_PDK,
                                    valData.JANGKA_PNDK, valData.NEXT_YEAR, valData.END_CONTRACT, valData.PROYEKSI_CONTRACT);
                }

                //Codes for the Closed XML
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    var ws = wb.Worksheets.Add("ReportJKP");
                    ws.Cell(1, 1).InsertData(new string[] { "Periode" });
                    ws.Cell(1, 2).InsertData(new string[] { param.PERIOD });
                    ws.Cell(2, 1).InsertData(new string[] { "Tahun" });
                    ws.Cell(2, 2).InsertData(new string[] { year });
                    ws.Cell(4, 1).InsertTable(dt);
                    wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

    }
}
