﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.ReportOtherService;
using Remote.Models.Select2;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
//using System.Web.Services;
using ClosedXML.Excel;
using System.IO;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ReportOtherServiceController : Controller
    {
        //
        // GET: /ReportOtherServices/
        public ActionResult Index()
        {
            return View("ReportOtherServiceIndex");
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //-------------------------- SELECT2 BUSINESS ENTITY -----------------
        public JsonResult getBusinessEntity()
        {

            string KodeCabang = CurrentUser.KodeCabang;

            ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
            IEnumerable<VW_BUSINESS_ENTITY> result = dal.getBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }

        //-------------------------- SELECT2 PROFIT CENTER -----------------
        public JsonResult getProfitCenter()
        {
            string KodeCabang = CurrentUser.KodeCabang;

            ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
            IEnumerable<VW_PROFIT_CENTER> result = dal.getProfitCenter(KodeCabang);
            return Json(new { data = result });
        }

        public JsonResult getProfitCenterD()
        {
            var queryString = Request.Query;
            string BE_ID = queryString["BE_ID"];

            string KodeCabang = CurrentUser.KodeCabang;
            ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
            IEnumerable<VW_PROFIT_CENTER> result = dal.getProfitCenterD(KodeCabang, BE_ID);
            return Json(new { data = result });
        }

        //-------------------------- SELECT2 SERVICE GROUP -----------------
        public JsonResult getServiceGroup()
        {
            ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
            IEnumerable<VW_SERVICE_GROUP> result = dal.getServiceGroup();
            return Json(new { data = result });
        }

        //----------------------- BUTTON SHOW ALL ----------------
        public JsonResult ShowAll()
        {
            DataTableReportOtherService result = new DataTableReportOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
                    result = dal.ShowAll(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------------------------- BUTTON FILTER -------------------
        [HttpGet]
        public JsonResult GetDataFilter()
        {
            DataTableReportOtherService result = new DataTableReportOtherService();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string be_id = queryString["be_id"];
                    string profit_center = queryString["profit_center"];
                    string service_group = queryString["service_group"];
                    string status = queryString["status"];
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    ReportOtherServiceDAL dal = new ReportOtherServiceDAL();
                    DataTableReportOtherService data = dal.GetDataFilternew(draw, start, length, be_id, profit_center, service_group, status, search, KodeCabang);

                    result = data;
                }
                catch (Exception ex)
                {
                    string aaa = ex.ToString();
                }
            }

            return Json(result);
        }

        //[WebMethod]

        [HttpPost]
        public JsonResult defineExcel([FromBody] ReportOtherServiceParam param)
        {
            nama result = new nama();
            string fileName = "Report Other Services(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                string KodeCabang = CurrentUser.KodeCabang;
                ReportOtherServiceDAL dal = new ReportOtherServiceDAL();

                if (param.BE_ID == "" && param.PROFIT_CENTER_ID == "" && param.SERVICE_GROUP_ID == "" && param.STATUS != null)
                {
                    var dalShowdata = dal.ShowFilterTwoDnew(KodeCabang);

                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[12] { 
                    new DataColumn("BE NAME", typeof(string)),
                    new DataColumn("PROFIT CENTER", typeof(string)),
                    new DataColumn("SERVICE GROUP",typeof(string)),
                    new DataColumn("SERVICE NAME", typeof(string)),
                    new DataColumn("GL ACCOUNT",typeof(string)),
                    new DataColumn("UNIT", typeof(string)),
                    new DataColumn("PRICE",typeof(string)),
                    new DataColumn("CURRENCY", typeof(string)),
                    new DataColumn("MULTIPLY FACTOR %",typeof(string)),
                    new DataColumn("LEGAL CONTRACT",typeof(string)),
                    new DataColumn("CONTRACT DATE",typeof(string)),
                    new DataColumn("STATUS",typeof(string))});
                    foreach (var valData in dalShowdata)
                    {
                        if (valData.ACTIVE == "1")
                        {
                            dt.Rows.Add(valData.BE_NAME, valData.PROFIT_CENTER_NAME, valData.SERVICE_GROUP_NAME, valData.SERVICE_NAME,
                                        valData.GL_ACCOUNT, valData.UNIT, valData.PRICE,
                                        valData.CURRENCY, valData.MULTIPLY_FUNCTION, valData.LEGAL_CONTRACT_NO, valData.CONTRACT_DATE, "ACTIVE");
                        }
                        else
                        {
                            dt.Rows.Add(valData.BE_NAME, valData.PROFIT_CENTER_NAME, valData.SERVICE_GROUP_NAME, valData.SERVICE_NAME,
                                        valData.GL_ACCOUNT, valData.UNIT, valData.PRICE,
                                        valData.CURRENCY, valData.MULTIPLY_FUNCTION, valData.LEGAL_CONTRACT_NO, valData.CONTRACT_DATE, "INACTIVE");
                        }
                    }

                    //Codes for the Closed XML
                    //using (XLWorkbook wb = new XLWorkbook())
                    //{
                    //    wb.Worksheets.Add(dt, "Report Other Services");
                    //    wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                    //    Response.Clear();

                    //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                    //}

                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        wb.Worksheets.Add(dt, "Report Other Services");
                        wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                    }
                }
                else
                {
                    var dalShowdata = dal.ShowFilterTwonew(param.BE_ID, param.PROFIT_CENTER_ID, param.SERVICE_GROUP_ID, param.STATUS, KodeCabang);

                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[12] { 
                    new DataColumn("BE NAME", typeof(string)),
                    new DataColumn("PROFIT CENTER", typeof(string)),
                    new DataColumn("SERVICE GROUP",typeof(string)),
                    new DataColumn("SERVICE NAME", typeof(string)),
                    new DataColumn("GL ACCOUNT",typeof(string)),
                    new DataColumn("UNIT", typeof(string)),
                    new DataColumn("PRICE",typeof(string)),
                    new DataColumn("CURRENCY", typeof(string)),
                    new DataColumn("MULTIPLY FACTOR %",typeof(string)),
                    new DataColumn("LEGAL CONTRACT",typeof(string)),
                    new DataColumn("CONTRACT DATE",typeof(string)),
                    new DataColumn("STATUS",typeof(string))});
                    foreach (var valData in dalShowdata)
                    {
                        if (valData.ACTIVE == "1")
                        {
                            dt.Rows.Add(valData.BE_NAME, valData.PROFIT_CENTER_NAME, valData.SERVICE_GROUP_NAME, valData.SERVICE_NAME,
                                        valData.GL_ACCOUNT, valData.UNIT, valData.PRICE,
                                        valData.CURRENCY, valData.MULTIPLY_FUNCTION, valData.LEGAL_CONTRACT_NO, valData.CONTRACT_DATE, "ACTIVE");
                        }
                        else
                        {
                            dt.Rows.Add(valData.BE_NAME, valData.PROFIT_CENTER_NAME, valData.SERVICE_GROUP_NAME, valData.SERVICE_NAME,
                                        valData.GL_ACCOUNT, valData.UNIT, valData.PRICE,
                                        valData.CURRENCY, valData.MULTIPLY_FUNCTION, valData.LEGAL_CONTRACT_NO, valData.CONTRACT_DATE, "INACTIVE");
                        }
                    }

                    //Codes for the Closed XML
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        wb.Worksheets.Add(dt, "Report Other Services");
                        wb.SaveAs(Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                    }
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [HttpPost]
        public JsonResult deleteExcel([FromBody] string fileName)
        {
            string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
            string filePath = Path.Combine(rootPath, "REMOTE", "Excel", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            return Json(new { Status = "S" });
        }

        public class nama
        {
            public string fileNames { get; set; }
        }

        //[WebMethod]
        //public string exportExcel()
        //{
        //    string sFiles = string.Empty;

        //    string[] nFiles = GetFileNames(Path.Combine(Server.MapPath("~/REMOTE/Excel/")), "*.xlsx");
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    sFiles = serializer.Serialize(nFiles);
        //    return sFiles;
        //}

        //private static string[] GetFileNames(string path, string filter)
        //{
        //    string[] files = Directory.GetFiles(path, filter);
        //    for (int i = 0; i < files.Length; i++)
        //        files[i] = Path.GetFileName(files[i]);
        //    return files;
        //}
    }

}