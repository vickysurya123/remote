﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.Models;
using Remote.Models.TransVB;
using Remote.Models.UserModels;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class SettingPostingController : Controller
    {
        // GET: SettingOtherServie
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            string NamaCabang = CurrentUser.NamaCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            ViewBag.NamaCabang = NamaCabang;
            return View("Index", viewModel);
        }

        public ActionResult Config()
        {

            var queryString = Request.Query;
            string id = queryString["id"];

            string KodeCabang = CurrentUser.KodeCabang;
            string NamaCabang = CurrentUser.NamaCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();

            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            ViewBag.NamaCabang = NamaCabang;

            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.HEADER_ID = id;
                SettingPostingDAL dals = new SettingPostingDAL();
                dynamic result = dals.GetDataHeader(id);
                ViewBag.EMPLOYEE_NAME = result.USER_NAME;
                ViewBag.USER_ID = result.USER_ID + "|" +result.USER_NAME;
            }

            return View("ConfigPosting", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetDataDropDownServiceGroup()
        {
            SettingOtherServiceDAL dal = new SettingOtherServiceDAL();
            IEnumerable<DDServiceGroup> result = dal.GetDataDropDownServiceGroup();
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownUser()
        {
            SettingOtherServiceDAL dal = new SettingOtherServiceDAL();
            IEnumerable<DataUser> result = dal.GetDataDropDownUser();
            return Json(new { data = result });
        }

        public JsonResult GetDataPosting()
        {
            DataTablesService result = new DataTablesService();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if(isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    SettingPostingDAL dal = new SettingPostingDAL();
                    result = dal.GetDataService(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [HttpPost]
        public JsonResult AddHeader([FromBody] DataTrans data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    SettingPostingDAL dal = new SettingPostingDAL();
                    //result = (data.ID != null ? dal.EditDataHeader(data, user.Name) : dal.AddDataHeader(data, user.Name));
                    result = dal.AddDataHeader(data, user.Name);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult Delete([FromBody] DataTrans data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    SettingPostingDAL dal = new SettingPostingDAL();
                    result = dal.Delete(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult DeleteHeader([FromBody] DataTrans data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if(isAjax)
            {
                try
                {
                    SettingPostingDAL dal = new SettingPostingDAL();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult EditDataDetail(string id)
        {
            SettingPostingDAL dal = new SettingPostingDAL();

            //ROLE
            IEnumerable<SettingPosting> result = dal.GetDataDetail(id);
            //result = result.Where(r => r.PROPERTY_ROLE != null);

            //param
            //IEnumerable<ParamRow> param = dal.GetDataDetailParam(id);

            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult InsertDetail([FromBody] SettingPosting2 data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    SettingPostingDAL dal = new SettingPostingDAL();
                    result = dal.AddDataDetail(data, user.Name);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
        public JsonResult GetStatusWater()
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if(isAjax)
            {
                try
                {
                    var user = CurrentUser;
                    SettingPostingDAL dal = new SettingPostingDAL();
                    result = dal.getStatusWater(user.UserID);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
    }
}