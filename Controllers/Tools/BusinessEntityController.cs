﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterBusinessEntity;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Models;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
//using System.Runtime.Remoting.Contexts;

namespace Remote.Controllers
{
    [Authorize]
    public class BusinessEntityController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //
        // GET: /BusinessEntity/
        public ActionResult Index()
        {
            return View("BEIndex");
        }

        public ActionResult Add()
        {
            MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
            var viewModel = new BusinessEntity
            {
                DDHarbour1 = dal.GetDataListHarbour(),
                DDProvince = dal.GetDataListProvince(),
            };
            return View(viewModel);
        }


        public ActionResult Tambah()
        {
            return View();
        }

        public ActionResult AddCoba()
        {
            return View();
        }

        //------------------- DATATABLE BE ----------------------------
        public JsonResult GetDataBE()
        {
            DataTablesBusinessEntity result = new DataTablesBusinessEntity();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = CurrentUser.KodeCabang;
                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    result = dal.GetDataBE(draw, start, length, search, KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataMeasurement(string id)
        {
            DataTablesMeasurement result = new DataTablesMeasurement();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    result = dal.GetDataMeasurement(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachment result = new DataTablesAttachment();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-----------UBAH STATUS MEASUREMENT
        public JsonResult UbahStatusMeasurement(string id)
        {
            dynamic message = null;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                message = dal.UbahStatusMeasurement(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        //-------------------TAMBAH DATA VIEW----------------------------
        [HttpPost]
        public JsonResult AddData([FromBody] DataBE data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    var result = dal.Add(User.Identity.Name, data);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            //ambil data dari return master rental object DAL
                            be_number = result.BE_NUMBER,
                            status = "S",
                            message = "Business Entity Has Been Created, BE NUMBER :" + result.BE_NUMBER

                            //message = "Data berhasil ditambahkan."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult AddDataMeasurement([FromBody] DataMeasurement data)
        {
            dynamic message = null;


            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    bool result = dal.AddM(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------UBAH DATA----------------------------
        [HttpPost]
        public JsonResult EditData([FromBody] DataBE data)
        {
            dynamic message = null;


            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    bool result = dal.Edit(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------UBAH DATAVIEW----------------------------
        public ActionResult Edit(string id)
        {
            ViewBag.BE_ID = id;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                AUP_BE_WEBAPPS data = dal.GetDataForEdit(id);

                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.BE_ADDRESS = data.BE_ADDRESS;
                ViewBag.BE_CITY = data.BE_CITY;
                ViewBag.BE_PROVINCE = data.BE_PROVINCE;
                ViewBag.BE_CONTACT_PERSON = data.BE_CONTACT_PERSON;
                ViewBag.BE_NPWP = data.BE_NPWP;
                ViewBag.BE_NOPPKP = data.BE_NOPPKP;
                ViewBag.BE_PPKP_DATE = data.BE_PPKP_DATE;
                ViewBag.CREATION_BY = data.CREATION_BY;
                ViewBag.CREATION_DATE = data.CREATION_DATE;
                ViewBag.LAST_UPDATE_BY = data.LAST_UPDATE_BY;
                ViewBag.LAST_UPDATE_DATE = data.LAST_UPDATE_DATE;
                ViewBag.VALID_FROM = data.VALID_FROM;
                ViewBag.VALID_TO = data.VALID_TO;

                ViewBag.VAL_FROM = data.VAL_FROM;
                ViewBag.VAL_TO = data.VAL_TO;
                ViewBag.HARBOUR_CLASS = data.HARBOUR_CLASS;
                ViewBag.POSTAL_CODE = data.POSTAL_CODE;
                ViewBag.HB_ID = data.HB_ID;
                ViewBag.BE_PROVINCE_ID = data.BE_PROVINCE_ID;

                ViewBag.PHONE_1 = data.PHONE_1;
                ViewBag.FAX_1 = data.FAX_1;
                ViewBag.PHONE_2 = data.PHONE_2;
                ViewBag.FAX_2 = data.FAX_2;
                ViewBag.EMAIL = data.EMAIL;
            }
            catch (Exception)
            {
                //ViewBag.BE_ID = string.Empty;
                //ViewBag.BE_NAME = string.Empty;
                //ViewBag.BE_ADDRESS = string.Empty;
                //ViewBag.BE_CITY = string.Empty;
                //ViewBag.BE_PROVINCE = string.Empty;
                //ViewBag.BE_CONTACT_PERSON = string.Empty;
                //ViewBag.BE_NPWP = string.Empty;
                //ViewBag.BE_NOPPKP = string.Empty;
                //ViewBag.BE_PPKP_DATE = string.Empty;
                //ViewBag.CREATION_BY = string.Empty;
                //ViewBag.CREATION_DATE = string.Empty;
                //ViewBag.LAST_UPDATE_BY = string.Empty;
                //ViewBag.LAST_UPDATE_DATE = string.Empty;
                //ViewBag.VALID_FROM = string.Empty;
                //ViewBag.VALID_TO = string.Empty;
                //ViewBag.HARBOUR_CLASS = string.Empty;
                //ViewBag.POSTAL_CODE = string.Empty;
            }

            MasterBusinessEntityDAL dal2 = new MasterBusinessEntityDAL();
            var viewModel = new BusinessEntity
            {
                DDHarbour1 = dal2.GetDataListHarbour(),
                DDProvince = dal2.GetDataListProvince(),
            };

            return View(viewModel);
        }

        public ActionResult AddMeasurement(string id)
        {
            ViewBag.BE_ID = id;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                AUP_BE_WEBAPPS data = dal.GetDataForMeasurement(id);

                ViewBag.BE_ID = data.BE_ID;


            }
            catch (Exception)
            {
                ViewBag.BE_ID = string.Empty;

            }

            return View();
        }

        //-------------------VIEW DETIL----------------------------
        public ActionResult Detil(string id)
        {
            ViewBag.BE_ID = id;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                AUP_BE_WEBAPPS data = dal.GetDataForDetil(id);

                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.BE_ADDRESS = data.BE_ADDRESS;
                ViewBag.BE_CITY = data.BE_CITY;
                ViewBag.BE_PROVINCE = data.BE_PROVINCE;
                ViewBag.VALID_FROM = data.VALID_FROM;
                ViewBag.VALID_TO = data.VALID_TO;
                ViewBag.REF_DESC = data.REF_DESC;
                ViewBag.POSTAL_CODE = data.POSTAL_CODE;
                ViewBag.PHONE_1 = data.PHONE_1;
                ViewBag.FAX_1 = data.FAX_1;
                ViewBag.EMAIL = data.EMAIL;

            }
            catch (Exception)
            {
                ViewBag.BE_ID = string.Empty;
                ViewBag.BE_NAME = string.Empty;
                ViewBag.BE_ADDRESS = string.Empty;
                ViewBag.BE_CITY = string.Empty;
                ViewBag.BE_PROVINCE = string.Empty;
                ViewBag.VALID_FROM = string.Empty;
                ViewBag.VALID_TO = string.Empty;
                ViewBag.REF_DESC = string.Empty;
                ViewBag.POSTAL_CODE = string.Empty;
                ViewBag.PHONE_1 = string.Empty;
                ViewBag.FAX_1 = string.Empty;
                ViewBag.EMAIL = string.Empty;
            }

            return View();
        }

        //-------------------DELETE DATA----------------------------
        [HttpPost]
        public JsonResult DeleteData([FromBody] string id)
        {
            dynamic message = null;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                message = dal.DeleteData(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        //---------------------- LIST DROPDOWN HARBOUR CLASS -------------------------------


        //-------------------- LIST DROP DOWN PROVINCE --------------------------------


        //-------------------- LIST DROP DOWN MEASUREMENT --------------------------------
        public JsonResult GetDataCBMeasurement()
        {
            MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
            IEnumerable<DDHarbour> result = dal.GetDataMeasurement();
            return Json(new { data = result });
        }

        //-----------Edit Data Measurement 
        [HttpPost]
        public JsonResult EditDataMeasurement([FromBody] DataMeasurement data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                    bool result = dal.EditMeasurement(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //public FilePathResult Image()
        //{
        //    string filename = Request.Url.AbsolutePath.Replace("/home/image", "");
        //    string contentType = "";
        //    var filePath = new FileInfo(Server.MapPath("~/FileUpload") + filename);

        //    var index = filename.LastIndexOf(".") + 1;
        //    var extension = filename.Substring(index).ToUpperInvariant();

        //    // Fix for IE not handling jpg image types
        //    contentType = string.Compare(extension, "JPG") == 0 ? "image/jpeg" : string.Format("image/{0}", extension);

        //    return File(filePath.FullName, contentType);
        //}

        [HttpPost]
        public ContentResult UploadFiles([FromForm] BusinessEntityAttachment data)//, [FromForm] List<Microsoft.AspNetCore.Http.IFormFile> files)
        {
            var r = new List<UploadFilesResult>();

            foreach (var file in data.files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "BusinessEntity", subPath);

                bool exists = System.IO.Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);

                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/BusinessEntity/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                bool result = dal.AddFiles(sFile, directory, KodeCabang, data.BE_ID);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "BusinessEntity", uri, filename);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }
        public JsonResult PinPoint(string BE_ID)
        {
            dynamic message = null;

            try
            {
                MasterBusinessEntityDAL dal = new MasterBusinessEntityDAL();
                message = dal.PinPoint(BE_ID);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }
    }
}