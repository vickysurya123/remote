﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models.MonitoringKronologisMaster;
using Remote.Models;
using Remote.ViewModels;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
/*using System.Security.Claims;*/
using Microsoft.AspNetCore.Http;
using ClosedXML.Excel;
using Newtonsoft.Json;
using Remote.Models.MasterVariousBusiness;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using RestSharp;
using System.Net.Http;
using Remote.Models.SettingDisposisi;

namespace Remote.Controllers
{
    [Authorize]
    public class SettingDisposisiController : Controller
    {
        // GET: Parameter   
        public ActionResult Index(string cabang)
        {
            try
            {
                ViewBag.PARAM = cabang;
            }
            catch
            {
                ViewBag.PARAM = string.Empty;
            }

            ApprovalSettingDAL dal1 = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBusinessEntity = dal1.GetDataListBusinessEntity(CurrentUser.KodeCabang),
            };
            return View("SettingDisposisiIndex", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                    /*{D:\pel3remotev3-web\Models\NotificationContract\;*/
                }
            }

            return Json(result);
        }

        public ActionResult Add()
        {
            var queryString = Request.Query;
            string id = queryString["id"];

            string KodeCabang = CurrentUser.KodeCabang;
            string NamaCabang = CurrentUser.NamaCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();

            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(KodeCabang),
            };

            ViewBag.KodeCabang = KodeCabang;
            ViewBag.NamaCabang = NamaCabang;

            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.ID = id;
                SettingDisposisiDal dals = new SettingDisposisiDal();
                dynamic result = dals.GetDataHeader(id);
                ViewBag.NAME = result.NAME;
                ViewBag.USER_ID = result.USER_ID;
                ViewBag.START_DATE = result.START_DATE;
                ViewBag.END_DATE = result.END_DATE;
                ViewBag.MEMO = result.MEMO;
                ViewBag.INSTRUKSI = result.INSTRUKSI;
            }

            return View("Form", viewModel);
        }

        [HttpPost]
        public JsonResult AddHeader([FromBody] SettingDisposisi data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.AddDataHeader(data, user.UserID);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult EditHeader([FromBody] SettingDisposisi data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.EditDataHeader(data, user.UserID);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult DeleteHeader([FromBody] SettingDisposisi data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult EditStatus([FromBody] SettingDisposisi data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.EditDataStatus(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult InsertDetail([FromBody] SettingDetail data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    SettingDisposisiDal dal = new SettingDisposisiDal();
                    result = dal.AddDataDetail(data, user.Name);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult EditDataDetail(string id)
        {
            SettingDisposisiDal dal = new SettingDisposisiDal();

            //ROLE
            IEnumerable<SettingDetailD> result = dal.GetDataDetail(id);
            //result = result.Where(r => r.PROPERTY_ROLE != null);

            //param
            //IEnumerable<ParamRow> param = dal.GetDataDetailParam(id);

            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownUser()
        {
            SettingDisposisiDal dal = new SettingDisposisiDal();
            IEnumerable<DDEmailEmployee> result = dal.GetDataEmployee();
            return Json(new { data = result });
        }

        [HttpGet]
        public JsonResult GetDataUser(string q)
        {
            string KodeCabang = CurrentUser.KodeCabang;

            SettingDisposisiDal dal = new SettingDisposisiDal();
            IEnumerable<DDEmailEmployee> result = dal.GetDataEmployee(KodeCabang, q);

            return Json(new
            {
                results = result
            });
        }

    }
}
