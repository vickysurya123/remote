﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.MasterConfiguration;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ConfigurationController : Controller
    {
        //
        // GET: /Configuration/
        public ActionResult Index()
        {
            return View("ConfigIndex");
        }

        public ActionResult AddConfig()
        {
            MasterConfigurationDAL dal = new MasterConfigurationDAL();
            var viewModel = new Configuration
            {
                DDConfig = dal.GetDataListConfig(),
            };
            return View(viewModel);
        }

        //-------------------------------------- VIEW DATA CONFIG ------------------------------

        public JsonResult GetDataConfig()
        {
            DataTablesConfiguration result = new DataTablesConfiguration();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    MasterConfigurationDAL dal = new MasterConfigurationDAL();
                    result = dal.GetDataConfig(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //-------------------------------- EDIT DATA CONFIG -------------------------

        [HttpPost]
        public JsonResult EditData([FromBody] DATAREF_D data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterConfigurationDAL dal = new MasterConfigurationDAL();
                    bool result = dal.Edit(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Edit Success."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Edit Failed."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "There is an error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //-------------------------- VIEW EDIT DATA CONFIG ------------------------

        public ActionResult Edit(string id)
        {
            ViewBag.ID = id;

            try
            {
                MasterConfigurationDAL dal = new MasterConfigurationDAL();
                AUP_CONFIGURATION_WEBAPPS data = dal.GetDataForEdit(id);

                ViewBag.REF_CODE_C = data.REF_CODE_C;
                ViewBag.REF_DATA = data.REF_DATA;
                ViewBag.REF_DESC_C = data.REF_DESC_C;
                ViewBag.ATTRIB1 = data.ATTRIB1;
                ViewBag.ATTRIB2 = data.ATTRIB2;
                ViewBag.ATTRIB3 = data.ATTRIB3;
                ViewBag.ATTRIB4 = data.ATTRIB4;

                ViewBag.VAL1 = data.VAL1;
                ViewBag.VAL2 = data.VAL2;
                ViewBag.VAL3 = data.VAL3;
                ViewBag.VAL4 = data.VAL4;
            }
            catch (Exception)
            {
                ViewBag.REF_CODE_C = string.Empty;
                ViewBag.REF_DATA = string.Empty;
                ViewBag.REF_DESC_C = string.Empty;
                ViewBag.ATTRIB1 = string.Empty;
                ViewBag.VAL1 = string.Empty;
            }

            return View();
        }

        //------------------------- DELETE DATA CONFIG --------------------------

        [HttpPost]
        public JsonResult DeleteData([FromBody] string id)
        {
            dynamic message = null;

            try
            {
                MasterConfigurationDAL dal = new MasterConfigurationDAL();
                message = dal.DeleteData(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        //-------------------------- INSERT DATA CONFIG --------------------------
        [HttpPost]
        public JsonResult AddData([FromBody] DATAREF_D data)
        {
            dynamic message = null; 
            
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    MasterConfigurationDAL dal = new MasterConfigurationDAL();
                    bool result = dal.Add(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //---------------------------- UBAH STATUS CONFIG -------------------------

        public JsonResult UbahStatus(string id)
        {
            dynamic message = null;

            try
            {
                MasterConfigurationDAL dal = new MasterConfigurationDAL();
                message = dal.UbahStatus(id);
            }
            catch (Exception)
            {
            }

            return Json(message);
        }

        public JsonResult GetLastID(string REF_CODE)
        {
            MasterConfigurationDAL dal = new MasterConfigurationDAL();
            IEnumerable<AUP_CONFIGURATION_WEBAPPS> result = dal.GetLastID(REF_CODE);
            return Json(new { data = result });
        }
	}
}