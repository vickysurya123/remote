﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;

namespace Remote.Controllers
{
    [Authorize]
    public class ParameterController : Controller
    {
        // GET: Parameter
        public ActionResult Index()
        {
            return View("ApprovalSettingParam");
        }
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ParameterDAL dal = new ParameterDAL();
                    result = dal.GetData(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [HttpPost]
        public  JsonResult Insert([FromBody]APPROVAL_SETTING_PARAMETER data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    string id = queryString["id"];
                    
                    ParameterDAL dal = new ParameterDAL();

                    result = (data.ID != null ? dal.EditDataHeader(data) : dal.AddDataHeader(data));

                    //result = dal.AddDataHeader(data, id);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
        //public JsonResult Edit(APPROVAL_SETTING_PARAMETER data)
        //{
        //    dynamic result = null;

        //    bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";if (isAjax)
        //    {
        //        try
        //        {
        //            var queryString = Request.Query;
        //            string id = queryString["id"];
        //            ParameterDAL dal = new ParameterDAL();
        //            //result = (data.ID != null ? dal.EditDataHeader(data) : dal.AddDataHeader(data));
        //            result = dal.EditDataHeader(data, id);

        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    }

        //    return Json(result);
        //}
        [HttpPost]
        public JsonResult Delete([FromBody]APPROVAL_SETTING_PARAMETER data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    ParameterDAL dal = new ParameterDAL();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
    }
}