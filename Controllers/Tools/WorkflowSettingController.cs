﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.WorkflowSetting;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Remote.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class WorkflowSettingController : Controller
    {
        //
        // GET: /WorkflowSetting/
        public ActionResult Index()
        {
            return View("WorkflowSettingIndex");
        }

        public ActionResult AddConfig()
        {
            return View("AddWorkflowSetting");            
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        //----------------------- GET DATATABLE WORKFLOW SETTING ----------------
        public JsonResult GetDataWorkflowSetting()
        {
            DataTablesWorkflowSetting result = new DataTablesWorkflowSetting();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    WorkflowSettingDAL dal = new WorkflowSettingDAL();
                    result = dal.GetDataWorkflowSetting(draw, start, length, search);
                }
                catch (Exception ex)
                {
                }
            }

            return Json(result);
        }
        public JsonResult getDataBusinessEntity()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            WorkflowSettingDAL dal = new WorkflowSettingDAL();
            IEnumerable<DDBusinessEntity> result = dal.getDataBusinessEntity(KodeCabang);
            return Json(new { data = result });
        }
	}
}