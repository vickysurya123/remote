﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Remote.DAL;
using Remote.Models;
using Remote.Models.DynamicDatatable;
using Remote.Models.ListNotifikasi;
using Remote.Models.GeneralResult;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;
using RestSharp;

namespace Remote.Controllers
{
    public class ListNotifikasiController : Controller
    {

        // GET: ListNotifikasi
        [Authorize]
        public ActionResult Index()
        {
            var user = CurrentUser;
            ListNotifikasiDAL dal = new ListNotifikasiDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(user.KodeCabang),
            };

            ViewBag.KodeCabang = CurrentUser.KodeCabang;
            return View("Index", viewModel);
        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        #region "Notification Mobile API"
        [HttpPost]
        public JsonResult regFirebase([FromBody] ParamFirebase param)
        {
            Results res = new Results();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.regFirebase(param);
            }
            catch(Exception e)
            {
                res.Status = "E";
                res.Msg = e.Message + "\n" + e.StackTrace;
            }
            return Json(new { Status = res.Status, Message = res.Msg });
        }

        [HttpPost]
        public JsonResult unRegFirebase([FromBody] ParamFirebase param)
        {
            Results res = new Results();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.unRegFirebase(param);
            }
            catch (Exception e)
            {
                res.Status = "E";
                res.Msg = e.Message + "\n" + e.StackTrace;
            }
            return Json(new { Status = res.Status, Message = res.Msg });
        }

        [HttpPost]
        public JsonResult listTokenFirebase([FromBody] ParamFirebase param)
        {
            List<ListFirebase> res = null;
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.listTokenFirebase(param.username).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
            }
            return Json(new { Status = "S", Message = res });
        }

        [HttpPost]
        public JsonResult addNewNotif([FromBody] ParamNotification param)
        {
            Results res = new Results();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.addNewNotif(param);
                if(res.Status == "S")
                {
                    var result = sendByFirebase(param);
                }
            }
            catch(Exception e)
            {
                res.Status = "E";
                res.Msg = e.Message + "\n" + e.StackTrace;
            }
            return Json(new { Status = res.Status, Message = res.Msg });
        }

        [HttpGet]
        public JsonResult getNotifByID(string username, string last_ID)
        {
            dynamic response = null;
            ResultsNotif res = new ResultsNotif();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.getNotifByID(username, last_ID);
                if(res.status == "E")
                {
                    response = new { Status = "E", Message = res.message };
                }
                else
                {
                    response = new { Status = "S", Message = res.data };
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpGet]
        public JsonResult getNotifNotRead(string username)
        {
            dynamic response = null;
            ResultsNotif res = new ResultsNotif();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.getNotifNotRead(username);
                if (res.status == "E")
                {
                    response = new { Status = "E", Message = res.message };
                }
                else
                {
                    response = new { Status = "S", Message = res.data };
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpGet]
        public JsonResult getCountAppDisPar(string username, string user_id, string role_id, string propertyRole, string kdcabang)
        {
            dynamic response = null;
            ResultsNotif res = new ResultsNotif();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.getCountDisposisi(username, user_id, role_id);
                if (res.status == "E")
                {
                    response = new { Status = "E", Message = res.message };
                }
                else
                {
                    string nilaiDisposisi = res.message;
                    res = dal.getCountParaf(username, user_id, role_id);
                    if (res.status == "E")
                    {
                        response = new { Status = "E", Message = res.message };
                    }
                    else
                    {
                        string nilaiParaf = res.message;
                        res = dal.getCountApproval(username, user_id, role_id, propertyRole, kdcabang);
                        if (res.status == "E")
                        {
                            response = new { Status = "E", Message = res.message };
                        }
                        else
                        {
                            response = new { Status = "S", Disposisi = nilaiDisposisi, Paraf = nilaiParaf, Approval = res.message };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        public JsonResult getCountNotif(string username)
        {
            dynamic response = null;
            ResultsNotif res = new ResultsNotif();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.getCountNotif(username);
                if (res.status == "E")
                {
                    response = new { Status = "E", Message = res.message };
                }
                else
                {
                    response = new { Status = "S", Notifikasi = res.message };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpPost]
        public JsonResult sendByFirebase([FromBody] ParamNotification param)
        {
            dynamic getData = null;
            string url = "https://fcm.googleapis.com/fcm/send";
            dynamic jsonBody = null;
            dynamic getList = null;

            try
            {
                ParamFirebase data = new ParamFirebase();
                data.username = param.username;
                getList = listTokenFirebase(data);
            }
            catch(Exception e)
            {

            }

            Results res = new Results();
            RestClient client = new RestClient(url);
            RestRequest request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            var dipanggil = 0;
            foreach(var item in getList.Value.Message)
            {
                var dal = new ListNotifikasiDAL();
                string namaPengirim = dal.getNamaDariNipp(param.pengirim);
                string Judul = "";
                if (param.type == "1")
                {
                    Judul = "Permohonan Approval";
                } 
                else if (param.type == "2")
                {
                    Judul = "Menerima Disposisi";
                } 
                else if (param.type == "3")
                {
                    Judul = "Permohonan Paraf";
                }
                else if (param.type == "4")
                {
                    Judul = "Informasi Void";
                }
                else if (param.type == "5")
                {
                    Judul = "Informasi Revise";
                }
                else if (param.type == "6")
                {
                    Judul = "Melakukan Disposisi";
                }
                else
                {
                    Judul = "Notifikasi";
                }

                string IsiNotif = "" + Judul + " dari " + namaPengirim + " untuk kontrak " + param.no_contract + " dengan memo " + param.notification;

                request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };
                request.AddHeader("Accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "key=AAAA-ZWpu18:APA91bEez_SCGEwQLOJTHRcbW50Oj2qpthVQRuIBVMPACgjQXxFIj6_5TQE6B_7h8Yg8eKIBhKxZVW6y9zaA94_BhRmPQKuLlZuGzUHWR8FKIVu6LtKOA9moAXJTtSYndpLaNjVLyB5A");
                jsonBody = new
                {
                    to = item.token_firebase,
                    notification = new
                    {
                        sound = "default",
                        body = IsiNotif, //param.notification,
                        title = Judul, //param.type,
                        content_available = true,
                        priority = "low",
                    },
                    data = new
                    {
                        sound = "default",
                        body = param.no_contract,
                        title = param.pengirim,
                        pesan = param.notification,
                        tipe = param.type,
                        content_available = true,
                        priority = "low",
                    }
                };
                request.AddJsonBody(jsonBody);
                getData = client.Execute<dynamic>(request);
                dipanggil = 1;
            }
            if (dipanggil == 1)
            {
                if (getData.StatusDescription == "OK")
                {
                    res.Status = "S";
                    res.Msg = "Berhasil";
                }
                else
                {
                    res.Status = "E";
                    res.Msg = "Gagal Kirim Notification";
                }
            } else
            {
                res.Status = "S";
                res.Msg = "Berhasil";
            }
            return Json(new { Status = res.Status, Message = res.Msg });
        }

        [HttpGet]
        public JsonResult getAllNotif(int start, int length, string username)
        {
            dynamic response = null;
            ResultsNotif res = new ResultsNotif();
            try
            {
                var dal = new ListNotifikasiDAL();
                res = dal.getAllNotif(username, start, length);
                if (res.status == "E")
                {
                    response = new { Status = "E", Message = res.message };
                }
                else
                {
                    response = new { Status = "S", Message = res.data };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpGet]
        public JsonResult PinPointMobile(string BRANCH_ID)
        {
            dynamic response = null;
            dynamic message = null;

            ResultsNotif res = new ResultsNotif();
            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.PinPoint(BRANCH_ID);
                //if (res.status == "E")
                //{
                //    response = new { Status = "E", Message = res.message };
                //}
                //else
                //{
                response = message;
                //response = new { Status = "S", Message = message };
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpGet]
        public JsonResult PinROMobile(string BRANCH_ID)
        {
            dynamic response = null;
            dynamic message = null;

            ResultsNotif res = new ResultsNotif();
            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.PinRO(BRANCH_ID, null, null);
                //if (res.status == "E")
                //{
                //    response = new { Status = "E", Message = res.message };
                //}
                //else
                //{
                response = message;
                //response = new { Status = "S", Message = message };
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }

        [HttpGet]
        public JsonResult GetImageMobile(string RO_NUMBER)
        {
            dynamic message = null;

            try
            {
                MapsGeoDAL dal = new MapsGeoDAL();
                message = dal.GetImage(RO_NUMBER);
                foreach (dynamic x in message.message)
                {
                    x.DIRECTORY = Url.Content(x.DIRECTORY);
                }
            }
            catch (Exception ex)
            {
                string aaa = ex.ToString();
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult readNotif([FromBody] ParamNotification param)
        {
            dynamic response = null;
            Results results = new Results();
            try
            {
                var dal = new ListNotifikasiDAL();
                results = dal.readNotif(param.username, param.no_contract);
                if(results.Status == "S")
                {
                    response = new { Status = results.Status, Message = results.Msg };
                }
                else
                {
                    response = new { Status = results.Status, Message = results.Msg };
                }
            }
            catch(Exception e)
            {
                response = new { Status = "E", Message = e.Message };
            }
            return Json(response);
        }
        #endregion

        [Authorize]
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            var user = CurrentUser;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if(isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ListNotifikasiDAL dal = new ListNotifikasiDAL();
                    result = dal.GetData(draw, start, length, search, user.KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [Authorize]
        public JsonResult GetDataDropDownRole()
        {
            ListNotifikasiDAL dal = new ListNotifikasiDAL();
            IEnumerable<DDPosition> result = dal.GetDataListPosition();
            // result = result.Where(r => r.PROPERTY_ROLE != null);
            return Json(new { data = result });
        }

        [Authorize]
        public JsonResult ViewDetail(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; 
            if(isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ListNotifikasiDAL dal = new ListNotifikasiDAL();
                    result = dal.GetViewDetail(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult InsertHeader([FromBody] DataListNotifikasiHeader data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    ListNotifikasiDAL dal = new ListNotifikasiDAL();
                    result = (data.ID != null && data.ID != "" ? dal.EditDataHeader(data, user.Name) : dal.AddDataHeader(data, user.Name));

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult InsertDetail([FromBody] DataListNotifikasiDetail data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    ListNotifikasiDAL dal = new ListNotifikasiDAL();
                    result = (data.ID != null ? dal.EditDataDetail(data, user.Name) : dal.AddDataDetail(data, user.Name));

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        public JsonResult EditDataDetail(string id)
        {
            ListNotifikasiDAL dal = new ListNotifikasiDAL();

            //ROLE
            IEnumerable<ListRoleRow> result = dal.GetDataDetailRow(id);
            //result = result.Where(r => r.PROPERTY_ROLE != null);

            //param
            //IEnumerable<ParamRow> param = dal.GetDataDetailParam(id);

            return Json(new { data = result });
        }

        [Authorize]
        [HttpPost]
        public JsonResult DeleteHeader([FromBody] DataListNotifikasiDetail data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest"; if(isAjax)
            {
                try
                {
                    ListNotifikasiDAL dal = new ListNotifikasiDAL();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
    }
}