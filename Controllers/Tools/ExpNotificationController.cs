﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.DynamicDatatable;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ExpNotificationController : Controller
    {
        public object CurrentUser { get; private set; }

        // GET: ExpNotification
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            var user = CurrentUser;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.GetDataExp(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        [HttpPost]
        public JsonResult Status([FromBody] EXP_NOTIF data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.StatusExpNotif(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
        [HttpPost]
        public JsonResult Insert([FromBody] EXP_NOTIF data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.InsertExpNotif(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
    }
}