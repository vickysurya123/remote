﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.Role;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        public ActionResult Index()
        {
            return View("RoleIndex");
        }

        public ActionResult AddRole()
        {
            return View();
        }

        //-------------------------------------- VIEW DATA ROLE ------------------------------
        public JsonResult GetDataRole()
        {
            DataTablesRole result = new DataTablesRole();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    RoleDAL dal = new RoleDAL();
                    result = dal.GetDataRole(draw, start, length, search);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //------------------------------------- INSERT DATA ROLE --------------------------
        [HttpPost]
        public JsonResult InstDataRole([FromBody] DataRole data)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    RoleDAL dal = new RoleDAL();
                    bool result = dal.AddDataRole(User.Identity.Name, data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        //------------------------------------- UPDATE DATA ROLE --------------------------
        [HttpPost]
        public JsonResult EditDataRole([FromBody] DataRole xDATA)
        {
            dynamic message = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    RoleDAL dal = new RoleDAL();
                    bool result = dal.UpdateDataRole(User.Identity.Name, xDATA);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

	}
}