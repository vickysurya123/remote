﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Helper;
using Remote.Helpers;
using Remote.Models;
using Remote.Models.ApprovalSetting;
using Remote.Models.DynamicDatatable;
using Remote.Models.EmailConfiguration;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class ApprovalSettingController : Controller
    {
        // GET: ApprovalSetting
        public ActionResult Index()
        {
            var user = CurrentUser;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(user.KodeCabang),
            };
            
            return View("ApprovalSettingIndex", viewModel);
        }
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();
            var user = CurrentUser;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.GetData(draw, start, length, search, user.KodeCabang);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult InsertHeader([FromBody] DataApprovalSettingHeader data)
        {
            dynamic result = null;


            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result =(data.ID != null && data.ID != "" ? dal.EditDataHeader(data, user.Name) : dal.AddDataHeader(data, user.Name)) ;

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult DeleteHeader([FromBody] DataApprovalSettingHeader data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.DeleteDataHeader(data);

                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public ActionResult AddParam(string mimoto)
        {
            string id = Base64Utils.DecodeFrom64(mimoto);
            ViewBag.IdHeader = id;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            try
            {
                APPROVAL_SETTING_HEADER data = dal.GetData(id);
                ViewBag.NamaCabang = data.BE_NAME;
                ViewBag.KdCabang = data.BE_ID;
                ViewBag.KdModul = data.MODUL_ID;
            }
            catch (Exception e)
            {
                string aaa = e.ToString();
            }

            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
                DDPosition = dal.GetDataListPosition(),
                DDRentalType = dal.GetDataRentalType(),
                DDContractUsage = dal.GetDataContractUsage(),
            };


            return View("ApprovalSettingDetail",viewModel);

        }
        public JsonResult GetDataDetail(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.GetDataDetail(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public JsonResult GetDataDropDownRole()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            IEnumerable<DDPosition> result = dal.GetDataListPosition();
            result = result.Where(r => r.PROPERTY_ROLE != null);
            return Json(new { data = result });
        }

        public JsonResult GetDataDropDownParam()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            IEnumerable<APPROVAL_SETTING_PARAMETER> result = dal.GetDataListParam();
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult InsertDetail([FromBody] DataApprovalSettingDetail data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var user = CurrentUser;
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = (data.ID != null && data.ID != "" ? dal.EditDataDetail(data, user.Name) : dal.AddDataDetail(data, user.Name));

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
        public JsonResult ViewDetail(string id)
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.GetViewDetail(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult ViewDetailParam(string id)
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.GetViewDetailParam(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult DeleteDetail([FromBody] DataApprovalSettingDetail data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.DeleteDataDetail(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        public JsonResult EditDataDetail(string id)
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();

            //ROLE
            IEnumerable<RoleRow> result = dal.GetDataDetailRow(id);
            result = result.Where(r => r.PROPERTY_ROLE != null);

            //param
            IEnumerable<ParamRow> param = dal.GetDataDetailParam(id);

            return Json(new { data = result, param = param });
        }
        public JsonResult SendMail(EmailConf data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    EmailConfigurationDAL dal = new EmailConfigurationDAL();
                    result = dal.SendMail(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
        public JsonResult SearchBranch(DataApprovalSettingHeader data)
        {
            dynamic result = null;

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    ApprovalSettingDAL dal = new ApprovalSettingDAL();
                    result = dal.SearchBranch(data);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }
    }
}