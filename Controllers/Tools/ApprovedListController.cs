﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models.TransContractOffer;
using Remote.Models.ApprovedList;
using Remote.Models;
using Remote.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dapper;
using System.Security.Claims;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using RestSharp;
using Newtonsoft.Json;

namespace Remote.Controllers
{
    [Authorize]
    public class ApprovedListController : Controller
    {
        //
        // GET: /ApprovedList/
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ViewBag.KodeCabang = KodeCabang;
            return View("ApprovedListIndex");
        }

        //--------------------------- DATA TABLES CONTRACT OFFER --------------------- 
        public JsonResult GetDataHeaderContractOffer()
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;
                    string UserRole = CurrentUser.UserRoleID;
                    string UserProperty = CurrentUser.UserRoleProperty;

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataHeaderContractOffernew(draw, start, length, search, KodeCabang, UserRole, UserProperty);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        

        //--------------------------- DATA TABLES HISTORY WORKFLOW --------------------- 
        public JsonResult GetDataHistoryWorkflow(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataHistoryWorkflownew(draw, start, length, search, KodeCabang, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- NEW DATA TABLES HISTORY WORKFLOW --------------------- 
        public JsonResult GetDataHistoryWorkflowNew(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    string KodeCabang = CurrentUser.KodeCabang;

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataHistoryWorkflownew(draw, start, length, search, KodeCabang, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL OBJECT --------------------- 
        public JsonResult GetDataDetailObject(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataDetailObject(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL MANUAL --------------------- 
        public JsonResult GetDataDetailManualy(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataDetailManualy(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailCondition(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataDetailCondition(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        //--------------------------- DATA TABLES DETAIL CONDITION --------------------- 
        public JsonResult GetDataDetailMemo(string id)
        {
            DataTablesApprovedList result = new DataTablesApprovedList();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataDetailMemo(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        public JsonResult GetDataAttachment(string id)
        {
            DataTablesAttachmentOffer result = new DataTablesAttachmentOffer();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    ApprovedListDAL dal = new ApprovedListDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id);
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ContentResult> UploadFilesAsync([FromForm] AttachmentApproved data)
        {
            var XKD_CABANG = CurrentUser.KodeCabang;
            var r = new List<UploadFilesResult>();

            foreach (var file in Request.Form.Files)
            {
                string subPath = data.BE_ID;
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string filePath = Path.Combine(rootPath, "REMOTE", "ContractOffer", subPath);

                bool exists = Directory.Exists(filePath);

                if (!exists)
                    System.IO.Directory.CreateDirectory(filePath);

                var sFile = "";
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);

                sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                string savedFileName = Path.Combine(filePath, sFile);
                using (var stream = new FileStream(savedFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string directory = "~/REMOTE/ContractOffer/" + subPath;
                string KodeCabang = CurrentUser.KodeCabang;

                dynamic getData = null;
                GetUrl test = new GetUrl();
                string url = test.UploadCloadUrl + "upload";
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.None,
                    AlwaysMultipartFormData = true
                };
                request.AddHeader("content-type", "multipart/form-data");
                request.AddHeader("Accept", "application/json");
                request.AddParameter("folder", "REMOTE/ContractOffer/" + subPath);
                request.AddFile("file", savedFileName);
                request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                IRestResponse responseOrg = client.Execute<dynamic>(request);
                var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                string file2 = list.message.url;

                ApprovedListDAL dal = new ApprovedListDAL();
                bool result = dal.AddFiles(sFile, file2, KodeCabang, subPath);

                r.Add(new UploadFilesResult()
                {
                    Name = sFile,
                    Length = Convert.ToInt32(file.Length),
                    Type = file.ContentType,
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {

                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "ContractOffer", uri, filename);

                //string fullPath = Request.MapPath(directory + "/" + filename);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                ApprovedListDAL dal = new ApprovedListDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception ex)
            {

            }

            return Json(message);
        }
	}
}