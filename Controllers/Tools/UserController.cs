﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Remote.DAL;
using Remote.Entities;
using Remote.Models;
using Remote.Models.DataTablesUser;
using Remote.Models.MasterInstallation;
using Remote.Models.UserModels;
using Remote.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Remote.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Index()
        {
            string KodeCabang = CurrentUser.KodeCabang;
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListAll(),
            };
            ViewBag.role = CurrentUser.UserRoleID;
            return View("UserIndex", viewModel);
        }

        [HttpPost]
        public JsonResult GetProfitCenternew([FromBody] string be_id)
        {
            string KodeCabang = CurrentUser.KodeCabang;
            UserDAL dal = new UserDAL();
            IEnumerable<DDProfitCenter> result = dal.GetDataProfitCenter(be_id);
            return Json(new { data = result });
        }

        public ActionResult EditUser(string id)
        {
            /*ViewBag.aidi = id;
            UserDAL dal = new UserDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(),
                DDPosition = dal.GetDataListPosition(),
            };
            return View(viewModel);*/

            ViewBag.aidi = id;
            UserDAL dal = new UserDAL();
            try
            {
                AUP_DATA_USER data = dal.GetDataForEdit(id);

                ViewBag.UserLogin = data.USER_LOGIN;
                ViewBag.UserName = data.USER_NAME;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                ViewBag.UserEmail = data.USER_EMAIL;
                ViewBag.UserPhone = data.USER_PHONE;
                ViewBag.ROLE_NAME = data.ROLE_NAME;
                ViewBag.BRANCH_ID = data.KD_CABANG;

                ViewBag.DB_ID = data.BE_ID;
                ViewBag.DP_ID = data.ROLE_ID;
            }
            catch (Exception e)
            {
                //ViewBag.BE_ID = string.Empty;
                //ViewBag.BE_NAME = string.Empty;
                //ViewBag.BE_ADDRESS = string.Empty;
                //ViewBag.BE_CITY = string.Empty;
                //ViewBag.BE_PROVINCE = string.Empty;
                //ViewBag.BE_CONTACT_PERSON = string.Empty;
                //ViewBag.BE_NPWP = string.Empty;
                //ViewBag.BE_NOPPKP = string.Empty;
                //ViewBag.BE_PPKP_DATE = string.Empty;
                //ViewBag.CREATION_BY = string.Empty;
                //ViewBag.CREATION_DATE = string.Empty;
                //ViewBag.LAST_UPDATE_BY = string.Empty;
                //ViewBag.LAST_UPDATE_DATE = string.Empty;
                //ViewBag.VALID_FROM = string.Empty;
                //ViewBag.VALID_TO = string.Empty;
                //ViewBag.HARBOUR_CLASS = string.Empty;
                //ViewBag.POSTAL_CODE = string.Empty;
                string aaa = e.ToString();
            }

            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(),
                DDPosition = dal.GetDataListPosition()
            };


            return View(viewModel);

        }

        public ActionResult CloneUsers(string id)
        {
            /*ViewBag.aidi = id;
            UserDAL dal = new UserDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(),
                DDPosition = dal.GetDataListPosition(),
            };
            return View(viewModel);*/

            ViewBag.aidi = id;
            UserDAL dal = new UserDAL();
            try
            {
                AUP_DATA_USER data = dal.GetDataForEdit(id);

                ViewBag.UserLogin = data.USER_LOGIN;
                ViewBag.UserName = data.USER_NAME;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.UserEmail = data.USER_EMAIL;
                ViewBag.UserPhone = data.USER_PHONE;
                ViewBag.ROLE_NAME = data.ROLE_NAME;

                ViewBag.DB_ID = data.BE_ID;
                ViewBag.DP_ID = data.ROLE_ID;
            }
            catch (Exception e)
            {
                //ViewBag.BE_ID = string.Empty;
                //ViewBag.BE_NAME = string.Empty;
                //ViewBag.BE_ADDRESS = string.Empty;
                //ViewBag.BE_CITY = string.Empty;
                //ViewBag.BE_PROVINCE = string.Empty;
                //ViewBag.BE_CONTACT_PERSON = string.Empty;
                //ViewBag.BE_NPWP = string.Empty;
                //ViewBag.BE_NOPPKP = string.Empty;
                //ViewBag.BE_PPKP_DATE = string.Empty;
                //ViewBag.CREATION_BY = string.Empty;
                //ViewBag.CREATION_DATE = string.Empty;
                //ViewBag.LAST_UPDATE_BY = string.Empty;
                //ViewBag.LAST_UPDATE_DATE = string.Empty;
                //ViewBag.VALID_FROM = string.Empty;
                //ViewBag.VALID_TO = string.Empty;
                //ViewBag.HARBOUR_CLASS = string.Empty;
                //ViewBag.POSTAL_CODE = string.Empty;

                string aaa = e.ToString();
            }

            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(),
                DDPosition = dal.GetDataListPosition()
            };


            return View(viewModel);

        }

        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult AddUser()
        {
            UserDAL dal = new UserDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(),
                DDPosition = dal.GetDataListPosition(),
            };
            return View(viewModel);
            //return View();
        }

        public JsonResult GetDataUser(string kdCabang)
        {
            DataTablesUser result = new DataTablesUser();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    string KodeCabang = kdCabang == null || kdCabang == "" ? CurrentUser.KodeCabang : kdCabang;

                    UserDAL dal = new UserDAL();
                    result = dal.GetDataUser(draw, start, length, KodeCabang, search);
                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult AddUser1([FromBody] DataUser data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result = dal.AddUserDAL(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Added Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult EditUser1([FromBody] DataUser data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result = dal.UpdateUserDAL(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult CloneUser1([FromBody] DataUser data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result = dal.CloneUserDAL(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Clone Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Clone Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult CloneStatusMenu1([FromBody] DataUserMenu[] data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    for (var i = 0; i < data.Length; i++)
                    {
                        bool result = dal.CloneStatusUserMenuDAL(data[i].MENU_ID, data[i].STATUS, data[i].ROLE_ID);
                        if (result)
                        {
                            message = new
                            {
                                status = "S",
                                message = "Data Clone Successfully."
                            };
                        }
                        else
                        {
                            message = new
                            {
                                status = "E",
                                message = "Failed to Clone Data."
                            };
                        }
                    }


                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult DeleteUpdateMenu1(DataUserMenu data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result = dal.DeleteUpdateMenuDAL(data);

                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult UpdateMenu1(DataUserMenu data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result2 = dal.UpdateMenuDAL(data);

                    if (result2)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult EditStatus1([FromBody] DataUser data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result2 = dal.UpdateStatusUserDAL(data);

                    if (result2)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        [HttpPost]
        public JsonResult EditStatusMenu1([FromBody] DataUserMenu[] data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    UserDAL dal = new UserDAL();
                    bool result = dal.UpdateStatusUserMenuDAL(data);
                    if (result)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Data Update Successfully."
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Update Data."
                        };
                    }


                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Terjadi error. " + e.Message
                    };
                }
            }

            return Json(message);
        }

        public JsonResult getRoleMenu(string xIDRole)
        {
            IEnumerable<VW_ROLE_MENU> result = null;
            UserDAL dal = new UserDAL();
            result = dal.getDataRoleMenu(xIDRole);
            return Json(result);
        }

        public JsonResult getUserMenu(string xID, string xIDRole)
        {
            IEnumerable<VW_USER_MENU> result = null;
            UserDAL dal = new UserDAL();
            result = dal.getDataRole(xID, xIDRole);
            return Json(result);
        }
    }
}