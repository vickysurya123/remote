﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remote.DAL;
using Remote.ViewModels;
using Remote.Models.TarifPedoman;
using Remote.Models.DynamicDatatable;
using System.Security.Claims;
using Remote.Models;
using System.IO;
using Remote.Models.MasterRentalObject;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using RestSharp;
using Newtonsoft.Json;

namespace Remote.Controllers
{
    [Authorize]
    public class TarifPedomanController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        private IWebHostEnvironment _hostingEnvironment;
        public TarifPedomanController(IWebHostEnvironment webHostEnvironment, IWebHostEnvironment environment)
        {
            _hostingEnvironment = environment;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: TarifPedoman
        public ActionResult Index()
        {
            ViewBag.KodeCabang = CurrentUser.KodeCabang;
            return View("TarifIndustriIndex");
        }


        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }

        public ActionResult Add()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
            };
            return View("TarifIndustriAdd", viewModel);
        }

        public ActionResult Edit(string id)
        {
            TarifPedomanDAL dal = new TarifPedomanDAL();

            try
            {
                DataTarifPedoman data = dal.GetPedoman(int.Parse(id));
                ViewBag.ID = id;
                ViewBag.BE_ID = data.BE_ID;
                ViewBag.BE_NAME = data.BE_NAME;
                ViewBag.BRANCH_ID = data.BRANCH_ID;
                ViewBag.PROFIT_CENTER_ID = data.PROFIT_CENTER_ID;
                ViewBag.PROFIT_CENTER_NAME = data.PROFIT_CENTER_NAME;
                ViewBag.START_ACTIVE_DATE = data.START_ACTIVE_DATE;
                ViewBag.END_ACTIVE_DATE = data.END_ACTIVE_DATE;
                ViewBag.TIPE_TARIF = data.TIPE_TARIF;
            }
            catch
            {
                ViewBag.ID = string.Empty;
                ViewBag.BE_ID = string.Empty;
                ViewBag.BE_NAME = string.Empty;
                ViewBag.BRANCH_ID = string.Empty;
                ViewBag.PROFIT_CENTER_ID = string.Empty;
                ViewBag.PROFIT_CENTER_NAME = string.Empty;
                ViewBag.START_ACTIVE_DATE = string.Empty;
                ViewBag.END_ACTIVE_DATE = string.Empty;
                ViewBag.TIPE_TARIF = string.Empty;
            }
            return View("TarifIndustriEdit");
        }

        public ActionResult Njop(string cabang)
        {
            try
            {
                ViewBag.PARAM = cabang;
            } catch
            {
                ViewBag.PARAM = string.Empty;
            }

            ApprovalSettingDAL dal1 = new ApprovalSettingDAL();
            //MasterRentalObjectDAL dal2 = new MasterRentalObjectDAL();
            var viewModel = new TarifPedoman
            {
                DDBranch = dal1.GetDataListBranch(CurrentUser.KodeCabang),
                //DDProfitCenters = dal2.GetDataProfitCenter(CurrentUser.KodeCabang),
            };
            return View("TarifNjop", viewModel);
        }

        [HttpPost]
        public JsonResult SaveHeader([FromBody] DataTarifPedoman data)
        {
            dynamic message = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    //string KodeCabang = CurrentUser.KodeCabang;

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    var result = dal.AddPedoman(CurrentUser.Name, data);

                    if (result.RESULT_STAT)
                    {
                        message = new
                        {
                            status = "S",
                            message = "Tarif Pedoman telah disimpan!"
                        };
                    }
                    else
                    {
                        message = new
                        {
                            status = "E",
                            message = "Failed to Add Data." + result.MessageInfo
                        };
                    }
                }
                catch (Exception e)
                {
                    message = new
                    {
                        status = "E",
                        message = "Something went wrong. " + e.Message
                    };
                }
            }

            return Json(message);
        }
        
        public JsonResult GetActivePedoman(string be_id, string profit_center)
        {
            if (string.IsNullOrEmpty(profit_center))
            {
                profit_center = "0";
            }
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.GetActivePedoman(be_id, profit_center);
            }
            return Json(result);
        }

        public JsonResult GetDataActivePedoman(string be_id, string kode_industri, string batas_luas)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int beid = int.Parse(be_id);
                    int batas = int.Parse(batas_luas);
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataActivePedoman(beid, kode_industri, batas, draw, start, length, search);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        public JsonResult GetPedoman(string id)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.GetPedoman(int.Parse(id));
            }
            return Json(result);
        }

        public JsonResult GetNjop(string id)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.GetNjop(int.Parse(id));
            }
            return Json(result);
        }

        // be_id INI YANG DIMAKSUD ADALAH BRANCH_ID
        public JsonResult GetDataNJOP(string be_id)
        {
            IDataTable result = new IDataTable();

            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    var queryString = Request.Query;
                    int beid = int.Parse(be_id);
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataNJOP(beid, draw, start, length, search);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        // be_id INI YANG DIMAKSUD ADALAH BE_ID
        public JsonResult GetDataActiveNJOP(string be_id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int beid = int.Parse(be_id);
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataActiveNJOP(beid, draw, start, length, search);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult GetProfitCenter([FromBody] string be_id)
        {
            MasterRentalObjectDAL dal = new MasterRentalObjectDAL();
            IEnumerable<FDDProfitCenter> result = dal.GetProfitCenter(be_id);
            return Json(new { data = result });
        }

        public JsonResult GetData()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    int kd_cabang = int.Parse(CurrentUser.KodeCabang);

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDatanew(draw, start, length, search, kd_cabang);
                }
                catch (Exception) { }
            }

            return Json(result);
        }
        
        public JsonResult GetDataHistory()
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];
                    int kd_cabang = int.Parse(CurrentUser.KodeCabang);

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataHistorynew(draw, start, length, search, kd_cabang);
                }
                catch (Exception) { }
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult SaveNJOP([FromBody] DataNJOP data)
        {
            dynamic result = null;

            if (IsAjaxUtil.validate(HttpContext))
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.AddNJOP(CurrentUser.Name, data);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult InactiveDataNJOP([FromBody] DataNJOP data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.InactiveNJOP(CurrentUser.Name, data);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult ActiveDataNJOP([FromBody] DataNJOP data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.ActiveNJOP(CurrentUser.Name, data);
            }
            return Json(result);
        }
        
        public JsonResult InactivatePedoman(TARIF_PEDOMAN_HEADER data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.InactivePedoman(CurrentUser.Name, data);
            }
            return Json(result);
        }

        public JsonResult ActivatePedoman(TARIF_PEDOMAN_HEADER data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                TarifPedomanDAL dal = new TarifPedomanDAL();
                result = dal.ActivePedoman(CurrentUser.Name, data);
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<ContentResult> UploadFilesPedomanAsync([FromForm] AttachmentTarif data)
        {
            //var XKD_CABANG = CurrentUser.KodeCabang;
            var r = new List<UploadFilesResult>();
            //var filePath = Path.GetTempFileName();
            foreach (var file in Request.Form.Files)
            {
                if (file.Length > 0)
                {

                    string subPath = data.idPedoman;
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    string filePath = Path.Combine(rootPath, "REMOTE", "TarifPedoman", subPath);

                    bool exists = Directory.Exists(filePath);

                    if (!exists)
                        Directory.CreateDirectory(filePath);

                    var sFile = "";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileExt = Path.GetExtension(file.FileName);

                    sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                    string savedFileName = Path.Combine(filePath, sFile);
                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    string directory = "~/REMOTE/TarifPedoman/" + subPath;
                    //string KodeCabang = CurrentUser.KodeCabang;

                    //upload cloud p3
                    dynamic getData = null;
                    GetUrl test = new GetUrl();
                    string url = test.UploadCloadUrl + "upload";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.None,
                        AlwaysMultipartFormData = true
                    };
                    request.AddHeader("content-type", "multipart/form-data");
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("folder", "TMP/Information/" + subPath);
                    request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                    request.AddFile("file", savedFileName);
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    string file2 = list.message.url;


                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    bool result = dal.AddFiles(sFile, file2, subPath, "PEDOMAN");

                    r.Add(new UploadFilesResult()
                    {
                        Name = sFile,
                        Length = Convert.ToInt32(file.Length),
                        Type = file.ContentType,
                    });

                }

            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        [HttpPost]
        public async Task<ContentResult> UploadFilesNjop([FromForm] AttachmentTarif data)
        {
            //var XKD_CABANG = CurrentUser.KodeCabang;
            var r = new List<UploadFilesResult>();

            foreach (var file in Request.Form.Files)
            {
                if (file.Length > 0)
                {
                    string subPath = data.idPedoman;

                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    string filePath = Path.Combine(rootPath, "REMOTE", "TarifNjop", subPath);

                    bool exists = System.IO.Directory.Exists(filePath);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(filePath);

                    var sFile = "";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileExt = Path.GetExtension(file.FileName);

                    sFile = fileName + DateTime.Now.ToString("yyyyMMddHHmmss") + fileExt;

                    string savedFileName = Path.Combine(filePath, sFile);

                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    string directory = "~/REMOTE/TarifNjop/" + subPath;
                    string KodeCabang = CurrentUser.KodeCabang;

                    //upload cloud p3
                    dynamic getData = null;
                    GetUrl test = new GetUrl();
                    string url = test.UploadCloadUrl + "upload";
                    RestClient client = new RestClient(url);
                    RestRequest request = new RestRequest(Method.POST)
                    {
                        RequestFormat = DataFormat.None,
                        AlwaysMultipartFormData = true
                    };
                    request.AddHeader("content-type", "multipart/form-data");
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("folder", "REMOTE/TarifNjop/" + subPath);
                    request.AddParameter("api_key", "ZNxHHgfl9K5o7Ywb4cG68YssTj227QRR2+4htsKlHS0OU8RKguBvfhy2lYhIkzZ5tcmZL4WWuzw1hdjYH3w=");
                    request.AddFile("file", savedFileName);
                    IRestResponse responseOrg = client.Execute<dynamic>(request);
                    var list = JsonConvert.DeserializeObject<dynamic>(responseOrg.Content);
                    string file2 = list.message.url;

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    bool result = dal.AddFiles(sFile, file2, subPath, "NJOP");

                    r.Add(new UploadFilesResult()
                    {
                        Name = sFile,
                        Length = Convert.ToInt32(file.Length),
                        Type = file.ContentType,
                    });
                }
               
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult DeleteDataAttachment(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "TarifPedoman", uri, filename);

                //string fullPath = Request.MapPath(directory + "/" + filename);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                TarifPedomanDAL dal = new TarifPedomanDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult DeleteDataAttachmentNjop(string id, string directory, string filename, string uri)
        {
            dynamic message = null;
            string dir = directory;
            try
            {
                string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                string fullPath = Path.Combine(rootPath, "REMOTE", "TarifNjop", uri, filename);

                //string fullPath = Request.MapPath(directory + "/" + filename);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                TarifPedomanDAL dal = new TarifPedomanDAL();
                message = dal.DeleteDataAttachment(id);
            }
            catch (Exception)
            {

            }

            return Json(message);
        }

        public JsonResult GetDataAttachmentPedoman(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id, "PEDOMAN");
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }
        public JsonResult GetDataAttachmentNjop(string id)
        {
            IDataTable result = new IDataTable();

            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    var queryString = Request.Query;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search[value]"];

                    TarifPedomanDAL dal = new TarifPedomanDAL();
                    result = dal.GetDataAttachment(draw, start, length, search, id, "NJOP");
                }
                catch (Exception)
                {
                }
            }

            return Json(result);
        }

    }
}