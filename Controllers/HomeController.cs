﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Remote.Models;
using System.Security.Claims;
using Remote.DAL;
using Remote.Entities;
using Remote.ViewModels;
using System.Data;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using DocumentFormat.OpenXml.Drawing;
using Microsoft.AspNetCore.Hosting;

namespace Remote.Controllers
{
    public class HomeController : Controller
    {
        public AppUser CurrentUser
        {
            get
            {
                return new AppUser(this.User as ClaimsPrincipal);
            }
        }
        [Authorize]
        public ActionResult Index()
        {
            ApprovalSettingDAL dal = new ApprovalSettingDAL();
            var viewModel = new UserViewModels
            {
                DDBranch = dal.GetDataListBranch(CurrentUser.KodeCabang),
            };
            //ViewBag.kodeCabang = user.KodeCabang;
            //ViewBag.namaCabang = user.NamaCabang;
            return View(viewModel);
        }

        [Authorize]
        public JsonResult GetData(SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.dataDashboard(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetDetail([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetData(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetDetailRkap(string kdCabang, int triwulan, int tahun)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDataRkap(kdCabang, triwulan, tahun);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult defineExcel([FromBody] DashExcelParam param)
        {
            nama result = new nama();
            var tableName = "";
            if (param.TYPE == "1")
            {
                tableName = "Kontrak Aktif";
            }
            else if (param.TYPE == "2")
            {
                tableName = "Kontrak yang akan Habis";
            }
            else if (param.TYPE == "3")
            {
                tableName = "Kontrak Habis";
            }
            else if (param.TYPE == "4")
            {
                tableName = "Kontrak Diperpanjang";
            }
            else if (param.TYPE == "5")
            {
                tableName = "Kontrak Tidak Diperpanjang";
            }
            else if (param.TYPE == "6")
            {
                tableName = "Kontrak Baru";
            }
            string fileName = "ReportDashboard(" + tableName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                //string KodeCabang = "100";
                HomeDAL dal = new HomeDAL();
                var dalShowdata = dal.getExcel(param.TYPE, param.BUSINESS_ENTITY, param.START_DATE, param.END_DATE, CurrentUser.KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {
                new DataColumn("CABANG", typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("START DATE", typeof(string)),
                new DataColumn("END DATE",typeof(string)),
                new DataColumn("CUSTOMER", typeof(string)),
                new DataColumn("BADAN USAHA ",typeof(string)),
                new DataColumn("TOTAL NILAI ",typeof(string)),
                new DataColumn("TOTAL LUAS TANAH", typeof(string)),
                new DataColumn("TOTAL LUAS BANGUNAN",typeof(string)),
                new DataColumn("STATUS", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE_NAME, valData.CONTRACT_NO, valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE,
                                valData.BUSINESS_PARTNER + " - " + valData.BUSINESS_PARTNER_NAME, valData.MPLG_BADAN_USAHA, valData.TOTAL_NET_VALUE,
                                valData.LUAS_TANAH, valData.LUAS_BANGUNAN, valData.STATUS);
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "Report Dashboard");
                //    //wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    HttpContext.Response.AppendTrailer("content-disposition", "attachment; filename=" + fileName);
                //}
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Dashboard");
                    wb.SaveAs(System.IO.Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }
        public class nama
        {
            public string fileNames { get; set; }
        }
        [Authorize]
        public JsonResult GetPiutang(SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.dataPiutang(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetDetailPiutang([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDetailPiutang(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }
            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult defineExcel_4([FromBody] DashExcelParam param)
        {
            nama result = new nama();
            var tableName = "";
            var kolomName = "";
            if (param.TYPE == "1")
            {
                tableName = "PROPERTY";
                kolomName = "NO KONTRAK";
            }
            else if (param.TYPE == "2")
            {
                tableName = "WATER AND ELECTRICITY";
                kolomName = "NO INSTALASI";
            }
            else if (param.TYPE == "3")
            {
                tableName = "OTHER SERVICES";
                kolomName = "KELOMPOK PENDAPATAN";
            }
            string fileName = "ReportPiutang(" + tableName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                HomeDAL dal = new HomeDAL();
                var dalShowdata = dal.getExcelPiutang(param.TYPE, param.BUSINESS_ENTITY, param.END_DATE, CurrentUser.KodeCabang);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[8] {
                new DataColumn("CABANG", typeof(string)),
                new DataColumn("JENIS TAGIHAN", typeof(string)),
                new DataColumn("NOMOR NOTA", typeof(string)),
                new DataColumn(kolomName,typeof(string)),
                new DataColumn("KODE CUSTOMER", typeof(string)),
                new DataColumn("NAMA CUSTOMER ",typeof(string)),
                new DataColumn("TANGGAL TRANSAKSI ",typeof(string)),
                new DataColumn("JUMLAH TAGIHAN", typeof(string))});
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(valData.BE_NAME, valData.JENIS_TAGIHAN, valData.BILLING_NO, valData.CODE,
                                valData.CUSTOMER_CODE, valData.CUSTOMER_NAME, valData.PSTNG_DATE, valData.AMOUNT);
                }

                //Codes for the Closed XML
                //using (XLWorkbook wb = new XLWorkbook())
                //{
                //    wb.Worksheets.Add(dt, "Report Dashboard Piutang");
                //    // wb.SaveAs(Path.Combine(Server.MapPath("~/REMOTE/Excel/") + fileName));
                //    Response.Clear();

                //    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    HttpContext.Response.AppendTrailer("content-disposition", "attachment; filename=" + fileName);
                //}
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Dashboard Piutang");
                    wb.SaveAs(System.IO.Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }
        [Authorize]
        public JsonResult GetRO(SearchDashboard param)
       {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.dataRO(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetPenggunaanLahan(SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.DataPenggunaanLahan(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetDetailLahan([FromBody] SearchDashboardLahan data)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.DetailPenggunaanLahan(data, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetRODetail([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.dataRODetail(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }


        // SLICE PIE CHART BELUM BERSERTIFIKAT//
        [Authorize]
        [HttpPost]
        public JsonResult GetDetailSertifikat([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDetailSertifikat(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }

        //SLICE PIE CHART BERSERTIFIKAT//
        [Authorize]
        [HttpPost]
        public JsonResult GetDetailSertifikat2([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            bool isAjax = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            if (isAjax)
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDetailSertifikat2(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }


        //SLICE PIE CHART BERSERTIFIKAT DAN BELUM BERSERTIFIKAT//
        [Authorize]
        [HttpPost]
        public JsonResult GetDetailSertifikat3([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDetailSertifikat3(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }



        [Authorize]
        [HttpPost]
        public JsonResult GetDetailRO([FromBody] SearchDashboard param)
        {
            dynamic result = null;
            if (IsAjaxUtil.validate(HttpContext))
            {
                try
                {
                    HomeDAL dal = new HomeDAL();
                    result = dal.GetDetailRO(param, CurrentUser.KodeCabang);

                }
                catch (Exception e)
                {
                    string aaa = e.ToString();
                }
            }

            //var jsonResult = Json(result);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public JsonResult defineExcel_5([FromBody] DashExcelParam parame)
        {
            nama result = new nama();
            var tableName = parame.STATUS + " - " + parame.TYPE;
            // var kolomName = "";
            string fileName = "ReportRO(" + tableName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                SearchDashboard param = new SearchDashboard();
                param.BE_ID = parame.BUSINESS_ENTITY;
                param.jenis = parame.STATUS;
                param.jenis2 = parame.TYPE;
                param.end = parame.END_DATE;
                //string KodeCabang = "100";
                HomeDAL dal = new HomeDAL();
                var temp = dal.GetDetailRO(param, CurrentUser.KodeCabang);
                dynamic dalShowdata = temp.Data;

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[23] {
                new DataColumn("BUSINESS ENTITY", typeof(string)),
                new DataColumn("PROFIT CENTER", typeof(string)),
                new DataColumn("RO NUMBER", typeof(string)),
                new DataColumn("RO NAME",typeof(string)),
                new DataColumn("ZONE ZIP", typeof(string)),
                new DataColumn("LAND DIMENSION",typeof(string)),
                new DataColumn("BUILDING DIMENSION", typeof(string)),
                new DataColumn("CONTRACT NO", typeof(string)),
                new DataColumn("CONTRACT OFFER", typeof(string)),
                new DataColumn("CUSTOMER", typeof(string)),
                new DataColumn("BADAN USAHA", typeof(string)),
                new DataColumn("CERTIFICATE NO",typeof(string)),
                new DataColumn("USAGE TYPE", typeof(string)),
                new DataColumn("FUNCTION",typeof(string)),
                new DataColumn("VACANCY STATUS", typeof(string)),
                new DataColumn("VACANCY REASON",typeof(string)),
                new DataColumn("VALID FROM",typeof(string)),
                new DataColumn("VALID TO",typeof(string)),
                new DataColumn("ADDRESS",typeof(string)),
                new DataColumn("POSTAL CODE", typeof(string)),
                new DataColumn("CITY",typeof(string)),
                new DataColumn("PROVINCE",typeof(string)),
                new DataColumn("STATUS",typeof(string))
                });
                foreach (var valData in dalShowdata)
                {
                    if (valData.ACTIVE == "1")
                    {
                        dt.Rows.Add(valData.BE_ID, valData.PROFIT_CENTER, valData.RO_CODE, valData.RO_NAME,
                                    valData.ZONE_RIP, valData.LAND_DIMENSION, valData.BUILDING_DIMENSION,
                                    valData.CONTRACT_NO, valData.CONTRACT_OFFER_NO, valData.MPLG_KODE + " - " + valData.MPLG_NAMA, valData.MPLG_BADAN_USAHA,
                                    valData.RO_CERTIFICATE_NUMBER, valData.USAGE_TYPE, valData.FUNCTION_ID,
                                    valData.STATUS, valData.REASON, valData.VALID_FROM, valData.VALID_TO,
                                    valData.RO_ADDRESS, valData.RO_POSTALCODE, valData.RO_CITY, valData.PROVINCE, "ACTIVE");
                    }
                    else
                    {
                        dt.Rows.Add(valData.BE_ID, valData.PROFIT_CENTER, valData.RO_CODE, valData.RO_NAME,
                                    valData.ZONE_RIP, valData.LAND_DIMENSION, valData.BUILDING_DIMENSION,
                                    valData.CONTRACT_NO, valData.CONTRACT_OFFER_NO, valData.MPLG_KODE + " - " + valData.MPLG_NAMA, valData.MPLG_BADAN_USAHA,
                                    valData.RO_CERTIFICATE_NUMBER, valData.USAGE_TYPE, valData.FUNCTION_ID,
                                    valData.STATUS, valData.REASON, valData.VALID_FROM, valData.VALID_TO,
                                    valData.RO_ADDRESS, valData.RO_POSTALCODE, valData.RO_CITY, valData.PROVINCE, "INACTIVE");
                    }
                }

                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                        wb.Worksheets.Add(dt, "Report Dashboard Piutang");
                        wb.SaveAs(System.IO.Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                    }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }

        [Authorize]
        [HttpPost]
        public JsonResult defineExcel_6([FromBody] DashExcelParam parame)
        {
            nama result = new nama();
            var tableName = parame.TYPE;
            // var kolomName = "";
            string fileName = "ReportPenggunaanLahan(" + tableName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                SearchDashboard param = new SearchDashboard();
                param.BE_ID = parame.BE_ID;
                param.jenis = parame.STATUS;
                param.jenis2 = parame.TYPE;
                param.end = parame.end;
                //string KodeCabang = "100";
                HomeDAL dal = new HomeDAL();
                var temp = dal.GetDetailExcel(param, CurrentUser.KodeCabang);
                dynamic dalShowdata = temp.Data;

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[13] {
                new DataColumn("No", typeof(string)),
                new DataColumn("NO RO", typeof(string)),
                new DataColumn("NAMA PENYEWA", typeof(string)),
                new DataColumn("ALAMAT LOKASI LAHAN", typeof(string)),
                new DataColumn("LUAS LAHAN", typeof(string)),
                new DataColumn("SERTIFIKAT LAHAN",typeof(string)),
                new DataColumn("NOMOR ASET LAHAN", typeof(string)),
                new DataColumn("RIP",typeof(string)),
                new DataColumn("NO KONTRAK", typeof(string)),
                new DataColumn("JANGKA WAKTU SEWA", typeof(string)),
                new DataColumn("START DATE", typeof(string)),
                new DataColumn("END DATE", typeof(string)),
                new DataColumn("STATUS PEMBAYARAN", typeof(string))
                });

                int no = 1;

                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(no, valData.RO_CODE, valData.BUSINESS_PARTNER_NAME,  valData.RO_ADDRESS,
                                valData.LAND_DIMENSION, valData.RO_CERTIFICATE_NUMBER,
                                valData.KODE_ASET, valData.ZONE_RIP, valData.CONTRACT_NO,
                                valData.TERM_IN_MONTHS, valData.CONTRACT_START_DATE, valData.CONTRACT_END_DATE, valData.CONTRACT_NO);

                    no++;
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Dashboard Lahan");
                    wb.SaveAs(System.IO.Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }


        [Authorize]
        [HttpPost]
        public JsonResult defineExcel_7([FromBody] DashExcelParam parame)
        {
            nama result = new nama();
            var tableName = parame.TYPE;
            // var kolomName = "";
            string fileName = "ReportPenggunaanLahan(" + tableName + ")(" + DateTime.Now.ToString("yyyyMMddHHmm") + ").xlsx";

            try
            {
                SearchDashboard param = new SearchDashboard();
                param.BE_ID = parame.BE_ID;
                param.jenis = parame.STATUS;
                param.jenis2 = parame.TYPE;
                param.end = parame.end;
                //string KodeCabang = "100";
                HomeDAL dal = new HomeDAL();
                var temp = dal.GetDetailExcel(param, CurrentUser.KodeCabang);
                dynamic dalShowdata = temp.Data;

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[7] {
                new DataColumn("NO", typeof(string)),
                new DataColumn("NAMA OBJEK LAHAN", typeof(string)),
                new DataColumn("NO RO", typeof(string)),
                new DataColumn("ALAMAT LOKASI LAHAN", typeof(string)),
                new DataColumn("LUAS LAHAN", typeof(string)),
                new DataColumn("SERTIFIKAT LAHAN",typeof(string)),
                new DataColumn("NOMOR ASET LAHAN", typeof(string))
                });
                int no = 1;
                foreach (var valData in dalShowdata)
                {
                    dt.Rows.Add(no, valData.RO_NAME, valData.RO_CODE, valData.RO_ADDRESS,
                                valData.LAND_DIMENSION, valData.RO_CERTIFICATE_NUMBER,
                                valData.KODE_ASET);
                    no++;
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    string rootPath = ((IWebHostEnvironment)HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment))).WebRootPath;
                    wb.Worksheets.Add(dt, "Report Dashboard Lahan");
                    wb.SaveAs(System.IO.Path.Combine(rootPath, "REMOTE", "Excel", fileName));
                }
            }
            catch (Exception) { }

            result.fileNames = fileName;
            var nameFile = result.fileNames;
            return Json(new { data = nameFile });
        }



        [Authorize]
        public ActionResult DownloadFile()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "~/REMOTE/Download/";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "MANUAL_BOOK.Pdf");
            string fileName = "MANUAL_BOOK.pdf";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


    }
}