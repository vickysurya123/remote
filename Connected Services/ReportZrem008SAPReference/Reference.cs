﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportZrem008SAPReference
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://integrator.pelindo.co.id/", ConfigurationName="ReportZrem008SAPReference.REMOTEIBZREM008Soap")]
    public interface REMOTEIBZREM008Soap
    {
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundZrem008", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<ReportZrem008SAPReference.inboundZrem008Response> inboundZrem008Async(ReportZrem008SAPReference.inboundZrem008Request request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZST_ZFMREM_IB_008
    {
        
        private string cONTRACTField;
        
        private string roField;
        
        private string cONDTYPEField;
        
        private string iTEM_TRXField;
        
        private string kD_CATEGORYField;
        
        private string bUKRSField;
        
        private string bELNRField;
        
        private string gJAHRField;
        
        private string bUZEIField;
        
        private string pRCTRField;
        
        private string pRCTR2Field;
        
        private string kTEXTField;
        
        private string bULANField;
        
        private string bILLING_NOField;
        
        private string mASA_SEWAField;
        
        private string pERIODField;
        
        private string cONTRACT_STARTField;
        
        private string cONTRACT_ENDField;
        
        private string lEGAL_CONTRACTField;
        
        private string pOSTING_DATEField;
        
        private string cUSTOMERField;
        
        private string nAME1Field;
        
        private string kETERANGANField;
        
        private string gL_REVENUEField;
        
        private decimal qUANTITYField;
        
        private bool qUANTITYFieldSpecified;
        
        private string bASE_UOMField;
        
        private string cURRENCYField;
        
        private decimal vALUE_CONTRACTField;
        
        private bool vALUE_CONTRACTFieldSpecified;
        
        private decimal vALUE_PER_MONTHField;
        
        private bool vALUE_PER_MONTHFieldSpecified;
        
        private decimal aR_DField;
        
        private bool aR_DFieldSpecified;
        
        private decimal pANJANG_DField;
        
        private bool pANJANG_DFieldSpecified;
        
        private decimal pENDEK_DField;
        
        private bool pENDEK_DFieldSpecified;
        
        private decimal pENDAPATAN_DField;
        
        private bool pENDAPATAN_DFieldSpecified;
        
        private decimal pANJANG_CField;
        
        private bool pANJANG_CFieldSpecified;
        
        private decimal pENDEK_CField;
        
        private bool pENDEK_CFieldSpecified;
        
        private decimal pENDAPATAN_CField;
        
        private bool pENDAPATAN_CFieldSpecified;
        
        private decimal pANJANG_SALDOField;
        
        private bool pANJANG_SALDOFieldSpecified;
        
        private decimal pENDEK_SALDOField;
        
        private bool pENDEK_SALDOFieldSpecified;
        
        private decimal sISA_PENDEK_SALDOField;
        
        private bool sISA_PENDEK_SALDOFieldSpecified;
        
        private decimal pENDAPATAN_SALDOField;
        
        private bool pENDAPATAN_SALDOFieldSpecified;
        
        private string pOST_STATUSField;
        
        private string cANCEL_STATUSField;
        
        private string tERMINATE_STATUSField;
        
        private string cREATED_BYField;
        
        private string cREATED_DATEField;
        
        private string lAST_CHANGED_BYField;
        
        private string lAST_CHANGED_DATEField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string CONTRACT
        {
            get
            {
                return this.cONTRACTField;
            }
            set
            {
                this.cONTRACTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string RO
        {
            get
            {
                return this.roField;
            }
            set
            {
                this.roField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CONDTYPE
        {
            get
            {
                return this.cONDTYPEField;
            }
            set
            {
                this.cONDTYPEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string ITEM_TRX
        {
            get
            {
                return this.iTEM_TRXField;
            }
            set
            {
                this.iTEM_TRXField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string KD_CATEGORY
        {
            get
            {
                return this.kD_CATEGORYField;
            }
            set
            {
                this.kD_CATEGORYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string BUKRS
        {
            get
            {
                return this.bUKRSField;
            }
            set
            {
                this.bUKRSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string BELNR
        {
            get
            {
                return this.bELNRField;
            }
            set
            {
                this.bELNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string GJAHR
        {
            get
            {
                return this.gJAHRField;
            }
            set
            {
                this.gJAHRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string BUZEI
        {
            get
            {
                return this.bUZEIField;
            }
            set
            {
                this.bUZEIField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public string PRCTR
        {
            get
            {
                return this.pRCTRField;
            }
            set
            {
                this.pRCTRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=10)]
        public string PRCTR2
        {
            get
            {
                return this.pRCTR2Field;
            }
            set
            {
                this.pRCTR2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=11)]
        public string KTEXT
        {
            get
            {
                return this.kTEXTField;
            }
            set
            {
                this.kTEXTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=12)]
        public string BULAN
        {
            get
            {
                return this.bULANField;
            }
            set
            {
                this.bULANField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=13)]
        public string BILLING_NO
        {
            get
            {
                return this.bILLING_NOField;
            }
            set
            {
                this.bILLING_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=14)]
        public string MASA_SEWA
        {
            get
            {
                return this.mASA_SEWAField;
            }
            set
            {
                this.mASA_SEWAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=15)]
        public string PERIOD
        {
            get
            {
                return this.pERIODField;
            }
            set
            {
                this.pERIODField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=16)]
        public string CONTRACT_START
        {
            get
            {
                return this.cONTRACT_STARTField;
            }
            set
            {
                this.cONTRACT_STARTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=17)]
        public string CONTRACT_END
        {
            get
            {
                return this.cONTRACT_ENDField;
            }
            set
            {
                this.cONTRACT_ENDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=18)]
        public string LEGAL_CONTRACT
        {
            get
            {
                return this.lEGAL_CONTRACTField;
            }
            set
            {
                this.lEGAL_CONTRACTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=19)]
        public string POSTING_DATE
        {
            get
            {
                return this.pOSTING_DATEField;
            }
            set
            {
                this.pOSTING_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=20)]
        public string CUSTOMER
        {
            get
            {
                return this.cUSTOMERField;
            }
            set
            {
                this.cUSTOMERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=21)]
        public string NAME1
        {
            get
            {
                return this.nAME1Field;
            }
            set
            {
                this.nAME1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=22)]
        public string KETERANGAN
        {
            get
            {
                return this.kETERANGANField;
            }
            set
            {
                this.kETERANGANField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=23)]
        public string GL_REVENUE
        {
            get
            {
                return this.gL_REVENUEField;
            }
            set
            {
                this.gL_REVENUEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=24)]
        public decimal QUANTITY
        {
            get
            {
                return this.qUANTITYField;
            }
            set
            {
                this.qUANTITYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QUANTITYSpecified
        {
            get
            {
                return this.qUANTITYFieldSpecified;
            }
            set
            {
                this.qUANTITYFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=25)]
        public string BASE_UOM
        {
            get
            {
                return this.bASE_UOMField;
            }
            set
            {
                this.bASE_UOMField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=26)]
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=27)]
        public decimal VALUE_CONTRACT
        {
            get
            {
                return this.vALUE_CONTRACTField;
            }
            set
            {
                this.vALUE_CONTRACTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VALUE_CONTRACTSpecified
        {
            get
            {
                return this.vALUE_CONTRACTFieldSpecified;
            }
            set
            {
                this.vALUE_CONTRACTFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=28)]
        public decimal VALUE_PER_MONTH
        {
            get
            {
                return this.vALUE_PER_MONTHField;
            }
            set
            {
                this.vALUE_PER_MONTHField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VALUE_PER_MONTHSpecified
        {
            get
            {
                return this.vALUE_PER_MONTHFieldSpecified;
            }
            set
            {
                this.vALUE_PER_MONTHFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=29)]
        public decimal AR_D
        {
            get
            {
                return this.aR_DField;
            }
            set
            {
                this.aR_DField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AR_DSpecified
        {
            get
            {
                return this.aR_DFieldSpecified;
            }
            set
            {
                this.aR_DFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=30)]
        public decimal PANJANG_D
        {
            get
            {
                return this.pANJANG_DField;
            }
            set
            {
                this.pANJANG_DField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PANJANG_DSpecified
        {
            get
            {
                return this.pANJANG_DFieldSpecified;
            }
            set
            {
                this.pANJANG_DFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=31)]
        public decimal PENDEK_D
        {
            get
            {
                return this.pENDEK_DField;
            }
            set
            {
                this.pENDEK_DField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDEK_DSpecified
        {
            get
            {
                return this.pENDEK_DFieldSpecified;
            }
            set
            {
                this.pENDEK_DFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=32)]
        public decimal PENDAPATAN_D
        {
            get
            {
                return this.pENDAPATAN_DField;
            }
            set
            {
                this.pENDAPATAN_DField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDAPATAN_DSpecified
        {
            get
            {
                return this.pENDAPATAN_DFieldSpecified;
            }
            set
            {
                this.pENDAPATAN_DFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=33)]
        public decimal PANJANG_C
        {
            get
            {
                return this.pANJANG_CField;
            }
            set
            {
                this.pANJANG_CField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PANJANG_CSpecified
        {
            get
            {
                return this.pANJANG_CFieldSpecified;
            }
            set
            {
                this.pANJANG_CFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=34)]
        public decimal PENDEK_C
        {
            get
            {
                return this.pENDEK_CField;
            }
            set
            {
                this.pENDEK_CField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDEK_CSpecified
        {
            get
            {
                return this.pENDEK_CFieldSpecified;
            }
            set
            {
                this.pENDEK_CFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=35)]
        public decimal PENDAPATAN_C
        {
            get
            {
                return this.pENDAPATAN_CField;
            }
            set
            {
                this.pENDAPATAN_CField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDAPATAN_CSpecified
        {
            get
            {
                return this.pENDAPATAN_CFieldSpecified;
            }
            set
            {
                this.pENDAPATAN_CFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=36)]
        public decimal PANJANG_SALDO
        {
            get
            {
                return this.pANJANG_SALDOField;
            }
            set
            {
                this.pANJANG_SALDOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PANJANG_SALDOSpecified
        {
            get
            {
                return this.pANJANG_SALDOFieldSpecified;
            }
            set
            {
                this.pANJANG_SALDOFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=37)]
        public decimal PENDEK_SALDO
        {
            get
            {
                return this.pENDEK_SALDOField;
            }
            set
            {
                this.pENDEK_SALDOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDEK_SALDOSpecified
        {
            get
            {
                return this.pENDEK_SALDOFieldSpecified;
            }
            set
            {
                this.pENDEK_SALDOFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=38)]
        public decimal SISA_PENDEK_SALDO
        {
            get
            {
                return this.sISA_PENDEK_SALDOField;
            }
            set
            {
                this.sISA_PENDEK_SALDOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SISA_PENDEK_SALDOSpecified
        {
            get
            {
                return this.sISA_PENDEK_SALDOFieldSpecified;
            }
            set
            {
                this.sISA_PENDEK_SALDOFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=39)]
        public decimal PENDAPATAN_SALDO
        {
            get
            {
                return this.pENDAPATAN_SALDOField;
            }
            set
            {
                this.pENDAPATAN_SALDOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PENDAPATAN_SALDOSpecified
        {
            get
            {
                return this.pENDAPATAN_SALDOFieldSpecified;
            }
            set
            {
                this.pENDAPATAN_SALDOFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=40)]
        public string POST_STATUS
        {
            get
            {
                return this.pOST_STATUSField;
            }
            set
            {
                this.pOST_STATUSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=41)]
        public string CANCEL_STATUS
        {
            get
            {
                return this.cANCEL_STATUSField;
            }
            set
            {
                this.cANCEL_STATUSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=42)]
        public string TERMINATE_STATUS
        {
            get
            {
                return this.tERMINATE_STATUSField;
            }
            set
            {
                this.tERMINATE_STATUSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=43)]
        public string CREATED_BY
        {
            get
            {
                return this.cREATED_BYField;
            }
            set
            {
                this.cREATED_BYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=44)]
        public string CREATED_DATE
        {
            get
            {
                return this.cREATED_DATEField;
            }
            set
            {
                this.cREATED_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=45)]
        public string LAST_CHANGED_BY
        {
            get
            {
                return this.lAST_CHANGED_BYField;
            }
            set
            {
                this.lAST_CHANGED_BYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=46)]
        public string LAST_CHANGED_DATE
        {
            get
            {
                return this.lAST_CHANGED_DATEField;
            }
            set
            {
                this.lAST_CHANGED_DATEField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class ZFMREM_IB_008Response
    {
        
        private ZST_ZFMREM_IB_008[] t_DATAField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        public ZST_ZFMREM_IB_008[] T_DATA
        {
            get
            {
                return this.t_DATAField;
            }
            set
            {
                this.t_DATAField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZST_ZFMREM_IB_007B
    {
        
        private string pROFIT_CENTERField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string PROFIT_CENTER
        {
            get
            {
                return this.pROFIT_CENTERField;
            }
            set
            {
                this.pROFIT_CENTERField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZST_ZFMREM_IB_007C
    {
        
        private string pOSTING_DATEField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string POSTING_DATE
        {
            get
            {
                return this.pOSTING_DATEField;
            }
            set
            {
                this.pOSTING_DATEField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="inboundZrem008", WrapperNamespace="http://integrator.pelindo.co.id/", IsWrapped=true)]
    public partial class inboundZrem008Request
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=0)]
        public string I_POSTING_DATE_FR;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=1)]
        public string I_POSTING_DATE_TO;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=2)]
        public string I_REPORT_TYPE;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=3)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_008[] T_DATA;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=4)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_007C[] T_POSTING_DATE;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=5)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_007B[] T_PROFIT_CENTER;
        
        public inboundZrem008Request()
        {
        }
        
        public inboundZrem008Request(string I_POSTING_DATE_FR, string I_POSTING_DATE_TO, string I_REPORT_TYPE, ReportZrem008SAPReference.ZST_ZFMREM_IB_008[] T_DATA, ReportZrem008SAPReference.ZST_ZFMREM_IB_007C[] T_POSTING_DATE, ReportZrem008SAPReference.ZST_ZFMREM_IB_007B[] T_PROFIT_CENTER)
        {
            this.I_POSTING_DATE_FR = I_POSTING_DATE_FR;
            this.I_POSTING_DATE_TO = I_POSTING_DATE_TO;
            this.I_REPORT_TYPE = I_REPORT_TYPE;
            this.T_DATA = T_DATA;
            this.T_POSTING_DATE = T_POSTING_DATE;
            this.T_PROFIT_CENTER = T_PROFIT_CENTER;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="inboundZrem008Response", WrapperNamespace="http://integrator.pelindo.co.id/", IsWrapped=true)]
    public partial class inboundZrem008Response
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=0)]
        public ReportZrem008SAPReference.ZFMREM_IB_008Response inboundZrem008Result;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=1)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_008[] T_DATA;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=2)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_007C[] T_POSTING_DATE;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://integrator.pelindo.co.id/", Order=3)]
        public ReportZrem008SAPReference.ZST_ZFMREM_IB_007B[] T_PROFIT_CENTER;
        
        public inboundZrem008Response()
        {
        }
        
        public inboundZrem008Response(ReportZrem008SAPReference.ZFMREM_IB_008Response inboundZrem008Result, ReportZrem008SAPReference.ZST_ZFMREM_IB_008[] T_DATA, ReportZrem008SAPReference.ZST_ZFMREM_IB_007C[] T_POSTING_DATE, ReportZrem008SAPReference.ZST_ZFMREM_IB_007B[] T_PROFIT_CENTER)
        {
            this.inboundZrem008Result = inboundZrem008Result;
            this.T_DATA = T_DATA;
            this.T_POSTING_DATE = T_POSTING_DATE;
            this.T_PROFIT_CENTER = T_PROFIT_CENTER;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface REMOTEIBZREM008SoapChannel : ReportZrem008SAPReference.REMOTEIBZREM008Soap, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class REMOTEIBZREM008SoapClient : System.ServiceModel.ClientBase<ReportZrem008SAPReference.REMOTEIBZREM008Soap>, ReportZrem008SAPReference.REMOTEIBZREM008Soap
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public REMOTEIBZREM008SoapClient(EndpointConfiguration endpointConfiguration) : 
                base(REMOTEIBZREM008SoapClient.GetBindingForEndpoint(endpointConfiguration), REMOTEIBZREM008SoapClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public REMOTEIBZREM008SoapClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(REMOTEIBZREM008SoapClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public REMOTEIBZREM008SoapClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(REMOTEIBZREM008SoapClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public REMOTEIBZREM008SoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<ReportZrem008SAPReference.inboundZrem008Response> inboundZrem008Async(ReportZrem008SAPReference.inboundZrem008Request request)
        {
            return base.Channel.inboundZrem008Async(request);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.REMOTEIBZREM008Soap))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            if ((endpointConfiguration == EndpointConfiguration.REMOTEIBZREM008Soap12))
            {
                System.ServiceModel.Channels.CustomBinding result = new System.ServiceModel.Channels.CustomBinding();
                System.ServiceModel.Channels.TextMessageEncodingBindingElement textBindingElement = new System.ServiceModel.Channels.TextMessageEncodingBindingElement();
                textBindingElement.MessageVersion = System.ServiceModel.Channels.MessageVersion.CreateVersion(System.ServiceModel.EnvelopeVersion.Soap12, System.ServiceModel.Channels.AddressingVersion.None);
                result.Elements.Add(textBindingElement);
                System.ServiceModel.Channels.HttpTransportBindingElement httpBindingElement = new System.ServiceModel.Channels.HttpTransportBindingElement();
                httpBindingElement.AllowCookies = true;
                httpBindingElement.MaxBufferSize = int.MaxValue;
                httpBindingElement.MaxReceivedMessageSize = int.MaxValue;
                result.Elements.Add(httpBindingElement);
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.REMOTEIBZREM008Soap))
            {
                return new System.ServiceModel.EndpointAddress("http://integrator.pelindo.co.id/REMOTEIBZREM008.asmx");
            }
            if ((endpointConfiguration == EndpointConfiguration.REMOTEIBZREM008Soap12))
            {
                return new System.ServiceModel.EndpointAddress("http://integrator.pelindo.co.id/REMOTEIBZREM008.asmx");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        public enum EndpointConfiguration
        {
            
            REMOTEIBZREM008Soap,
            
            REMOTEIBZREM008Soap12,
        }
    }
}
