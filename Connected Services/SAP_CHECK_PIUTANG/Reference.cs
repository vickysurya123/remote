﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_CHECK_PIUTANG
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://integrator.pelindo.co.id/", ConfigurationName="SAP_CHECK_PIUTANG.CheckLockPiutangSoap")]
    public interface CheckLockPiutangSoap
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundCheckLock", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckLock> inboundCheckLockAsync(string I_BKTXT, string I_BLART, string I_CUSTOMER);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundCheckLock_P3", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckLock> inboundCheckLock_P3Async(string BKTXT, string BLART, string KD_CABANG, string CUSTOMER);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundCheckAdvPay_P3", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<string> inboundCheckAdvPay_P3Async(string BKTXT, string BLART, string KD_CABANG, string CUSTOMER);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/outboundListPiutang", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retListPiutang> outboundListPiutangAsync(string KD_CABANG, string CUSTOMER);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundCheckUtip", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.ZAJG_UTIP[]> inboundCheckUtipAsync(string I_CUSTOMER, string I_PROFCENTER, string I_SPGL);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://integrator.pelindo.co.id/inboundCheckNota", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckNota> inboundCheckNotaAsync(string I_BELNR, string I_BUKRS, string I_GJAHR, string I_PROFCENTER);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class retCheckLock
    {
        
        private string e_DESCRIPTIONField;
        
        private string e_RELEASE_STATUSField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string E_DESCRIPTION
        {
            get
            {
                return this.e_DESCRIPTIONField;
            }
            set
            {
                this.e_DESCRIPTIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string E_RELEASE_STATUS
        {
            get
            {
                return this.e_RELEASE_STATUSField;
            }
            set
            {
                this.e_RELEASE_STATUSField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class retCheckNota
    {
        
        private decimal e_AMOUNTField;
        
        private string e_CLEARING_DATEField;
        
        private string e_INVOICE_STATUSField;
        
        private string e_KODE_BAYARField;
        
        private string e_POSTING_DATEField;
        
        private string e_TAXNUMBERField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public decimal E_AMOUNT
        {
            get
            {
                return this.e_AMOUNTField;
            }
            set
            {
                this.e_AMOUNTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string E_CLEARING_DATE
        {
            get
            {
                return this.e_CLEARING_DATEField;
            }
            set
            {
                this.e_CLEARING_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string E_INVOICE_STATUS
        {
            get
            {
                return this.e_INVOICE_STATUSField;
            }
            set
            {
                this.e_INVOICE_STATUSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string E_KODE_BAYAR
        {
            get
            {
                return this.e_KODE_BAYARField;
            }
            set
            {
                this.e_KODE_BAYARField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string E_POSTING_DATE
        {
            get
            {
                return this.e_POSTING_DATEField;
            }
            set
            {
                this.e_POSTING_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string E_TAXNUMBER
        {
            get
            {
                return this.e_TAXNUMBERField;
            }
            set
            {
                this.e_TAXNUMBERField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZAJG_UTIP
    {
        
        private string bELNRField;
        
        private string gJAHRField;
        
        private string bLARTField;
        
        private string bUDATField;
        
        private string pRCTRField;
        
        private string wAERSField;
        
        private decimal wRBTRField;
        
        private bool wRBTRFieldSpecified;
        
        private string zUONRField;
        
        private string xBLNRField;
        
        private string sGTXTField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string BELNR
        {
            get
            {
                return this.bELNRField;
            }
            set
            {
                this.bELNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string GJAHR
        {
            get
            {
                return this.gJAHRField;
            }
            set
            {
                this.gJAHRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string BLART
        {
            get
            {
                return this.bLARTField;
            }
            set
            {
                this.bLARTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string BUDAT
        {
            get
            {
                return this.bUDATField;
            }
            set
            {
                this.bUDATField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string PRCTR
        {
            get
            {
                return this.pRCTRField;
            }
            set
            {
                this.pRCTRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string WAERS
        {
            get
            {
                return this.wAERSField;
            }
            set
            {
                this.wAERSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public decimal WRBTR
        {
            get
            {
                return this.wRBTRField;
            }
            set
            {
                this.wRBTRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WRBTRSpecified
        {
            get
            {
                return this.wRBTRFieldSpecified;
            }
            set
            {
                this.wRBTRFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string ZUONR
        {
            get
            {
                return this.zUONRField;
            }
            set
            {
                this.zUONRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string XBLNR
        {
            get
            {
                return this.xBLNRField;
            }
            set
            {
                this.xBLNRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public string SGTXT
        {
            get
            {
                return this.sGTXTField;
            }
            set
            {
                this.sGTXTField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class LCK_V_NOTA_LOCKING
    {
        
        private System.Nullable<long> kD_CABANGField;
        
        private string nAMA_CABANGField;
        
        private string nO_NOTAField;
        
        private string nO_REF1Field;
        
        private string nO_REF2Field;
        
        private string nO_REF3Field;
        
        private string tIPE_TRANSField;
        
        private string kD_PELANGGANField;
        
        private System.Nullable<long> sISA_BAYARField;
        
        private string nO_APPROVALField;
        
        private string sAP_CUSTOMER_CODE_DOCField;
        
        private string sAP_CUSTOMER_CODEField;
        
        private string sAP_DOC_NOField;
        
        private string sAP_CREATION_DOC_NOField;
        
        private System.Nullable<System.DateTime> tGL_POSTINGField;
        
        private System.Nullable<System.DateTime> tGL_PERPANJANGAN_MAXField;
        
        private System.Nullable<long> kANPUSField;
        
        private string kD_VALUTAField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public System.Nullable<long> KD_CABANG
        {
            get
            {
                return this.kD_CABANGField;
            }
            set
            {
                this.kD_CABANGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string NAMA_CABANG
        {
            get
            {
                return this.nAMA_CABANGField;
            }
            set
            {
                this.nAMA_CABANGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string NO_NOTA
        {
            get
            {
                return this.nO_NOTAField;
            }
            set
            {
                this.nO_NOTAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string NO_REF1
        {
            get
            {
                return this.nO_REF1Field;
            }
            set
            {
                this.nO_REF1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string NO_REF2
        {
            get
            {
                return this.nO_REF2Field;
            }
            set
            {
                this.nO_REF2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string NO_REF3
        {
            get
            {
                return this.nO_REF3Field;
            }
            set
            {
                this.nO_REF3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string TIPE_TRANS
        {
            get
            {
                return this.tIPE_TRANSField;
            }
            set
            {
                this.tIPE_TRANSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string KD_PELANGGAN
        {
            get
            {
                return this.kD_PELANGGANField;
            }
            set
            {
                this.kD_PELANGGANField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=8)]
        public System.Nullable<long> SISA_BAYAR
        {
            get
            {
                return this.sISA_BAYARField;
            }
            set
            {
                this.sISA_BAYARField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string NO_APPROVAL
        {
            get
            {
                return this.nO_APPROVALField;
            }
            set
            {
                this.nO_APPROVALField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string SAP_CUSTOMER_CODE_DOC
        {
            get
            {
                return this.sAP_CUSTOMER_CODE_DOCField;
            }
            set
            {
                this.sAP_CUSTOMER_CODE_DOCField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public string SAP_CUSTOMER_CODE
        {
            get
            {
                return this.sAP_CUSTOMER_CODEField;
            }
            set
            {
                this.sAP_CUSTOMER_CODEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=12)]
        public string SAP_DOC_NO
        {
            get
            {
                return this.sAP_DOC_NOField;
            }
            set
            {
                this.sAP_DOC_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=13)]
        public string SAP_CREATION_DOC_NO
        {
            get
            {
                return this.sAP_CREATION_DOC_NOField;
            }
            set
            {
                this.sAP_CREATION_DOC_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=14)]
        public System.Nullable<System.DateTime> TGL_POSTING
        {
            get
            {
                return this.tGL_POSTINGField;
            }
            set
            {
                this.tGL_POSTINGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=15)]
        public System.Nullable<System.DateTime> TGL_PERPANJANGAN_MAX
        {
            get
            {
                return this.tGL_PERPANJANGAN_MAXField;
            }
            set
            {
                this.tGL_PERPANJANGAN_MAXField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=16)]
        public System.Nullable<long> KANPUS
        {
            get
            {
                return this.kANPUSField;
            }
            set
            {
                this.kANPUSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=17)]
        public string KD_VALUTA
        {
            get
            {
                return this.kD_VALUTAField;
            }
            set
            {
                this.kD_VALUTAField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class VW_LCK_PELANGGAN
    {
        
        private System.Nullable<long> kD_CABANGField;
        
        private string mPLG_KODEField;
        
        private string mPLG_KODE_SAPField;
        
        private string mPLG_NAMAField;
        
        private string mPLG_NPWPField;
        
        private System.Nullable<long> sALDO_PIUTANGField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public System.Nullable<long> KD_CABANG
        {
            get
            {
                return this.kD_CABANGField;
            }
            set
            {
                this.kD_CABANGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string MPLG_KODE
        {
            get
            {
                return this.mPLG_KODEField;
            }
            set
            {
                this.mPLG_KODEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string MPLG_KODE_SAP
        {
            get
            {
                return this.mPLG_KODE_SAPField;
            }
            set
            {
                this.mPLG_KODE_SAPField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string MPLG_NAMA
        {
            get
            {
                return this.mPLG_NAMAField;
            }
            set
            {
                this.mPLG_NAMAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string MPLG_NPWP
        {
            get
            {
                return this.mPLG_NPWPField;
            }
            set
            {
                this.mPLG_NPWPField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=5)]
        public System.Nullable<long> SALDO_PIUTANG
        {
            get
            {
                return this.sALDO_PIUTANGField;
            }
            set
            {
                this.sALDO_PIUTANGField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://integrator.pelindo.co.id/")]
    public partial class retListPiutang
    {
        
        private string kD_CABANGField;
        
        private string cUSTOMERField;
        
        private VW_LCK_PELANGGAN[] h_DATAField;
        
        private LCK_V_NOTA_LOCKING[] d_DATAField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string KD_CABANG
        {
            get
            {
                return this.kD_CABANGField;
            }
            set
            {
                this.kD_CABANGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string CUSTOMER
        {
            get
            {
                return this.cUSTOMERField;
            }
            set
            {
                this.cUSTOMERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=2)]
        public VW_LCK_PELANGGAN[] H_DATA
        {
            get
            {
                return this.h_DATAField;
            }
            set
            {
                this.h_DATAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=3)]
        public LCK_V_NOTA_LOCKING[] D_DATA
        {
            get
            {
                return this.d_DATAField;
            }
            set
            {
                this.d_DATAField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface CheckLockPiutangSoapChannel : SAP_CHECK_PIUTANG.CheckLockPiutangSoap, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class CheckLockPiutangSoapClient : System.ServiceModel.ClientBase<SAP_CHECK_PIUTANG.CheckLockPiutangSoap>, SAP_CHECK_PIUTANG.CheckLockPiutangSoap
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public CheckLockPiutangSoapClient(EndpointConfiguration endpointConfiguration) : 
                base(CheckLockPiutangSoapClient.GetBindingForEndpoint(endpointConfiguration), CheckLockPiutangSoapClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public CheckLockPiutangSoapClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(CheckLockPiutangSoapClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public CheckLockPiutangSoapClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(CheckLockPiutangSoapClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public CheckLockPiutangSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckLock> inboundCheckLockAsync(string I_BKTXT, string I_BLART, string I_CUSTOMER)
        {
            return base.Channel.inboundCheckLockAsync(I_BKTXT, I_BLART, I_CUSTOMER);
        }
        
        public System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckLock> inboundCheckLock_P3Async(string BKTXT, string BLART, string KD_CABANG, string CUSTOMER)
        {
            return base.Channel.inboundCheckLock_P3Async(BKTXT, BLART, KD_CABANG, CUSTOMER);
        }
        
        public System.Threading.Tasks.Task<string> inboundCheckAdvPay_P3Async(string BKTXT, string BLART, string KD_CABANG, string CUSTOMER)
        {
            return base.Channel.inboundCheckAdvPay_P3Async(BKTXT, BLART, KD_CABANG, CUSTOMER);
        }
        
        public System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retListPiutang> outboundListPiutangAsync(string KD_CABANG, string CUSTOMER)
        {
            return base.Channel.outboundListPiutangAsync(KD_CABANG, CUSTOMER);
        }
        
        public System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.ZAJG_UTIP[]> inboundCheckUtipAsync(string I_CUSTOMER, string I_PROFCENTER, string I_SPGL)
        {
            return base.Channel.inboundCheckUtipAsync(I_CUSTOMER, I_PROFCENTER, I_SPGL);
        }
        
        public System.Threading.Tasks.Task<SAP_CHECK_PIUTANG.retCheckNota> inboundCheckNotaAsync(string I_BELNR, string I_BUKRS, string I_GJAHR, string I_PROFCENTER)
        {
            return base.Channel.inboundCheckNotaAsync(I_BELNR, I_BUKRS, I_GJAHR, I_PROFCENTER);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.CheckLockPiutangSoap))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            if ((endpointConfiguration == EndpointConfiguration.CheckLockPiutangSoap12))
            {
                System.ServiceModel.Channels.CustomBinding result = new System.ServiceModel.Channels.CustomBinding();
                System.ServiceModel.Channels.TextMessageEncodingBindingElement textBindingElement = new System.ServiceModel.Channels.TextMessageEncodingBindingElement();
                textBindingElement.MessageVersion = System.ServiceModel.Channels.MessageVersion.CreateVersion(System.ServiceModel.EnvelopeVersion.Soap12, System.ServiceModel.Channels.AddressingVersion.None);
                result.Elements.Add(textBindingElement);
                System.ServiceModel.Channels.HttpTransportBindingElement httpBindingElement = new System.ServiceModel.Channels.HttpTransportBindingElement();
                httpBindingElement.AllowCookies = true;
                httpBindingElement.MaxBufferSize = int.MaxValue;
                httpBindingElement.MaxReceivedMessageSize = int.MaxValue;
                result.Elements.Add(httpBindingElement);
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.CheckLockPiutangSoap))
            {
                return new System.ServiceModel.EndpointAddress("http://integrator.pelindo.co.id/CheckLockPiutang.asmx");
            }
            if ((endpointConfiguration == EndpointConfiguration.CheckLockPiutangSoap12))
            {
                return new System.ServiceModel.EndpointAddress("http://integrator.pelindo.co.id/CheckLockPiutang.asmx");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        public enum EndpointConfiguration
        {
            
            CheckLockPiutangSoap,
            
            CheckLockPiutangSoap12,
        }
    }
}
