﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Remote
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal)
            : base(principal)
        {
        }

        public string Name
        {
            get
            {
                return this.FindFirst(ClaimTypes.Name).Value;
            }
        }
        public string UserID
        {
            get
            {
                return this.FindFirst("USER_ID").Value;
            }
        }
        public string UserLoginID
        {
            get
            {
                return this.FindFirst("USER_LOGIN").Value;
            }
        }
        public string KodeCabang
        {
            get
            {
                return this.FindFirst("KD_CABANG").Value;
            }
        }

        public string KodeCurrency
        {
            get
            {
                return this.FindFirst("KD_CURRENCY").Value;
            }
        }
        public string NamaCabang
        {
            get
            {
                return this.FindFirst("NAMA_CABANG").Value;
            }
        }
        public string UserRoleID
        {
            get
            {
                return this.FindFirst("USER_ROLE_ID").Value;
            }
        }

        public string UserRoleName
        {
            get
            {
                return this.FindFirst("ROLE_NAME").Value;
            }
        }
        public string KodeTerminal
        {
            get
            {
                return this.FindFirst("KD_TERMINAL").Value;
            }
        }
        public string ProfitCenter
        {
            get
            {
                return this.FindFirst("PROFIT_CENTER").Value;
            }
        }
        
        public string UserRoleNama
        {
            get
            {
                return this.FindFirst("USER_NAMA").Value;
            }
        }
        public string UserRoleProperty
        {
            get
            {
                return this.FindFirst("PROPERTY_ROLE").Value;
            }
        }
        public string UserEmail
        {
            get
            {
                return this.FindFirst("USER_EMAIL").Value;
            }
        }
        public string UserPhone
        {
            get
            {
                return this.FindFirst("USER_PHONE").Value;
            }
        }

        //Generate Session ID for Log Login
       
        public string generateID
        {
            get
            {
                
                return "test";
            }
        }
        
    }
}